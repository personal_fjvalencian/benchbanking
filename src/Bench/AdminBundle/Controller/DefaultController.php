<?php

namespace Bench\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bench\AdminBundle\Entity\Admin;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\UsuariosBundle\Entity\Conyuge;
use Bench\TienesBundle\Entity\Bienesraices;
use Bench\TienesBundle\Entity\Ahorroinverson;
use Bench\TienesBundle\Entity\Automoviles;
use Bench\TienesBundle\Entity\Sociedades;
use Bench\DebesBundle\Entity\DebesCreditoConsumo;
use Bench\DebesBundle\Entity\DebesLineaCredito;
use Bench\DebesBundle\Entity\DebesTarjetaCredito;
use Bench\AdminBundle\Controller\PDFMerger;
use Bench\DebesBundle\Entity\Debeshipotecario;
use Bench\UsuariosBundle\Entity\Cotizaciones;
use Bench\CreditosBundle\Entity\CreditoAutomotriz;
use Bench\CreditosBundle\Entity\CreditoConsolidacion;
use Bench\CreditosBundle\Entity\CreditoConsumo;
use Bench\CreditosBundle\Entity\CreditoCuenta;
use Bench\CreditosBundle\Entity\CreditoHipotecario;
use Bench\UsuariosBundle\mail\Postmark;



use Ps\PdfBundle\Annotation\Pdf;






class DefaultController extends Controller {



    public function indexAction() {

        return $this->render('BenchAdminBundle:Default:index.html.twig');
    }


    public function pruebaRemaxAction() {
        return $this->render('BenchAdminBundle:Default:remaxprueba.html.twig');
    }


    public function pruebaCapiAction(){
        return $this->render('BenchAdminBundle:Default:capiTest.html.twig');


    }


    public function EliminarCotizacionesCosumoAction(Request $request) {


        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $cotizacion = $em->getRepository('BenchUsuariosBundle:Cotizaciones')->findOneBy(array('id' => $id));

        $em->remove($cotizacion);
        $em->flush();

        return new Response('200');
    }

    public function UpdateCotizacionesArchivoAction() {




        return new Response('100');
    }

    public function updateCotizacionesAction(Request $request) {

        

        $banco = $request->request->get('banco');
        $idCredito = $request->request->get('idCredito');
        $url = $request->request->get('url');
        $filename = $request->request->get('filename');
        $llave2 = $request->request->get('llave2');
        $consumoRechazo = $request->request->get('consumoRechazo');
        $


        $em = $this->getDoctrine()->getManager();


        $cotizacion = $em->getRepository('BenchUsuariosBundle:Cotizaciones')->findOneBy(array('id' => $idCredito));
        $cotizacion->setArchivollave($llave2);

        $cotizacion->setMotivoRechazo($consumoRechazo);
        $cotizacion->setBanco($banco);
        $cotizacion->setArchivonombreArchivo($filename);
        $cotizacion->setArchivourl($url);
        $cotizacion->setFecha(new \DateTime('now'));

        $em->persist($cotizacion);
        $em->flush();


        return new Response('200');
    }




    public function verDatosAction($banco,$id){


function calculaedad($fechanacimiento){
list($ano,$mes,$dia) = explode("-",$fechanacimiento);
$ano_diferencia = date("Y") - $ano;
$mes_diferencia = date("m") - $mes;
$dia_diferencia = date("d") - $dia;
if ($dia_diferencia < 0 && $mes_diferencia <= 0)
$ano_diferencia--;
return $ano_diferencia;
}


        $id = $id;
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));
        $antecedentes = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));
       
        $TotalPropiedadesApi =  $usuario->getTotalPropiedades();
        $TotalAutomovilesApi = $usuario->getTotalAutomoviles();
        $totalAhorroInversionApi = $usuario->getTotalAhorroInversion();
        $pagoMensualApi = $usuario->getPagomensualHipo();
        $pagoMensualCreditoApi = $usuario->getPagomensualCredito();
        $pagoMensualLineaCreditoApi = $usuario->getPagomensualLineaTarjeta();






        $creditoHipotecario = $em->getRepository('BenchCreditosBundle:CreditoHipotecario')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));
        $creditoConsumo = $em->getRepository('BenchCreditosBundle:CreditoConsumo')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));
        $creditoAutomotriz = $em->getRepository('BenchCreditosBundle:CreditoAutomotriz')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));
        if ( $creditoHipotecario) {



        }else{

            $creditoHipotecario = '';



        }



        $Conyuge = $em->getRepository('BenchUsuariosBundle:Conyuge')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));

        $tienesHipotecarioTotal = $em->getRepository('BenchTienesBundle:Bienesraices')->findBy(array('usuario' => $id));
        $tienesHiporecario = $em->getRepository('BenchTienesBundle:Bienesraices')->findBy(array('usuario' => $id));

        $cantidadHipotecario = 0;
        $totalHipotecario = 0;

        foreach ($tienesHipotecarioTotal as $tienesHipotecarioTotal):
            $total1 = $tienesHipotecarioTotal->getAvaluo();
            $totalHipotecario1 = str_replace(".", "", $total1);
            $totalHipotecario1 = str_replace(".", "", $total1);
            $totalHipotecario1 = str_replace(".", "", $total1);
            $totalHipotecario1 = str_replace(".", "", $total1);
            $totalHipotecario = $totalHipotecario1 + $totalHipotecario;
            $cantidadHipotecario =  $cantidadHipotecario + 1;


        endforeach;



/*  tienes  */

        if (isset($totalHipotecario)) {

        } else {

            $totalHipotecario = 0;
        }



        $tienesAutomovilesTotal = $em->getRepository('BenchTienesBundle:Automoviles')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $tienesAutomoviles = $em->getRepository('BenchTienesBundle:Automoviles')->findBy(array('usuario' => $id), array('id' => 'Asc'));




        $totalAutomoviles = 0;
        $cantidadAutomoviles = 0;


        foreach ($tienesAutomovilesTotal as $tienesAutomovilesTotal):
            $total3 = $tienesAutomovilesTotal->getValorcomercial();

            $totalAutomoviles1 = str_replace(".", "", $total3);
            $totalAutomoviles1 = str_replace(".", "", $total3);
            $totalAutomoviles1 = str_replace(".", "", $total3);
            $totalAutomoviles1 = str_replace(".", "", $total3);
            $totalAutomoviles = $totalAutomoviles1 + $totalAutomoviles;
            $cantidadAutomoviles  = $cantidadAutomoviles + 1;



        endforeach;

        if (isset($totalAutomoviles)) {

        } else {

            $totalAutomoviles = 0;
        }




        $totalAhorroInversionAcciones = 0;
        $cantidadAhorroInversion  = 0;






        $tienesAhorroInversionTotalAcciones = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id , 'tipo' => 'Acciones'), array('id' => 'Asc'));
        $cantidadSociedades = 0;

        foreach ($tienesAhorroInversionTotalAcciones as $tienesAhorroInversionTotalAcciones):
            $total2 = $tienesAhorroInversionTotalAcciones->getValorcomercial();
            $totalAhorroInversionAcciones1 = str_replace(".", "", $total2);
            $totalAhorroInversionAcciones1 = str_replace(".", "", $total2);
            $totalAhorroInversionAcciones1 = str_replace(".", "", $total2);
            $totalAhorroInversionAcciones1 = str_replace(".", "", $total2);
            $totalAhorroInversionAcciones = $totalAhorroInversionAcciones + $totalAhorroInversionAcciones1;
            $cantidadAhorroInversion = $cantidadAhorroInversion + 1;
            $cantidadSociedades =  $cantidadSociedades + 1;


        endforeach;

        if (isset($totalAhorroInversionAcciones)) {

        } else {

            $totalAhorroInversionAcciones = 0;
        }





        $totalAhorroInversionCuentas = 0;




        $tienesAhorroInversionTotalCuentas = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id , 'tipo' => 'Cuentas'), array('id' => 'Asc'));
        foreach ($tienesAhorroInversionTotalCuentas as $tienesAhorroInversionTotalCuentas):
            $total2 = $tienesAhorroInversionTotalCuentas->getValorcomercial();
            $totalAhorroInversionCuentas1 = str_replace(".", "", $total2);
            $totalAhorroInversionCuentas1 = str_replace(".", "", $total2);
            $totalAhorroInversionCuentas1 = str_replace(".", "", $total2);
            $totalAhorroInversionCuentas1 = str_replace(".", "", $total2);
            $totalAhorroInversionCuentas = $totalAhorroInversionCuentas + $totalAhorroInversionCuentas1;

        endforeach;

        if (isset($totalAhorroInversionCuentas)) {

        } else {

            $totalAhorroInversionCuentas = 0;
        }





        $tienesSociedadesTotal = $em->getRepository('BenchTienesBundle:Sociedades')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $tienesSociedades = $em->getRepository('BenchTienesBundle:Sociedades')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalS = 0;

        foreach ($tienesSociedadesTotal as $tienesSociedadesTotal):
            $total4 = $tienesSociedadesTotal->getValor();

            $totalSociedades = str_replace(".", "", $total4);
            $totalSociedades = str_replace(".", "", $total4);
            $totalSociedades = str_replace(".", "", $total4);
            $totalSociedades = str_replace(".", "", $total4);
            $totalSociedades = str_replace(".", "", $total4);
            $totalS =  $totalS  + $totalSociedades;


        endforeach;

        if (isset($totalS)) {

        } else {

            $totalS = 0;
        }






        $totalAhorroInversion = 0;




        $tienesAhorroInversionTotal = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $tienesAhorroInversion = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        foreach ($tienesAhorroInversionTotal as $tienesAhorroInversionTotal):
            $total2 = $tienesAhorroInversionTotal->getValorcomercial();
            $totalAhorroInversion1 = str_replace(".", "", $total2);
            $totalAhorroInversion1 = str_replace(".", "", $total2);
            $totalAhorroInversion1 = str_replace(".", "", $total2);
            $totalAhorroInversion1 = str_replace(".", "", $total2);
            $totalAhorroInversion1 = str_replace(".", "", $total2);
            $totalAhorroInversion = $totalAhorroInversion + $totalAhorroInversion1;

        endforeach;

        if (isset($totalAhorroInversion)) {

        } else {

            $totalAhorroInversion = 0;
        }



              $otrosTienes = $totalAhorroInversion + $totalS    -  $totalAhorroInversionCuentas  - $totalAhorroInversionAcciones;







        $totalActivos =   $totalAutomoviles +  $totalHipotecario  + $totalAhorroInversion + $totalS;



        







        /* Fin de tienes  */


       /* Inicio  debes */


        $debesHipotecarioTotal = $em->getRepository('BenchDebesBundle:Debeshipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $debesHipotecario =  $em->getRepository('BenchDebesBundle:Debeshipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $totalDebesHipotecario = 0;
        $cantidadDebesHipotecario = 0;
        $totalDebesHipotecarioMensual = 0;




        foreach ($debesHipotecarioTotal as $debesHipotecarioTotal):
            $total5 = $debesHipotecarioTotal->getDuedavigente();


            $totalDebesHipotecario1 = str_replace(".", "", $total5);
            $totalDebesHipotecario1 = str_replace(".", "", $total5);
            $totalDebesHipotecario1 = str_replace(".", "", $total5);
            $totalDebesHipotecario1 = str_replace(".", "", $total5);
            $totalDebesHipotecario1 = str_replace(".", "", $total5);
            $totalDebesHipotecario = $totalDebesHipotecario + $totalDebesHipotecario1;
            $cantidadDebesHipotecario = $cantidadDebesHipotecario + 1;


            $totalMensual = $debesHipotecarioTotal->getPagomensual();

            $totalDebesHipotecarioMensual1 = str_replace(".", "", $totalMensual);
            $totalDebesHipotecarioMensual1 = str_replace(".", "", $totalMensual);
            $totalDebesHipotecarioMensual1 = str_replace(".", "", $totalMensual);
            $totalDebesHipotecarioMensual1 = str_replace(".", "", $totalMensual);
            $totalDebesHipotecarioMensual =  $totalDebesHipotecarioMensual +  $totalDebesHipotecarioMensual1;


        endforeach;

        if (isset($totalDebesHipotecario)) {

        } else {

            $totalDebesHipotecario = 0;
        }
        if ($totalDebesHipotecarioMensual){

        }else{

            $totalDebesHipotecarioMensual = 0;

        }


            



        $debesCreditoConsumoTotal = $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $debesCreditoConsumo =  $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $totalDebesCredito = 0;
        $cantidadDebesConsumo = 0;
        $totalPagoMensual= 0;


        foreach ($debesCreditoConsumoTotal as $debesCreditoConsumoTotal):
            $total8 = $debesCreditoConsumoTotal->getDeudatotal();


            $totalDebesCredito1 = str_replace(".", "", $total8);
            $totalDebesCredito1 = str_replace(".", "", $total8);
            $totalDebesCredito1 = str_replace(".", "", $total8);
            $totalDebesCredito1 = str_replace(".", "", $total8);
            $totalDebesCredito = $totalDebesCredito + $totalDebesCredito1;

            $Mensual = $debesCreditoConsumoTotal->getPagomensual();

            $totalPagoMensual1 =  str_replace(".", "",    $Mensual );
            $totalPagoMensual1 =  str_replace(".", "",    $Mensual );
            $totalPagoMensual1 =  str_replace(".", "",    $Mensual );
            $totalPagoMensual1 =  str_replace(".", "",    $Mensual );

              $totalPagoMensual = $totalPagoMensual1  +     $totalPagoMensual ;





            $cantidadDebesConsumo = $cantidadDebesConsumo + 1;

        endforeach;


        if(isset($totalPagoMensual)){


        }else{

            $totalPagoMensual = 0;

        }

        if (isset($totalDebesCredito)) {

        } else {


            $totalDebesCredito = 0;
        }

        if(isset($totalPagoMensualCreditoConsumo)){


        }else{

            $totalPagoMensualCreditoConsumo = 0;

        }






        $debesTarjetaCreditoTotal = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $debesTarjetaCredito = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $totalDebesTarjeta = 0;
        $cantidadDebesTarjeta = 0;


        foreach ($debesTarjetaCreditoTotal as $debesTarjetaCreditoTotal):
            $total6 = $debesTarjetaCreditoTotal->getMontoutilizado();





            $totalDebesTarjeta1 = str_replace(".", "", $total6);
            $totalDebesTarjeta = $totalDebesTarjeta + $totalDebesTarjeta1;
            $cantidadDebesTarjeta = $cantidadDebesTarjeta + 1;
       
        endforeach;

        if (isset($totalDebesTarjeta)) {

        } else {

            $totalDebesTarjeta = 0;
        }






        $totalDebesLinea = 0;
        $cantidadLineaCredito = 0 ;



        $debesLineaCreditoTotal = $em->getRepository('BenchDebesBundle:DebesLineaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $debesLineaCredito =   $em->getRepository('BenchDebesBundle:DebesLineaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        
        foreach ($debesLineaCreditoTotal as $debesLineaCreditoTotal):
            $total7 = $debesLineaCreditoTotal->getMontoUtilizado();
            $totalDebesLinea1 = str_replace(".", "", $total7);
            $totalDebesLinea = $totalDebesLinea + $totalDebesLinea1;
            $cantidadLineaCredito = $cantidadLineaCredito + 1;




        endforeach;

        if (isset($totalDebesLinea)) {

        } else {

            $totalDebesLinea = 0;
        }





        $totalDebes = $totalDebesCredito + $totalDebesLinea + $totalDebesTarjeta + $totalDebesHipotecario;







/*
         return $this->render('BenchAdminBundle:plantillas:'.$banco.'.html.twig' , array('usuario' =>  $usuario ,
         'antecedentes' =>   $antecedentes , 'totalHipotecario' => $totalHipotecario , 'totalAutomoviles' => $totalAutomoviles  ,'totalAhorroInversionAcciones' => $totalAhorroInversionAcciones,
         'totalAhorroInversionCuentas' => $totalAhorroInversionCuentas , 'totalDebesHipotecario' => $totalDebesHipotecario , 'totalDebesCredito' => $totalDebesCredito ,
         'totalDebesTarjeta' => $totalDebesTarjeta , 'totalDebesLinea' => $totalDebesLinea ,
         'otrosTienes' => $otrosTienes , 'totalActivos' => $totalActivos , 'totalDebes' => $totalDebes , 'creditoHipotecario' => $creditoHipotecario




         ));


*/  
     if($antecedentes){

     if($antecedentes->getFechaingreso()){
        $tiempoTrabajo =  $antecedentes->getFechaingreso();
         $var = (explode('/',   $tiempoTrabajo , 10));
         $dia =  $var[0];
         $mes = $var[1];
         $ano = $var[2];

         $tiempoTrabajo = calculaedad (''.$ano.'-'.$mes.'-'.$dia.'');


     }else{

        $tiempoTrabajo = '';



     }


   }else{

 $tiempoTrabajo = '';
    
   }

     if($usuario->getFechanacimiento()) {  
     $edad = $usuario->getFechanacimiento();
     $var = (explode('/',  $edad , 10));
     $dia =  $var[0];
     $mes = $var[1];
     $ano = $var[2];
     $edadUsuario = calculaedad (''.$ano.'-'.$mes.'-'.$dia.'');

     }else{

    $edadUsuario  = '';

     }



     if($Conyuge){
     if($Conyuge->getFechanacimiento()){

     $edadCon = $Conyuge->getFechanacimiento();
     $var = (explode('/',  $edadCon , 10));
     $dia =  $var[0];
     $mes = $var[1];
     $ano = $var[2];
     $edadCon  = calculaedad (''.$ano.'-'.$mes.'-'.$dia.'');

     }else{

     $edadCon = '';
     



     }

 }else{

           $edadCon = '';


 }


 $creditoConsolidacion = $em->getRepository('BenchCreditosBundle:CreditoConsolidacion')->findBy(array('usuario' => $id), array('id' => 'Desc'));
        $html = $this->renderView('BenchAdminBundle:plantillas:'.$banco.'.html.twig' , array('usuario' =>  $usuario ,
            'antecedentes' =>   $antecedentes , 'totalHipotecario' => $totalHipotecario , 'totalAutomoviles' => $totalAutomoviles  ,'totalAhorroInversionAcciones' => $totalAhorroInversionAcciones,
            'totalAhorroInversionCuentas' => $totalAhorroInversionCuentas , 'totalDebesHipotecario' => $totalDebesHipotecario , 'totalDebesCredito' => $totalDebesCredito ,
            'totalDebesTarjeta' => $totalDebesTarjeta , 'totalDebesLinea' => $totalDebesLinea , 'otrosTienes' => $otrosTienes ,
            'totalActivos' => $totalActivos , 'totalDebes' => $totalDebes , 'creditoHipotecario' => $creditoHipotecario , 'Conyuge' => $Conyuge , 'tienesAhorroInversion' => $tienesAhorroInversion,
            'totalAhorroInversion' => $totalAhorroInversion , 'tienesAutomoviles' => $tienesAutomoviles , 'tienesHiporecario' => $tienesHiporecario , 'tienesSociedades' => $tienesSociedades ,
            'totalSociedades' => $totalS , 'debesLineaCredito' => $debesLineaCredito , 'debesTarjetaCredito' => $debesTarjetaCredito , 'debesHipotecario' => $debesHipotecario
            , 'debesCreditoConsumo' => $debesCreditoConsumo , 'cantidadHipotecario' => $cantidadHipotecario , 'cantidadAutomoviles' => $cantidadAutomoviles ,
             'cantidadAhorroInversion' => $cantidadAhorroInversion , 'cantidadSociedades' => $cantidadSociedades , 'totalPagoMensualCreditoConsumo' => $totalPagoMensualCreditoConsumo ,
             'cantidadDebesConsumo' => $cantidadDebesConsumo , 'totalPagoMensual' => $totalPagoMensual , 'cantidadDebesTarjeta' => $cantidadDebesTarjeta , 'cantidadLineaCredito' => $cantidadLineaCredito,
            'cantidadDebesHipotecario' => $cantidadDebesHipotecario , 'creditoConsumo' => $creditoConsumo , 'creditoAutomotriz' => $creditoAutomotriz
            , 'totalDebesHipotecarioMensual' => $totalDebesHipotecarioMensual , 'totalPagoMensual' => $totalPagoMensual,
            'TotalPropiedadesApi' => $TotalPropiedadesApi , 'TotalAutomovilesApi' => $TotalAutomovilesApi, 'totalAhorroInversionApi' => $totalAhorroInversionApi, 
            'pagoMensualApi' => $pagoMensualApi , 'pagoMensualCreditoApi' => $pagoMensualCreditoApi
            , 'pagoMensualLineaCreditoApi' => $pagoMensualLineaCreditoApi , 'edadUsuario' => $edadUsuario , 'tiempoTrabajo' => $tiempoTrabajo ,

            'edadCon' => $edadCon , 'creditoConsolidacion' => $creditoConsolidacion







        ));








        $pdfGenerator = $this->get('spraed.pdf.generator');

     
        return new Response($pdfGenerator->generatePDF($html), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' .'Eess-'.$banco.'-'.$usuario->getNombre().'-'.$usuario->getApellido().'.pdf"'
        ));





        //

    }
    public function GuardarCotizacionesAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $Cotizar = new Cotizaciones();

        $idUsuario = $request->request->get('idUsuario');
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $idUsuario));
        $consumoMonto = $request->request->get('Monto');
        $consumoPlazo = $request->request->get('Plazo');
        $idCredito = $request->request->get('idCredito');
        $tipo = $request->request->get('tipo');


        $Cotizar->setMonto($consumoMonto);
        $Cotizar->setPlazo($consumoPlazo);
        $Cotizar->setTipo($tipo);
        $Cotizar->setUsuario($usuario);
        $Cotizar->setCreditoid($idCredito);
        $em->persist($usuario);
        $em->persist($Cotizar);
        $em->flush();



        switch ($tipo) {
            case 'Crédito Consumo':
             
                
              $CreditoConsumo = new CreditoConsumo();
              $CreditoConsumo = $em->getRepository('BenchCreditosBundle:CreditoConsumo')->findOneBy(array('id' => $idCredito));
              $CreditoConsumo->setEstado('Cotizado');
              $em->persist($CreditoConsumo);
              $em->flush();
              break;
            
             case 'Crédito Hipotecario':
                 
       
             $creditoHipotecario = $em->getRepository('BenchCreditosBundle:CreditoHipotecario')->findOneBy(array('id' => $idCredito));    
             $creditoHipotecario->setEstado('Cotizado');
             $em->persist($creditoHipotecario);


             $em->flush();
             
                
             break;
            
            
            case 'Crédito Consolidacion Deuda':
                

             $creditoCon = $em->getRepository('BenchCreditosBundle:CreditoConsolidacion')->findOneBy(array('id' => $idCredito));    
             $creditoCon->setEstado('Cotizado');
             $em->persist($creditoCon);
             $em->flush();
          
                break;
            
            
            case 'Cuenta Corriente':
                
                
             $creditoCuenta = new CreditoCuenta();
             $creditoCuenta = $em->getRepository('BenchCreditosBundle:CreditoCuenta')->findOneBy(array('id' => $idCredito));    
             $creditoCuenta->setEstado('Cotizado');
             
             $em->persist($creditoCuenta);
             
             $em->flush();
          
             
                
                break;
            
            
            case  'Crédito Automotriz':
                
              
                $creditoAutomotriz = $em->getRepository('BenchCreditosBundle:CreditoAutomotriz')->findOneBy(array('id' => $idCredito));  
                $creditoAutomotriz->setEstado('Cotizado');
                $em->persist($creditoAutomotriz);
                $em->flush();
                
            break;
            
        }





        $id = $Cotizar->getId();

        return new Response($id);
    }

    public function cotizarAction($id) {

        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));

        return $this->render('BenchAdminBundle:Default:cotizar.html.twig', array('usuario' => $usuario));
    }

    public function otrosUploadAction($id) {

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));
 
        
     
        $archivos1 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id, 'tipo' => 'Otros'), array('id' => 'Asc'));

        return $this->render('BenchAdminBundle:Default:uploadArchivos.html.twig', array('archivos1' => $archivos1, 'usuario' => $usuario , 'id' => $id));
    }

    public function updateArchivos2Action() {



        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findAll();

        while (list($key, $value) = each($usuario)) {
            set_time_limit(1000000000);


            $archivo = $value->getArchivos();

            if ($archivo) {



                $value->setTienearchivo('SI');
                $em->persist($value);
                $em->flush();
            }

            return new Response('listooo');
        }
    }

    public function updateArchivosAction() {


        $em = $this->getDoctrine()->getManager();


        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findAll();

        while (list($key, $value) = each($usuario)) {
            set_time_limit(1000000000);
            $rut = $value->getRut();
            $ruta = '../web/documentos/' . $rut . '/';



            if (file_exists($ruta)) {


                $value->setTienearchivo('SI');
                $em->persist($value);
                $em->flush();
            }
        }



        return new Response(0);
    }

    /// Usuarios Archivados 


    private function _datatable2() {



        return $this->get('datatable')
                        ->setEntity("BenchUsuariosBundle:Usuario", "x")                          // replace "XXXMyBundle:Entity" by your entity
                        ->setFields(
                                array(
                                    "Fecha de Actualización" => 'x.fechaactualizacionFinal',
                                    "Rut" => 'x.rut',
                                    "Nombre" => 'x.nombre',
                                    "Apellido" => 'x.apellido',
                                    "Email" => 'x.email',
                                    "Telefono" => 'x.telefono',
                                    
                                    "Archivos Adjuntos" => 'x.tienearchivo',
                                    "Dicom Adjuntos" => 'x.dicom',
                                    "_identifier_" => 'x.id')
                        )
                        ->setWhere(
                                'x.archivar = :archivar', array('archivar' => 'SI')
                        )
                       
                        ->setHasAction(true);
    }



// todos los usuarios


    private function _datatable() {


        return $this->get('datatable')
                        ->setEntity("BenchUsuariosBundle:Usuario", "x")
                        ->setFields(
                                array(
                                    "id" => 'x.id',
                                    "Fecha de Actualización" => 'x.fechaactualizacionFinal',
                                    "Rut" => 'x.rut',
                                    "Nombre" => 'x.nombre',
                                    "Apellido" => 'x.apellido',
                                    "Email" => 'x.email',
                                    "Telefono" => 'x.telefono',
                                    "Archivos Adjuntos" => 'x.tienearchivo',
                                    "Dicom Adjuntos" => 'x.dicom',
                               
                                    "_identifier_" => 'x.id')
                        )
                       

                        ->setHasAction(true)
                        ->setOrder("x.id", "ASC")

                        ;
    }

    // usuario con pedidos boton final

    private function _datatable3() {



        return $this->get('datatable')
                        ->setEntity("BenchUsuariosBundle:Usuario", "x")                          // replace "XXXMyBundle:Entity" by your entity
                        ->setFields(
                                array(
                                    "Fecha de Actualización" => 'x.fechaactualizacionFinal',
                                    "Rut" => 'x.rut',
                                     "Nombre" => 'x.nombre',
                                    "Apellido" => 'x.apellido',
                                    "Email" => 'x.email',
                                    "Telefono" => 'x.telefono',
                                    "Archivos Adjuntos" => 'x.tienearchivo',
                                    "Dicom Adjuntos" => 'x.dicom',
                               
                                    "_identifier_" => 'x.id')
                        )
                        ->setWhere(
                                'x.final = :final and x.archivar = :archivar', array('final' => 'SI', 'archivar' => 'no')
                        )
                        ->setOrder("x.id", "ASC")
                        ->setHasAction(true);
    }

    private function _datatable4() {

        return $this->get('datatable')
                        ->setEntity("BenchUsuariosBundle:Usuario", "x")
                        ->setFields(
                                array(
                                    "Fecha de Actualización" => 'x.fechaactualizacionFinal',
                                    "Rut" => 'x.rut',
                                    "Nombre" => 'x.nombre',
                                    "Apellido" => 'x.apellido',
                                    "Email" => 'x.email',
                                    "Telefono" => 'x.telefono',
                                    "Archivos" => 'x.tienearchivo',
                                  "Archivos Ad" => 'x.tienearchivo',
                                    "Dicom Ad" => 'x.dicom',
                                    "_identifier_" => 'x.id')
                        )
                        ->setWhere('x.final != :final', array('final' => 'null')
                        )
                        ->setOrder("x.id", "ASC")
                        ->setHasAction(true);
    }

    public function gridAction() {
        return $this->_datatable()->setSearch(TRUE)->execute();
    }

    public function grid2Action() {

        return $this->_datatable2()->setSearch(TRUE)->execute();
    }

    public function grid3Action() {

        return $this->_datatable3()->setSearch(TRUE)->execute();
    }

    public function grid4Action() {

        return $this->_datatable4()->setSearch(TRUE)->execute();
    }

    public function vistaSoloRegistradosAction() {
        $session = $this->getRequest()->getSession();
        $nombre = $session->get('nombre');
        $contrasena = $session->get('contrasena');
        $em = $this->getDoctrine()->getManager();

        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));

        if ($admin) {

            $this->_datatable4();

            return $this->render('BenchAdminBundle:Default:SoloRegistrados.html.twig');
        } else {

            return new Response('Acceso no autorizado');
        }
    }

    public function vistaArchivadosAction() {
        $session = $this->getRequest()->getSession();
        $nombre = $session->get('nombre');
        $contrasena = $session->get('contrasena');
        $em = $this->getDoctrine()->getManager();

        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));

        if ($admin) {
            $this->_datatable2();

            return $this->render('BenchAdminBundle:Default:vistaArchivados.html.twig');
        } else {

            return new Response('Acceso no autorizado');
        }
    }

    public function Up2Action($id) {

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));

        return $this->render('BenchAdminBundle:Default:uploadDico.html.twig', array('archivos1' => $archivos1, 'usuario' => $usuario , 'id' => $id));
        
    }

    public function UpDicomAction($id) {


        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));
        $rut = $usuario->getRut();
        
        $session = $this->getRequest()->getSession();
        
        $session->set('rut2', $rut);
        
        
        $archivos1 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id, 'tipo' => 'Dicom'), array('id' => 'Asc'));

        
        return $this->render('BenchAdminBundle:Default:uploadDico.html.twig', array('archivos1' => $archivos1, 'usuario' => $usuario , 'id' => $id));
    }

    public function usuarioPedidoAction() {


        $session = $this->getRequest()->getSession();
        $nombre = $session->get('nombre');
        $contrasena = $session->get('contrasena');
        $em = $this->getDoctrine()->getManager();

        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));

        if ($admin) {
            $this->_datatable3();

            return $this->render('BenchAdminBundle:Default:vistaConpedidos.html.twig');
        } else {


            return new Response('Acceso no autorizado');
        }
    }

// todos 
    public function index2Action() {


        $session = $this->getRequest()->getSession();
        $nombre = $session->get('nombre');
        $contrasena = $session->get('contrasena');
        $em = $this->getDoctrine()->getManager();
        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));


        if ($admin) {

            $this->_datatable();

            return $this->render('BenchAdminBundle:Default:prueba.html.twig.twig');
        } else {
            
        }
    }

    /**
     * Lists all entities.
     * @return Response
     */
    public function archivarUsuario2Action() {

        return new Response(100);
    }

    public function crearpdf2Action($id) {



        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));
        $archivos1 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id));
        $cont = 0;








        $pdfGenerator = $this->get('knp_snappy.pdf');


        return new Response($this->get('knp_snappy.pdf')->generate('http://www.theclick.cl/prueba.html', 'yo'));










        //$container->get('knp_snappy.pdf')->generate('http://www.google.cl', '/path/to/the/file.pdf');



        /*
          while (list($key, $value) = each($archivos1)) {

          $cont++;
          set_time_limit(1000000000) ;

          if(($archivos1)){

          $cuentaSiPdf = substr_count($value->getNombreArchivo(), '.pdf');


          if($cuentaSiPdf > 0){

          $pdf[$cont] =$value->getUrl();

          }

          }



          }


          $htmlCollection = array();


          $pdfGenerator = $this->get('spraed.pdf.generator');
          return new Response($pdfGenerator->generatePDFs($pdf, 'UTF-8'),
          200,
          array(
          'Content-Type' => 'application/pdf',
          'Content-Disposition' => 'inline; filename="out.pdf"'
          )
          );

         * 
         * 
         */
    }

    public function CrearPdfAction($id) {




        $name = 'prueba';


        $em = $this->getDoctrine()->getManager();

        $session = $this->getRequest()->getSession();



        if ($id) {



            $session->start();
            $session->set('id', $id);
        } else {


            $session->start();

            $id = $session->get('id');
        }

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));
        $antecedentes = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));

        $conyuge = $em->getRepository('BenchUsuariosBundle:Conyuge')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));



        if ($conyuge) {
            
        } else {

            $conyuge = new Conyuge();
        }





        $tienesHipotecarioTotal = $em->getRepository('BenchTienesBundle:Bienesraices')->findBy(array('usuario' => $id));




        $totalHipotecario = 0;

        foreach ($tienesHipotecarioTotal as $tienesHipotecarioTotal):
            $total1 = $tienesHipotecarioTotal->getAvaluo();
            $totalHipotecario1 = str_replace(".", "", $total1);
            $totalHipotecario = $totalHipotecario1 + $totalHipotecario;

        endforeach;





        if (isset($totalHipotecario)) {
            
        } else {

            $totalHipotecario = 0;
        }




        $totalAhorroInversion = 0;




        $tienesAhorroInversionTotal = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        $tienesAhorroInversion =  $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        foreach ($tienesAhorroInversionTotal as $tienesAhorroInversionTotal):
            $total2 = $tienesAhorroInversionTotal->getValorcomercial();
            $totalAhorroInversion1 = str_replace(".", "", $total2);
            $totalAhorroInversion = $totalAhorroInversion + $totalAhorroInversion1;

        endforeach;

        if (isset($totalAhorroInversion)) {
            
        } else {

            $totalAhorroInversion = 0;
        }





        $tienesAutomovilesTotal = $em->getRepository('BenchTienesBundle:Automoviles')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalAutomoviles = 0;

        foreach ($tienesAutomovilesTotal as $tienesAutomovilesTotal):
            $total3 = $tienesAutomovilesTotal->getValorcomercial();

            $totalAutomoviles1 = str_replace(".", "", $total3);
            $totalAutomoviles = $totalAutomoviles1 + $totalAutomoviles;

        endforeach;

        if (isset($totalAutomoviles)) {
            
        } else {

            $totalAutomoviles = 0;
        }





        $tienesSociedadesTotal = $em->getRepository('BenchTienesBundle:Sociedades')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        $totalSociedades = 0;

        foreach ($tienesSociedadesTotal as $tienesSociedadesTotal):
            $total4 = $tienesSociedadesTotal->getValor();

            $totalSociedades = str_replace(".", "", $total4);
            $totalSociedades = $totalSociedades + $totalSociedades;


        endforeach;

        if (isset($totalSociedades)) {
            
        } else {

            $totalSociedades = 0;
        }


        $activos = $totalAutomoviles + $totalSociedades + $totalAhorroInversion + $totalHipotecario;




        $debesHipotecarioTotal = $em->getRepository('BenchDebesBundle:Debeshipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalDebesHipotecario = 0;


        foreach ($debesHipotecarioTotal as $debesHipotecarioTotal):
            $total5 = $debesHipotecarioTotal->getDuedavigente();


            $totalDebesHipotecario1 = str_replace(".", "", $total5);
            $totalDebesHipotecario = $totalDebesHipotecario + $totalDebesHipotecario1;

        endforeach;

        if (isset($totalDebesHipotecario)) {
            
        } else {

            $totalDebesHipotecario = 0;
        }



        $totalDebesLinea = 0;

        $debesLineaCreditoTotal = $em->getRepository('BenchDebesBundle:DebesLineaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        foreach ($debesLineaCreditoTotal as $debesLineaCreditoTotal):
            $total7 = $debesLineaCreditoTotal->getMontoUtilizado();
            $totalDebesLinea1 = str_replace(".", "", $total7);
            $totalDebesLinea = $totalDebesLinea + $totalDebesLinea1;

        endforeach;

        if (isset($totalDebesLinea)) {
            
        } else {

            $totalDebesLinea = 0;
        }





        $debesTarjetaCreditoTotal = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalDebesTarjeta = 0;


        foreach ($debesTarjetaCreditoTotal as $debesTarjetaCreditoTotal):
            $total6 = $debesTarjetaCreditoTotal->getMontoutilizado();



            $totalDebesTarjeta1 = str_replace(".", "", $total6);
            $totalDebesTarjeta = $totalDebesTarjeta + $totalDebesTarjeta1;

        endforeach;

        if (isset($totalDebesTarjeta)) {
            
        } else {

            $totalDebesTarjeta = 0;
        }



        $debesCreditoConsumoTotal = $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalDebesCredito = 0;

        foreach ($debesCreditoConsumoTotal as $debesCreditoConsumoTotal):
            $total8 = $debesCreditoConsumoTotal->getDeudatotal();


            $totalDebesCredito1 = str_replace(".", "", $total8);
            $totalDebesCredito = $totalDebesCredito + $totalDebesCredito1;

        endforeach;

        if (isset($totalDebesCredito)) {
            
        } else {


            $totalDebesCredito = 0;
        }



        $totalDebes = $totalDebesCredito + $totalDebesLinea + $totalDebesTarjeta + $totalDebesHipotecario;

        $patrimonio = $activos - $totalDebes;

        $tienesHipotecario = $em->getRepository('BenchTienesBundle:Bienesraices')->findBy(array('usuario' => $id));

        if ($tienesHipotecario) {
            
        } else {

            $tienesHipotecario = new Bienesraices();
        }

        $tienesAhorroInversion = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        if ($tienesAhorroInversion) {
            
        } else {

            $tienesAhorroInversion = new Ahorroinverson();
        }



        $tienesAutomoviles = $em->getRepository('BenchTienesBundle:Automoviles')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        if ($tienesAutomoviles) {
            
        } else {


            $tienesAutomoviles = new Automoviles();
        }



        $tienesSociedades = $em->getRepository('BenchTienesBundle:Sociedades')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        if ($tienesSociedades) {
            
        } else {


            $tienesSociedades = new Sociedades();
        }
        // Debes 



        $debesHipotecario = $em->getRepository('BenchDebesBundle:Debeshipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        if ($debesHipotecario) {
            
        } else {

            $debesHipotecario = new DebesCreditoConsumo();
        }




        $debesTarjetaCredito = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        $debesLineaCredito = $em->getRepository('BenchDebesBundle:DebesLineaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        if ($debesLineaCredito) {
            
        } else {

            $debesLineaCredito = new DebesLineaCredito();
        }

        $debesCreditoConsumo = $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));



        // Creditos 



        $creditoConsumo = $em->getRepository('BenchCreditosBundle:CreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        if ($creditoConsumo) {
            
        } else {


            $creditoConsumo = new CreditoConsumo();
        }


        $creditoAutomotriz = $em->getRepository('BenchCreditosBundle:CreditoAutomotriz')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        if ($creditoAutomotriz) {
            
        } else {

            $creditoAutomotriz = new CreditoAutomotriz();
        }


        $creditoHipotecario = $em->getRepository('BenchCreditosBundle:CreditoHipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        if ($creditoHipotecario) {
            
        } else {

            $creditoHipotecario = new CreditoHipotecario();
        }

        $creditoConsolidacion = $em->getRepository('BenchCreditosBundle:CreditoConsolidacion')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        if ($creditoConsolidacion) {
            
        } else {

            $creditoConsolidacion = new CreditoConsolidacion();
        }

        $creditoCuenta = $em->getRepository('BenchCreditosBundle:CreditoCuenta')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        if ($creditoCuenta) {
            
        } else {

            $creditoCuenta = new creditoCuenta();
        }



        $rut = $usuario->getRut();

  $filepickerArchivo = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id), array('id' => 'Asc'));


$cont = 0;


if(!function_exists('el_crypto_hmacSHA1')){

  function el_crypto_hmacSHA1($key, $data, $blocksize = 64) {
        if (strlen($key) > $blocksize) $key = pack('H*', sha1($key));
        $key = str_pad($key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack( 'H*', sha1(
        ($key ^ $opad) . pack( 'H*', sha1(
          ($key ^ $ipad) . $data
        ))
      ));
        return base64_encode($hmac);
    }
  }

  if(!function_exists('el_s3_getTemporaryLink')){
  
    
  function el_s3_getTemporaryLink($accessKey, $secretKey, $bucket, $path, $expires = 336) {

      $expires = time() + intval(floatval($expires) * 60);
   
      $path = str_replace('%2F', '/', rawurlencode($path = ltrim($path, '/')));
      
      $signpath = '/'. $bucket .'/'. $path;

      $signsz = implode("\n", $pieces = array('GET', null, null, $expires, $signpath));
   
      $signature = el_crypto_hmacSHA1($secretKey, $signsz);
      
      $url = sprintf('https://%s.s3.amazonaws.com/%s', $bucket, $path);
     
      $qs = http_build_query($pieces = array(
        'AWSAccessKeyId' => $accessKey,
        'Expires' => $expires,
        'Signature' => $signature,
      ));
     
      return $url.'?'.$qs;
    }
  }


  
if( $filepickerArchivo){
foreach ( $filepickerArchivo as  $filepickerArchivo ) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion',  $filepickerArchivo->getLlave().'_'. $filepickerArchivo->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' =>  $filepickerArchivo->getId() , 'llave' =>  $filepickerArchivo->getLlave() , 'nombreArchivo' =>  $filepickerArchivo->getnombreArchivo() , 'tipoArchivo' => $filepickerArchivo->getTipo());

    # code...
}

 $filepickerArchivo =  $myArray;
}








        $html = $this->renderView('BenchAdminBundle:Default:form2.html.twig', array('name' => $name, 'usuario' => $usuario, 'antecedentes' => $antecedentes, 'conyuge' => $conyuge, 'totalHipotecario' => $totalHipotecario, 'totalAhorroInversion' => $totalAhorroInversion, 'totalAutomoviles' => $totalAutomoviles, 'totalSociedades' => $totalSociedades, 'activos' => $activos, 'totalDebesHipotecario' => $totalDebesHipotecario, 'totalDebesLinea' => $totalDebesLinea, 'totalDebesTarjeta' => $totalDebesTarjeta, 'totalDebesCredito' => $totalDebesCredito, 'totalDebes' => $totalDebes, 'patrimonio' => $patrimonio, 'tienesHipotecario' => $tienesHipotecario, 'tienesHipotecario' => $tienesHipotecario, 'tienesAhorroInversion' => $tienesAhorroInversion, 'tienesAutomoviles' => $tienesAutomoviles, 'tienesSociedades' => $tienesSociedades, 'debesHipotecario' => $debesHipotecario, 'debesTarjetaCredito' => $debesTarjetaCredito, 'debesLineaCredito' => $debesLineaCredito, 'debesCreditoConsumo' => $debesCreditoConsumo, 'creditoConsumo' => $creditoConsumo, 'creditoAutomotriz' => $creditoAutomotriz, 'creditoHipotecario' => $creditoHipotecario, 'creditoConsolidacion' => $creditoConsolidacion, 'creditoCuenta' => $creditoCuenta , 'filepickerArchivo' => $filepickerArchivo));



        $pdfGenerator = $this->get('spraed.pdf.generator');

        return new Response($pdfGenerator->generatePDF($html), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $usuario->getNombre() . '.pdf"'
        ));
    }

    public function verAction($id) {


        $request = $this->getRequest();



        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();
        if ($id) {




            $session->start();
            $session->set('id', $id);
        } else {


            $session->start();

            $id = $session->get('id');
        }

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));





        $tienesHipotecarioTotal = $em->getRepository('BenchTienesBundle:Bienesraices')->findBy(array('usuario' => $id));


        $totalHipotecario = 0;

        foreach ($tienesHipotecarioTotal as $tienesHipotecarioTotal):
            $total1 = $tienesHipotecarioTotal->getAvaluo();
            $totalHipotecario1 = str_replace(".", "", $total1);
            $totalHipotecario = $totalHipotecario1 + $totalHipotecario;

        endforeach;

        if (isset($totalHipotecario)) {
            
        } else {

            $totalHipotecario = 0;
        }


        $totalAhorroInversion = 0;




        $tienesAhorroInversionTotal = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));
        foreach ($tienesAhorroInversionTotal as $tienesAhorroInversionTotal):
            $total2 = $tienesAhorroInversionTotal->getValorcomercial();
            $totalAhorroInversion1 = str_replace(".", "", $total2);
            $totalAhorroInversion = $totalAhorroInversion + $totalAhorroInversion1;

        endforeach;

        if (isset($totalAhorroInversion)) {
            
        } else {

            $totalAhorroInversion = 0;
        }




        $tienesAutomovilesTotal = $em->getRepository('BenchTienesBundle:Automoviles')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalAutomoviles = 0;

        foreach ($tienesAutomovilesTotal as $tienesAutomovilesTotal):
            $total3 = $tienesAutomovilesTotal->getValorcomercial();

            $totalAutomoviles1 = str_replace(".", "", $total3);
            $totalAutomoviles = $totalAutomoviles1 + $totalAutomoviles;

        endforeach;

        if (isset($totalAutomoviles)) {
            
        } else {

            $totalAutomoviles = 0;
        }



        $tienesSociedadesTotal = $em->getRepository('BenchTienesBundle:Sociedades')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        $totalSociedades = 0;

        foreach ($tienesSociedadesTotal as $tienesSociedadesTotal):
            $total4 = $tienesSociedadesTotal->getValor();

            $totalSociedades = str_replace(".", "", $total4);
            $totalSociedades = $totalSociedades + $totalSociedades;


        endforeach;

        if (isset($totalSociedades)) {
            
        } else {

            $totalSociedades = 0;
        }


        $activos = $totalAutomoviles + $totalSociedades + $totalAhorroInversion + $totalHipotecario;



        $debesHipotecarioTotal = $em->getRepository('BenchDebesBundle:Debeshipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalDebesHipotecario = 0;


        foreach ($debesHipotecarioTotal as $debesHipotecarioTotal):
            $total5 = $debesHipotecarioTotal->getDuedavigente();


            $totalDebesHipotecario1 = str_replace(".", "", $total5);
            $totalDebesHipotecario = $totalDebesHipotecario + $totalDebesHipotecario1;

        endforeach;

        if (isset($totalDebesHipotecario)) {
            
        } else {

            $totalDebesHipotecario = 0;
        }


        $debesTarjetaCreditoTotal = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalDebesTarjeta = 0;


        foreach ($debesTarjetaCreditoTotal as $debesTarjetaCreditoTotal):
            $total6 = $debesTarjetaCreditoTotal->getMontoutilizado();



            $totalDebesTarjeta1 = str_replace(".", "", $total6);
            $totalDebesTarjeta = $totalDebesTarjeta + $totalDebesTarjeta1;

        endforeach;

        if (isset($totalDebesTarjeta)) {
            
        } else {

            $totalDebesTarjeta = 0;
        }





        $totalDebesLinea = 0;

        $debesLineaCreditoTotal = $em->getRepository('BenchDebesBundle:DebesLineaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        foreach ($debesLineaCreditoTotal as $debesLineaCreditoTotal):
            $total7 = $debesLineaCreditoTotal->getMontoUtilizado();
            $totalDebesLinea1 = str_replace(".", "", $total7);
            $totalDebesLinea = $totalDebesLinea + $totalDebesLinea1;

        endforeach;

        if (isset($totalDebesLinea)) {
            
        } else {

            $totalDebesLinea = 0;
        }



        $debesCreditoConsumoTotal = $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $totalDebesCredito = 0;

        foreach ($debesCreditoConsumoTotal as $debesCreditoConsumoTotal):
            $total8 = $debesCreditoConsumoTotal->getDeudatotal();


            $totalDebesCredito1 = str_replace(".", "", $total8);
            $totalDebesCredito = $totalDebesCredito + $totalDebesCredito1;

        endforeach;

        if (isset($totalDebesCredito)) {
            
        } else {


            $totalDebesCredito = 0;
        }



        $totalDebes = $totalDebesCredito + $totalDebesLinea + $totalDebesTarjeta + $totalDebesHipotecario;

        $antecedentes = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));

        $conyuge = $em->getRepository('BenchUsuariosBundle:Conyuge')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));


        $tienesHipotecario = $em->getRepository('BenchTienesBundle:Bienesraices')->findBy(array('usuario' => $id));

        $tienesAhorroInversion = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $tienesAutomoviles = $em->getRepository('BenchTienesBundle:Automoviles')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        $tienesSociedades = $em->getRepository('BenchTienesBundle:Sociedades')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        $debesHipotecario = $em->getRepository('BenchDebesBundle:Debeshipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));


        $debesTarjetaCredito = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $debesLineaCredito = $em->getRepository('BenchDebesBundle:DebesLineaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $debesCreditoConsumo = $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));



        $creditoConsumo = $em->getRepository('BenchCreditosBundle:CreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $dondeCreditoConsumo = $em->getRepository('BenchSolicitudesCreditosBundle:SolicitudesCreditos')->findBy(array('usuario' => $id, 'tipo' => 'CreditoConsumo'));






        $creditoAutomotriz = $em->getRepository('BenchCreditosBundle:CreditoAutomotriz')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $creditoHipotecario = $em->getRepository('BenchCreditosBundle:CreditoHipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $creditoConsolidacion = $em->getRepository('BenchCreditosBundle:CreditoConsolidacion')->findBy(array('usuario' => $id), array('id' => 'Asc'));

        $creditoCuenta = $em->getRepository('BenchCreditosBundle:CreditoCuenta')->findBy(array('usuario' => $id), array('id' => 'Asc'));



        $filepickerArchivo = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id), array('id' => 'Asc'));


$cont = 0;


if(!function_exists('el_crypto_hmacSHA1')){

  function el_crypto_hmacSHA1($key, $data, $blocksize = 64) {
        if (strlen($key) > $blocksize) $key = pack('H*', sha1($key));
        $key = str_pad($key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack( 'H*', sha1(
        ($key ^ $opad) . pack( 'H*', sha1(
          ($key ^ $ipad) . $data
        ))
      ));
        return base64_encode($hmac);
    }
  }

  if(!function_exists('el_s3_getTemporaryLink')){
  
    
  function el_s3_getTemporaryLink($accessKey, $secretKey, $bucket, $path, $expires = 336) {

      $expires = time() + intval(floatval($expires) * 60);
   
      $path = str_replace('%2F', '/', rawurlencode($path = ltrim($path, '/')));
      
      $signpath = '/'. $bucket .'/'. $path;

      $signsz = implode("\n", $pieces = array('GET', null, null, $expires, $signpath));
   
      $signature = el_crypto_hmacSHA1($secretKey, $signsz);
      
      $url = sprintf('https://%s.s3.amazonaws.com/%s', $bucket, $path);
     
      $qs = http_build_query($pieces = array(
        'AWSAccessKeyId' => $accessKey,
        'Expires' => $expires,
        'Signature' => $signature,
      ));
     
      return $url.'?'.$qs;
    }
  }


  
if( $filepickerArchivo){
foreach ( $filepickerArchivo as  $filepickerArchivo ) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion',  $filepickerArchivo->getLlave().'_'. $filepickerArchivo->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' =>  $filepickerArchivo->getId() , 'llave' =>  $filepickerArchivo->getLlave() , 'nombreArchivo' =>  $filepickerArchivo->getnombreArchivo());

    # code...
}

 $filepickerArchivo =  $myArray;
}

      


        return $this->render('BenchAdminBundle:Default:detalleUsuario.html.php', array('usuario' => $usuario, 'antecedentes' => $antecedentes, 'conyuge' => $conyuge, 'tienesHipotecario' => $tienesHipotecario, 'tienesAhorroInversion' => $tienesAhorroInversion, 'tienesAutomoviles' => $tienesAutomoviles, 'tienesSociedades' => $tienesSociedades, 'debesHipotecario' => $debesHipotecario, 'debesTarjetaCredito' => $debesTarjetaCredito, 'debesLineaCredito2' => $debesLineaCredito, 'debesCreditoConsumo' => $debesCreditoConsumo, 'creditoConsumo' => $creditoConsumo, 'creditoAutomotriz' => $creditoAutomotriz, 'creditoHipotecario' => $creditoHipotecario, 'creditoConsolidacion' => $creditoConsolidacion, 'creditoCuenta' => $creditoCuenta, 'totalHipotecario' => $totalHipotecario, 'totalAhorroInversion' => $totalAhorroInversion, 'totalAutomoviles' => $totalAutomoviles, 'totalSociedades' => $totalSociedades, 'activos' => $activos, 'totalDebesHipotecario' => $totalDebesHipotecario, 'totalDebesTarjeta' => $totalDebesTarjeta, 'totalDebesLinea' => $totalDebesLinea, 'totalDebesCredito' => $totalDebesCredito, 'totalDebes' => $totalDebes, 'dondeCreditoConsumo' => $dondeCreditoConsumo, 'filepickerArchivo' => $filepickerArchivo));
    }

    public function add() {

        return 0;
    }

    public function del() {

        return 0;
    }

    /// borrar 
    public function pdf3Action(Request $request) {
        
    }

    public function ingresoAdminAction(Request $request) {

        $nombre = $request->request->get('nombre');
        $contrasena = $request->request->get('contrasena');
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $session->set('usuario', 'admin');

        $session->set('nombre', $nombre);
        $session->set('contrasena', $contrasena);




        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));


        if ($admin) {

            $response = '100';


            $session->set('admin', $nombre);



            return new Response($response);
        } else {

            $response = '200';
            return new Response($response);
        }
    }

    public function detalleUsuarioAction(Request $request) {



        return new Response(0);
    }

    public function cargaDatosAction() {



        $session = $this->getRequest()->getSession();
        $nombre = $session->get('nombre');
        $contrasena = $session->get('contrasena');
        $em = $this->getDoctrine()->getManager();




        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));


        if ($admin) {

            $em = $this->getDoctrine()->getManager();
            $q = $em->createQuery("select u from BenchUsuariosBundle:Usuario u where u.archivar = 'no' or u.archivar is null and u.final = 'SI'");
            $usuarios = $q->getResult();

            return $this->render('BenchAdminBundle:Default:principal.html.php', array('usuario' => $usuarios));
        }
    }

    public function archivadosAction() {

        $session = $this->getRequest()->getSession();
        $nombre = $session->get('nombre');
        $contrasena = $session->get('contrasena');
        $em = $this->getDoctrine()->getManager();




        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));


        if ($admin) {

            $em = $this->getDoctrine()->getManager();
            $q = $em->createQuery("select u from BenchUsuariosBundle:Usuario u where u.archivar = 'si' ");
            $usuarios = $q->getResult();
            return $this->render('BenchAdminBundle:Default:archivados.html.php', array('usuario' => $usuarios));
        }
    }

    public function todosAction() {


        $session = $this->getRequest()->getSession();
        $nombre = $session->get('nombre');
        $contrasena = $session->get('contrasena');
        $em = $this->getDoctrine()->getManager();




        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));


        if ($admin) {

            $em = $this->getDoctrine()->getManager();
            $usuarios = $em->getRepository('BenchUsuariosBundle:Usuario')->findAll();
            return $this->render('BenchAdminBundle:Default:todos.html.php', array('usuario' => $usuarios));
        }
    }

    public function todos2Action() {


        $session = $this->getRequest()->getSession();
        $nombre = $session->get('nombre');
        $contrasena = $session->get('contrasena');
        $em = $this->getDoctrine()->getManager();




        $admin = $em->getRepository('BenchAdminBundle:Admin')->findOneBy(array('nombre' => $nombre, 'password' => $contrasena));


        if ($admin) {

            $em = $this->getDoctrine()->getManager();
            $usuarios = $em->getRepository('BenchUsuariosBundle:Usuario')->findAll();
            return $this->render('BenchAdminBundle:Default:todos.html.php', array('usuario' => $usuarios));
        }
    }

    public function asignarAction(Request $request) {


        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        $us = $request->request->get('valor');



        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));


        $usuario->setAsignar($us);
        $em->merge($usuario);
        $em->flush();




        $respuesta = '100';

        return new Response($respuesta);
    }

    public function dicomUsuarioAction(Request $request) {

        $request = $this->get('request');
        $id = $request->request->get('dicom');
        $respuesta = '100';
        $session = $this->getRequest()->getSession();

        $admin = $session->get('admin');

        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));

        $usuario->setAsignar($admin);
        $usuario->setArchivar('SI');
        $usuario->setDicom('SI');
        $em->merge($usuario);
        $em->flush();
        






        $nombe = $usuario->getNombre();
        $apellido = $usuario->getApellido();
        $apellido2 = $usuario->getApellidoseg();
        $mail = $usuario->getEmail();


        $address = $mail;
        $postmark = new Postmark("f907f8d4-c537-4c49-8ccc-3b96938cef8a", "info@benchbanking.com", "info@benchbanking.com");

        $result = $postmark->to($address)
                ->subject("BenchBanking ")
                ->html_message('


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin t&iacute;tulo</title>

</head>

<body>

<style>

#content {
	width: 600px;
	border-top-width: 5px;
	border-bottom-width: 5px;
	border-top-style: solid;
	border-bottom-style: solid;
	border-top-color: #e7501e;
	border-bottom-color: #e7501e;
	background-color: #FFFFFF;
}
#header {
	height: 156px;
}

#texto {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #9d9d9d;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #dadbdb;
	padding-top: 20px;
	padding-bottom: 20px;
	margin-right: 20px;
	margin-left: 20px;
}
#datos {
	float: left;
}
#startup {
	float: right;
}
#footer {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #9d9d9d;
	height: 40px;
	border-top-style: solid;
	border-top-width: 1px;
	border-top-color: #dadbdb;
	margin: 20px;
	padding-top: 20px;
	padding-right: 0px;
	padding-bottom: 20px;
	padding-left: 0px;
}

.destacado {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 24px;
	color: #e7501e;
}
.naranjo {
	color: #E7501E;
}
a:link {
	font-weight: bold;
	color: #E7501E;
	text-decoration: none;
}
a:hover {
	font-weight: bold;
	color: #E7501E;
	text-decoration: underline;
}
a:visited {
	font-weight: bold;
	color: #9D9D9D;
	text-decoration: none;
}
a:active {
	font-weight: bold;
	color: #E7501E;
	text-decoration: none;
}

</style>
<div id="content">
  <div id="header"><img src="http://www.theclick.cl/mailingBench/img/header.gif" width="600" height="156" /></div>
  <div id="texto"> 
    <p><strong>Estimado(a) :</strong> ' . $nombe . ' ' . $apellido . '  </p>
    <p>      Lamentablemente, nuestro sistema ha arrojado que presentas deudas impagas en el sistema financiero.
    </p>
    <p>Estas deudas morosas impiden el acceso a créditos en todas las instituciones financieras con las que BenchBanking trabaja.</p>
    <p>Te recomendamos solucionar esta situación para poder acceder a nuestros servicios. </p>
    <p>Saludos cordiales,</p>
    <p>Equipo <strong>Bench<span class="naranjo">Banking</span></strong></p>
    </div>
  <div id="footer">
    <div id="datos">Si deseas ponerte en contacto con nosotros<br />
escríbenos a <a href="mailto:info@benchbanking.com">info@benchbanking.cl</a><br />
o llámanos al <strong>+56 (2) 2570 9496</strong></div>
    <div id="startup">Proyecto financiado por:<br />
    <img src="http://www.theclick.cl/mailingBench/img/startup_logo.gif" width="179" height="20" /></div>
  </div>
</div>




</body>
</html>
























		')
                ->send();



        if ($result === true) {

            echo "Mensaje enviado";
        } else {
            echo "mensaje no enviado";
        }



        return new Response($respuesta);
    }

    public function archivarUsuarioAction(Request $request) {


        $request = $this->get('request');
        $id = $request->request->get('archivar');
        $respuesta = '100';


        $em = $this->getDoctrine()->getManager();
        $session = $this->getRequest()->getSession();

        $admin = $session->get('admin');
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $id));
        $usuario->setAsignar($admin);
        $usuario->setArchivar('SI');

        $em->merge($usuario);
        $em->flush();


        return new Response($respuesta);
    }

}
