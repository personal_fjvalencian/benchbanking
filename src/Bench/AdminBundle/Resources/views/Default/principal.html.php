<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Administrador Bench Banking</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <meta name="robots" content="noindex">
  <link href="<?php echo $view['assets']->getUrl('bundles/benchadmin/bootstrap/css/bootstrap.css');?>" rel="stylesheet" id="main-theme-script" />
  <link href="<?php echo $view['assets']->getUrl('bundles/benchadmin/css/themes/default.css');?>" rel="stylesheet" id="theme-specific-script" />

  <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery.js');?>"></script>
  
  
  <script type="text/javascript" src="php/combo.js"></script>
  <script type="text/javascript" src="php/asignar.js"></script>




     <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/tables/jquery.dataTables.min.js');?>" type="text/javascript"></script>

     <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/tables/jquery.dataTables.columnFilter.js');?>" type="text/javascript"></script>

  
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/usuarios/tablauserformato.js')?>"></script>

     
   <script type="text/javascript" src="<?php echo $view['assets']->getUrl('bundles/benchadmin/js/archivar.js')?>"></script>
   
   
   <script type="text/javascript" src="<?php echo $view['assets']->getUrl('bundles/benchadmin/js/dicom.js')?>"></script>

  
   
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('bundles/benchadmin/js/asignar.js')?>"></script>

    
    
    
  <link href="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/datepicker/css/datepicker.css');?>" rel="stylesheet" />
  


  <link rel="stylesheet" type="text/css" media="screen,projection" href="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/uniform/css/uniform.default.css');?>" />
  

  <link type="text/css" href="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/chosen/chosen/chosen.intenso.css');?>" rel="stylesheet" />   

 
  <link href="<?php echo $view['assets']->getUrl('bundles/benchadmin/css/simplenso.css');?>" rel="stylesheet" />
  

  <link rel="shortcut icon" href="<?php echo $view['assets']->getUrl('bundles/benchadmin/images/ico/favicon.ico');?>" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $view['assets']->getUrl('bundles/benchadmin/images/ico/apple-touch-icon-144-precomposed.png');?>" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $view['assets']->getUrl('bundles/benchadmin/images/ico/apple-touch-icon-114-precomposed.png');?>" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $view['assets']->getUrl('bundles/benchadmin/images/ico/apple-touch-icon-72-precomposed.png');?>" />
  <link rel="apple-touch-icon-precomposed" href="<?php echo $view['assets']->getUrl('bundles/benchadmin/images/ico/apple-touch-icon-57-precomposed.png')?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body id="dashboard" class="hidden">






 <div id="div_carga">
   <img id="cargador" src="./img/loader.gif"/>
   </div>

<!-- Top navigation bar -->
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="benchAdmin">Bench banking</a>
      <div class="btn-group pull-right">
        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="icon-user"></i> 
          <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
        
          <li><a href="ingresoAdmin">Logout</a></li>
        </ul>
      </div>
      <div class="nav-collapse">
          
          
        <ul class="nav">
        
              
          <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Menu
                      <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                	  <li class="nav-header">Main</li>        
          <li class="active"><a href="benchAdmin"><i class="icon-home"></i> Home </a></li>
           <li class="active"><a href="benchAdmin"><i class="icon-home"></i> Home </a></li>
             <li class="active"><a href="benchAdmin"><i class="icon-home"></i> Home </a></li>
      
         
                </ul>
          </li>
       
        </ul>
      </div>
    </div>
  </div>
</div>
  
<div class="container-fluid">
  <div class="row-fluid">
    
    <!-- Bread Crumb Navigation -->
  <div class="span10">
      
              <h6><a href="todos">Ver todos los usuarios</a></h6>
              <h6><a href="archivados">Ver Usuario archivados</a></h6>
              
              <h5 style="font-size: 30px;color:#000;">Usuarios / No revisados </h5>
        
	  <!-- Table -->
      <div  style="width:1900px; height:auto; margin:auto;">
      
        <div class="span12">
         	 <!-- Portlet: Browser Usage Graph -->
             <div class="box" id="box-0">
              <h4 class="box-header round-top">Usuarios
                  <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                     <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>  
                 
              </h4>         
              <div class="box-container-toggle">
                  <div class="box-content" style="">
                    

      
      

                     <section id="contenido">
                         
                         
                         
                         
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-condensed" id="tablaU" width="1200" >
                <thead>
                	<tr>
                    
                        <th width="1%"><button>id</button></th>
                  <th width="8%"><button>fecha registro</button></th>
                  <th width="8%" ><button>Rut</button></th>
                  <th width="8%" ><button>Nombre / Apellidos</button></th>
           
                  <th width="8%"><button>Email</button></th>
                   <th width="8%"><button>Teléfono</button></th>
                   <th width="1%">Dicom</th>
                  
                  
                  
                  <th width="1%">Adjuntos</th>
                  <th width="9%"> Ver Datos</th>
                   <th width="8%"> Crear Pdf</th>
           
             
                  <th width="9%">ACTIVOS - PASIVOS</th>
                  <th width="9%">Archivar1</th>

                  <th width="13%">Dicom</th>
                  
                 
                


				</tr>

				<tr>
                
                  <th width="1%"></th>
                <th width="1%">fecha registro</th>
                 <th width="1%">Rut</th>
                  <th  width="1%">Nombre / Apellidos</th>
           
                  <th width="1%">Email</th>
                 <th width="1%">Teléfono</th>
                  <th width="1%">Dicom</th>
                  
                  
                  <th width="1%" >Adjuntos</th>
        
                  <th width="1%"></th>
                  <th width="1%"></th>
                  <th width="1%"></th>
                  <th width="1%"></th>
                  <th width="1%"></th>
</tr>
                 </thead>
            
          
                
                  <tbody>
            
            
                 <?php     foreach ($usuario as $usuario): ?>
                      
                    <tr id="itemusuario_<?php echo $usuario->getId()?>">
                     
                        
                        <td><?php echo $usuario->getId();?></d>
                         <td style="font-size:14px"><?php echo $usuario->getFechaingreso()->format('d-m-Y');?></td>
                         <td style="font-size:14px"><?php echo $usuario->getRut(); ?></td>
                         <td  style="font-size:14px"><?php echo $usuario->getNombre(); ?> <?php echo $usuario->getApellido();?></td>
			 <td  style="font-size:11px;" ><?php echo $usuario->getEmail(); ?></td>
                         <td  style="font-size:11px;" ><?php echo $usuario->getTelefono(); ?></td>
                         <td><?php  echo $usuario->getAutorizoDicomSiNo();?></td>
                         
                 
                         

               


                  <td>
                      
                      <?php 
                      
                      $filename= '../web/documentos/'.$usuario->getRut();
                      $archivo = $usuario->getArchivos();
                      $cont = 0;
                      foreach ($archivo as $archivo){
                          $cont++;
                          
                          
                      }
                      
                     
                      if(file_exists($filename) or $cont > 0 ){
                          
                          echo 'SI';
                          
                          
                      }else{
                          
                          echo 'NO';
                      }
                      
                      
                      ?>
                      
                      
                 
                      
                   
                  </td>



                <td>


               <form action="detalleUsuario" method="post" target="_blank" >
                                    
                                    <input name="id" type="hidden" value="<?php echo $usuario->getId()?>">
                                    <button class="btn btn-success" href="#">
                                   

                                        <i class="icon-zoom-in icon-white"></i>
                                        Ver Datos
                                    </button>
                               
</form>

       


                <td>
                      <form action="pdf3" method="post" target="_blank" >
                                    
                                    <input name="id" type="hidden" value="<?php echo $usuario->getId()?>">
                                    <button class="btn btn-success" href="#">
                                   

                                        <i class="icon-zoom-in icon-white"></i>
                                        Crear pdf
                                    </button>
                               
</form>
                </td>



      <td>

          
          <?php 
          
                $bienesRaices = $usuario->getBienesraices();
                $ahorroInVersion = $usuario->getAhorroinverson();
                $automoviles = $usuario->getAutomoviles();
                $sociedades = $usuario->getSociedades();
               
          ?>
             
                  <?php
                  
                  
                  $totalHipotecario = 0;
                  
                  foreach ($bienesRaices as $bienesRaices):
                      
                       $total = $bienesRaices->getAvaluo();
                  
                       $totalHipotecario1 =str_replace(".","",$total);
                       
                       $totalHipotecario = $totalHipotecario + $totalHipotecario1;
                     
                 
                  
                      
                      
                  endforeach;
                  
                  
                  
                    
                                    if(isset($totalHipotecario)){
                                        
                                        
                                    }else{
                                        
                                         $totalHipotecario = 0;
                                    }
        
                                    
                  
                   $totalAhorroInversion = 0;
                  
                  foreach($ahorroInVersion as $ahorroInVersion):
                                   
                                    $total =$ahorroInVersion->getValorcomercial();
                  
                                    $totalAhorroInversion1 =str_replace(".","",$total);
                                    
                                    $totalAhorroInversion = $totalAhorroInversion + $totalAhorroInversion1;
                                    endforeach;
                  
                     if(isset($totalAhorroInversion)){
                                        
                                    }else{
                                        
                                        $totalAhorroInversion = 0;
                                    }
                                    
                                    
                  
               
                 $totalAutomoviles = 0;
                  
                  foreach ($automoviles as $automoviles):
                          $total = $automoviles->getValorcomercial();
                          $totalAutomoviles1 =str_replace(".","",$total);
                          
                          $totalAutomoviles = $totalAutomoviles + $totalAutomoviles1;
                           endforeach;
                  
                  
                      
                                    if(isset($totalAutomoviles)){
                                        
                                        
                                    }else{
                                        
                                        $totalAutomoviles = 0;
                                    }
     
                    
                                    
                   $totalSociedades = 0;
                  
                     foreach ($sociedades as $sociedades):
                      
                               $total = $sociedades->getValor();
                     
                               $totalSociedades1=str_replace(".","",$total);
                               
                               $totalSociedades = $totalSociedades + $totalSociedades1;
                       endforeach;
                       
                       
                         
                                    if(isset($totalSociedades)){
                                        
                                        
                                    }else{
                                        
                                        $totalSociedades = 0;
                                    }
                                    
                             $activos =   $totalAutomoviles + $totalSociedades + $totalAhorroInversion + $totalHipotecario;
                             
                             
                             
                             
                             $debesHipotecarioTotal = $usuario->getDebeshipotecario();
                             $debesTarjetaCreditoTotal = $usuario->getDebestarjetacredito();
                             $debesLineaCreditoTotal = $usuario->getDebeslineacredito();
                             $debesCreditoConsumoTotal = $usuario->getDebescreditoconsumo();
                             
                             
                             
                             
                             
                             
                               
                             $totalDebesHipotecario = 0;
                             
                               foreach ($debesHipotecarioTotal as $debesHipotecarioTotal):
                                    $total = $debesHipotecarioTotal->getDuedavigente();
                                    $totalDebesHipotecario1=str_replace(".","",$total);
                                    $totalDebesHipotecario =  $totalDebesHipotecario +  $totalDebesHipotecario1;
                                    endforeach;
                                    
                                    if(isset($totalDebesHipotecario)){
                                        
                                        
                                    }else{
                                        
                                         $totalDebesHipotecario = 0;
                                    }
                             
                                    
            $totalDebesTarjeta  = 0;  
            
            
            
                                     
            foreach ($debesTarjetaCreditoTotal as $debesTarjetaCreditoTotal):
                                    $total = $debesTarjetaCreditoTotal->getMontoutilizado();
                                    $totalDebesTarjeta1=str_replace(".","",$total);
                                    $totalDebesTarjeta =  $totalDebesTarjeta +  $totalDebesTarjeta1;
                                    endforeach;
                                    
                                    if(isset($totalDebesTarjeta)){
                                        
                                        
                                    }else{
                                        
                                         $totalDebesTarjeta = 0;
                                    }
                                    
                                    
                                    
                                     $totalDebesLinea = 0;
                                   foreach ($debesLineaCreditoTotal as $debesLineaCreditoTotal):
                                    $total = $debesLineaCreditoTotal->getMontoUtilizado();
                                    $totalDebesLinea1=str_replace(".","",$total);
                                    $totalDebesLinea =  $totalDebesLinea +  $totalDebesLinea1;
                                    endforeach;
                                    
                                    if(isset($totalDebesLinea)){
                                        
                                        
                                    }else{
                                        
                                         $totalDebesLinea = 0;
                                    }
                                        
                                    
                                    
                                    
                                    
                                $totalDebesCredito = 0;
                                foreach ($debesCreditoConsumoTotal as $debesCreditoConsumoTotal):
                                    $total = $debesCreditoConsumoTotal->getDeudatotal();
                                    $totalDebesCredito1=str_replace(".","",$total);
                                    $totalDebesCredito =  $totalDebesCredito +  $totalDebesCredito1;
                                    endforeach;
                                    
                                    if(isset($totalDebesCredito)){
                                        
                                        
                                    }else{
                                        
                                        
                                         $totalDebesCredito = 0;
                                         
                                    }
                                    
                             
                   $pasivos = $totalDebesCredito + $totalDebesLinea + $totalDebesTarjeta + $totalDebesHipotecario;
                     
                   
                    $total = $activos - $pasivos;
                    
                    
                    echo $total;
                  
                  ?>




      </td>



               

                  <td>


                    
                  
                    <input type="submit" class="archivar_button"  value="archivar1" id="archivar-<?php echo $usuario->getId();?>">
                

                  </td>





                    <td>


                    
                  
                    <input type="submit" class="dicom_button"  value="dicom" id="dicom-<?php echo $usuario->getId();?>">
                

                    
                  </td>
               
               
            
	</tr>

 <?php endforeach ?>
					
                <tbody>
            </table>



                     </section>










                  </div>
              </div>
            </div><!--/span-->




   </div>
      </div>  
 
    </div><!--/span-->
  </div><!--/row-->



<div style="clear:both;"></div>


  <script>

      $( document ).ready(function() {


      $('#myModal').modal('show');
      });


  </script>



  !-- Modal -->
  <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Modal header</h3>
      </div>
      <div class="modal-body">
          <p>One fine body…</p>
      </div>
      <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
          <button class="btn btn-primary">Save changes</button>
      </div>
  </div>






  <footer>
    <p>&copy; Bench Banking</p>
  </footer>
    <div id="box-config-modal" class="modal hide fade in" style="display: none;">
      <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3></h3>
      </div>
      <div class="modal-body">
        <p></p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal"></a>
        <a href="#" class="btn" data-dismiss="modal"></a>
      </div>
    </div>
</div><!--/.fluid-container-->

    
  

    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.core.min.js');?>"></script>
	<script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js');?>"></script>
	<script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js');?>"></script>
	<script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js');?>"></script>
    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js');?>"></script>
    
  
    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js')?>"></script>
    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js')?>"></script>

 
    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/bootstrap/js/bootstrap.min.js')?>'"></script>
    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/bootbox/bootbox.min.js')?>"></script>

    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/datepicker/js/bootstrap-datepicker.js');?>"></script>

		
    <!-- jQuery Cookie -->    
    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/jquery.cookie/jquery.cookie.js');?>"></script>
    
    <!-- Full Calender -->
    <script type='text/javascript' src='<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/fullcalendar/fullcalendar/fullcalendar.min.js');?>'></script>
    
    <!-- CK Editor -->
	<script type="text/javascript" src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/ckeditor/ckeditor.js');?>"></script>
    
    <!-- Chosen multiselect -->
    <script type="text/javascript" language="javascript" src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/chosen/chosen/chosen.jquery.min.js');?>"></script>  
    
    <!-- Uniform -->
    <script type="text/javascript" language="javascript" src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/uniform/jquery.uniform.min.js')?>"></script>
    
    
	
    <script src="<?php echo $view['assets']->getUrl('bundles/benchadmin/scripts/simplenso/simplenso.js');?>"></script>
  
</body>
