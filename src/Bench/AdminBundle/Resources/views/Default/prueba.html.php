<meta name="robots" content="noindex">

<link href="{{ asset('bundles/benchadmin/css/table.css') }}" type="text/css" rel="stylesheet" />

<link href="{{ asset('bundles/alidatatable/css/smoothness/jquery-ui-1.8.4.custom.css') }}" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="{{ asset('bundles/alidatatable/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('bundles/alidatatable/js/jquery.datatable.inc.js') }}"></script>
<script type="text/javascript" src="{{ asset('bundles/alidatatable/js/jquery.dataTables.min.js') }}"></script>    


<scrip>
    

{{ datatable({ 
        'edit_route' : 'add',
        'delete_route' : 'del',
        'otra_route' : 'pdf',

        'js' : {
            'sAjaxSource' : path('grid')
        }
    })
}}


</scrip>

