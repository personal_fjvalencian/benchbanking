<!DOCTYPE HTML>
<html>

<head>
<meta name="robots" content="noindex">
<META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<title>BenchBanking - Estado de situación</title>

<link href="<?php echo $view['assets']->getUrl('bundles/benchadmin/css/style-pdf.css');?>" rel="stylesheet" type="text/css" />

</head>

<body>
    
    
<header><img src="<?php echo $view['assets']->getUrl('bundles/benchadmin/img/header.png');?>" width="626" height="160"></header>

<!------- PAGINA 1 --------->

<div class="container">

</br>
<form id="contactform" class="rounded" method="post" action="">
<h3>Datos Personales</h3>

<div class="cols">

<div class="field">
	<label for="name">Rut:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Fecha Registro:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Nombre:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Apellidos:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">fecha de Nacimiento:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Sexo:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Nacionalidad:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Estado Civil:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Nivel Educación:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Profesión:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Universidad:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="email">Email:</label>
  	<input type="text" class="input" name="email" id="email" />
	<!--<p class="hint">Enter your email.</p>-->
</div>

<div class="field">
	<label for="name">Celular:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Tipo de Casa:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">N° Dependientes:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Arriendo / Dividendo:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Región:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Comuna:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>



</div>

<div class="field">
	<label class="label-adress" for="name">Dirección:</label>
  	<input type="text" class="input-adress" name="name" id="name" />
</div>

</br>



<h3>Datos Laborales</h3>


<div class="cols">
<div class="field">
	<label for="name">Situación Laboral:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Empleador:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Industria:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">fecha de ingreso:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Cargo Actual:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>



<div class="field">
	<label for="name">Rut Empresa:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Región:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Comuna:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Ciudad:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Teléfono:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="email">Tipo de Sueldo:</label>
  	<input type="text" class="input" name="email" id="email" />
	<!--<p class="hint">Enter your email.</p>-->
</div>

<div class="field">
	<label for="name">Sueldo Fijo:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Sueldo Variable:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Sueldo Líquido:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

</div>

<div class="field">
	<label class="label-adress" for="name">Dirección:</label>
  	<input type="text" class="input-adress" name="name" id="name" />
</div>


</br>
<h3>Datos Conyuge</h3>
<div class="cols">
<div class="field">
	<label for="name">Nombres:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Apellidos:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Rut:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Fecha de Nacimiento:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Nacionalidad:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Nivel de Educación:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Profesión:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Universidad:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Actividad:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Cargo Actual:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Antiguedad Laboral:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Renta Líquida:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Empresa:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Teléfono:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>




</div>

</br>



</form>

</div>


</div>

<!------- PAGINA 2 --------->

<div class="container">

<form id="contactform" class="rounded" method="post" action="">
</br>
<h3>Resumen Activos</h3>

<div class="cols">

<div class="field">
	<label for="name">Bienes Raices:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Ahorro e inversión:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Automóviles:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Sociedades:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name" style="color:#F60;">TOTAL ACTIVOS:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>





</div>



</br>

<h3>Resumen Pasivos</h3>

<div class="cols">

<div class="field">
	<label for="name">Créditos Hipotecarios</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Línea de Crédito:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Tarjeta de Crédito:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name">Créditos de Consumo:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>

<div class="field">
	<label for="name" style="color:#F60;">TOTAL PASIVOS:</label>
  	<input type="text" class="input" name="name" id="name" />
</div>



</div>

</br>

<div class="box-patri"><img src="img/star.png" width="23" height="24"> PATRIMONIO: DATA</div>

</br>

</form>
</div>


<!------- PAGINA 3 --------->

<div class="container">

<form id="contactform" class="rounded" method="post" action="">
</br>
<h4>Activos</h4>
<h3>Bienes Raices</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Tipo</th>
    <th>Dirección</th>
    <th>Región</th>
    <th>Comuna</th>
    <th>Avalúo Fiscal</th>
    <th>Rol</th>
    <th>Hipotecario</th></tr></thead>
<tbody><tr><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>
</br>

<h3>Ahorro de Inversión</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Tipo</th>
    <th>Institución</th>
    <th>Valor Comercial</th>
    <th>Prendado</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
</tbody>
</table></div>

</br>

<h3>Automóviles</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Tipo</th>
    <th>Marca</th>
    <th>Modelo</th>
    <th>Patente</th>
    <th>Año de Auto</th>
    <th>Valor Comercial</th>
    <th>Prendado</th>
</tr>
</thead>
<tbody><tr><td>data</td><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
<tr><td>data</td><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
</tbody>
</table></div>
</br>

<h3>Participación en Sociedades</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Nombre Sociedad</th>
    <th>Rut</th>
    <th>Porcentaje (%)</th>
    <th>Valor Participativo</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
</tbody>
</table></div>


</br>

</form>
</div>

<!------- PAGINA 4 --------->

<div class="container">

<form id="contactform" class="rounded" method="post" action="">
</br>
<h4>Pasivos</h4>
<h3>Hipotecario</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Institución</th>
    <th>Pago Mensual</th>
    <th>Deuda Total</th>
    <th>Vencimiento Final</th>
    <th>Deuda Vigente</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>
</br>

<h3>Tarjeta de Crédito</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Tipo de Tarjeta</th>
    <th>Institución</th>
    <th>Monto Aprobado</th>
    <th>Monto Utilizado</th>
    <th>Vencimiento</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>
</br>

<h3> Línea de Crédito</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Institución</th>
    <th>Monto Aprobado</th>
    <th>Monto Utilizado</th>
    <th>Vencimiento</th>
    </tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
</tr>
</tbody>
</table></div>
</br>

<h3>Créditos de Consumo</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Institución</th>
    <th>Pago Mensual</th>
    <th>Deuda Total</th>
    <th>Vencimiento Final</th>
    <th>Deuda</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>

</br>

</form>
</div>

<!------- PAGINA 5 --------->

<div class="container">

<form id="contactform" class="rounded" method="post" action="">
</br>
<h4>Pedidos</h4>
<h3>Créditos de Consumo</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Monto Crédito</th>
    <th>Plazo de Crédito</th>
    <th>Degravamen</th>
    <th>Cesantía</th>
    <th>¿Para qué utilizarás el dinero?</th>
    <th>Fecha<br>
      del<br>
      pedido</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>&nbsp;</td>
  <td>data</td></tr>
</tbody>
</table></div>

</br>

<h3>Automotriz</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Monto del Auto:</th>
    <th>Monto del Píe:</th>
    <th>Plazo del Crédito</th>
    <th>¿Cuándo piensas comprar tu auto?</th>
    <th>Marca</th>
    <th>Año</th>
    </tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
</tbody>
</table></div>
</br>

<h3>Hipotecario</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Valor Propiedad</th>
    <th>Monto Píe</th>
    <th>Porcentaje</th>
    <th>Monto de Crédito</th>
    <th>Plazo de Crédito</th>
    <th>Tipo de propiedad</th>
    <th>dfl2</th>
    <th>Fecha de Pedido</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>

</br>

<h3>Consolidación de deuda / Refinanciamiento</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Crédito a
      Consolidar /
      Refinanciar</th>
    <th>Monto de
      Crédito</th>
    <th>Plazo
      Solicitado</th>
    <th>Fecha
      del
      pedido</th>
    </tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
</tr>
</tbody>
</table></div>

</br>

<h3>Pedidos / Cuenta Corriente</h3>
<div class="datagrid"><table>
<thead><tr>
    <th>Producto</th>
    <th>Fecha del pedido</th>
    </tr></thead>
<tbody><tr><td>data</td><td>data</td>
</tr>
</tbody>
</table></div>

</br>

</form>

</div>


</body>

</html>