<?php

namespace Bench\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bank
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Bank
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /** @ORM\Column(type="string", length=100  , nullable=true) */



    private $bank_name;




    /** @ORM\Column(type="datetime"  , nullable=true) */



    private $created_date;


    /**
     * @ORM\ManyToOne(targetEntity="Bench\AdminBundle\Entity\Admin", inversedBy="bank")
     * @ORM\JoinColumn(name="admin_id", referencedColumnName="id")
     */

    private $admin;


    /**
     * @ORM\OneToMany(targetEntity="Bench\AdminBundle\Entity\Product", mappedBy="bank")
     */



    private $product;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->product = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bank_name
     *
     * @param string $bankName
     * @return Bank
     */
    public function setBankName($bankName)
    {
        $this->bank_name = $bankName;
    
        return $this;
    }

    /**
     * Get bank_name
     *
     * @return string 
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * Set created_date
     *
     * @param \DateTime $createdDate
     * @return Bank
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;
    
        return $this;
    }

    /**
     * Get created_date
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set admin
     *
     * @param \Bench\AdminBundle\Entity\Admin $admin
     * @return Bank
     */
    public function setAdmin(\Bench\AdminBundle\Entity\Admin $admin = null)
    {
        $this->admin = $admin;
    
        return $this;
    }

    /**
     * Get admin
     *
     * @return \Bench\AdminBundle\Entity\Admin 
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Add product
     *
     * @param \Bench\AdminBundle\Entity\Product $product
     * @return Bank
     */
    public function addProduct(\Bench\AdminBundle\Entity\Product $product)
    {
        $this->product[] = $product;
    
        return $this;
    }

    /**
     * Remove product
     *
     * @param \Bench\AdminBundle\Entity\Product $product
     */
    public function removeProduct(\Bench\AdminBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

    /**
     * Get product
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProduct()
    {
        return $this->product;
    }
}