<?php

namespace Bench\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BankExecutive
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class BankExecutive
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
