<?php

namespace Bench\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(type="string", length=100  , nullable=true) */



    private $product_name;



    /** @ORM\Column(type="datetime"  , nullable=true) */



    private $created_date;


    /**
     * @ORM\ManyToOne(targetEntity="Bench\AdminBundle\Entity\ProductCategory", inversedBy="product")
     * @ORM\JoinColumn(name="productcategory_id", referencedColumnName="id")
     */

    private $category;


    /**
     * @ORM\ManyToOne(targetEntity="Bench\AdminBundle\Entity\Bank", inversedBy="product")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */



    private $bank;







    private $admin;

    /**
     * @ORM\ManyToOne(targetEntity="Bench\AdminBundle\Entity\Admin", inversedBy="product")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */










    /** @ORM\Column(type="integer" , nullable=true) */




    private $min_salary;


    /** @ORM\Column(type="integer" , nullable=true) */




    private $initial_ammount;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product_name
     *
     * @param string $productName
     * @return Product
     */
    public function setProductName($productName)
    {
        $this->product_name = $productName;
    
        return $this;
    }

    /**
     * Get product_name
     *
     * @return string 
     */
    public function getProductName()
    {
        return $this->product_name;
    }

    /**
     * Set created_date
     *
     * @param \DateTime $createdDate
     * @return Product
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;
    
        return $this;
    }

    /**
     * Get created_date
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * Set min_salary
     *
     * @param integer $minSalary
     * @return Product
     */
    public function setMinSalary($minSalary)
    {
        $this->min_salary = $minSalary;
    
        return $this;
    }

    /**
     * Get min_salary
     *
     * @return integer 
     */
    public function getMinSalary()
    {
        return $this->min_salary;
    }

    /**
     * Set initial_ammount
     *
     * @param integer $initialAmmount
     * @return Product
     */
    public function setInitialAmmount($initialAmmount)
    {
        $this->initial_ammount = $initialAmmount;
    
        return $this;
    }

    /**
     * Get initial_ammount
     *
     * @return integer 
     */
    public function getInitialAmmount()
    {
        return $this->initial_ammount;
    }

    /**
     * Set category
     *
     * @param \Bench\AdminBundle\Entity\ProductCategory $category
     * @return Product
     */
    public function setCategory(\Bench\AdminBundle\Entity\ProductCategory $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Bench\AdminBundle\Entity\ProductCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set bank
     *
     * @param \Bench\AdminBundle\Entity\Bank $bank
     * @return Product
     */
    public function setBank(\Bench\AdminBundle\Entity\Bank $bank = null)
    {
        $this->bank = $bank;
    
        return $this;
    }

    /**
     * Get bank
     *
     * @return \Bench\AdminBundle\Entity\Bank 
     */
    public function getBank()
    {
        return $this->bank;
    }


}