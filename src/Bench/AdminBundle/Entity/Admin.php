<?php

namespace Bench\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Admin
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Admin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
      /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100 , nullable=true)
     */
    
    
    private $nombre;
    
      /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100 , nullable=true)
     */
    
    
    private $password;
    
      /**
     * @var string
     *
     * @ORM\Column(name="rol", type="string", length=100 , nullable=true)
     */
    
    
    private $rol;
    
    
    
    
      /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime" , nullable=true)
     */
    
    
    
    private $fecha;
    
    
    
      /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=100 , nullable=true)
     */
    
    
    private $estado;



    /**
     * @ORM\OneToMany(targetEntity="Bench\AdminBundle\Entity\Bank", mappedBy="admin")
     */


    private $bank;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bank = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Admin
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Admin
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set rol
     *
     * @param string $rol
     * @return Admin
     */
    public function setRol($rol)
    {
        $this->rol = $rol;
    
        return $this;
    }

    /**
     * Get rol
     *
     * @return string 
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Admin
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Admin
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add bank
     *
     * @param \Bench\AdminBundle\Entity\Bank $bank
     * @return Admin
     */
    public function addBank(\Bench\AdminBundle\Entity\Bank $bank)
    {
        $this->bank[] = $bank;
    
        return $this;
    }

    /**
     * Remove bank
     *
     * @param \Bench\AdminBundle\Entity\Bank $bank
     */
    public function removeBank(\Bench\AdminBundle\Entity\Bank $bank)
    {
        $this->bank->removeElement($bank);
    }

    /**
     * Get bank
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBank()
    {
        return $this->bank;
    }
}