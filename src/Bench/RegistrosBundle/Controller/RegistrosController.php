<?php

namespace Bench\RegistrosBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\UsuariosBundle\Entity\Antecedenteslaboral;
use Bench\UsuariosBundle\mail\Postmark;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Bench\RegistrosBundle\codificar;
use Bench\RegistrosBundle\Controller\zendesk;
use Bench\RegistrosBundle\Controller\codificaUrl;
use Bench\UsuariosBundle\Controller\verificaRut;




class RegistrosController extends Controller {





    
    public function registroAction(Request $request) {

        $session = $this->getRequest()->getSession();





        $EmailUsuario = $session->get('EmailUsuario');

        if($EmailUsuario){


        }else{

            $EmailUsuario = '';
        }

        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        return $this->render('BenchRegistrosBundle:Default:registro.html.twig' , array('token' => $token , 'EmailUsuario' => $EmailUsuario ));
        
    }


    public function activarUserAction($token){


                $codificaUrl = new codificaUrl();

       
                $rut = $codificaUrl->base64_url_decode($token);
                $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
                $em = $this->getDoctrine()->getManager();

               
                $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut ));
                $usuario->setActivado('si');
                $em->persist($usuario);
                $em->flush();
                return $this->render('BenchPaginasBundle:Default:activarUsuario.html.twig' , array('token' => $token , 'usuario' => $usuario ));

    }

    public function guardaRegistroAction(Request $request) {
        
        $request->headers->addCacheControlDirective('no-store', true);
        $nombre = $request->request->get('nombre');
        $apellido1 = $request->request->get('aapellido1');
        $apellido2 = $request->request->get('apellido2');
        $email = $request->request->get('email');
        $password2 = $request->request->get('password2');
        $rut = $request->request->get('rutRegistro');
        $telefono = $request->request->get('telefono');
        $tokenpost = $request->request->get('token');
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
    


        $iterations = 20;
        $salt = '5512858';
        $pas2 = hash_pbkdf2("sha512", $password2,  $salt , $iterations,32);
        
        $verificaRut = new verificaRut();
        $verifica  = $verificaRut->valida_rut($rut);

         if($verifica == 1){


        if($tokenpost == $token){

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
        if ($usuario) {
            $respuesta = '900';
            return new Response($respuesta);
        } else {


            function valida_email($email) {
                if (eregi("^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email))
                    return true;
                else
                    return false;
                 $respuesta = '880';
            }

            if ($nombre == '' or $apellido1 == '' or $apellido2 == '' or $email == '' or $password2 == '' or $rut == '' or $telefono == '') {

                $respuesta = '880';


                return new Response($respuesta);
            } else {

                    if (valida_email($email)) {
                        
                   
                    $usuario = new Usuario();
                    $usuario->setRut($rut);
                    $usuario->setNombre($nombre);
                    $usuario->setApellido($apellido1);
                    $usuario->setApellidoseg($apellido2);
                    $usuario->setEmail($email);
                    $usuario->setNuevoPassword($pas2);
                    $usuario->setEstadoPassword('activado');
                    $usuario->setTelefono($telefono);
                    $usuario->setFechaingreso(new \DateTime());
                    
                    $em->persist($usuario);
                    $em->flush();

                    $idUsuario = $usuario->getId();
                    $usuario2 = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $idUsuario));
                    $Antecedenteslaboral = new Antecedenteslaboral();
                    $Antecedenteslaboral->setTelefono($telefono);
                    $Antecedenteslaboral->setUsuario($usuario2);
                    $em->persist($usuario2);
                    $em->persist($Antecedenteslaboral);
                    $em->flush();






                    $session = $this->getRequest()->getSession();
                    $session->set('rut', $rut);
                    $session->set('user' , 'nuevo');

                    $session->start();



                    $_SESSION['rut3'] = $rut;
                     
           
                    





              
                    $address = $email;


                    $postmark = new Postmark("f907f8d4-c537-4c49-8ccc-3b96938cef8a", "info@benchbanking.com", "info@benchbanking.com");

                    $codificaUrl = new codificaUrl();

                    $rut = $codificaUrl->base64_url_encode($rut);

                    if($_SERVER['HTTP_HOST'] == 'localhost:8888'){
                     $urlActivar = 'http://localhost:8888/bench/web/activarUser/'.$rut;
                    }

                    if($_SERVER['HTTP_HOST'] == 'www.benchbanking.com'){

                       $urlActivar = 'https://www.benchbanking.com/activarUser/'.$rut;



                    }                    
                    

                    if($_SERVER['HTTP_HOST'] == 'box.benchbanking.com'){
                    

                    $urlActivar = 'http://box.benchbanking.com/home/home/web/activarUser/'.$rut;

                    }
                    
                    $result = $postmark->to($address)
                                    ->subject("¡Gracias por registrarte en BenchBanking!")
                                    ->html_message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Mailing BenchBanking</title>
    <style>

        #outlook a {padding:0;}     
        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}     
        .ReadMsgBody {width: 100%;}
        .ExternalClass {width:100%;} 
        .backgroundTable {margin:0 auto; padding:0; width:100%;!important;} 
        table td {border-collapse: collapse;}
        .ExternalClass * {line-height: 115%;}
        img{width:100%;}
a:link {
    color: #F60;
    text-decoration: none;
}
a:hover {
    color: #F60;
    text-decoration: underline;
}
a:visited {
    font-weight: bold;
    color: #F60;
    text-decoration: none;
}
a:active {
    font-weight: bold;
    color: #F60;
    text-decoration: none;
}

p{
 margin-bottom:-2%;
 position:relative; 
}
        
        
        /* End reset */
        @media screen and (max-width: 640px){
            
            *[class="container"] { width: 320px !important; padding:0px !important}                                 
            *[class="mobile-column"] {display: block;}
            *[class="mob-column"] {float: none !important;width: 100% !important;}         
            *[class="mobile-padding"] {padding-left:10px !important;padding-right:10px !important;}         
            *[class="hide"] {display:none !important;}          
            *[class="100p"] {width:100% !important; height:auto !important;}
            *[class="50p"] {width:100% !important; height:auto !important;}
            img{width:100%;}
                    
        
        }
        
    </style>
</head>

<body bgcolor="#FFFFFF" style="margin:0px; padding:0px;">
    <table border="0" cellpadding="0" cellspacing="0" class="mobile-padding" style="background-color: #ffffff" width="100%">
        <tr>
            <td align="center" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td align="center" height="100" valign="middle"><img src="https://www.benchbanking.com/imgMaling/header.png"/></td>
                    </tr>
                </table>

              <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td height="20"></td>
                </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td align="center" valign="top"><span style="font-size:24px; color: #F60; font-family: Verdana, Geneva, sans-serif;">¡Gracias por registrarte en BenchBanking!</span></td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td>
                            <table border="0" cellpadding="10" cellspacing="0">
                                <tr>
                                   
                                </tr>

                                <tr>

                                    <p>Ahora sólo debes completar tus datos y cotizar el crédito que buscas.                                    </p>
                                    <p>Para activar tu usuario as click en siguiente <a href="'.$urlActivar.'"> '.$urlActivar.' </a>
                                    '.$urlActivar.'
                                    <p><strong>¡Nosotros haremos todo lo demás gratis!</strong></p>
                                    <p>Te saluda,</p>
                                    
                                    <p><strong style="color:#F60">Equipo BenchBanking</strong></p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                  </tr>
              </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640" style="margin-top:20px;">
                    <tr>
                        <td style="border-top: #CCC 1px solid;" height="20"><img src="https://www.benchbanking.com/imgMaling/flow.jpg"/></td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td align="center" valign="top">
                          <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                                <tr>
                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                            <tr>
                                                <td style="background-color: #EEEEEE" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0">
                                                        <tr>
                                                            <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif">Contacto</td>
                                                        </tr>

                                                        <tr>
                                                          <td align="left" style="font-size:12px; color: #666; font-family: Verdana, Geneva, sans-serif">Si deseas ponerte en contacto con nosotros<br />
escríbenos a <a href="mailto:info@benchbanking.com">info@benchbanking.com</a><br />
o llámanos al <br />
<a href="tel:  +56(2)25709004">  +56 2 25709004 </a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="20">
                                            <tr>
                                                <td align="left" height="20">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                            <tr>
                                                <td style="background-color: #EEEEEE" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0">
                                                        <tr>
                                                            <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif;"></td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" style="font-size:12px; color: #666; font-family: Verdana, Geneva, sans-serif;"> </br></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="20">
                                            <tr>
                                                <td align="left" height="20">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                            <tr>
                                                <td style="background-color: #EEEEEE" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0">
                                                        <tr>
                                                            <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif;">Financiado por:</td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" style="font-size:14px; color:#333333; font-family:Arial, Helvetica, sans-serif;"><p><img src="https://www.benchbanking.com/imgMaling/logo01.png"/></p>
                                                              <p>
                                                            <img src="https://www.benchbanking.com/imgMaling/logo02.png"/></p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td height="20"></td>
                    </tr>
                </table>

              <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td style="background-color: #F60">&nbsp;</td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td height="20"></td>
                    </tr>
                </table>
          </td>
        </tr>
    </table>
</body>
</html>')->send();



                    if ($result === true) {
                        
                    } else {
                        
                    }


/* Zend desk 
$zendesk = new zendesk();
$nombreCompleto = $usuario->getNombre() .'  '.  $usuario->getApellido() . '  ' .$usuario->getApellidoseg();            
$asuntoZend = 'NUEVO USUARIO';
$detalleZend ='Se ha registrado un nuevo usuario  Nombre '.$nombreCompleto.'  Email : '.$usuario->getEmail().'
 Rut : '.$usuario->getRut().'';
$create = json_encode(
array(
'ticket' => array(
'requester' => array(
'name' => $usuario->getNombre(),
'email' => $usuario->getEmail(),
),
'group_id' => '22',
'assignee_id' => 'matias@benchbanking.com',
'subject' =>$asuntoZend,
'custom_fields' =>
array(
	'23412128' =>$usuario->getRut() ,
               '23412138' => $nombreCompleto ,
               
              ),
    


'description' => $detalleZend

)
),
JSON_FORCE_OBJECT
);

$zendDesk = new zendesk();
$data = $zendesk->curlWrap("/tickets.json", $create, "POST");
*/






                    return new Response('100');
                } else {
                    
                }
                $respuesta = '200';

                return new Response($respuesta);
            }
        }
    }



          return new Response(10000);
}

       
  }

}

?>
