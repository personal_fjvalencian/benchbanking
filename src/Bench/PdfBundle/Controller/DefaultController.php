<?php

namespace Bench\PdfBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BenchPdfBundle:Default:index.html.twig', array('name' => $name));
    }
}
