<?php

namespace Bench\DatosUsuariosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BenchDatosUsuariosBundle:Default:index.html.twig', array('name' => $name));
    }
}
