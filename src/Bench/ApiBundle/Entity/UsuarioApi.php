<?php

namespace Bench\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsuarioApi
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class UsuarioApi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=200 , nullable=true)
     */

    private $usuario;


    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=200 , nullable=true)
     */



    private $token;


    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=200 , nullable=true)
     */




    private $comentarios;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return UsuarioApi
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return UsuarioApi
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     * @return UsuarioApi
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;
    
        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string 
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }
}