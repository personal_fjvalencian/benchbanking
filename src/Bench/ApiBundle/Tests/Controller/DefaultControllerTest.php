<?php

namespace Bench\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bench\UsuariosBundle\Entity\Usuario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Bench\ApiBundle\Exception\InvalidFormException;
use Bench\Apibundle\Form\UsuarioType;
use Bench\ApiBundle\Model\UsuarioInterface;
use Bench\UsuariosBundle\Entity\Ejecutivos;
use Bench\CreditosBundle\Entity\CreditoHipotecario;
use Bench\UsuariosBundle\Entity\Cotizaciones;
use Bench\RegistrosBundle\Controller\zendesk;

use Symfony\Component\HttpFoundation\Response;



class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/hello/Fabien');

        $this->assertTrue($crawler->filter('html:contains("Hello Fabien")')->count() > 0);
    }


    public function testAdd()
    {


        $client = static::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/usuario/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /usuario/");
        $crawler = $client->click($crawler->selectLink('Create a new entry')->link());

    }



}
