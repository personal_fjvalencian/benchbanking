<?php

namespace Bench\ApiBundle\Model;


Interface  UsuarioInterface {


    /**
     * Set rut
     *
     * @param string $rut
     * @return UsuarioInterface
     */
    public function setRut($rut);

    /**
     * Get rut
     *
     * @return string
     */
    public function getRut();

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return  UsuarioInterface
     */
    public function setNombre($nombre);

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre();


}