<?php


namespace Bench\ApiBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\UsuariosBundle\Entity\Conyuge;
use Bench\UsuariosBundle\Entity\Antecedenteslaboral;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Bench\ApiBundle\Exception\InvalidFormException;
use Bench\Apibundle\Form\UsuarioType;
use Bench\ApiBundle\Model\UsuarioInterface;
use Bench\UsuariosBundle\Entity\Ejecutivos;
use Bench\CreditosBundle\Entity\CreditoHipotecario;
use Bench\DebesBundle\Entity\DebesCreditoConsumo;
use Bench\DebesBundle\Entity\Debeshipotecario;
use Bench\DebesBundle\Entity\DebesTarjetaCredito;

use Bench\TienesBundle\Entity\Ahorroinverson;
use Bench\TienesBundle\Entity\Automoviles;
use Bench\TienesBundle\Entity\Bienesraices;
use Bench\TienesBundle\Entity\Sociedades;

use Bench\UsuariosBundle\Entity\Cotizaciones;
use Bench\RegistrosBundle\Controller\zendesk;
use Bench\FilepickerBundle\Entity\Archivos;
use Symfony\Component\HttpFoundation\Response;
use \DateTime;

class ApiController  extends FOSRestController{







	 /**
     * List all Usuarios.
     *

     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description=".")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description=".")
     *
     * @Annotations\View(
     *  templateVar="usuarios"
     * )
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     * @param string     $token
     * @return array
     */



    public function getUserAction(Request $request, ParamFetcherInterface $paramFetcher)
    {


     $nombre = $request->request->get('nombre');
    	$offset = $paramFetcher->get('offset');
        $offset = null == $offset ? 0 : $offset;
        $limit = $paramFetcher->get('limit');

        return $this->container->get('bench_api.usuario.handler')->all($limit, $offset);
       
    }






    /**
     * un Usuario.
     *

     *
     *
     *  @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing pages.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="5", description="How many pages to return.")
     *
     *   * @Annotations\View(
     *  template = "BenchApiBundle:Usuario:getUsuario.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = ""
     * )
     *
     * @param int     $id      the page id
     * @param token     $token      the page id
     * @return array
     *
     * @throws NotFoundHttpException when page not exist
     */



    public function getUsuarioAction($id , Request $request )
    {


        $usuario = $request->query->get('usuario');
        $token = $request->query->get('token');

        $em = $this->getDoctrine()->getManager();
        $usuariosApi = $em->getRepository('BenchApiBundle:UsuarioApi')->findOneBy(array('usuario' => $usuario , 'token' => $token));

        if($usuariosApi){

        $usario = $this->getOr404($id);


        return $usario;

        }else{


            $serializer = $this->container->get('jms_serializer');

            $respuesta = array  ('Respuesta' => 'Token o usuario incorrectos' );

            return  $serializer->serialize( $respuesta, 'json');





        }


    }







    /**
     * Crear un Usuario a partir de un post
     *
     * @ApiDoc(
     *   resource = true,
     *   description = " Crear un Usuario a partir de un post .",
     *   input = "Bench\ApiBundle\Form\UsuarioType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     ** @Annotations\View(
     *  template = "BenchApiBundle:Usuario:newUsuario.html.twig",
     *
     *  templateVar = "form"
     * )

     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */




/*

    public function postUserNuevoAction(Request $request)
    {


        try {
            $newUsuario = $this->container->get('bench_api.usuario.handler')->post(
                $request->request->all()
            );

            $routeOptions = array(

                'id' => $newUsuario ->getId(),
                '_format' => $request->get('_format')
            );


            return $this->routeRedirectView('ap', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

*/




    public function postUserInmobiliariasAction(Request $request){

            $em = $this->getDoctrine()->getManager();
            $donde = $request->request->get('26645736');
            $dondeEs = $request->request->get('26652999');
            $Inversionista = $request->request->get('27374558');

           $token = 'ae93bd241d61aa157b29b5097c3173c3';
           $tokenPost = $request->request->get('26644770');
           $tokenPost2 = $request->request->get('26653012');
           $tokenPost3 = $request->request->get('27374564');


            if($Inversionista  == 'Inversionista'){
                
                $ejecutivoNombre = $request->request->get('25813048');
                $ejecutivoEmail = $request->request->get('25813048');

          
                $ejecutivoTelefono = 'Inversionista';
                $Aliansas = $em->getRepository('BenchUsuariosBundle:Aliansas')->findOneBy(array ('nombre' => 'Inversionista' ));
                $Ejecutivo = $em->getRepository('BenchUsuariosBundle:Ejecutivos')->findOneBy(array('email' => $ejecutivoEmail ));
                $tipoAprobacion1 = $request->request->get('25594965_1');
                $tipoAprobacion2 = $request->request->get('25594965_2');
                $nombreUsuario = $request->request->get('25594829');
                $emailUsuario = $request->request->get('25813255');
                $profesion = $request->request->get('25177124');
                $rutUsuario = $request->request->get('25594830');
                $direccion = $request->request->get('25594833');
                $comuna = $request->request->get('25813256');
                $universidad = $request->request->get('27529362');
                $telefUsuario = $request->request->get('25594832');
                $nacionalidad = $request->request->get('25594834');
                $total1 = $request->request->get('25594854');
                $total2 = $request->request->get('25594856');
                $total3 = $request->request->get('25594858');


                $totalActivos = $total1 + $total2 +  $total3;

                


                $debesHipo = $request->request->get('25594861');
                $debesCredito = $request->request->get('25594865');
                $debesLinea = $request->request->get('25594865');

              




                $ProValor = $request->request->get('25177108');
                $ProPlazo = $request->request->get('25177110');
                $ProTipo = $request->request->get('25177112');
                $ProCuandoCompra = $request->request->get('25177114');
                $ProComuna = $request->request->get('25177115');
                $ProPie = $request->request->get('25177109');

                $dflSi = $request->request->get('25177111_1');
                $dflNo = $request->request->get('25177111_2');




                $dfl = '';
                if($dflSi == 'SI'){
                    $dfl = 'SI';
                }

                if($dflNo == 'NO'){
                    $dfl = 'NO';
                }

                $estado = $request->request->get('25594824');
                $proyecto = $request->request->get('25594827');


                $sueldoLiquido = $request->request->get('25813116');
                $situacionLaboral = $request->request->get('25594838');
                $nombreEmpleador = $request->request->get('25813117'); // no esta
                $rutEmpleador = $request->request->get('25813118');
                $direccionEmpresa = $request->request->get('25813120');
                $comunaEmpresa = $request->request->get('25813188');


                $nombreConyuge = '';
                $rutConyuge = '';
                $sueldoConyuge = '';
                $situacionLaboralConyuge = '';

                $urlUltimoMes = $request->request->get('25594846');

                $urlMes2 = $request->request->get('25594847');

                $urlMes3 = $request->request->get('25594848');
                $urlCarnet = $request->request->get('25594845');

                $urlAfp = $request->request->get('25594849');
                $urlOtros1 = $request->request->get('25594850');
                $urlOtros2 = $request->request->get('25177141');
                $urlDeclarAnual = $request->request->get('25813103');

                $totalPropiedad = $request->request->get('25177144');
                $totalAutomoviles = $request->request->get('25462475');
                $totalAhorroInversion = $request->request->get('25462481');


            }

            if($dondeEs == 'ES'){



                $ejecutivoNombre = 'ES';
                $ejecutivoEmail =  'ES';
                $ejecutivoTelefono = 'ES';
                $Aliansas = $em->getRepository('BenchUsuariosBundle:Aliansas')->findOneBy(array ('nombre' => 'ES' ));
                $Ejecutivo = $em->getRepository('BenchUsuariosBundle:Ejecutivos')->findOneBy(array('email' => $ejecutivoEmail));

                $nombreUsuario = $request->request->get('25177118');
                $emailUsuario = $request->request->get('25177122');
                $profesion = $request->request->get('25177124');
                $rutUsuario = $request->request->get('25177119');

                $universidad = '';

                $telefUsuario = $request->request->get('25177121');
                $nacionalidad = $request->request->get('25177123');


                $total1 = $request->request->get('25177144');
                $total2 = $request->request->get('25462475');
                $total3 = $request->request->get('25462481');


                $totalActivos = $total1 + $total2 +  $total3;

                $debesHipo = $request->request->get('25462295');
                $debesCredito = $request->request->get('25462183');
                $debesLinea = $request->request->get('25177151');

                $ProValor = $request->request->get('25177108');
                $ProPlazo = $request->request->get('25177110');
                $ProTipo = $request->request->get('25177112');
                $ProCuandoCompra = $request->request->get('25177114');
                $ProComuna = $request->request->get('25177115');
                $ProPie = $request->request->get('25177109');

                $dflSi = $request->request->get('25177111_1');
                $dflNo = $request->request->get('25177111_2');



                $dfl = '';
                if($dflSi == 'SI'){
                    $dfl = 'SI';
                }

                if($dflNo == 'NO'){
                    $dfl = 'NO';
                }

                $estado = $request->request->get('25177113');
                $proyecto = $request->request->get('25177116');



      
                $sueldoLiquido = $request->request->get('25177126');
                $situacionLaboral = $request->request->get('25177127');
                $nombreEmpleador = '';
                $rutEmpleador = '';
                $direccionEmpresa  = '';
                $comunaEmpresa  = '';

                $nombreConyuge = $request->request->get('25177129');
                $rutConyuge = $request->request->get('25177130');
                $sueldoConyuge = $request->request->get('25177132');
                $situacionLaboralConyuge = $request->request->get('25177133');

                $urlUltimoMes = $request->request->get('25177136');
                $urlMes2 = $request->request->get('25177137');
                $urlMes3 = $request->request->get('25177138');
                $urlCarnet = $request->request->get('25177135');
                $urlAfp = $request->request->get('25177139');
                $urlOtros1 = $request->request->get('25177140');
                $urlOtros2 = $request->request->get('25177141');
                $urlDeclarAnual = '';

                $totalPropiedad = $request->request->get('25177144');
                $totalAutomoviles = $request->request->get('25462475');
                $totalAhorroInversion = $request->request->get('25462481');





            }


            if($donde == 'inmobiliarias' ){


            $ejecutivoNombre = $request->request->get('23590838');
            $ejecutivoEmail = $request->request->get('23590839');
            $ejecutivoTelefono = $request->request->get('23590840');
            $Aliansas = $em->getRepository('BenchUsuariosBundle:Aliansas')->findOneBy(array ('nombre' => 'inmobiliarias' ));
            $Ejecutivo = $em->getRepository('BenchUsuariosBundle:Ejecutivos')->findOneBy(array('email' => $ejecutivoEmail));



            $nombreUsuario = $request->request->get('23590969');
            $emailUsuario = $request->request->get('23590823');
            $profesion = $request->request->get('23590906');
            $rutUsuario = $request->request->get('23590821');
            $telefUsuario = $request->request->get('23590886');
            $nacionalidad = $request->request->get('23590824');
            $totalActivos = $request->request->get('23591082');
            $debesHipo = $request->request->get('23591108');
            $debesCredito = $request->request->get('23591219');
            $debesLinea = $request->request->get('23591186');
            $universidad = '';

            $ProValor = $request->request->get('23590833');
            $ProPlazo = $request->request->get('23590836');
            $ProTipo = $request->request->get('23590835');
            $ProCuandoCompra = $request->request->get('23591325');
            $ProComuna = $request->request->get('23590877');
            $ProPie = $request->request->get('23590834');

            $dflSi = $request->request->get('23590879_1');
            $dflNo = $request->request->get('23590879_2');

            $nombreConyuge = $request->request->get('23590820');
            $rutConyuge = $request->request->get('23590971');
            $sueldoConyuge = $request->request->get('23590971');
            $situacionLaboralConyuge = $request->request->get('23591016');




            $dfl = '';
            if($dflSi == 'SI'){
            $dfl = 'SI';
            }

            if($dflNo == 'NO'){
            $dfl = 'NO';
            }

            $estado = $request->request->get('23590871');
            $proyecto = $request->request->get('23590865');



            $sueldoLiquido = $request->request->get('23590902');
            $situacionLaboral = $request->request->get('23590926');
            $nombreEmpleador = '';
            $rutEmpleador = '';
            $direccionEmpresa  = '';
            $comunaEmpresa  = '';

            $urlUltimoMes = $request->request->get('23590826');
            $urlMes2 = $request->request->get('23590827');
            $urlMes3 = $request->request->get('23590828');
            $urlCarnet = $request->request->get('23590825');
            $urlAfp = $request->request->get('23590829');
            $urlOtros1 = $request->request->get('23590831');
            $urlOtros2 = $request->request->get('23590830');
            $urlDeclarAnual = '';


            }




            if($token == $tokenPost or $token == $tokenPost2 or $tokenPost3 == $token){

                function rut( $rut ) {
                    return number_format( substr ( $rut, 0 , -1 ) , 0, "", ".") . '-' . substr ( $rut, strlen($rut) -1 , 1 );
                }




        if($Ejecutivo){

            $Ejecutivo->setNombre($ejecutivoNombre);
            $Ejecutivo->setNombre($request);
            $Ejecutivo->setEmail($ejecutivoEmail);
            $Ejecutivo->setTelefono($ejecutivoTelefono);
            $Ejecutivo->setAliansas($Aliansas);
            $em->persist($Ejecutivo);
            $em->flush($Ejecutivo);

            
             }else{
            
            $Ejecutivo = new Ejecutivos();
            $Ejecutivo->setNombre($ejecutivoNombre);
            $Ejecutivo->setNombre($request);
            $Ejecutivo->setEmail($ejecutivoEmail);
            $Ejecutivo->setTelefono($ejecutivoTelefono);
            $Ejecutivo->setAliansas($Aliansas);
            $em->persist($Ejecutivo);
            $em->flush($Ejecutivo);

        }

        $idEjecutivo = $Ejecutivo->getId();

        $Ejecutivo = $em->getRepository('BenchUsuariosBundle:Ejecutivos')->findOneBy(array('id' => $idEjecutivo));

                $rut2 = str_replace(".", "",  $rutUsuario);
                $rut2 = str_replace(".", "",  $rut2);
                $rut2 = str_replace(".", "",  $rut2);
                $rut2 = str_replace(".", "",  $rut2);
                $rut2 = str_replace("-", "",  $rut2);



                $clienteRut = rut($rut2);



        $rut5 =  substr($rutUsuario ,0, 4);
        $nombre5 = substr($nombreUsuario  , 0 , 4);
        $contrasena = $rut5.$nombre5;

        function codificar($string, $key) {
            $result = '';
            for ($i = 0; $i < strlen($string); $i++) {
                $char = substr($string, $i, 1);
                $keychar = substr($key, ($i % strlen($key)) - 1, 1);
                $char = chr(ord($char) + ord($keychar));
                $result.=$char;
            }
            return base64_encode($result);
        }



        $contrasena = codificar($contrasena, "5512858");






       $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $clienteRut));


       if($usuario){


           $respuesta = array  ('Respuesta' => 'Usuario existe' );



       }else{

           $usuario = new Usuario();
           $usuario->setNombre($nombreUsuario);
           $usuario->setEmail($emailUsuario);
           $usuario->setProfesion($profesion);
           $usuario->setTelefono($telefUsuario);
           $usuario->setNacionalidad($nacionalidad);
           $usuario->setContrasena($contrasena);
           $usuario->setRut($clienteRut);
           $usuario->setEjecutivos($Ejecutivo);
           $usuario->setUniversidad($universidad);
           $usuario->setFechaingreso((new \DateTime()));

           $usuario->setFinal('SI');
           $usuario->setArchivar('no');



           $em->persist($usuario);
           $em->flush($usuario);
            $idUser = $usuario->getId();

           $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $idUser));
          


      if($dondeEs  == 'ES' or $Inversionista  == 'Inversionista'){

            

            $Bienesraices = new Bienesraices();
            $Bienesraices->setAvaluo(   $total1);
            $Bienesraices->setUsuario($usuario);
            $Bienesraices->setFecha(new \DateTime());
            $em->persist($Bienesraices);
            $em->flush($Bienesraices);

            $Automoviles = new Automoviles();
            $Automoviles->setValorcomercial($total2);
            $Automoviles->setUsuario($usuario);
            $Automoviles->setFecha(new \DateTime());
            $em->persist($Automoviles);
            $em->flush($Automoviles);

            
            $Ahorroinverson = new Ahorroinverson();
            $Ahorroinverson->setValorcomercial($total3);
            $Ahorroinverson->setFecha(new \DateTime());
            $Ahorroinverson->setUsuario($usuario);
            $em->persist($Ahorroinverson);
            $em->flush($Ahorroinverson);
     



           }

    





           $debesHipotecario = new Debeshipotecario();
           $debesHipotecario->setUsuario($usuario);
           $debesHipotecario->setPagomensual($debesCredito);
           $debesHipotecario->setFecha(new \DateTime());
           $em->persist($debesHipotecario);
           $em->flush($debesHipotecario);
           


           $debesConsumo = new DebesCreditoConsumo();
           $debesConsumo->setUsuario($usuario);
           $debesConsumo->setPagomensual($debesCredito);
           $debesConsumo->setFecha(new \DateTime());
           $em->persist($debesConsumo);
           $em->flush($debesConsumo);


           

           $debesTarjeta = new DebesTarjetaCredito();
           $debesTarjeta->setMontoaprobado($debesLinea);
           $debesTarjeta->setUsuario($usuario);
           $debesTarjeta->setFecha(new \DateTime());
           $em->persist($debesConsumo);
           $em->flush($debesConsumo);






       }
      


        /*  Propiedad   */

       if(  $tipoAprobacion1 ){

        $tipoAprobacion =   $tipoAprobacion1;


       }else{

        $tipoAprobacion =   $tipoAprobacion2;

       }
        $CreditoHipotecario = new CreditoHipotecario();
        $CreditoHipotecario->setValorPropiedad($ProValor);
        $CreditoHipotecario->setPlazoCredito($ProPlazo);
        $CreditoHipotecario->setTipoPropiedad($ProTipo);
        $CreditoHipotecario->setCuandoDeseaComprar($ProCuandoCompra);
        $CreditoHipotecario->setCuandoDeseaComprar($ProComuna);
        $CreditoHipotecario->setMontoPie($ProPie);
        $CreditoHipotecario->setDfl2($dfl);
        $CreditoHipotecario->setProyecto($proyecto);
        $CreditoHipotecario->setEstadoPropiedad($estado);
        $CreditoHipotecario->setUsuario($usuario);
        $CreditoHipotecario->setTipoSolicitud($tipoAprobacion);
        $CreditoHipotecario->setFecha(new \DateTime());

        $em->persist($CreditoHipotecario);
        $em->flush($CreditoHipotecario);


        $Antecedenteslaboral = new Antecedenteslaboral();
 

        $Antecedenteslaboral->setIndependientesueldoliquido($sueldoLiquido);
        $Antecedenteslaboral->setSituacionlaboral($situacionLaboral);
        $Antecedenteslaboral->setEmpleadorempresa($nombreEmpleador);
        $Antecedenteslaboral->setDireccion($direccionEmpresa);
        $Antecedenteslaboral->setComuna($comunaEmpresa);

         
        $em->persist($Antecedenteslaboral);
        $em->flush($Antecedenteslaboral);

        /* fin de antecedentes laboral */


        /* Conyuge */



        $Conyuge = new Conyuge();


        $rut2 = str_replace(".", "",  $rutConyuge);
        $rut2 = str_replace(".", "",  $rut2);
        $rut2 = str_replace(".", "",  $rut2);
        $rut2 = str_replace(".", "",  $rut2);
        $rut2 = str_replace("-", "",  $rut2);

        $rutConyuge = rut($rut2);

        $Conyuge->setNombre($nombreConyuge);
        $Conyuge->setRut($rutConyuge);
        $Conyuge->setRentaliquidad($sueldoConyuge);
        $Conyuge->setSituacionLaboral($situacionLaboralConyuge);
        $Conyuge->setUsuario($usuario);
        $Conyuge->setFecha(new \DateTime());
        $em->persist($Conyuge);
        $em->flush($Conyuge);

        /* fin de conyuge */


        /* Archivos */


        /* archivos */

        $archivo = new Archivos();

        $tipo ='Liquidaciones de sueldo - Último Mes';

        $archivo->setEstado('activo');
        $archivo->setFecha(new \DateTime());
        $archivo->setUrl($urlUltimoMes);
        $archivo->setNombreArchivo('');
        $archivo->setTipo($tipo);
        $archivo->setUsuario($usuario);
        $em->persist($archivo);
        $em->flush($archivo);



        $archivo = new Archivos();

        $tipo ='Liquidación Sueldo - Mes 2';


        $archivo->setEstado('activo');
        $archivo->setFecha(new \DateTime());
        $archivo->setUrl( $urlMes2);
        $archivo->setNombreArchivo('');
        $archivo->setTipo($tipo);
        $archivo->setUsuario($usuario);
        $em->persist($archivo);
        $em->flush($archivo);



        $archivo = new Archivos();

        $tipo ='Liquidación Sueldo - Mes 3';


        $archivo->setEstado('activo');
        $archivo->setFecha(new \DateTime());
        $archivo->setUrl($urlMes3);
        $archivo->setNombreArchivo('');
        $archivo->setTipo($tipo);
        $archivo->setUsuario($usuario);
        $em->persist($archivo);
        $em->flush($archivo);



        $archivo = new Archivos();

        $tipo ='Fotocopia Carnet - Cliente';

        $archivo->setEstado('activo');
        $archivo->setFecha(new \DateTime());
        $archivo->setUrl($urlCarnet);
        $archivo->setNombreArchivo('');
        $archivo->setTipo($tipo);
        $archivo->setUsuario($usuario);
        $em->persist($archivo);
        $em->flush($archivo);



        $archivo = new Archivos();

        $tipo ='Certificado AFP';

        $archivo->setEstado('activo');
        $archivo->setFecha(new \DateTime());
        $archivo->setUrl($urlAfp);
        $archivo->setNombreArchivo('');
        $archivo->setTipo($tipo);
        $archivo->setUsuario($usuario);
        $em->persist($archivo);
        $em->flush($archivo);


        $archivo = new Archivos();

        $tipo ='Otros 1';

        $archivo->setEstado('activo');
        $archivo->setFecha(new \DateTime());
        $archivo->setUrl($urlOtros1);
        $archivo->setNombreArchivo('');
        $archivo->setTipo($tipo);
        $archivo->setUsuario($usuario);
        $em->persist($archivo);
        $em->flush($archivo);


        $archivo = new Archivos();

        $tipo ='Otros 2';

        $archivo->setEstado('activo');
        $archivo->setFecha(new \DateTime());
        $archivo->setUrl($urlOtros2);
        $archivo->setNombreArchivo('');
        $archivo->setTipo($tipo);
        $archivo->setUsuario($usuario);
        $em->persist($archivo);
        $em->flush($archivo);
        
     

      
       $archivo = new Archivos();

        $tipo ='Declaración Anual de Impuestos';

        $archivo->setEstado('activo');
        $archivo->setFecha(new \DateTime());
        $archivo->setUrl( $urlDeclarAnual);
        $archivo->setNombreArchivo('');
        $archivo->setTipo($tipo);
        $archivo->setUsuario($usuario);
        $em->persist($archivo);
        $em->flush($archivo);






        $serializer = $this->container->get('jms_serializer');



        /* creacion de ticket zendesk */

        $NombreCompleto =  $usuario->getNombre() . ' ' . $usuario->getApellido();
        $ejecutivoTelfono  = $Ejecutivo->getTelefono();
        $usuarioId = $usuario->getId();


        $liquidacionUltimoMes = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Liquidaciones de sueldo - Último Mes'  ));
        $liquidacionMes2 = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Liquidación Sueldo - Mes 2'  ));
        $liquidacionMes3 = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Liquidación Sueldo - Mes 3'  ));
        $fotocopiaCarnet = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Fotocopia Carnet - Cliente'  ));
        $certificadoAfp = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Certificado AFP'  ));
        $Otros1 = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Otros 1'  ));
        $Otros2 = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Otros 2'  ));
        $urlDeclarAnual = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Declaración Anual de Impuestos'  ));


if($Inversionista =='Inversionista'){
    $donde = $Inversionista;

}

if($dondeEs == 'ES'){

     $donde = $dondeEs;

}

if($donde == 'inmobiliarias'){

$donde = $donde;

}
 $montoCredito =  $ProValor - $ProPie;

$porcentaje =  $montoCredito  /  $ProValor;

        $asuntoZend = 'Nuevo Cliente '.$nombreUsuario.' - Crédito Hipotecario - '.$ProValor.' - '.$donde.'';

        $detalle ='


Nombre Ejecutivo Remax: '.$ejecutivoNombre.'

Email Ejecutivo Remax: '.$ejecutivoEmail.'

Telefono Ejecutivo Remax: '.$ejecutivoTelfono.'

Solicitud de crédito 


Rut Cliente: '.$clienteRut.'

Nombre y Apellido Cliente: '. $nombreUsuario .'


Telefono Cliente:  '.$telefUsuario.'

Email Cliente: '. $emailUsuario .'

Sueldo Liquido Cliente:

Valor Propiedad - UF:  '.  $ProValor .'

Plazo de Crédito:  '.$ProPlazo .'

Monto PIE - UF: '.   $ProPie .'

¿Es la propiedad DFL2?:

Tipo de Propiedad:   '.  $ProTipo .'

Estado Propiedad:  '.    $estado  .'

Comuna Propiedad: '. $ProComuna .'

¿Cuándo comprará la propiedad?:

Nombre Proyecto / Inmobiliaria: '.  $proyecto .'

Ver Reportes

https://www.benchbanking.com/adminBench/pCrearPdf/'.$usuarioId.'
https://www.benchbanking.com/banco/reporte/bbva/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/bci/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/consorcio/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/metlife/'.$usuarioId.'/





Archivos

Liquidación ultimo mes

'.$liquidacionUltimoMes->getUrl().'

Liquidación ultimo mes 2

'.$liquidacionMes2->getUrl().'


Liquidación ultimo mes 3

'.$liquidacionMes3->getUrl().'

Fotocopia carnet

'.$fotocopiaCarnet->getUrl().'


Certificado Afp

'.$certificadoAfp->getUrl().'

Otros 1

'.$Otros1->getUrl().'

Otros 2


'.$Otros2->getUrl().'

Declaración Anual de Impuestos

'.$urlDeclarAnual->getUrl().'



              ';


        $create = json_encode(
            array(
                'ticket' => array(
                    'requester' => array(
                        'name' => $NombreCompleto,
                        'email' => $usuario->getEmail()
                         ,
                    ),
                    'group_id' => '22',

                    'subject' => $asuntoZend,
                    'custom_fields' =>
                        array(
                            '23412128' => $clienteRut,
                            '23412138' => $NombreCompleto,
                            '23808956' => $ejecutivoNombre,
                            '23784888' => $ejecutivoEmail,
                            '23784898' => $ejecutivoTelefono,
                            '23723488' => '',
                            '23057889' => $porcentaje,
                            '23723498' => 'Hipotecario',

                            '23750746' => '1000000'),
                    'description' => $detalle
                )
            ), JSON_FORCE_OBJECT
        );

        $zendDesk = new zendesk();
        $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");






        return new Response("guardado");

    



}


    }

    public function postUserAliansasNuevoRemaxAction(Request $request)
        {



            function rut($rut_param){

                $parte4 = substr($rut_param, -1); // seria solo el numero verificador
                $parte3 = substr($rut_param, -4,3); // la cuenta va de derecha a izq
                $parte2 = substr($rut_param, -7,3);
                $parte1 = substr($rut_param, 0,-8); //de esta manera toma todos los caracteres desde el 8 hacia la izq

                return $parte1.".".$parte2.".".$parte3."-".$parte4;

            }






            /* array json
           { "ejecutivoNombre": " " , "ejecutivoEmail": "" ,"ejecutivoTelefono": " "  ,  "clienteTelefono": "", "clienteRut" : "", "clieteSueldo" : "" , "clienteNombreApellido" : "",  "clienteEmail" : ""  , "propiedadValor" : "" ,  "propiedadMonto" : "", "propiedadTipo" : "" ,  "propiedadComuna" : "" ,  "propiedadProyecto" : "" ,  "propiedadPlazo" : "",  "propiedadDfl2Si" : "", "propiedadDfl2No" : "", "propiedadEstado" :"" ; }
             */
            $em = $this->getDoctrine()->getManager();


            $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');



            if($token == $token){


               $tipoCredito = 'Hipotecario';
               $ejecutivoNombre = $request->request->get('23456146');
               $ejecutivoEmail = $request->request->get('23455943');
               $ejecutivoTelefono = $request->request->get('23455945');
               $clienteTelefono = $request->request->get('23455882');
               $clienteRut = $request->request->get('23455872');

                $clienteRut = rut($clienteRut);

               $clieteSueldo  = $request->request->get('23455917');
               $clienteNombreApellido = $request->request->get('23455878');
               $clienteEmail = $request->request->get('23455896');
               $propiedadValor = $request->request->get('23594511');

               $propiedadTipo = $request->request->get('23594515');
               $propiedadComuna = $request->request->get('23594518');
               $propiedadProyecto = $request->request->get('23594519');
               $propiedadPlazo = $request->request->get('23594513');
               $propiedadDfl2Si = $request->request->get('23594514');
               $propiedadDfl2No  = $request->request->get('23594514');
               $propiedadEstado  = $request->request->get('23594516');
                $propiedadMonto = $request->request->get('23594512');
               $rut5 =  substr($clienteRut,0, 4);
               $nombre5 = substr($clienteNombreApellido , 0 , 4);
               $contrasena = $rut5.$nombre5;



               $origin = $request->request->get('origin');

               $Aliansas = $em->getRepository('BenchUsuariosBundle:Aliansas')->findOneBy(array ('nombre' => 'remax' ));
               $Ejecutivo = $em->getRepository('BenchUsuariosBundle:Ejecutivos')->findOneBy(array('email' => $ejecutivoEmail));

               if($Ejecutivo){

                   $Ejecutivo->setAliansas($Aliansas);
                   $Ejecutivo->setNombre( $ejecutivoNombre);
                   $Ejecutivo->setEmail($ejecutivoEmail);
                   $Ejecutivo->setTelefono($ejecutivoTelefono);
                   $Ejecutivo->setFechaCreacion(new \DateTime());
                   $em->persist($Ejecutivo);
                   $em->persist($Aliansas);
                   $em->flush();

               }else{


               $Ejecutivo = new Ejecutivos();
               $Ejecutivo->setAliansas($Aliansas);
               $Ejecutivo->setNombre( $ejecutivoNombre);
               $Ejecutivo->setEmail($ejecutivoEmail);
               $Ejecutivo->setTelefono($ejecutivoTelefono);
               $Ejecutivo->setFechaCreacion(new \DateTime());
               $em->persist($Ejecutivo);
               $em->persist($Aliansas);
               $em->flush();


               }


                function codificar($string, $key) {
                    $result = '';
                    for ($i = 0; $i < strlen($string); $i++) {
                        $char = substr($string, $i, 1);
                        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
                        $char = chr(ord($char) + ord($keychar));
                        $result.=$char;
                    }
                    return base64_encode($result);
                }



                $contrasena = codificar($contrasena, "5512858");



                $idEjecutivo = $Ejecutivo->getId();
                $Ejecutivo = $em->getRepository('BenchUsuariosBundle:Ejecutivos')->findOneBy(array('id' => $idEjecutivo ));
                $rutP = substr($clienteRut , 4);
                $nomP = substr( $clienteNombreApellido, 4);
                $contrasena = $rutP.$nomP;
                $Usuario = new Usuario();
                $Usuario->setNombre($clienteNombreApellido);
                $Usuario->setTelefono( $clienteTelefono);
                $Usuario->setRut($clienteRut);
                $Usuario->setEmail($clienteEmail);
                $Usuario->setEjecutivos($Ejecutivo);
                $Usuario->setContrasena($contrasena);



                $em->persist($Ejecutivo);

                $em->persist($Usuario);
                $em->flush();

                $usuarioId = $Usuario->getId();

                $Usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' => $usuarioId  ));

                $CreditoHipotecario = new CreditoHipotecario();
                $CreditoHipotecario->setMontoCredito($propiedadValor);
                $CreditoHipotecario->setValorPropiedad( $propiedadMonto);
                $CreditoHipotecario->setTipoPropiedad( $propiedadTipo);
                $CreditoHipotecario->setComunaComprar( $propiedadComuna);
                $CreditoHipotecario->setProyecto($propiedadProyecto);
                $CreditoHipotecario->setEstadoPropiedad( $propiedadEstado);
                $CreditoHipotecario->setUsuario($Usuario);
                $CreditoHipotecario->setFecha(new \DateTime());


                $em->persist($CreditoHipotecario);
                $em->persist($Usuario);
                $em->flush();



                /* archivos */

                $archivo = new Archivos();

                $tipo ='Liquidaciones de sueldo - Último Mes';
                $url = $request->request->get('23455947');
                $archivo->setEstado('activo');
                $archivo->setFecha(new \DateTime());
                $archivo->setUrl($url);
                $archivo->setNombreArchivo('');
                $archivo->setTipo($tipo);
                $archivo->setUsuario($Usuario);
                $em->persist($archivo);
                $em->flush();



                $archivo = new Archivos();

                $tipo ='Liquidación Sueldo - Mes 2';
                $url = $request->request->get('23456102');
                $archivo->setEstado('activo');
                $archivo->setFecha(new \DateTime());
                $archivo->setUrl($url);
                $archivo->setNombreArchivo('');
                $archivo->setTipo($tipo);
                $archivo->setUsuario($Usuario);
                $em->persist($archivo);
                $em->flush();



                $archivo = new Archivos();

                $tipo ='Liquidación Sueldo - Mes 3';
                $url = $request->request->get('23463605');
                $archivo->setEstado('activo');
                $archivo->setFecha(new \DateTime());
                $archivo->setUrl($url);
                $archivo->setNombreArchivo('');
                $archivo->setTipo($tipo);
                $archivo->setUsuario($Usuario);
                $em->persist($archivo);
                $em->flush();



                $archivo = new Archivos();

                $tipo ='Fotocopia Carnet - Cliente';
                $url = $request->request->get('23456043');
                $archivo->setEstado('activo');
                $archivo->setFecha(new \DateTime());
                $archivo->setUrl($url);
                $archivo->setNombreArchivo('');
                $archivo->setTipo($tipo);
                $archivo->setUsuario($Usuario);
                $em->persist($archivo);
                $em->flush();



                $archivo = new Archivos();

                $tipo ='Certificado AFP';
                $url = $request->request->get('23456042');
                $archivo->setEstado('activo');
                $archivo->setFecha(new \DateTime());
                $archivo->setUrl($url);
                $archivo->setNombreArchivo('');
                $archivo->setTipo($tipo);
                $archivo->setUsuario($Usuario);
                $em->persist($archivo);
                $em->flush();


                $archivo = new Archivos();

                $tipo ='Otros 1';
                $url = $request->request->get('23488550');
                $archivo->setEstado('activo');
                $archivo->setFecha(new \DateTime());
                $archivo->setUrl($url);
                $archivo->setNombreArchivo('');
                $archivo->setTipo($tipo);
                $archivo->setUsuario($Usuario);
                $em->persist($archivo);
                $em->flush();


                $archivo = new Archivos();

                $tipo ='Otros 2';
                $url = $request->request->get('23488553');
                $archivo->setEstado('activo');
                $archivo->setFecha(new \DateTime());
                $archivo->setUrl($url);
                $archivo->setNombreArchivo('');
                $archivo->setTipo($tipo);
                $archivo->setUsuario($Usuario);
                $em->persist($archivo);
                $em->flush();






                $serializer = $this->container->get('jms_serializer');
                $respuesta = 'Datos Guardados';
                $respuesta = array  ('Respuesta' => '200' );



            }else{


                $serializer = $this->container->get('jms_serializer');
                $respuesta = 'Datos no Guardados';
                $respuesta = array  ('Respuesta' => '100');
                return  $serializer->serialize( $respuesta, 'json');



            }

            $NombreCompleto =  $Usuario->getNombre() . ' ' . $Usuario->getApellido();
            $ejecutivoTelfono  = $Ejecutivo->getTelefono();
            $usuarioId = $Usuario->getId();

            $liquidacionUltimoMes = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Liquidaciones de sueldo - Último Mes'  ));
            $liquidacionMes2 = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Liquidación Sueldo - Mes 2'  ));
            $liquidacionMes3 = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Liquidación Sueldo - Mes 3'  ));
            $fotocopiaCarnet = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Fotocopia Carnet - Cliente'  ));
            $certificadoAfp = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Certificado AFP'  ));
            $Otros1 = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Otros 1'  ));
            $Otros2 = $em->getRepository('BenchFilepickerBundle:Archivos')->findOneBy(array('usuario' => $usuarioId , 'tipo' => 'Otros 2'  ));





            $asuntoZend = ' Nuevo Cliente REMAX *** TEST ';

              $detalle ='


  Nombre Ejecutivo Remax: '.$ejecutivoNombre.'

Email Ejecutivo Remax: '.$ejecutivoEmail.'

Telefono Ejecutivo Remax: '.$ejecutivoTelfono.'

Solicitud de crédito REMAX: Solicitud de crédito REMAX

Rut Cliente: '.$clienteRut.'

Nombre y Apellido Cliente: '.$clienteNombreApellido.'


Telefono Cliente:  '.$clienteTelefono.'

Email Cliente: '.$clienteEmail.'

Sueldo Liquido Cliente:

Valor Propiedad - UF:  '.$propiedadValor.'

Plazo de Crédito:  '.$propiedadPlazo.'

Monto PIE - UF: '.$propiedadValor.'

¿Es la propiedad DFL2?:

Tipo de Propiedad:   '.$propiedadTipo.'

Estado Propiedad:  '.$propiedadEstado.'

Comuna Propiedad: '.$propiedadComuna.'

¿Cuándo comprará la propiedad?:

Nombre Proyecto / Inmobiliaria: '. $propiedadProyecto.'

Ver Reporte

http://test.benchbanking.com/test2/sym/2.3.8/web/banco/reporte/bbva/'.$usuarioId.'/

Archivos

Liquidación ultimo mes

http://'.$liquidacionUltimoMes->getUrl().'

Liquidación ultimo mes 2

http://'.$liquidacionMes2->getUrl().'


Liquidación ultimo mes 3

http://'.$liquidacionMes3->getUrl().'

Fotocopia carnet

http://'.$fotocopiaCarnet->getUrl().'


Certificado Afp

http://'.$certificadoAfp->getUrl().'

Otros 1

http://'.$Otros1->getUrl().'

Otros 2


http://'.$Otros2->getUrl().'



              ';


            $create = json_encode(
                array(
                    'ticket' => array(
                        'requester' => array(
                            'name' => $NombreCompleto,
                            'email' => 'giorgosbarkos@gmail.com',
                        ),
                        'group_id' => '22',

                        'subject' => $asuntoZend,
                        'custom_fields' =>
                            array(
                                '23412128' => $clienteRut,
                                '23412138' => $NombreCompleto,
                                '23723488' => '',
                                '23723498' => $tipoCredito,

                                '23750746' => '1000000'),
                        'description' => $detalle
                    )
                ), JSON_FORCE_OBJECT
            );

            $zendDesk = new zendesk();
            $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");




      var_dump( $data);









        }












    /**
     * Presents the form to use to create a new page.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\View(
     *  templateVar = "form"
     * )
     *
     * @return FormTypeInterface
     */
    public function newPageAction()
    {
        return $this->createForm(new PageType());
    }






    /**
     *
     *
     * @param mixed $id
     *
     * @return UsuarioInterface
     *
     * @throws NotFoundHttpException
     */

    protected function getOr404($id)
    {
        if (!($usuario = $this->container->get('bench_api.usuario.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('el Usuario \'%s\' no fue encontrado.',$id));
        }

        return $usuario;
    }





} 