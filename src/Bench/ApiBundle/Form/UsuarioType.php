<?php
/**
 * Created by PhpStorm.
 * User: giorgosbarkos
 * Date: 27-03-14
 * Time: 16:18
 */

namespace Bench\ApiBundle\Form;
use Bench\CreditosBundle\Entity\CreditoHipotecario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zend\XmlRpc\Response;


class  UsuarioType extends  AbstractType {


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder
            ->add('rut')
            ->add('nombre')
        ->add('telefono')

        ->add('creditoHipotecario', 'collection', array(
                'type' => new creditoHipotecario(),
                'allow_add' => true,
                'prototype' => true,
                // Post update
                'by_reference' => false,

    ))


            ->add('save', 'submit')
            ->add('saveAndAdd', 'submit');
        ;




    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return '';
    }
}