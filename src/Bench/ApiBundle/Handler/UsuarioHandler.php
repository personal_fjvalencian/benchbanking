<?

namespace Bench\ApiBundle\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Bench\ApiBundle\Exception\InvalidFormException;
use Bench\ApiBundle\Form\UsuarioType;
use Bench\ApiBundle\Model\UsuarioInterface;



class UsuarioHandler implements UsuarioHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get a Page.
     *
     * @param mixed $id
     *
     * @return  UsuarioInterface
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     *  lista todos los usuario.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0)
    {
        return $this->repository->findBy(array(), null, $limit, $offset);
    }

    /**
     * Crear un nuevo Usuario.
     *
     * @param array $parameters
     *
     * @return  UsuarioInterface
     */
    public function post(array $parameters)
    {
        $usuario = $this->createUsuario();

        return $this->processForm( $usuario, $parameters, 'POST');
    }


    /**
     * .
     *
     * @param  UsuarioInterface $usuario
     * @param array         $parameters
     *
     * @return  UsuarioInterface
     */
    public function put(UsuarioInterface $usuario, array $parameters)
    {
        return $this->processForm($usuario, $parameters, 'PUT');
    }

    /**
     * Partially update a Page.
     *
     * @param  UsuarioInterface $usuario
     * @param array         $parameters
     *
     * @return  UsuarioInterface
     */

    public function patch( UsuarioInterface $usuario, array $parameters)
    {
        return $this->processForm($usuario, $parameters, 'PATCH');
    }

    /**
     * Processes the form.
     *
     * @param PageInterface $usuario
     * @param array         $parameters
     * @param String        $method
     *
     * @return  UsuarioInterface
     *
     * @throws \Bench\ApiBundle\Exception\InvalidFormException
     */
    private function processForm(UsuarioInterface $usuario, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new UsuarioType(), $usuario, array('method' => $method ,'csrf_protection' => false));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $usuario = $form->getData();
            $this->om->persist($usuario);
            $this->om->flush($usuario);

            return $usuario;

        }


        throw new InvalidFormException('Invalid submitted data', $form);
    }

    private function createUsuario()
    {
        return new $this->entityClass();
    }

}

?>
