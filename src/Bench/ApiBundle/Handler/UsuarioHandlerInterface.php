<?php

namespace Bench\ApiBundle\Handler;

use Bench\ApiBundle\Model\UsuarioInterface;

interface UsuarioHandlerInterface
{
    /**
     * Get a Page given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return  UsuarioInterface
     */


    public function get($id);

    /**
     * Get a list of Usuario.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */


    public function all($limit = 5, $offset = 0);

    /**
     * Post Page, creates a new Page.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return  UsuarioInterface
     */
    public function post(array $parameters);

    /**
     * Edit a Page.
     *
     * @api
     *
     * @param  UsuarioInterface   $usuario
     * @param array           $parameters
     *
     * @return PageInterface
     */
    public function put(UsuarioInterface $usuario, array $parameters);

    /**
     * Partially update a Page.
     *
     * @api
     *
     * @param  UsuarioInterface   $usuario
     * @param array           $parameters
     *
     * @return UsuarioInterface
     */
    public function patch(UsuarioInterface $usuario, array $parameters);
}