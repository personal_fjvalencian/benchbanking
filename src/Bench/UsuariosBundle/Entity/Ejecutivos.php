<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ejecutivos
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Ejecutivos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100 , nullable=true)
     */

    private $nombre;



    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=100 , nullable=true)
     */


    private $telefono;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100 , nullable=true)
     */


    
    private $email;






    /**
     *
     *@var \DateTime
     *
     * @ORM\Column(name="fechaCreacion", type="date"  , nullable=true)
     */


    private $fechaCreacion;



    /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Aliansas", inversedBy="ejecutivos")
     * @ORM\JoinColumn(name="aliansas_id", referencedColumnName="id")
     */


    private $aliansas;


    /**
     * @ORM\OneToMany(targetEntity="Bench\UsuariosBundle\Entity\Usuario" , mappedBy="ejecutivos")
     */



    private $usuario;




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuario = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Ejecutivos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Ejecutivos
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Ejecutivos
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return Ejecutivos
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    
        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set aliansas
     *
     * @param \Bench\UsuariosBundle\Entity\Aliansas $aliansas
     * @return Ejecutivos
     */
    public function setAliansas(\Bench\UsuariosBundle\Entity\Aliansas $aliansas = null)
    {
        $this->aliansas = $aliansas;
    
        return $this;
    }

    /**
     * Get aliansas
     *
     * @return \Bench\UsuariosBundle\Entity\Aliansas 
     */
    public function getAliansas()
    {
        return $this->aliansas;
    }

    /**
     * Add usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Ejecutivos
     */
    public function addUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario)
    {
        $this->usuario[] = $usuario;
    
        return $this;
    }

    /**
     * Remove usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     */
    public function removeUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario)
    {
        $this->usuario->removeElement($usuario);
    }

    /**
     * Get usuario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}