<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conyuge
 *
 * @ORM\Table(name="Conyuge")
 * @ORM\Entity
 */
class Conyuge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rut", type="string", length=100)
     */
    private $rut;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100 , nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=100 ,  nullable=true)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoseg", type="string", length=100 ,  nullable=true)
     */
    private $apellidoseg;

    /**
     * @var string
     *
     * @ORM\Column(name="fechanacimiento", type="string", length=100 ,  nullable=true)
     */
    private $fechanacimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="nacionalidad", type="string", length=100 ,  nullable=true)
     */
    private $nacionalidad;

    /**
     * @var string
     *
     * @ORM\Column(name="niveleducacion", type="string", length=100 , nullable=true)
     */
    private $niveleducacion;

    /**
     * @var string
     *
     * @ORM\Column(name="profesion", type="string", length=100 ,  nullable=true)
     */
    private $profesion;

    /**
     * @var string
     *
     * @ORM\Column(name="universidad", type="string", length=100 ,  nullable=true)
     */
    private $universidad;

    /**
     * @var string
     *
     * @ORM\Column(name="actividad", type="string", length=100 ,  nullable=true)
     */
    private $actividad;

    /**
     * @var string
     *
     * @ORM\Column(name="cargoactual", type="string", length=100 , nullable=true)
     */
    private $cargoactual;

    /**
     * @var string
     *
     * @ORM\Column(name="antiguedadlaboral", type="string", length=100 ,  nullable=true)
     */
    private $antiguedadlaboral;

    /**
     * @var string
     *
     * @ORM\Column(name="rentaliquidad", type="string", length=100 ,  nullable=true)
     */
    private $rentaliquidad;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa", type="string", length=100 ,  nullable=true)
     */
    private $empresa;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=100 ,  nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="situacionLaboral", type="string", length=100 ,  nullable=true)
     */

    private $situacionLaboral;




    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime" , nullable=true)
     */
    private $fecha;

      /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="conyuge")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    private $usuario;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return Conyuge
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    
        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Conyuge
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Conyuge
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    
        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set apellidoseg
     *
     * @param string $apellidoseg
     * @return Conyuge
     */
    public function setApellidoseg($apellidoseg)
    {
        $this->apellidoseg = $apellidoseg;
    
        return $this;
    }

    /**
     * Get apellidoseg
     *
     * @return string 
     */
    public function getApellidoseg()
    {
        return $this->apellidoseg;
    }

    /**
     * Set fechanacimiento
     *
     * @param string $fechanacimiento
     * @return Conyuge
     */
    public function setFechanacimiento($fechanacimiento)
    {
        $this->fechanacimiento = $fechanacimiento;
    
        return $this;
    }

    /**
     * Get fechanacimiento
     *
     * @return string 
     */
    public function getFechanacimiento()
    {
        return $this->fechanacimiento;
    }

    /**
     * Set nacionalidad
     *
     * @param string $nacionalidad
     * @return Conyuge
     */
    public function setNacionalidad($nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;
    
        return $this;
    }

    /**
     * Get nacionalidad
     *
     * @return string 
     */
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    /**
     * Set niveleducacion
     *
     * @param string $niveleducacion
     * @return Conyuge
     */
    public function setNiveleducacion($niveleducacion)
    {
        $this->niveleducacion = $niveleducacion;
    
        return $this;
    }

    /**
     * Get niveleducacion
     *
     * @return string 
     */
    public function getNiveleducacion()
    {
        return $this->niveleducacion;
    }

    /**
     * Set profesion
     *
     * @param string $profesion
     * @return Conyuge
     */
    public function setProfesion($profesion)
    {
        $this->profesion = $profesion;
    
        return $this;
    }

    /**
     * Get profesion
     *
     * @return string 
     */
    public function getProfesion()
    {
        return $this->profesion;
    }

    /**
     * Set universidad
     *
     * @param string $universidad
     * @return Conyuge
     */
    public function setUniversidad($universidad)
    {
        $this->universidad = $universidad;
    
        return $this;
    }

    /**
     * Get universidad
     *
     * @return string 
     */
    public function getUniversidad()
    {
        return $this->universidad;
    }

    /**
     * Set actividad
     *
     * @param string $actividad
     * @return Conyuge
     */
    public function setActividad($actividad)
    {
        $this->actividad = $actividad;
    
        return $this;
    }

    /**
     * Get actividad
     *
     * @return string 
     */
    public function getActividad()
    {
        return $this->actividad;
    }

    /**
     * Set cargoactual
     *
     * @param string $cargoactual
     * @return Conyuge
     */
    public function setCargoactual($cargoactual)
    {
        $this->cargoactual = $cargoactual;
    
        return $this;
    }

    /**
     * Get cargoactual
     *
     * @return string 
     */
    public function getCargoactual()
    {
        return $this->cargoactual;
    }

    /**
     * Set antiguedadlaboral
     *
     * @param string $antiguedadlaboral
     * @return Conyuge
     */
    public function setAntiguedadlaboral($antiguedadlaboral)
    {
        $this->antiguedadlaboral = $antiguedadlaboral;
    
        return $this;
    }

    /**
     * Get antiguedadlaboral
     *
     * @return string 
     */
    public function getAntiguedadlaboral()
    {
        return $this->antiguedadlaboral;
    }

    /**
     * Set rentaliquidad
     *
     * @param string $rentaliquidad
     * @return Conyuge
     */
    public function setRentaliquidad($rentaliquidad)
    {
        $this->rentaliquidad = $rentaliquidad;
    
        return $this;
    }

    /**
     * Get rentaliquidad
     *
     * @return string 
     */
    public function getRentaliquidad()
    {
        return $this->rentaliquidad;
    }

    /**
     * Set empresa
     *
     * @param string $empresa
     * @return Conyuge
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    
        return $this;
    }

    /**
     * Get empresa
     *
     * @return string 
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Conyuge
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set situacionLaboral
     *
     * @param string $situacionLaboral
     * @return Conyuge
     */
    public function setSituacionLaboral($situacionLaboral)
    {
        $this->situacionLaboral = $situacionLaboral;
    
        return $this;
    }

    /**
     * Get situacionLaboral
     *
     * @return string 
     */
    public function getSituacionLaboral()
    {
        return $this->situacionLaboral;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Conyuge
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Conyuge
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}