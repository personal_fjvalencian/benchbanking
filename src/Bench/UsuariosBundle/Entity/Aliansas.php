<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Aliansas
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Aliansas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;




    /**
     * @var string
     *
     * @ORM\Column( type="string", length=100 , nullable=true)
     */


    private $nombre;



    /**
     * @ORM\OneToMany(targetEntity="Bench\UsuariosBundle\Entity\Ejecutivos" , mappedBy="aliansas")
     */

    private $ejecutivos;




    /**
     * @var string
     *
     * @ORM\Column( type="datetime", length=100 , nullable=true)
     */

    private $createDate;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ejecutivos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Aliansas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     * @return Aliansas
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
    
        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime 
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Add ejecutivos
     *
     * @param \Bench\UsuariosBundle\Entity\Ejecutivos $ejecutivos
     * @return Aliansas
     */
    public function addEjecutivo(\Bench\UsuariosBundle\Entity\Ejecutivos $ejecutivos)
    {
        $this->ejecutivos[] = $ejecutivos;
    
        return $this;
    }

    /**
     * Remove ejecutivos
     *
     * @param \Bench\UsuariosBundle\Entity\Ejecutivos $ejecutivos
     */
    public function removeEjecutivo(\Bench\UsuariosBundle\Entity\Ejecutivos $ejecutivos)
    {
        $this->ejecutivos->removeElement($ejecutivos);
    }

    /**
     * Get ejecutivos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEjecutivos()
    {
        return $this->ejecutivos;
    }
}