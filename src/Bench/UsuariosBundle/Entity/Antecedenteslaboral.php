<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Antecedenteslaboral
 *
 * @ORM\Table(name="Antecedenteslaboral")
 * @ORM\Entity
 */
class Antecedenteslaboral
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="situacionlaboral", type="string", length=100  , nullable=true)
     */
    private $situacionlaboral;





    /**
     * @var string
     *
     * @ORM\Column(name="empleadorempresa", type="string", length=100 , nullable=true)
     */
    private $empleadorempresa;

    /**
     * @var string
     *
     * @ORM\Column(name="industria", type="string", length=100 , nullable=true)
     */
    private $industria;

    /**
     * @var string
     *
     * @ORM\Column(name="fechaingreso", type="string", length=100 , nullable=true)
     */
    private $fechaingreso;

    /**
     * @var string
     *
     * @ORM\Column(name="cargoactual", type="string", length=100 , nullable=true)
     */
    private $cargoactual;

    /**
     * @var string
     *
     * @ORM\Column(name="rutempresa", type="string", length=100 , nullable=true)
     */
    private $rutempresa;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=100 , nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=100 , nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="comuna", type="string", length=100 , nullable=true)
     */
    private $comuna;

    /**
     * @var string
     *
     * @ORM\Column(name="ciudad", type="string", length=100 , nullable=true)
     */
    private $ciudad;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=50 , nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoActividad", type="string", length=100 , nullable=true)
     */
    private $tipoactividad;

    /**
     * @var string
     *
     * @ORM\Column(name="tiposueldo", type="string", length=100 , nullable=true)
     */
    private $tiposueldo;

    /**
     * @var string
     *
     * @ORM\Column(name="rutEmpleador", type="string", length=100 ,  nullable=true)
     */


    private $rutEmpleador;


        /**
     * @var string
     *
     * @ORM\Column(name="sueldofijo", type="string", length=100 ,  nullable=true)
     */
    private $sueldofijo;

    /**
     * @var string
     *
     * @ORM\Column(name="rentavariable", type="string", length=100 , nullable=true)
     */
    private $rentavariable;

    /**
     * @var string
     *
     * @ORM\Column(name="independienteSueldoLiquido", type="string", length=100 , nullable=true)
     */
    private $independientesueldoliquido;




    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaultima", type="datetime" , nullable=true)
     */
    private $fechaultima;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechacambio", type="datetime" , nullable=true)
     */
    private $fechacambio;

    
     /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="antecedenteslaboral")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    
  
    private $usuario;


   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set situacionlaboral
     *
     * @param string $situacionlaboral
     * @return Antecedenteslaboral
     */
    public function setSituacionlaboral($situacionlaboral)
    {
        $this->situacionlaboral = $situacionlaboral;
    
        return $this;
    }

    /**
     * Get situacionlaboral
     *
     * @return string 
     */
    public function getSituacionlaboral()
    {
        return $this->situacionlaboral;
    }

    /**
     * Set empleadorempresa
     *
     * @param string $empleadorempresa
     * @return Antecedenteslaboral
     */
    public function setEmpleadorempresa($empleadorempresa)
    {
        $this->empleadorempresa = $empleadorempresa;
    
        return $this;
    }

    /**
     * Get empleadorempresa
     *
     * @return string 
     */
    public function getEmpleadorempresa()
    {
        return $this->empleadorempresa;
    }

    /**
     * Set industria
     *
     * @param string $industria
     * @return Antecedenteslaboral
     */
    public function setIndustria($industria)
    {
        $this->industria = $industria;
    
        return $this;
    }

    /**
     * Get industria
     *
     * @return string 
     */
    public function getIndustria()
    {
        return $this->industria;
    }

    /**
     * Set fechaingreso
     *
     * @param string $fechaingreso
     * @return Antecedenteslaboral
     */
    public function setFechaingreso($fechaingreso)
    {
        $this->fechaingreso = $fechaingreso;
    
        return $this;
    }

    /**
     * Get fechaingreso
     *
     * @return string 
     */
    public function getFechaingreso()
    {
        return $this->fechaingreso;
    }

    /**
     * Set cargoactual
     *
     * @param string $cargoactual
     * @return Antecedenteslaboral
     */
    public function setCargoactual($cargoactual)
    {
        $this->cargoactual = $cargoactual;
    
        return $this;
    }

    /**
     * Get cargoactual
     *
     * @return string 
     */
    public function getCargoactual()
    {
        return $this->cargoactual;
    }

    /**
     * Set rutempresa
     *
     * @param string $rutempresa
     * @return Antecedenteslaboral
     */
    public function setRutempresa($rutempresa)
    {
        $this->rutempresa = $rutempresa;
    
        return $this;
    }

    /**
     * Get rutempresa
     *
     * @return string 
     */
    public function getRutempresa()
    {
        return $this->rutempresa;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Antecedenteslaboral
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Antecedenteslaboral
     */
    public function setRegion($region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set comuna
     *
     * @param string $comuna
     * @return Antecedenteslaboral
     */
    public function setComuna($comuna)
    {
        $this->comuna = $comuna;
    
        return $this;
    }

    /**
     * Get comuna
     *
     * @return string 
     */
    public function getComuna()
    {
        return $this->comuna;
    }

    /**
     * Set ciudad
     *
     * @param string $ciudad
     * @return Antecedenteslaboral
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    
        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string 
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Antecedenteslaboral
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set tipoactividad
     *
     * @param string $tipoactividad
     * @return Antecedenteslaboral
     */
    public function setTipoactividad($tipoactividad)
    {
        $this->tipoactividad = $tipoactividad;
    
        return $this;
    }

    /**
     * Get tipoactividad
     *
     * @return string 
     */
    public function getTipoactividad()
    {
        return $this->tipoactividad;
    }

    /**
     * Set tiposueldo
     *
     * @param string $tiposueldo
     * @return Antecedenteslaboral
     */
    public function setTiposueldo($tiposueldo)
    {
        $this->tiposueldo = $tiposueldo;
    
        return $this;
    }

    /**
     * Get tiposueldo
     *
     * @return string 
     */
    public function getTiposueldo()
    {
        return $this->tiposueldo;
    }

    /**
     * Set rutEmpleador
     *
     * @param string $rutEmpleador
     * @return Antecedenteslaboral
     */
    public function setRutEmpleador($rutEmpleador)
    {
        $this->rutEmpleador = $rutEmpleador;
    
        return $this;
    }

    /**
     * Get rutEmpleador
     *
     * @return string 
     */
    public function getRutEmpleador()
    {
        return $this->rutEmpleador;
    }

    /**
     * Set sueldofijo
     *
     * @param string $sueldofijo
     * @return Antecedenteslaboral
     */
    public function setSueldofijo($sueldofijo)
    {
        $this->sueldofijo = $sueldofijo;
    
        return $this;
    }

    /**
     * Get sueldofijo
     *
     * @return string 
     */
    public function getSueldofijo()
    {
        return $this->sueldofijo;
    }

    /**
     * Set rentavariable
     *
     * @param string $rentavariable
     * @return Antecedenteslaboral
     */
    public function setRentavariable($rentavariable)
    {
        $this->rentavariable = $rentavariable;
    
        return $this;
    }

    /**
     * Get rentavariable
     *
     * @return string 
     */
    public function getRentavariable()
    {
        return $this->rentavariable;
    }

    /**
     * Set independientesueldoliquido
     *
     * @param string $independientesueldoliquido
     * @return Antecedenteslaboral
     */
    public function setIndependientesueldoliquido($independientesueldoliquido)
    {
        $this->independientesueldoliquido = $independientesueldoliquido;
    
        return $this;
    }

    /**
     * Get independientesueldoliquido
     *
     * @return string 
     */
    public function getIndependientesueldoliquido()
    {
        return $this->independientesueldoliquido;
    }

    /**
     * Set fechaultima
     *
     * @param \DateTime $fechaultima
     * @return Antecedenteslaboral
     */
    public function setFechaultima($fechaultima)
    {
        $this->fechaultima = $fechaultima;
    
        return $this;
    }

    /**
     * Get fechaultima
     *
     * @return \DateTime 
     */
    public function getFechaultima()
    {
        return $this->fechaultima;
    }

    /**
     * Set fechacambio
     *
     * @param \DateTime $fechacambio
     * @return Antecedenteslaboral
     */
    public function setFechacambio($fechacambio)
    {
        $this->fechacambio = $fechacambio;
    
        return $this;
    }

    /**
     * Get fechacambio
     *
     * @return \DateTime 
     */
    public function getFechacambio()
    {
        return $this->fechacambio;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Antecedenteslaboral
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}