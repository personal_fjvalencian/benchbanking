<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Donde
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Donde
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


/**
     * @var string
     *
     * @ORM\Column(name="institucion", type="string", length=100 , nullable=true)
     */
    
    
    private $institucion;
    
    
 
    /**
     * @var string
     *
     * @ORM\Column(name="dividendo", type="string", length=100 , nullable=true)
     */
    
    
  
    private $dividendo;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="cae", type="string", length=100 , nullable=true)
     */
    
    private $cae;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="costoFinalCredito", type="string", length=100 , nullable=true)
     */
    
    
    private $costoFinalCredito;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="rango", type="string", length=100 , nullable=true)
     */
    
    
    private $rango;
    
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="valoracion", type="string", length=100 , nullable=true)
     */
    
    private $valoracion;
    
    
    
    
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return Donde
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set dividendo
     *
     * @param string $dividendo
     * @return Donde
     */
    public function setDividendo($dividendo)
    {
        $this->dividendo = $dividendo;
    
        return $this;
    }

    /**
     * Get dividendo
     *
     * @return string 
     */
    public function getDividendo()
    {
        return $this->dividendo;
    }

    /**
     * Set cae
     *
     * @param string $cae
     * @return Donde
     */
    public function setCae($cae)
    {
        $this->cae = $cae;
    
        return $this;
    }

    /**
     * Get cae
     *
     * @return string 
     */
    public function getCae()
    {
        return $this->cae;
    }

    /**
     * Set costoFinalCredito
     *
     * @param string $costoFinalCredito
     * @return Donde
     */
    public function setCostoFinalCredito($costoFinalCredito)
    {
        $this->costoFinalCredito = $costoFinalCredito;
    
        return $this;
    }

    /**
     * Get costoFinalCredito
     *
     * @return string 
     */
    public function getCostoFinalCredito()
    {
        return $this->costoFinalCredito;
    }

    /**
     * Set rango
     *
     * @param string $rango
     * @return Donde
     */
    public function setRango($rango)
    {
        $this->rango = $rango;
    
        return $this;
    }

    /**
     * Get rango
     *
     * @return string 
     */
    public function getRango()
    {
        return $this->rango;
    }

    /**
     * Set valoracion
     *
     * @param string $valoracion
     * @return Donde
     */
    public function setValoracion($valoracion)
    {
        $this->valoracion = $valoracion;
    
        return $this;
    }

    /**
     * Get valoracion
     *
     * @return string 
     */
    public function getValoracion()
    {
        return $this->valoracion;
    }
}