<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


use Bench\ApiBundle\Model\UsuarioInterface;

/**
 * Usuario
 *
 * @ORM\Table(name="Usuario")
 * @ORM\Entity
 */


class Usuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;




    /**
     * @var string
     *
     * @ORM\Column(name="rut", type="string", length=100 , nullable=true , unique=true)
     */
    private $rut;


    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="text", nullable=true)
     */
    private $salt;


    /**
     * @var string
     *
     * @ORM\Column(name="activado", type="text", nullable=true)
     */

    private $activado;



    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=100 , nullable=true)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidoseg", type="string", length=100 , nullable=true)
     */
    private $apellidoseg;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100 , nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="contrasena", type="string", length=100 , nullable=true)
     */
    private $contrasena;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", length=100 , nullable=true)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(name="nacionalidad", type="string", length=100 , nullable=true)
     */
    private $nacionalidad;

    /**
     * @var string
     *
     * @ORM\Column(name="fechaNacimiento", type="string", length=100 , nullable=true)
     */
    private $fechanacimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="estadocivil", type="string", length=100 , nullable=true)
     */
    private $estadocivil;

    /**
     * @var string
     *
     * @ORM\Column(name="niveleducacion", type="string", length=100 , nullable=true)
     */
    private $niveleducacion;

    /**
     * @var string
     *
     * @ORM\Column(name="profesion", type="string", length=100 , nullable=true)
     */
    private $profesion;

    /**
     * @var string
     *
     * @ORM\Column(name="universidad", type="string", length=100 , nullable=true)
     */
    private $universidad;

    /**
     * @var string
     *
     * @ORM\Column(name="tipocasa", type="string", length=100 , nullable=true)
     */
    private $tipocasa;

    /**
     * @var string
     *
     * @ORM\Column(name="ndependientes", type="string", length=100 , nullable=true)
     */
    private $ndependientes;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=100 , nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=100 , nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="comuna", type="string", length=100 , nullable=true)
     */
    private $comuna;

    /**
     * @var string
     *
     * @ORM\Column(name="montoArriendo", type="string", length=100 , nullable=true)
     */
    private $montoarriendo;

    
    /**
     * 
     *@var \DateTime
     * 
     * @ORM\Column(name="fechaIngreso", type="date"  , nullable=true)
     */
    private $fechaingreso;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=100 ,nullable=true)
     */
    
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="final", type="string", length=100 ,nullable=true)
     */
    private $final;

    /**
     * @var string
     *
     * @ORM\Column(name="asignar", type="string", length=100 , nullable=true)
     */
    private $asignar;

    /**
     * @var string
     *
     * @ORM\Column(name="navegador", type="string", length=100 , nullable=true)
     */
    private $navegador;

    /**
     * @var string
     *
     * @ORM\Column(name="archivar", type="string", length=100 , nullable=true)
     */
    private $archivar;

    /**
     * @var string
     *
     * @ORM\Column(name="dicom", type="string", length=100 , nullable=true)
     */
    private $dicom;

  
    
     /**
     * @var string
     *
     * @ORM\Column(name="autorizoDicomSiNo", type="string", length=100 , nullable=true)
     */
    
    private $autorizoDicomSiNo;
    
    
    
      /**
     * @var string
     *
     * @ORM\Column(name="vecesLogin", type="string", length=100 , nullable=true)
     */
    
    
    
    private $veceslogin;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="tienesBienesRaicesSiNo", type="string", length=10 , nullable=true)
     */
    
    
    private $tienesBienesRaicesSiNo;
    
     /**
     * @var string
     *
     * @ORM\Column(name="tienesAhorroInversionSiNo", type="string", length=10 , nullable=true)
     */
    
    
    private $tienesAhorroInversionSiNo;
    
    
      /**
     * @var string
     *
     * @ORM\Column(name="tienesAutomovilesSiNo", type="string", length=10 , nullable=true)
     */
    
    
    
    private $tienesAutomovilesSiNo;
    
    
       /**
     * @var string
     *
     * @ORM\Column(name="tienesSociedadesSiNo", type="string", length=10 , nullable=true)
     */
    
    private $tienesSociedadesSiNo;
    
    
    
       /**
     * @var string
     *
     * @ORM\Column(name="debesHipotecarioSiNo", type="string", length=10 , nullable=true)
     */
    
    
    private $debesHipotecarioSiNo;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="debesTarjetaCreditoSiNo", type="string", length=10 , nullable=true)
     */
    
    
    
    private $debesTarjetaCreditoSiNo;
    
    
         /**
     * @var string
     *
     * @ORM\Column(name="debesLineaCreditoSiNo", type="string", length=10 , nullable=true)
     */
    
    
    
    
    private $debesLineaCreditoSiNo;
    
    
           /**
     * @var string
     *
     * @ORM\Column(name="debesCreditoConsumoSiNo", type="string", length=10 , nullable=true)
     */
    
    
    
    private $debesCreditoConsumoSiNo;
    


    /**
     * @ORM\OneToMany(targetEntity="Bench\UsuariosBundle\Entity\Antecedenteslaboral", mappedBy="usuario")
     */
    
      protected $antecedenteslaboral;
      
      
       /**
     * @ORM\OneToMany(targetEntity="Bench\UsuariosBundle\Entity\Conyuge", mappedBy="usuario")
     */
      
      protected $conyuge;
      
      
 
      
      
       /**
     * @ORM\OneToMany(targetEntity="Bench\TienesBundle\Entity\Bienesraices", mappedBy="usuario")
     */
      
      protected  $bienesraices;
      
      
      
     /**
     * @ORM\OneToMany(targetEntity="Bench\TienesBundle\Entity\Ahorroinverson", mappedBy="usuario")
     */
      
      
      
      protected $ahorroinverson;
      
      
       /**
     * @ORM\OneToMany(targetEntity="Bench\TienesBundle\Entity\Automoviles", mappedBy="usuario")
     */
      
      
      protected $automoviles;
      
  
      
      
         /**
     * @ORM\OneToMany(targetEntity="Bench\TienesBundle\Entity\Sociedades", mappedBy="usuario")
     */
      
      protected $sociedades;
      
      
     
      
      
        /**
     * @ORM\OneToMany(targetEntity="Bench\DebesBundle\Entity\Debeshipotecario", mappedBy="usuario")
     */
      
      protected $debeshipotecario;
      
      
      
        /**
     * @ORM\OneToMany(targetEntity="Bench\DebesBundle\Entity\DebesTarjetaCredito", mappedBy="usuario")
     */
      
      
      protected $debestarjetacredito;
      
   
         /**
     * @ORM\OneToMany(targetEntity="Bench\DebesBundle\Entity\DebesLineaCredito", mappedBy="usuario")
     */
      
      protected $debeslineacredito;
    
      
      
         /**
     * @ORM\OneToMany(targetEntity="Bench\DebesBundle\Entity\DebesCreditoConsumo", mappedBy="usuario")
     */
      
      
      protected  $debescreditoconsumo;
      


      /**
     * @ORM\OneToMany(targetEntity="Bench\CreditosBundle\Entity\CreditoConsumo", mappedBy="usuario")
     */
      
      protected $creditoconsumo;
      
      
      /**
     * @ORM\OneToMany(targetEntity="Bench\CreditosBundle\Entity\CreditoAutomotriz", mappedBy="usuario")
     */
      
      
      protected $creditoautomotriz;
      
         /**
     * @ORM\OneToMany(targetEntity="Bench\CreditosBundle\Entity\CreditoHipotecario", mappedBy="usuario" , cascade={"persist", "remove"})
     */
  
    protected $creditohipotecario;
    
    
    
     /**
     * @ORM\OneToMany(targetEntity="Bench\CreditosBundle\Entity\CreditoConsolidacion", mappedBy="usuario")
     */
    
    protected $creditoconsolidacion;
    
    
      /**
     * @ORM\OneToMany(targetEntity="Bench\CreditosBundle\Entity\CreditoCuenta", mappedBy="usuario")
     */
    
  

   protected $creditocuenta;
   
   
     /**
     * @ORM\OneToMany(targetEntity="Bench\SolicitudesCreditosBundle\Entity\SolicitudesCreditos", mappedBy="usuario")
     */
    
   
   protected $solicitudescreditos;
   
   
   
     /**
     * @ORM\OneToMany(targetEntity="Bench\UsuariosBundle\Entity\autorizoDicom", mappedBy="usuario")
     */
   
   
   
   protected $autorizodicom;
   
   
   
   
    /**
     * @ORM\OneToMany(targetEntity="Bench\FilepickerBundle\Entity\Archivos", mappedBy="usuario")
    */
   
    
      protected $archivos;
      
      
      
      
          /**
     * @ORM\OneToMany(targetEntity="Bench\UsuariosBundle\Entity\Cotizaciones", mappedBy="usuario")
    */
   
    
      protected $cotizaciones;
      
      
      
      
      /**
     * @ORM\OneToMany(targetEntity="Bench\UsuariosBundle\Entity\FechaActualizacion", mappedBy="usuario")
    */
      
      
   
        protected $fechaactualizacion;
      
      
      
      
      /**
     * @var string
     *
     * @ORM\Column(name="tienearchivo", type="string", length=100 , nullable=true)
     */
      
      
      protected $tienearchivo;
      
     
      
      
          /**
     * @var string
     *
     * @ORM\Column(name="fechaactualizacionFinal", type="string", length=100 , nullable=true)
     */
      
      
      protected $fechaactualizacionFinal;


    /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Ejecutivos", inversedBy="usuario")
     * @ORM\JoinColumn(name="ejecutivos_id", referencedColumnName="id")
     */
      
      
      protected $ejecutivos;



    /**
     * @var string
     *
     * @ORM\Column(name="totalActivos", type="string", length=100 , nullable=true)
     */


      protected $totalActivos;


    /**
     * @var string
     *
     * @ORM\Column(name="totalPasivos", type="string", length=100 , nullable=true)
     */


      protected  $totalPasivos;


    /**
     * @var string
     *
     * @ORM\Column(name="pagomensualHipo", type="string", length=100 , nullable=true)
     */



    protected $pagomensualHipo;

    /**
     * @var string
     *
     * @ORM\Column(name="pagomensualCredito", type="string", length=100 , nullable=true)
     */



     protected $pagomensualCredito;


    /**
     * @var string
     *
     * @ORM\Column(name="pagomensualLineaTarjeta", type="string", length=100 , nullable=true)
     */




    protected $pagomensualLineaTarjeta;

    /**
     * @var string
     *
     * @ORM\Column(name="totalPropiedades", type="string", length=100 , nullable=true)
     */





    protected $totalPropiedades;


    /**
     * @var string
     *
     * @ORM\Column(name="totalAutomoviles", type="string", length=100 , nullable=true)
     */


    protected $totalAutomoviles;


    /**
     * @var string
     *
     * @ORM\Column(name="totalAhorroInversion", type="string", length=100 , nullable=true)
     */


    protected $totalAhorroInversion;


        /**
     * @var string
     *
     * @ORM\Column(name="nuevoPassword", type="string", length=100 , nullable=true)
     */

    protected $nuevoPassword;


     /**
     * @var string
     *
     * @ORM\Column(name="estadoPassword", type="string", length=100 , nullable=true)
     */

    protected $estadoPassword;



     /**
     * @var string
     *
     * @ORM\Column(name="errorLogin", type="string", length=100 , nullable=true)
     */


    protected $errorLogin;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->antecedenteslaboral = new \Doctrine\Common\Collections\ArrayCollection();
        $this->conyuge = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bienesraices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ahorroinverson = new \Doctrine\Common\Collections\ArrayCollection();
        $this->automoviles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sociedades = new \Doctrine\Common\Collections\ArrayCollection();
        $this->debeshipotecario = new \Doctrine\Common\Collections\ArrayCollection();
        $this->debestarjetacredito = new \Doctrine\Common\Collections\ArrayCollection();
        $this->debeslineacredito = new \Doctrine\Common\Collections\ArrayCollection();
        $this->debescreditoconsumo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creditoconsumo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creditoautomotriz = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creditohipotecario = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creditoconsolidacion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creditocuenta = new \Doctrine\Common\Collections\ArrayCollection();
        $this->solicitudescreditos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->autorizodicom = new \Doctrine\Common\Collections\ArrayCollection();
        $this->archivos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cotizaciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fechaactualizacion = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return Usuario
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    
        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Usuario
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set activado
     *
     * @param string $activado
     * @return Usuario
     */
    public function setActivado($activado)
    {
        $this->activado = $activado;
    
        return $this;
    }

    /**
     * Get activado
     *
     * @return string 
     */
    public function getActivado()
    {
        return $this->activado;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Usuario
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    
        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set apellidoseg
     *
     * @param string $apellidoseg
     * @return Usuario
     */
    public function setApellidoseg($apellidoseg)
    {
        $this->apellidoseg = $apellidoseg;
    
        return $this;
    }

    /**
     * Get apellidoseg
     *
     * @return string 
     */
    public function getApellidoseg()
    {
        return $this->apellidoseg;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set contrasena
     *
     * @param string $contrasena
     * @return Usuario
     */
    public function setContrasena($contrasena)
    {
        $this->contrasena = $contrasena;
    
        return $this;
    }

    /**
     * Get contrasena
     *
     * @return string 
     */
    public function getContrasena()
    {
        return $this->contrasena;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     * @return Usuario
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    
        return $this;
    }

    /**
     * Get sexo
     *
     * @return string 
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set nacionalidad
     *
     * @param string $nacionalidad
     * @return Usuario
     */
    public function setNacionalidad($nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;
    
        return $this;
    }

    /**
     * Get nacionalidad
     *
     * @return string 
     */
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    /**
     * Set fechanacimiento
     *
     * @param string $fechanacimiento
     * @return Usuario
     */
    public function setFechanacimiento($fechanacimiento)
    {
        $this->fechanacimiento = $fechanacimiento;
    
        return $this;
    }

    /**
     * Get fechanacimiento
     *
     * @return string 
     */
    public function getFechanacimiento()
    {
        return $this->fechanacimiento;
    }

    /**
     * Set estadocivil
     *
     * @param string $estadocivil
     * @return Usuario
     */
    public function setEstadocivil($estadocivil)
    {
        $this->estadocivil = $estadocivil;
    
        return $this;
    }

    /**
     * Get estadocivil
     *
     * @return string 
     */
    public function getEstadocivil()
    {
        return $this->estadocivil;
    }

    /**
     * Set niveleducacion
     *
     * @param string $niveleducacion
     * @return Usuario
     */
    public function setNiveleducacion($niveleducacion)
    {
        $this->niveleducacion = $niveleducacion;
    
        return $this;
    }

    /**
     * Get niveleducacion
     *
     * @return string 
     */
    public function getNiveleducacion()
    {
        return $this->niveleducacion;
    }

    /**
     * Set profesion
     *
     * @param string $profesion
     * @return Usuario
     */
    public function setProfesion($profesion)
    {
        $this->profesion = $profesion;
    
        return $this;
    }

    /**
     * Get profesion
     *
     * @return string 
     */
    public function getProfesion()
    {
        return $this->profesion;
    }

    /**
     * Set universidad
     *
     * @param string $universidad
     * @return Usuario
     */
    public function setUniversidad($universidad)
    {
        $this->universidad = $universidad;
    
        return $this;
    }

    /**
     * Get universidad
     *
     * @return string 
     */
    public function getUniversidad()
    {
        return $this->universidad;
    }

    /**
     * Set tipocasa
     *
     * @param string $tipocasa
     * @return Usuario
     */
    public function setTipocasa($tipocasa)
    {
        $this->tipocasa = $tipocasa;
    
        return $this;
    }

    /**
     * Get tipocasa
     *
     * @return string 
     */
    public function getTipocasa()
    {
        return $this->tipocasa;
    }

    /**
     * Set ndependientes
     *
     * @param string $ndependientes
     * @return Usuario
     */
    public function setNdependientes($ndependientes)
    {
        $this->ndependientes = $ndependientes;
    
        return $this;
    }

    /**
     * Get ndependientes
     *
     * @return string 
     */
    public function getNdependientes()
    {
        return $this->ndependientes;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Usuario
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Usuario
     */
    public function setRegion($region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set comuna
     *
     * @param string $comuna
     * @return Usuario
     */
    public function setComuna($comuna)
    {
        $this->comuna = $comuna;
    
        return $this;
    }

    /**
     * Get comuna
     *
     * @return string 
     */
    public function getComuna()
    {
        return $this->comuna;
    }

    /**
     * Set montoarriendo
     *
     * @param string $montoarriendo
     * @return Usuario
     */
    public function setMontoarriendo($montoarriendo)
    {
        $this->montoarriendo = $montoarriendo;
    
        return $this;
    }

    /**
     * Get montoarriendo
     *
     * @return string 
     */
    public function getMontoarriendo()
    {
        return $this->montoarriendo;
    }

    /**
     * Set fechaingreso
     *
     * @param \DateTime $fechaingreso
     * @return Usuario
     */
    public function setFechaingreso($fechaingreso)
    {
        $this->fechaingreso = $fechaingreso;
    
        return $this;
    }

    /**
     * Get fechaingreso
     *
     * @return \DateTime 
     */
    public function getFechaingreso()
    {
        return $this->fechaingreso;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Usuario
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set final
     *
     * @param string $final
     * @return Usuario
     */
    public function setFinal($final)
    {
        $this->final = $final;
    
        return $this;
    }

    /**
     * Get final
     *
     * @return string 
     */
    public function getFinal()
    {
        return $this->final;
    }

    /**
     * Set asignar
     *
     * @param string $asignar
     * @return Usuario
     */
    public function setAsignar($asignar)
    {
        $this->asignar = $asignar;
    
        return $this;
    }

    /**
     * Get asignar
     *
     * @return string 
     */
    public function getAsignar()
    {
        return $this->asignar;
    }

    /**
     * Set navegador
     *
     * @param string $navegador
     * @return Usuario
     */
    public function setNavegador($navegador)
    {
        $this->navegador = $navegador;
    
        return $this;
    }

    /**
     * Get navegador
     *
     * @return string 
     */
    public function getNavegador()
    {
        return $this->navegador;
    }

    /**
     * Set archivar
     *
     * @param string $archivar
     * @return Usuario
     */
    public function setArchivar($archivar)
    {
        $this->archivar = $archivar;
    
        return $this;
    }

    /**
     * Get archivar
     *
     * @return string 
     */
    public function getArchivar()
    {
        return $this->archivar;
    }

    /**
     * Set dicom
     *
     * @param string $dicom
     * @return Usuario
     */
    public function setDicom($dicom)
    {
        $this->dicom = $dicom;
    
        return $this;
    }

    /**
     * Get dicom
     *
     * @return string 
     */
    public function getDicom()
    {
        return $this->dicom;
    }

    /**
     * Set autorizoDicomSiNo
     *
     * @param string $autorizoDicomSiNo
     * @return Usuario
     */
    public function setAutorizoDicomSiNo($autorizoDicomSiNo)
    {
        $this->autorizoDicomSiNo = $autorizoDicomSiNo;
    
        return $this;
    }

    /**
     * Get autorizoDicomSiNo
     *
     * @return string 
     */
    public function getAutorizoDicomSiNo()
    {
        return $this->autorizoDicomSiNo;
    }

    /**
     * Set veceslogin
     *
     * @param string $veceslogin
     * @return Usuario
     */
    public function setVeceslogin($veceslogin)
    {
        $this->veceslogin = $veceslogin;
    
        return $this;
    }

    /**
     * Get veceslogin
     *
     * @return string 
     */
    public function getVeceslogin()
    {
        return $this->veceslogin;
    }

    /**
     * Set tienesBienesRaicesSiNo
     *
     * @param string $tienesBienesRaicesSiNo
     * @return Usuario
     */
    public function setTienesBienesRaicesSiNo($tienesBienesRaicesSiNo)
    {
        $this->tienesBienesRaicesSiNo = $tienesBienesRaicesSiNo;
    
        return $this;
    }

    /**
     * Get tienesBienesRaicesSiNo
     *
     * @return string 
     */
    public function getTienesBienesRaicesSiNo()
    {
        return $this->tienesBienesRaicesSiNo;
    }

    /**
     * Set tienesAhorroInversionSiNo
     *
     * @param string $tienesAhorroInversionSiNo
     * @return Usuario
     */
    public function setTienesAhorroInversionSiNo($tienesAhorroInversionSiNo)
    {
        $this->tienesAhorroInversionSiNo = $tienesAhorroInversionSiNo;
    
        return $this;
    }

    /**
     * Get tienesAhorroInversionSiNo
     *
     * @return string 
     */
    public function getTienesAhorroInversionSiNo()
    {
        return $this->tienesAhorroInversionSiNo;
    }

    /**
     * Set tienesAutomovilesSiNo
     *
     * @param string $tienesAutomovilesSiNo
     * @return Usuario
     */
    public function setTienesAutomovilesSiNo($tienesAutomovilesSiNo)
    {
        $this->tienesAutomovilesSiNo = $tienesAutomovilesSiNo;
    
        return $this;
    }

    /**
     * Get tienesAutomovilesSiNo
     *
     * @return string 
     */
    public function getTienesAutomovilesSiNo()
    {
        return $this->tienesAutomovilesSiNo;
    }

    /**
     * Set tienesSociedadesSiNo
     *
     * @param string $tienesSociedadesSiNo
     * @return Usuario
     */
    public function setTienesSociedadesSiNo($tienesSociedadesSiNo)
    {
        $this->tienesSociedadesSiNo = $tienesSociedadesSiNo;
    
        return $this;
    }

    /**
     * Get tienesSociedadesSiNo
     *
     * @return string 
     */
    public function getTienesSociedadesSiNo()
    {
        return $this->tienesSociedadesSiNo;
    }

    /**
     * Set debesHipotecarioSiNo
     *
     * @param string $debesHipotecarioSiNo
     * @return Usuario
     */
    public function setDebesHipotecarioSiNo($debesHipotecarioSiNo)
    {
        $this->debesHipotecarioSiNo = $debesHipotecarioSiNo;
    
        return $this;
    }

    /**
     * Get debesHipotecarioSiNo
     *
     * @return string 
     */
    public function getDebesHipotecarioSiNo()
    {
        return $this->debesHipotecarioSiNo;
    }

    /**
     * Set debesTarjetaCreditoSiNo
     *
     * @param string $debesTarjetaCreditoSiNo
     * @return Usuario
     */
    public function setDebesTarjetaCreditoSiNo($debesTarjetaCreditoSiNo)
    {
        $this->debesTarjetaCreditoSiNo = $debesTarjetaCreditoSiNo;
    
        return $this;
    }

    /**
     * Get debesTarjetaCreditoSiNo
     *
     * @return string 
     */
    public function getDebesTarjetaCreditoSiNo()
    {
        return $this->debesTarjetaCreditoSiNo;
    }

    /**
     * Set debesLineaCreditoSiNo
     *
     * @param string $debesLineaCreditoSiNo
     * @return Usuario
     */
    public function setDebesLineaCreditoSiNo($debesLineaCreditoSiNo)
    {
        $this->debesLineaCreditoSiNo = $debesLineaCreditoSiNo;
    
        return $this;
    }

    /**
     * Get debesLineaCreditoSiNo
     *
     * @return string 
     */
    public function getDebesLineaCreditoSiNo()
    {
        return $this->debesLineaCreditoSiNo;
    }

    /**
     * Set debesCreditoConsumoSiNo
     *
     * @param string $debesCreditoConsumoSiNo
     * @return Usuario
     */
    public function setDebesCreditoConsumoSiNo($debesCreditoConsumoSiNo)
    {
        $this->debesCreditoConsumoSiNo = $debesCreditoConsumoSiNo;
    
        return $this;
    }

    /**
     * Get debesCreditoConsumoSiNo
     *
     * @return string 
     */
    public function getDebesCreditoConsumoSiNo()
    {
        return $this->debesCreditoConsumoSiNo;
    }

    /**
     * Set tienearchivo
     *
     * @param string $tienearchivo
     * @return Usuario
     */
    public function setTienearchivo($tienearchivo)
    {
        $this->tienearchivo = $tienearchivo;
    
        return $this;
    }

    /**
     * Get tienearchivo
     *
     * @return string 
     */
    public function getTienearchivo()
    {
        return $this->tienearchivo;
    }

    /**
     * Set fechaactualizacionFinal
     *
     * @param string $fechaactualizacionFinal
     * @return Usuario
     */
    public function setFechaactualizacionFinal($fechaactualizacionFinal)
    {
        $this->fechaactualizacionFinal = $fechaactualizacionFinal;
    
        return $this;
    }

    /**
     * Get fechaactualizacionFinal
     *
     * @return string 
     */
    public function getFechaactualizacionFinal()
    {
        return $this->fechaactualizacionFinal;
    }

    /**
     * Set totalActivos
     *
     * @param string $totalActivos
     * @return Usuario
     */
    public function setTotalActivos($totalActivos)
    {
        $this->totalActivos = $totalActivos;
    
        return $this;
    }

    /**
     * Get totalActivos
     *
     * @return string 
     */
    public function getTotalActivos()
    {
        return $this->totalActivos;
    }

    /**
     * Set totalPasivos
     *
     * @param string $totalPasivos
     * @return Usuario
     */
    public function setTotalPasivos($totalPasivos)
    {
        $this->totalPasivos = $totalPasivos;
    
        return $this;
    }

    /**
     * Get totalPasivos
     *
     * @return string 
     */
    public function getTotalPasivos()
    {
        return $this->totalPasivos;
    }

    /**
     * Set pagomensualHipo
     *
     * @param string $pagomensualHipo
     * @return Usuario
     */
    public function setPagomensualHipo($pagomensualHipo)
    {
        $this->pagomensualHipo = $pagomensualHipo;
    
        return $this;
    }

    /**
     * Get pagomensualHipo
     *
     * @return string 
     */
    public function getPagomensualHipo()
    {
        return $this->pagomensualHipo;
    }

    /**
     * Set pagomensualCredito
     *
     * @param string $pagomensualCredito
     * @return Usuario
     */
    public function setPagomensualCredito($pagomensualCredito)
    {
        $this->pagomensualCredito = $pagomensualCredito;
    
        return $this;
    }

    /**
     * Get pagomensualCredito
     *
     * @return string 
     */
    public function getPagomensualCredito()
    {
        return $this->pagomensualCredito;
    }

    /**
     * Set pagomensualLineaTarjeta
     *
     * @param string $pagomensualLineaTarjeta
     * @return Usuario
     */
    public function setPagomensualLineaTarjeta($pagomensualLineaTarjeta)
    {
        $this->pagomensualLineaTarjeta = $pagomensualLineaTarjeta;
    
        return $this;
    }

    /**
     * Get pagomensualLineaTarjeta
     *
     * @return string 
     */
    public function getPagomensualLineaTarjeta()
    {
        return $this->pagomensualLineaTarjeta;
    }

    /**
     * Set totalPropiedades
     *
     * @param string $totalPropiedades
     * @return Usuario
     */
    public function setTotalPropiedades($totalPropiedades)
    {
        $this->totalPropiedades = $totalPropiedades;
    
        return $this;
    }

    /**
     * Get totalPropiedades
     *
     * @return string 
     */
    public function getTotalPropiedades()
    {
        return $this->totalPropiedades;
    }

    /**
     * Set totalAutomoviles
     *
     * @param string $totalAutomoviles
     * @return Usuario
     */
    public function setTotalAutomoviles($totalAutomoviles)
    {
        $this->totalAutomoviles = $totalAutomoviles;
    
        return $this;
    }

    /**
     * Get totalAutomoviles
     *
     * @return string 
     */
    public function getTotalAutomoviles()
    {
        return $this->totalAutomoviles;
    }

    /**
     * Set totalAhorroInversion
     *
     * @param string $totalAhorroInversion
     * @return Usuario
     */
    public function setTotalAhorroInversion($totalAhorroInversion)
    {
        $this->totalAhorroInversion = $totalAhorroInversion;
    
        return $this;
    }

    /**
     * Get totalAhorroInversion
     *
     * @return string 
     */
    public function getTotalAhorroInversion()
    {
        return $this->totalAhorroInversion;
    }

    /**
     * Set nuevoPassword
     *
     * @param string $nuevoPassword
     * @return Usuario
     */
    public function setNuevoPassword($nuevoPassword)
    {
        $this->nuevoPassword = $nuevoPassword;
    
        return $this;
    }

    /**
     * Get nuevoPassword
     *
     * @return string 
     */
    public function getNuevoPassword()
    {
        return $this->nuevoPassword;
    }

    /**
     * Set estadoPassword
     *
     * @param string $estadoPassword
     * @return Usuario
     */
    public function setEstadoPassword($estadoPassword)
    {
        $this->estadoPassword = $estadoPassword;
    
        return $this;
    }

    /**
     * Get estadoPassword
     *
     * @return string 
     */
    public function getEstadoPassword()
    {
        return $this->estadoPassword;
    }

    /**
     * Set errorLogin
     *
     * @param string $errorLogin
     * @return Usuario
     */
    public function setErrorLogin($errorLogin)
    {
        $this->errorLogin = $errorLogin;
    
        return $this;
    }

    /**
     * Get errorLogin
     *
     * @return string 
     */
    public function getErrorLogin()
    {
        return $this->errorLogin;
    }

    /**
     * Add antecedenteslaboral
     *
     * @param \Bench\UsuariosBundle\Entity\Antecedenteslaboral $antecedenteslaboral
     * @return Usuario
     */
    public function addAntecedenteslaboral(\Bench\UsuariosBundle\Entity\Antecedenteslaboral $antecedenteslaboral)
    {
        $this->antecedenteslaboral[] = $antecedenteslaboral;
    
        return $this;
    }

    /**
     * Remove antecedenteslaboral
     *
     * @param \Bench\UsuariosBundle\Entity\Antecedenteslaboral $antecedenteslaboral
     */
    public function removeAntecedenteslaboral(\Bench\UsuariosBundle\Entity\Antecedenteslaboral $antecedenteslaboral)
    {
        $this->antecedenteslaboral->removeElement($antecedenteslaboral);
    }

    /**
     * Get antecedenteslaboral
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAntecedenteslaboral()
    {
        return $this->antecedenteslaboral;
    }

    /**
     * Add conyuge
     *
     * @param \Bench\UsuariosBundle\Entity\Conyuge $conyuge
     * @return Usuario
     */
    public function addConyuge(\Bench\UsuariosBundle\Entity\Conyuge $conyuge)
    {
        $this->conyuge[] = $conyuge;
    
        return $this;
    }

    /**
     * Remove conyuge
     *
     * @param \Bench\UsuariosBundle\Entity\Conyuge $conyuge
     */
    public function removeConyuge(\Bench\UsuariosBundle\Entity\Conyuge $conyuge)
    {
        $this->conyuge->removeElement($conyuge);
    }

    /**
     * Get conyuge
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConyuge()
    {
        return $this->conyuge;
    }

    /**
     * Add bienesraices
     *
     * @param \Bench\TienesBundle\Entity\Bienesraices $bienesraices
     * @return Usuario
     */
    public function addBienesraice(\Bench\TienesBundle\Entity\Bienesraices $bienesraices)
    {
        $this->bienesraices[] = $bienesraices;
    
        return $this;
    }

    /**
     * Remove bienesraices
     *
     * @param \Bench\TienesBundle\Entity\Bienesraices $bienesraices
     */
    public function removeBienesraice(\Bench\TienesBundle\Entity\Bienesraices $bienesraices)
    {
        $this->bienesraices->removeElement($bienesraices);
    }

    /**
     * Get bienesraices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBienesraices()
    {
        return $this->bienesraices;
    }

    /**
     * Add ahorroinverson
     *
     * @param \Bench\TienesBundle\Entity\Ahorroinverson $ahorroinverson
     * @return Usuario
     */
    public function addAhorroinverson(\Bench\TienesBundle\Entity\Ahorroinverson $ahorroinverson)
    {
        $this->ahorroinverson[] = $ahorroinverson;
    
        return $this;
    }

    /**
     * Remove ahorroinverson
     *
     * @param \Bench\TienesBundle\Entity\Ahorroinverson $ahorroinverson
     */
    public function removeAhorroinverson(\Bench\TienesBundle\Entity\Ahorroinverson $ahorroinverson)
    {
        $this->ahorroinverson->removeElement($ahorroinverson);
    }

    /**
     * Get ahorroinverson
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAhorroinverson()
    {
        return $this->ahorroinverson;
    }

    /**
     * Add automoviles
     *
     * @param \Bench\TienesBundle\Entity\Automoviles $automoviles
     * @return Usuario
     */
    public function addAutomovile(\Bench\TienesBundle\Entity\Automoviles $automoviles)
    {
        $this->automoviles[] = $automoviles;
    
        return $this;
    }

    /**
     * Remove automoviles
     *
     * @param \Bench\TienesBundle\Entity\Automoviles $automoviles
     */
    public function removeAutomovile(\Bench\TienesBundle\Entity\Automoviles $automoviles)
    {
        $this->automoviles->removeElement($automoviles);
    }

    /**
     * Get automoviles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAutomoviles()
    {
        return $this->automoviles;
    }

    /**
     * Add sociedades
     *
     * @param \Bench\TienesBundle\Entity\Sociedades $sociedades
     * @return Usuario
     */
    public function addSociedade(\Bench\TienesBundle\Entity\Sociedades $sociedades)
    {
        $this->sociedades[] = $sociedades;
    
        return $this;
    }

    /**
     * Remove sociedades
     *
     * @param \Bench\TienesBundle\Entity\Sociedades $sociedades
     */
    public function removeSociedade(\Bench\TienesBundle\Entity\Sociedades $sociedades)
    {
        $this->sociedades->removeElement($sociedades);
    }

    /**
     * Get sociedades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSociedades()
    {
        return $this->sociedades;
    }

    /**
     * Add debeshipotecario
     *
     * @param \Bench\DebesBundle\Entity\Debeshipotecario $debeshipotecario
     * @return Usuario
     */
    public function addDebeshipotecario(\Bench\DebesBundle\Entity\Debeshipotecario $debeshipotecario)
    {
        $this->debeshipotecario[] = $debeshipotecario;
    
        return $this;
    }

    /**
     * Remove debeshipotecario
     *
     * @param \Bench\DebesBundle\Entity\Debeshipotecario $debeshipotecario
     */
    public function removeDebeshipotecario(\Bench\DebesBundle\Entity\Debeshipotecario $debeshipotecario)
    {
        $this->debeshipotecario->removeElement($debeshipotecario);
    }

    /**
     * Get debeshipotecario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDebeshipotecario()
    {
        return $this->debeshipotecario;
    }

    /**
     * Add debestarjetacredito
     *
     * @param \Bench\DebesBundle\Entity\DebesTarjetaCredito $debestarjetacredito
     * @return Usuario
     */
    public function addDebestarjetacredito(\Bench\DebesBundle\Entity\DebesTarjetaCredito $debestarjetacredito)
    {
        $this->debestarjetacredito[] = $debestarjetacredito;
    
        return $this;
    }

    /**
     * Remove debestarjetacredito
     *
     * @param \Bench\DebesBundle\Entity\DebesTarjetaCredito $debestarjetacredito
     */
    public function removeDebestarjetacredito(\Bench\DebesBundle\Entity\DebesTarjetaCredito $debestarjetacredito)
    {
        $this->debestarjetacredito->removeElement($debestarjetacredito);
    }

    /**
     * Get debestarjetacredito
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDebestarjetacredito()
    {
        return $this->debestarjetacredito;
    }

    /**
     * Add debeslineacredito
     *
     * @param \Bench\DebesBundle\Entity\DebesLineaCredito $debeslineacredito
     * @return Usuario
     */
    public function addDebeslineacredito(\Bench\DebesBundle\Entity\DebesLineaCredito $debeslineacredito)
    {
        $this->debeslineacredito[] = $debeslineacredito;
    
        return $this;
    }

    /**
     * Remove debeslineacredito
     *
     * @param \Bench\DebesBundle\Entity\DebesLineaCredito $debeslineacredito
     */
    public function removeDebeslineacredito(\Bench\DebesBundle\Entity\DebesLineaCredito $debeslineacredito)
    {
        $this->debeslineacredito->removeElement($debeslineacredito);
    }

    /**
     * Get debeslineacredito
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDebeslineacredito()
    {
        return $this->debeslineacredito;
    }

    /**
     * Add debescreditoconsumo
     *
     * @param \Bench\DebesBundle\Entity\DebesCreditoConsumo $debescreditoconsumo
     * @return Usuario
     */
    public function addDebescreditoconsumo(\Bench\DebesBundle\Entity\DebesCreditoConsumo $debescreditoconsumo)
    {
        $this->debescreditoconsumo[] = $debescreditoconsumo;
    
        return $this;
    }

    /**
     * Remove debescreditoconsumo
     *
     * @param \Bench\DebesBundle\Entity\DebesCreditoConsumo $debescreditoconsumo
     */
    public function removeDebescreditoconsumo(\Bench\DebesBundle\Entity\DebesCreditoConsumo $debescreditoconsumo)
    {
        $this->debescreditoconsumo->removeElement($debescreditoconsumo);
    }

    /**
     * Get debescreditoconsumo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDebescreditoconsumo()
    {
        return $this->debescreditoconsumo;
    }

    /**
     * Add creditoconsumo
     *
     * @param \Bench\CreditosBundle\Entity\CreditoConsumo $creditoconsumo
     * @return Usuario
     */
    public function addCreditoconsumo(\Bench\CreditosBundle\Entity\CreditoConsumo $creditoconsumo)
    {
        $this->creditoconsumo[] = $creditoconsumo;
    
        return $this;
    }

    /**
     * Remove creditoconsumo
     *
     * @param \Bench\CreditosBundle\Entity\CreditoConsumo $creditoconsumo
     */
    public function removeCreditoconsumo(\Bench\CreditosBundle\Entity\CreditoConsumo $creditoconsumo)
    {
        $this->creditoconsumo->removeElement($creditoconsumo);
    }

    /**
     * Get creditoconsumo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreditoconsumo()
    {
        return $this->creditoconsumo;
    }

    /**
     * Add creditoautomotriz
     *
     * @param \Bench\CreditosBundle\Entity\CreditoAutomotriz $creditoautomotriz
     * @return Usuario
     */
    public function addCreditoautomotriz(\Bench\CreditosBundle\Entity\CreditoAutomotriz $creditoautomotriz)
    {
        $this->creditoautomotriz[] = $creditoautomotriz;
    
        return $this;
    }

    /**
     * Remove creditoautomotriz
     *
     * @param \Bench\CreditosBundle\Entity\CreditoAutomotriz $creditoautomotriz
     */
    public function removeCreditoautomotriz(\Bench\CreditosBundle\Entity\CreditoAutomotriz $creditoautomotriz)
    {
        $this->creditoautomotriz->removeElement($creditoautomotriz);
    }

    /**
     * Get creditoautomotriz
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreditoautomotriz()
    {
        return $this->creditoautomotriz;
    }

    /**
     * Add creditohipotecario
     *
     * @param \Bench\CreditosBundle\Entity\CreditoHipotecario $creditohipotecario
     * @return Usuario
     */
    public function addCreditohipotecario(\Bench\CreditosBundle\Entity\CreditoHipotecario $creditohipotecario)
    {
        $this->creditohipotecario[] = $creditohipotecario;
    
        return $this;
    }

    /**
     * Remove creditohipotecario
     *
     * @param \Bench\CreditosBundle\Entity\CreditoHipotecario $creditohipotecario
     */
    public function removeCreditohipotecario(\Bench\CreditosBundle\Entity\CreditoHipotecario $creditohipotecario)
    {
        $this->creditohipotecario->removeElement($creditohipotecario);
    }

    /**
     * Get creditohipotecario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreditohipotecario()
    {
        return $this->creditohipotecario;
    }

    /**
     * Add creditoconsolidacion
     *
     * @param \Bench\CreditosBundle\Entity\CreditoConsolidacion $creditoconsolidacion
     * @return Usuario
     */
    public function addCreditoconsolidacion(\Bench\CreditosBundle\Entity\CreditoConsolidacion $creditoconsolidacion)
    {
        $this->creditoconsolidacion[] = $creditoconsolidacion;
    
        return $this;
    }

    /**
     * Remove creditoconsolidacion
     *
     * @param \Bench\CreditosBundle\Entity\CreditoConsolidacion $creditoconsolidacion
     */
    public function removeCreditoconsolidacion(\Bench\CreditosBundle\Entity\CreditoConsolidacion $creditoconsolidacion)
    {
        $this->creditoconsolidacion->removeElement($creditoconsolidacion);
    }

    /**
     * Get creditoconsolidacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreditoconsolidacion()
    {
        return $this->creditoconsolidacion;
    }

    /**
     * Add creditocuenta
     *
     * @param \Bench\CreditosBundle\Entity\CreditoCuenta $creditocuenta
     * @return Usuario
     */
    public function addCreditocuenta(\Bench\CreditosBundle\Entity\CreditoCuenta $creditocuenta)
    {
        $this->creditocuenta[] = $creditocuenta;
    
        return $this;
    }

    /**
     * Remove creditocuenta
     *
     * @param \Bench\CreditosBundle\Entity\CreditoCuenta $creditocuenta
     */
    public function removeCreditocuenta(\Bench\CreditosBundle\Entity\CreditoCuenta $creditocuenta)
    {
        $this->creditocuenta->removeElement($creditocuenta);
    }

    /**
     * Get creditocuenta
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreditocuenta()
    {
        return $this->creditocuenta;
    }

    /**
     * Add solicitudescreditos
     *
     * @param \Bench\SolicitudesCreditosBundle\Entity\SolicitudesCreditos $solicitudescreditos
     * @return Usuario
     */
    public function addSolicitudescredito(\Bench\SolicitudesCreditosBundle\Entity\SolicitudesCreditos $solicitudescreditos)
    {
        $this->solicitudescreditos[] = $solicitudescreditos;
    
        return $this;
    }

    /**
     * Remove solicitudescreditos
     *
     * @param \Bench\SolicitudesCreditosBundle\Entity\SolicitudesCreditos $solicitudescreditos
     */
    public function removeSolicitudescredito(\Bench\SolicitudesCreditosBundle\Entity\SolicitudesCreditos $solicitudescreditos)
    {
        $this->solicitudescreditos->removeElement($solicitudescreditos);
    }

    /**
     * Get solicitudescreditos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSolicitudescreditos()
    {
        return $this->solicitudescreditos;
    }

    /**
     * Add autorizodicom
     *
     * @param \Bench\UsuariosBundle\Entity\autorizoDicom $autorizodicom
     * @return Usuario
     */
    public function addAutorizodicom(\Bench\UsuariosBundle\Entity\autorizoDicom $autorizodicom)
    {
        $this->autorizodicom[] = $autorizodicom;
    
        return $this;
    }

    /**
     * Remove autorizodicom
     *
     * @param \Bench\UsuariosBundle\Entity\autorizoDicom $autorizodicom
     */
    public function removeAutorizodicom(\Bench\UsuariosBundle\Entity\autorizoDicom $autorizodicom)
    {
        $this->autorizodicom->removeElement($autorizodicom);
    }

    /**
     * Get autorizodicom
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAutorizodicom()
    {
        return $this->autorizodicom;
    }

    /**
     * Add archivos
     *
     * @param \Bench\FilepickerBundle\Entity\Archivos $archivos
     * @return Usuario
     */
    public function addArchivo(\Bench\FilepickerBundle\Entity\Archivos $archivos)
    {
        $this->archivos[] = $archivos;
    
        return $this;
    }

    /**
     * Remove archivos
     *
     * @param \Bench\FilepickerBundle\Entity\Archivos $archivos
     */
    public function removeArchivo(\Bench\FilepickerBundle\Entity\Archivos $archivos)
    {
        $this->archivos->removeElement($archivos);
    }

    /**
     * Get archivos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArchivos()
    {
        return $this->archivos;
    }

    /**
     * Add cotizaciones
     *
     * @param \Bench\UsuariosBundle\Entity\Cotizaciones $cotizaciones
     * @return Usuario
     */
    public function addCotizacione(\Bench\UsuariosBundle\Entity\Cotizaciones $cotizaciones)
    {
        $this->cotizaciones[] = $cotizaciones;
    
        return $this;
    }

    /**
     * Remove cotizaciones
     *
     * @param \Bench\UsuariosBundle\Entity\Cotizaciones $cotizaciones
     */
    public function removeCotizacione(\Bench\UsuariosBundle\Entity\Cotizaciones $cotizaciones)
    {
        $this->cotizaciones->removeElement($cotizaciones);
    }

    /**
     * Get cotizaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCotizaciones()
    {
        return $this->cotizaciones;
    }

    /**
     * Add fechaactualizacion
     *
     * @param \Bench\UsuariosBundle\Entity\FechaActualizacion $fechaactualizacion
     * @return Usuario
     */
    public function addFechaactualizacion(\Bench\UsuariosBundle\Entity\FechaActualizacion $fechaactualizacion)
    {
        $this->fechaactualizacion[] = $fechaactualizacion;
    
        return $this;
    }

    /**
     * Remove fechaactualizacion
     *
     * @param \Bench\UsuariosBundle\Entity\FechaActualizacion $fechaactualizacion
     */
    public function removeFechaactualizacion(\Bench\UsuariosBundle\Entity\FechaActualizacion $fechaactualizacion)
    {
        $this->fechaactualizacion->removeElement($fechaactualizacion);
    }

    /**
     * Get fechaactualizacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFechaactualizacion()
    {
        return $this->fechaactualizacion;
    }

    /**
     * Set ejecutivos
     *
     * @param \Bench\UsuariosBundle\Entity\Ejecutivos $ejecutivos
     * @return Usuario
     */
    public function setEjecutivos(\Bench\UsuariosBundle\Entity\Ejecutivos $ejecutivos = null)
    {
        $this->ejecutivos = $ejecutivos;
    
        return $this;
    }

    /**
     * Get ejecutivos
     *
     * @return \Bench\UsuariosBundle\Entity\Ejecutivos 
     */
    public function getEjecutivos()
    {
        return $this->ejecutivos;
    }
}