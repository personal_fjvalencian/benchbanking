<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parametros
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Parametros
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @ORM\Column(type="integer")
     */


    private $tasaMensual;



    /**
     * @ORM\Column(type="integer")
     */


    
    private $sueldoMinimoHipotecario;


    /**
     * @ORM\Column(type="integer")
     */


    private $sueldoMinimoConsumo;



    /**
     * @ORM\Column(type="integer")
     */



    private $sueldoMinimoConsolidacion;


    
    /**
     * @ORM\Column(type="integer")
     */



    private $sueldoMinimoCuentaCorriente;


    
 

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tasaMensual
     *
     * @param integer $tasaMensual
     * @return Parametros
     */
    public function setTasaMensual($tasaMensual)
    {
        $this->tasaMensual = $tasaMensual;
    
        return $this;
    }

    /**
     * Get tasaMensual
     *
     * @return integer 
     */
    public function getTasaMensual()
    {
        return $this->tasaMensual;
    }

    /**
     * Set sueldoMinimoHipotecario
     *
     * @param integer $sueldoMinimoHipotecario
     * @return Parametros
     */
    public function setSueldoMinimoHipotecario($sueldoMinimoHipotecario)
    {
        $this->sueldoMinimoHipotecario = $sueldoMinimoHipotecario;
    
        return $this;
    }

    /**
     * Get sueldoMinimoHipotecario
     *
     * @return integer 
     */
    public function getSueldoMinimoHipotecario()
    {
        return $this->sueldoMinimoHipotecario;
    }

    /**
     * Set sueldoMinimoConsumo
     *
     * @param integer $sueldoMinimoConsumo
     * @return Parametros
     */
    public function setSueldoMinimoConsumo($sueldoMinimoConsumo)
    {
        $this->sueldoMinimoConsumo = $sueldoMinimoConsumo;
    
        return $this;
    }

    /**
     * Get sueldoMinimoConsumo
     *
     * @return integer 
     */
    public function getSueldoMinimoConsumo()
    {
        return $this->sueldoMinimoConsumo;
    }

    /**
     * Set sueldoMinimoConsolidacion
     *
     * @param integer $sueldoMinimoConsolidacion
     * @return Parametros
     */
    public function setSueldoMinimoConsolidacion($sueldoMinimoConsolidacion)
    {
        $this->sueldoMinimoConsolidacion = $sueldoMinimoConsolidacion;
    
        return $this;
    }

    /**
     * Get sueldoMinimoConsolidacion
     *
     * @return integer 
     */
    public function getSueldoMinimoConsolidacion()
    {
        return $this->sueldoMinimoConsolidacion;
    }

    /**
     * Set sueldoMinimoCuentaCorriente
     *
     * @param integer $sueldoMinimoCuentaCorriente
     * @return Parametros
     */
    public function setSueldoMinimoCuentaCorriente($sueldoMinimoCuentaCorriente)
    {
        $this->sueldoMinimoCuentaCorriente = $sueldoMinimoCuentaCorriente;
    
        return $this;
    }

    /**
     * Get sueldoMinimoCuentaCorriente
     *
     * @return integer 
     */
    public function getSueldoMinimoCuentaCorriente()
    {
        return $this->sueldoMinimoCuentaCorriente;
    }
}