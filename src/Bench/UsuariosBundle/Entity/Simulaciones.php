<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Simulaciones
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Simulaciones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=100  , nullable=true)
     */


    private $tipo;


    /**
     * @var string
     *
     * @ORM\Column(name="montoCredito", type="string", length=100  , nullable=true)
     */

    private $montoCredito;


    /**
     * @var string
     *
     * @ORM\Column(name="pie", type="string", length=100  , nullable=true)
     */

    private $pie;


    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100  , nullable=true)
     */


    private $email;


    /**
     * @var string
     *
     * @ORM\Column(name="Sueldo", type="string", length=100  , nullable=true)
     */


    private $Sueldo;



    /**
     * @var string
     *
     * @ORM\Column(name="Plazo", type="string", length=100  , nullable=true)
     */

    private $Plazo;


    /**
     *
     *@var \DateTime
     *
     * @ORM\Column(name="fechaSimulacion", type="date"  , nullable=true)
     */


    private $fechaSimulacion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Simulaciones
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set montoCredito
     *
     * @param string $montoCredito
     * @return Simulaciones
     */
    public function setMontoCredito($montoCredito)
    {
        $this->montoCredito = $montoCredito;
    
        return $this;
    }

    /**
     * Get montoCredito
     *
     * @return string 
     */
    public function getMontoCredito()
    {
        return $this->montoCredito;
    }

    /**
     * Set pie
     *
     * @param string $pie
     * @return Simulaciones
     */
    public function setPie($pie)
    {
        $this->pie = $pie;
    
        return $this;
    }

    /**
     * Get pie
     *
     * @return string 
     */
    public function getPie()
    {
        return $this->pie;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Simulaciones
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set Sueldo
     *
     * @param string $sueldo
     * @return Simulaciones
     */
    public function setSueldo($sueldo)
    {
        $this->Sueldo = $sueldo;
    
        return $this;
    }

    /**
     * Get Sueldo
     *
     * @return string 
     */
    public function getSueldo()
    {
        return $this->Sueldo;
    }

    /**
     * Set Plazo
     *
     * @param string $plazo
     * @return Simulaciones
     */
    public function setPlazo($plazo)
    {
        $this->Plazo = $plazo;
    
        return $this;
    }

    /**
     * Get Plazo
     *
     * @return string 
     */
    public function getPlazo()
    {
        return $this->Plazo;
    }

    /**
     * Set fechaSimulacion
     *
     * @param \DateTime $fechaSimulacion
     * @return Simulaciones
     */
    public function setFechaSimulacion($fechaSimulacion)
    {
        $this->fechaSimulacion = $fechaSimulacion;
    
        return $this;
    }

    /**
     * Get fechaSimulacion
     *
     * @return \DateTime 
     */
    public function getFechaSimulacion()
    {
        return $this->fechaSimulacion;
    }
}