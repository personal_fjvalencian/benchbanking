<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistoricoFechaAct
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class HistoricoFechaAct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    
    
      /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="historicofechaact")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    private $usuario;

    

    
    
    
       /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaActual", type="datetime" , nullable=true)
     */
    
    
    private $fechaActual;
    
    
       /**
     * @var string
     *
     * @ORM\Column(name="donde", type="string", length=100 ,  nullable=true)
     */
    
    
    private $donde;
    
    
    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaActual
     *
     * @param \DateTime $fechaActual
     * @return HistoricoFechaAct
     */
    public function setFechaActual($fechaActual)
    {
        $this->fechaActual = $fechaActual;
    
        return $this;
    }

    /**
     * Get fechaActual
     *
     * @return \DateTime 
     */
    public function getFechaActual()
    {
        return $this->fechaActual;
    }

    /**
     * Set donde
     *
     * @param string $donde
     * @return HistoricoFechaAct
     */
    public function setDonde($donde)
    {
        $this->donde = $donde;
    
        return $this;
    }

    /**
     * Get donde
     *
     * @return string 
     */
    public function getDonde()
    {
        return $this->donde;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return HistoricoFechaAct
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}