<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * autorizoDicom
 *
 * @ORM\Table()
 * @ORM\Entity
 */


class autorizoDicom
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
      /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime" , nullable=true)
     */
    
    
    private $fecha;
    
    
   
    
    
      /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=100 ,  nullable=true)
     */
    
    
    
    
    
    
    private $estado;
    
    
    
    
      /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=100 ,  nullable=true)
     */
    
    
    
    
    
    private $tipo;
    
    
    
     /**
     * @var integer
     *
     * @ORM\Column(name="credito_id", type="integer")
     */
    
    
    
    private $credito_id;
    
    
    
    


      /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="autorizodicom")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    private $usuario;
    
    


  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return autorizoDicom
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return autorizoDicom
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return autorizoDicom
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set credito_id
     *
     * @param integer $creditoId
     * @return autorizoDicom
     */
    public function setCreditoId($creditoId)
    {
        $this->credito_id = $creditoId;
    
        return $this;
    }

    /**
     * Get credito_id
     *
     * @return integer 
     */
    public function getCreditoId()
    {
        return $this->credito_id;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return autorizoDicom
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}