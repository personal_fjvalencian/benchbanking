<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FechaActualizacion
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FechaActualizacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    
    
        /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="fechaactualizacion")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    
    
    
    private $usuario;
            
            
    
           
    
      /**
     * @var string
     *
     * @ORM\Column(name="donde", type="string", length=100 ,  nullable=true)
     */
    
    
    private $donde;
    
    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime" , nullable=true)
     */
    
    
    private $fecha;
    
    
  


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set donde
     *
     * @param string $donde
     * @return FechaActualizacion
     */
    public function setDonde($donde)
    {
        $this->donde = $donde;
    
        return $this;
    }

    /**
     * Get donde
     *
     * @return string 
     */
    public function getDonde()
    {
        return $this->donde;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return FechaActualizacion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return FechaActualizacion
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}