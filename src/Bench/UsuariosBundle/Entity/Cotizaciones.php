<?php

namespace Bench\UsuariosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cotizaciones
 *
 * @ORM\Table()
 * @ORM\Entity
 */


class Cotizaciones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
       /**
     * @var string
     *
     * @ORM\Column(name="monto", type="string", length=100 ,  nullable=true)
     */

  
    
    
    private $monto;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="plazo", type="string", length=100 ,  nullable=true)
     */

    
    private $plazo;
    
    
      /**
     * @var string
     *
     * @ORM\Column(name="banco", type="string", length=100 ,  nullable=true)
     */
    
    
    
    private $banco;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=100 ,  nullable=true)
     */
    
    
    
    private $tipo;
    
    
       /** @ORM\Column(type="string", length=500 ,  nullable=true) */
    
        private $Archivourl;
    
    /** @ORM\Column(type="string", length=100 ,  nullable=true) */
    
    private $Archivoestado;
    
    
    
    /** @ORM\Column(type="string", length=100  , nullable=true) */
    
    
    private $Archivollave;
    
    
    /** @ORM\Column(type="string", length=100  , nullable=true) */
    
    private $ArchivonombreArchivo;
    
    
    
    
    /** @ORM\Column(type="string", length=100  , nullable=true) */
    
    
    private $Archivotipo;
    
    
    
    
 /** @ORM\Column(type="string", length=100  , nullable=true) */
    
    
    private $motivoRechazo;
    
    
    
     /** @ORM\Column(type="string", length=100  , nullable=true) */
    
    
    private $estado;
    
    
    
        
    /** @ORM\Column(type="datetime"  , nullable=true) */
    
    
    
    private $fecha;
    
    
     
    

    
    
     /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="cotizaciones")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    private $usuario;
    
    
     
       /**
     * @var integer
     *
     * @ORM\Column(name="creditoid", type="integer", length=100 ,  nullable=true)
     */
    
    
    private $creditoid;
    
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set monto
     *
     * @param string $monto
     * @return Cotizaciones
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    
        return $this;
    }

    /**
     * Get monto
     *
     * @return string 
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set plazo
     *
     * @param string $plazo
     * @return Cotizaciones
     */
    public function setPlazo($plazo)
    {
        $this->plazo = $plazo;
    
        return $this;
    }

    /**
     * Get plazo
     *
     * @return string 
     */
    public function getPlazo()
    {
        return $this->plazo;
    }

    /**
     * Set banco
     *
     * @param string $banco
     * @return Cotizaciones
     */
    public function setBanco($banco)
    {
        $this->banco = $banco;
    
        return $this;
    }

    /**
     * Get banco
     *
     * @return string 
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Cotizaciones
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set Archivourl
     *
     * @param string $archivourl
     * @return Cotizaciones
     */
    public function setArchivourl($archivourl)
    {
        $this->Archivourl = $archivourl;
    
        return $this;
    }

    /**
     * Get Archivourl
     *
     * @return string 
     */
    public function getArchivourl()
    {
        return $this->Archivourl;
    }

    /**
     * Set Archivoestado
     *
     * @param string $archivoestado
     * @return Cotizaciones
     */
    public function setArchivoestado($archivoestado)
    {
        $this->Archivoestado = $archivoestado;
    
        return $this;
    }

    /**
     * Get Archivoestado
     *
     * @return string 
     */
    public function getArchivoestado()
    {
        return $this->Archivoestado;
    }

    /**
     * Set Archivollave
     *
     * @param string $archivollave
     * @return Cotizaciones
     */
    public function setArchivollave($archivollave)
    {
        $this->Archivollave = $archivollave;
    
        return $this;
    }

    /**
     * Get Archivollave
     *
     * @return string 
     */
    public function getArchivollave()
    {
        return $this->Archivollave;
    }

    /**
     * Set ArchivonombreArchivo
     *
     * @param string $archivonombreArchivo
     * @return Cotizaciones
     */
    public function setArchivonombreArchivo($archivonombreArchivo)
    {
        $this->ArchivonombreArchivo = $archivonombreArchivo;
    
        return $this;
    }

    /**
     * Get ArchivonombreArchivo
     *
     * @return string 
     */
    public function getArchivonombreArchivo()
    {
        return $this->ArchivonombreArchivo;
    }

    /**
     * Set Archivotipo
     *
     * @param string $archivotipo
     * @return Cotizaciones
     */
    public function setArchivotipo($archivotipo)
    {
        $this->Archivotipo = $archivotipo;
    
        return $this;
    }

    /**
     * Get Archivotipo
     *
     * @return string 
     */
    public function getArchivotipo()
    {
        return $this->Archivotipo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Cotizaciones
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set creditoid
     *
     * @param integer $creditoid
     * @return Cotizaciones
     */
    public function setCreditoid($creditoid)
    {
        $this->creditoid = $creditoid;
    
        return $this;
    }

    /**
     * Get creditoid
     *
     * @return integer 
     */
    public function getCreditoid()
    {
        return $this->creditoid;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Cotizaciones
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set motivoRechazo
     *
     * @param string $motivoRechazo
     * @return Cotizaciones
     */
    public function setMotivoRechazo($motivoRechazo)
    {
        $this->motivoRechazo = $motivoRechazo;
    
        return $this;
    }

    /**
     * Get motivoRechazo
     *
     * @return string 
     */
    public function getMotivoRechazo()
    {
        return $this->motivoRechazo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Cotizaciones
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }
}