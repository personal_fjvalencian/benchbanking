<?php

namespace Bench\UsuariosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AntecedenteslaboralType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('situacionlaboral')
            ->add('empleadorempresa')
            ->add('industria')
            ->add('fechaingreso')
            ->add('cargoactual')
            ->add('rutempresa')
            ->add('direccion')
            ->add('region')
            ->add('comuna')
            ->add('ciudad')
            ->add('telefono')
            ->add('tipoactividad')
            ->add('tiposueldo')
            ->add('sueldofijo')
            ->add('sueldoliquido')
            ->add('rentaliquida')
            ->add('rentavariable')
            ->add('independientesueldoliquido')
            ->add('fechaultima')
            ->add('fechacambio')
            ->add('usuario')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bench\UsuariosBundle\Entity\Antecedenteslaboral'
        ));
    }

    public function getName()
    {
        return 'bench_usuariosbundle_antecedenteslaboraltype';
    }
}
