<?php

namespace Bench\UsuariosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rut')
            ->add('nombre')
            ->add('apellido')
            ->add('apellidoseg')
            ->add('email')
            ->add('contrasena')
            ->add('sexo')
            ->add('nacionalidad')
            ->add('fechaNacimiento')
            ->add('estadocivil')
            ->add('niveleducacion')
            ->add('profesion')
            ->add('universidad')
            ->add('tipocasa')
            ->add('ndependientes')
            ->add('direccion')
            ->add('region')
            ->add('comuna')
            ->add('montoArriendo')
            ->add('fechaIngreso')
            ->add('telefono')
            ->add('final')
            ->add('asignar')
            ->add('navegador')
            ->add('archivar')
            ->add('dicom')
            ->add('vecesLogin')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bench\UsuariosBundle\Entity\Usuario'
        ));
    }

    public function getName()
    {
        return 'bench_usuariosbundle_usuariotype';
    }
}
