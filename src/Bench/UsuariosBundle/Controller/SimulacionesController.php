<?php
/**
 * Created by PhpStorm.
 * User: giorgosbarkos
 * Date: 06-03-14
 * Time: 14:18
 */

namespace Bench\UsuariosBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\DataTransformerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;
use Bench\UsuariosBundle\Entity\Simulaciones;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use \DateTime;



class SimulacionesController extends Controller {



    public function simulacionesSaveAction(Request $request) {

        $em = $this->getDoctrine()->getManager();


        $session = $this->getRequest()->getSession();


        $sueldo = $request->request->get('sueldo');
        $montoCredito = $request->request->get('montoCredito');
        $pie = $request->request->get('pie');
        $tipo = $request->request->get('tipo');
        $plazo = $request->request->get('plazo');
        $email = $request->request->get('email');
        $pieP = $request->request->get('pieP');

        $montoPropiedadSin = $request->request->get('montoPropiedadSin');


       $credito = array(
               'sueldo' => $sueldo,
               'montoCredito' => $montoCredito ,
               'pie' => $pie ,
               'tipo' => $tipo,
               'plazo' => $plazo,
               'email' => $email,
               'pieP' => $pieP,
               'montoPropiedadSin' =>   $montoPropiedadSin );



        $session->set('credito' , $credito );
        $session->set('EmailUsuario' , $email);






        $simulacion = new Simulaciones();
        $simulacion->setSueldo($sueldo);
        $simulacion->setMontoCredito($montoCredito);
        $simulacion->setPie($pie);
        $simulacion->setTipo($tipo);
        $simulacion->setPlazo($plazo);
        $simulacion->setEmail($email);




        $em->persist($simulacion);

        $em->flush();


        return New Response('100');










    }

} 