<?php

namespace Bench\UsuariosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\DataTransformerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\UsuariosBundle\Entity\Antecedenteslaboral;
use Bench\UsuariosBundle\Entity\Conyuge;
use Bench\UsuariosBundle\Entity\Cotizaciones;
use Symfony\Component\HttpFoundation\Response;

use Bench\RegistrosBundle\Controller\zendesk;
use Bench\UsuariosBundle\Entity\HistoricoFechaAct;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use \DateTime;
use JMS\Serializer\SerializerBuilder;

class DefaultController extends Controller {

    public function indexAction($name) {
        return $this->render('BenchUsuariosBundle:Default:index.html.twig', array('name' => $name));
    }


    /* test de serializer */
    
    public function testApiAction(){

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findAll();
        $serializer = $this->container->get('jms_serializer');



        return  $serializer->serialize($usuario, 'xml');


    }

    /* test de serializer */

    

    public function datospersonalesAction(Request $request) {

  return $this->render('BenchUsuariosBundle:default:datospersonales.html.twig', array('usuarios' => $entity));
    }

///Datos PERSONALES GUARDADO

    
    
    
    public function datospesonalesAction(Request $request) {
        
       $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        $request->headers->addCacheControlDirective('no-store', true);
        $tokenPost = $request->request->get('token');
        
        
        if($token == $tokenPost){
            
            
        $session = $this->getRequest()->getSession();

        $rut = $session->get('rut');

        $nombre = $request->request->get('nombres');
        $apellido1 = $request->request->get('apellido1');
        $apellido2 = $request->request->get('apellido2');
        $rut2 = $request->request->get('rut');
        $nacimiento = $request->request->get('nacimiento');
        $sexo = $request->request->get('sexo');
        $nacionalidad = $request->request->get('nacionalidad');
        $estadociv = $request->request->get('estadociv');
        $niveleducacion = $request->request->get('niveleducacion');
        $profesion = $request->request->get('profesion');
        $universidad = $request->request->get('universidad');
        $telefono1 = $request->request->get('telefono1');
        $tipocasa = $request->request->get('tipocasa');
        $ndependientes = $request->request->get('ndependientes');
        $montoardiv = $request->request->get('montoardiv');
        $direccion = $request->request->get('direccion');
        $regionP = $request->request->get('regionP');
        $comunaP = $request->request->get('comunaP');
        
        


        /// 

        if ($nombre == '' or $apellido1 == '' or $apellido2 == '' or $rut2 == '' or $nacimiento == '' or $sexo == '' or $nacionalidad == '' or
                $estadociv == '' or $niveleducacion == '' or $profesion == '' or $universidad == '' or $telefono1 == '' or
                $tipocasa == '' or $ndependientes == '' or $montoardiv == '' or $direccion == '' or $regionP == '' or $comunaP == ''
        ) {


            $return = ('100');


            return new Response($return); //
        } else {

            ///    

            $em = $this->getDoctrine()->getManager();


           $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
            $usuario ->setNombre($nombre);
            $usuario->setApellido($apellido1);
            $usuario->setApellidoseg($apellido2);
            $usuario->setRut($rut2);
            $usuario->setFechaNacimiento($nacimiento);
            $usuario->setSexo($sexo);
            $usuario->setNiveleducacion($niveleducacion);
            $usuario->setNacionalidad($nacionalidad);
            $usuario->setEstadocivil($estadociv);
            $usuario->setProfesion($profesion);
            $usuario->setUniversidad($universidad);
            $usuario->setTelefono($telefono1);
            $usuario->setTipocasa($tipocasa);
            $usuario->setNdependientes($ndependientes);
            $usuario->setMontoArriendo($montoardiv);
            $usuario->setDireccion($direccion);
            $usuario->setRegion($regionP);
            $usuario->setComuna($comunaP);
    
            /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
      if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }


        /* fin de Actualizar estado */
        
        

            $dts =( new \DateTime());
            $fecha=$dts->format('d-m-Y H:i:s');
            $usuario->setFechaactualizacionFinal($fecha);
            
            

            $em->merge($usuario); // merge se usa en el caso de un update 
            $em->flush();

                   
            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Datos Personales');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();
            
           
            $return = ('200');

        }

            return new Response($return); //
            
            
        }
    }

    /// GUARDAR DATOS LABORALES 

    
    
    public function datoslaboralesAction(Request $request) {
        
        
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        $request->headers->addCacheControlDirective('no-store', true);
        $tokenPost = $request->request->get('token');
        
        if($token == $tokenPost){
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');


        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
        $laboralempresa = $request->request->get('laboralempresa');
        $situacion_laboral = $request->request->get('situacion_laboral');
        $laboralIndustria = $request->request->get('laboralIndustria');
        $fechaingresoLaboral = $request->request->get('fechaingresoLaboral');
        $cargoLaboral = $request->request->get('cargoLaboral');
        $laboraldirecccion = $request->request->get('laboraldirecccion');
        $ComboLaboralRegion = $request->request->get('ComboLaboralRegion');
        $ComboLaboralComuna = $request->request->get('ComboLaboralComuna');
        $laboralcuidad = $request->request->get('laboralcuidad');
        $laboraltelefono4 = $request->request->get('laboraltelefono4');
        $laboraltiporenta = $request->request->get('laboraltiporenta');
        $sueldofijoLaboral = $request->request->get('sueldofijoLaboral');
        $rentavariableLaboral = $request->request->get('rentavariableLaboral');
        $indepsueldoliquidoLaboral = $request->request->get('indepsueldoliquidoLaboral');
        $laboralrutempresa = $request->request->get('laboralrutempresa');
    
        if ($laboralempresa == '' or  $situacion_laboral == '' or $laboralIndustria == '' or $fechaingresoLaboral == ''  or $cargoLaboral == '' or $laboraldirecccion == '' or $ComboLaboralRegion  == '' or
             $ComboLaboralComuna == '' or  $laboralcuidad  == '' or   $laboraltelefono4 == ''  or  $sueldofijoLaboral == '' 
             or $sueldofijoLaboral == '' or $laboraltiporenta == '') {
            
         
            
            
            
              $return = ('100');


            return new Response($return); //
            
            
            
            
            
        } else {
            
          
            $Antecedenteslaboral = new Antecedenteslaboral();
            $Antecedenteslaboral->setUsuario($usuario);
            $Antecedenteslaboral->setEmpleadorempresa($laboralempresa);
            $Antecedenteslaboral->setRutempresa($laboralrutempresa);
            $Antecedenteslaboral->setSituacionlaboral($situacion_laboral);

            $Antecedenteslaboral->setIndustria($laboralIndustria);
            $Antecedenteslaboral->setFechaingreso($fechaingresoLaboral);
            $Antecedenteslaboral->setCargoactual($cargoLaboral);
            $Antecedenteslaboral->setDireccion($laboraldirecccion);
            $Antecedenteslaboral->setRegion($ComboLaboralRegion);

            $Antecedenteslaboral->setComuna($ComboLaboralComuna);
            $Antecedenteslaboral->setCiudad($laboralcuidad);
            $Antecedenteslaboral->setTelefono($laboraltelefono4);
            $Antecedenteslaboral->setTiposueldo($laboraltiporenta);
            $Antecedenteslaboral->setSueldofijo($sueldofijoLaboral);
            $Antecedenteslaboral->setRentavariable($rentavariableLaboral);

            $Antecedenteslaboral->setIndependienteSueldoLiquido($indepsueldoliquidoLaboral);
     
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
   if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }


       
            $dts =( new \DateTime());
            $fecha=$dts->format('d-m-Y H:i:s');
            $usuario->setFechaactualizacionFinal($fecha);
            $em->persist($usuario);
            $em->persist($Antecedenteslaboral);
            $em->flush();

             $return = ('200');
             
            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Datos Laborales');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();                   
            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Datos Personales');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();
        }

            return new Response($return); //
            
            
            
            
        }
    }

    // Guarda datos de conyuge 


    
    
    


    public function conyugeAction(Request $request) {
        
        
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        $tokePost = $request->request->get('token');
        $request->headers->addCacheControlDirective('no-store', true);
        
        if($token == $tokePost){
            
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));


        $Connombres = $request->request->get('Connombres');
        $Conapellido1 = $request->request->get('Conapellido1');
        $Conapellido2 = $request->request->get('Conapellido2');
        $Conrutconyuge = $request->request->get('Conrutconyuge');
        $Connacimiento = $request->request->get('Connacimiento');
        $Connacionalidad = $request->request->get('Connacionalidad');
        $Conniveleducacion = $request->request->get('Conniveleducacion');
        $Conprofesion = $request->request->get('Conprofesion');
        $Conuniversidad = $request->request->get('Conuniversidad');
        $Conactividad = $request->request->get('Conactividad');
        $Concargo = $request->request->get('Concargo');
        $ConAntiguedad = $request->request->get('ConAntiguedad');
        $ConRenta = $request->request->get('ConRenta');
        $Conempresa = $request->request->get('Conempresa');
        $Contelefono = $request->request->get('Contelefono');

        $Conyuge = new Conyuge();
        $Conyuge->setUsuario($usuario);
        $Conyuge->setNombre($Connombres);
        $Conyuge->setApellido($Conapellido1);
        $Conyuge->setApellidoseg($Conapellido2);
        $Conyuge->setRut($Conrutconyuge);
        $Conyuge->setFechanacimiento($Connacimiento);
        $Conyuge->setNacionalidad($Connacionalidad);
        $Conyuge->setNiveleducacion($Conniveleducacion);
        $Conyuge->setProfesion($Conprofesion);
        $Conyuge->setUniversidad($Conuniversidad);
        $Conyuge->setActividad($Conactividad);
        $Conyuge->setCargoactual($Concargo);
        $Conyuge->setAntiguedadlaboral($ConAntiguedad);
        $Conyuge->setRentaliquidad($ConRenta);
        $Conyuge->setEmpresa($Conempresa);
        $Conyuge->setTelefono($Contelefono);
        $Conyuge->setFecha(new \DateTime());
        
        
            /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
        
        
        if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }


        /* fin de Actualizar estado */
        

        $dts =( new \DateTime());
        $fecha=$dts->format('d-m-Y H:i:s');
        $usuario->setFechaactualizacionFinal($fecha);



        $em->persist($usuario);

        $em->persist($Conyuge);

        $em->flush();
        
        $HistoricoFechaAct = new HistoricoFechaAct();
        $HistoricoFechaAct->setDonde('Datos Conyuge');
        $HistoricoFechaAct->setUsuario($usuario);
        $HistoricoFechaAct->setFechaActual(new \DateTime());
        $em->persist($usuario);
        $em->persist($HistoricoFechaAct);
        $em->flush();
            

        $return = '100';
        return new Response($return);
        
        
        }
        
        
    }
    
    
    public function enviarCotizacionAction(Request $request){
        
          
        $idCredito   = $request->request->get('idCredito');
        $em = $this->getDoctrine()->getManager();
        $cotizaciones = new Cotizaciones();
        $cotizaciones = $em->getRepository('BenchUsuariosBundle:Cotizaciones')->findOneBy(array('id' => $idCredito));
        $cotizaciones->setEstado('Cotizado');
        $em->persist($cotizaciones);
        $em->flush();
        $request->headers->addCacheControlDirective('no-store', true);
        
        $tipoCredito = $cotizaciones->getTipo();
        $montoS  = $cotizaciones->getMonto();
        $banco = $cotizaciones->getBanco();
       
        
        
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $nombre = $usuario->getNombre();
        $apellido1 = $usuario->getRut();
        
        
      $asuntoZend = 'Bench Banking el usuario ' . $nombre . ' ' . $apellido1 . 'a Seleccionado una nueva Cotización';
        $NombreCompleto = $usuario->getNombre() . ' ' . $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
        
        
              $detalleZend = 'El Usuario ' . $nombre . ' ' . $apellido1 . '  Rut ' . $rut . 'a Seleccionado una nueva Cotización ' . $tipoCredito . ' 
            Monto . ' . $montoS . 'Banco ' . $banco ;


        // Envio a zend Desk

        $create = json_encode(
                array(
            'ticket' => array(
                'requester' => array(
                    'name' => $usuario->getNombre(),
                    'email' => $usuario->getEmail(),
                ),
                'group_id' => '22',
                'assignee_id' => 'matias@benchbanking.com',
                'subject' => $asuntoZend,
                'custom_fields' =>
                array(
                    '23412128' => $usuario->getRut(),
                    '23412138' => $NombreCompleto,
                    '23723488' => $usuario->getTelefono(),
                    '23723498' => $tipoCredito,
         
                    '23750746' => $montoS),
                'description' => $detalleZend
            )
                ), JSON_FORCE_OBJECT
        );

        $zendDesk = new zendesk();
        $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");
        
        
        
        
      
        $return = '200';
        return new Response($return);
        
    }
    
    
    

}
