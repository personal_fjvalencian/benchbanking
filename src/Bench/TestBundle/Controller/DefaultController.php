<?php

namespace Bench\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BenchTestBundle:Default:index.html.twig', array('name' => $name));
    }

    public function uploadAction(){


        $em = $this->getDoctrine()->getManager();
        $entity = $test = $em->getRepository('BenchTestBundle:TestUpload')->findAll();
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $path = $helper->asset($entity, 'image');
        return $this->render('BenchTestBundle:Default:index.html.twig');



    }
}
