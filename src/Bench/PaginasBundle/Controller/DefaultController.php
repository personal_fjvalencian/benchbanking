<?php

namespace Bench\PaginasBundle\Controller;

use Bench\UsuariosBundle\Entity\Usuario;
use Bench\UsuariosBundle\Entity\Antecedenteslaboral;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bench\UsuariosBundle\Controller\validacionController;
use Bench\UsuariosBundle\mail\Postmark;
use Bench\UsuariosBundle\Entity\HistoricoFechaAct;
use Bench\UsuariosBundle\Entity\NoregistradosMail;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Bench\RegistrosBundle\Controller\zendesk;
use Bench\RegistrosBundle\Controller\codificaDec;

class DefaultController extends Controller {





    // Credito Hipotecario Nuevo Home



    public function testRutAction(){



        function rut($rut_param){

            $parte4 = substr($rut_param, -1); // seria solo el numero verificador
            $parte3 = substr($rut_param, -4,3); // la cuenta va de derecha a izq
            $parte2 = substr($rut_param, -7,3);
            $parte1 = substr($rut_param, 0,-8); //de esta manera toma todos los caracteres desde el 8 hacia la izq

            return $parte1.".".$parte2.".".$parte3."-".$parte4;

        }
        function rut2( $rut ) {
            return number_format( substr ( $rut, 0 , -1 ) , 0, "", ".") . '-' . substr ( $rut, strlen($rut) -1 , 1 );
        }


        $rut = '14.156.537-0';

        $rut2 = str_replace(".", "",  $rut);
        $rut2 = str_replace(".", "",  $rut2);
        $rut2 = str_replace(".", "",  $rut2);
        $rut2 = str_replace(".", "",  $rut2);
        $rut2 = str_replace("-", "",  $rut2);





        $clienteRut = rut($rut2);
        $clienteRut2 = rut2($rut2);




        return  new Response(  $clienteRut . 'rut 2 es' . $clienteRut2) ;







    }

    public function usuarioNoRegistradosAction(Request $request)

    {

        if ($request->getMethod() == 'POST') {


            $token =  $request->request->get('token');
            $email =  $request->request->get('email');
            $em = $this->getDoctrine()->getManager();


            if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            {

                return new Response('900');

            }else{

                $mail2 = $em->getRepository('BenchUsuariosBundle:NoregistradosMail')->findOneBy(array('mail' => $email));

                if($mail2){


                }else{



                    $mail = new NoregistradosMail();
                    $mail->setMail($email);
                    $mail->setFecha(new \DateTime());
                    $em->merge($mail);
                    $em->flush();
                    $respuesta = '200';



                }



            }


        }else{


            $respuesta  = '100';


        }



        $respuesta = 0;


        return new Response($respuesta);

    }



    public function inmob2Action(){


        return $this->render('BenchPaginasBundle:Default:inmob2.html.twig' );



    }
    public function credito1Action()


    {

        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');


        /*
        $tmp = "/tmp/indicadores.xml.tmp";

        if(!file_exists($tmp) OR (time() - filemtime($tmp)) > 600 ){
            $c = file_get_contents("http://indicador.eof.cl/xml", "w+");
            if(!empty($c))
                file_put_contents($tmp, $c);
        }


        if($I = simplexml_load_file($tmp)){
            echo "<ul>";
            foreach($I as $i){
                list($n) = $i->attributes();
                if(preg_match("/UF/", $n))
                    $uf = $i;
            }

        }
 */
           $uf = '23.936,36';

        return $this->render('BenchPaginasBundle:Default:creditoHipotecario.html.twig' , array('token' => $token , 'uf' => $uf));



    }




public function capitalizarmeAction(){


    return $this->render('BenchPaginasBundle:Default:rem.html.twig');

}

public function RFAction(){

 return $this->render('BenchPaginasBundle:Default:RF.html.twig');



}



    public function referenciaAction(){


        return $this->render('BenchPaginasBundle:Default:referencia.html.twig');

    }

    public function credito3Action()
    {

        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        $tmp = "/tmp/indicadores.xml.tmp";
  /*
        if(!file_exists($tmp) OR (time() - filemtime($tmp)) > 600 ){
            $c = file_get_contents("http://indicador.eof.cl/xml", "w+");
            if(!empty($c))
                file_put_contents($tmp, $c);
        }

        if($I = simplexml_load_file($tmp)){
            echo "<ul>";
            foreach($I as $i){
                list($n) = $i->attributes();
                if(preg_match("/UF/", $n))
                    $uf = $i;
            }

        }
   */
        $uf = '23.936,36';

        return $this->render('BenchPaginasBundle:Default:creditoConsolidacion.html.twig' , array('token' => $token , 'uf' => $uf ));


    }


    public function credito4Action()
    {
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        $tmp = "/tmp/indicadores.xml.tmp";

        /*
        if(!file_exists($tmp) OR (time() - filemtime($tmp)) > 600 ){
            $c = file_get_contents("http://indicador.eof.cl/xml", "w+");
            if(!empty($c))
                file_put_contents($tmp, $c);
        }

        if($I = simplexml_load_file($tmp)){
            echo "<ul>";
            foreach($I as $i){
                list($n) = $i->attributes();
                if(preg_match("/UF/", $n))
                    $uf = $i;
            }

        }

        */
        $uf = '23.936,36';

        return $this->render('BenchPaginasBundle:Default:creditoCuentaCorriente.html.twig' , array('token' => $token , 'uf' => $uf ));


        if ($request->getMethod() == 'POST') {


            $token =  $request->request->get('token');
            $email =  $request->request->get('email');
            $em = $this->getDoctrine()->getManager();




            if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            {

                return new Response('900');

            }else{

                $mail2 = $em->getRepository('BenchUsuariosBundle:NoregistradosMail')->findOneBy(array('mail' => $email));

                if($mail2){


                }else{



                    $mail = new NoregistradosMail();
                    $mail->setMail($email);
                    $mail->setFecha(new \DateTime());
                    $em->merge($mail);
                    $em->flush();
                    $respuesta = '200';



                }



            }


        }else{


            $respuesta  = '100';


        }



        $respuesta = 0;


        return new Response($respuesta);

    }





    public function credito2Action()
    {

        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');


       /*
        $tmp = "/tmp/indicadores.xml.tmp";

        if(!file_exists($tmp) OR (time() - filemtime($tmp)) > 600 ){
            $c = file_get_contents("http://indicador.eof.cl/xml", "w+");
            if(!empty($c))
                file_put_contents($tmp, $c);
        }

        if($I = simplexml_load_file($tmp)){
            echo "<ul>";
            foreach($I as $i){
                list($n) = $i->attributes();
                if(preg_match("/UF/", $n))
                    $uf = $i;
            }




        }

       */

        $uf = '23.936,36';

        return $this->render('BenchPaginasBundle:Default:creditoConsumo.html.twig' , array('token' => $token , 'uf' => $uf ));


    }









    public function executeXmlSitemapAction(){


        return $this->render('BenchPaginasBundle:Default:maps.html.php');


    }

    public function mantencionAction(){
        return $this->render('BenchPaginasBundle:Default:mantencion.html.twig');

    }


    public function remaxAction(){
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        return $this->render('BenchPaginasBundle:Default:remax.html.twig' , array('token' => $token));


    }






    public function inmobiliariasAction(){


        return $this->render('BenchPaginasBundle:Default:inmobiliarias.html.twig');


    }


    public function remax2Action(){
        return $this->render('BenchPaginasBundle:Default:remax2.html.twig');

    }

    public function test2Action(){

        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        return $this->render('BenchPaginasBundle:Default:test2.html.twig' , array('token' => $token));






    }


    public function misCotizacionesAction()
    {

        return $this->render('BenchPaginasBundle:Default:cotizaciones.html.twig');
    }



    public function simuladorAction(){

        $data = file_get_contents("http://www.terra.cl/valores/");

        if ( preg_match('|<td align="right" class="texto2">UF : </td>\s+<td class="texto2"><b>(.*?)</b></td>|is' , $data , $cap ) )
        {
            $uf = $cap[1];


        }



        return $this->render('BenchPaginasBundle:Default:simulador.html.twig' , array('uf' => $uf));



    }


    public function simuladorLogAction(){


        $data = file_get_contents("http://www.terra.cl/valores/");

        if ( preg_match('|<td align="right" class="texto2">UF : </td>\s+<td class="texto2"><b>(.*?)</b></td>|is' , $data , $cap ) )
        {
            $uf = $cap[1];


        }

        return $this->render('BenchPaginasBundle:Default:simuladorLog.html.twig' , array('uf' => $uf));


    }


    public function exceptionAction(FlattenException $exception, DebugLoggerInterface $logger = null, $format = 'html', $embedded = false)
    {
        $arraytopass= array('exception' => $exception);

        return  $this->render('DevDreamBundle:Exception:error.html.twig' , $arraytopass);


    }


    public function invitacionAction(){



        return $this->render('BenchPaginasBundle:Default:invitacion.html.twig');

    }






    public function testAction(){

        return $this->render('BenchPaginasBundle:Default:pruebas.html.twig.twig');

    }

    public function quickdemoAction(){

        return $this->render('BenchPaginasBundle:Default:video.html.twig');
    }




    public function indexMobilAction(Request $request) {


        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');



        return $this->render('BenchPaginasBundle:Default:index.html.twig' , array('token' => $token  ));


    }
    public function indexAction(Request $request) {





        if(preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])){



            return $this->redirect($this->generateUrl('homeMobile'));

        }






        $session = $this->getRequest()->getSession();
        $mobil = $session->get('mobil');






        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');



        return $this->render('BenchPaginasBundle:Default:index.html.twig' , array('token' => $token , 'mobil' => $mobil ));


    }

    public function recuperaAction() {

        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        return $this->render('BenchPaginasBundle:Default:recupera.html.twig' , array('token' => $token));
    }

    // Que hacemos
    public function quehacemosAction() {

        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        return $this->render('BenchPaginasBundle:Default:quehacemos.html.twig' , array ('token' => $token  ) ) ;
    }

    public function conocecreditosAction() {


        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        return $this->render('BenchPaginasBundle:Default:conocecreditos.html.twig' , array ('token' => $token ));
    }



    public function logoutAction() {


        $session = $this->getRequest()->getSession();

        $session->clear();


        $mobil = $session->get('mobil');



        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        return $this->render('BenchPaginasBundle:Default:index.html.twig' , array('token' => $token , 'mobil' => $mobil));


        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        return $this->render('BenchPaginasBundle:Default:conocecreditos.html.twig' , array ('token' => $token ));
    }


    // Formulario principal




    public function generateCode($url) {
        $url = $_SERVER['SERVER_NAME'] . '/embed/' . $url;
        $return = '<iframe>'.$url.'</iframe>';
        return $return;
    }



    public function formularioAction() {


        $session = $this->getRequest()->getSession();
        //   $session->start(); server



        $rut = $session->get('rut');
        $user = $session->get('user');

        if ($user == 'nuevo' ){
            $user = 'nuevo';


        }else{

            $user = '';

        }


        if (!($rut)) {


            return $this->render('BenchPaginasBundle:Default:index.html.twig');
        } else {




            $em = $this->getDoctrine()->getManager();




            $em = $this->getDoctrine()->getManager();


            $usuario2 = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

            $id = $usuario2->getId();

            $antecedentes = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));

            $conyuge = $em->getRepository('BenchUsuariosBundle:Conyuge')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));


            $usuario2 = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

            $id = $usuario2->getId();

            $antecedentes = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));

            $conyuge = $em->getRepository('BenchUsuariosBundle:Conyuge')->findOneBy(array('usuario' => $id), array('id' => 'Desc'));


            $tienesHipotecario = $em->getRepository('BenchTienesBundle:Bienesraices')->findBy(array('usuario' => $id));


            $tienesAhorroInversion = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));

            $tienesAutomoviles = $em->getRepository('BenchTienesBundle:Automoviles')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $tienesSociedades = $em->getRepository('BenchTienesBundle:Sociedades')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $tienesHipotecario = $em->getRepository('BenchTienesBundle:Bienesraices')->findBy(array('usuario' => $id));

            $tienesAhorroInversion = $em->getRepository('BenchTienesBundle:Ahorroinverson')->findBy(array('usuario' => $id), array('id' => 'Asc'));

            $tienesAutomoviles = $em->getRepository('BenchTienesBundle:Automoviles')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $tienesSociedades = $em->getRepository('BenchTienesBundle:Sociedades')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $debesHipotecario = $em->getRepository('BenchDebesBundle:Debeshipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $debesHipotecario = $em->getRepository('BenchDebesBundle:Debeshipotecario')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $debesTarjetaCredito = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $debesLineaCredito = $em->getRepository('BenchDebesBundle:DebesLineaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));

            $debesTarjetaCredito = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $debesLineaCredito = $em->getRepository('BenchDebesBundle:DebesLineaCredito')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            $debesCreditoConsumo = $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));



            $debesCreditoConsumo = $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->findBy(array('usuario' => $id), array('id' => 'Asc'));


            // Archivos ****************

            $archivos1 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id , 'tipo' =>'fijo3ultimasliquidaciones' ), array('id' => 'Asc'));
            $archivo2 = $em ->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id , 'tipo' =>'variable6ultimasliquidaciones' ), array('id' => 'Asc'));
            $archivo3 = $em ->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id , 'tipo' =>'cartolaAfp' ), array('id' => 'Asc'));
            $archivo4 = $em ->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id , 'tipo' =>'Circulacion' ), array('id' => 'Asc'));
            $archivo5 = $em ->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id , 'tipo' =>'impuestos3' ), array('id' => 'Asc'));

            $archivo6 = $em  ->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id , 'tipo' =>'Anual' ), array('id' => 'Asc'));
            $archivo7 = $em  ->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $id , 'tipo' =>'Carnet' ), array('id' => 'Asc'));

            //***************************

  /*

            $tmp = "/tmp/indicadores.xml.tmp";

            if(!file_exists($tmp) OR (time() - filemtime($tmp)) > 600 ){
                $c = file_get_contents("http://indicador.eof.cl/xml", "w+");
                if(!empty($c))
                    file_put_contents($tmp, $c);
            }

            if($I = simplexml_load_file($tmp)){
                echo "<ul>";
                foreach($I as $i){
                    list($n) = $i->attributes();
                    if(preg_match("/UF/", $n))
                        $uf = $i;
                }

            }

  */   $uf = '23.936,31';


            $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');


            $credito = $session->get('credito');

            if($credito){


            }else{


                $credito = array(
                    'sueldo' => '',
                    'montoCredito' => '' ,
                    'pie' => '' ,
                    'tipo' => '' ,
                    'plazo' => '',
                    'email' => '',
                    'pieP' => '',
                    'montoPropiedadSin' =>   ' ');




            }

if(!function_exists('el_crypto_hmacSHA1')){

  function el_crypto_hmacSHA1($key, $data, $blocksize = 64) {
        if (strlen($key) > $blocksize) $key = pack('H*', sha1($key));
        $key = str_pad($key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack( 'H*', sha1(
        ($key ^ $opad) . pack( 'H*', sha1(
          ($key ^ $ipad) . $data
        ))
      ));
        return base64_encode($hmac);
    }
  }

  if(!function_exists('el_s3_getTemporaryLink')){
  
    
  function el_s3_getTemporaryLink($accessKey, $secretKey, $bucket, $path, $expires = 336) {

      $expires = time() + intval(floatval($expires) * 60);
   
      $path = str_replace('%2F', '/', rawurlencode($path = ltrim($path, '/')));
      
      $signpath = '/'. $bucket .'/'. $path;

      $signsz = implode("\n", $pieces = array('GET', null, null, $expires, $signpath));
   
      $signature = el_crypto_hmacSHA1($secretKey, $signsz);
      
      $url = sprintf('https://%s.s3.amazonaws.com/%s', $bucket, $path);
     
      $qs = http_build_query($pieces = array(
        'AWSAccessKeyId' => $accessKey,
        'Expires' => $expires,
        'Signature' => $signature,
      ));
     
      return $url.'?'.$qs;
    }
  }

 

$cont = 0;
if($archivos1){
foreach ($archivos1 as $archivos1 ) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion', $archivos1->getLlave().'_'.$archivos1->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' => $archivos1->getId() , 'llave' => $archivos1->getLlave() , 'nombreArchivo' => $archivos1->getnombreArchivo());

    # code...
}

 $archivos1 =  $myArray;
}




   
$cont = 0;
if($archivo2){
foreach ($archivo2 as $archivo2) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion', $archivo2->getLlave().'_'.$archivo2->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' => $archivo2->getId() , 'llave' => $archivo2->getLlave() , 'nombreArchivo' => $archivo2->getnombreArchivo());

    # code...
}

 $archivo2 =  $myArray;
}




   
$cont = 0;
if(  $archivo3){
foreach ($archivo3 as $archivo3) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion', $archivo3->getLlave().'_'.$archivo3->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' => $archivo3->getId() , 'llave' => $archivo3->getLlave() , 'nombreArchivo' => $archivo3->getnombreArchivo());

    # code...
}

 $archivo3 =  $myArray;
}




$cont = 0;
if(  $archivo4){
foreach ($archivo4 as $archivo4) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion', $archivo4->getLlave().'_'.$archivo4->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' => $archivo4->getId() , 'llave' => $archivo4->getLlave() , 'nombreArchivo' => $archivo4->getnombreArchivo());

    # code...
}

 $archivo4 =  $myArray;
}


$cont = 0;
if(  $archivo5){
foreach ($archivo5 as $archivo5) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion', $archivo5->getLlave().'_'.$archivo5->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' => $archivo5->getId() , 'llave' => $archivo5->getLlave() , 'nombreArchivo' => $archivo5->getnombreArchivo());

    # code...
}

 $archivo5 =  $myArray;
}



$cont = 0;
if(  $archivo6){
foreach ($archivo6 as $archivo6) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion', $archivo6->getLlave().'_'.$archivo6->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' => $archivo6->getId() , 'llave' => $archivo6->getLlave() , 'nombreArchivo' => $archivo6->getnombreArchivo());

    # code...
}

 $archivo6 =  $myArray;
}




$cont = 0;
if(  $archivo7){
foreach ($archivo7 as $archivo7) {
    $cont = $cont = $cont + 1;
    $fil3 = el_s3_getTemporaryLink('AKIAIBJXN57FVPAJHIMQ', 'fS38IOGyt2qgbm5HmXaJbmUjzo2/PzmAYda4435U', 'benchbanking-data-produccion', $archivo7->getLlave().'_'.$archivo7->getNombreArchivo());
    $myArray[$cont]  = (object)  array('url' => $fil3 , 'id' => $archivo7->getId() , 'llave' => $archivo7->getLlave() , 'nombreArchivo' => $archivo7->getnombreArchivo());

    # code...
}

 $archivo7 =  $myArray;
}





            return $this->render('BenchPaginasBundle:Default:formulario.html.twig', array('usuario' => $usuario2, 'antecedentes' => $antecedentes, 'conyuge' => $conyuge, 'tienesHipotecario' => $tienesHipotecario, 'tienesAhorroInversion' => $tienesAhorroInversion, 'tienesAutomoviles' => $tienesAutomoviles, 'tienesSociedades' => $tienesSociedades, 'debesHipotecario' => $debesHipotecario, 'debesTarjetaCredito' => $debesTarjetaCredito, 'debesLineaCredito' => $debesLineaCredito, 'debesCreditoConsumo' => $debesCreditoConsumo, 'archivos1' =>  $archivos1 , 'archivo2' => $archivo2 , 'archivo3' => $archivo3 , 'archivo4' => $archivo4 , 'archivo5' => $archivo5 ,
                'archivo6' => $archivo6 , 'archivo7' => $archivo7 , 'uf' => $uf   , 'token' =>  $token , 'credito' => $credito , 'user' => $user ));





        }
    }


    public function ZendDeskAction(){

    }





    public function enviarMailAction(Request $request){


        $request= $this->get('request');


        $nombreMailInfo = $request->request->get('nombreMailInfo');
        $asuntoMailInfo = $request->request->get('asuntoMailInfo');
        $emailMailInfo = $request->request->get('emailMailInfo');
        $mensajeMailInfo = $request->request->get('mensajeMailInfo');

        if( $nombreMailInfo == '' or  $asuntoMailInfo == ''  or     $emailMailInfo == '' or $mensajeMailInfo == '' ){

            $respuesta = 900;

        }else{



            $respuesta = 100;






            $detalleZend ='
  .
  Nombre =   '.$nombreMailInfo.'

  Asunto =  '.$asuntoMailInfo.'

  Email = '.$emailMailInfo.'

  Mensaje =  '.$mensajeMailInfo.'';

            $create = json_encode(
                array(
                    'ticket' => array(
                        'requester' => array(
                            'name' => $nombreMailInfo,
                            'email' => $emailMailInfo,
                        ),
                        'group_id' => '22',

                        'subject' =>'Nueva Pregunta',
                        'custom_fields' =>
                            array(

                                '23412138' => $nombreMailInfo),



                        'description' => $detalleZend

                    )
                ),
                JSON_FORCE_OBJECT
            );

            $zendDesk = new zendesk();
            $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");








        }


        return new Response($respuesta);

    }
    



 public function RecuperContrasenFormAction(Request $request){


          $functClaves = new codificaDec();
          $em = $this->getDoctrine()->getManager();
          $conEnviada =  $request->request->get('contEnviada');
          $contrasenaAc =   $request->request->get('contrasenaAc'); // antigua
          $tokenPOST =  $request->request->get('token');
          $idUser = $request->request->get('idUser');
          $rut =  $request->request->get('rut');
          $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

          $contrasenaCod =   $functClaves->codificar($contrasenaAc , '5512858');

          $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut ));


          if($usuario){



         if($token == $tokenPOST ) {
            $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' =>  $idUser ));

            $apellido = $usuario->getApellido();
            $password_destino1 = $usuario->getContrasena();
            
            $iterations = 20;
            $salt = '5512858';
            $passTemporalCod = hash_pbkdf2("sha512", $conEnviada,   $salt , $iterations,32);

   
        

            $usuario->setNuevoPassword($passTemporalCod);
            $usuario->setActivado('si');
            $usuario->setErrorLogin(0);
            $em->persist($usuario);
            $em->flush();


                   return new Response('100');

        }

  return new Response('300');


            }else{


  return new Response('900');
 
            }
          


 }

    public function VistaRecuperar2Action($id){



  $functClaves = new codificaDec();

  $idUser =   $functClaves->decodificar($id , '5512858');
  $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
  $em = $this->getDoctrine()->getManager();
  $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' =>  $idUser ));
  return $this->render('BenchPaginasBundle:Default:recu2.html.twig' , array('usuario' => $usuario , 'token' => $token ));

    }





    public function recuperaContrasenaAction(Request $request) {

        $request = $this->get('request');
        $email = $request->request->get('email');




        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('email' => $email));


          

        if ($usuario) {

            $email2 = $usuario->getEmail();
            $nombre = $usuario->getNombre();
            $idUser = $usuario->getId();

            $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('id' =>  $idUser ));

            $apellido = $usuario->getApellido();
            $password_destino1 = $usuario->getContrasena();

            $functClaves = new codificaDec();

            $passTemporal = $functClaves->generaPass();
            $passTemporalCod = $functClaves->codificar($passTemporal , '5512858');
            $idCodificada =  $functClaves->codificar($idUser , '5512858');

            $usuario->setContrasena($passTemporalCod);
            $em->persist($usuario);
            $em->flush();


            $address = $email2;
            $postmark = new Postmark("f907f8d4-c537-4c49-8ccc-3b96938cef8a", "info@benchbanking.com", "info@benchbanking.com");

          if($_SERVER['HTTP_HOST'] == 'localhost:8888'){

            $url = 'http://localhost:8888/bench/web/FormularioRecuperaContrasena/'. $idCodificada;
            
            }


          if($_SERVER['HTTP_HOST'] == 'box.benchbanking.com'){
          
          $url = 'https://benchbanking.com/FormularioRecuperaContrasena/'. $idCodificada;
           }



         if($_SERVER['HTTP_HOST'] == 'www.benchbanking.com'){
        

            $url = 'https://benchbanking.com/FormularioRecuperaContrasena/'. $idCodificada;

         }


            $result = $postmark->to($address)
                ->subject(" Hola ¡Olvidaste tu clave! Bench Banking debe decir ¡Recupera tu contraseña de BenchBanking! ")
                ->html_message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

                <title>Mailing BenchBanking</title>
                <style>

        #outlook a {padding:0;}
                    body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
                    .ReadMsgBody {width: 100%;}
                    .ExternalClass {width:100%;}
                    .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
                    table td {border-collapse: collapse;}
                    .ExternalClass * {line-height: 115%;}
                    img{width:100%;}
                    a:link {
                       color: #F60;
                       text-decoration: none;
                   }
                   a:hover {
                       color: #F60;
                       text-decoration: underline;

                   }

                   a:link {
                   text-decoration: none;
}                  }
                   a:visited {
                       font-weight: bold;
                       color: #F60;
                       text-decoration: none;
                   }
                   a:active {
                       font-weight: bold;
                       color: #F60;
                       text-decoration: none;
                   }


                   /* End reset */
                   @media screen and (max-width: 640px){

            *[class="container"] { width: 320px !important; padding:0px !important}
            *[class="mobile-column"] {display: block;}
            *[class="mob-column"] {float: none !important;width: 100% !important;}
            *[class="mobile-padding"] {padding-left:10px !important;padding-right:10px !important;}
            *[class="hide"] {display:none !important;}
            *[class="100p"] {width:100% !important; height:auto !important;}
            *[class="50p"] {width:100% !important; height:auto !important;}
                     img{width:100%;}


                 }

                 .destacado {
                   font-family: Arial, Helvetica, sans-serif;
                   font-size: 24px;
                   color: #FF6600;
               }
           </style>
       </head>

       <body bgcolor="#FFFFFF" style="margin:0px; padding:0px;">
        <table border="0" cellpadding="0" cellspacing="0" class="mobile-padding" style="background-color: #ffffff" width="100%">
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                        <tr>
                            <td align="center" height="100" valign="middle"><img src="https://www.benchbanking.com/imgMaling/header.png"/></td>
                        </tr>
                    </table>

                    <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                        <tr>
                            <td height="20"></td>
                        </tr>
                    </table>

                    <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                        <tr>
                            <td align="center" valign="top"><span class="destacado">¡Olvidaste tu clave!</span></td>
                        </tr>
                    </table>

                    <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                        <tr>
                            <td>
                                <table border="0" cellpadding="10" cellspacing="0">
                                    <tr>

                                    </tr>

                                    <tr>
                                        <td align="left" style="font-size:14px; color: #666; font-family: Verdana, Geneva, sans-serif;"><p>No te preocupes</p>
                                          
                                
                                          
                                          <p>Debes cambiarla  en el siguiente <a href="'.  $url  .'" target="_blank"> Link  </a> '.$url.' </p>

                                          <p>Te saluda,</p>
                                          <p><strong  style="color:#F60">Equipo BenchBanking</strong></p>
                                      </td>

                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>

                  <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td style="border-top: #CCC 1px solid;" height="20"><img src="https://www.benchbanking.com/imgMaling/flow.jpg"/></td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td align="center" valign="top">
                          <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                            <tr>
                                <td align="left" class="mobile-column" valign="top">
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                        <tr>
                                            <td style="background-color: #EEEEEE" valign="top">
                                                <table border="0" cellpadding="10" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif">Contacto</td>
                                                    </tr>

                                                    <tr>
                                                      <td align="left" style="font-size:12px; color: #666; font-family: Verdana, Geneva, sans-serif">Si deseas ponerte en contacto con nosotros<br />
                                                        escríbenos a <a href="mailto:info@benchbanking.com">info@benchbanking.com</a><br />
                                                        o llámanos al <br />
                                                        <a href="tel:+56 2 26402912">+56 2 25709004</a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td align="left" class="mobile-column" valign="top">
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="20">
                                        <tr>
                                            <td align="left" height="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>

                                <td align="left" class="mobile-column" valign="top">
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                        <tr>
                                            <td style="background-color: #EEEEEE" valign="top">
                                                <table border="0" cellpadding="10" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif;">Dirección</td>
                                                    </tr>

                                                    <tr>
                                                        <td align="left" style="font-size:12px; color: #666; font-family: Verdana, Geneva, sans-serif;">Guardia Vieja 181, Oficina 506, Providencia.</br></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="20">
                                            <tr>
                                                <td align="left" height="20">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                            <tr>
                                                <td style="background-color: #EEEEEE" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0">
                                                        <tr>
                                                            <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif;">Financiado por:</td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" style="font-size:14px; color:#333333; font-family:Arial, Helvetica, sans-serif;"><p><img src="http://www.benchbanking.com/imgMaling/logo01.png"/></p>
                                                              <p>
                                                                <img src="https://www.benchbanking.com/imgMaling/logo02.png"/></p></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                        <tr>
                            <td height="20"></td>
                        </tr>
                    </table>

                    <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                        <tr>
                            <td style="background-color: #F60">&nbsp;</td>
                        </tr>
                    </table>

                    <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                        <tr>
                            <td height="20"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
    </html>')->send();


            $respuesta = 200;

            return new Response($respuesta);
        } else {



            $respuesta = "<p> Este correo no esta registrado en nuestra base datos </p>";


            return new Response($respuesta);
        }
    }







}
