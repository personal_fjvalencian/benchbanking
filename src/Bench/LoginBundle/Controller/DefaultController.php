<?php

namespace Bench\LoginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\RegistrosBundle\Controller\CodificaAnt;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Bench\UsuariosBundle\Controller\verificaRut;


class DefaultController extends Controller {

    public function indexAction($name) {
        
        
        return $this->render('BenchLoginBundle:Default:index.html.twig', array('name' => $name));
        
        
    }

    public function loginAction(Request $request) {


        $rut = $request->request->get('rut');
        $contrasenaOr = $request->request->get('contrasena');
        $tokenpost = $request->request->get('token');
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $session->start(  );
        $verificaRut = new verificaRut();
        $verifica  = $verificaRut->valida_rut($rut);
         
         if($verifica == 1){

     
        if($tokenpost == $token){

        $usuarioBus = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut ));

        if($usuarioBus){
        
        $veces = $usuarioBus->getErrorLogin();
    

          if(  $veces > 10){
          
          $usuarioBus->setActivado('no');
          $em->persist($usuarioBus);
          $em->flush();



          return new Response ('<p>Debes solicitar activar el usuario  o recupera tu contraseña </p>');

          }
          $usuarioBus->setErrorLogin($veces + 1);
          $em->persist($usuarioBus);
          $em->flush();
        }

  
        $salt = '5512858';
        $iterations = 20;
    
        $session->set('rut', $rut);
        $_SESSION['rut3'] = $rut;
        $em = $this->getDoctrine()->getManager();

       $CodificaAnt = new CodificaAnt();
       $contrasena1 =  $CodificaAnt->codificar($contrasenaOr, "5512858");
     


        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut, 'contrasena' => $contrasena1  ));

        if($usuario){



            $estadoPass = $usuario->getEstadoPassword();
            if(    $estadoPass  == ''){


            $iterations = 20;
            $salt = '5512858';
            $pas2 = hash_pbkdf2("sha512",  $contrasenaOr ,  $salt , $iterations,32);


            $usuario->setEstadoPassword('activado');
            $usuario->setNuevoPassword($pas2);

            $usuario->setErrorLogin(0);

            $em->persist($usuario);
            $em->flush();



            }


        }



        $contrasena =   $pas2 = hash_pbkdf2("sha512",  $contrasenaOr ,  $salt , $iterations,32);
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut, 'nuevoPassword' => $contrasena  ));

          
    

         if ($usuario) {
         
             $veces = $usuario->getVeceslogin();
          
          if($veces == ''){
              
              $veces = 1;
          }else{
              
              $veces = $veces +1;
          }
            
            $navegador = ($_SERVER['HTTP_USER_AGENT']);
            $usuario->setNavegador($navegador);
            $usuario->setVeceslogin($veces);
            $em->persist($usuario);
            $em->flush();


          $estado = $usuario->getActivado();
          
          if($estado == 'si'){
           
           $return = ('100');
           
           }else{

               $return = ('<p>Debes activar el usuario </p>');

           }
            
           return new Response($return); //  
        } else {
            
        


            $return = ('<p>Combinación incorrecta de rut y clave</p>');

            
        }

            return new Response($return); //  
        }

        return new Response('token invalido');
    }


  }

}
