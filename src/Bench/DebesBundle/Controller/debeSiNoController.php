<?php

namespace Bench\DebesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\TienesBundle\Entity\Bienesraices;
use Bench\TienesBundle\Entity\Sociedades;
use Bench\TienesBundle\Entity\Automoviles;
use Bench\TienesBundle\Entity\Ahorroinverson;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class debeSiNoController extends Controller {





    public function debesBienesRaicesSiNoAction(Request $request) {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $Sino = $request->request->get('Sino');
       // $usuario = new Usuario;
        $usuario->setDebesHipotecarioSiNo($Sino);

        $em->merge($usuario); // merge se usa en el caso de un update 
        $em->flush();


        $return = ' 100';


        return new Response($return);
    }
    
    
    
      public function debesTarjetaCreditoSiNoAction(Request $request) {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $Sino = $request->request->get('Sino');
     // $usuario = new Usuario;
        $usuario->setDebesTarjetaCreditoSiNo($Sino);

        $em->merge($usuario); // merge se usa en el caso de un update 
        $em->flush();


        $return = ' 100';


        return new Response($return);
    }

    
    
    
       public function debesLineaCreditoSiNoAction(Request $request) {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $Sino = $request->request->get('Sino');
      //  $usuario = new Usuario;
        $usuario->setDebesLineaCreditoSiNo($Sino);

        $em->merge($usuario); // merge se usa en el caso de un update 
        $em->flush();


        $return = ' 100';


        return new Response($return);
    }
  
    
    
    
    
       public function debesCreditoConsumoSiNoAction(Request $request) {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $Sino = $request->request->get('Sino');
    //  $usuario = new Usuario;
        $usuario->setDebesCreditoConsumoSiNo($Sino);
        

        $em->merge($usuario); // merge se usa en el caso de un update 
        $em->flush();


        $return = ' 100';


        return new Response($return);
    }
    
    

}
