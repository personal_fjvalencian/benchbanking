<?php

namespace Bench\DebesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bench\UsuariosBundle\Entity\Usuario;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Bench\DebesBundle\Entity\Debeshipotecario;
use Bench\DebesBundle\Entity\DebesTarjetaCredito;
use Bench\DebesBundle\Entity\DebesLineaCredito;
use Bench\DebesBundle\Entity\DebesCreditoConsumo;
use Bench\UsuariosBundle\Entity\HistoricoFechaAct;
use \DateTime;

class DefaultController extends Controller {

    public function indexAction($name) {
        return $this->render('BenchDebesBundle:Default:index.html.twig', array('name' => $name));
    }

    public function deldebesCreditoconsumoAction(Request $request) {
        $request = $this->get('request');
        $request->headers->addCacheControlDirective('no-store', true);

        $em = $this->getDoctrine()->getManager();
        $recordToDelete = $request->request->get('recordToDelete');
        $DebesCreditoConsumo = $em->getRepository('BenchDebesBundle:DebesCreditoConsumo')->find($recordToDelete);
        $em->remove($DebesCreditoConsumo);
        $em->flush();



        $return = ' 100';


        return new Response($return); //make sure it has the correct content type
    }

    public function creditoConsumoAction(Request $request) {


         $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
         $request->headers->addCacheControlDirective('no-store', true);
         $tokePost = $request->request->get('token');
         
         if($token == $tokePost ){
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $InstitucionConsumo = $request->request->get('InstitucionConsumo');
        $PagoMensualConsumo = $request->request->get('PagoMensualConsumo');
        $deudaTotalConsumo = $request->request->get('deudaTotalConsumo');
        $VencimientoFinalConsumo = $request->request->get('VencimientoFinalConsumo');
        $DeudaVigenteConsumo = $request->request->get('DeudaVigenteConsumo');


        if ($InstitucionConsumo == '' or $PagoMensualConsumo == '' or $deudaTotalConsumo == '' or $VencimientoFinalConsumo == '' or $DeudaVigenteConsumo == '') {

            $respuesta = '200';


            return new Response($respuesta);
        } else {



            $DebesCreditoConsumo = new DebesCreditoConsumo();

            $DebesCreditoConsumo->setUsuario($usuario);
            $DebesCreditoConsumo->setInstitucion($InstitucionConsumo);
            $DebesCreditoConsumo->setPagomensual($PagoMensualConsumo);
            $DebesCreditoConsumo->setDeudatotal($deudaTotalConsumo);
            $DebesCreditoConsumo->setFechaultimopago($VencimientoFinalConsumo);
            $DebesCreditoConsumo->setDeudavigente($DeudaVigenteConsumo);
            $DebesCreditoConsumo->setFecha(new \DateTime());

            $em->persist($usuario);
            $em->persist($DebesCreditoConsumo);
            $em->flush();

            $id = $DebesCreditoConsumo->getId();


            $response = '
                
  <tbody style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">

          <tr  id="itemDebesConsumo_' . $id . '" style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;" >
				
					<td><p style="text-align:center;color:#666;">' . $InstitucionConsumo . '</p></td>
					<td><p style="text-align:center;color:#666;">' . $PagoMensualConsumo . '</p></td>
					<td><p style="text-align:center;color:#666;">' . $deudaTotalConsumo . '</p></td>
					<td><p style="text-align:center;color:#666;">' . $VencimientoFinalConsumo . '</p></td>
				  <td><p style="text-align:center;color:#666;">' . $DeudaVigenteConsumo . '</p></td>
          <td><p style="text-align:center;color:#666;"> <a href="#" class="del_button" id="del-' . $id . '">eliminar</a> </p> </td>

          </tr>
          </tbody>

                

';

            
         /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
      
        
           if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        

        /* fin de Actualizar estado */
        

            $dts = ( new \DateTime());
            $fecha = $dts->format('d-m-Y H:i:s');
            $usuario->setFechaactualizacionFinal($fecha);


            $usuario->setDebesCreditoConsumoSiNo('SI');

            $em->merge($usuario);
            $em->flush();



            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Debes Credito Consumo');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();




            return new Response($response);
            
        }
        }
    }

    public function deldebesHipotecarioAction(Request $request) {
        $request->headers->addCacheControlDirective('no-store', true);
        $em = $this->getDoctrine()->getManager();
        $recordToDelete = $request->request->get('recordToDelete');
        $Debeshipotecario = $em->getRepository('BenchDebesBundle:Debeshipotecario')->find($recordToDelete);
        $em->remove($Debeshipotecario);
        $em->flush();



        $return = ' 100';


        return new Response($return); //make sure it has the correct content type
    }

    //// GUARDA hipotecario
    public function hipotecarioAction(Request $request) {

         $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
         $tokenPost = $request->request->get('token');
         $request->headers->addCacheControlDirective('no-store', true);
         
         if($token == $tokenPost) {
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
        $institucion = $request->request->get('institucion');
        $pagoM = $request->request->get('pagoM');
        $deudaT = $request->request->get('deudaT');
        $vencimientoF = $request->request->get('vencimientoF');
        $deudaV = $request->request->get('deudaV');


        if ($institucion == '' or $pagoM == '' or $deudaT == '' or $vencimientoF == '' or $deudaV == '') {
            $reponse = '200';

            return new Response($reponse);
        } else {




            $debesHipotecario = new Debeshipotecario();

            $debesHipotecario->setUsuario($usuario);
            $debesHipotecario->setInstitucion($institucion);
            $debesHipotecario->setPagomensual($pagoM);
            $debesHipotecario->setDeudatotal($deudaT);
            $debesHipotecario->setVenciminetofinal($vencimientoF);
            $debesHipotecario->setDuedavigente($deudaV);
            $debesHipotecario->setFecha(new \DateTime());

            $em->persist($usuario);
            $em->persist($debesHipotecario);
            $em->flush();

            $id = $debesHipotecario->getId();


            $request = '
              
               <tbody style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">
                <tr id="itemDebesHipotecario_' . $id . '" style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">

                    <td> <p style="text-align:center; color: #666;">' . $institucion . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $pagoM . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $deudaT . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $vencimientoF . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $deudaV . '</p></td>
                    
                    <td><p style="text-align:center;color:#666;"> <a href="#" class="del_button" id="del-' . $id . '">eliminar</a> </p> </td>
                  </tr>
                  </tbody>
                  ';
            
            
        /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
    
        
           if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        
        
        /* fin de Actualizar estado */
            
            
             $dts =( new \DateTime());
             $fecha=$dts->format('d-m-Y H:i:s');
             $usuario->setFechaactualizacionFinal($fecha);

            $usuario->setDebesHipotecarioSiNo('SI');

            $em->merge($usuario);

            $em->flush();
            
            
            
            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Debes Hipotecario');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();



            return new Response($request);
        }
        }
    }

    public function deldebesTarjetaAction(Request $request) {
        
        $request->headers->addCacheControlDirective('no-store', true);
        $em = $this->getDoctrine()->getManager();
        $recordToDelete = $request->request->get('recordToDelete');
        $Debeshipotecario = $em->getRepository('BenchDebesBundle:DebesTarjetaCredito')->find($recordToDelete);
        $em->remove($Debeshipotecario);
        $em->flush();





        $return = ' 100';


        return new Response($return); //make sure it has the correct content type
    }

    /// SAVE TARJETA CREDITOS

    public function tarjetacreditoAction(Request $request) {


        $request->headers->addCacheControlDirective('no-store', true);
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
        $institucion = $request->request->get('institucion');
        $montoAprobado = $request->request->get('montoAprobado');
        $tipoTarjeta = $request->request->get('tipoTarjeta');
        $montoUtilizado = $request->request->get('montoUtilizado');

        if ($institucion == '' or $montoAprobado == '' or $tipoTarjeta == '' or $montoUtilizado == '') {



            $response = '200';

            return new Response($response);
        } else {




            $debesTarjetaCredito = new DebesTarjetaCredito();
            $debesTarjetaCredito->setUsuario($usuario);
            $debesTarjetaCredito->setInstitucion($institucion);
            $debesTarjetaCredito->setMontoaprobado($montoAprobado);
            $debesTarjetaCredito->setTipotarjeta($tipoTarjeta);
            $debesTarjetaCredito->setMontoutilizado($montoUtilizado);
            $debesTarjetaCredito->setFecha(new \DateTime());
            $em->persist($usuario);
            $em->persist($debesTarjetaCredito);
            $em->flush();
            $id = $debesTarjetaCredito->getId();
            $reponse = '
             <tbody style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">
            <tr  id="itemTarjeta_' . $id . '" style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">
				
					<td><p style="text-align:center;color:#666;">' . $tipoTarjeta . '</p></td>
					<td><p style="text-align:center;color:#666;">' . $institucion . '</p></td>
					<td><p style="text-align:center;color:#666;">' . $montoAprobado . '</p></td>
					<td><p style="text-align:center;color:#666;">' . $montoUtilizado . '</p></td>
				
          <td><p style="text-align:center;color:#666;"> <a href="#" class="del_button" id="del-' . $id . '">eliminar</a> </p> </td>

          </tr>
          </tbody>
';
            
        /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
      
        
        
       if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        
        /* fin de Actualizar estado */
             $dts =( new \DateTime());
             $fecha=$dts->format('d-m-Y H:i:s');
             $usuario->setFechaactualizacionFinal($fecha);

             
            $usuario->setDebesTarjetaCreditoSiNo('SI');
            $em->merge($usuario);
            $em->flush();
            
            
            
             $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Debes Tarjeta Credito');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();




            return new Response($reponse);
        }
    }

    public function lineacreditoAction(Request $request) {

        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $institucion = $request->request->get('institucion');
        $montoAprobado = $request->request->get('montoAprobado');
        $montoUtilizado = $request->request->get('montoUtilizado');
        $vencimiento = $request->request->get('vencimiento');

        if ($institucion == '' or $montoAprobado == '' or $montoUtilizado == '' or $vencimiento == '') {


            $reponse = '200';


            return new Response($reponse);
        } else {




            $DebesLineaCredito = new DebesLineaCredito();


            $DebesLineaCredito->setUsuario($usuario);
            $DebesLineaCredito->setInstitucion($institucion);
            $DebesLineaCredito->setMontoAprobado($montoAprobado);
            $DebesLineaCredito->setMontoUtilizado($montoUtilizado);
            $DebesLineaCredito->setVencimientofinal($vencimiento);
            $DebesLineaCredito->setFecha(new \DateTime());
            
           

             
            $em->persist($usuario);
            $em->persist($DebesLineaCredito);
            $em->flush();
            $id = $DebesLineaCredito->getId();


            

            $reponse = '
        
                <tbody style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">
            <tr  id="itemLinea_' . $id . '" style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">
				
					<td><p style="text-align:center;color:#666;">' . $institucion . '</p></td>
					<td><p style="text-align:center;color:#666;">' . $montoAprobado . '</p></td>
					<td><p style="text-align:center;color:#666;">' . $montoUtilizado . '</p></td>
				  <td><p style="text-align:center;color:#666;">' . $vencimiento . '</p></td>
          <td><p style="text-align:center;color:#666;"> <a href="#" class="del_button" id="del-' . $id . '">eliminar</a> </p> </td>

          </tr>
          </tbody>
          
';
            
   
            /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
     
        
          if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        
        /* fin de Actualizar estado */
        
        
             $dts =( new \DateTime());
             $fecha=$dts->format('d-m-Y H:i:s');
             $usuario->setFechaactualizacionFinal($fecha);


            $usuario->setDebesLineaCreditoSiNo('SI');

            $em->merge($usuario);
            $em->flush();
            
            
            
            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Debes Linea Credito');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();


            return new Response($reponse);
            
            
        }
    }

    /// GUARDAR SOCIEDADES 





    public function deldebeslineaCreditoAction(Request $request) {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $recordToDelete = $request->request->get('recordToDelete');
        $DebesLineaCredito = $em->getRepository('BenchDebesBundle:DebesLineaCredito')->find($recordToDelete);
        $em->remove($DebesLineaCredito);

        $em->flush();



        $return = ' 100';


        return new Response($return); //make sure it has the correct content type
    }

}
