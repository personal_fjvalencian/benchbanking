<?php

namespace Bench\DebesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DebesLineaCredito
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class DebesLineaCredito
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
 

 protected $id;


 /** @ORM\Column(type="string", length=100) */


    protected $institucion;

   /** @ORM\Column(type="string", length=100 , nullable=true) */




    protected $montoAprobado;

    

 /** @ORM\Column(type="string", length=100 , nullable=true) */

 


    protected  $montoUtilizado;
 /** @ORM\Column(type="string", length=100 , nullable=true) */




    protected $vencimientofinal;




   /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="debeslineacredito")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    

protected $usuario;





   /** @ORM\Column(type="datetime") */


protected $fecha;

    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return DebesLineaCredito
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set montoAprobado
     *
     * @param integer $montoAprobado
     * @return DebesLineaCredito
     */
    public function setMontoAprobado($montoAprobado)
    {
        $this->montoAprobado = $montoAprobado;
    
        return $this;
    }

    /**
     * Get montoAprobado
     *
     * @return integer 
     */
    public function getMontoAprobado()
    {
        return $this->montoAprobado;
    }

    /**
     * Set montoUtilizado
     *
     * @param integer $montoUtilizado
     * @return DebesLineaCredito
     */
    public function setMontoUtilizado($montoUtilizado)
    {
        $this->montoUtilizado = $montoUtilizado;
    
        return $this;
    }

    /**
     * Get montoUtilizado
     *
     * @return integer 
     */
    public function getMontoUtilizado()
    {
        return $this->montoUtilizado;
    }

    /**
     * Set vencimientofinal
     *
     * @param string $vencimientofinal
     * @return DebesLineaCredito
     */
    public function setVencimientofinal($vencimientofinal)
    {
        $this->vencimientofinal = $vencimientofinal;
    
        return $this;
    }

    /**
     * Get vencimientofinal
     *
     * @return string 
     */
    public function getVencimientofinal()
    {
        return $this->vencimientofinal;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return DebesLineaCredito
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return DebesLineaCredito
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}