<?php

namespace Bench\DebesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Debeshipotecario
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Debeshipotecario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    
    
     protected $id;

   
     
     
     /** @ORM\Column(type="string", length=100) */
     
     protected $institucion;
    
    
     
     /** @ORM\Column(type="string", length=100) */
     
     protected $pagomensual;
    
     
     /** @ORM\Column(type="string", length=100) */
     
     protected  $deudatotal;
    
    
     
     /** @ORM\Column(type="string", length=100) */
     
     
     protected $venciminetofinal;
    
    
     /** @ORM\Column(type="string", length=100) */
     
     
     
     protected $duedavigente;
    
    
     
   /** @ORM\Column(type="datetime") */
    
    
    
    protected $fecha;
    
    

    /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="debeshipotecario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
   

    
    protected $usuario;
    
    

 
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return Debeshipotecario
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set pagomensual
     *
     * @param string $pagomensual
     * @return Debeshipotecario
     */
    public function setPagomensual($pagomensual)
    {
        $this->pagomensual = $pagomensual;
    
        return $this;
    }

    /**
     * Get pagomensual
     *
     * @return string 
     */
    public function getPagomensual()
    {
        return $this->pagomensual;
    }

    /**
     * Set deudatotal
     *
     * @param string $deudatotal
     * @return Debeshipotecario
     */
    public function setDeudatotal($deudatotal)
    {
        $this->deudatotal = $deudatotal;
    
        return $this;
    }

    /**
     * Get deudatotal
     *
     * @return string 
     */
    public function getDeudatotal()
    {
        return $this->deudatotal;
    }

    /**
     * Set venciminetofinal
     *
     * @param string $venciminetofinal
     * @return Debeshipotecario
     */
    public function setVenciminetofinal($venciminetofinal)
    {
        $this->venciminetofinal = $venciminetofinal;
    
        return $this;
    }

    /**
     * Get venciminetofinal
     *
     * @return string 
     */
    public function getVenciminetofinal()
    {
        return $this->venciminetofinal;
    }

    /**
     * Set duedavigente
     *
     * @param string $duedavigente
     * @return Debeshipotecario
     */
    public function setDuedavigente($duedavigente)
    {
        $this->duedavigente = $duedavigente;
    
        return $this;
    }

    /**
     * Get duedavigente
     *
     * @return string 
     */
    public function getDuedavigente()
    {
        return $this->duedavigente;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Debeshipotecario
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Debeshipotecario
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}