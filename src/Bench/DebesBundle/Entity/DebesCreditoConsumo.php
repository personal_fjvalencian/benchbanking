<?php

namespace Bench\DebesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DebesCreditoConsumo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class DebesCreditoConsumo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
       /** @ORM\Column(type="string", length=100 , nullable=true) */
    private $institucion;
    
       /** @ORM\Column(type="string", length=100 , nullable=true) */
    
    private $pagomensual;
    
       /** @ORM\Column(type="string", length=100 , nullable=true) */
    
    private $deudatotal;
    
       /** @ORM\Column(type="string", length=100 , nullable=true) */
    
    private $fechaultimopago;
    
     /** @ORM\Column(type="string", length=100 , nullable=true) */
    
    private $deudavigente;
    

       
  
   /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="debescreditoconsumo")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    
    
    private $usuario;
    
    
    

  /** @ORM\Column(type="datetime") */
    
    private $fecha;
    
    
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set institucion
     *
     * @param integer $institucion
     * @return DebesCreditoConsumo
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return integer 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set pagomensual
     *
     * @param integer $pagomensual
     * @return DebesCreditoConsumo
     */
    public function setPagomensual($pagomensual)
    {
        $this->pagomensual = $pagomensual;
    
        return $this;
    }

    /**
     * Get pagomensual
     *
     * @return integer 
     */
    public function getPagomensual()
    {
        return $this->pagomensual;
    }

    /**
     * Set deudatotal
     *
     * @param integer $deudatotal
     * @return DebesCreditoConsumo
     */
    public function setDeudatotal($deudatotal)
    {
        $this->deudatotal = $deudatotal;
    
        return $this;
    }

    /**
     * Get deudatotal
     *
     * @return integer 
     */
    public function getDeudatotal()
    {
        return $this->deudatotal;
    }

    /**
     * Set fechaultimopago
     *
     * @param integer $fechaultimopago
     * @return DebesCreditoConsumo
     */
    public function setFechaultimopago($fechaultimopago)
    {
        $this->fechaultimopago = $fechaultimopago;
    
        return $this;
    }

    /**
     * Get fechaultimopago
     *
     * @return integer 
     */
    public function getFechaultimopago()
    {
        return $this->fechaultimopago;
    }

    /**
     * Set deudavigente
     *
     * @param integer $deudavigente
     * @return DebesCreditoConsumo
     */
    public function setDeudavigente($deudavigente)
    {
        $this->deudavigente = $deudavigente;
    
        return $this;
    }

    /**
     * Get deudavigente
     *
     * @return integer 
     */
    public function getDeudavigente()
    {
        return $this->deudavigente;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return DebesCreditoConsumo
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return DebesCreditoConsumo
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}