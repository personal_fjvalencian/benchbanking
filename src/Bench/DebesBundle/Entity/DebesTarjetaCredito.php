<?php

namespace Bench\DebesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DebesTarjetaCredito
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class DebesTarjetaCredito
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


  
 /** @ORM\Column(type="string", length=100) */




    protected $tipotarjeta;


 /** @ORM\Column(type="string", length=100) */




    protected $institucion;

/** @ORM\Column(type="string", length=100 , nullable=true) */

     

    protected $montoaprobado;



/** @ORM\Column(type="string", length=100 , nullable=true) */

     

    protected $montoutilizado;




   /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="debestarjetacredito")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    

protected $usuario;




   /** @ORM\Column(type="datetime") */


protected $fecha;

    
    

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipotarjeta
     *
     * @param string $tipotarjeta
     * @return DebesTarjetaCredito
     */
    public function setTipotarjeta($tipotarjeta)
    {
        $this->tipotarjeta = $tipotarjeta;
    
        return $this;
    }

    /**
     * Get tipotarjeta
     *
     * @return string 
     */
    public function getTipotarjeta()
    {
        return $this->tipotarjeta;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return DebesTarjetaCredito
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set montoaprobado
     *
     * @param integer $montoaprobado
     * @return DebesTarjetaCredito
     */
    public function setMontoaprobado($montoaprobado)
    {
        $this->montoaprobado = $montoaprobado;
    
        return $this;
    }

    /**
     * Get montoaprobado
     *
     * @return integer 
     */
    public function getMontoaprobado()
    {
        return $this->montoaprobado;
    }

    /**
     * Set montoutilizado
     *
     * @param integer $montoutilizado
     * @return DebesTarjetaCredito
     */
    public function setMontoutilizado($montoutilizado)
    {
        $this->montoutilizado = $montoutilizado;
    
        return $this;
    }

    /**
     * Get montoutilizado
     *
     * @return integer 
     */
    public function getMontoutilizado()
    {
        return $this->montoutilizado;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return DebesTarjetaCredito
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return DebesTarjetaCredito
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}