


$(document).ready(function() {

	$('#SiguienteDebesCreditoConsumo').click(function(event){

	
         $(".spinner").css("display", "block");

   if ($('#respuesta4>tbody>tr').length   >  0){
       
                $(".spinner").css("display", "none");
    $('#tab1 a[href="#blue"]').tab('show');
  $('#linkdondecotizar').prop('disabled' , false);
  $("#linkdondecotizar").popover('destroy');

       var trackPageview = '/formulario/donde-cotizar';
       dataLayer.push({
           'event':'PageView',
           'trackPageview': trackPageview });



   }else{
                $(".spinner").css("display", "none");
       
       $('#debes_credito_consumo').parsley('validate');
       $('#debes_credito22').parsley('validate');
                    
       
       
   }
   
        });


    $("#guardaConsumo").click(function(e) {

                 $(".spinner").css("display", "block");

        e.preventDefault();
        var myData = 'InstitucionConsumo=' + $("#InstitucionConsumo").val()
                + '&PagoMensualConsumo=' + $("#PagoMensualConsumo").val()
                + '&deudaTotalConsumo=' + $("#deudaTotalConsumo").val()
                + '&VencimientoFinalConsumo=' + $("#VencimientoFinalConsumo").val()
                + '&DeudaVigenteConsumo=' + $("#DeudaVigenteConsumo").val()
                + '&token=' +$('#token').val()
        
                ;


        jQuery.ajax({
            type: "POST", // metodo post o get 
            url: $("#debes_credito_consumo").attr("action"),
            dataType: "text", // tipo datos text o json etc 
            data: myData,
            success: function(response) {

                         $(".spinner").css("display", "none");





                var respuesta = response;


                if (respuesta == '200') {

                    $('#debes_credito_consumo').parsley('validate');
                    $('#debes_credito22').parsley('validate');
                    
                    


                } else {




                    var trackPageview = '/formulario/que-debes/credito-consumo/Guardado';
                    dataLayer.push({
                        'event':'PageView',
                        'trackPageview': trackPageview });







                    $("#respuesta4").append(response);
                     $('#linkcotizar').prop('disabled' , false);
                     $("#linkcotizar").popover('destroy');

                }


            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);



            }

        });
    });



// eliminar 

    $("#respuesta4").on("click", ".del_button", function(e) {
        e.preventDefault();
        var clickedID = this.id.split('-');
        var DbNumberID = clickedID[1]; // toma el numero del array

        var myData = 'recordToDelete=' + DbNumberID;



        jQuery.ajax({
            type: "POST",
            url: $("#debescreditoconsumoForm").attr("action"),
            dataType: "text",
            data: myData,
            success: function(response) {

                $('#itemDebesConsumo_' + DbNumberID).remove();

            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError);

            }

        });
    });

});