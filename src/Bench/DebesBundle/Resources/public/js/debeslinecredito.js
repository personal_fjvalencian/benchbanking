/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {



    $('#SiguienteDebesTarjetaCredito').click(function(event) {
         $(".spinner").css("display", "block");


        if ($('#respuesta3>tbody>tr').length > 0) {

                     $(".spinner").css("display", "none");


            $('#ulDebes a[href="#debesCreditoConsumo"]').tab('show');
            $('#linkCreditoConsumo').prop('disabled', false);
            $("#linkCreditoConsumo").popover('destroy');


        }else{

                     $(".spinner").css("display", "none");
            
                      $('#debesLineacredito2').parsley('validate');
            
        }

    });

    $("#guardatarjetaC").click(function(e) {

                 $(".spinner").css("display", "block");

        e.preventDefault();


        var myData = 'id=' + $("#debesTCretipoid").val()
                + '&institucion=' + $("#debesTarjetainstitucion").val()
                + '&montoAprobado=' + $("#debesTarjetamontoAprobado").val()
                + '&tipoTarjeta=' + $("#debesTarjetatipo").val()
                + '&montoUtilizado=' + $("#debesTarjetamontoUtilizado").val()
                + '&vencimiento=' + $("#debesTarjetavencimiento").val()

                ;


        jQuery.ajax({
            type: "POST", // metodo post o get 
            url: $("#debesLineacredito2").attr("action"),
            dataType: "text", // tipo datos text o json etc 
            data: myData,
            success: function(response) {

                         $(".spinner").css("display", "none");

                var respuesta = response;


                if (respuesta == '200') {


                    $('#debesLineacredito2').parsley('validate');


                } else {



                    var trackPageview = '/formulario/que-debes/linea-credito/Guardado';
                    dataLayer.push({
                        'event':'PageView',
                        'trackPageview': trackPageview });

  
   

                    $('#linkTarjetaCredito2').prop('disabled', false);
                    $("#linkTarjetaCredito2").popover('destroy');
                    $("#respuesta3").append(response);


                }


            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);


            }

        });
    });



// eliminar 

    $("#respuesta3").on("click", ".del_button", function(e) {
        e.preventDefault();
        var clickedID = this.id.split('-');
        var DbNumberID = clickedID[1]; // toma el numero del array

        var myData = 'recordToDelete=' + DbNumberID;



        jQuery.ajax({
            type: "POST",
            url: $("#debeslineacreditoForm").attr("action"),
            dataType: "text",
            data: myData,
            success: function(response) {

                $('#itemLinea_' + DbNumberID).remove();

            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError);

            }

        });
    });

});