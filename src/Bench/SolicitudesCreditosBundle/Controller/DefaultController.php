<?php

namespace Bench\SolicitudesCreditosBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Bench\SolicitudesCreditosBundle\Entity\SolicitudesCreditos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\UsuariosBundle\Entity\Antecedenteslaboral;
use Bench\UsuariosBundle\mail\Postmark;
use Bench\UsuariosBundle\Entity\autorizoDicom;
use Bench\RegistrosBundle\Controller\zendesk;

class DefaultController extends Controller {

    public function indexAction($name) {
        return $this->render('BenchSolicitudesCreditosBundle:Default:index.html.twig', array('name' => $name));
    }

    public function guardaSolicitudAction(Request $request) {

        $request->headers->addCacheControlDirective('no-store', true);
        $rut = $request->request->get('rut');
        $contrasena = $request->request->get('contrasena');
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $rut = $session->get('rut');

        $idCredito = $session->get('idCredito');
        $tipoCredito = $session->get('tipoCredito');


        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $FINANCIALGROUP = $request->request->get('FINANCIALGROUP');
        $CONSORCIO = $request->request->get('CONSORCIO');
        $BCI = $request->request->get('BCI');
        $SCOTIABANK = $request->request->get('SCOTIABANK');
        $SANTANDER = $request->request->get('SANTANDER');
        $ITAU = $request->request->get('ITAU');
        $BICE = $request->request->get('BICE');
        $CHILE = $request->request->get('CHILE');
        $SECURITY = $request->request->get('SECURITY');
        $BBVA = $request->request->get('BBVA');
        $METLIFE = $request->request->get('METLIFE');
        
        $SolicitudesCreditos = new SolicitudesCreditos();
        $SolicitudesCreditos->setFinancialgroup($FINANCIALGROUP);
        $SolicitudesCreditos->setConsorcio($CONSORCIO);
        $SolicitudesCreditos->setBci($BCI);
        $SolicitudesCreditos->setScotiabank($SCOTIABANK);
        $SolicitudesCreditos->setSantander($SANTANDER);
        $SolicitudesCreditos->setItau($ITAU);
        $SolicitudesCreditos->setBci($BICE);
        $SolicitudesCreditos->setBancoChile($CHILE);
        $SolicitudesCreditos->setBancoSecurity($SECURITY);
        $SolicitudesCreditos->setBbva($BBVA);
        $SolicitudesCreditos->setMetlife($METLIFE);
        $SolicitudesCreditos->setTipo($tipoCredito);
        $SolicitudesCreditos->setCreditoid($idCredito);
        $SolicitudesCreditos->setUsuario($usuario);
        $em->persist($SolicitudesCreditos);
        $em->persist($usuario);
        $em->flush();
        
        $rut = $request->request->get('rut');
        $contrasena = $request->request->get('contrasena');
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $rut = $session->get('rut');

       $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
       $usuarioId =  $usuario->getId();
       $antecedente = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $usuarioId), array('id' => 'Desc'));
 
        $usuario->setFinal('SI');
        $usuario->setArchivar('no');
        $em->merge($usuario);
        $em->flush();

        $session = $this->getRequest()->getSession();
        $estado = $request->request->get('autorizoDicom');


        $idCredito = $session->get('idCredito');

        if (empty($idCredito)) {

            $idCredito = 0;
        }


        $tipoCredito = $session->get('tipoCredito');
        $pieS = $session->get('pie');
        $montoS = $session->get('monto');



        if (empty($tipoCredito)) {

            $tipoCredito = 0;
            $pieS = '';
            $montoS = '';
        }

        if ($estado == 'SI') {
            
        } else {

            $estado = 'NO';
        }



        // autorizacion Dicom 
        $autorizarDicom = new autorizoDicom();
        $autorizarDicom->setCreditoId($idCredito);
        $autorizarDicom->setTipo($tipoCredito);
        $autorizarDicom->setFecha(new \DateTime());

        $autorizarDicom->setEstado($estado);
        $autorizarDicom->setUsuario($usuario);

        $em->persist($autorizarDicom);
        $em->persist($usuario);
        $em->flush();




        $usuario->setAutorizoDicomSiNo($estado);
        $em->merge($usuario);
        $em->flush();





        $nombre = $usuario->getNombre();
        $apellido1 = $usuario->getApellido();
        $email = $usuario->getEmail();

        $NombreCompleto = $usuario->getNombre() . ' ' . $usuario->getApellido() . ' ' . $usuario->getApellidoseg();

        if ($tipoCredito == 'CreditoCuenta') {

            $tipoCredito = 'Credito Cuenta Corriente';
        }

        if ($tipoCredito == 'CreditoConsolidacion') {


            $tipoCredito = 'Credito Consolidacion';
        }

        if ($tipoCredito == 'CreditoHipotecario') {


            $creditohipotecario = $em->getRepository('BenchCreditosBundle:CreditoHipotecario')->findOneBy(array('usuario' => $usuarioId), array('id' => 'Desc'));
            $porcentaje =   $creditohipotecario->getMontoCredito() / $creditohipotecario->getValorPropiedad();


            $tipoCredito = 'Credito Hipotecario';
        }else{

            $porcentaje = '';
        }



        if ($tipoCredito == 'CreditoConsumo') {


            $tipoCredito = 'Credito Consumo';
        }



        if ($tipoCredito == 'CreditoAutomotriz') {


            $tipoCredito = 'Credito Automotriz';
        }

        $cartolaAfp = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'cartolaAfp'  ));
        $variable6ultimasliquidaciones = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'variable6ultimasliquidaciones'  ));
        $Carnet = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Carnet'  ));
        $impuestos3 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'impuestos3'  ));
        $AnualDeclaracion = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Anual'  ));
        $fijo3ultimasliquidaciones =$em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'fijo3ultimasliquidaciones'  ));


$imp = '';
if($impuestos3){
foreach ($impuestos3 as $impuestos3 ) :

$imp .= $impuestos3->getUrl();
$imp .= '  

';


 endforeach;

}


$anual = '';
if($AnualDeclaracion){
foreach ($AnualDeclaracion as  $AnualDeclaracion) {
   $anual .= $AnualDeclaracion->getUrl();
   $anual .= '   
   ';

}

}

$car = '';

if($Carnet ){
foreach ($Carnet  as   $Carnet  ) {
 
$car .=  $Carnet->getUrl();
$car .= '

';
}

}

$fijo3 = '';

if($fijo3ultimasliquidaciones){

    foreach ($fijo3ultimasliquidaciones as $fijo3ultimasliquidaciones ) {
        $fijo3 .=  $fijo3ultimasliquidaciones->getUrl();
        $fijo3 .= '

        ';
    }
}
$cartola = '';
if($cartolaAfp){

    foreach ($cartolaAfp as $cartolaAfp) {
       $cartola  .= $cartolaAfp->getUrl();
       $cartola .= '+

       ';
    }
}


$variable6 = '';

if($variable6ultimasliquidaciones){

    foreach ($variable6ultimasliquidaciones as $variable6ultimasliquidaciones) {
       $variable6 .= $variable6ultimasliquidaciones->getUrl();
       $variable6 .= ' 

       ';
    }
}



 $asuntoZend = 'Bench Banking el usuario ' . $nombre . ' ' . $apellido1 . ' -  '.$tipoCredito.'   '.$montoS  .' ';
           $detalleZend  ='


Solicitud de crédito 

Ha pedido un nuevo  ' . $tipoCredito . ' 
Monto . ' . $montoS . ' Pie de .' . $pieS . '


Rut Cliente: '. $usuario->getRut().'

Nombre y Apellido Cliente: '. $usuario->getNombre(). ' ' . $usuario->getApellido()   .  ' ' .$usuario->getApellidoseg() .'


Telefono Cliente:  '. $usuario->getTelefono().'

Email Cliente: '. $usuario->getEmail() .'

Sueldo Liquido Cliente: '.$antecedente->getIndependientesueldoliquido().'

Ver Reportes

https://www.benchbanking.com/adminBench/pCrearPdf/'.$usuarioId.'

https://www.benchbanking.com/banco/reporte/bbva/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/bci/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/consorcio/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/metlife/'.$usuarioId.'/


3 últimas declaraciones de impuesto
'.$imp.'


Declaración Anual de los Últimos dos Años.
'.$anual.'


Fotocopia Carnet.

'.$car.'


Sueldo Fijo 3 últimas líquidaciones de sueld

'.$fijo3.'


Cartola de últimas 12 cotizaciones afp.

'.$cartola.'



Sueldo Variable 6 últimas líquidaciones de sueldo.


'. $variable6. '




';



   

        // Envio a zend Desk

        $create = json_encode(
                array(
            'ticket' => array(
                'requester' => array(
                    'name' => $usuario->getNombre() . $usuario->getApellido()  .  $usuario->getApellidoseg() , 
                    'email' => $usuario->getEmail(),
                ),
                'group_id' => '22',

                'subject' => $asuntoZend,
                'custom_fields' =>
                array(

                    '23412128' => $usuario->getRut(),
                    '23412138' => $NombreCompleto,
                    '23723488' => $usuario->getTelefono(),
                    '23723498' => $tipoCredito,
                    '23723588' => $pieS,
                    '23057889' =>  $porcentaje,
                    '23750746' => $montoS),


                'description' => $detalleZend
            )
                ), JSON_FORCE_OBJECT
        );

        $zendDesk = new zendesk();
        $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");

      

        



        ///////////////////////////////////////////////////

        $nombre_destino = $nombre . " " . $apellido1;

        $asunto = "Gracias por cotizar  en BenchBanking ";
        $address = $email;

        $postmark = new Postmark("f907f8d4-c537-4c49-8ccc-3b96938cef8a", "info@benchbanking.com", "info@benchbanking.com");



        $result = $postmark->to($address)
                        ->subject("Gracias por cotizar  en BenchBanking")
                        ->html_message('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Mailing BenchBanking</title>
    <style>

        #outlook a {padding:0;}     
        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}     
        .ReadMsgBody {width: 100%;}
        .ExternalClass {width:100%;} 
        .backgroundTable {margin:0 auto; padding:0; width:100%;!important;} 
        table td {border-collapse: collapse;}
        .ExternalClass * {line-height: 115%;}
		img{width:100%;}
a:link {
	color: #F60;
	text-decoration: none;
}
a:hover {
	color: #F60;
	text-decoration: underline;
}
a:visited {
	font-weight: bold;
	color: #F60;
	text-decoration: none;
}
a:active {
	font-weight: bold;
	color: #F60;
	text-decoration: none;
}		
        

        @media screen and (max-width: 640px){
            
            *[class="container"] { width: 320px !important; padding:0px !important}                                 
            *[class="mobile-column"] {display: block;}
            *[class="mob-column"] {float: none !important;width: 100% !important;}         
            *[class="mobile-padding"] {padding-left:10px !important;padding-right:10px !important;}         
            *[class="hide"] {display:none !important;}          
            *[class="100p"] {width:100% !important; height:auto !important;}
            *[class="50p"] {width:100% !important; height:auto !important;}
			img{width:100%;}
                    
        
        }
        
    .destacado {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 24px;
	color: #FF6600;
}
    </style>
</head>

<body bgcolor="#FFFFFF" style="margin:0px; padding:0px;">
    <table border="0" cellpadding="0" cellspacing="0" class="mobile-padding" style="background-color: #ffffff" width="100%">
        <tr>
            <td align="center" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td align="center" height="100" valign="middle"><img src="http://www.benchbanking.com/imgMaling/header.png"/></td>
                    </tr>
                </table>

              <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td height="20"></td>
                </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td align="center" valign="top"><span class="destacado" style="font-family: Arial, Helvetica, sans-serif;
	font-size: 24px;
	color: #FF6600;">¡Gracias por cotizar con nosotros!</span></td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td>
                            <table border="0" cellpadding="10" cellspacing="0">
                                <tr>
                                   
                                </tr>

                                <tr>
                                    <td align="left" style="font-size:14px; color: #666; font-family: Verdana, Geneva, sans-serif;"><p>Tu información ha sido enviada a los bancos que seleccionaste, en  5 días recibirás tu respuesta.</p>
                                      <p>¡Mucho éxito!</p>
                                 
                                    <p>Te saluda,</p>
                                    <p><strong  style="color:#F60">Equipo BenchBanking</strong></p>
                                    </td>
                                    
                                </tr>
                            </table>
                        </td>
                  </tr>
              </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td style="border-top: #CCC 1px solid;" height="20"><img src="http://www.benchbanking.com/imgMaling/flow.jpg"/></td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td align="center" valign="top">
                          <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                                <tr>
                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                            <tr>
                                                <td style="background-color: #EEEEEE" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0">
                                                        <tr>
                                                            <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif">Contacto</td>
                                                        </tr>

                                                        <tr>
                                                          <td align="left" style="font-size:12px; color: #666; font-family: Verdana, Geneva, sans-serif">Si deseas ponerte en contacto con nosotros<br />
escríbenos a <a href="mailto:info@benchbanking.com">info@benchbanking.com </a><br />
o llámanos al <br />
<a href="tel:+56(2)25709004">  +56(2) 25709004</a></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="20">
                                            <tr>
                                                <td align="left" height="20">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                            <tr>
                                                <td style="background-color: #EEEEEE" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0">
                                                        <tr>
                                                            <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif;"></td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" style="font-size:12px; color: #666; font-family: Verdana, Geneva, sans-serif;"><br />.</br></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="20">
                                            <tr>
                                                <td align="left" height="20">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>

                                    <td align="left" class="mobile-column" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="200">
                                            <tr>
                                                <td style="background-color: #EEEEEE" valign="top">
                                                    <table border="0" cellpadding="10" cellspacing="0">
                                                        <tr>
                                                            <td align="left" style="font-size:14px; color: #F60; font-family: Verdana, Geneva, sans-serif;">Financiado por:</td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" style="font-size:14px; color:#333333; font-family:Arial, Helvetica, sans-serif;"><p><img src="http://www.benchbanking.com/imgMaling/logo01.png"/></p>
                                                              <p>
                                                            <img src="http://www.benchbanking.com/imgMaling/logo02.png"/></p></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td height="20"></td>
                    </tr>
                </table>

              <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td style="background-color: #F60">&nbsp;</td>
                    </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" class="100p" width="640">
                    <tr>
                        <td height="20"></td>
                    </tr>
                </table>
          </td>
        </tr>
    </table>
</body>
</html>')->send();




        $return = ('200');


        return new Response($return); //
    }

    public function mandaSolicitudAction(Request $request) {


        $return = ('200');


        return new Response($return); //
    }

}

