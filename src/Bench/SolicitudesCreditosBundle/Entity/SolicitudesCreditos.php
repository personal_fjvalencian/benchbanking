<?php

namespace Bench\SolicitudesCreditosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SolicitudesCreditos
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SolicitudesCreditos
{
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=100 , nullable=true)
     */

    private $tipo;
     /**
     * @var string
     *
     * @ORM\Column(name="creditoid", type="integer",  nullable=true)
     */
    
    
    private $creditoid;
    
   
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="BancoChile", type="string", length=100 , nullable=true)
     */
    
    
    
    private $BancoChile;
     /**
     * @var string
     *
     * @ORM\Column(name="BancoEstado", type="string", length=100 , nullable=true)
     */
    
    private $BancoEstado;
    
     /**
     * @var string
     *
     * @ORM\Column(name="BancoFalabella", type="string", length=100 , nullable=true)
     */
    private $BancoFalabella;
    
     /**
     * @var string
     *
     * @ORM\Column(name="BancoSecurity", type="string", length=100 , nullable=true)
     */
    
    private $BancoSecurity;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="Santander", type="string", length=100 , nullable=true)
     */
    
    private $Santander;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="Scotiabank", type="string", length=100 , nullable=true)
     */
    
    private $Scotiabank;
    
    

    
    
      /**
     * @var string
     *
     * @ORM\Column(name="bancobice", type="string", length=100 , nullable=true)
     */
    
    private $bancobice;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="bbva", type="string", length=100 , nullable=true)
     */
    
    
    private $bbva;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="bci", type="string", length=100 , nullable=true)
     */
    
    
    private $bci;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="consorcio", type="string", length=100 , nullable=true)
     */
    
    private $consorcio;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="corpbanca", type="string", length=100 , nullable=true)
     */
    
    
    private $corpbanca;
    
    
    
       
     /**
     * @var string
     *
     * @ORM\Column(name="metlife", type="string", length=100 , nullable=true)
     */
    
    
    
    private $metlife;
    
    
    
 
    
      
     /**
     * @var string
     *
     * @ORM\Column(name="itau", type="string", length=100 , nullable=true)
     */
    
    private $itau;
    
    
        /**
     * @var string
     *
     * @ORM\Column(name="financialgroup", type="string", length=100 , nullable=true)
     */
    
    
    
    
    private $financialgroup;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="fecha",  type="datetime", nullable=true)
     */
    
    
    
    private $fecha;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="estado",  type="string", length=100 ,  nullable=true)
     */
    
    
    private $estado;
    
    
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="solicitudescreditos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    private $usuario;
    
    
  

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return SolicitudesCreditos
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set creditoid
     *
     * @param integer $creditoid
     * @return SolicitudesCreditos
     */
    public function setCreditoid($creditoid)
    {
        $this->creditoid = $creditoid;
    
        return $this;
    }

    /**
     * Get creditoid
     *
     * @return integer 
     */
    public function getCreditoid()
    {
        return $this->creditoid;
    }

    /**
     * Set BancoChile
     *
     * @param string $bancoChile
     * @return SolicitudesCreditos
     */
    public function setBancoChile($bancoChile)
    {
        $this->BancoChile = $bancoChile;
    
        return $this;
    }

    /**
     * Get BancoChile
     *
     * @return string 
     */
    public function getBancoChile()
    {
        return $this->BancoChile;
    }

    /**
     * Set BancoEstado
     *
     * @param string $bancoEstado
     * @return SolicitudesCreditos
     */
    public function setBancoEstado($bancoEstado)
    {
        $this->BancoEstado = $bancoEstado;
    
        return $this;
    }

    /**
     * Get BancoEstado
     *
     * @return string 
     */
    public function getBancoEstado()
    {
        return $this->BancoEstado;
    }

    /**
     * Set BancoFalabella
     *
     * @param string $bancoFalabella
     * @return SolicitudesCreditos
     */
    public function setBancoFalabella($bancoFalabella)
    {
        $this->BancoFalabella = $bancoFalabella;
    
        return $this;
    }

    /**
     * Get BancoFalabella
     *
     * @return string 
     */
    public function getBancoFalabella()
    {
        return $this->BancoFalabella;
    }

    /**
     * Set BancoSecurity
     *
     * @param string $bancoSecurity
     * @return SolicitudesCreditos
     */
    public function setBancoSecurity($bancoSecurity)
    {
        $this->BancoSecurity = $bancoSecurity;
    
        return $this;
    }

    /**
     * Get BancoSecurity
     *
     * @return string 
     */
    public function getBancoSecurity()
    {
        return $this->BancoSecurity;
    }

    /**
     * Set Santander
     *
     * @param string $santander
     * @return SolicitudesCreditos
     */
    public function setSantander($santander)
    {
        $this->Santander = $santander;
    
        return $this;
    }

    /**
     * Get Santander
     *
     * @return string 
     */
    public function getSantander()
    {
        return $this->Santander;
    }

    /**
     * Set Scotiabank
     *
     * @param string $scotiabank
     * @return SolicitudesCreditos
     */
    public function setScotiabank($scotiabank)
    {
        $this->Scotiabank = $scotiabank;
    
        return $this;
    }

    /**
     * Get Scotiabank
     *
     * @return string 
     */
    public function getScotiabank()
    {
        return $this->Scotiabank;
    }

    /**
     * Set bancobice
     *
     * @param string $bancobice
     * @return SolicitudesCreditos
     */
    public function setBancobice($bancobice)
    {
        $this->bancobice = $bancobice;
    
        return $this;
    }

    /**
     * Get bancobice
     *
     * @return string 
     */
    public function getBancobice()
    {
        return $this->bancobice;
    }

    /**
     * Set bbva
     *
     * @param string $bbva
     * @return SolicitudesCreditos
     */
    public function setBbva($bbva)
    {
        $this->bbva = $bbva;
    
        return $this;
    }

    /**
     * Get bbva
     *
     * @return string 
     */
    public function getBbva()
    {
        return $this->bbva;
    }

    /**
     * Set bci
     *
     * @param string $bci
     * @return SolicitudesCreditos
     */
    public function setBci($bci)
    {
        $this->bci = $bci;
    
        return $this;
    }

    /**
     * Get bci
     *
     * @return string 
     */
    public function getBci()
    {
        return $this->bci;
    }

    /**
     * Set consorcio
     *
     * @param string $consorcio
     * @return SolicitudesCreditos
     */
    public function setConsorcio($consorcio)
    {
        $this->consorcio = $consorcio;
    
        return $this;
    }

    /**
     * Get consorcio
     *
     * @return string 
     */
    public function getConsorcio()
    {
        return $this->consorcio;
    }

    /**
     * Set corpbanca
     *
     * @param string $corpbanca
     * @return SolicitudesCreditos
     */
    public function setCorpbanca($corpbanca)
    {
        $this->corpbanca = $corpbanca;
    
        return $this;
    }

    /**
     * Get corpbanca
     *
     * @return string 
     */
    public function getCorpbanca()
    {
        return $this->corpbanca;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return SolicitudesCreditos
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return SolicitudesCreditos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return SolicitudesCreditos
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set financialgroup
     *
     * @param string $financialgroup
     * @return SolicitudesCreditos
     */
    public function setFinancialgroup($financialgroup)
    {
        $this->financialgroup = $financialgroup;
    
        return $this;
    }

    /**
     * Get financialgroup
     *
     * @return string 
     */
    public function getFinancialgroup()
    {
        return $this->financialgroup;
    }

    /**
     * Set itau
     *
     * @param string $itau
     * @return SolicitudesCreditos
     */
    public function setItau($itau)
    {
        $this->itau = $itau;
    
        return $this;
    }

    /**
     * Get itau
     *
     * @return string 
     */
    public function getItau()
    {
        return $this->itau;
    }

    /**
     * Set metlife
     *
     * @param string $metlife
     * @return SolicitudesCreditos
     */
    public function setMetlife($metlife)
    {
        $this->metlife = $metlife;
    
        return $this;
    }

    /**
     * Get metlife
     *
     * @return string 
     */
    public function getMetlife()
    {
        return $this->metlife;
    }
}