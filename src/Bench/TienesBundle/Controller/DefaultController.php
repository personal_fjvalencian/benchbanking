<?php

namespace Bench\TienesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\TienesBundle\Entity\Bienesraices;
use Bench\TienesBundle\Entity\Sociedades;
use Bench\TienesBundle\Entity\Automoviles;
use Bench\TienesBundle\Entity\Ahorroinverson;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Bench\UsuariosBundle\Entity\HistoricoFechaAct;
use \DateTime;

class DefaultController extends Controller {

    public function indexAction($name) {
        return $this->render('BenchTienesBundle:Default:index.html.twig', array('name' => $name));
    }

    public function delbienesraicesAction(Request $request) {
        $request->headers->addCacheControlDirective('no-store', true);
        $em = $this->getDoctrine()->getManager();
        $recordToDelete = $request->request->get('recordToDelete');
        $bienesRaices = $em->getRepository('BenchTienesBundle:Bienesraices')->find($recordToDelete);
        $em->remove($bienesRaices);
        $em->flush();



        $return = ' 100';


        return new Response($return); 
    }


    public function bienesRaicesAction(Request $request) {
        
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        $tokenPost = $request->request->get('token');
        $request->headers->addCacheControlDirective('no-store', true);
        
        if($token == $tokenPost){
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $THipoRol = $request->request->get('THipoRol');
        $THipoAvaluo_fiscal = $request->request->get('THipoAvaluo_fiscal');
        $ComboHipoteRegion = $request->request->get('ComboHipote');
        $ComboHipoComuna = $request->request->get('ComboHipoComuna');
        $THipoDireccion = $request->request->get('THipoDireccion');
        $THipoHipotecado = $request->request->get('THipoHipotecado');
        $THipoTipo = $request->request->get('THipoTipo');



        if ($THipoRol == '' or $THipoAvaluo_fiscal == '' or $ComboHipoteRegion == '' or $ComboHipoComuna == '' or $THipoDireccion == '' or $THipoHipotecado == '' or $THipoTipo == '') {

            $return = '200';


            return new Response($return); //make sure it has the correct content type
        } else {

            $bienesRaices = new Bienesraices();

            $bienesRaices->setRol($THipoRol);
            $bienesRaices->setUsuario($usuario);
            $bienesRaices->setAvaluo($THipoAvaluo_fiscal);
            $bienesRaices->setRegion($ComboHipoteRegion);
            $bienesRaices->setComuna($ComboHipoComuna);
            $bienesRaices->setDireccion($THipoDireccion);
            $bienesRaices->setHipotecado($THipoHipotecado);
            $bienesRaices->setTipo($THipoTipo);
            $bienesRaices->setFecha(new \DateTime());



            $em->persist($usuario);
            $em->persist($bienesRaices);
            $em->flush();

            $id = $bienesRaices->getId(); // recupero el id 

            $return = '

   <tbody style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">                

<tr id="item_' . $id . '">
           
                    <td><p style="text-align:center;color:#666;">' . $THipoTipo . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $THipoDireccion . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $ComboHipoteRegion . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $ComboHipoComuna . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $THipoAvaluo_fiscal . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $THipoRol . '</p></td>
                    <td><p style="text-align:center;color:#666;">' . $THipoHipotecado . '</p></td>
                    <td><p style="text-align:center;color:#666;"> <a href="#" class="del_button" id="del-' . $id . '">eliminar</a> </p> </td>
 </tr>
</tbody> 
';



            /*  Actualizar estado usuario si paso mas  6 horas  */
            $fechaA = $usuario->getFechaactualizacionFinal();
            $date1 = new DateTime($fechaA);
            $date2 = new DateTime('now');
            $interval = $date1->diff($date2);
            $h = $interval->format('%H');
            $anos = $interval->format('%y');
            $meses = $interval->format('%m');
            $dias = $interval->format('%d');
            $final = $usuario->getFinal();
            
        
            
              if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        
        

            /* fin de Actualizar estado */

            $dts = ( new \DateTime());
            $fecha = $dts->format('d-m-Y H:i:s');
            $usuario->setFechaactualizacionFinal($fecha);


            $usuario->setTienesBienesRaicesSiNo('SI');
            $em->merge($usuario);
            $em->flush();



            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Tienes Bienes Raices');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();






        }

            return new Response($return); //make sure it has the correct content type
        }
    }

/// Del ahorroinversion

    public function delautomovilesAction(Request $request) {

        $request->headers->addCacheControlDirective('no-store', true);
        $em = $this->getDoctrine()->getManager();
        $recordToDelete = $request->request->get('recordToDelete');
        $automoviles = $em->getRepository('BenchTienesBundle:Automoviles')->find($recordToDelete);
        $em->remove($automoviles);
        $em->flush();

        $return = ' ';

        return new Response($return);
    }

    /// GUARDA AUTOMOVILES 
    public function automovilesAction(Request $request) {
        
        
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        $tokenPost = $request->request->get('token');
        
        
        
        if($token == $tokenPost){
        
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));


        $TienesATipo = $request->request->get('TienesATipo');
        $TienesAMarca = $request->request->get('TienesAMarca');
        $TienesAPatente = $request->request->get('TienesAPatente');
        $TienesAAno = $request->request->get('TienesAAno');
        $TienesAAvaluo = $request->request->get('TienesAAvaluo');
        $TienesAPrendado = $request->request->get('TienesAPrendado');
        $TienesAModelo = $request->request->get('TienesAModelo');


        if ($TienesATipo == '' or $TienesAMarca == '' or $TienesAPatente == '' or $TienesAAno == '' or $TienesAAvaluo == '' or $TienesAPrendado == '') {

            $return = '200';


            return new Response($return); //make sure it has the correct content type
        } else {





            $Automoviles = new Automoviles();


            $Automoviles->setTipo($TienesATipo);
            $Automoviles->setMarcaauto($TienesAMarca);
            $Automoviles->setPatente($TienesAPatente);
            $Automoviles->setAnoauto($TienesAAno);
            $Automoviles->setValorcomercial($TienesAAvaluo);
            $Automoviles->setPrendado($TienesAPrendado);
            $Automoviles->setModelo($TienesAModelo);
            $Automoviles->setUsuario($usuario);
            $Automoviles->setFecha(new \DateTime());


            $em->persist($usuario);
            $em->persist($Automoviles);

            $em->flush();
            $id = $Automoviles->getId();


            $return = ';

                   <tbody style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">

          <tr id="itemTienesAutos_' . $id . '">
          <td><p style="text-align:center;color:#666;">' . $TienesATipo . '</p></td>
          <td><p style="text-align:center;color:#666;">' . $TienesAMarca . '</p></td>
          <td><p style="text-align:center;color:#666;">' . $TienesAModelo . '</p></td>
          <td><p style="text-align:center;color:#666;">' . $TienesAPatente . '</p></td>
          <td><p style="text-align:center;color:#666;">' . $TienesAAvaluo . '</p></td>
          <td><p style="text-align:center;color:#666;">' . $TienesAAno . '</p></td>  
          <td><p style="text-align:center;color:#666;">' . $TienesAPrendado . '</p></td> 
          <td><p style="text-align:center;color:#666;"> <a href="#" class="del_button" id="del-' . $id . '">eliminar</a> </p> </td>
          </tr>

   
          </tbody>

';
            
    /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
      
        
       if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        /* fin de Actualizar estado */
        
        

            $dts = ( new \DateTime());
            $fecha = $dts->format('d-m-Y H:i:s');
            $usuario->setFechaactualizacionFinal($fecha);


            $usuario->setTienesAutomovilesSiNo('SI');
            $em->merge($usuario);
            $em->flush();




            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Tienes Automoviles');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();





        }

            return new Response($return);
        }
    }

    // borra ahorro inversion
    public function delahorroinversionAction(Request $request) {
        
        $request->headers->addCacheControlDirective('no-store', true);
        $em = $this->getDoctrine()->getManager();
        $recordToDelete = $request->request->get('recordToDelete');
        $ahorroInversion = $em->getRepository('BenchTienesBundle:Ahorroinverson')->find($recordToDelete);
        $em->remove($ahorroInversion);
        $em->flush();
        $return = ' ';
        return new Response($return);
    }

    // Guarda ahorro inversion 
    public function ahorroinversionAction(Request $request) {
         
         $request->headers->addCacheControlDirective('no-store', true);
         $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
         $tokenPost = $request->request->get('token');
    
       if( $token == $tokenPost){
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
        $TAhorroTipoBienes = $request->request->get('TAhorroTipoBienes');
        $TAhorroInstitucion = $request->request->get('TAhorroInstitucion');
        $TAhorroVComercial = $request->request->get('TAhorroVComercial');
        $TAhorroVPrendadoInversion = $request->request->get('TAhorroVPrendadoInversion');
        $ahorroInversion = new Ahorroinverson();
        $ahorroInversion->setTipo($TAhorroTipoBienes);
        $ahorroInversion->setInstitucion($TAhorroInstitucion);
        $ahorroInversion->setValorcomercial($TAhorroVComercial);
        $ahorroInversion->setPrendado($TAhorroVPrendadoInversion);
        $ahorroInversion->setUsuario($usuario);


        if ($TAhorroTipoBienes == '' or $TAhorroInstitucion == '' or $TAhorroVComercial == '' or $TAhorroVPrendadoInversion == '') {


            $return = '200';


            return new Response($return); //make sure it has the correct content type
        } else {



            $em->persist($usuario);
            $em->persist($ahorroInversion);
            $em->flush();

            $id = $ahorroInversion->getId();


            $return = '
                  <tbody style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">
           <tr  id="itemahorroinversion_' . $id . '">
        
          <td><p style="text-align:center;color:#666;">' . $TAhorroTipoBienes . '  </p></td>
          <td><p style="text-align:center;color:#666;">' . $TAhorroInstitucion . ' </p></td>
          <td><p style="text-align:center;color:#666;">' . $TAhorroVComercial . ' </p></td>
          <td><p style="text-align:center;color:#666;">' . $TAhorroVPrendadoInversion . '</p></td>
          
          <td><p style="text-align:center;color:#666;"> <a href="#" class="del_button" id="del-' . $id . '">eliminar</a> </p> </td>

          </tr></tbody>';
            
            
    /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
       
        if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }


        /* fin de Actualizar estado */
        
        


            $dts = ( new \DateTime());
            $fecha = $dts->format('d-m-Y H:i:s');
            $usuario->setFechaactualizacionFinal($fecha);


            $usuario->setTienesAhorroInversionSiNo('SI');
            $em->merge($usuario);
            $em->flush();




            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Tienes Ahorro Inversion');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();
            return new Response($return); 
            
        }
        
          return new Response($return); 
      
        
          }
          
          
            return new Response($return);
      
            
            
            
    }

    /// GUARDAR SOCIEDADES 


    public function delsociedades2Action(Request $request) {

        $request->headers->addCacheControlDirective('no-store', true);
        $id = $request->request->get('recordToDelete');
        $em = $this->getDoctrine()->getManager();
        $Sociedades = $em->getRepository('BenchTienesBundle:Sociedades')->find($id);
        $em->remove($Sociedades);
        $em->flush();


        $return = '';


        return new Response($return);
    }

    public function sociedadesAction(Request $request) {
        
         $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
         $tokenPost = $request->request->get('token');
         $return ='200'
                 
                 ;
         
         
         if($token == $tokenPost){

        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $TSnombre = $request->request->get('TSnombre');
        $TSRut = $request->request->get('TSRut');
        $TSPorcentaje = $request->request->get('TSPorcentaje');
        $TSValor = $request->request->get('TSValor');

        if ($TSnombre == '' or $TSRut == '' or $TSPorcentaje == '' or $TSValor == '') {


            $return = '200';


            return new Response($return); //make sure it has the correct content type
        } else {


            $sociedades = new Sociedades();



            $sociedades->setUsuario($usuario);
            $sociedades->setNombre($TSnombre);
            $sociedades->setRut($TSRut);
            $sociedades->setPorcentaje($TSPorcentaje);
            $sociedades->setValor($TSValor);
            $sociedades->setFecha(new \DateTime());
            $em->persist($usuario);
            $em->persist($sociedades);
            $em->flush();

            $id = $sociedades->getId();


            $return = '
 
          <tbody style="background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;">
        <tr id="itemTienesSociedades_' . $id . '">
					
         
	  <td><p style="text-align:center;color:#666;">' . $TSnombre . '</p></td>
		
          <td><p style="text-align:center;color:#666;">' . $TSRut . '</p></td>
          <td><p style="text-align:center;color:#666;">' . $TSPorcentaje . '</p></td>  
          <td><p style="text-align:center;color:#666;">' . $TSValor . '</p></td>  
          
          
          <td><p style="text-align:center;color:#666;"> <a href="#" class="del_button" id="del-' . $id . '">eliminar</a> </p> </td>
				  
          </tr>
</tbody>          
';
            
            
            
                     
    /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
       
        
   
         if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        
        /* fin de Actualizar estado */
        
        


            $dts = ( new \DateTime());
            $fecha = $dts->format('d-m-Y H:i:s');
            $usuario->setFechaactualizacionFinal($fecha);
            $usuario->setTienesSociedadesSiNo('SI');

            $em->merge($usuario);
            $em->flush();



            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Tienes Sociedades');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();








    
            return new Response($return); //make sure it has the correct content type
        }
        
          return new Response($return); 
         }
         
           return new Response($return); 
    }

}
