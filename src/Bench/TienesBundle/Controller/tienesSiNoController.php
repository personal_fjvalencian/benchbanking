<?php

namespace Bench\TienesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Bench\UsuariosBundle\Entity\Usuario;
use Bench\TienesBundle\Entity\Bienesraices;
use Bench\TienesBundle\Entity\Sociedades;
use Bench\TienesBundle\Entity\Automoviles;
use Bench\TienesBundle\Entity\Ahorroinverson;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class tienesSiNoController extends Controller {

    public function tienesBienesRaicesSiNoAction(Request $request) {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $Sino = $request->request->get('Sino');

        $usuario->setTienesBienesRaicesSiNo($Sino);

        $em->merge($usuario); // merge se usa en el caso de un update 
        $em->flush();


        $return = ' 100';


        return new Response($return);
    }

    public function tienesAhorroInversionSiNoAction(Request $request) {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $Sino = $request->request->get('Sino');


        $usuario->setTienesAhorroInversionSiNo($Sino);

        $em->merge($usuario); // merge se usa en el caso de un update 
        $em->flush();


        $return = ' 100';


        return new Response($return);
    }

    public function tienesAutomovilesSiNoAction(Request $request) {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $Sino = $request->request->get('Sino');
        //$usuario = new Usuario();

        $usuario->setTienesAutomovilesSiNo($Sino);

        $em->merge($usuario); // merge se usa en el caso de un update 
        $em->flush();


        $return = ' 100';


        return new Response($return);
    }

    public function tieneSociedadesSiNoAction(Request $request) {
        $request = $this->get('request');
        $session = $this->getRequest()->getSession();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $Sino = $request->request->get('Sino');
       // $usuario = new Usuario();

        $usuario->setTienesSociedadesSiNo($Sino);

        $em->merge($usuario); // merge se usa en el caso de un update 
        $em->flush();


        $return = ' 100';


        return new Response($return);
    }

    //put your code here
}

?>
