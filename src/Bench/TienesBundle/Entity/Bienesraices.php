<?php

namespace Bench\TienesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BienesRaices
 *
 * @ORM\Table(name="Bienesraices")
 * @ORM\Entity
 */


class Bienesraices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    
    
   
    
        
    
     
    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=100)
     */ 

   private $tipo;
   
   
    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=100)
     */ 

   
   private $direccion;
   
   
    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=100)
     */ 
   
   
   
   private $region;
   
   
    
    /**
     * @var string
     *
     * @ORM\Column(name="comuna", type="string", length=100)
     */ 
   
   
   
   private $comuna;
   
   
     /**
     * @var string
     *
     * @ORM\Column(name="avaluo", type="string", length=100)
     */ 
   
   
   
   private $avaluo;
   
   
     /**
     * @var string
     *
     * @ORM\Column(name="rol", type="string", length=100)
     */ 
   
   
   
   private $rol;
   
   
    /**
     * @var string
     *
     * @ORM\Column(name="hipotecado", type="string", length=100)
     */ 
   
   
   private $hipotecado;
   
   
   
   
      
  /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
   
   
   private $fecha;
   
           
     /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="bienesraices")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
   
    private $usuario;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Bienesraices
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Bienesraices
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Bienesraices
     */
    public function setRegion($region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set comuna
     *
     * @param string $comuna
     * @return Bienesraices
     */
    public function setComuna($comuna)
    {
        $this->comuna = $comuna;
    
        return $this;
    }

    /**
     * Get comuna
     *
     * @return string 
     */
    public function getComuna()
    {
        return $this->comuna;
    }

    /**
     * Set avaluo
     *
     * @param string $avaluo
     * @return Bienesraices
     */
    public function setAvaluo($avaluo)
    {
        $this->avaluo = $avaluo;
    
        return $this;
    }

    /**
     * Get avaluo
     *
     * @return string 
     */
    public function getAvaluo()
    {
        return $this->avaluo;
    }

    /**
     * Set rol
     *
     * @param string $rol
     * @return Bienesraices
     */
    public function setRol($rol)
    {
        $this->rol = $rol;
    
        return $this;
    }

    /**
     * Get rol
     *
     * @return string 
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Set hipotecado
     *
     * @param string $hipotecado
     * @return Bienesraices
     */
    public function setHipotecado($hipotecado)
    {
        $this->hipotecado = $hipotecado;
    
        return $this;
    }

    /**
     * Get hipotecado
     *
     * @return string 
     */
    public function getHipotecado()
    {
        return $this->hipotecado;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Bienesraices
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Bienesraices
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}