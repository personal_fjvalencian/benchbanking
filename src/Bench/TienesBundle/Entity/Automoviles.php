<?php

namespace Bench\TienesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Automoviles
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Automoviles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


     /** @ORM\Column(type="string", length=100) */

    
    protected $tipo;

    
      /** @ORM\Column(type="string", length=100) */


    protected $marcaauto;

    
    
    

  /** @ORM\Column(type="string", length=100 , nullable=true) */

    protected $modelo;

    /** @ORM\Column(type="string", length=100 , nullable=true) */


    protected $patente;


  /** @ORM\Column(type="string", length=100 , nullable=true) */


    protected $anoauto;



   /** @ORM\Column(type="string", length=100 , nullable=true) */


    protected $valorcomercial;



   /** @ORM\Column(type="string", length=100 , nullable=true) */
    
    protected $prendado;
    
    
    
    /** @ORM\Column(type="datetime") */
    
    
    
    protected $fecha;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="automoviles")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */

    protected $usuario;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Automoviles
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set marcaauto
     *
     * @param string $marcaauto
     * @return Automoviles
     */
    public function setMarcaauto($marcaauto)
    {
        $this->marcaauto = $marcaauto;
    
        return $this;
    }

    /**
     * Get marcaauto
     *
     * @return string 
     */
    public function getMarcaauto()
    {
        return $this->marcaauto;
    }

    /**
     * Set modelo
     *
     * @param integer $modelo
     * @return Automoviles
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    
        return $this;
    }

    /**
     * Get modelo
     *
     * @return integer 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set patente
     *
     * @param integer $patente
     * @return Automoviles
     */
    public function setPatente($patente)
    {
        $this->patente = $patente;
    
        return $this;
    }

    /**
     * Get patente
     *
     * @return integer 
     */
    public function getPatente()
    {
        return $this->patente;
    }

    /**
     * Set anoauto
     *
     * @param integer $anoauto
     * @return Automoviles
     */
    public function setAnoauto($anoauto)
    {
        $this->anoauto = $anoauto;
    
        return $this;
    }

    /**
     * Get anoauto
     *
     * @return integer 
     */
    public function getAnoauto()
    {
        return $this->anoauto;
    }

    /**
     * Set valorcomercial
     *
     * @param string $valorcomercial
     * @return Automoviles
     */
    public function setValorcomercial($valorcomercial)
    {
        $this->valorcomercial = $valorcomercial;
    
        return $this;
    }

    /**
     * Get valorcomercial
     *
     * @return string 
     */
    public function getValorcomercial()
    {
        return $this->valorcomercial;
    }

    /**
     * Set prendado
     *
     * @param string $prendado
     * @return Automoviles
     */
    public function setPrendado($prendado)
    {
        $this->prendado = $prendado;
    
        return $this;
    }

    /**
     * Get prendado
     *
     * @return string 
     */
    public function getPrendado()
    {
        return $this->prendado;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Automoviles
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Automoviles
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
    
    
}