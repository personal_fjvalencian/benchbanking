<?php

namespace Bench\TienesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ahorroinverson
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Ahorroinverson
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(type="string", length=100) */



    protected $tipo;



/** @ORM\Column(type="string", length=100) */


    protected $institucion;

/** @ORM\Column(type="string", length=100 , nullable=true) */

     



    protected $valorcomercial;


/** @ORM\Column(type="string", length=100 , nullable=true) */



    protected $prendado;


 /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="ahorroinverson")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
   

    protected $usuario;

 
    /** @ORM\Column(type="datetime"  , nullable=true) */
    
    
    
    protected $fecha;
    

    



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Ahorroinverson
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return Ahorroinverson
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set valorcomercial
     *
     * @param string $valorcomercial
     * @return Ahorroinverson
     */
    public function setValorcomercial($valorcomercial)
    {
        $this->valorcomercial = $valorcomercial;
    
        return $this;
    }

    /**
     * Get valorcomercial
     *
     * @return string 
     */
    public function getValorcomercial()
    {
        return $this->valorcomercial;
    }

    /**
     * Set prendado
     *
     * @param string $prendado
     * @return Ahorroinverson
     */
    public function setPrendado($prendado)
    {
        $this->prendado = $prendado;
    
        return $this;
    }

    /**
     * Get prendado
     *
     * @return string 
     */
    public function getPrendado()
    {
        return $this->prendado;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Ahorroinverson
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Ahorroinverson
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}