<?php

namespace Bench\TienesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sociedades
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Sociedades
{
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    
    
    
    private $id;
    
    
 
   /** @ORM\Column(type="string", length=100) */


    
    
   private $nombre;
   
   
   /** @ORM\Column(type="string", length=100) */
   
   private $rut;
   
   /** @ORM\Column(type="string", length=100) */
   
   private $porcentaje;
   
  /** @ORM\Column(type="string", length=100) */
   
   private $valor;
   
   
  
   /** @ORM\Column(type="datetime") */
   
   
   
   private $fecha;
   
   
   
   
     /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="sociedades")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */

   
   
   
   private $usuario;
   
   
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Sociedades
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set rut
     *
     * @param string $rut
     * @return Sociedades
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    
        return $this;
    }

    /**
     * Get rut
     *
     * @return string 
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * Set porcentaje
     *
     * @param string $porcentaje
     * @return Sociedades
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;
    
        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return string 
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return Sociedades
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    
        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Sociedades
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return Sociedades
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}