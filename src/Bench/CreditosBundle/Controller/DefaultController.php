<?php

namespace Bench\CreditosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Bench\CreditosBundle\Entity\CreditoConsumo;
use Bench\CreditosBundle\Entity\CreditoAutomotriz;
use Bench\CreditosBundle\Entity\CreditoHipotecario;
use Bench\CreditosBundle\Entity\CreditoConsolidacion;
use Bench\CreditosBundle\Entity\CreditoCuenta;
use Bench\UsuariosBundle\Entity\HistoricoFechaAct;
use \DateTime;
use Bench\RegistrosBundle\Controller\zendesk;

class DefaultController extends Controller {

    public function indexAction($name) {
        return $this->render('BenchCreditosBundle:Default:index.html.twig', array('name' => $name));
    }

    // Guarda Credito Cuenta corriente

    public function guardaCuentaCorrienteAction(Request $request) {
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        
        $request = $this->get('request');
        $tokenPost= $request->request->get('token');
        
        
        
     

        
        if($token ==  $tokenPost ) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        $session->start();

        $rut = $session->get('rut');
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
        $producto1 = $request->request->get('producto1');
        $producto2 = $request->request->get('producto2');
        $producto3 = $request->request->get('producto3');

        $CreditoCuenta = new CreditoCuenta();
        $CreditoCuenta->setProducto($producto1);
        $CreditoCuenta->setProducto2($producto2);
        $CreditoCuenta->setProducto3($producto3);
        $CreditoCuenta->setUsuario($usuario);


        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
        
        

        if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }


        $CreditoCuenta->setFecha(new \DateTime());


        $dts = ( new \DateTime());
        $fecha = $dts->format('d-m-Y H:i:s');
        $usuario->setFechaactualizacionFinal($fecha);





        $em->persist($usuario);
        $em->persist($CreditoCuenta);
        $em->flush();







        $tipo = 'CreditoCuenta';

        $id = $CreditoCuenta->getId();
        $session->set('idCredito', $id);
        $session->set('tipoCredito', $tipo);
        $session->set('pie', '');
        $session->set('monto', '');
        $return = '100';




        $HistoricoFechaAct = new HistoricoFechaAct();
        $HistoricoFechaAct->setDonde('Credito Cuenta Corriente');
        $HistoricoFechaAct->setUsuario($usuario);
        $HistoricoFechaAct->setFechaActual(new \DateTime());
        $em->persist($usuario);
        $em->persist($HistoricoFechaAct);
        $em->flush();
        $request->headers->addCacheControlDirective('no-store', true);



        }

        return new Response($return);
    }

    // Guarda credito consolidacion

    public function guardaconsolidacionAction(Request $request) {

        

       $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
       $request = $this->get('request');
       $request->headers->addCacheControlDirective('no-store', true);
       $tokenPost  = $request->request->get('token');
        
        if( $token == $tokenPost  ){
            
        $session = $this->getRequest()->getSession();
        $session->start();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
        $ComboCon1 = $request->request->get('ComboCon1');
        $montoCon1 = $request->request->get('montoCon1');
        $ComboCon2 = $request->request->get('ComboCon2');
        $ComboCon3 = $request->request->get('ComboCon3');
        $montoCon3 = $request->request->get('montoCon3');
        $ComboCon4 = $request->request->get('ComboCon4');
        $ComboCon5 = $request->request->get('ComboCon5');
        $montoCon5 = $request->request->get('montoCon5');
        $ComboCon6 = $request->request->get('ComboCon6');

        $CreditoConsolidacion = new CreditoConsolidacion();

        $CreditoConsolidacion->setTipoCredito($ComboCon1);
        $CreditoConsolidacion->setMontoCredito($montoCon1);
        $CreditoConsolidacion->setPlazo($ComboCon2);
        $CreditoConsolidacion->setUsuario($usuario);
        $CreditoConsolidacion->setFecha(new \DateTime());

        $CreditoConsolidacion->setTipoCredito2($ComboCon3);
        $CreditoConsolidacion->setMontoCredito2($montoCon3);
        $CreditoConsolidacion->setPlazo2($ComboCon4);

        $CreditoConsolidacion->setTipoCredito3($ComboCon5);
        $CreditoConsolidacion->setMontoCredito3($montoCon5);
        $CreditoConsolidacion->setPlazo3($ComboCon6);


        /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
        if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }
        
        


        /* fin de Actualizar estado */



        $dts = ( new \DateTime());
        $fecha = $dts->format('d-m-Y H:i:s');
        $usuario->setFechaactualizacionFinal($fecha);


        $em->persist($usuario);
        $em->persist($CreditoConsolidacion);
        $em->flush();

        $id = $CreditoConsolidacion->getId();
        $tipo = 'CreditoConsolidacion';

        $session->set('idCredito', $id);
        $session->set('tipoCredito', $tipo);
        $session->set('pie', '');
        $session->set('monto', '');



        $HistoricoFechaAct = new HistoricoFechaAct();
        $HistoricoFechaAct->setDonde('Credito Consolidacion');
        $HistoricoFechaAct->setUsuario($usuario);
        $HistoricoFechaAct->setFechaActual(new \DateTime());
        $em->persist($usuario);
        $em->persist($HistoricoFechaAct);
        $em->flush();


$montoCon =  $montoCon1 +  $montoCon3 +  $montoCon5;
/* ticket zend desk */
  $usuarioId = $usuario->getId();
  $nombre =  $usuario->getNombre();
  $montoS =$montoCon;
  $apellido1  = $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
  $pieS = "";
  $antecedente = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $usuarioId), array('id' => 'Desc'));
  $NombreCompleto = $usuario->getNombre() . ' ' . $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
  $porcentaje = '';


     $cartolaAfp = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'cartolaAfp'  ));
        $variable6ultimasliquidaciones = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'variable6ultimasliquidaciones'  ));
        $Carnet = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Carnet'  ));
        $impuestos3 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'impuestos3'  ));
        $AnualDeclaracion = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Anual'  ));
        $fijo3ultimasliquidaciones =$em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'fijo3ultimasliquidaciones'  ));


$imp = '';
if($impuestos3){
foreach ($impuestos3 as $impuestos3 ) :

$imp .= $impuestos3->getUrl();
$imp .= '  

';


 endforeach;

}


$anual = '';
if($AnualDeclaracion){
foreach ($AnualDeclaracion as  $AnualDeclaracion) {
   $anual .= $AnualDeclaracion->getUrl();
   $anual .= '   
   ';

}

}

$car = '';

if($Carnet ){
foreach ($Carnet  as   $Carnet  ) {
 
$car .=  $Carnet->getUrl();
$car .= '

';
}

}

$fijo3 = '';

if($fijo3ultimasliquidaciones){

    foreach ($fijo3ultimasliquidaciones as $fijo3ultimasliquidaciones ) {
        $fijo3 .=  $fijo3ultimasliquidaciones->getUrl();
        $fijo3 .= '

        ';
    }
}
$cartola = '';
if($cartolaAfp){

    foreach ($cartolaAfp as $cartolaAfp) {
       $cartola  .= $cartolaAfp->getUrl();
       $cartola .= '+

       ';
    }
}


$variable6 = '';

if($variable6ultimasliquidaciones){

    foreach ($variable6ultimasliquidaciones as $variable6ultimasliquidaciones) {
       $variable6 .= $variable6ultimasliquidaciones->getUrl();
       $variable6 .= ' 

       ';
    }
}


$tipoCredito = 'Crédito Consolidación de deuda';   
$asuntoZend = 'Bench Banking el usuario ' . $nombre . ' ' . $apellido1 . ' -  '.$tipoCredito.'   '.$montoS  .' ';
           $detalleZend  ='


Solicitud de crédito 

Ha pedido un nuevo  ' . $tipoCredito . ' 
Monto . ' . $montoS . ' Pie de .' . $pieS . '


Rut Cliente: '. $usuario->getRut().'

Nombre y Apellido Cliente: '. $usuario->getNombre(). ' ' . $usuario->getApellido()   .  ' ' .$usuario->getApellidoseg() .'


Telefono Cliente:  '. $usuario->getTelefono().'

Email Cliente: '. $usuario->getEmail() .'

Sueldo Liquido Cliente: '.$antecedente->getIndependientesueldoliquido().'

Ver Reportes

https://www.benchbanking.com/adminBench/pCrearPdf/'.$usuarioId.'

https://www.benchbanking.com/banco/reporte/bbva/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/bci/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/consorcio/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/metlife/'.$usuarioId.'/


3 últimas declaraciones de impuesto
'.$imp.'


Declaración Anual de los Últimos dos Años.
'.$anual.'


Fotocopia Carnet.

'.$car.'


Sueldo Fijo 3 últimas líquidaciones de sueld

'.$fijo3.'


Cartola de últimas 12 cotizaciones afp.

'.$cartola.'



Sueldo Variable 6 últimas líquidaciones de sueldo.


'. $variable6. '




';



   

        // Envio a zend Desk

        $create = json_encode(
                array(
            'ticket' => array(
                'requester' => array(
                    'name' => $usuario->getNombre() . $usuario->getApellido()  .  $usuario->getApellidoseg() , 
                    'email' => $usuario->getEmail(),
                ),
                'group_id' => '22',

                'subject' => $asuntoZend,
                'custom_fields' =>
                array(

                    '23412128' => $usuario->getRut(),
                    '23412138' => $NombreCompleto,
                    '23723488' => $usuario->getTelefono(),
                    '23723498' => $tipoCredito,
                    '23723588' => $pieS,
                    '23057889' =>  $porcentaje,
                    '23750746' => $montoS),


                'description' => $detalleZend
            )
                ), JSON_FORCE_OBJECT
        );

        $zendDesk = new zendesk();
        $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");

      /* ticket zend desk */



        $respuesta = '100';
        
        }
        return new Response($respuesta);
    }

    //GUARDA  CREDITO HIPOTECARIO
    public function guardahipotecarioAction(Request $request) {

        $request = $this->get('request');
        $request->headers->addCacheControlDirective('no-store', true);
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        $tokenPost = $request->request->get('token');
        
    if($token == $tokenPost){
        
        $session = $this->getRequest()->getSession();
        $session->start();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));
         $usuarioId = $usuario->getId();
        $valor99 = $request->request->get('valor99');
        $pie99 = $request->request->get('pie99');
        $porcentaje99 = $request->request->get('porcentaje99');
        $total99 = $request->request->get('total99');
        $CuandoHipotecario = $request->get('CuandoHipotecario');
        $plazoCreditoH = $request->request->get('plazoCreditoH');
        $tipoPropiedad = $request->request->get('tipoPropiedad');
        $estadoPropiedad = $request->request->get('estadoPropiedad');
        $comunaDondeComprar = $request->request->get('comunaDondeComprar');
        $proyectoInmobiliaria = $request->request->get('proyectoInmobiliaria');
        $dfl2 = $request->request->get('dfl2');



        if ($comunaDondeComprar == '') {

            $respuesta = '200';
            return new Response($respuesta);
        } else {

            $CreditoHipotecario = new CreditoHipotecario();

            $CreditoHipotecario->setValorPropiedad($valor99);
            $CreditoHipotecario->setMontoPie($pie99);
            $CreditoHipotecario->setPorcentaje($porcentaje99);
            $CreditoHipotecario->setMontoCredito($total99);
            $CreditoHipotecario->setPlazoCredito($plazoCreditoH);
            $CreditoHipotecario->setTipoPropiedad($tipoPropiedad);
            $CreditoHipotecario->setEstadoPropiedad($estadoPropiedad);
            $CreditoHipotecario->setEstado($estadoPropiedad);
            $CreditoHipotecario->setDfl2($dfl2);
            $CreditoHipotecario->setCuandoDeseaComprar($CuandoHipotecario);
            
            $CreditoHipotecario->setComunaComprar($comunaDondeComprar);
            $CreditoHipotecario->setProyecto($proyectoInmobiliaria);
            $CreditoHipotecario->setFecha(new \DateTime());
            $CreditoHipotecario->setUsuario($usuario);
        
        /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
       
        
        
          if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        
        

        /* fin de Actualizar estado */


            $dts = ( new \DateTime());
            $fecha = $dts->format('d-m-Y H:i:s');
            $usuario->setFechaactualizacionFinal($fecha);


            $em->persist($usuario);
            $em->persist($CreditoHipotecario);
            $em->flush();
            $tipo = 'CreditoHipotecario';
            $id = $CreditoHipotecario->getId();


            $session->set('idCredito', $id);
            $session->set('tipoCredito', $tipo);
            $session->set('pie', $pie99);
            $session->set('monto', $valor99);

            $respuesta = '100';




            $HistoricoFechaAct = new HistoricoFechaAct();
            $HistoricoFechaAct->setDonde('Credito Hipotecario');
            $HistoricoFechaAct->setUsuario($usuario);
            $HistoricoFechaAct->setFechaActual(new \DateTime());
            $em->persist($usuario);
            $em->persist($HistoricoFechaAct);
            $em->flush();





        }

/* ticket zend desk */
  $usuarioId = $usuario->getId();
  $nombre =  $usuario->getNombre();
  $montoS = $valor99;
  $apellido1  = $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
  $pieS = $pie99;
  $antecedente = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $usuarioId), array('id' => 'Desc'));
  $NombreCompleto = $usuario->getNombre() . ' ' . $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
  $porcentaje = $porcentaje99;


     $cartolaAfp = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'cartolaAfp'  ));
        $variable6ultimasliquidaciones = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'variable6ultimasliquidaciones'  ));
        $Carnet = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Carnet'  ));
        $impuestos3 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'impuestos3'  ));
        $AnualDeclaracion = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Anual'  ));
        $fijo3ultimasliquidaciones =$em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'fijo3ultimasliquidaciones'  ));


$imp = '';
if($impuestos3){
foreach ($impuestos3 as $impuestos3 ) :

$imp .= $impuestos3->getUrl();
$imp .= '  

';


 endforeach;

}


$anual = '';
if($AnualDeclaracion){
foreach ($AnualDeclaracion as  $AnualDeclaracion) {
   $anual .= $AnualDeclaracion->getUrl();
   $anual .= '   
   ';

}

}

$car = '';

if($Carnet ){
foreach ($Carnet  as   $Carnet  ) {
 
$car .=  $Carnet->getUrl();
$car .= '

';
}

}

$fijo3 = '';

if($fijo3ultimasliquidaciones){

    foreach ($fijo3ultimasliquidaciones as $fijo3ultimasliquidaciones ) {
        $fijo3 .=  $fijo3ultimasliquidaciones->getUrl();
        $fijo3 .= '

        ';
    }
}
$cartola = '';
if($cartolaAfp){

    foreach ($cartolaAfp as $cartolaAfp) {
       $cartola  .= $cartolaAfp->getUrl();
       $cartola .= '+

       ';
    }
}


$variable6 = '';

if($variable6ultimasliquidaciones){

    foreach ($variable6ultimasliquidaciones as $variable6ultimasliquidaciones) {
       $variable6 .= $variable6ultimasliquidaciones->getUrl();
       $variable6 .= ' 

       ';
    }
}


 $tipoCredito = 'Crédito Hipotecario';   

 $asuntoZend = 'Bench Banking el usuario ' . $nombre . ' ' . $apellido1 . ' -  '.$tipoCredito.'   '.$montoS  .' ';
           $detalleZend  ='


Solicitud de crédito 

Ha pedido un nuevo  ' . $tipoCredito . ' 
Monto . ' . $montoS . ' Pie de .' . $pieS . '


Rut Cliente: '. $usuario->getRut().'

Nombre y Apellido Cliente: '. $usuario->getNombre(). ' ' . $usuario->getApellido()   .  ' ' .$usuario->getApellidoseg() .'


Telefono Cliente:  '. $usuario->getTelefono().'

Email Cliente: '. $usuario->getEmail() .'

Sueldo Liquido Cliente: '.$antecedente->getIndependientesueldoliquido().'

Ver Reportes

https://www.benchbanking.com/adminBench/pCrearPdf/'.$usuarioId.'

https://www.benchbanking.com/banco/reporte/bbva/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/bci/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/consorcio/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/metlife/'.$usuarioId.'/


3 últimas declaraciones de impuesto
'.$imp.'


Declaración Anual de los Últimos dos Años.
'.$anual.'


Fotocopia Carnet.

'.$car.'


Sueldo Fijo 3 últimas líquidaciones de sueld

'.$fijo3.'


Cartola de últimas 12 cotizaciones afp.

'.$cartola.'



Sueldo Variable 6 últimas líquidaciones de sueldo.


'. $variable6. '




';



   

        // Envio a zend Desk

        $create = json_encode(
                array(
            'ticket' => array(
                'requester' => array(
                    'name' => $usuario->getNombre() . $usuario->getApellido()  .  $usuario->getApellidoseg() , 
                    'email' => $usuario->getEmail(),
                ),
                'group_id' => '22',

                'subject' => $asuntoZend,
                'custom_fields' =>
                array(

                    '23412128' => $usuario->getRut(),
                    '23412138' => $NombreCompleto,
                    '23723488' => $usuario->getTelefono(),
                    '23723498' => $tipoCredito,
                    '23723588' => $pieS,
                    '23057889' =>  $porcentaje,
                    '23750746' => $montoS),


                'description' => $detalleZend
            )
                ), JSON_FORCE_OBJECT
        );

        $zendDesk = new zendesk();
        $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");

      /* ticket zend desk */



            return new Response($respuesta);
        }
    }

    //Guarda consumo 

    public function guardaconsumoAction(Request $request) {
        
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        $request = $this->get('request');
        $request->headers->addCacheControlDirective('no-store', true);
        $tokenPost = $request->request->get('token');
        
        
        if($token == $tokenPost ){
        $session = $this->getRequest()->getSession();
        $session->start();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $montoCredito = $request->request->get('montoCredito');
        $plazoCreditoConsumo = $request->request->get('plazoCreditoConsumo');
        $paraqueConsumo = $request->request->get('paraqueConsumo');
        $seguro1 = $request->request->get('seguro1');


        $CreditoConsumo = new CreditoConsumo();

        $CreditoConsumo->setMontoCredito($montoCredito);
        $CreditoConsumo->setParaque($paraqueConsumo);
        $CreditoConsumo->setPlazoCredito($plazoCreditoConsumo);
        $CreditoConsumo->setSegurosAsociados($seguro1);
        $CreditoConsumo->setFecha(new \DateTime());
        $CreditoConsumo->setUsuario($usuario);
        
        
        
        
         /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
          if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }


        /* fin de Actualizar estado */
        
        

        $dts = ( new \DateTime());
        $fecha = $dts->format('d-m-Y H:i:s');
        $usuario->setFechaactualizacionFinal($fecha);


        $em->persist($usuario);
        $em->persist($CreditoConsumo);
        $em->flush();

        $id = $CreditoConsumo->getId();
        $tipo = 'CreditoConsumo';
        $session->set('idCredito', $id);
        $session->set('tipoCredito', $tipo);
        $session->set('pie', '');
        $session->set('monto', $montoCredito);



        $HistoricoFechaAct = new HistoricoFechaAct();
        $HistoricoFechaAct->setDonde('Credito Consumo');
        $HistoricoFechaAct->setUsuario($usuario);
        $HistoricoFechaAct->setFechaActual(new \DateTime());
        $em->persist($usuario);
        $em->persist($HistoricoFechaAct);
        $em->flush();






/* ticket zend desk */
  $usuarioId = $usuario->getId();
  $nombre =  $usuario->getNombre();
  $montoS =$montoCredito;
  $apellido1  = $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
  $pieS = '';
  $antecedente = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $usuarioId), array('id' => 'Desc'));
  $NombreCompleto = $usuario->getNombre() . ' ' . $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
  $porcentaje = '';


     $cartolaAfp = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'cartolaAfp'  ));
        $variable6ultimasliquidaciones = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'variable6ultimasliquidaciones'  ));
        $Carnet = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Carnet'  ));
        $impuestos3 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'impuestos3'  ));
        $AnualDeclaracion = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Anual'  ));
        $fijo3ultimasliquidaciones =$em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'fijo3ultimasliquidaciones'  ));


$imp = '';
if($impuestos3){
foreach ($impuestos3 as $impuestos3 ) :

$imp .= $impuestos3->getUrl();
$imp .= '  

';


 endforeach;

}


$anual = '';
if($AnualDeclaracion){
foreach ($AnualDeclaracion as  $AnualDeclaracion) {
   $anual .= $AnualDeclaracion->getUrl();
   $anual .= '   
   ';

}

}

$car = '';

if($Carnet ){
foreach ($Carnet  as   $Carnet  ) {
 
$car .=  $Carnet->getUrl();
$car .= '

';
}

}

$fijo3 = '';

if($fijo3ultimasliquidaciones){

    foreach ($fijo3ultimasliquidaciones as $fijo3ultimasliquidaciones ) {
        $fijo3 .=  $fijo3ultimasliquidaciones->getUrl();
        $fijo3 .= '

        ';
    }
}
$cartola = '';
if($cartolaAfp){

    foreach ($cartolaAfp as $cartolaAfp) {
       $cartola  .= $cartolaAfp->getUrl();
       $cartola .= '+

       ';
    }
}


$variable6 = '';

if($variable6ultimasliquidaciones){

    foreach ($variable6ultimasliquidaciones as $variable6ultimasliquidaciones) {
       $variable6 .= $variable6ultimasliquidaciones->getUrl();
       $variable6 .= ' 

       ';
    }
}


 $tipoCredito = 'Crédito Consumo';   

 $asuntoZend = 'Bench Banking el usuario ' . $nombre . ' ' . $apellido1 . ' -  '.$tipoCredito.'   '.$montoS  .' ';
           $detalleZend  ='


Solicitud de crédito 

Ha pedido un nuevo  ' . $tipoCredito . ' 
Monto . ' . $montoS . ' Pie de .' . $pieS . '


Rut Cliente: '. $usuario->getRut().'

Nombre y Apellido Cliente: '. $usuario->getNombre(). ' ' . $usuario->getApellido()   .  ' ' .$usuario->getApellidoseg() .'


Telefono Cliente:  '. $usuario->getTelefono().'

Email Cliente: '. $usuario->getEmail() .'

Sueldo Liquido Cliente: '.$antecedente->getIndependientesueldoliquido().'

Ver Reportes

https://www.benchbanking.com/adminBench/pCrearPdf/'.$usuarioId.'

https://www.benchbanking.com/banco/reporte/bbva/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/bci/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/consorcio/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/metlife/'.$usuarioId.'/


3 últimas declaraciones de impuesto
'.$imp.'


Declaración Anual de los Últimos dos Años.
'.$anual.'


Fotocopia Carnet.

'.$car.'


Sueldo Fijo 3 últimas líquidaciones de sueld

'.$fijo3.'


Cartola de últimas 12 cotizaciones afp.

'.$cartola.'



Sueldo Variable 6 últimas líquidaciones de sueldo.


'. $variable6. '




';



   

        // Envio a zend Desk

        $create = json_encode(
                array(
            'ticket' => array(
                'requester' => array(
                    'name' => $usuario->getNombre() . $usuario->getApellido()  .  $usuario->getApellidoseg() , 
                    'email' => $usuario->getEmail(),
                ),
                'group_id' => '22',

                'subject' => $asuntoZend,
                'custom_fields' =>
                array(

                    '23412128' => $usuario->getRut(),
                    '23412138' => $NombreCompleto,
                    '23723488' => $usuario->getTelefono(),
                    '23723498' => $tipoCredito,
                    '23723588' => $pieS,
                    '23057889' =>  $porcentaje,
                    '23750746' => $montoS),


                'description' => $detalleZend
            )
                ), JSON_FORCE_OBJECT
        );

        $zendDesk = new zendesk();
        $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");

      /* ticket zend desk */



        $respuesta = '100';
        
        }
        
        
        return new Response($respuesta);
    }

    // Guarda AUTOMOTRIZ

    public function guardaautomotrizAction(Request $request) {
       
        $token = $this->get('form.csrf_provider')->generateCsrfToken('message_list');
        $request = $this->get('request');
        $request->headers->addCacheControlDirective('no-store', true);
        
        $tokenPost = $request->request->get('token');
        
        
        
        if($token ==  $tokenPost ){
        
        $session = $this->getRequest()->getSession();
        $session->start();
        $rut = $session->get('rut');
        $em = $this->getDoctrine()->getManager();


        $valorAutomotriz = $request->request->get('valorAutomotriz');
        $pieAutomotriz = $request->request->get('pieAutomotriz');
        $plazoAutomotriz = $request->request->get('plazoAutomotriz');
        $CuandoAutomotriz = $request->request->get('CuandoAutomotriz');
        $marcaAutomotriz = $request->request->get('marcaAutomotriz');
        $anoAutomotriz = $request->request->get('anoAutomotriz');
        $usuario = $em->getRepository('BenchUsuariosBundle:Usuario')->findOneBy(array('rut' => $rut));

        $CreditoAutomotriz = new CreditoAutomotriz();
        $CreditoAutomotriz->setMonto($valorAutomotriz);
        $CreditoAutomotriz->setPie($pieAutomotriz);
        $CreditoAutomotriz->setPlazo($plazoAutomotriz);
        $CreditoAutomotriz->setCuandocompras($CuandoAutomotriz);
        $CreditoAutomotriz->setMarcaauto($marcaAutomotriz);
        $CreditoAutomotriz->setUsuario($usuario);
        $CreditoAutomotriz->setAno($anoAutomotriz);
        
        
        
        
         /*  Actualizar estado usuario si paso mas  6 horas  */
        $fechaA = $usuario->getFechaactualizacionFinal();
        $date1 = new DateTime($fechaA);
        $date2 = new DateTime('now');
        $interval = $date1->diff($date2);
        $h = $interval->format('%H');
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');
        $final = $usuario->getFinal();
       
        if ($final == 'SI') {
            
            if ($anos >= 0) {
                
            
                
                
                if($anos > 0 ){
                    
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                }
                
                
                if ($h < 6 ) {
                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                    
                  
                }

                if ($dias > 0) {

                    $usuario->setFinal('SI');
                    $usuario->setArchivar('no');
                }
                
                   if ($meses > 0 ){
                     
                       $usuario->setFinal('SI');
                     $usuario->setArchivar('no');
                    
                }
                
            }
        }

        
        /* fin de Actualizar estado */
        
        
        $CreditoAutomotriz->setFecha(new \DateTime());

        $em->persist($usuario);
        $em->persist($CreditoAutomotriz);
        $em->flush();

        $id = $CreditoAutomotriz->getId();
        $tipo = 'CreditoAutomotriz';

        $session->set('pie', $pieAutomotriz);
        $session->set('monto', $valorAutomotriz);
        $session->set('idCredito', $id);
        $session->set('tipoCredito', $tipo);



        $HistoricoFechaAct = new HistoricoFechaAct();
        $HistoricoFechaAct->setDonde('Credito Automotriz');
        $HistoricoFechaAct->setUsuario($usuario);
        $HistoricoFechaAct->setFechaActual(new \DateTime());

        $dts = ( new \DateTime());
        $fecha = $dts->format('d-m-Y H:i:s');
        $usuario->setFechaactualizacionFinal($fecha);


        $em->persist($usuario);
        $em->persist($HistoricoFechaAct);
        $em->flush();




/* ticket zend desk */
  $usuarioId = $usuario->getId();
  $nombre =  $usuario->getNombre();
  $montoS =$valorAutomotriz;
  $apellido1  = $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
  $pieS =$pieAutomotriz;
  $antecedente = $em->getRepository('BenchUsuariosBundle:Antecedenteslaboral')->findOneBy(array('usuario' => $usuarioId), array('id' => 'Desc'));
  $NombreCompleto = $usuario->getNombre() . ' ' . $usuario->getApellido() . ' ' . $usuario->getApellidoseg();
  $porcentaje = '';


     $cartolaAfp = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'cartolaAfp'  ));
        $variable6ultimasliquidaciones = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'variable6ultimasliquidaciones'  ));
        $Carnet = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Carnet'  ));
        $impuestos3 = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'impuestos3'  ));
        $AnualDeclaracion = $em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'Anual'  ));
        $fijo3ultimasliquidaciones =$em->getRepository('BenchFilepickerBundle:Archivos')->findBy(array('usuario' => $usuarioId , 'tipo' => 'fijo3ultimasliquidaciones'  ));


$imp = '';
if($impuestos3){
foreach ($impuestos3 as $impuestos3 ) :

$imp .= $impuestos3->getUrl();
$imp .= '  

';


 endforeach;

}


$anual = '';
if($AnualDeclaracion){
foreach ($AnualDeclaracion as  $AnualDeclaracion) {
   $anual .= $AnualDeclaracion->getUrl();
   $anual .= '   
   ';

}

}

$car = '';

if($Carnet ){
foreach ($Carnet  as   $Carnet  ) {
 
$car .=  $Carnet->getUrl();
$car .= '

';
}

}

$fijo3 = '';

if($fijo3ultimasliquidaciones){

    foreach ($fijo3ultimasliquidaciones as $fijo3ultimasliquidaciones ) {
        $fijo3 .=  $fijo3ultimasliquidaciones->getUrl();
        $fijo3 .= '

        ';
    }
}
$cartola = '';
if($cartolaAfp){

    foreach ($cartolaAfp as $cartolaAfp) {
       $cartola  .= $cartolaAfp->getUrl();
       $cartola .= '+

       ';
    }
}


$variable6 = '';

if($variable6ultimasliquidaciones){

    foreach ($variable6ultimasliquidaciones as $variable6ultimasliquidaciones) {
       $variable6 .= $variable6ultimasliquidaciones->getUrl();
       $variable6 .= ' 

       ';
    }
}


$tipoCredito = 'Crédito Automotriz';   
$asuntoZend = 'Bench Banking el usuario ' . $nombre . ' ' . $apellido1 . ' -  '.$tipoCredito.'   '.$montoS  .' ';
           $detalleZend  ='


Solicitud de crédito 

Ha pedido un nuevo  ' . $tipoCredito . ' 
Monto . ' . $montoS . ' Pie de .' . $pieS . '


Rut Cliente: '. $usuario->getRut().'

Nombre y Apellido Cliente: '. $usuario->getNombre(). ' ' . $usuario->getApellido()   .  ' ' .$usuario->getApellidoseg() .'


Telefono Cliente:  '. $usuario->getTelefono().'

Email Cliente: '. $usuario->getEmail() .'

Sueldo Liquido Cliente: '.$antecedente->getIndependientesueldoliquido().'

Ver Reportes

https://www.benchbanking.com/adminBench/pCrearPdf/'.$usuarioId.'

https://www.benchbanking.com/banco/reporte/bbva/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/bci/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/consorcio/'.$usuarioId.'/

https://www.benchbanking.com/banco/reporte/metlife/'.$usuarioId.'/


3 últimas declaraciones de impuesto
'.$imp.'


Declaración Anual de los Últimos dos Años.
'.$anual.'


Fotocopia Carnet.

'.$car.'


Sueldo Fijo 3 últimas líquidaciones de sueld

'.$fijo3.'


Cartola de últimas 12 cotizaciones afp.

'.$cartola.'



Sueldo Variable 6 últimas líquidaciones de sueldo.


'. $variable6. '




';



   

        // Envio a zend Desk

        $create = json_encode(
                array(
            'ticket' => array(
                'requester' => array(
                    'name' => $usuario->getNombre() . $usuario->getApellido()  .  $usuario->getApellidoseg() , 
                    'email' => $usuario->getEmail(),
                ),
                'group_id' => '22',

                'subject' => $asuntoZend,
                'custom_fields' =>
                array(

                    '23412128' => $usuario->getRut(),
                    '23412138' => $NombreCompleto,
                    '23723488' => $usuario->getTelefono(),
                    '23723498' => $tipoCredito,
                    '23723588' => $pieS,
                    '23057889' =>  $porcentaje,
                    '23750746' => $montoS),


                'description' => $detalleZend
            )
                ), JSON_FORCE_OBJECT
        );

        $zendDesk = new zendesk();
        $data = $zendDesk->curlWrap("/tickets.json", $create, "POST");

      /* ticket zend desk */





        $respuesta = '100';
        
        }
        
        
        return new Response($respuesta);
    }

}
