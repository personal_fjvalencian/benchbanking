<?php

namespace Bench\CreditosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CreditoConsumo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CreditoConsumo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    

 /** @ORM\Column(type="string", length=50  , nullable=true) */
    
    
    private $montoCredito;
 /** @ORM\Column(type="string", length=50  , nullable=true) */
    
    private $plazoCredito;
    
    
    /** @ORM\Column(type="string", length=50  , nullable=true) */
    
    private $segurosAsociados;
    
    
   
    /** @ORM\Column(type="string", length=200  , nullable=true) */
    
    
    
    
    private $paraque;
    
 
   /** @ORM\Column(type="datetime"  , nullable=true) */

    
   
    private $fecha;
    
    
    
    /** @ORM\Column(type="string", length=50 , nullable=true) */
    
    private $estado;
    
    
    

   
    
   /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="creditoconsumo")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    
    
    private $usuario;
    
    


    /** @ORM\Column(type="string", length=50 , nullable=true) */


    private $tipoSolicitud;
    



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montoCredito
     *
     * @param string $montoCredito
     * @return CreditoConsumo
     */
    public function setMontoCredito($montoCredito)
    {
        $this->montoCredito = $montoCredito;
    
        return $this;
    }

    /**
     * Get montoCredito
     *
     * @return string 
     */
    public function getMontoCredito()
    {
        return $this->montoCredito;
    }

    /**
     * Set plazoCredito
     *
     * @param string $plazoCredito
     * @return CreditoConsumo
     */
    public function setPlazoCredito($plazoCredito)
    {
        $this->plazoCredito = $plazoCredito;
    
        return $this;
    }

    /**
     * Get plazoCredito
     *
     * @return string 
     */
    public function getPlazoCredito()
    {
        return $this->plazoCredito;
    }

    /**
     * Set segurosAsociados
     *
     * @param string $segurosAsociados
     * @return CreditoConsumo
     */
    public function setSegurosAsociados($segurosAsociados)
    {
        $this->segurosAsociados = $segurosAsociados;
    
        return $this;
    }

    /**
     * Get segurosAsociados
     *
     * @return string 
     */
    public function getSegurosAsociados()
    {
        return $this->segurosAsociados;
    }

    /**
     * Set paraque
     *
     * @param string $paraque
     * @return CreditoConsumo
     */
    public function setParaque($paraque)
    {
        $this->paraque = $paraque;
    
        return $this;
    }

    /**
     * Get paraque
     *
     * @return string 
     */
    public function getParaque()
    {
        return $this->paraque;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return CreditoConsumo
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CreditoConsumo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipoSolicitud
     *
     * @param string $tipoSolicitud
     * @return CreditoConsumo
     */
    public function setTipoSolicitud($tipoSolicitud)
    {
        $this->tipoSolicitud = $tipoSolicitud;
    
        return $this;
    }

    /**
     * Get tipoSolicitud
     *
     * @return string 
     */
    public function getTipoSolicitud()
    {
        return $this->tipoSolicitud;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return CreditoConsumo
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}