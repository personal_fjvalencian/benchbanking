<?php

namespace Bench\CreditosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CreditoCuenta
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CreditoCuenta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

 /** @ORM\Column(type="string", length=200  , nullable=true) */
    private $producto;
    
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */
    private $producto2;
    
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */
    private $producto3;
    
    
   /** @ORM\Column(type="datetime") */  
    private $fecha;
    
  /** @ORM\Column(type="string", length=50 , nullable=true) */
    private $estado;
    
       /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="creditocuenta")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    private $usuario;
    
    
    
    /** @ORM\Column(type="string", length=50 , nullable=true) */


    private $tipoSolicitud;

    
    
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set producto
     *
     * @param string $producto
     * @return CreditoCuenta
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;
    
        return $this;
    }

    /**
     * Get producto
     *
     * @return string 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set producto2
     *
     * @param string $producto2
     * @return CreditoCuenta
     */
    public function setProducto2($producto2)
    {
        $this->producto2 = $producto2;
    
        return $this;
    }

    /**
     * Get producto2
     *
     * @return string 
     */
    public function getProducto2()
    {
        return $this->producto2;
    }

    /**
     * Set producto3
     *
     * @param string $producto3
     * @return CreditoCuenta
     */
    public function setProducto3($producto3)
    {
        $this->producto3 = $producto3;
    
        return $this;
    }

    /**
     * Get producto3
     *
     * @return string 
     */
    public function getProducto3()
    {
        return $this->producto3;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return CreditoCuenta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CreditoCuenta
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipoSolicitud
     *
     * @param string $tipoSolicitud
     * @return CreditoCuenta
     */
    public function setTipoSolicitud($tipoSolicitud)
    {
        $this->tipoSolicitud = $tipoSolicitud;
    
        return $this;
    }

    /**
     * Get tipoSolicitud
     *
     * @return string 
     */
    public function getTipoSolicitud()
    {
        return $this->tipoSolicitud;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return CreditoCuenta
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}