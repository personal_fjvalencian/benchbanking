<?php

namespace Bench\CreditosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * CreditoHipotecario
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CreditoHipotecario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */
    public $valorPropiedad;
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */
    public $montoPie;
     /** @ORM\Column(type="string", length=200  , nullable=true) */

    public $porcentaje;
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */

    public $montoCredito;
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */

    public $plazoCredito;
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */
    
    private $tipoPropiedad;
    
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */


    public $estadoPropiedad;
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */

    public $dfl2;


    /** @ORM\Column(type="string", length=200  , nullable=true) */
   
    
    public $cuandoDeseaComprar;

    
   /** @ORM\Column(type="string", length=200  , nullable=true) */
    
    public $comunaComprar;
    
    
   
    /** @ORM\Column(type="string", length=200  , nullable=true) */


    public $proyecto;
    
    
    
    /** @ORM\Column(type="datetime") */

    public $fecha;
    
       /** @ORM\Column(type="string", length=50 , nullable=true) */

    public $estado;
    
    
       /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="creditohipotecario")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */

    public $usuario;
    

    /** @ORM\Column(type="string", length=50 , nullable=true) */


    private $tipoSolicitud;



    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valorPropiedad
     *
     * @param string $valorPropiedad
     * @return CreditoHipotecario
     */
    public function setValorPropiedad($valorPropiedad)
    {
        $this->valorPropiedad = $valorPropiedad;
    
        return $this;
    }

    /**
     * Get valorPropiedad
     *
     * @return string 
     */
    public function getValorPropiedad()
    {
        return $this->valorPropiedad;
    }

    /**
     * Set montoPie
     *
     * @param string $montoPie
     * @return CreditoHipotecario
     */
    public function setMontoPie($montoPie)
    {
        $this->montoPie = $montoPie;
    
        return $this;
    }

    /**
     * Get montoPie
     *
     * @return string 
     */
    public function getMontoPie()
    {
        return $this->montoPie;
    }

    /**
     * Set porcentaje
     *
     * @param string $porcentaje
     * @return CreditoHipotecario
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;
    
        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return string 
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * Set montoCredito
     *
     * @param string $montoCredito
     * @return CreditoHipotecario
     */
    public function setMontoCredito($montoCredito)
    {
        $this->montoCredito = $montoCredito;
    
        return $this;
    }

    /**
     * Get montoCredito
     *
     * @return string 
     */
    public function getMontoCredito()
    {
        return $this->montoCredito;
    }

    /**
     * Set plazoCredito
     *
     * @param string $plazoCredito
     * @return CreditoHipotecario
     */
    public function setPlazoCredito($plazoCredito)
    {
        $this->plazoCredito = $plazoCredito;
    
        return $this;
    }

    /**
     * Get plazoCredito
     *
     * @return string 
     */
    public function getPlazoCredito()
    {
        return $this->plazoCredito;
    }

    /**
     * Set tipoPropiedad
     *
     * @param string $tipoPropiedad
     * @return CreditoHipotecario
     */
    public function setTipoPropiedad($tipoPropiedad)
    {
        $this->tipoPropiedad = $tipoPropiedad;
    
        return $this;
    }

    /**
     * Get tipoPropiedad
     *
     * @return string 
     */
    public function getTipoPropiedad()
    {
        return $this->tipoPropiedad;
    }

    /**
     * Set estadoPropiedad
     *
     * @param string $estadoPropiedad
     * @return CreditoHipotecario
     */
    public function setEstadoPropiedad($estadoPropiedad)
    {
        $this->estadoPropiedad = $estadoPropiedad;
    
        return $this;
    }

    /**
     * Get estadoPropiedad
     *
     * @return string 
     */
    public function getEstadoPropiedad()
    {
        return $this->estadoPropiedad;
    }

    /**
     * Set dfl2
     *
     * @param string $dfl2
     * @return CreditoHipotecario
     */
    public function setDfl2($dfl2)
    {
        $this->dfl2 = $dfl2;
    
        return $this;
    }

    /**
     * Get dfl2
     *
     * @return string 
     */
    public function getDfl2()
    {
        return $this->dfl2;
    }

    /**
     * Set cuandoDeseaComprar
     *
     * @param string $cuandoDeseaComprar
     * @return CreditoHipotecario
     */
    public function setCuandoDeseaComprar($cuandoDeseaComprar)
    {
        $this->cuandoDeseaComprar = $cuandoDeseaComprar;
    
        return $this;
    }

    /**
     * Get cuandoDeseaComprar
     *
     * @return string 
     */
    public function getCuandoDeseaComprar()
    {
        return $this->cuandoDeseaComprar;
    }

    /**
     * Set comunaComprar
     *
     * @param string $comunaComprar
     * @return CreditoHipotecario
     */
    public function setComunaComprar($comunaComprar)
    {
        $this->comunaComprar = $comunaComprar;
    
        return $this;
    }

    /**
     * Get comunaComprar
     *
     * @return string 
     */
    public function getComunaComprar()
    {
        return $this->comunaComprar;
    }

    /**
     * Set proyecto
     *
     * @param string $proyecto
     * @return CreditoHipotecario
     */
    public function setProyecto($proyecto)
    {
        $this->proyecto = $proyecto;
    
        return $this;
    }

    /**
     * Get proyecto
     *
     * @return string 
     */
    public function getProyecto()
    {
        return $this->proyecto;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return CreditoHipotecario
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CreditoHipotecario
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipoSolicitud
     *
     * @param string $tipoSolicitud
     * @return CreditoHipotecario
     */
    public function setTipoSolicitud($tipoSolicitud)
    {
        $this->tipoSolicitud = $tipoSolicitud;
    
        return $this;
    }

    /**
     * Get tipoSolicitud
     *
     * @return string 
     */
    public function getTipoSolicitud()
    {
        return $this->tipoSolicitud;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return CreditoHipotecario
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}