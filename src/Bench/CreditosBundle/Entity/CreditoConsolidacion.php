<?php

namespace Bench\CreditosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CreditoConsolidacion
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CreditoConsolidacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    

 /** @ORM\Column(type="string", length=200 ,  nullable=true) */
    private $tipoCredito;
    
    
   
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */
    
    private $montoCredito;
     /** @ORM\Column(type="string", length=200  , nullable=true) */
    
    private $plazo;
    
    
    
   /** @ORM\Column(type="string", length=200 ,  nullable=true) */
    private $tipoCredito2;
    
    
    /** @ORM\Column(type="string", length=200 ,  nullable=true) */
      
    
    private $montoCredito2;
      
     
    /** @ORM\Column(type="string", length=200 ,  nullable=true) */
      
    private  $plazo2;
   
    
    
  /** @ORM\Column(type="string", length=200 ,  nullable=true) */
   
    private $tipoCredito3;
      
  
    /** @ORM\Column(type="string", length=200 ,  nullable=true) */
      
    
    private $montoCredito3;
      
     
    /** @ORM\Column(type="string", length=200 ,  nullable=true) */
      
    private  $plazo3;
    
    
    
 /** @ORM\Column(type="datetime" , nullable=true) */
    private $fecha;
    
    
     /** @ORM\Column(type="string", length=200  , nullable=true) */
    
    private $estado;


       /** @ORM\Column(type="string", length=50 , nullable=true) */


    private $tipoSolicitud;
    
    
    
         
      /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="creditoconsolidacion")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    private $usuario;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoCredito
     *
     * @param string $tipoCredito
     * @return CreditoConsolidacion
     */
    public function setTipoCredito($tipoCredito)
    {
        $this->tipoCredito = $tipoCredito;
    
        return $this;
    }

    /**
     * Get tipoCredito
     *
     * @return string 
     */
    public function getTipoCredito()
    {
        return $this->tipoCredito;
    }

    /**
     * Set montoCredito
     *
     * @param string $montoCredito
     * @return CreditoConsolidacion
     */
    public function setMontoCredito($montoCredito)
    {
        $this->montoCredito = $montoCredito;
    
        return $this;
    }

    /**
     * Get montoCredito
     *
     * @return string 
     */
    public function getMontoCredito()
    {
        return $this->montoCredito;
    }

    /**
     * Set plazo
     *
     * @param string $plazo
     * @return CreditoConsolidacion
     */
    public function setPlazo($plazo)
    {
        $this->plazo = $plazo;
    
        return $this;
    }

    /**
     * Get plazo
     *
     * @return string 
     */
    public function getPlazo()
    {
        return $this->plazo;
    }

    /**
     * Set tipoCredito2
     *
     * @param string $tipoCredito2
     * @return CreditoConsolidacion
     */
    public function setTipoCredito2($tipoCredito2)
    {
        $this->tipoCredito2 = $tipoCredito2;
    
        return $this;
    }

    /**
     * Get tipoCredito2
     *
     * @return string 
     */
    public function getTipoCredito2()
    {
        return $this->tipoCredito2;
    }

    /**
     * Set montoCredito2
     *
     * @param string $montoCredito2
     * @return CreditoConsolidacion
     */
    public function setMontoCredito2($montoCredito2)
    {
        $this->montoCredito2 = $montoCredito2;
    
        return $this;
    }

    /**
     * Get montoCredito2
     *
     * @return string 
     */
    public function getMontoCredito2()
    {
        return $this->montoCredito2;
    }

    /**
     * Set plazo2
     *
     * @param string $plazo2
     * @return CreditoConsolidacion
     */
    public function setPlazo2($plazo2)
    {
        $this->plazo2 = $plazo2;
    
        return $this;
    }

    /**
     * Get plazo2
     *
     * @return string 
     */
    public function getPlazo2()
    {
        return $this->plazo2;
    }

    /**
     * Set tipoCredito3
     *
     * @param string $tipoCredito3
     * @return CreditoConsolidacion
     */
    public function setTipoCredito3($tipoCredito3)
    {
        $this->tipoCredito3 = $tipoCredito3;
    
        return $this;
    }

    /**
     * Get tipoCredito3
     *
     * @return string 
     */
    public function getTipoCredito3()
    {
        return $this->tipoCredito3;
    }

    /**
     * Set montoCredito3
     *
     * @param string $montoCredito3
     * @return CreditoConsolidacion
     */
    public function setMontoCredito3($montoCredito3)
    {
        $this->montoCredito3 = $montoCredito3;
    
        return $this;
    }

    /**
     * Get montoCredito3
     *
     * @return string 
     */
    public function getMontoCredito3()
    {
        return $this->montoCredito3;
    }

    /**
     * Set plazo3
     *
     * @param string $plazo3
     * @return CreditoConsolidacion
     */
    public function setPlazo3($plazo3)
    {
        $this->plazo3 = $plazo3;
    
        return $this;
    }

    /**
     * Get plazo3
     *
     * @return string 
     */
    public function getPlazo3()
    {
        return $this->plazo3;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return CreditoConsolidacion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CreditoConsolidacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipoSolicitud
     *
     * @param string $tipoSolicitud
     * @return CreditoConsolidacion
     */
    public function setTipoSolicitud($tipoSolicitud)
    {
        $this->tipoSolicitud = $tipoSolicitud;
    
        return $this;
    }

    /**
     * Get tipoSolicitud
     *
     * @return string 
     */
    public function getTipoSolicitud()
    {
        return $this->tipoSolicitud;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return CreditoConsolidacion
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}