<?php

namespace Bench\CreditosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CreditoAutomotriz
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class CreditoAutomotriz
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
        /** @ORM\Column(type="string", length=200 , nullable=true) */
    private $monto;
    
        /** @ORM\Column(type="string", length=200 , nullable=true) */
    
    private $pie;
        /** @ORM\Column(type="string", length=200 , nullable=true) */
    
    private $plazo;
        /** @ORM\Column(type="string", length=200 , nullable=true) */
    
    private $cuandocompras;
    
    
        /** @ORM\Column(type="string", length=200 , nullable=true) */
    
    private $marcaauto;
    
        /** @ORM\Column(type="string", length=200 , nullable=true) */
    
    
    private $ano;
    
    
      /** @ORM\Column(type="datetime") */
    
    
    
    private $fecha;
    
    
      /** @ORM\Column(type="string", length=50 , nullable=true) */
    
    private $estado;

   /** @ORM\Column(type="string", length=50 , nullable=true) */


    private $tipoSolicitud;

    
    
        
   /**
     * @ORM\ManyToOne(targetEntity="Bench\UsuariosBundle\Entity\Usuario", inversedBy="creditoautomotriz")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    
    private $usuario;
    
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set monto
     *
     * @param string $monto
     * @return CreditoAutomotriz
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;
    
        return $this;
    }

    /**
     * Get monto
     *
     * @return string 
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set pie
     *
     * @param string $pie
     * @return CreditoAutomotriz
     */
    public function setPie($pie)
    {
        $this->pie = $pie;
    
        return $this;
    }

    /**
     * Get pie
     *
     * @return string 
     */
    public function getPie()
    {
        return $this->pie;
    }

    /**
     * Set plazo
     *
     * @param string $plazo
     * @return CreditoAutomotriz
     */
    public function setPlazo($plazo)
    {
        $this->plazo = $plazo;
    
        return $this;
    }

    /**
     * Get plazo
     *
     * @return string 
     */
    public function getPlazo()
    {
        return $this->plazo;
    }

    /**
     * Set cuandocompras
     *
     * @param string $cuandocompras
     * @return CreditoAutomotriz
     */
    public function setCuandocompras($cuandocompras)
    {
        $this->cuandocompras = $cuandocompras;
    
        return $this;
    }

    /**
     * Get cuandocompras
     *
     * @return string 
     */
    public function getCuandocompras()
    {
        return $this->cuandocompras;
    }

    /**
     * Set marcaauto
     *
     * @param string $marcaauto
     * @return CreditoAutomotriz
     */
    public function setMarcaauto($marcaauto)
    {
        $this->marcaauto = $marcaauto;
    
        return $this;
    }

    /**
     * Get marcaauto
     *
     * @return string 
     */
    public function getMarcaauto()
    {
        return $this->marcaauto;
    }

    /**
     * Set ano
     *
     * @param string $ano
     * @return CreditoAutomotriz
     */
    public function setAno($ano)
    {
        $this->ano = $ano;
    
        return $this;
    }

    /**
     * Get ano
     *
     * @return string 
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return CreditoAutomotriz
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CreditoAutomotriz
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipoSolicitud
     *
     * @param string $tipoSolicitud
     * @return CreditoAutomotriz
     */
    public function setTipoSolicitud($tipoSolicitud)
    {
        $this->tipoSolicitud = $tipoSolicitud;
    
        return $this;
    }

    /**
     * Get tipoSolicitud
     *
     * @return string 
     */
    public function getTipoSolicitud()
    {
        return $this->tipoSolicitud;
    }

    /**
     * Set usuario
     *
     * @param \Bench\UsuariosBundle\Entity\Usuario $usuario
     * @return CreditoAutomotriz
     */
    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Bench\UsuariosBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}