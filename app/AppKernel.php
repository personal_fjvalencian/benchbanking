<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    
     public function init()
    {
        date_default_timezone_set( 'America/Santiago' );
        parent::init();
    }
    
    public function registerBundles()
    {
        
        

        
        
        
        
        $bundles = array(


         
            new Ali\DatatableBundle\AliDatatableBundle(),
            new Ps\PdfBundle\PsPdfBundle(),
            new Spraed\PDFGeneratorBundle\SpraedPDFGeneratorBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),

           /* prueba api */
            new FOS\RestBundle\FOSRestBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle($this),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),

/* *///
             new Nelmio\SecurityBundle\NelmioSecurityBundle(),

            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Bench\UsuariosBundle\BenchUsuariosBundle(),
            new Bench\CreditosBundle\BenchCreditosBundle(),
            new Bench\SolicitudesCreditosBundle\BenchSolicitudesCreditosBundle(),
            new Bench\TienesBundle\BenchTienesBundle(),
            new Bench\DebesBundle\BenchDebesBundle(),
            new Bench\PaginasBundle\BenchPaginasBundle(),

            new Bench\RegistrosBundle\BenchRegistrosBundle(),
            new Bench\LoginBundle\BenchLoginBundle(),
            new Bench\AdminBundle\BenchAdminBundle(),
         
  
            new Bench\UploadArchivosBundle\BenchUploadArchivosBundle(),
            new Bench\UploadBundle\BenchUploadBundle(),
           
            new Bench\PdfBundle\BenchPdfBundle(),
            
            new Bench\FilepickerBundle\BenchFilepickerBundle(),
            new Bench\MobilBundle\BenchMobilBundle(),
            new Bench\ApiBundle\BenchApiBundle(),

            new Bench\TestBundle\BenchTestBundle()
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
