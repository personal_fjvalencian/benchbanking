<?php

/* BenchPaginasBundle:Default:headertab.html.twig */
class __TwigTemplate_b026429c3fc530b75ddc4427540d37c865c9f506e9f099c3c7989caf3e6455f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "



<header>

<meta http-equiv=\"X-Frame-Options\" content=\"deny\">
<meta http-equiv=\"cache-control\" content=\"max-age=0\" />
<meta http-equiv=\"cache-control\" content=\"no-cache\" />
<meta http-equiv=\"expires\" content=\"0\" />
<meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />
<meta http-equiv=\"pragma\" content=\"no-cache\" />

    <div class=\"wrapper-header\" >
        <a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" id=\"logo2\"><img  src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/logo2.png"), "html", null, true);
        echo "\"></a>
        <nav class=\"nav2\">
            <div id=\"menu-nav2\">
                <ul>
                    <li-nav>
                        
                        <a href=\"#\" class=\"top\">Qué Hacemos</a><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("quehacemos");
        echo "\" target=\"_parent\">Qué Hacemos</a>
                        
                    </li-nav>
                    <li-nav>
                        <a href=\"#\" class=\"top\">Conoce de Créditos</a><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("conocecreditos");
        echo "\" target=\"_parent\">Conoce de Créditos</a>
                    </li-nav>
                </ul>
            </div>

        </nav>
        <div class=\"box-btnes-right2\">

            <a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"btn-hm\" title=\"Home\"><span class=\"displace\">Home</span></a><br>
            <a href=\"#\" class=\"btn-fb\" title=\"Facebook\"><span class=\"displace\">Facebook</span></a><br>
            <!--<a href=\"#\" class=\"btn-tw\" title=\"Twitter\"><span class=\"displace\">Twitter</span></a><br>-->
        </div>

        <!--<a href=\"#\" class=\"btn-ajuste\" data-dropdown=\"#dropdown-1\"></a><br>-->
        <a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("logout");
        echo "\"><div class=\"btn-logout\">Cerrar Sesión</div></a>

        <div class=\"title-profile-header\">Bienvenido(a):<br>
            
            <span class=\"orange\"> ";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nombre", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellido", array()), "html", null, true);
        echo "</span></div>
            
        <div class=\"clearfix\"></div>

    </div>


    <!-- Remember to put your dropdown menus before your ending BODY tag -->
    <!--<div id=\"dropdown-1\" class=\"dropdown-menu has-tip\">
            <ul>
                    <li><a href=\"logout.php\" id=\"CerraSession\">Cerrar Sesión</a></li>
        <li class=\"divider\"></li>
                    <li><a href=\"#2\">Ver mis pedidos</a></li>
        <li class=\"divider\"></li>
                    <li><a href=\"#3\">Cerrar Sesión</a></li>
    
            </ul>
    </div>-->

</header> 















";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:headertab.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 44,  73 => 40,  64 => 34,  53 => 26,  46 => 22,  35 => 16,  19 => 2,);
    }
}
