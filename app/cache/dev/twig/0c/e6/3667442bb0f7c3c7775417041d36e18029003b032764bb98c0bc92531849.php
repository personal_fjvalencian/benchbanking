<?php

/* BenchAdminBundle:Default/layer:modalSeleccionBancos.html.twig */
class __TwigTemplate_0ce63667442bb0f7c3c7775417041d36e18029003b032764bb98c0bc92531849 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " <!-- Modal -->
 <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
     <div class=\"modal-header\">
         <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
         <h3 id=\"myModalLabel\">Seleccione formato exportacion</h3>
     </div>
     <div class=\"modal-body\">


         <p><a href=\"\" id=\"estandar\" target=\"_blank\">Estandar </a></p>

         <p><a href=\"\" id=\"bbva\" target=\"_blank\"> BBVA </a></p>
         <p><a href=\"\" id=\"bbva2\" target=\"_blank\"> BBVA PRE APROBACION </a></p>
         <p><a href=\"\" id=\"bci\" target=\"_blank\">BCI </a></p>
         <p><a href=\"\" id=\"consorcio\" target=\"_blank\">CONSORCIO </a></p>
         <p><a href=\"\" id=\"metlife\" target=\"_blank\">METLIFE </a></p>


     </div>
     <div class=\"modal-footer\">
         <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>

     </div>
 </div>
";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default/layer:modalSeleccionBancos.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
