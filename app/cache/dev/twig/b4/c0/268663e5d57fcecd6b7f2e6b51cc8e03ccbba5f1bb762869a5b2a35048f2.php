<?php

/* BenchUploadBundle:Default:upload9.html.twig */
class __TwigTemplate_b4c0268663e5d57fcecd6b7f2e6b51cc8e03ccbba5f1bb762869a5b2a35048f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form  method=\"post\" action=\"";
        echo $this->env->getExtension('routing')->getPath("carnet");
        echo "\">

    <div id=\"uploader12\" style=\"width:950px;height:200px;margin-top:250px;position: relative;\">
        
        <p></p>
        
        
    </div>
</form>

<script type=\"text/javascript\">


    \$(document).ready(function() {



        \$(function() {
            \$(\"#uploader12\").plupload({
              
                runtimes: 'html5,flash',
                url: 'carnet',
                max_file_size: '1000mb',
                autostart : true,
                max_file_count: 20, 
                chunk_size: '1mb',
                rename: true,
                multiple_queues: true,
              
                rename: true,
                     
                        sortable: true,
              
                filters: [
                    {title: \"Image files\", extensions: \"jpg,gif,png\"},
                    {title: \"Zip files\", extensions: \"zip,avi,pdf,doc,pdf\"}
                ],
          
          
          
                flash_swf_url: \"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.flash.swf"), "html", null, true);
        echo "\",
              
                silverlight_xap_url: \"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.silverlight.xap"), "html", null, true);
        echo "\",
            });

            \$('form').submit(function(e) {
                var uploader = \$('#uploader').plupload('getUploader');

                if (uploader.files.length > 0) {
                
                
                
                    uploader.bind('StateChanged', function() {
                        if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                            \$('form')[0].submit();
                        }
                    });

                    uploader.start();
                } else
                    alert('You must at least upload one file.');

                return false;
            });


        });
    });
    </script>
    


";
    }

    public function getTemplateName()
    {
        return "BenchUploadBundle:Default:upload9.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 43,  63 => 41,  19 => 1,);
    }
}
