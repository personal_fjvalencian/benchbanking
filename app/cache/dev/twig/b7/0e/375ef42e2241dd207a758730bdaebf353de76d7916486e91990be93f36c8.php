<?php

/* BenchPaginasBundle:Default:remax.html.twig */
class __TwigTemplate_b70e375ef42e2241dd207a758730bdaebf353de76d7916486e91990be93f36c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "
<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>Remax</title>

<style>

header{
    width: 626px;
    height: 160px;
    margin: 0 auto;
}

.container-js{
    width: 1000px;
    height: 1200px;
    margin: 0 auto;
    position: relative;
    
}

</style>

</head>

<body>
    
    <header>
        <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/header.png"), "html", null, true);
        echo "\"/>
    </header>
    <div class=\"container-js\">

        
<script type=\"text/javascript\" src=\"https://www.formstack.com/forms/js.php?1660803-WhGckOFoZp-v3\"></script><noscript><a href=\"https://www.formstack.com/forms/?1660803-WhGckOFoZp\" title=\"Online Form\">Online Form - Formulario REMAX</a></noscript>
    </div>

</body>


<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
<script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>

<form action=\"\" id=\"formRemax\"></form>
<input type=\"hidden\" id=\"userApi\" value=\"bench\">
<input type=\"hidden\" id=\"userPassword\" value=\"bench2012\" >
<input type=\"hidden\" id=\"origin\" value=\"remax\">
<input type=\"hidden\" id=\"token\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">


<script>
    \$( document ).ready(function() {







        \$( \"#fsSubmitButton1660803\" ).click(function() {

            var userApi = \$('#userApi').val();
            var userPassword = \$('#userPassword').val();
            var token = \$('#token').val();
            var origin = \$('#origin').val();



            /* Datos del ejecutivo */


            var ejecutivoNombre = \$('#field23456146').val();
            var ejecutivoEmail = \$('#field23455943').val() ;
            var ejecutivoTelefono = \$('#field23455945').val();

           /* Datos del cliente */

            var clienteRut = \$('#field23455872').val();
            var clienteRut =   \$.Rut.formatear(clienteRut);
            var clienteTelefono = \$('#field23455872').val();
            var clieteSueldo = \$('#field23455917').val();
            var clienteNombreApellido = \$('#field23455917').val();
            var clienteEmail = \$('#field23455896').val();

            /* Datos de la propiedad  */


            var propiedadValor = \$('#field23594511').val();
            var propiedadMonto = \$('#field23594512').val();
            var propiedadTipo = \$('#field23594515').val();
            var propiedadComuna = \$('#field23594518').val();
            var propiedadProyecto = \$('#field23594519').val();
            var propiedadPlazo = \$('#field23594513').val();
            var propiedadDfl2Si = \$('#field23594514_1').val();
            var propiedadDfl2No = \$('#field23594514_2').val();
            var propiedadEstado = \$('#field23594516').val();
            var propiedadCuando = \$('#field23594517').val();

            if(clienteRut != '' &&  clienteTelefono != '' && clienteNombreApellido != '' && clienteEmail != ''  ){



            var myData = 'ejecutivoNombre='+ejecutivoNombre
                    + '&userApi='+ userApi
                    + '&userPassword=' + userPassword
                    + '&token=' + token
                    + '&origin=' + origin

                    + '&ejecutivoEmail='+ejecutivoEmail
                    + '&ejecutivoTelefono='+ ejecutivoTelefono

                    + '&clienteRut='+clienteRut
                    + '&clienteTelefono ='+clienteTelefono
                    + '&clieteSueldo ='+clieteSueldo
                    + '&clienteNombreApellido='+ clienteNombreApellido
                    + '&clienteEmail='+ clienteEmail

                    + '&propiedadValor='+ propiedadValor
                    + '&propiedadMonto='+ propiedadMonto
                    + '&propiedadTipo='+ propiedadTipo
                    + '&propiedadComuna='+ propiedadComuna
                    + '&propiedadProyecto='+ propiedadProyecto
                    + '&propiedadPlazo='+ propiedadPlazo

                    + '&propiedadDfl2Si ='+ propiedadDfl2Si
                    + '&propiedadDfl2No ='+ propiedadPlazo
                    + '&propiedadEstado ='+ propiedadCuando


                    ;



            jQuery.ajax({
                type: \"POST\", // metodo post o get
                url: \$('#formRemax').attr('action'),
                dataType: \"text\", // tipo datos text o json etc
                data:myData,
                success:function(response){


                    var respuesta = response;

                    if(respuesta == '200'){





                    }else{






                    }
                },

                error:function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);



                }

            });




}

        });



    });

</script>
</html>


";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:remax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 51,  66 => 45,  51 => 33,  19 => 3,);
    }
}
