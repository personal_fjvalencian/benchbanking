<?php

/* BenchFilepickerBundle:Default:SueldoVariable6.html.twig */
class __TwigTemplate_f485a9a02393554d6c72b6c2d8c024ee245abe13509afb3273e3ac930f1269ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/js/variable6.js"), "html", null, true);
        echo "\"></script>
<div class=\"container-upload\">

<div class=\"box-upload\">
<span class=\"orange\">Sueldo Variable 6 últimas líquidaciones de sueldo. <span class=\"color-porcentaje\" id=\"porsueldoVariable6\"></span><span class=\"carga-ie\" id=\"sueldoVariableie\"><img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/img/load.gif"), "html", null, true);
        echo "\"></span>  </span>







        <ul  id=\"listaVariable6\" style=\"width:600px;height:auto;\">
            
       
        
       
        
        ";
        // line 19
        if ((isset($context["archivo2"]) ? $context["archivo2"] : $this->getContext($context, "archivo2"))) {
            // line 20
            echo "        
        ";
            // line 21
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["archivo2"]);
            foreach ($context['_seq'] as $context["_key"] => $context["archivo2"]) {
                // line 22
                echo "       

            
            <li  class=\"li-upload\" id=\"itemVariable6-";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo2"], "llave", array()), "html", null, true);
                echo "\">
                
            <a href=\"";
                // line 27
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo2"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> <img src=\"img/fileicon.jpg\"> <span id=\"nombrefile\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo2"], "nombreArchivo", array()), "html", null, true);
                echo "</a></span><button class=\"del_button\" id=\"del-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo2"], "llave", array()), "html", null, true);
                echo "\">X</button>
            
            </li>
            
         
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivo2'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "        ";
        }
        echo "    
            
        
        
                                                                                                                                                                                                      
        </ul>
        <!--<input type=\"file\" id=\"AgregarVariable6\" class=\"buttonupload\">-->
        
        <div style=\"clear: both;\"></div>
        
        <div class=\"file-input-wrapper\">
        <button class=\"btn-file-input\">Agregar Archivos</button>
        <input type=\"file\" name=\"file\" id=\"AgregarVariable6\"/>
       </div>
        
        
        
      
        
     
        
        
        
        </div>
    
    </div>


";
    }

    public function getTemplateName()
    {
        return "BenchFilepickerBundle:Default:SueldoVariable6.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 33,  63 => 27,  58 => 25,  53 => 22,  49 => 21,  46 => 20,  44 => 19,  27 => 5,  19 => 1,);
    }
}
