<?php

/* BenchPaginasBundle:Default:headerCreditos.html.twig */
class __TwigTemplate_1d3cd547cdbf0edbd1d1ee01143b4c3aee7207742c5258d55b07de1246bbfbf0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        $this->displayBlock('header', $context, $blocks);
        // line 195
        echo "



";
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "
<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\"></script>
<meta name=\"google-site-verification\" content=\"4sibG4Sz2wGxTyteKnT1E8uJM6vt67srq5ANMPkQ5Vg\" />
<META HTTP-EQUIV=\"CACHE-CONTROL\" CONTENT=\"NO-CACHE\">
<meta http-equiv=\"X-Frame-Options\" content=\"deny\">
<link rel=\"shortcut icon\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
<link rel=\"icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchlogin/js/login.js"), "html", null, true);
        echo "\"></script>


<META Http-Equiv=\"Cache-Control\" Content=\"no-cache\">
<META Http-Equiv=\"Pragma\" Content=\"no-cache\">
<META Http-Equiv=\"Expires\" Content=\"0\">
<header>

<div class=\"wrapper-header\">
   <a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" id=\"logo\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/logo.png"), "html", null, true);
        echo "\"></a>
  <nav>
<style>



#menu-nav2{
    height: 220px; width: 325px;
    overflow: hidden;
    text-align: center;
}

#menu-nav2 ul li-nav {
    float: left;
    margin: 0 1px;
    list-style-type: none;
}

/* Menu Link Styles */
#menu-nav2 ul a.top {
    background: #F95B00; color: #fff;
}

#menu-nav2 ul a {
    display: block;
    background: #E8501F; color: #fff;
    height: 220px; width: 160px;
    font: lighter 1em/200px 'junctionregular';
    text-decoration: none;
}  

.form-control{
    height: 30px;
    width: 100px;

} 


#formlogin{
 margin-left: -2%;
 position: relative;

}

.box-login{
    position:relative;
    margin-left: 2.1%;
    height: 220px;
}


  </style>
<div id=\"menu-nav2\">
<ul>
        
        <li-nav>
        <a href=\"";
        // line 76
        echo $this->env->getExtension('routing')->getPath("quehacemos");
        echo "\" class=\"top\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_qa2.png"), "html", null, true);
        echo "\"></a><a href=\"";
        echo $this->env->getExtension('routing')->getPath("quehacemos");
        echo "\" target=\"_parent\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_qa.png"), "html", null, true);
        echo "\"></a>
        </li-nav>
        <li-nav>
        <a href=\"";
        // line 79
        echo $this->env->getExtension('routing')->getPath("conocecreditos");
        echo "\" class=\"top\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_cdc2.png"), "html", null, true);
        echo "\"></a><a href=\"";
        echo $this->env->getExtension('routing')->getPath("conocecreditos");
        echo "\" target=\"_parent\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_cdc.png"), "html", null, true);
        echo "\"></a>
        </li-nav>
    </ul>
</div>
</nav>
 
           <div class=\"box-login\">
           <h1 style=\"font-size:20px;margin-top:-5%;margin-left: -2%;\">Iniciar Sesión</h1>
           <div id=\"div_carga\"><img id=\"cargador\" src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/loader2.gif"), "html", null, true);
        echo "\"/></div>
           <form class=\"form-login\" id=\"formlogin\" action=\"";
        // line 88
        echo $this->env->getExtension('routing')->getPath("login");
        echo "\" id=\"ingreso\"  method=\"post\" autocomplete=\"off\" >
           <label class=\"col-sm-2 control-label\" style=\"margin-top: -4%;\"><p>RUT</p></label>
           <input name=\"rut\" class=\"form-control\"  style=\"width:240px; height:23px;margin-top:-3%;\" id=\"rut\" type=\"text\">
           <label class=\"col-sm-2 control-label\"><p>Clave</p></label>
           
           <input name=\"contrasena\" id=\"contrasenaform\"  style=\"width:240px;height:23px;margin-top:-3%;\" class=\"form-control\" type=\"password\">
           <br>
           <input name=\"\" class=\"classname\" type=\"submit\"  value=\"Acceder\" id=\"butingreso\"  style=\"margin-top:-10%;position:relative;\"onClick=\"\">
           
               
          </form>
           <div  id=\"alert\" class=\"alert-form\"> </div>
          
      
           <div class=\"btn-register\" style=\"margin-left:-1%;\"><a href=\"";
        // line 102
        echo $this->env->getExtension('routing')->getPath("registro");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_register.png"), "html", null, true);
        echo "\" width=\"45\" height=\"40\">   <span class=\"white\">¡Regístrate !</span></a></div>
           <div class=\"alert-form3\"><p><a href=\"";
        // line 103
        echo "recupera";
        echo "\">¿Has olvidado tu contraseña?</a></p></div>
        </div>
       
       <div class=\"box-btnes-right\">
       
  <a href=\"";
        // line 108
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"btn-hm\" title=\"Home\"><span class=\"displace\">Home</span></a><br>

  <a href=\"https://twitter.com/benchbanking\" class=\"btn-tw\" title=\"Twitter\"><span class=\"displace\">Twitter</span></a><br>
  </div>
  <div class=\"clearfix\"></div>
  
  </div>

</header> 
<input type=\"hidden\" id=\"token\" value=\"";
        // line 117
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">
<script>

\$(document).ready(function() {
    \$(\"#butingreso\").click(function(e) {

      \$(\".spinner\").css(\"display\", \"block\");





        e.preventDefault();
        var myData = 'rut=' + \$(\"#rut\").val()
                + '&contrasena=' + \$(\"#contrasenaform\").val()
                + '&token=' + \$('#token').val(); 
                ;


        var token =  \$('#token').val(); 
      

        jQuery.ajax({
            type: \"POST\", // metodo post o get 
            url: \$(\"#formlogin\").attr(\"action\"),
            dataType: \"text\", // tipo datos text o json etc 
            data: myData,
            success: function(response) {



      \$(\".spinner\").css(\"display\", \"none\");



                \$('#loading').append(' ');


                var respuesta = response;
                

                if (respuesta == 100) {
      var username=\$(\"#rut\").val();
  
{

}



                    window.location = \"";
        // line 167
        echo $this->env->getExtension('routing')->getPath("formulario");
        echo "\";


                } else {


                    \$('#alert').empty();
                    \$(\"#alert\").append(response);


                }



            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);



            },
        });
    });
});


</script>
";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:headerCreditos.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  254 => 167,  201 => 117,  189 => 108,  181 => 103,  175 => 102,  158 => 88,  154 => 87,  137 => 79,  125 => 76,  64 => 20,  52 => 11,  48 => 10,  44 => 9,  37 => 4,  34 => 3,  26 => 195,  24 => 3,  20 => 1,);
    }
}
