<?php

/* BenchPaginasBundle:Default:quehacemos.html.twig */
class __TwigTemplate_840c9cd771658534f626d5221d6b4cb14c0f7208edf0ef084f5cee90caf1a5aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE HTML>
  
<html>

<head>
        
    
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">

<link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
<link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/ship.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/basic.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/flexslider.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 

<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.flexslider-min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.carouFredSel-6.1.0-packed.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciador.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/botoneshead.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/html5.js"), "html", null, true);
        echo "\"></script>

<META name=\"keywords\" content=\"credito hipotecario, credito consumo, simulador, tasa, banco\">
<title> BenchBanking │ Créditos Convenientes </title>
<META name=\"description\" content=\"Créditos más convenientes y mejor informados, compara las tasas antes de contratarlo\">  
<meta name=\"google-site-verification\" content=\"4sibG4Sz2wGxTyteKnT1E8uJM6vt67srq5ANMPkQ5Vg\" />
<link rel=\"shortcut icon\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
<link rel=\"icon\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
</head>

<body>
    
    ";
        // line 34
        $this->env->loadTemplate("BenchPaginasBundle:Default:header.html.twig")->display($context);
        // line 35
        echo "    ";
        $this->displayBlock('header', $context, $blocks);
        // line 43
        echo "    
    
    
    <input type=\"hidden\" id=\"token\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">
    
    
    
<div class=\"wrapper\" style=\"padding-top:20px;\">

<div class=\"container-sections\">

<div class=\"h7\" style=\"margin:10px auto;\">Qué Hacemos</div>
\t\t\t<p class=\"gray\">Pedir un préstamo es una tarea abrumadora, toma mucho tiempo y es difícil de entender. En nuestra plataforma puedes subir en un solo lugar tu  información, elige los bancos donde deseas cotizar y nosotros se la enviamos.</p>
            <p class=\"gray\">&nbsp;</p>
\t\t    <br>
            <h6 class=\"gray\">¡ÚNETE!</h6>
<br>
            
            <div style=\"margin:auto; text-align:center;\"><img src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/flujo.jpg"), "html", null, true);
        echo "\"></div>
            
            </div>
               
</div>

<div class=\"clearfix\"></div>



</body>





<script src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.flexslider-min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.carouFredSel-6.1.0-packed.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciador.js"), "html", null, true);
        echo "\"></script>


  ";
        // line 84
        $this->env->loadTemplate("BenchPaginasBundle:Default:footer.html.twig")->display($context);
        // line 85
        echo "





</html> ";
    }

    // line 35
    public function block_header($context, array $blocks = array())
    {
        // line 36
        echo "    
    
    
    
    
    
    ";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:quehacemos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 36,  174 => 35,  164 => 85,  162 => 84,  156 => 81,  152 => 80,  148 => 79,  144 => 78,  140 => 77,  121 => 61,  103 => 46,  98 => 43,  95 => 35,  93 => 34,  85 => 29,  81 => 28,  72 => 22,  68 => 21,  64 => 20,  60 => 19,  56 => 18,  52 => 17,  48 => 16,  43 => 14,  39 => 13,  35 => 12,  31 => 11,  20 => 2,);
    }
}
