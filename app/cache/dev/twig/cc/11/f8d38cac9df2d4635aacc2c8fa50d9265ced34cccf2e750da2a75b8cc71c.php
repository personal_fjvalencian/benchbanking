<?php

/* BenchAdminBundle:Default:cotizar.html.twig */
class __TwigTemplate_cc11f8d38cac9df2d4635aacc2c8fa50d9265ced34cccf2e750da2a75b8cc71c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\" />
  <title>Administrador Bench Banking </title>
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
  <meta name=\"description\" content=\"\" />
  <meta name=\"author\" content=\"\" />
  <META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, FOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"INDEX, NOFOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, NOFOLLOW\">
<meta name=\"robots\" content=\"noindex\">
<script>
document.querySelector('input').onchange = function(){
  \tdocument.querySelector('label').innerHTML = this.value;
};

</script>
<style>
.cancelButton{
    
\tbackground-color:#768d87;
    
    
    
    
    
    
    }
        
* {
  margin: 0;
  padding: 0;
  font-family: segoe ui, helvetica neue, helvetica, arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  font-weight: 200;
}

input {
  display: none;
}

label {
  padding: 10px;
  background: #dedede;
  margin: 5px auto;
  display: block;
  width: 200px;
  cursor: pointer;
  color: #444;
  font-size: 90%;
  background: linear-gradient(#eee,#ddd);
  color: #666;
  border-radius: 2px;
  box-shadow: inset 0px 1px 0px rgba(255, 255, 255, 0.5);
  border: 1px solid #d2d2d2;
}

button {
\t-moz-box-shadow:inset 0px 1px 3px 0px #91b8b3;
\t-webkit-box-shadow:inset 0px 1px 3px 0px #91b8b3;
\tbox-shadow:inset 0px 1px 3px 0px #91b8b3;
\tbackground:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #768d87), color-stop(1, #6c7c7c));
\tbackground:-moz-linear-gradient(top, #768d87 5%, #6c7c7c 100%);
\tbackground:-webkit-linear-gradient(top, #768d87 5%, #6c7c7c 100%);
\tbackground:-o-linear-gradient(top, #768d87 5%, #6c7c7c 100%);
\tbackground:-ms-linear-gradient(top, #768d87 5%, #6c7c7c 100%);
\tbackground:linear-gradient(to bottom, #768d87 5%, #6c7c7c 100%);
\tfilter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#768d87', endColorstr='#6c7c7c',GradientType=0);
\tbackground-color:#768d87;
\t-moz-border-radius:5px;
\t-webkit-border-radius:5px;
\tborder-radius:5px;
\tborder:1px solid #566963;
\tdisplay:inline-block;
\tcursor:pointer;
\tcolor:#ffffff;
\tfont-family:arial;
                height: 10px;
\tfont-size:10px;
\tfont-weight:bold;
\tpadding:11px 23px;
\ttext-decoration:none;
\ttext-shadow:0px -1px 0px #2b665e;
}
button:hover {
\tbackground:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #6c7c7c), color-stop(1, #768d87));
\tbackground:-moz-linear-gradient(top, #6c7c7c 5%, #768d87 100%);
\tbackground:-webkit-linear-gradient(top, #6c7c7c 5%, #768d87 100%);
\tbackground:-o-linear-gradient(top, #6c7c7c 5%, #768d87 100%);
\tbackground:-ms-linear-gradient(top, #6c7c7c 5%, #768d87 100%);
\tbackground:linear-gradient(to bottom, #6c7c7c 5%, #768d87 100%);
\tfilter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#6c7c7c', endColorstr='#768d87',GradientType=0);
\tbackground-color:#6c7c7c;
}
button:active {
\tposition:relative;
\ttop:1px;
}

.texto1{
    margin-top: -12%;
    }
</style>

  <link href=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"main-theme-script\" />
  <link href=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-specific-script\" />
  <script src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/mask.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.js"), "html", null, true);
        echo "\"></script>





     <script src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

     <script src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.columnFilter.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

  
    
     

  
  <link href=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/css/datepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  


  <link rel=\"stylesheet\" type=\"text/css\" media=\"screen,projection\" href=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />
  

  <link type=\"text/css\" href=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.intenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />   

 
  <link href=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/simplenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  

  <link rel=\"shortcut icon\" href=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/favicon.ico"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-144-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-114-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-72-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" href=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-57-precomposed.png"), "html", null, true);
        echo "\" />
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>
<body id=\"dashboard\" class=\"hidden\">






 <div id=\"div_carga\">
   <img id=\"cargador\" src=\"./img/loader.gif\"/>
   </div>

<!-- Top navigation bar -->
<div class=\"navbar navbar-fixed-top\">
  <div class=\"navbar-inner\">
    <div class=\"container-fluid\">
      <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </a>
      <a class=\"brand\" href=\"index.html\">Bench banking</a>
   
      <div class=\"nav-collapse\">
          
          
        <ul class=\"nav\">
        
              
          <li class=\"dropdown\">
              
          </li>
       
        </ul>
      </div>
    </div>
  </div>
</div>
  
<div class=\"container-fluid\">
  <div class=\"row-fluid\">
    
    <!-- Bread Crumb Navigation -->
  
      

   
  
         
         <!-- Portlet Set 5 -->

             

        
\t  <!-- Table -->
      
      
         \t <!-- Portlet: Browser Usage Graph -->
            
                 <h4 class=\"box-header round-top\"></h4>
                 <H4> Cotizar a Usuario rut / ";
        // line 203
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "rut", array()), "html", null, true);
        echo "</H4>
       
                 
                     
              </h4>         
              
      
      

                     <section id=\"contenido\">
                         
                         
          

                         
                         

                         
                         
                             <script type=\"text/javascript\" src=\"//api.filepicker.io/v1/filepicker.js\"></script>
        <link rel=\"stylesheet\" href=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/plug/docsupport/style.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/plug/docsupport/prism.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/plug/chosen.css"), "html", null, true);
        echo "\">
        <link href=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"main-theme-script\" />
        <link href=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-specific-script\" />
        <script src=\"";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/cotizarConsumo.js.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/cotizarAutomotriz.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 231
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/cotizarHipotecario.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 232
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/cotizarConsoli.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/cotizarCuenta.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
        
        <script src=\"";
        // line 235
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

    </head>


    <body>
        <input type=\"hidden\" value=\"";
        // line 241
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "id", array()), "html", null, true);
        echo "\"  id=\"usuarioid\">


        <div style=\"width:80%;height:auto;margin-left: 2%;position:relative; \">
            <h4> Lista de créditos solicitados  </h4>
";
        // line 246
        $context["CreditoConsumo"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditoconsumo", array());
        // line 247
        echo " ";
        $context["CreditoAutomotriz"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditoautomotriz", array());
        echo "      
  ";
        // line 248
        $context["CreditoHipotecario"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditohipotecario", array());
        echo "       
   ";
        // line 249
        $context["CreditoConsolidacion"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditoconsolidacion", array());
        echo "  
    ";
        // line 250
        $context["creditocuenta"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditocuenta", array());
        echo "    
    ";
        // line 251
        $context["cotizaciones"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "cotizaciones", array());
        echo "        

            <table class=\"table table-striped table-bordered table-condensed\"  id=\"tablaCotizaciones\">
                <thead>

                    <tr> 

                        <td width=\"10%\">Monto</td>
                        <td width=\"10%\" >Plazo</td>
                        <td width=\"10%\">tipo</td>
                        <td width=\"10%\">Selccionar</td>
                    </tr>
                </thead>
                <tbody>

                  ";
        // line 266
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["CreditoConsumo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["CreditoConsumo"]) {
            // line 267
            echo "                   
                    <tr id=\"Primeraconsumo-\"";
            // line 268
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "id", array()), "html", null, true);
            echo ">
                        
                        
                        
                        

                        <td >";
            // line 274
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "montoCredito", array()), "html", null, true);
            echo "<input type=\"hidden\" id=\"consumoMonto-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "montoCredito", array()), "html", null, true);
            echo "\"><input type=\"hidden\" id=\"usuario-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "id", array()), "html", null, true);
            echo "\"  value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "id", array()), "html", null, true);
            echo "\"></td>
                        <td>";
            // line 275
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "plazoCredito", array()), "html", null, true);
            echo " <input type=\"hidden\"  id=\"consumoPlazo-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "id", array()), "html", null, true);
            echo "\"   value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "plazoCredito", array()), "html", null, true);
            echo "\"></td>
                        <td>Crédito Consumo <input type=\"hidden\" id=\"consumoTipo-";
            // line 276
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "id", array()), "html", null, true);
            echo "\" value=\"Crédito Consumo\" ></td>

                        <td><button id=\"idConsumo-";
            // line 278
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "id", array()), "html", null, true);
            echo "\" class=\"del_button\"  ><p class=\"texto1\">Seleccionar</p></button></td>



                    </tr>
                    
         
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['CreditoConsumo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 286
        echo "
                    <!-- INICIO  Credito Automotriz -->

                   ";
        // line 289
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["CreditoAutomotriz"]);
        foreach ($context['_seq'] as $context["_key"] => $context["CreditoAutomotriz"]) {
            // line 290
            echo "                    
        
                    <tr>

                        <td>";
            // line 294
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "monto", array()), "html", null, true);
            echo " <input type=\"hidden\" id =\"automotrizMonto-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "monto", array()), "html", null, true);
            echo "\" ></td>
                        <td>";
            // line 295
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "plazo", array()), "html", null, true);
            echo " <input type=\"hidden\" id=\"automotrizPlazo-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "plazo", array()), "html", null, true);
            echo "\"></td>
                        <td>Crédito Automotriz <input type=\"hidden\" id=\"automotrizTipo-";
            // line 296
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "id", array()), "html", null, true);
            echo "\" value=\"Crédito Automotriz\" ></td>
                        <td><button id=\"idAutomotriz-";
            // line 297
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "id", array()), "html", null, true);
            echo "\" class=\"del_buttonAutos\" ><p class=\"texto1\">Seleccionar</p></button></td>

                    </tr>
    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['CreditoAutomotriz'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 302
        echo "


                    ";
        // line 305
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["CreditoHipotecario"]);
        foreach ($context['_seq'] as $context["_key"] => $context["CreditoHipotecario"]) {
            // line 306
            echo "           
                    <tr>

                        <td>";
            // line 309
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "montoCredito", array()), 0, ".", "."), "html", null, true);
            echo " <input type=\"hidden\" id =\"HipotecarioMonto-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "montoCredito", array()), 0, ".", "."), "html", null, true);
            echo "\" ></td>
                        <td>";
            // line 310
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "plazoCredito", array()), "html", null, true);
            echo " <input type=\"hidden\" id=\"HipotecarioPlazo-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "plazoCredito", array()), "html", null, true);
            echo "\"></td>
                        <td>Crédito Hipotecario <input type=\"hidden\" id=\"HipotecarioTipo-";
            // line 311
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "id", array()), "html", null, true);
            echo "\" value=\"Crédito Hipotecario\"></td>
                        <td><button id=\"idHipotecario-";
            // line 312
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "id", array()), "html", null, true);
            echo "\" class=\"del_buttonHipotecario\"><p class=\"texto1\">Seleccionar</p></button></td>

                    </tr>    

                    <!-- FIN Credito Automotriz -->
           
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['CreditoHipotecario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 319
        echo "

                    <!-- Credito consolidacion -->
                  ";
        // line 322
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["CreditoConsolidacion"]);
        foreach ($context['_seq'] as $context["_key"] => $context["CreditoConsolidacion"]) {
            // line 323
            echo "               
                    
                    <tr>
                        
                        
      <td>";
            // line 328
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "montoCredito", array()), "html", null, true);
            echo " <input type=\"hidden\" id =\"ConsolidacionMonto-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "montoCredito", array()), "html", null, true);
            echo "\" ></td>
     <td>";
            // line 329
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "plazo", array()), "html", null, true);
            echo " <input type=\"hidden\" id=\"ConsolidacionPlazo-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "id", array()), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "plazo", array()), "html", null, true);
            echo "\"></td>
     <td>Crédito Consolidacion Deuda <input type=\"hidden\" id=\"ConsolidacionTipo-";
            // line 330
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "id", array()), "html", null, true);
            echo "\" value=\"Crédito Consolidacion Deuda\"></td>
     <td><button id=\"idConsolidacion-";
            // line 331
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "id", array()), "html", null, true);
            echo "\" class=\"del_buttonConsolidacion\"><p class=\"texto1\">Seleccionar</p></button></td>
 
                    </tr>
                  
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['CreditoConsolidacion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 336
        echo "                    
                    
                    ";
        // line 338
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["creditocuenta"]);
        foreach ($context['_seq'] as $context["_key"] => $context["creditocuenta"]) {
            // line 339
            echo "                   
                    
                    
                    <tr>
                        <td>";
            // line 343
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditocuenta"], "producto", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditocuenta"], "producto2", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditocuenta"], "producto3", array()), "html", null, true);
            echo "</td>
                        <td></td>
                           <td>Cuenta Corriente <input type=\"hidden\" id=\"CuentaTipo-";
            // line 345
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditocuenta"], "id", array()), "html", null, true);
            echo "\" value=\"Cuenta Corriente\"></td>

                       <td><button id=\"idCuenta-";
            // line 347
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditocuenta"], "id", array()), "html", null, true);
            echo "\" class=\"del_buttonCuenta\"><p class=\"texto1\">Seleccionar</p></button></td>
 
                        
                    
                    
                    </tr>
         
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditocuenta'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 355
        echo "                    
                    
                
                    
                    
                        <!-- FIN  Credito consolidacion -->

                </tbody>
            </table>


            <h4>Subir cotizaciones nuevas. </h4>



            <h4>  <span class=\"sr-only\" id=\"textoPorcentaje\"></span></h4>




            <table class=\"table table-striped table-bordered table-condensed\"  id=\"tablaCotizacionesFinal\">
                <thead>

                    <tr> 
                        <td width=\"10%\">Monto</td>
                        <td width=\"10%\">Plazo</td>
                        <td width=\"10%\">Banco</td>
                        <td width=\"10%\">Tipo</td>
                        <td width=\"20%\">Rechazar</td>
                        <td width=\"10%\">Adjuntar</td>
                        <td width=\"10%\">Eliminar</td>

                    </tr>


                </thead>


                <tbody>

                    <tr>
                    
                    </tr>
                    
                    </tbody>
                    
                    </table>
            
            

    <form id=\"GuardarCotizaciones\" action=\"";
        // line 405
        echo $this->env->getExtension('routing')->getPath("GuardarCotizaciones");
        echo "\"></form>
    <form id=\"UpdateCotizaciones\" action=\"";
        // line 406
        echo $this->env->getExtension('routing')->getPath("updateCotizaciones");
        echo "\"></form>
    <form id=\"EliminarCotizacionesCosumo\" action=\"";
        // line 407
        echo $this->env->getExtension('routing')->getPath("EliminarCotizacionesCosumo");
        echo "\"></form>
    <form id=\"UpdateCotizacionesArchivo\" action=\"";
        // line 408
        echo $this->env->getExtension('routing')->getPath("UpdateCotizacionesArchivo");
        echo "\"></form>


    


    <h3>Lista de cotizaciones subidas.</h3>
    
    <table class=\"table table-striped table-bordered table-condensed\" id=\"cotizacionesAnteriores\">
        
        <thead>
            <tr>
                     <td width=\"10%\">Tipo</td>
                    <td width=\"10%\">Banco</td>
                    <td width=\"10%\">Monto</td>
                    <td width=\"10%\">Plazo</td>
                
                    <td width=\"10%\">Archivo</td>
                    <td width=\"10%\"> Rechazo</td>
                    <td width=\"10%\">Eliminar</td>
            
                        
            </tr>
        
        </thead>
        <tbody>
           ";
        // line 434
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["cotizaciones"]);
        foreach ($context['_seq'] as $context["_key"] => $context["cotizaciones"]) {
            // line 435
            echo "            <tr>
                
                <td>";
            // line 437
            echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "tipo", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 438
            echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "banco", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 439
            echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "monto", array()), "html", null, true);
            echo "</td>
                   <td>";
            // line 440
            echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "plazo", array()), "html", null, true);
            echo "</td>
             
                   <td>";
            // line 442
            if ($this->getAttribute($context["cotizaciones"], "Archivourl", array())) {
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "Archivourl", array()), "html", null, true);
                echo "\" target=\"_blank\">Archivo</a>";
            }
            echo "</td>
                   <td>";
            // line 443
            echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "motivoRechazo", array()), "html", null, true);
            echo "</td>
                   <td>Eliminar</td>
                   
            </tr>
            
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cotizaciones'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 449
        echo "        
               
        </tbody>
    
    
    </table>

    
    
    
                     </section>

              
              

              
              
              








                  </div>
              </div>
            </div><!--/span-->




   </div>
      </div>  
 
    </div><!--/span-->
  </div><!--/row-->



<div style=\"clear:both;\"></div>










  <footer>
    <p>&copy; Bench Banking</p>
  </footer>
    <div id=\"box-config-modal\" class=\"modal hide fade in\" style=\"display: none;\">
      <div class=\"modal-header\">
        <button class=\"close\" data-dismiss=\"modal\">×</button>
        <h3></h3>
      </div>
      <div class=\"modal-body\">
        <p></p>
      </div>
      <div class=\"modal-footer\">
        <a href=\"#\" class=\"btn btn-primary\" data-dismiss=\"modal\"></a>
        <a href=\"#\" class=\"btn\" data-dismiss=\"modal\"></a>
      </div>
    </div>
</div><!--/.fluid-container-->

    
  
    <script src=\"";
        // line 521
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.core.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 522
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 523
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 524
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 525
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
    
  
    <script src=\"";
        // line 528
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 529
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js"), "html", null, true);
        echo "\"></script>

 
    <script src=\"";
        // line 532
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 533
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/bootbox/bootbox.min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 535
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>

\t\t
    <!-- jQuery Cookie -->    
    <script src=\"";
        // line 539
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.cookie/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Full Calender -->
    <script type='text/javascript' src='";
        // line 542
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
    
    <!-- CK Editor -->
\t<script type=\"text/javascript\" src=\"";
        // line 545
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Chosen multiselect -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 548
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script>  
    
    <!-- Uniform -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 551
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
    
    
\t
    <script src=\"";
        // line 555
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/simplenso/simplenso.js"), "html", null, true);
        echo "\"></script>
  </body>


                 
                         
                
  
  ";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:cotizar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  918 => 555,  911 => 551,  905 => 548,  899 => 545,  893 => 542,  887 => 539,  880 => 535,  875 => 533,  871 => 532,  865 => 529,  861 => 528,  855 => 525,  851 => 524,  847 => 523,  843 => 522,  839 => 521,  765 => 449,  753 => 443,  745 => 442,  740 => 440,  736 => 439,  732 => 438,  728 => 437,  724 => 435,  720 => 434,  691 => 408,  687 => 407,  683 => 406,  679 => 405,  627 => 355,  613 => 347,  608 => 345,  599 => 343,  593 => 339,  589 => 338,  585 => 336,  574 => 331,  570 => 330,  562 => 329,  554 => 328,  547 => 323,  543 => 322,  538 => 319,  525 => 312,  521 => 311,  513 => 310,  505 => 309,  500 => 306,  496 => 305,  491 => 302,  480 => 297,  476 => 296,  468 => 295,  460 => 294,  454 => 290,  450 => 289,  445 => 286,  431 => 278,  426 => 276,  418 => 275,  406 => 274,  397 => 268,  394 => 267,  390 => 266,  372 => 251,  368 => 250,  364 => 249,  360 => 248,  355 => 247,  353 => 246,  345 => 241,  336 => 235,  331 => 233,  327 => 232,  323 => 231,  319 => 230,  315 => 229,  311 => 228,  307 => 227,  303 => 226,  299 => 225,  295 => 224,  291 => 223,  268 => 203,  204 => 142,  200 => 141,  196 => 140,  192 => 139,  188 => 138,  182 => 135,  176 => 132,  170 => 129,  163 => 125,  153 => 118,  148 => 116,  139 => 110,  135 => 109,  131 => 108,  127 => 107,  19 => 1,);
    }
}
