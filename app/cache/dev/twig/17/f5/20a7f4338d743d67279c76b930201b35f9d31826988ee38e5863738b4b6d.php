<?php

/* BenchDebesBundle:Default:debesCreditoConsumo.html.twig */
class __TwigTemplate_17f520a7f4338d743d67279c76b930201b35f9d31826988ee38e5863738b4b6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<script src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchdebes/js/debescreditoconsumo.js"), "html", null, true);
        echo "\"></script>
\t\t<div class=\"fila\">
 
           <!--<h1 class=\"doc\"><span class=\"orange\">Crédito de Consumo</span></h1>-->
        \t<div class=\"col1\">

 <form  id=\"DebesCreditoConsumo\" action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("debesCreditoConsumoSiNo");
        echo "\" method=\"POST\" autocomplete=\"off\"  >
     

<input type=\"hidden\" id=\"token\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">
     
       <label class=\"label-text2\" for=\"rut\">¿Tienes algún  Crédito de Consumo?</label>
       <input type=\"hidden\" value=\"<?php echo \$idUser3;?>\" name=\"debesCreditoConsumo\" id=\"debesCreditoConsumo\">
 

       
              ";
        // line 18
        if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "debesCreditoConsumoSiNo", array()) == "SI")) {
            // line 19
            echo "
          
          <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"DebesCreditoConsumoRadio\" name=\"DebesCreditoConsumoRadio\" id=\"DebesCreditoConsumoRadio\"  checked >
         No<input type=\"radio\" value=\"NO\"   name=\"DebesCreditoConsumoRadio\" id=\"DebesCreditoConsumoRadio\" class=\"DebesCreditoConsumoRadio\"></p> 


         ";
        } else {
            // line 26
            echo "    
          <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"DebesCreditoConsumoRadio\" name=\"DebesCreditoConsumoRadio\" id=\"DebesCreditoConsumoRadio\" >
         No<input type=\"radio\" value=\"NO\"    name=\"DebesCreditoConsumoRadio\" id=\"DebesCreditoConsumoRadio\" class=\"DebesCreditoConsumoRadio\" checked ></p> 


          
     
    ";
        }
        // line 33
        echo "  



          
          
    
\t<div style=\"float:left; height:10px; width:100%;\"></div>
    <button style=\"float:left;\" class=\"classname\" value=\"Agregar\" id=\"guardaConsumo1\" type=\"submit\">GUARDAR Y SIGUIENTE</button> 



               </form>


             
              
              
               <form id=\"debes_credito_consumo\" method=\"post\" action=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("creditoConsumo");
        echo "\" class=\"form-personal\">
               <label class=\"label-text2\" for=\"institucion\">Institución</label>
               
               <input type=\"text\" class=\"input-box2 validate[required]\" autofocus name=\"InstitucionConsumo\"  id=\"InstitucionConsumo\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
         

               
             <label class=\"label-text2\" for=\"institucion\">Pago Mensual</label>
               <input type=\"text\"class=\"input-box2 validate[required]\" name=\"PagoMensualConsumo\" id=\"PagoMensualConsumo\"   placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  data-validation-engine=\"validate[required]\" data-required=\"true\" >
              
              <label class=\"label-text2\" for=\"rut\">Deuda Total</label>
              <input type=\"text\" class=\"input-box2 required\" name=\"deudaTotalConsumo\" id=\"deudaTotalConsumo\"  placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
              
              </div>
              </form>
              <div class=\"col1\">
                  <form id=\"debes_credito22\">
              <label class=\"label-text2\" for=\"rut\">Fecha Último Pago</label>
              <input type=\"text\" class=\"input-box2 validate[required]\" id=\"VencimientoFinalConsumo\" name=\"VencimientoFinalConsumo\"  data-validation-engine=\"validate[required]\" data-required=\"true\" >
              
              <label class=\"label-text2\" for=\"rut\">Deuda Vigente</label>
              <input type=\"text\" class=\"input-box2 validate[required]\"  name=\"DeudaVigenteConsumo\" id=\"DeudaVigenteConsumo\"  placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
              </form>
              
              
            </div>
            
            <div class=\"clearfix\"></div>
            

       \t  <div class=\"col2\">
          <button style=\"float:left;margin-top:10px;\" id=\"guardaConsumo\" class=\"classname\" value=\"Agregar\" type=\"submit\" onClick=\"\">AGREGAR</button>
        <div id=\"SiguienteDebesCreditoConsumo\" class=\"classname\" style=\"margin-top:10px; margin-left: 1%; position: relative;width: 80px; float: left;\" >SIGUIENTE</div>
        </div>

            
    
        <div class=\"flix-clear\"></div>

           <div class=\"clearfix\"></div>
      <br> 
            
            <div class=\"col2\">
          <table class=\"table table-striped\" id=\"respuesta4\">
\t\t\t    <thead style=\"background:#F60\">
          <tr>
\t\t\t\t\t<td><p style=\"text-align:center; color:#FFF;\">Institución</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#FFF;\">Pago Mensual </p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#FFF;\">Deuda Total</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#FFF;\">Fecha Último Pago</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#FFF;\">Deuda Vigente</p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Eliminar</p></td>
\t\t\t\t  </tr>
\t\t\t\t</thead>
\t    
     

      <tbody style=\"background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;\">

          

        ";
        // line 112
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["debesCreditoConsumo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["debesCreditoConsumo"]) {
            echo "  
            
      
          <tr  id=\"itemDebesConsumo_";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t
\t\t\t\t\t<td><p style=\"text-align:center;color:#666;\">";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "institucion", array()), "html", null, true);
            echo "</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#666;\">";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "pagomensual", array()), "html", null, true);
            echo "</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#666;\">";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudatotal", array()), "html", null, true);
            echo "</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#666;\">";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "fechaultimopago", array()), "html", null, true);
            echo "</p></td>
\t\t\t\t  <td><p style=\"text-align:center;color:#666;\">";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudavigente", array()), "html", null, true);
            echo "</p></td>
          <td><p style=\"text-align:center;color:#666;\"> <a href=\"#\" class=\"del_button\" id=\"del-";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "id", array()), "html", null, true);
            echo "\">eliminar</a> </p> </td>

          </tr>

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesCreditoConsumo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "    </tbody>


                      
\t\t\t 
             </table>
              
              
              
              
              
              
              
             
              
              
             
            
            </div>
            
            

\t
\t\t</div>
\t
        

        
            



</form>
                    
                    <form id=\"debescreditoconsumoForm\" action=\"";
        // line 161
        echo $this->env->getExtension('routing')->getPath("deldebesCreditoconsumo");
        echo "\"></form>";
    }

    public function getTemplateName()
    {
        return "BenchDebesBundle:Default:debesCreditoConsumo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 161,  196 => 127,  185 => 122,  181 => 121,  177 => 120,  173 => 119,  169 => 118,  165 => 117,  160 => 115,  152 => 112,  88 => 51,  68 => 33,  58 => 26,  49 => 19,  47 => 18,  37 => 11,  31 => 8,  22 => 2,  19 => 1,);
    }
}
