<?php

/* BenchAdminBundle:plantillas:bbva.html.twig */
class __TwigTemplate_e407fc899fad951f73aa5ed5fb8adf62fcb836815b6bc86ac2b0f22df63ce4cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Document</title>
</head>
<body>


<style>
    body{

        font-family: arial, sans-serif;
        font-size: 12px;
    }
    #contenedor{
        width: 700px;
        height: 1000px;
        /*  background-color: #ffeeee; */
        position: relative;
        margin: auto;


    }

    .logo{
        width: 200px;
        height: 70px;
        background-color: #ffffff;
        position: relative;
        float: left;



    }

    .titulo{

        width: 700px;
        background-color: #001d6e;
        height: 70px;
        position: relative;
        float: right;



    }


    h3{

        color: #ffffff;



    }

    .titulo2{
        margin-top: 49px;
        float: right;
        margin-right: 5px;
        width: auto;
        height: auto;
        position: relative;

    }
    .row880{
        position: relative;
        height: 24px;
        width: 890px;
        margin: auto;

        background-color: #ffffff;

    }

    .row880Azul{
        position: relative;
        height: 21px;
        width: 900px;
        margin: auto;

        background-color: #001d6e;

    }



    .clear{

        clear: both;
    }
    input[type=\"text\"] {
        padding: 10px;
        margin-top:-20px;
        border: none;
        text-align: center;
        border-bottom: solid 1px #000;
        transition: border 0.3s;
    }


    input[type=\"text\"]:focus,
    input[type=\"text\"].focus {
        border-bottom: solid 1px #000;
    }


    .titleInput1{
        position: relative;
        height: 100px;
        width: 100px;
        margin-top: 100px;

    }

    table{
        margin-top:10px;
    }

    .simuInput{
        width: 300px;
        height: 20px;
        border-bottom: 1px;
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        border-style: solid;
        position: relative;
        float: left;


    }

    .titulo33{
        position: relative;
        width: auto;
        height: auto;
        float: left;
        margin-right: 8px;
    }
        

    

</style>



<div id=\"contenedor\" style=\"background-color:#FFF;\">


<table width=\"695\">
    <tr>
        <td width=\"300\" height=\"96\"><img src=\"http://www.mediadiv.cl/imgPdf/bvva.jpg\"></td>
        <td width=\"383\" style=\"background-color:#001d6e\"><span style=\"color:#FFF;width:auto;height:auto;float:right;margin-right:4px;margin-top:60px;\">PREAPROBACION DE CREDITOS </span></td>

    </tr>

</table>
<table>

    <tr></tr>
</table>


<table width=\"700\">
    <tr>


        <td width=\"301\"><span class=\"titulo33\">Inmobiliarias:       </span> <div class=\"simuInput\" style=\"width:140px; \"> &nbsp  ";
        // line 171
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "proyecto", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "proyecto", array()), "html", null, true);
                echo " ";
            }
        }
        echo "   </div>  </td>
        <td width=\"185\"><span class=\"titulo33\"> Proyecto: </span> <div class=\"simuInput\" style=\"width:90px; \"> &nbsp ";
        // line 172
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "proyecto", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "proyecto", array()), "html", null, true);
                echo " ";
            }
        }
        echo "   </div></td>

        <td width=\"198\"> <span class=\"titulo33\">Ejecutivo:</span> <div class=\"simuInput\" style=\"width:110px; \"> &nbsp </div></td>


    </tr>


</table>



<table width=\"700\" height=\"10\">
    <tr>

        <td  style=\"background-color: #001d6e;\"><span style=\"color:#FFF;\">ANTECEDENTES DEL CLIENTE</span></td>

    </tr>


</table>




<table width=\"702\">
    <tr>
        <td width=\"489\"><span class=\"titulo33\">Nombre :</span> <div class=\"simuInput\" style=\"width:390px; \"> &nbsp; ";
        // line 199
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nombre", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nombre", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellido", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellido", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellidoseg", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

        <td width=\"201\"> <span class=\"titulo33\"> Rut:</span> <div class=\"simuInput\" style=\"width:141px; \">&nbsp; ";
        // line 201
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "rut", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "rut", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
    </tr>


</table>



<table width=\"700\">
    <tr>
        <td colspan=\"2\"><span class=\"titulo33\" style=\"width:200px;\"> Estado Civil:</span> <div class=\"simuInput\" style=\"width:280px;\">&nbsp; ";
        // line 211
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "estadocivil", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "estadocivil", array()), "html", null, true);
            echo " ";
        }
        echo "</div> </td>
        <td width=\"219\"> <span class=\"titulo33\"> Fecha Nacimiento:</span> <div class=\"simuInput\" style=\"width:130px; \">&nbsp; ";
        // line 212
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "fechaNacimiento", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "fechaNacimiento", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>

        <td width=\"130\"><span class=\"titulo33\">  Edad:</span> <div class=\"simuInput\" style=\"width:130px; \"> &nbsp ";
        // line 214
        echo twig_escape_filter($this->env, (isset($context["edadUsuario"]) ? $context["edadUsuario"] : $this->getContext($context, "edadUsuario")), "html", null, true);
        echo "  </div></td>
    </tr>


</table>


<table width=\"700\">

    <td><span class=\"titulo33\"> Domicilio: </span> <div class=\"simuInput\" style=\"width:602px; \"> ";
        // line 223
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "comuna", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "region", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "region", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
</table>


<table width=\"700\">
    <tr>
        <td width=\"236\"><span class=\"titulo33\"> Vivienda: </span> <div class=\"simuInput\" style=\"width:152px; \"> &nbsp; ";
        // line 229
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "tipocasa", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "tipocasa", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
        <td width=\"252\"><span class=\"titulo33\"> Años residencia : </span> <div class=\"simuInput\" style=\"width:150px; \"> &nbsp; </div></td>

        <td width=\"196\"><span class=\"titulo33\"> Familiares Nº:</span> <div class=\"simuInput\" style=\"width:78px;\"> &nbsp; ";
        // line 232
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "ndependientes", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "ndependientes", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
    </tr>


</table>


<table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\"> Arriendo / Dividendo: </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp; ";
        // line 241
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "montoArriendo", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "montoArriendo", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
        <td width=\"224\"><span class=\"titulo33\"> Teléfono : </span> <div class=\"simuInput\" style=\"width:120px;\"> &nbsp; ";
        // line 242
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "telefono", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "telefono", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>

     
    </tr>




</table>


<hr style=\"color: #001d6e;
background-color: #001d6e;
height: 5px;\">


<span style=\"color:#001d6e;\">DATOS PROFESIONALES DEL CLIENTE</span>


";
        // line 261
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            // line 262
            echo "<table width=\"700\">
    <tr>
        <td width=\"326\"><span class=\"titulo33\"> Actividad : </span> <div class=\"simuInput\" style=\"width:233px;\"> &nbsp ";
            // line 264
            if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "profesion", array()), "html", null, true);
                echo "  ";
            }
            echo "</div></td>
        <td width=\"362\"><span class=\"titulo33\">  Empresa: </span> <div class=\"simuInput\" style=\"width:271px;\"> &nbsp ";
            // line 265
            if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "industria", array()), "html", null, true);
                echo " ";
            }
            echo "</div></td>


    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"205\"><span class=\"titulo33\"> Giro Emp: </span><div class=\"simuInput\" style=\"width:110px;\"> &nbsp  </div></td>
        <td width=\"179\"><span class=\"titulo33\">Rut: </span><div class=\"simuInput\" style=\"width:133px;\">&nbsp  ";
            // line 278
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "rutempresa", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "rutempresa", array()), "html", null, true);
                echo "  ";
            }
            echo " </div></td>
        <td width=\"200\"><span class=\"titulo33\"> Teléfono Of.:  </span> <div class=\"simuInput\" style=\"width:130px;\">&nbsp ";
            // line 279
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "telefono", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "telefono", array()), "html", null, true);
                echo " ";
            }
            echo " </div></td>

    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"326\"><span class=\"titulo33\"> Profesión:</span> <div class=\"simuInput\" style=\"width:234px;\"> &nbsp ";
            // line 290
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "profesion", array()), "html", null, true);
            echo "  </div></td>
        
        <td width=\"171\"><span class=\"titulo33\"> Fax.: </span> <div class=\"simuInput\" style=\"width:110px;\">&nbsp  </div></td>

    </tr>

    <tr>
        
<td width=\"187\"><span class=\"titulo33\"> Contrato:  </span> <div class=\"simuInput\" style=\"width:202px;\"> ";
            // line 298
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "situacionlaboral", array())) {
                echo " &nbsp ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "situacionlaboral", array()), "html", null, true);
                echo "  ";
            }
            echo "    </div></td>

    </tr>


</table>



<table width=\"700\">
    <tr>

      
        


        <td width=\"233\"><span class=\"titulo33\"> Fecha Inicio / Act:</span> <div class=\"simuInput\" style=\"width:90px;\">&nbsp </div> </td>
        <td width=\"200\"><span class=\"titulo33\"> Email.: </span> <div class=\"simuInput\" style=\"width:130px;\">";
            // line 315
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "email", array()), "html", null, true);
            echo " </div></td>

    </tr>


<tr>
    
      <td width=\"205\"><span class=\"titulo33\"> Cargo Actual: </span><div class=\"simuInput\" style=\"width:210px;\">&nbsp ";
            // line 322
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "cargoactual", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "cargoactual", array()), "html", null, true);
                echo " ";
            }
        }
        echo "</div></td>


</tr>
</table>


<hr style=\"color: #001d6e;
background-color: #001d6e;
height: 5px;\">


<span style=\"color:#001d6e;\">DATOS PROFESIONALES DEL CLIENTE</span>
<div style=\"clear:both;\"></div>

<table width=\"700\" style=\"\">
    <tr>
        <td width=\"161\" style=\"border: solid 1px  #001d6e;\" ><span style=\"font-size:10px; text-align:center;width:auto;position:relative;margin:auto;\"> ACTIVOS </span></td>
        <td width=\"145\"  style=\"border: solid 1px  #001d6e;\" ><span style=\"font-size:10px; text-align:center;\"> PASIVOS </span></td>
        <td width=\"378\"  style=\"border: solid 1px  #001d6e;\"><span style=\"font-size:10px;;\"> INGRESOS LIQUIDOS MENSUAL </span></td>
    </tr>
</table>



<div class=\"clear:both;\"></div>

<table width=\"700\" height=\"20\">

    <tr>
        <td width=\"55\"><p style=\"font-size:9px;\"> Tipo </p></td>
        <td width=\"102\"><p style=\"font-size:9px;\"> Valor Comercial </p></td>

        <td width=\"61\"><p style=\"font-size:9px;\"> Tipo deuda </p></td>
        <td width=\"81\"><p style=\"font-size:9px;\"> Deuda vigente </p></td>
        <td width=\"365\"><p style=\"font-size:9px;\"> Pago Mensual</p></td>
        <td width=\"8\"></td>
    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Bienes Raices</span> <div class=\"simuInput\" style=\"width:100px;\">&nbsp; ";
        // line 367
        if ((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : $this->getContext($context, "TotalPropiedadesApi"))) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : $this->getContext($context, "TotalPropiedadesApi")), 0, ".", "."), "html", null, true);
                echo " ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo "</div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Hipotecario </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp; ";
        // line 368
        if ((isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : $this->getContext($context, "totalDebesHipotecario"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : $this->getContext($context, "totalDebesHipotecario")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : $this->getContext($context, "pagoMensualApi"))) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo "</div>  <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; ";
        if ((isset($context["totalDebesHipotecarioMensual"]) ? $context["totalDebesHipotecarioMensual"] : $this->getContext($context, "totalDebesHipotecarioMensual"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecarioMensual"]) ? $context["totalDebesHipotecarioMensual"] : $this->getContext($context, "totalDebesHipotecarioMensual")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : $this->getContext($context, "pagoMensualApi"))) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : $this->getContext($context, "pagoMensualApi")), 0, ".", "."), "html", null, true);
                echo "  ";
            } else {
                echo " NO  ";
            }
            echo " ";
        }
        echo "   </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Sueldo  </span> <div class=\"simuInput\" style=\"width:150px;\">&nbsp; ";
        // line 369
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "independientesueldoliquido", array()), "html", null, true);
            echo " ";
        }
        echo " </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Vehículos </span><div class=\"simuInput\" style=\"width:115px;\">&nbsp; ";
        // line 379
        if ((isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : $this->getContext($context, "TotalAutomovilesApi"))) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : $this->getContext($context, "TotalAutomovilesApi")), 0, ".", "."), "html", null, true);
                echo "  ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Consumo </span> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp; ";
        // line 380
        if ((isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : $this->getContext($context, "totalDebesCredito"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : $this->getContext($context, "totalDebesCredito")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : $this->getContext($context, "pagoMensualCreditoApi"))) {
                echo " SI ";
            } else {
                echo " NO  ";
            }
            echo " ";
        }
        echo "</div> <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; ";
        if ((isset($context["totalPagoMensual"]) ? $context["totalPagoMensual"] : $this->getContext($context, "totalPagoMensual"))) {
            echo " SI 
        ";
            // line 381
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalPagoMensual"]) ? $context["totalPagoMensual"] : $this->getContext($context, "totalPagoMensual")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : $this->getContext($context, "pagoMensualCreditoApi"))) {
                echo " ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : $this->getContext($context, "pagoMensualCreditoApi")), 0, ".", "."), "html", null, true);
                echo "  ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> </span> <div class=\"simuInput\" style=\"width:130px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Acciones </span> <div class=\"simuInput\" style=\"width:115px;\">&nbsp; ";
        // line 392
        if ((isset($context["totalAhorroInversionAcciones"]) ? $context["totalAhorroInversionAcciones"] : $this->getContext($context, "totalAhorroInversionAcciones"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionAcciones"]) ? $context["totalAhorroInversionAcciones"] : $this->getContext($context, "totalAhorroInversionAcciones")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        echo "</div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Tarjeta Crédito</span> <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp; ";
        // line 393
        if ((isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : $this->getContext($context, "totalDebesTarjeta"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : $this->getContext($context, "totalDebesTarjeta")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        echo " </div><div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp;  </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Boletas Hon. </span> <div class=\"simuInput\" style=\"width:140px;\">&nbsp;  </div></p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Ahorros </span><div class=\"simuInput\" style=\"width:118px;\">&nbsp; ";
        // line 405
        if ((isset($context["totalAhorroInversionCuentas"]) ? $context["totalAhorroInversionCuentas"] : $this->getContext($context, "totalAhorroInversionCuentas"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionCuentas"]) ? $context["totalAhorroInversionCuentas"] : $this->getContext($context, "totalAhorroInversionCuentas")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : $this->getContext($context, "totalAhorroInversionApi"))) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : $this->getContext($context, "totalAhorroInversionApi")), 0, ".", "."), "html", null, true);
                echo " ";
            } else {
                echo " NO  ";
            }
        }
        echo " </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Linea sobregiro </span><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp; ";
        // line 406
        if ((isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : $this->getContext($context, "totalDebesLinea"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : $this->getContext($context, "totalDebesLinea")), 0, ".", "."), "html", null, true);
            echo "  ";
        } else {
            echo "  ";
            if ((isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : $this->getContext($context, "pagoMensualLineaCreditoApi"))) {
                echo " SI ";
            } else {
                echo " NO   ";
            }
            echo " ";
        }
        echo "</div> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> 


";
        // line 409
        if ((isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : $this->getContext($context, "pagoMensualLineaCreditoApi"))) {
            echo " SI  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : $this->getContext($context, "pagoMensualLineaCreditoApi")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        // line 410
        echo "         &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">  Pension/Jubil.</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otros </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; ";
        // line 421
        if ((isset($context["otrosTienes"]) ? $context["otrosTienes"] : $this->getContext($context, "otrosTienes"))) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["otrosTienes"]) ? $context["otrosTienes"] : $this->getContext($context, "otrosTienes")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        echo "</div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otras Deudas <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div>  <div class=\"simuInput\" style=\"width:110px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Arriendos </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  </div> </p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total Act. </span><div class=\"simuInput\" style=\"width:105px;\">&nbsp;

         ";
        // line 436
        if ((isset($context["totalActivos"]) ? $context["totalActivos"] : $this->getContext($context, "totalActivos"))) {
            echo "   ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalActivos"]) ? $context["totalActivos"] : $this->getContext($context, "totalActivos")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo " 


         </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Total Pas. </span> <div class=\"simuInput\" style=\"width:220px;\">&nbsp; ";
        // line 440
        if ((isset($context["totalDebes"]) ? $context["totalDebes"] : $this->getContext($context, "totalDebes"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebes"]) ? $context["totalDebes"] : $this->getContext($context, "totalDebes")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "  </div>  </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total Ingresos</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp; ";
        // line 441
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "independientesueldoliquido", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "independientesueldoliquido", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo " </div> </p></td>

    </tr>


</table>






<table width=\"700\" height=\"10\">
    <tr>

        <td  style=\"background-color: #001d6e;\"><span style=\"color:#FFF;\">ANTECEDENTES DEL CONYUGE</span><input type=\"checkbox\"></td>







    </tr>


</table>




<table width=\"702\">
    <tr>
        <td width=\"489\"><span class=\"titulo33\">Nombre : </span> <div class=\"simuInput\" style=\"width:390px; \"> &nbsp    ";
        // line 474
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "nombre", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "apellido", array()), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "apellidoseg", array()), "html", null, true);
            echo "    ";
        }
        echo "   </div></td>

        <td width=\"201\"> <span class=\"titulo33\"> Rut:</span> <div class=\"simuInput\" style=\"width:141px; \"> &nbsp ";
        // line 476
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "rut", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"214\"><span class=\"titulo33\"> Estado Civil:</span> <div class=\"simuInput\" style=\"width:110px; \">&nbsp 

";
        // line 488
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " Casado ";
        }
        // line 489
        echo "        </div> </td>
        <td width=\"272\"> <span class=\"titulo33\"> Fecha Nacimiento:</span> <div class=\"simuInput\" style=\"width:110px; \">&nbsp ";
        // line 490
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "fechanacimiento", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

        <td width=\"198\"><span class=\"titulo33\">  Edad:</span> <div class=\"simuInput\" style=\"width:130px; \"> &nbsp  ";
        // line 492
        echo twig_escape_filter($this->env, (isset($context["edadCon"]) ? $context["edadCon"] : $this->getContext($context, "edadCon")), "html", null, true);
        echo "  </div></td>
    </tr>


</table>


<table width=\"700\">

    <td><span class=\"titulo33\"> Domicilio: </span> <div class=\"simuInput\" style=\"width:602px; \"> 
";
        // line 502
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo "  
";
            // line 503
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array()), "html", null, true);
                echo " ";
            }
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "comuna", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "comuna", array()), "html", null, true);
                echo " ";
            }
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "region", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "region", array()), "html", null, true);
                echo " ";
            }
        }
        // line 504
        echo "
    &nbsp </div></td>
</table>


<table width=\"700\">
    <tr>
        <td width=\"400\" colspan=\"2\"><span class=\"titulo33\"> Vivienda: </span> <div class=\"simuInput\" style=\"width:302px; \"> &nbsp";
        // line 511
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo "   ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "tipocasa", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "tipocasa", array()), "html", null, true);
                echo "  ";
            }
        }
        echo " </div></td>
        <td width=\"300\"><span class=\"titulo33\"> Años residencia : </span> <div class=\"simuInput\" style=\"width:150px; \"> &nbsp </div></td>

     
    </tr>


</table>


<table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\"> Arriendo / Dividendo: </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp ";
        // line 523
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "montoArriendo", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "montoArriendo", array()), "html", null, true);
                echo "  ";
            }
            echo "  ";
        }
        echo "  </div></td>
        <td width=\"224\"><span class=\"titulo33\"> Teléfono : </span> <div class=\"simuInput\" style=\"width:120px;\"> &nbsp  ";
        // line 524
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "telefono", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\">Profesión  </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp ";
        // line 535
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "profesion", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "profesion", array()), "html", null, true);
                echo "  ";
            }
            echo "  ";
        }
        echo "  </div></td>
        <td width=\"224\"><span class=\"titulo33\"> Universidad : </span> <div class=\"simuInput\" style=\"width:220px;\"> &nbsp  ";
        // line 536
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "universidad", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\">Sueldo  </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp ";
        // line 547
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "rentaliquidad", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "rentaliquidad", array()), "html", null, true);
                echo "  ";
            }
            echo "  ";
        }
        echo "  </div></td>
        <td width=\"224\"><span class=\"titulo33\"> Cargo : </span> <div class=\"simuInput\" style=\"width:200px;\"> &nbsp  ";
        // line 548
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "cargoactual", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

    </tr>



    <table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\">Empresa : </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp ";
        // line 556
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "empresa", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "empresa", array()), "html", null, true);
                echo "  ";
            }
            echo "  ";
        }
        echo "  </div></td>
        <td width=\"224\"><span class=\"titulo33\"> Antiguedad Laboral: </span> <div class=\"simuInput\" style=\"width:200px;\"> &nbsp  ";
        // line 557
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "antiguedadlaboral", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

    </tr>


</table>




<hr style=\"color: #001d6e;
background-color: #001d6e;
height: 5px;\">







<span style=\"color:#001d6e;\">DATOS PROFESIONALES DEL CLIENTE</span>
<div style=\"clear:both;\"></div>


<table width=\"700\" style=\"\">
    <tr>
        <td width=\"161\" style=\"border: solid 1px  #001d6e;\" ><span style=\"font-size:10px; text-align:center;width:auto;position:relative;margin:auto;\"> ACTIVOS </span></td>
        <td width=\"145\"  style=\"border: solid 1px  #001d6e;\" ><span style=\"font-size:10px; text-align:center;\"> PASIVOS </span></td>
        <td width=\"378\"  style=\"border: solid 1px  #001d6e;\"><span style=\"font-size:10px;;\"> INGRESOS LIQUIDOS MENSUAL </span></td>
    </tr>
</table>

<div class=\"clear:both;\"></div>

<table width=\"700\" height=\"20\">

    <tr>
        <td width=\"55\"><p style=\"font-size:9px;\"> Tipo </p></td>
        <td width=\"102\"><p style=\"font-size:9px;\"> Valor Comercial </p></td>

        <td width=\"61\"><p style=\"font-size:9px;\"> Tipo deuda </p></td>
        <td width=\"81\"><p style=\"font-size:9px;\"> Deuda vigente </p></td>
        <td width=\"365\"><p style=\"font-size:9px;\"> Deuda vigente </p></td>
        <td width=\"8\"></td>
    </tr>


</table>




<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Bienes Raices</span> <div class=\"simuInput\" style=\"width:100px;\">&nbsp; </div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Hipotecario </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp;</div>  <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Sueldo Fijo</span> <div class=\"simuInput\" style=\"width:150px;\">&nbsp; </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Vehículos </span><div class=\"simuInput\" style=\"width:115px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Consumo </span> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp; </div> <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; </div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Sueldo Variable</span> <div class=\"simuInput\" style=\"width:130px;\">&nbsp; </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Acciones </span> <div class=\"simuInput\" style=\"width:115px;\">&nbsp; </div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Tarjeta Crédito</span> <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div><div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp;  </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Boletas Hon. </span> <div class=\"simuInput\" style=\"width:140px;\">&nbsp;  </div></p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Ahorros </span><div class=\"simuInput\" style=\"width:118px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Linea sobregiro </span><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">  Pension/Jubil.</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otros </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otras Deudas <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div>  <div class=\"simuInput\" style=\"width:110px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Arriendos </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  </div> </p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total Act. </span><div class=\"simuInput\" style=\"width:105px;\">&nbsp;  </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Total Pas. </span> <div class=\"simuInput\" style=\"width:220px;\">&nbsp;  </div>  </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total Ingresos</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp;  </div> </p></td>

    </tr>


</table>




<br>
<br>
<br>
<br>
<br>
<br>

<br>
<br>

<br>
<br>
<br>
<br>

<br>
<br>
<br>
<br>


<hr style=\"color: #001d6e;
background-color: #001d6e;
height: 5px;\">



<span style=\"color:#001d6e;margin-top: 800px; position: relative;width: auto;height: auto;\">
DATOS CRÉDITO HIPOTECARIO
</span>




<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Valor Propiedad</span> <div class=\"simuInput\" style=\"width:100px;\">&nbsp; ";
        // line 721
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "valorPropiedad", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "valorPropiedad", array()), "html", null, true);
                echo " UF ";
            }
        }
        echo "</div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Reserva </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp;</div>  <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Estado </span> <div class=\"simuInput\" style=\"width:150px;\">&nbsp; ";
        // line 723
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "estado", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "estado", array()), "html", null, true);
                echo " &nbsp; ";
                if (($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "dfl2", array()) == "si")) {
                    echo "  DFL2  ";
                }
                echo " ";
            }
        }
        echo "</div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Estacionamiento </span><div class=\"simuInput\" style=\"width:70px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Pie  </span> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp; ";
        // line 734
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "montoPie", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "montoPie", array()), "html", null, true);
                echo " UF ";
            }
            echo " ";
        }
        echo "</div> <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; </div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> MONTO CRÉDITO </span> <div class=\"simuInput\" style=\"width:130px;\">&nbsp;";
        // line 735
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "montoCredito", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "montoCredito", array()), "html", null, true);
                echo " ";
            }
        }
        echo " </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Bodega</span> <div class=\"simuInput\" style=\"width:115px;\">&nbsp; </div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Ahorro </span> <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div><div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp;  </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">PLAZO (años) \t </span> <div class=\"simuInput\" style=\"width:140px;\">&nbsp;";
        // line 747
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "plazoCredito", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "plazoCredito", array()), "html", null, true);
                echo "  ";
            }
            echo " ";
        }
        echo " </div></p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Descuento </span><div class=\"simuInput\" style=\"width:118px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Subsidio </span><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">  % Financiamiento\t</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otras Deudas <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div>  <div class=\"simuInput\" style=\"width:110px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Fecha de Entrega\t  </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<br/><br/><br/>
<span style=\"color:#001d6e;margin-top: 800px; position: relative;width: auto;height: auto;\">
DATOS CRÉDITO CONSUMO
</span>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Monto de Crédito: </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp;  ";
        // line 789
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "montoCredito", array()), 0, ".", "."), "html", null, true);
            echo "  ";
        }
        echo "</div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Plazo de Crédito <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp; ";
        // line 790
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "plazoCredito", array()), "html", null, true);
            echo "  ";
        }
        echo " </div>   </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Seguros Asociados  </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  ";
        // line 791
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "segurosAsociados", array()), "html", null, true);
            echo "  ";
        }
        echo " </div> </p></td>

    </tr>


    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Monto de Crédito: </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; ";
        // line 797
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "paraque", array()), "html", null, true);
            echo "  ";
        }
        echo " </div> </p></td>


    </tr>


</table>


<br/><br/><br/>


<span style=\"color:#001d6e;margin-top: 800px; position: relative;width: auto;height: auto;\">
DATOS CRÉDITO AUTOMOTRIZ
</span>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Monto del Auto </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp;  ";
        // line 817
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "montoCredito", array()), "html", null, true);
            echo "  ";
        }
        echo "</div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Plazo de Crédito <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp; ";
        // line 818
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "plazoCredito", array()), "html", null, true);
            echo "  ";
        }
        echo " </div>   </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Seguros Asociados  </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  ";
        // line 819
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "segurosAsociados", array()), "html", null, true);
            echo "  ";
        }
        echo " </div> </p></td>

    </tr>


    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Monto de Crédito: </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; ";
        // line 825
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "paraque", array()), "html", null, true);
            echo "  ";
        }
        echo " </div> </p></td>


    </tr>


</table>


<br>
<br>
<br>
<br>
<br>
<br>
<br>


<table>
<thead>


<span style=\"color:#001d6e;margin-top: 800px; position: relative;width: auto;height: auto;\">
DATOS CRÉDITO CONSOLIDACION 
</span>
<tr>
    <th>Crédito </th>
    <th>Monto </th>
    <th>Plazo </th>

  
    </tr>



    </thead>

    <tr>
    ";
        // line 863
        $context["con"] = 0;
        // line 864
        echo "    ";
        if ((isset($context["creditoConsolidacion"]) ? $context["creditoConsolidacion"] : $this->getContext($context, "creditoConsolidacion"))) {
            // line 865
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["creditoConsolidacion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["creditoConsolidacion"]) {
                // line 866
                echo "    ";
                $context["con"] = ((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")) + 1);
                // line 867
                echo "
    ";
                // line 868
                if (((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")) <= 1)) {
                    // line 869
                    echo "    
    <tr>   

        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 872
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 873
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 874
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo", array()), "html", null, true);
                    echo " años </div> </td>
     </tr>
        
       <tr>    

        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 879
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 880
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 881
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo2", array()), "html", null, true);
                    echo " años </div> </td>
</tr>
<tr>   


        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 886
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito3", array()), "html", null, true);
                    echo "  </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 887
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito3", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 888
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo3", array()), "html", null, true);
                    echo "  </div></td>
       
   </tr> 


   ";
                }
                // line 894
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsolidacion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 896
            echo "    ";
        }
        // line 897
        echo "    
        </tr>

</table></div>









</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:plantillas:bbva.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1563 => 897,  1560 => 896,  1553 => 894,  1544 => 888,  1540 => 887,  1536 => 886,  1528 => 881,  1524 => 880,  1520 => 879,  1512 => 874,  1508 => 873,  1504 => 872,  1499 => 869,  1497 => 868,  1494 => 867,  1491 => 866,  1486 => 865,  1483 => 864,  1481 => 863,  1436 => 825,  1423 => 819,  1415 => 818,  1407 => 817,  1380 => 797,  1367 => 791,  1359 => 790,  1351 => 789,  1299 => 747,  1277 => 735,  1267 => 734,  1243 => 723,  1231 => 721,  1060 => 557,  1048 => 556,  1033 => 548,  1021 => 547,  1003 => 536,  991 => 535,  973 => 524,  961 => 523,  939 => 511,  930 => 504,  911 => 503,  907 => 502,  894 => 492,  885 => 490,  882 => 489,  878 => 488,  859 => 476,  846 => 474,  803 => 441,  795 => 440,  784 => 436,  760 => 421,  747 => 410,  739 => 409,  721 => 406,  704 => 405,  683 => 393,  673 => 392,  647 => 381,  629 => 380,  611 => 379,  594 => 369,  562 => 368,  544 => 367,  491 => 322,  481 => 315,  457 => 298,  446 => 290,  428 => 279,  420 => 278,  400 => 265,  392 => 264,  388 => 262,  386 => 261,  361 => 242,  353 => 241,  337 => 232,  327 => 229,  302 => 223,  290 => 214,  282 => 212,  275 => 211,  258 => 201,  238 => 199,  201 => 172,  191 => 171,  19 => 1,);
    }
}
