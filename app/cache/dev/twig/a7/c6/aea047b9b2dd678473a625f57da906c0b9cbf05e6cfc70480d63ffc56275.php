<?php

/* BenchAdminBundle:Default:index.html.twig */
class __TwigTemplate_a7c6aea047b9b2dd678473a625f57da906c0b9cbf05e6cfc70480d63ffc56275 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
<head>
<meta charset=\"utf-8\" />
<title>Administrador Bench Banking </title>
<meta name=\"robots\" content=\"noindex\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
<meta name=\"description\" content=\"\" />
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, FOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"INDEX, NOFOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, NOFOLLOW\">

<meta name=\"author\" content=\"\" />

<!--- ship -->

<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/login.js"), "html", null, true);
        echo "\"></script>





<!--- ship -->
<link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"main-theme-script\" />
<link href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-specific-script\" />
<link href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/css/bootstrap-responsive.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />


<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/fullcalendar/fullcalendar/fullcalendar.css"), "html", null, true);
        echo "\" />

<link href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/css/datepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />





<link rel=\"stylesheet\" type=\"text/css\" media=\"screen,projection\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />


<link type=\"text/css\" href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.intenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />


<link href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/simplenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />


<link rel=\"shortcut icon\" href=\"images/ico/favicon.ico\" />

<link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-144-precomposed.png"), "html", null, true);
        echo "\" />
<link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-114-precomposed.png"), "html", null, true);
        echo "\" />
<link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-72-precomposed.png"), "html", null, true);
        echo "\" />
<link rel=\"apple-touch-icon-precomposed\" href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-57-precomposed.png"), "html", null, true);
        echo "\" />
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>
<body id=\"dashboard\" class=\"hidden\">

<div class=\"container-fluid\">
  <div class=\"row-fluid\">
    <div class=\"span4\">&nbsp;</div>
    <div class=\"span4\">
        <div class=\"page-header\">
            <h1>Login Bench Banking</h1>
        </div>
      <div class=\"box\" style=\"margin-bottom:500px;\">
        <h4 class=\"box-header round-top\"> </h4>
        <div class=\"box-container-toggle\">
          <div class=\"box-content\">
            


            <form class=\"well form-search\" action=\"";
        // line 70
        echo $this->env->getExtension('routing')->getPath("ingresoAdmin");
        echo "\" method=\"post\"  id=\"login\"/>
              <input type=\"text\" class=\"input-small\" placeholder=\"Usuario\" name=\"nombre\" id=\"nombre\"  />
              <input type=\"password\" class=\"input-small\" placeholder=\"Password\" name=\"contrasena\"  id=\"contrasena\"/>
              <button  id=\"butingreso\" class=\"btn\">Ingresar </button>
              <label class=\"inline uniform\" for=\"remember\" style=\"vertical-align: bottom;\">
              

     
            </form>

            <section id=\"respuesta\">

               

            </section>


          </div>
        </div>
      </div>
  
    <div class=\"span4\">&nbsp;</div>
  </div>

</div>


<footer>
  <p class=\"pull-right\">&copy; </p>
</footer>
</div>




<script src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/DataTables/media/js/jquery.dataTables.js"), "html", null, true);
        echo "\"></script> 


<script src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.core.min.js"), "html", null, true);
        echo "\"></script> 
<script src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script> 
<script src=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js"), "html", null, true);
        echo "\"></script> 
<script src=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script> 


<script src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js"), "html", null, true);
        echo "\"></script> 
<script src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js"), "html", null, true);
        echo "\"></script> 


<script src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script> 
<script src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/bootbox/bootbox.min.js"), "html", null, true);
        echo "\"></script> 


<script src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script> 


<script src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.cookie/jquery.cookie.js"), "html", null, true);
        echo "\"></script> 


<script type='text/javascript' src='";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script> 

<script type=\"text/javascript\" src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>

<!-- Chosen multiselect --> 
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script> 

<!-- Uniform --> 
<script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script> 


 

<script src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/simplenso/simplenso.js"), "html", null, true);
        echo "\"></script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  255 => 142,  247 => 137,  241 => 134,  235 => 131,  230 => 129,  224 => 126,  218 => 123,  212 => 120,  208 => 119,  202 => 116,  198 => 115,  192 => 112,  188 => 111,  184 => 110,  180 => 109,  176 => 108,  170 => 105,  132 => 70,  111 => 52,  107 => 51,  103 => 50,  99 => 49,  91 => 44,  85 => 41,  79 => 38,  70 => 32,  65 => 30,  59 => 27,  55 => 26,  51 => 25,  41 => 18,  37 => 17,  19 => 1,);
    }
}
