<?php

/* PsPdfBundle:Example:usingFacadeDirectly.pdf.twig */
class __TwigTemplate_9f51d8bb7a00cbe42cfd35a8d1308bf94f7c2fb4b5149c2e2149e3dbd8e04f0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE pdf SYSTEM \"%resources%/dtd/doctype.dtd\">
<pdf>
    <dynamic-page>
\t\tHello!
    </dynamic-page>
</pdf>";
    }

    public function getTemplateName()
    {
        return "PsPdfBundle:Example:usingFacadeDirectly.pdf.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
