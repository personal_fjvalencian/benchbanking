<?php

/* BenchAdminBundle:plantillas:bbva2.html.twig */
class __TwigTemplate_ffb65fe48df3c8f52cd6ce503d3269fd754e46620ff86a4b3b92548df2f4a04d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
    <head>
        <meta charset=\"UTF-8\">
        <title>Plantilla BBVA</title>
        <style>

         body{

        font-family: arial, sans-serif;
        font-size: 12px;
    }

    
            #contenido{
                width: 710px;
            }
            
            .tabla{
                width: 710px;
                background: #efefef;
            }
            
            .simuInput{
                width: 100%;
                height: 20px;
                border-bottom: 1px;
                border-left: 0px;
                border-right: 0px;
                border-top: 0px;
                border-style: solid;
                position: relative;
                float: left;
            }
            
            table {
  border-collapse: separate;
  border-spacing:  10px 15px;
}

  table{
        margin-left:10px;
        position:relative;
        width:auto;
        height:auto;

    }  




        </style>
    </head>
    <body>




        <div id=\"contenido\">


        <table width=\"695\">
    <tr>
        <td width=\"300\" height=\"96\"><img src=\"http://www.mediadiv.cl/imgPdf/bvva.jpg\"></td>
        <td width=\"383\" style=\"background-color:#001d6e\"><span style=\"color:#FFF;width:auto;height:auto;float:right;margin-right:4px;margin-top:60px;\"> </span></td>

    </tr>

</table>



            <!-- Titulo cabecera -->
            <table class=\"tabla\">
                <tbody>
                    <tr>
                        <td>Estado de situación:</td>
                        <td>PERSONA NATURAL</td>
                        <td><div class=\"simuInput\"></div></td>
                        <td><img src=\"\"></td>
                    </tr>
                </tbody>
            </table>
            <!-- Titulo cabecera -->
            <br>
            <br>
            
            <!-- Antecedentes Personales -->
            <table class=\"tabla\" >
                <thead>
                <th colspan=\"6\" align=\"left\">
                    ANTECEDENTES PERSONALES:
                </th>
                </thead>
                <tbody>
                    <tr>
                        <td colspan=\"1\">Rut</td>
                        <td colspan=\"3\" >Nombres Apellidos</td>
                      
                    
                    </tr>
                    <tr>
                        <td colspan=\"1\" ><div class=\"simuInput\">&nbsp; ";
        // line 103
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "rut", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "rut", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"3\"><div class=\"simuInput\">&nbsp;

";
        // line 106
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nombre", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nombre", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellido", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellido", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellidoseg", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        // line 107
        echo "                        </div></td>
                      
                    </tr>


                    
                    <tr>
                  
                        <td>Fecha de nacimiento </td>
                          <td>Sexo</td>
                        
                    
                    </tr>
                    
                    <tr>
                    
            
                    <td><div class=\"simuInput\">&nbsp; ";
        // line 124
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "fechaNacimiento", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "fechaNacimiento", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    <td><div class=\"simuInput\">&nbsp;";
        // line 125
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "sexo", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "sexo", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                    
                    </tr>
                    
                    <tr>
                      
                        <td>Nacionalidad</td>
                        <td colspan=\"2\">Estado civil</td>
                        <td>Dependientes</td>
                    
                    </tr>
                    <tr>
                      
                        <td><div class=\"simuInput\">&nbsp; ";
        // line 138
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nacionalidad", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nacionalidad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"2\" ><div class=\"simuInput\">&nbsp;";
        // line 139
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "estadocivil", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "estadocivil", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 140
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "ndependientes", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "ndependientes", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                 
                    </tr>
                    
                        
                    <tr>
                     
                         <td>Personas grupo familiar</td>
                        <td>Nivel educacional</td>



                    </tr>

                    <tr>

                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp; ";
        // line 157
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "niveleducacion", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "niveleducacion", array()), "html", null, true);
            echo "   ";
        }
        echo "</div></td>

                    </tr>



                      <tr>


                       <tr>
                     
                         <td>Sueldo Liquido</td>
                



                    </tr>

                    <tr>

                        <td><div class=\"simuInput\">&nbsp; ";
        // line 177
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "independientesueldoliquido", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                  

                    </tr>




 <tr>


                       <tr>
                     
                         <td>Telefono</td>
                         <td>Email</td>



                    </tr>

                    <tr>

                      <td><div class=\"simuInput\">&nbsp; ";
        // line 199
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "telefono", array()), "html", null, true);
            echo "  ";
        }
        echo " </div></td>
                        <td><div class=\"simuInput\">&nbsp; ";
        // line 200
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "email", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                  

                    </tr>





                      
                    <td>&nbsp;</td>
                    </tr>
                    <tr>
                    
                    
                    <tr>
                    <td>
                    DIRECCIÓN PARTICULAR:
                    </td>
                    
                    
                       
                      <tr>
                    <td>&nbsp;</td>
                    </tr>
               
                    
                    
                    
                  
                       <tr>
                        <td colspan=\"2\">Avenida/Calle</td>
                        <td>N°</td>
                        <td>Depto</td>
                      
                       </tr>
                    
                    <tr>
                        <td colspan=\"2\" ><div class=\"simuInput\">&nbsp; ";
        // line 238
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;  </div></td>
                        <td><div class=\"simuInput\">&nbsp; </div></td>
                 
              
                    </tr>
                    
                    
                    
                     
                       <tr>

                     
                        <td colspan=\"1\">Comuna</td>
                        <td colspan=\"3\">Region </td>
                      
                       </tr>
                    
                    <tr>
                  
                        <td colspan=\"1\"><div class=\"simuInput\">&nbsp;";
        // line 258
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "comuna", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                        <td colspan=\"3\"><div class=\"simuInput\">&nbsp;
                        ";
        // line 260
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "region", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "region", array()), "html", null, true);
            echo " ";
        }
        // line 261
        echo "
                            
                        </div></td>
              
                    </tr>
                    
                    
                    
                      <tr>
                    <td>
                  DIRECCIÓN COMERCIAL
:
                    </td>
                    
                    
                       
                      <tr>
                    <td>&nbsp;</td>
                    </tr>
               
                    
                    
                    
                  
                    
                    
                  
                       <tr>
                        <td colspan=\"2\">Avenida/Calle</td>
                        <td>N°</td>
                        <td>Depto</td>
                      
                       </tr>
                    
                    <tr>
                        <td colspan=\"2\" ><div class=\"simuInput\">&nbsp; ";
        // line 296
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;  </div></td>
                        <td><div class=\"simuInput\">&nbsp; </div></td>
                 
              
                    </tr>
                    
                    
                    
                     
                       <tr>

                     
                        <td colspan=\"1\">Comuna</td>
                        <td colspan=\"3\">Region </td>
                      
                       </tr>
                    
                    <tr>
                  
                        <td colspan=\"1\"><div class=\"simuInput\">&nbsp;";
        // line 316
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                        <td colspan=\"3\"><div class=\"simuInput\">&nbsp;
                        ";
        // line 318
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "region", array()), "html", null, true);
            echo " ";
        }
        // line 319
        echo "
                            
                        </div></td>
              
                    </tr>
                    
                    
                    
                    
                    
                  
                    
                  
                    
                    
              
              </tbody>
            </table>
            <!-- Antecedentes Personales -->
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

            <!-- Antecedentes Laborlales -->
            <table class=\"tabla\" >
                <thead>
                <th colspan=\"4\" align=\"left\">
                    ANTECEDENTES LABORALES
                </th>
                </thead>
                <tbody>
                    <tr>
                        <td>Situación laboral</td>
                        <td>Profesión</td>
                        <td>Actividad o Cargo</td>
                    </tr>


                    <tr>
                        <td><div class=\"simuInput\">&nbsp; ";
        // line 371
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "situacionlaboral", array()), "html", null, true);
            echo "   ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 372
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "profesion", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 373
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "cargoactual", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    </tr>




           


                    <tr>
                        <td>Nombre completo empleador</td>
                        <td>Rut Empleador</td>
                        <td>Giro Empressa</td>
                        <td>Fecha ingreso empleador</td>
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 389
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "empleadorempresa", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 390
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "rutempresa", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 391
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 392
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "fechaingreso", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                    </tr>
                    <tr>
                        <td>Nombre empleador anterior</td>
                        <td>Antgüedad laboral </td>
                        <td>Inicio actividad independiente</td>
                        <td>Otra actividad</td>
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 401
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 402
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["tiempoTrabajo"]) ? $context["tiempoTrabajo"] : $this->getContext($context, "tiempoTrabajo")), "html", null, true);
            echo "   ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 403
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;";
        // line 404
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
        }
        echo "</div></td>
                    </tr>


                    <tr>
                        <td>Sueldo</td>

                    </tr>

                    <tr>
                       <td colspan=\"3\">

                       <div class=\"simuInput\">";
        // line 416
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "independientesueldoliquido", array()), "html", null, true);
            echo " ";
        }
        echo "   </div> </td> 

                    </tr>
                </tbody>
            </table>
            <!-- Antecedentes Laborlales -->
            <br>
            <br>
            <br>
            <br>
            <!-- Antecedentes Conyuge -->
            <table class=\"tabla\" >
                <thead>
                <th colspan=\"7\" align=\"left\">Antencedentes del (de la) Conyuge:</th>
                </thead>
                <tbody>
                    <tr>
                        <td>Rut</td>
                        <td colspan=\"2\" >Nombres </td>
                        <td colspan=\"3\">Apellido Materno</td>
                       
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp; ";
        // line 439
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "rut", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp;";
        // line 440
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "nombre", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"3\"><div class=\"simuInput\">&nbsp;";
        // line 441
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "apellido", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                      
                    </tr>
                    
                    <tr>
                    
                     <td colspan=\"2\">Apellido Paterno</td>
                        <td>Fecha de nacimientos</td>
                        
                    </tr>
                    
                    <td colspan=\"2\"><div class=\"simuInput\">&nbsp; ";
        // line 452
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                     <td><div class=\"simuInput\">&nbsp; ";
        // line 453
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "fechanacimiento", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    
                    <tr>
                    
                    
                    </tr>
                    
                    
                    <tr>
                        <td>Sexo</td>
                        <td colspan=\"2\">Nacionalidad</td>
                        <td>Dependientes</td>
                      
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp; ";
        // line 468
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "sexo", array())) {
            echo " ";
            if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "sexo", array()) == "Hombre")) {
                echo " Mujer  ";
            } else {
                echo " Hombre ";
            }
        }
        echo " </div></td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp; ";
        // line 469
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "nacionalidad", array()), "html", null, true);
        }
        echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        
                    </tr>
                    
                    <tr>
                      <td colspan=\"2\">Nivel educacional</td>
                    </tr>
                    <tr>
                      <td colspan=\"2\"><div class=\"simuInput\">&nbsp; ";
        // line 478
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "niveleducacion", array()), "html", null, true);
            echo " ";
        }
        echo "</div> </td>
                    </tr>
                    <tr>
                        <td colspan=\"7\">
                              ANTECEDENTES LABORALES:
                        </td>
                    </tr>
                    <tr>
                        <td>Profesión</td>
                        <td colspan=\"2\">Actividad o cargo</td>
                        <td colspan=\"3\">Nombre completo empleador</td>
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp; ";
        // line 491
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "profesion", array()), "html", null, true);
            echo "  ";
        }
        echo " </div></td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp; ";
        // line 492
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "actividad", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                        <td colspan=\"3\"><div class=\"simuInput\">&nbsp;";
        // line 493
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "empresa", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                    </tr>
                    <tr>
                        <td>Fecha ingreso empleador:</td>
                        <td>Direccion Comercial:</td>
                        <td>Oficina</td>
                   
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;  </div></td>
                        <td><div class=\"simuInput\">&nbsp; </div></td>
                        <td><div class=\"simuInput\">&nbsp; </div></td>
            
                    </tr>
                    
                    <tr>
                        <td colspan=\"2\">Cargo Actual:</td>
                        <td colspan=\"2\">Antiguedad Laboral:</td>
                     
                    </tr>
                    
                    <tr>
                
                    <td colspan=\"2\" ><div class=\"simuInput\">&nbsp;";
        // line 516
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "cargoactual", array()), "html", null, true);
            echo "  ";
        }
        echo " </div></td>
                    <td colspan=\"2\"><div class=\"simuInput\">&nbsp; ";
        // line 517
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "antiguedadlaboral", array()), "html", null, true);
            echo "  ";
        }
        echo " </div></td>
                 
            
                       </tr>
                       
                       
                      
                 
                    <tr>
                        <td>Sueldo liquido mensual</td>
                      
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp; ";
        // line 530
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "rentaliquidad", array()), "html", null, true);
            echo "  ";
        }
        echo " </div></td>
          
                    </tr>
                </tbody>
            </table>
            <!-- Antecedentes Conyuge -->
            <br>
            <br>
            <br>
            <br>
            <br>
            <!-- Bienes Activos -->
            <table class=\"tabla\" >
                <thead>
                <th colspan=\"4\" align=\"left\"> ACTIVOS </th>
                </thead>
                <tbody>
                    <tr>
                        <td colspan=\"4\" align=\"4\" >DEPOSITOS A PLAZO, AHORRO, FONDOS MUTUOS o INVERSIONES / ";
        // line 548
        if ((isset($context["tienesAhorroInversion"]) ? $context["tienesAhorroInversion"] : $this->getContext($context, "tienesAhorroInversion"))) {
            echo " SI ";
        } else {
            echo "  ";
            if ((isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : $this->getContext($context, "totalAhorroInversionApi"))) {
                echo "SI ";
            } else {
                echo " NO  ";
            }
        }
        echo "</td>
                    </tr>
                    <tr>
                        <td>Tipo</td>
                        <td>Institución</td>
               
                        <td>Valor</td>
                        <td>Prendado</td>
                    </tr>

                    ";
        // line 558
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesAhorroInversion"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesAhorroInversion"]) {
            // line 559
            echo "                    <tr>
                        <td><div class=\"simuInput\">&nbsp; ";
            // line 560
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "tipo", array()), "html", null, true);
            echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp; ";
            // line 561
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "institucion", array()), "html", null, true);
            echo "</div></td>
                         <td><div class=\"simuInput\">&nbsp; ";
            // line 562
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "valorcomercial", array()), "html", null, true);
            echo "</div></td>
             
                        <td><div class=\"simuInput\">&nbsp; ";
            // line 564
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "prendado", array()), "html", null, true);
            echo " </div></td>
                    </tr>
                   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAhorroInversion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 567
        echo "
                </tbody>
                
                <tr>
                    <td><div class=\"simInput\">&nbsp; ";
        // line 571
        if ((isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : $this->getContext($context, "totalAhorroInversion"))) {
            echo "   Total  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : $this->getContext($context, "totalAhorroInversion")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : $this->getContext($context, "totalAhorroInversionApi")), 0, ".", "."), "html", null, true);
            echo "  ";
        }
        echo "</div> </td>
                </tr>


            </table>

          
            <br>
            <br>
            <br>
            <br>
            <br>
            <!-- Vehiculos -->
            <table class=\"tabla\" >
                <thead> 
                <th align=\"left\" colspan=\"7\">Vehículo    / ";
        // line 586
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : $this->getContext($context, "tienesAutomoviles"))) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : $this->getContext($context, "TotalAutomovilesApi"))) {
                echo " SI  ";
            } else {
                echo "  NO ";
            }
            echo " ";
        }
        echo "</th>
                </thead>
                <tbody>
                    <tr>
                        <td>Marca</td>
                        <td>Tipo</td>
                        <td>Modelo</td>
                        <td>Año</td>
                        <td>Valor comercial</td>
                        <td>Prenda</td>
            
                    </tr>

                    ";
        // line 599
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : $this->getContext($context, "tienesAutomoviles"))) {
            // line 600
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesAutomoviles"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesAutomoviles"]) {
                // line 601
                echo "                    <tr>
                        <td><div class=\"simuInput\">&nbsp; ";
                // line 602
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "marcaauto", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp; ";
                // line 603
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "tipo", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp; ";
                // line 604
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "modelo", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp; ";
                // line 605
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "anoauto", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp; ";
                // line 606
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "valorcomercial", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"simuInput\">&nbsp; ";
                // line 607
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "prendado", array()), "html", null, true);
                echo "</div></td>
                       
                    </tr>

                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAutomoviles'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 612
            echo "                    ";
        }
        // line 613
        echo "                  
                    <tr>
                        <td colspan=\"3\" align=\"right\">TOTAL VALOR COMERCIAL VEHICULOS</td>
                        <td colspan=\"4\"><div class=\"simuInput\">&nbsp; ";
        // line 616
        if ((isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : $this->getContext($context, "TotalAutomovilesApi"))) {
                echo " ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : $this->getContext($context, "TotalAutomovilesApi")), 0, ".", "."), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo "</div></td>


                    </tr>
                </tbody>
            </table>
            <!-- Vehiculos -->
            <br>
            <br>
            <br>
            <!-- Propiedades -->
            <table class=\"tabla\" >
                <thead>
                <th align=\"left\" colspan=\"5\" >
                    PROPIEDADES /  ";
        // line 630
        if ((isset($context["tienesHiporecario"]) ? $context["tienesHiporecario"] : $this->getContext($context, "tienesHiporecario"))) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : $this->getContext($context, "TotalPropiedadesApi"))) {
                echo "  SI  ";
            } else {
                echo " NO ";
            }
        }
        // line 631
        echo "                </th>
                </thead>
                <tbody>
                  
                    <tr>
                        <td>Tipo</td>
                        <td>Direccion</td>
                        <td>Region</td>

                        <td>Avalúo Fiscal </td>
                        <td>Rol </td>
                        <td>Hipotecado </td>
                    </tr>



                    ";
        // line 647
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesHiporecario"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesHiporecario"]) {
            // line 648
            echo "

                        
                        <tr>
                        <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp; ";
            // line 652
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "tipo", array()), "html", null, true);
            echo "</div></td>
                        <td><div class=\"simuInput\" style=\"width:140px;font-size:10px;\" >&nbsp; ";
            // line 653
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "direccion", array()), "html", null, true);
            echo "</div></td>
                        <td><div class=\"simuInput\" style=\"width:140px;font-size:10px;\">&nbsp; ";
            // line 654
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "region", array()), "html", null, true);
            echo "</div></td>

                        <td><div class=\"simuInput\" style=\"width:90px;\"> &nbsp; ";
            // line 656
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "avaluo", array()), "html", null, true);
            echo "</div></td>
                        <td><div class=\"simuInput\" style=\"width:90px;font-size:10px;\"> &nbsp; ";
            // line 657
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "rol", array()), "html", null, true);
            echo "</div></td>
                        <td><div class=\"simuInput\" style=\"width:40px;\"> &nbsp; ";
            // line 658
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "hipotecado", array()), "html", null, true);
            echo "</div></td>
                        </tr>
                  


                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesHiporecario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 664
        echo "
                    <tr>
                        <td align=\"right\" colspan=\"3\">TOTAL VALOR COMERCIAL PROPIEDADES</td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp; ";
        // line 667
        if ((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : $this->getContext($context, "TotalPropiedadesApi")), 0, ".", "."), "html", null, true);
            echo "  ";
        }
        echo " </div></td>
                    </tr>
                    <tr>
                        <td align=\"right\" colspan=\"3\">TOTAL ACTIVOS</td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp; 
                        <!-- TOTAL DE ACTIVOS -->


                         
                        ";
        // line 676
        $context["totalAct"] = (((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario")) + (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles"))) + (isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : $this->getContext($context, "totalAhorroInversion")));
        // line 677
        echo "                            
                          ";
        // line 678
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAct"]) ? $context["totalAct"] : $this->getContext($context, "totalAct")), 0, ".", "."), "html", null, true);
        echo "




                        <!-- TOTAL  -->



                        </div></td>
                    </tr>
                </tbody>
            </table>
            <!-- Propiedades -->
            <br>
            <br>
            <br>
            <!-- Pasivos -->
            <table class=\"tabla\">
                <thead>
                <th align=\"left\" colspan=\"6\">PASIVOS (Deudas Vigentes)</th>
                </thead>
                <tbody>
                    <tr>
                        <td align=\"left\" colspan=\"6\">DEUDAS DE CORTO PLAZO ( Líneas de Crédito )    ";
        // line 702
        if ((isset($context["debesLineaCredito"]) ? $context["debesLineaCredito"] : $this->getContext($context, "debesLineaCredito"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo "</td>
                    </tr>
                    <tr>
                    
                    <td>Institución</td>
                    <td>Monto Aprobado</td>
                    <td>Monto Utilizado</td>
                    <td>Vencimiento </td>
                    </tr>


                     ";
        // line 713
        if ((isset($context["debesLineaCredito"]) ? $context["debesLineaCredito"] : $this->getContext($context, "debesLineaCredito"))) {
            // line 714
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesLineaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesLineaCredito"]) {
                // line 715
                echo "                        <tr>
                            <td><div class=\"simuInput\">&nbsp; ";
                // line 716
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "institucion", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
                // line 717
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoAprobado", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
                // line 718
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoUtilizado", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
                // line 719
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "vencimientofinal", array()), "html", null, true);
                echo "</div></td>

                        </tr>

                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesLineaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 724
            echo "
                ";
        }
        // line 726
        echo "
                  <tr>
                        <td align=\"right\" colspan=\"3\">TOTAL </td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp;  ";
        // line 729
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : $this->getContext($context, "totalDebesLinea")), 0, ".", "."), "html", null, true);
        echo " </div></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <br>
            <!-- TARJETAS DE CREDITO BANCARIAS -->
            <table class=\"tabla\" >
                <thead>
                <th colspan=\"5\" align=\"left\">TARJETAS DE CREDITO BANCARIAS / ";
        // line 739
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : $this->getContext($context, "debesTarjetaCredito"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo "</th>
                </thead>
                <tbody>


                    <tr>
                    <td>Tipo de Tarjeta</td>
                    <td>Institucion</td>
                    <td>Monto Aprobado</td>
                    <td>Monto Utilizado</td>
                    </tr>

                

                ";
        // line 753
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : $this->getContext($context, "debesTarjetaCredito"))) {
            // line 754
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesTarjetaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesTarjetaCredito"]) {
                // line 755
                echo "                <tr>
                    <td><div class=\"simuInput\">&nbsp; ";
                // line 756
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "tipotarjeta", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"simuInput\">&nbsp; ";
                // line 757
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"simuInput\">&nbsp; ";
                // line 758
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoaprobado", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"simuInput\">&nbsp; ";
                // line 759
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoutilizado", array()), "html", null, true);
                echo "</div></td>


                </tr>


                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesTarjetaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 766
            echo "
                ";
        }
        // line 768
        echo "


                        <tr>
                          <td align=\"right\" colspan=\"3\">TOTAL </td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp;  ";
        // line 773
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : $this->getContext($context, "totalDebesLinea")), 0, ".", "."), "html", null, true);
        echo " </div></td>
                    </tr>






                    <tr>
                        <td align=\"right\" colspan=\"3\">TOTAL TARJETAS DE CREDITO BANCARIAS</td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp; ";
        // line 783
        if ((isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : $this->getContext($context, "totalDebesTarjeta"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : $this->getContext($context, "totalDebesTarjeta")), 0, ".", "."), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                    </tr>
                </tbody>
            </table>


<br>
<br>
<br>
<br>
<br>
<br>
<br>


            <table class=\"tabla\" >
                <thead>
                <th colspan=\"5\" align=\"left\">Crédito  Consumo                  ";
        // line 800
        if ((isset($context["debesCreditoConsumo"]) ? $context["debesCreditoConsumo"] : $this->getContext($context, "debesCreditoConsumo"))) {
            echo " / SI ";
        } else {
            echo " NO ";
        }
        echo "</th>
                </thead>
                <tbody>

                <tr>
                    <td>Institución</td>
                    <td>Monto Aprobado</td>
                    <td>Monto Utilizado</td>
                    <td>Vencimiento </td>

                </tr>
                
                


                ";
        // line 815
        if ((isset($context["debesCreditoConsumo"]) ? $context["debesCreditoConsumo"] : $this->getContext($context, "debesCreditoConsumo"))) {
            // line 816
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesCreditoConsumo"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesCreditoConsumo"]) {
                // line 817
                echo "                        <tr>
                            <td><div class=\"simuInput\">&nbsp; ";
                // line 818
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "institucion", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
                // line 819
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "pagomensual", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
                // line 820
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudatotal", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
                // line 821
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "fechaultimopago", array()), "html", null, true);
                echo "</div></td>
                          <td><div class=\"simuInput\">&nbsp; ";
                // line 822
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudavigente", array()), "html", null, true);
                echo "</div></td>

                        </tr>

                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesCreditoConsumo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 827
            echo "
                ";
        }
        // line 829
        echo "




                        <tr>
                          <td align=\"right\" colspan=\"3\">TOTAL </td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp;  ";
        // line 836
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : $this->getContext($context, "totalDebesCredito")), 0, ".", "."), "html", null, true);
        echo " </div></td>
                    </tr>






                </tbody>
            </table>



            <!-- TARJETAS DE CREDITO BANCARIAS -->
            <br>
            <br>
            <br>
            <table class=\"tabla\" >
                <thead>
                <th colspan=\"6\" align=\"left\">DEUDAS DE LARGO PLAZO (Créditos Hipotecarios)  /  ";
        // line 855
        if ((isset($context["debesHipotecario"]) ? $context["debesHipotecario"] : $this->getContext($context, "debesHipotecario"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo " </th>
                </thead>
                <tbody>
                     <tr>
                        <td>Institución</td>
                        <td>Pago Mensual</td>
                        <td>Deuda Total</td>
                        <td>Vencimiento Final</td>
                        <td>Deuda Vigente</td>
                    </tr>


                  
                   
                    ";
        // line 869
        if ((isset($context["debesHipotecario"]) ? $context["debesHipotecario"] : $this->getContext($context, "debesHipotecario"))) {
            // line 870
            echo "                         ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesHipotecario"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesHipotecario"]) {
                // line 871
                echo "                <tr>
                    <td><div class=\"simuInput\">&nbsp; ";
                // line 872
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"simuInput\">&nbsp;";
                // line 873
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "pagomensual", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"simuInput\">&nbsp;";
                // line 874
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "deudatotal", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"simuInput\">&nbsp;";
                // line 875
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "venciminetofinal", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"simuInput\">&nbsp;";
                // line 876
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "duedavigente", array()), "html", null, true);
                echo "</div></td>

                </tr>

                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesHipotecario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 881
            echo "                    ";
        }
        // line 882
        echo "

                     <tr>
                          <td align=\"right\" colspan=\"3\">TOTAL </td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp;  ";
        // line 886
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : $this->getContext($context, "totalDebesHipotecario")), 0, ".", "."), "html", null, true);
        echo " </div></td>
                    </tr>


                </tbody>
            </table>
            <br>
            <br>
            <br>
            <table class=\"tabla\" >
                <thead>
                <th colspan=\"5\" align=\"left\">DEUDAS INDIRECTAS</th>
                </thead>
                <tbody>
                    <tr>
                        <td>INSTITUCION</td>
                        <td>NOMBRE AVALADO</td>
                        <td>FECHA DE VENCIMIENTO</td>
                        <td>PAGO MENSUAL</td>
                        <td>DEUDA VIGENTE</td>
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                    </tr>
                  
                        <td colspan=\"3\" align=\"right\">TOTAL DEUDAS INDIRECTAS</td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp;</div></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <br>
            <table class=\"tabla\" >
                <thead>
                <th colspan=\"3\" align=\"left\">ARRIENDOS PAGADOS</th>
                </thead>
                <tbody>
                    <tr>
                        <td>DESCRIPCION</td>
                        <td>PAGO MENSUAL</td>
                        <td>TIEMPO DE RESIDENCIA</td>
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                    </tr>
                  
                    <tr>
                        <td colspan=\"2\" align=\"right\">TOTAL ARRIENDOS PAGADOS</td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <br>
            <table class=\"tabla\">
                <thead>
                <th colspan=\"4\" align=\"left\">RENTA LIQUIDA MENSUAL</th>
                </thead>
                <tbody>
                <tbody>
                    <tr>
                        <td>SUELDO LIQUIDO MENSUAL</td>
                        <td>PROMEDIO ULTIMOS 6 PPM</td>
                        <td>RENTA GLOBAL COMPLEMENTARIO</td>
                        <td>BOLETAS DE HONORARIOS</td>
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td>ARRIENDOS</td>
                        <td>OTROS</td>
                        <td colspan=\"2\">RENTA LIQUIDA MENSUAL TOTAL</td>
                    </tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td><div class=\"simuInput\">&nbsp;</div></td>
                        <td colspan=\"2\"><div class=\"simuInput\">&nbsp;</div></td>
                    </tr>
                </tbody>
                </tbody>
            </table>
            <!-- Pasivos -->



               <br/><br/><br/><br/><br/><br/><br/><br/>
               <br>
               <br>
               <br>
               <br>
               <br> 
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>

               
            <div style=\"width: 710px;\">
                <hr style=\"color: #808080;\" size=\"8\" noshade>
                <h1 align=\"right\">SOLICITUD DE PRODUCTOS</h1>
                <hr style=\"color: #808080;\" size=\"8\" noshade>
            </div>


            <h3>Crédito de consumo:</h3>
            <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Monto de Crédito:</td>
                        <td>Plazo de Crédito </td>
                        <td>Seguros Asociados </td>
                        <td>¿Para qué utilizarás el Dinero? </td>

                    </tr>



                            <tr>
                                <td><div class=\"simuInput\">&nbsp;   ";
        // line 1029
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "montoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"simuInput\">&nbsp;    ";
        // line 1030
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"simuInput\">&nbsp;    ";
        // line 1031
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "segurosAsociados", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                                <td><div class=\"simuInput\">&nbsp;    ";
        // line 1032
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "paraque", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                            </tr>






                    </tbody>
                </table>




                <h3>Crédito Hipotecario </h3>
                <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Valor Propiedad :</td>
                        <td>Monto de Píe </td>
                        <td>Porcentaje </td>
                       

                    </tr>



                        <tr>
                            <td><div class=\"simuInput\">&nbsp;   ";
        // line 1067
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "valorPropiedad", array()), "html", null, true);
            echo " UF ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1068
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "montoPie", array()), "html", null, true);
            echo " UF ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp;   ";
        // line 1069
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "porcentaje", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        


                        </tr>



                    <tr> <td colspan=\"2\">Monto de Crédito </td></tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;  ";
        // line 1079
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "montoCredito", array()), "html", null, true);
            echo " UF ";
        }
        echo "</div></td>
                    </tr>





                    <tr>
                        <td>Plazo de Crédito </td>
                        <td>Tipo de Propiedad </td>
                    
                        <td>¿Tu propiedad es DFL2 y nueva ? </td>

                    </tr>



                        <tr>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1097
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                            <td><div class=\"simuInput\">&nbsp;";
        // line 1098
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "tipoPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "   </div></td>
                           
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1100
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "dfl2", array()), "html", null, true);
            echo " ";
        }
        echo "            </div></td>


                        </tr>



                <tr><td colspan=\"2\">Estado</td></tr>

                <tr> <td colspan=\"2\" ><div class=\"simuInput\">&nbsp; ";
        // line 1109
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "estadoPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td></tr>








                    <tr>
                        <td>Comuna donde desea comprar</td>
                        <td>Proyecto / Inmobiliaria </td>


                    </tr>



                        <tr>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1128
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "comunaComprar", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1129
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "proyecto", array()), "html", null, true);
            echo "     ";
        }
        echo "</div></td>


                        </tr>







                    </tbody>
                </table>

    <h3>Crédito Automotriz</h3>

                    <table class=\"tabla\">
                        <thead>

                        </thead>
                        <tbody>
                        <tr>
                            <td>Monto del Auto </td>
                            <td>Monto del Píe  </td>
                            <td>Plazo del Crédito </td>


                        </tr>


                                <tr>
                                    <td><div class=\"simuInput\">&nbsp; ";
        // line 1160
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "monto", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                                    <td><div class=\"simuInput\">&nbsp; ";
        // line 1161
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "pie", array()), "html", null, true);
            echo "    ";
        }
        echo "</div></td>
                                    <td><div class=\"simuInput\">&nbsp; ";
        // line 1162
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "plazo", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>


                                </tr>



                        <tr>
                            <td>Cuándo piensas comprar tu auto </td>
                            <td>Marca de Auto  </td>
                            <td>Año del Auto </td>


                        </tr>


                        <tr>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1179
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "cuandocompras", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1180
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "marcaauto", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1181
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "ano", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>





                        </tbody>
                    </table>




<table class=\"tabla\" >

<h3>
DATOS CRÉDITO CONSOLIDACION 
</h3>
<tr>
    <th>Crédito </th>
    <th>Monto </th>
    <th>Plazo </th>

  
    </tr>



    </thead>

    <tr>
    ";
        // line 1214
        $context["con"] = 0;
        // line 1215
        echo "    ";
        if ((isset($context["creditoConsolidacion"]) ? $context["creditoConsolidacion"] : $this->getContext($context, "creditoConsolidacion"))) {
            // line 1216
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["creditoConsolidacion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["creditoConsolidacion"]) {
                // line 1217
                echo "    ";
                $context["con"] = ((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")) + 1);
                // line 1218
                echo "
    ";
                // line 1219
                if (((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")) <= 1)) {
                    // line 1220
                    echo "
    <tr>   

        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1223
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1224
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1225
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo", array()), "html", null, true);
                    echo " </div> </td>
     </tr>
        
       <tr>    

        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1230
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1231
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1232
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo2", array()), "html", null, true);
                    echo " </div> </td>
</tr>
<tr>   


        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1237
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito3", array()), "html", null, true);
                    echo "  </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1238
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito3", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1239
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo3", array()), "html", null, true);
                    echo " </div></td>
       
   </tr> 
     ";
                }
                // line 1243
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsolidacion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1244
            echo "    ";
        }
        // line 1245
        echo "
    
        </tr>

</table></div>
                
        </div>
    </body>
</html>


";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:plantillas:bbva2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2073 => 1245,  2070 => 1244,  2064 => 1243,  2057 => 1239,  2053 => 1238,  2049 => 1237,  2041 => 1232,  2037 => 1231,  2033 => 1230,  2025 => 1225,  2021 => 1224,  2017 => 1223,  2012 => 1220,  2010 => 1219,  2007 => 1218,  2004 => 1217,  1999 => 1216,  1996 => 1215,  1994 => 1214,  1954 => 1181,  1946 => 1180,  1938 => 1179,  1914 => 1162,  1906 => 1161,  1898 => 1160,  1860 => 1129,  1852 => 1128,  1827 => 1109,  1812 => 1100,  1803 => 1098,  1795 => 1097,  1770 => 1079,  1753 => 1069,  1745 => 1068,  1737 => 1067,  1695 => 1032,  1687 => 1031,  1679 => 1030,  1671 => 1029,  1525 => 886,  1519 => 882,  1516 => 881,  1505 => 876,  1501 => 875,  1497 => 874,  1493 => 873,  1489 => 872,  1486 => 871,  1481 => 870,  1479 => 869,  1458 => 855,  1436 => 836,  1427 => 829,  1423 => 827,  1412 => 822,  1408 => 821,  1404 => 820,  1400 => 819,  1396 => 818,  1393 => 817,  1388 => 816,  1386 => 815,  1364 => 800,  1340 => 783,  1327 => 773,  1320 => 768,  1316 => 766,  1303 => 759,  1299 => 758,  1295 => 757,  1291 => 756,  1288 => 755,  1283 => 754,  1281 => 753,  1260 => 739,  1247 => 729,  1242 => 726,  1238 => 724,  1227 => 719,  1223 => 718,  1219 => 717,  1215 => 716,  1212 => 715,  1207 => 714,  1205 => 713,  1187 => 702,  1160 => 678,  1157 => 677,  1155 => 676,  1135 => 667,  1130 => 664,  1118 => 658,  1114 => 657,  1110 => 656,  1105 => 654,  1101 => 653,  1097 => 652,  1091 => 648,  1087 => 647,  1069 => 631,  1058 => 630,  1029 => 616,  1024 => 613,  1021 => 612,  1010 => 607,  1006 => 606,  1002 => 605,  998 => 604,  994 => 603,  990 => 602,  987 => 601,  982 => 600,  980 => 599,  954 => 586,  928 => 571,  922 => 567,  913 => 564,  908 => 562,  904 => 561,  900 => 560,  897 => 559,  893 => 558,  871 => 548,  846 => 530,  826 => 517,  818 => 516,  788 => 493,  780 => 492,  772 => 491,  752 => 478,  737 => 469,  726 => 468,  704 => 453,  696 => 452,  678 => 441,  670 => 440,  662 => 439,  632 => 416,  615 => 404,  609 => 403,  601 => 402,  595 => 401,  579 => 392,  573 => 391,  565 => 390,  557 => 389,  534 => 373,  526 => 372,  518 => 371,  464 => 319,  458 => 318,  449 => 316,  422 => 296,  385 => 261,  379 => 260,  370 => 258,  343 => 238,  298 => 200,  291 => 199,  262 => 177,  235 => 157,  211 => 140,  204 => 139,  196 => 138,  176 => 125,  169 => 124,  150 => 107,  133 => 106,  123 => 103,  19 => 1,);
    }
}
