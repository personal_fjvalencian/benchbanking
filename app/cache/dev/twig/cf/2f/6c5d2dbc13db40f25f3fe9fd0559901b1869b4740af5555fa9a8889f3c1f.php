<?php

/* BenchAdminBundle:plantillas:consorcio.html.twig */
class __TwigTemplate_cf2f6c5d2dbc13db40f25f3fe9fd0559901b1869b4740af5555fa9a8889f3c1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"es\">
    <head>
        <meta charset=\"UTF-8\">
        <title>Plantilla Consorcio</title>
        <style>

        body{
            font-family: verdana;
            font-size: 12px;


        }

            .tabla{
                border: solid;
                color: #112b51;
                width: 690px;
            }

            .input{
                color: #112b51;
                width: 100%;
                height: 20px;
                border-style: solid;
                border-width: 1px;
                position: relative;
                float: left;
            }
        </style>
    </head>
    <body>
        <div id=\"container\">
            <table id=\"cabecera\" style=\"width: 710px;\">
                <tbody>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <img src=\"http://s3-eu-west-1.amazonaws.com/rankia/images/valoraciones/0011/9499/banco-consorcio.png?1374144783\">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        AHUMADA 370, Of. 800 - SANTIAGO
                                    </td>

                                </tr>
                                <tr>
                                    <td>Call Center: (02) 787 1800 </td>
                                </tr>
                                <tr>
                                    <td>www.bancoconsorcio.cl</td>
                                </tr>

                            </table>
                        </td>
                        <td>
                            <hr style=\"color: #808080;\" size=\"8\" noshade>
                            <h1 align=\"right\">ESTADO DE SITUACIÓN</h1>
                            <hr style=\"color: #808080;\" size=\"8\" noshade>
                        </td>

                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <br>
            <br>
            <h3>Antecedentes Personales</h3>
            <table class=\"tabla\">
                <tbody>
                    <tr>
                        <td colspan=\"4\" align=\"right\">Rut</td>
                        <td><div class=\"input\">&nbsp; ";
        // line 77
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "rut", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    </tr>
                    <tr>
                        <td colspan=\"2\">Nombre</td>
                        <td colspan=\"2\">Apellido Paterno</td>
                        <td>Apellido Materno</td>
                    </tr>
                    <tr>
                        <td colspan=\"5\"><div class=\"input\">&nbsp; ";
        // line 85
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nombre", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellido", array()), "html", null, true);
            echo "
                        ";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    </tr>
                    <tr>
                        <td>Fecha Nacimiento</td>
                        <td colspan=\"2\">Sexo</td>
                        <td  >Nacionalidad</td>
                        <td>Estado Civil</td>
                    </tr>
                    <tr>
                        <td><div class=\"input\" style=\"width:100px;\">&nbsp; ";
        // line 95
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "fechanacimiento", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"2\"><div class=\"input\" tyle=\"width:100px;\" >&nbsp; ";
        // line 96
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "sexo", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td ><div class=\"input\">&nbsp;  ";
        // line 97
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nacionalidad", array()), "html", null, true);
        }
        echo " </div></td>
                        <td ><div class=\"input\" style=\"font-size:9px;\">&nbsp;";
        // line 98
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "estadocivil", array()), "html", null, true);
        }
        echo "</div></td>
                    </tr>


                    <tr>
                        <td width=\"50px\">N° integrantes </td>
                        <td colspan=\"2\">Régimen Conyugal</td>
                        <td>R.U.T. Cónyuge</td>
                        <td colspan=\"2\" >Lugar donde habita</td>

                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;";
        // line 110
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "ndependientes", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"2\"><div class=\"input\">&nbsp;";
        // line 111
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "estadocivil", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                        <td><div class=\"input\" style=\"width:90px;\">&nbsp;";
        // line 112
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "rut", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                        <td colspan=\"2\"><div class=\"input\" style=\"width:140px;\">&nbsp;";
        // line 113
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "tipocasa", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

                    </tr>
                    <tr>
                        <td>Nivel de estudios</td>
                        <td colspan=\"2\" >Profesión</td>
                        <td colspan=\"2\">Nombre universidad o instituto prefesional</td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp; ";
        // line 122
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "niveleducacion", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"2\" ><div class=\"input\">&nbsp; ";
        // line 123
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "profesion", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"2\"><div class=\"input\">&nbsp; ";
        // line 124
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "universidad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    </tr>
                    <tr>
                        <td colspan=\"2\">Dirección</td>


                        <td>Comuna / Region </td>
                        <td> Region </td>
                    </tr>
                    <tr>
                        <td colspan=\"2\"><div class=\"input\"> ";
        // line 134
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        <td><div class=\"input\" style=\"width:180px;\">&nbsp;";
        // line 137
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "comuna", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                        <td><div class=\"input\" style=\"width:180px;\"> ";
        // line 138
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "region", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                    </tr>
                    <tr>

                        <td>Teléfono Particular</td>
         
                        <td colspan=\"2\">E-mail</td>
                    </tr>
                    <tr>

                        <td><div class=\"input\">&nbsp; ";
        // line 148
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "telefono", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                
                        <td colspan=\"2\" ><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 150
        if ((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "email", array()), "html", null, true);
        }
        echo "</div></td>
                    </tr>

                </tbody>
            </table>
            <br>
            <br>
            <h3>Antecedentes Laborales</h3>
            <table class=\"tabla\">
                <tbody>
                    <tr>
                        <td>Tipo Actividad</td>
                        <td>Tipo Contrato</td>

                        <td>Fecha Ingreso</td>
                      
                        <td>Renta liquida (\$)</td>

                    </tr>
                    <tr>
                        <td><div class=\"input\"> &nbsp; ";
        // line 170
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "industria", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td><div class=\"input\">&nbsp;  ";
        // line 171
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "   ";
        }
        echo "</div></td>

                        <td><div class=\"input\">&nbsp;  ";
        // line 173
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "fechaingreso", array()), "html", null, true);
            echo "  ";
        }
        echo "  </div></td>
                       
                        <td><div class=\"input\">&nbsp; ";
        // line 175
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "independientesueldoliquido", array()), "html", null, true);
            echo "  ";
        }
        echo "  </div></td>
                    </tr>

                    <tr >
                        <td colspan=\"2\">Tipo Renta</td>
                        <td colspan=\"2\">Cargo o actividad</td>
                    </tr>

                    <tr >

                        <td colspan=\"2\"> <div class=\"input\"  style=\"width:300px;\"> ";
        // line 185
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "situacionlaboral", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

                         <td colspan=\"2\"><div class=\"input\" style=\"width:300px;\" >&nbsp; ";
        // line 187
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "cargoactual", array()), "html", null, true);
            echo "  ";
        }
        echo " </div></td>
                    </tr>



                    <tr>
                        <td colspan=\"2\">R.U.T Empleador</td>
                        <td colspan=\"2\">Nombre empleador</td>
                        <td colspan=\"2\">Giro o rubro empleador</td>

                    </tr>
                    <tr>
                        <td colspan=\"2\"><div class=\"input\">&nbsp; ";
        // line 199
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "rutempresa", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                        <td colspan=\"2\"><div class=\"input\">&nbsp; ";
        // line 200
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "empleadorempresa", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td colspan=\"2\"><div class=\"input\">&nbsp;   </div></td>
                        <td><div class=\"input\"></div></td>
                    </tr>
                    <tr>
                        <td colspan=\"3\">Dirección</td>
                        <td>Número</td>
                        <td colspan=\"2\">Otros datos dirección</td>

                    </tr>
                    <tr>
                        <td colspan=\"3\"><div class=\"input\">&nbsp; ";
        // line 211
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                        <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td>Comuna</td>
                        <td>Ciudad</td>
                        <td colspan=\"2\">Región</td>
                        <td>Teléfono comercial</td>

                    </tr>


                    <tr>
                        <td><div class=\"input\">&nbsp; ";
        // line 226
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td><div class=\"input\">&nbsp;";
        // line 227
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "ciudad", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                        <td colspan=\"2\" ><div class=\"input\">&nbsp; ";
        // line 228
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "region", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        <td><div class=\"input\">&nbsp; ";
        // line 229
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "telefono", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>

                    </tr>

                </tbody>
            </table>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3>Referencias Personales</h3>
            <table class=\"tabla\">
                <tbody>
                    <tr>
                        <td>Nombre</td>
                        <td>Apellido Paterno</td>
                        <td>Apellido Materno</td>
                        <td>Relación con solicitante</td>
                        <td>Teléfono fijo</td>
                    </tr>
                    <tr>
                        <td colspan=\"3\"><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td>Nombre</td>
                        <td>Apellido Paterno</td>
                        <td>Apellido Materno</td>
                        <td>Relación con solicitante</td>
                        <td>Teléfono fijo</td>
                    </tr>
                    <tr>
                        <td colspan=\"3\"><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <h3>Resumen estado de situación</h3>
            <table id=\"resumenSituacion\" class=\"tabla\">
                <tbody>
                    <tr>
                        <td>Resumen Activos</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Resumen Pasivos</td>
                        <td>Número de acreedores</td>
                        <td> <div class=\"input\"></div></td>
                    </tr>
                    <tr style=\"margin-top:0;\">
                        <td colspan=\"4\">
                            <table>
                                <tr>
                                    <td></td>
                                    <td>Cantidad</td>
                                    <td>Valor Comercial</td>
                                </tr>
                                <tr>
                                    <td>Bienes Raices  /  ";
        // line 296
        if ((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario"))) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : $this->getContext($context, "TotalPropiedadesApi"))) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo "</td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\">&nbsp; ";
        // line 298
        if ((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["cantidadHipotecario"]) ? $context["cantidadHipotecario"] : $this->getContext($context, "cantidadHipotecario")), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:80px;\">&nbsp; ";
        // line 301
        if ((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario")), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : $this->getContext($context, "TotalPropiedadesApi"))) {
                echo " ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : $this->getContext($context, "TotalPropiedadesApi")), 0, ".", "."), "html", null, true);
                echo "   ";
            }
            echo " ";
        }
        echo "</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>vehículo  / ";
        // line 305
        if ((isset($context["cantidadAutomoviles"]) ? $context["cantidadAutomoviles"] : $this->getContext($context, "cantidadAutomoviles"))) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : $this->getContext($context, "TotalAutomovilesApi"))) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo "</td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\">&nbsp; ";
        // line 307
        if ((isset($context["cantidadAutomoviles"]) ? $context["cantidadAutomoviles"] : $this->getContext($context, "cantidadAutomoviles"))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["cantidadAutomoviles"]) ? $context["cantidadAutomoviles"] : $this->getContext($context, "cantidadAutomoviles")), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:80px;\" >&nbsp; ";
        // line 310
        if ((isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles")), 0, ".", "."), "html", null, true);
            echo "  ";
        } else {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : $this->getContext($context, "TotalPropiedadesApi")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Deposito a plazo cuenta de ahorro  /  ";
        // line 315
        if ((isset($context["cantidadAhorroInversion"]) ? $context["cantidadAhorroInversion"] : $this->getContext($context, "cantidadAhorroInversion"))) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : $this->getContext($context, "totalAhorroInversionApi"))) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            // line 316
            echo "                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\">&nbsp; ";
            // line 318
            if ((isset($context["cantidadAhorroInversion"]) ? $context["cantidadAhorroInversion"] : $this->getContext($context, "cantidadAhorroInversion"))) {
                echo twig_escape_filter($this->env, (isset($context["cantidadAhorroInversion"]) ? $context["cantidadAhorroInversion"] : $this->getContext($context, "cantidadAhorroInversion")), "html", null, true);
                echo " ";
            }
            echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:80px;\">&nbsp; ";
            // line 321
            if ((isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : $this->getContext($context, "totalAhorroInversion"))) {
                echo "  ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : $this->getContext($context, "totalAhorroInversion")), 0, ".", "."), "html", null, true);
                echo " ";
            } else {
                // line 322
                echo "                                         ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : $this->getContext($context, "totalAhorroInversionApi")), 0, ".", "."), "html", null, true);
                echo "   

                                         ";
            }
            // line 324
            echo " ";
        }
        echo "</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Cuenta corriente</td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\" >&nbsp;</div>
                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:80px;\">&nbsp;</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Acciones y bonos/
                                        Participacion en sociedades ";
        // line 340
        if ((isset($context["cantidadSociedades"]) ? $context["cantidadSociedades"] : $this->getContext($context, "cantidadSociedades"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 341
        echo "                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\" >&nbsp; ";
        // line 343
        if ((isset($context["cantidadSociedades"]) ? $context["cantidadSociedades"] : $this->getContext($context, "cantidadSociedades"))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["cantidadSociedades"]) ? $context["cantidadSociedades"] : $this->getContext($context, "cantidadSociedades")), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:80px;\" >&nbsp; ";
        // line 346
        if ((isset($context["totalAhorroInversionAcciones"]) ? $context["totalAhorroInversionAcciones"] : $this->getContext($context, "totalAhorroInversionAcciones"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionAcciones"]) ? $context["totalAhorroInversionAcciones"] : $this->getContext($context, "totalAhorroInversionAcciones")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=\"2\">Total</td>
                                    <td colspan=\"2\">
                                        <div  class=\"input\" >&nbsp; ";
        // line 352
        if ((isset($context["totalActivos"]) ? $context["totalActivos"] : $this->getContext($context, "totalActivos"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalActivos"]) ? $context["totalActivos"] : $this->getContext($context, "totalActivos")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                        <td colspan=\"3\">
                            <table>
                                <tr>
                                    <td></td>
                                    <td>Cantidad</td>
                                    <td>Pago Mensual</td>
                                    <td>Deuda Total </td>
                                </tr>
                                <tr>
                                    <td>
                                        Consumo/Comercial/Automotriz    ";
        // line 383
        if ((isset($context["cantidadDebesConsumo"]) ? $context["cantidadDebesConsumo"] : $this->getContext($context, "cantidadDebesConsumo"))) {
            echo " SI  ";
        } else {
            echo " NO ";
        }
        // line 384
        echo "                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\">&nbsp; ";
        // line 386
        if ((isset($context["cantidadDebesConsumo"]) ? $context["cantidadDebesConsumo"] : $this->getContext($context, "cantidadDebesConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["cantidadDebesConsumo"]) ? $context["cantidadDebesConsumo"] : $this->getContext($context, "cantidadDebesConsumo")), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:60px;\">&nbsp; ";
        // line 389
        if ((isset($context["totalPagoMensual"]) ? $context["totalPagoMensual"] : $this->getContext($context, "totalPagoMensual"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalPagoMensual"]) ? $context["totalPagoMensual"] : $this->getContext($context, "totalPagoMensual")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\" style=\"width:80px;\">&nbsp;  ";
        // line 392
        if ((isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : $this->getContext($context, "totalDebesCredito"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : $this->getContext($context, "totalDebesCredito")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Dscto por Planilla</td>
                                    <td>
                                        <div class=\"input\"  style=\"width:20px;\" >&nbsp;</div>
                                    </td>
                                    <td><div class=\"input\">&nbsp;</div>


                                    </td>
                                    <td>
                                        <div class=\"input\">&nbsp;</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tarjeta / ";
        // line 409
        if ((isset($context["cantidadDebesTarjeta"]) ? $context["cantidadDebesTarjeta"] : $this->getContext($context, "cantidadDebesTarjeta"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo " </td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\">&nbsp; ";
        // line 411
        if ((isset($context["cantidadDebesTarjeta"]) ? $context["cantidadDebesTarjeta"] : $this->getContext($context, "cantidadDebesTarjeta"))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["cantidadDebesTarjeta"]) ? $context["cantidadDebesTarjeta"] : $this->getContext($context, "cantidadDebesTarjeta")), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\">&nbsp;</div>
                                    </td>
                                    <td>
                                        <div class=\"input\">&nbsp; ";
        // line 417
        if ((isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : $this->getContext($context, "totalDebesTarjeta"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : $this->getContext($context, "totalDebesTarjeta")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Linea Credito /  ";
        // line 421
        if ((isset($context["cantidadLineaCredito"]) ? $context["cantidadLineaCredito"] : $this->getContext($context, "cantidadLineaCredito"))) {
            echo "  SI ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : $this->getContext($context, "pagoMensualCreditoApi"))) {
                echo " SI  ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\">&nbsp; ";
        // line 423
        if ((isset($context["cantidadLineaCredito"]) ? $context["cantidadLineaCredito"] : $this->getContext($context, "cantidadLineaCredito"))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["cantidadLineaCredito"]) ? $context["cantidadLineaCredito"] : $this->getContext($context, "cantidadLineaCredito")), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\">&nbsp; ";
        // line 426
        if ((isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : $this->getContext($context, "pagoMensualCreditoApi"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : $this->getContext($context, "pagoMensualCreditoApi")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\">&nbsp; ";
        // line 429
        if ((isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : $this->getContext($context, "totalDebesLinea"))) {
            echo "  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : $this->getContext($context, "totalDebesLinea")), 0, ".", "."), "html", null, true);
        }
        echo "</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bienes Raices ";
        // line 433
        if ((isset($context["cantidadDebesHipotecario"]) ? $context["cantidadDebesHipotecario"] : $this->getContext($context, "cantidadDebesHipotecario"))) {
            echo "  SI ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : $this->getContext($context, "pagoMensualApi"))) {
                echo "  SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </td>
                                    <td>
                                        <div class=\"input\" style=\"width:20px;\">&nbsp; ";
        // line 435
        if ((isset($context["cantidadDebesHipotecario"]) ? $context["cantidadDebesHipotecario"] : $this->getContext($context, "cantidadDebesHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["cantidadDebesHipotecario"]) ? $context["cantidadDebesHipotecario"] : $this->getContext($context, "cantidadDebesHipotecario")), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\">&nbsp; ";
        // line 438
        if ((isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : $this->getContext($context, "pagoMensualApi"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : $this->getContext($context, "pagoMensualApi")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                    <td>
                                        <div class=\"input\">&nbsp; ";
        // line 441
        if ((isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : $this->getContext($context, "totalDebesHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : $this->getContext($context, "totalDebesHipotecario")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Total Pasivos </td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class=\"input\">&nbsp; ";
        // line 449
        if ((isset($context["totalDebes"]) ? $context["totalDebes"] : $this->getContext($context, "totalDebes"))) {
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebes"]) ? $context["totalDebes"] : $this->getContext($context, "totalDebes")), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </tbody>
                <thead>
                <th colspan=\"7\" align=\"left\">Referencias Comerciales (Principal Institucion)</th>
                </thead>
                <tbody>

                </tbody>
            </table>
            <br>
            <br>
            <br>
            <h3>Empleo Anterior</h3>
            <table id=\"empleoAnterior\" class=\"tabla\">
                <tbody>
                    <tr>
                        <td>Cargo ó actividad</td>
                        <td>Nombre de empleador</td>
                        <td>Fecha de ingreso</td>
                        <td>Fecha de salida</td>
                        <td>Renta Liquida</td>
                    </tr>
                    <tr>
                        <td>
                            <div class=\"input\">&nbsp;</div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp;</div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp;</div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp;</div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp;</div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h3> Datos Cónyuge</h3>
            <table id=\"grupoFamiliar\" class=\"tabla\">
                <tbody>
                    <tr>
                        <td>Nombres</td>
                        <td>Apellido Paterno</td>
                        <td>Apellido Materno</td>
                    </tr>


                    ";
        // line 506
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            // line 507
            echo "                    <tr>

                        <td>
                            <div class=\"input\">&nbsp; ";
            // line 510
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "nombre", array()), "html", null, true);
            echo "  </div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp; ";
            // line 513
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "apellido", array()), "html", null, true);
            echo "</div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp; ";
            // line 516
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "apellidoseg", array()), "html", null, true);
            echo "</div>
                        </td>

                    </tr>

                    ";
        }
        // line 522
        echo "



                    <tr><td colspan=\"5\"><div class=\"input\"></div></td></tr>
                    <tr>
                        <td>Fecha de Nacimiento</td>
                        <td>Tipo de Actividad</td>
                        <td>Cargo/Actividad</td>
                        <td>Fecha Ingreso</td>

                    </tr>



                    ";
        // line 537
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            // line 538
            echo "

                    <tr>
                        <td><div class=\"input\">&nbsp; ";
            // line 541
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "fechanacimiento", array()), "html", null, true);
            echo "</div></td>
                        <td><div class=\"input\">&nbsp;";
            // line 542
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "actividad", array()), "html", null, true);
            echo "</div></td>
                        <td><div class=\"input\">&nbsp;";
            // line 543
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "cargoactual", array()), "html", null, true);
            echo " </div></td>
                        <td><div class=\"input\">&nbsp; </div></td>
                    </tr>



                    ";
        }
        // line 550
        echo "


                    <tr>
                        <td>Complemento Renta</td>
                        <td>R.U.T Empresa</td>
                        <td colspan=\"2\">Nombre Empleador</td>

                    </tr>

                    ";
        // line 560
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            // line 561
            echo "                    <tr>
                        <td><div class=\"input\">&nbsp;  </div></td>
                        <td><div class=\"input\">&nbsp; </div></td>
                        <td colspan=\"2\"><div class=\"input\">&nbsp; ";
            // line 564
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "empresa", array()), "html", null, true);
            echo " </div></td>
                    </tr>

                    ";
        }
        // line 568
        echo "
                    <tr>
                        <td colspan=\"3\">Teléfono Comercial</td>
                        <td colspan=\"2\">Renta Liquida</td>
                    </tr>
                    <tr>
                        <td colspan=\"2\" ><div class=\"input\">&nbsp;</div></td>
                        <td colspan=\"2\" ><div class=\"input\">&nbsp; ";
        // line 575
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "rentaliquidad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    </tr>
                </tbody>
            </table>
            <h3>Datos de otros miembros de la familia</h3>
            <table class=\"tabla\">
                <thead>
                <th align=\"center\"> Nombre</th>
                <th align=\"center\"> Fecha de nacimiento</th>
                <th align=\"center\"> Sexo </th>
                <th align=\"center\"> Relacion con el solicitante </th>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class=\"input\">&nbsp; ";
        // line 590
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "nombre", array()), "html", null, true);
            echo " ";
        }
        echo "</div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp;";
        // line 593
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge")), "fechanacimiento", array()), "html", null, true);
            echo " ";
        }
        echo "</div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp;";
        // line 596
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " ";
            if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "sexo", array()) == "Hombre")) {
                echo " Mujer ";
            } else {
                echo " Hombre ";
            }
        }
        echo "</div>
                        </td>
                        <td>
                            <div class=\"input\">&nbsp;";
        // line 599
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : $this->getContext($context, "Conyuge"))) {
            echo " Conyuge ";
        }
        echo "</div>
                        </td>
                    </tr>
                                     


                </tbody>
            </table>
            <br>
            <br>
            <br>
            <br/><br/><br/><br/><br/><br/>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <h3>Antecedentes Financieros</h3>
            <table id=\"antecedentesFinancieros\"  class=\"tabla\" >
                <tbody>
                    <tr>
                        <td>
                            <table>
                                <thead>
                                <th colspan=\"2\" align=\"2\">
                                    Ingreso Mensual
                                </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Renta</td>
                                        <td>Monto(\$)</td>
                                    </tr>
                                    <tr>
                                        <td>Fijo</td>

                                        <td><div class=\"input\">&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td>Varíable</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td>Total Ingreso Mensual</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Declaracion Anual de
                                            impuesto, DAI (Base imponible)
                                        </td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table>
                                <thead>
                                <th colspan=\"4\" align=\"2\">
                                    Detalle Renta Variable
                                </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Renta</td>
                                        <td>Monto(\$)</td>
                                        <td>Renta</td>
                                        <td>Monto(\$)</td>
                                    </tr>
                                    <tr>
                                        <td>Honorarios</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                        <td>Arriendos</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td>Comisión</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                        <td>Pensiones</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td>Bono</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                        <td>Otros</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan=\"3\">Total renta variable(\$)</td>
                                        <td><div class=\"input\">&nbsp;</div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h3>Detalles Activos</h3>
            Bienes Raíces /    ";
        // line 704
        if ((isset($context["tienesHiporecario"]) ? $context["tienesHiporecario"] : $this->getContext($context, "tienesHiporecario"))) {
            echo " SI ";
        } else {
            echo "  NO ";
        }
        // line 705
        echo "            <table class=\"tabla\" >
                <tbody>

                    <tr>
                        <td>Tipo</td>
                        <td>Direccion</td>
                        <td>Region</td>

                        <td>Avalúo Fiscal </td>
                        <td>Rol </td>
                        <td>Hipotecado </td>
                    </tr>

                    ";
        // line 718
        if ((isset($context["tienesHiporecario"]) ? $context["tienesHiporecario"] : $this->getContext($context, "tienesHiporecario"))) {
            // line 719
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesHiporecario"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesHiporecario"]) {
                // line 720
                echo "                    <tr>
                        <td><div class=\"input\" style=\"width:90px;\">&nbsp; ";
                // line 721
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "tipo", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\" style=\"width:140px;\" >&nbsp; ";
                // line 722
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "direccion", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\" style=\"width:140px;\">&nbsp; ";
                // line 723
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "region", array()), "html", null, true);
                echo "</div></td>

                        <td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
                // line 725
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "avaluo", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
                // line 726
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "rol", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\" style=\"width:40px;\"> &nbsp; ";
                // line 727
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "hipotecado", array()), "html", null, true);
                echo "</div></td>

                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesHiporecario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 731
            echo "                ";
        }
        // line 732
        echo "





                </tbody>
            </table>



            <table>
                <tr><td>Total Bienes Raíces </td></tr>
                <tr><td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
        // line 745
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario")), 0, ".", "."), "html", null, true);
        echo " </div></td></tr>
            </table>






            <h3>Vehículos  /   ";
        // line 753
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : $this->getContext($context, "tienesAutomoviles"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo "</h3>
            <table class=\"tabla\" >
                <tbody>
                    <tr>
                        <td>Tipo</td>
                        <td>Marca</td>
                        <td>Modelo</td>
                        <td>Año</td>
                        <td>Patente</td>

                        <td>Valor Comercial(\$)</td>
                    </tr>

                    ";
        // line 766
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : $this->getContext($context, "tienesAutomoviles"))) {
            // line 767
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesAutomoviles"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesAutomoviles"]) {
                // line 768
                echo "                    <tr>
                        <td><div class=\"input\">&nbsp;";
                // line 769
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "tipo", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\">&nbsp; ";
                // line 770
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "marcaauto", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\">&nbsp;";
                // line 771
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "modelo", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\">&nbsp;";
                // line 772
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "anoauto", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\">&nbsp;";
                // line 773
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "patente", array()), "html", null, true);
                echo "</div></td>
                        <td><div class=\"input\">&nbsp;";
                // line 774
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "valorcomercial", array()), "html", null, true);
                echo "</div></td>

                    </tr>




                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAutomoviles'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 782
            echo "                ";
        }
        // line 783
        echo "
                </tbody>
            </table>



            <table>
                <tr><td>Total Vehículos</td></tr>
                <tr><td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
        // line 791
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles")), 0, ".", "."), "html", null, true);
        echo " </div></td></tr>
            </table>






            <h3>Participación en Sociedades /
                ";
        // line 800
        if ((isset($context["tienesSociedades"]) ? $context["tienesSociedades"] : $this->getContext($context, "tienesSociedades"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo "</h3>
            <table class=\"tabla\">
                <tr>
                    <td>Razón Social</td>
                    <td>RUT Sociedad</td>

                    <td>% Participación</td>
                    <td>Valor comercial(\$)</td>
                </tr>


                ";
        // line 811
        if ((isset($context["tienesSociedades"]) ? $context["tienesSociedades"] : $this->getContext($context, "tienesSociedades"))) {
            // line 812
            echo "
                ";
            // line 813
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesSociedades"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesSociedades"]) {
                // line 814
                echo "                <tr>
                    <td><div class=\"input\">&nbsp; ";
                // line 815
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "nombre", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 816
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "rut", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 817
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "porcentaje", array()), "html", null, true);
                echo "</div></td>

                    <td><div class=\"input\">&nbsp; ";
                // line 819
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "valor", array()), "html", null, true);
                echo "</div></td>
                </tr>


                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesSociedades'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 824
            echo "                ";
        }
        // line 825
        echo "


            </table>


            <table>
                <tr><td>Total Participación en Sociedades</td></tr>
                <tr><td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
        // line 833
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalSociedades"]) ? $context["totalSociedades"] : $this->getContext($context, "totalSociedades")), 0, ".", "."), "html", null, true);
        echo " </div></td></tr>
            </table>






            </table>
            <h3>Ahorro Inversión  ";
        // line 842
        if ((isset($context["tienesAhorroInversion"]) ? $context["tienesAhorroInversion"] : $this->getContext($context, "tienesAhorroInversion"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo "</h3>
            <table class=\"tabla\">
                <tr>
                    <td>Tipo</td>
                    <td>Institución</td>

                    <td>Valor Comercial </td>
                    <td>Prendado </td>
                </tr>


                ";
        // line 853
        if ((isset($context["tienesAhorroInversion"]) ? $context["tienesAhorroInversion"] : $this->getContext($context, "tienesAhorroInversion"))) {
            // line 854
            echo "
                    ";
            // line 855
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesAhorroInversion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesAhorroInversion"]) {
                // line 856
                echo "                        <tr>
                            <td><div class=\"input\">&nbsp; ";
                // line 857
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "tipo", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
                // line 858
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "institucion", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
                // line 859
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "valorcomercial", array()), "html", null, true);
                echo "</div></td>

                            <td><div class=\"input\">&nbsp; ";
                // line 861
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "prendado", array()), "html", null, true);
                echo "</div></td>
                        </tr>


                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAhorroInversion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 866
            echo "                ";
        }
        // line 867
        echo "

            </table>



            <table>
                <tr><td>Ahorro Inversión </td></tr>
                <tr><td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
        // line 875
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : $this->getContext($context, "totalAhorroInversion")), 0, ".", "."), "html", null, true);
        echo " </div></td></tr>
            </table>



            <br/><br/><br/><br/><br/><br/>
            <br/><br/><br/><br>
            <br>
            <br>
            <br>
        

            <h3>Pasivos</h3>

            Hipotecario  / ";
        // line 889
        if ((isset($context["debesHipotecario"]) ? $context["debesHipotecario"] : $this->getContext($context, "debesHipotecario"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 890
        echo "            <table class=\"tabla\">
                <thead>
                <th colspan=\"5\" align=\"left\"> </th>
                </thead>
                <tbody>
                    <tr>
                        <td>Institución</td>
                        <td>Pago Mensual</td>
                        <td>Deuda Total</td>
                        <td>Vencimiento Final</td>
                        <td>Deuda Vigente</td>
                    </tr>

                    ";
        // line 903
        if ((isset($context["debesHipotecario"]) ? $context["debesHipotecario"] : $this->getContext($context, "debesHipotecario"))) {
            // line 904
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesHipotecario"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesHipotecario"]) {
                // line 905
                echo "                <tr>
                    <td><div class=\"input\">&nbsp; ";
                // line 906
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 907
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "pagomensual", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 908
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "deudatotal", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 909
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "venciminetofinal", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 910
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "duedavigente", array()), "html", null, true);
                echo "</div></td>

                </tr>

                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesHipotecario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 915
            echo "
                ";
        }
        // line 917
        echo "
                </tbody>
            </table>

            <table>
                <tr><td>Total </td></tr>
                <tr><td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
        // line 923
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : $this->getContext($context, "totalDebesHipotecario")), 0, ".", "."), "html", null, true);
        echo " </div></td></tr>
            </table>





         Tarjeta de Crédito /      ";
        // line 930
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : $this->getContext($context, "debesTarjetaCredito"))) {
            echo " SI  ";
        } else {
            echo " NO ";
        }
        // line 931
        echo "            <table class=\"tabla\">
                <thead>

                </thead>
                <tbody>
                <tr>
                    <td>Tipo de Tarjeta</td>
                    <td>Institucion</td>
                    <td>Monto Aprobado</td>
                    <td>Monto Utilizado</td>

                </tr>

                ";
        // line 944
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : $this->getContext($context, "debesTarjetaCredito"))) {
            // line 945
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesTarjetaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesTarjetaCredito"]) {
                // line 946
                echo "                <tr>
                    <td><div class=\"input\">&nbsp; ";
                // line 947
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "tipotarjeta", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 948
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 949
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoaprobado", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 950
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoutilizado", array()), "html", null, true);
                echo "</div></td>


                </tr>

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesTarjetaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 956
            echo "
                ";
        }
        // line 958
        echo "
                </tbody>
            </table>


               <table>
                <tr><td>Total </td></tr>
                <tr><td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
        // line 965
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : $this->getContext($context, "totalDebesTarjeta")), 0, ".", "."), "html", null, true);
        echo " </div></td></tr>
            </table>







 
            Línea de Crédito /    ";
        // line 975
        if ((isset($context["debesLineaCredito"]) ? $context["debesLineaCredito"] : $this->getContext($context, "debesLineaCredito"))) {
            echo " SI ";
        } else {
            echo " ";
        }
        // line 976
        echo "

            <table class=\"tabla\">
                <thead>

                </thead>
                <tbody>
                <tr>
                    <td>Institución</td>
                    <td>Monto Aprobado</td>
                    <td>Monto Utilizado</td>
                    <td>Vencimiento </td>

                </tr>

                ";
        // line 991
        if ((isset($context["debesLineaCredito"]) ? $context["debesLineaCredito"] : $this->getContext($context, "debesLineaCredito"))) {
            // line 992
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesLineaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesLineaCredito"]) {
                // line 993
                echo "                        <tr>
                            <td><div class=\"input\">&nbsp; ";
                // line 994
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "institucion", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
                // line 995
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoAprobado", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
                // line 996
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoUtilizado", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
                // line 997
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "vencimientofinal", array()), "html", null, true);
                echo "</div></td>

                        </tr>

                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesLineaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1002
            echo "
                ";
        }
        // line 1004
        echo "
                </tbody>
            </table>



               <table>
                <tr><td>Total </td></tr>
                <tr><td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
        // line 1012
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : $this->getContext($context, "totalDebesLinea")), 0, ".", "."), "html", null, true);
        echo " </div></td></tr>
            </table>





       Crédito de Consumo   ";
        // line 1019
        if ((isset($context["debesCreditoConsumo"]) ? $context["debesCreditoConsumo"] : $this->getContext($context, "debesCreditoConsumo"))) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 1020
        echo "
            <table class=\"tabla\">
                <thead>

                </thead>
                <tbody>
                <tr>
                    <td>Institución</td>
                    <td>Monto Aprobado</td>
                    <td>Monto Utilizado</td>
                    <td>Vencimiento </td>

                </tr>

                ";
        // line 1034
        if ((isset($context["debesCreditoConsumo"]) ? $context["debesCreditoConsumo"] : $this->getContext($context, "debesCreditoConsumo"))) {
            // line 1035
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesCreditoConsumo"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesCreditoConsumo"]) {
                // line 1036
                echo "                        <tr>
                            <td><div class=\"input\">&nbsp; ";
                // line 1037
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "institucion", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
                // line 1038
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "pagomensual", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
                // line 1039
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudatotal", array()), "html", null, true);
                echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
                // line 1040
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "fechaultimopago", array()), "html", null, true);
                echo "</div></td>
                             <td><div class=\"input\">&nbsp; ";
                // line 1041
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudavigente", array()), "html", null, true);
                echo "</div></td>

                        </tr>

                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesCreditoConsumo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1046
            echo "
                ";
        }
        // line 1048
        echo "
                </tbody>
            </table>


               <table>
                <tr><td>Total </td></tr>
                <tr><td><div class=\"input\" style=\"width:90px;\"> &nbsp; ";
        // line 1055
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : $this->getContext($context, "totalDebesCredito")), 0, ".", "."), "html", null, true);
        echo " </div></td></tr>
            </table>
            












            <h3>Seguros de vida</h3>
            <table class=\"tabla\">
                <tbody>
                    <tr>
                        <td>¿Tiene seguro de vida?</td>
                        <td>Institución</td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                </tbody>
            </table>
            <table class=\"tabla\" >
                <tbody>
                    <tr>
                        <td>¿Ha tenido algún juico comercial?(Explicar con detalles si es afirmativo)</td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td>¿Está involucrado en algún proceso legar?(Explicar con detalle si es afirmativo)</td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td>¿Está obligado/a a pagar pensión alimenticia?(Explicar con detalle si es afirmativo)</td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                </tbody>
            </table>
            <h3>Datos ejecutivo Colocador</h3>
            <table class=\"tabla\">
                <tbody>
                    <tr>
                        <td>RUT</td>
                        <td>ID Agente</td>
                        <td>Agencia / Canal de venta</td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                    <tr>
                        <td>Nombres</td>
                        <td>Apellido Paterno</td>
                        <td>Apellido Materno</td>
                    </tr>
                    <tr>
                        <td><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                        <td><div class=\"input\">&nbsp;</div></td>
                    </tr>
                </tbody>
            </table>


            <br/><br/><br/><br/><br/><br/><br/><br/>
            <div style=\"width: 710px;\">
                <hr style=\"color: #808080;\" size=\"8\" noshade>
                <h1 align=\"right\">SOLICITUD DE PRODUCTOS</h1>
                <hr style=\"color: #808080;\" size=\"8\" noshade>
            </div>


            <h3>Crédito de consumo:</h3>
            <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Monto de Crédito:</td>
                        <td>Plazo de Crédito </td>
                        <td>Seguros Asociados </td>
                        <td>¿Para qué utilizarás el Dinero? </td>

                    </tr>



                            <tr>
                                <td><div class=\"input\">&nbsp;   ";
        // line 1168
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "montoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 1169
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 1170
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "segurosAsociados", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 1171
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : $this->getContext($context, "creditoConsumo")), "paraque", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                            </tr>






                    </tbody>
                </table>




                <h3>Crédito Hipotecario </h3>
                <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Valor Propiedad :</td>
                        <td>Monto de Píe </td>
                        <td>Porcentaje </td>
                        <td>Monto de Crédito </td>

                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp;   ";
        // line 1206
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "valorPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1207
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "montoPie", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;   ";
        // line 1208
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "porcentaje", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;  ";
        // line 1209
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "montoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>









                    <tr>
                        <td>Plazo de Crédito </td>
                        <td>Tipo de Propiedad </td>
                        <td>Estado </td>
                        <td>¿Tu propiedad es DFL2 y nueva ? </td>

                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 1233
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;";
        // line 1234
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "tipoPropiedad", array()), "html", null, true);
            echo " ";
        }
        // line 1235
        echo "                                </div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1236
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "estadoPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1237
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "dfl2", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>









                    <tr>
                        <td>Comuna donde desea comprar</td>
                        <td>Proyecto / Inmobiliaria </td>


                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 1260
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "comunaComprar", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1261
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : $this->getContext($context, "creditoHipotecario")), "proyecto", array()), "html", null, true);
            echo "     ";
        }
        echo "</div></td>


                        </tr>







                    </tbody>
                </table>




                    Crédito Automotriz

                    <table class=\"tabla\">
                        <thead>

                        </thead>
                        <tbody>
                        <tr>
                            <td>Monto del Auto </td>
                            <td>Monto del Píe  </td>
                            <td>Plazo del Crédito </td>


                        </tr>


                                <tr>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1295
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "monto", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1296
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "pie", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1297
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "plazo", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                                </tr>



                        <tr>
                            <td>Cuándo piensas comprar tu auto </td>
                            <td>Marca de Auto  </td>
                            <td>Año del Auto </td>


                        </tr>


                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 1314
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "cuandocompras", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1315
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "marcaauto", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1316
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz"))) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : $this->getContext($context, "creditoAutomotriz")), "ano", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>




                        </tbody>
                    </table>

<table class=\"tabla\" >
<h3>
DATOS CRÉDITO CONSOLIDACION 
</h3>
<tr>
    <th>Crédito </th>
    <th>Monto </th>
    <th>Plazo </th>

  
    </tr>



    </thead>

    <tr>
    ";
        // line 1344
        $context["con"] = 0;
        // line 1345
        echo "    ";
        if ((isset($context["creditoConsolidacion"]) ? $context["creditoConsolidacion"] : $this->getContext($context, "creditoConsolidacion"))) {
            // line 1346
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["creditoConsolidacion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["creditoConsolidacion"]) {
                // line 1347
                echo "    ";
                $context["con"] = ((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")) + 1);
                // line 1348
                echo "
    ";
                // line 1349
                if (((isset($context["con"]) ? $context["con"] : $this->getContext($context, "con")) <= 1)) {
                    // line 1350
                    echo "
    <tr>   

        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1353
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1354
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1355
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo", array()), "html", null, true);
                    echo " </div> </td>
     </tr>
        
       <tr>    

        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1360
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1361
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1362
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo2", array()), "html", null, true);
                    echo " </div> </td>
</tr>
<tr>   


        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1367
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito3", array()), "html", null, true);
                    echo "  </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1368
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito3", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1369
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo3", array()), "html", null, true);
                    echo " </div></td>
       
   </tr> 
    

    ";
                }
                // line 1375
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsolidacion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1376
            echo "    ";
        }
        // line 1377
        echo "    
        </tr>

</table>


                    

















        </div>
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:plantillas:consorcio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2427 => 1377,  2424 => 1376,  2418 => 1375,  2409 => 1369,  2405 => 1368,  2401 => 1367,  2393 => 1362,  2389 => 1361,  2385 => 1360,  2377 => 1355,  2373 => 1354,  2369 => 1353,  2364 => 1350,  2362 => 1349,  2359 => 1348,  2356 => 1347,  2351 => 1346,  2348 => 1345,  2346 => 1344,  2311 => 1316,  2303 => 1315,  2295 => 1314,  2271 => 1297,  2263 => 1296,  2255 => 1295,  2214 => 1261,  2206 => 1260,  2177 => 1237,  2170 => 1236,  2167 => 1235,  2161 => 1234,  2153 => 1233,  2122 => 1209,  2114 => 1208,  2106 => 1207,  2098 => 1206,  2056 => 1171,  2048 => 1170,  2040 => 1169,  2032 => 1168,  1916 => 1055,  1907 => 1048,  1903 => 1046,  1892 => 1041,  1888 => 1040,  1884 => 1039,  1880 => 1038,  1876 => 1037,  1873 => 1036,  1868 => 1035,  1866 => 1034,  1850 => 1020,  1844 => 1019,  1834 => 1012,  1824 => 1004,  1820 => 1002,  1809 => 997,  1805 => 996,  1801 => 995,  1797 => 994,  1794 => 993,  1789 => 992,  1787 => 991,  1770 => 976,  1764 => 975,  1751 => 965,  1742 => 958,  1738 => 956,  1726 => 950,  1722 => 949,  1718 => 948,  1714 => 947,  1711 => 946,  1706 => 945,  1704 => 944,  1689 => 931,  1683 => 930,  1673 => 923,  1665 => 917,  1661 => 915,  1650 => 910,  1646 => 909,  1642 => 908,  1638 => 907,  1634 => 906,  1631 => 905,  1626 => 904,  1624 => 903,  1609 => 890,  1603 => 889,  1586 => 875,  1576 => 867,  1573 => 866,  1562 => 861,  1557 => 859,  1553 => 858,  1549 => 857,  1546 => 856,  1542 => 855,  1539 => 854,  1537 => 853,  1519 => 842,  1507 => 833,  1497 => 825,  1494 => 824,  1483 => 819,  1478 => 817,  1474 => 816,  1470 => 815,  1467 => 814,  1463 => 813,  1460 => 812,  1458 => 811,  1440 => 800,  1428 => 791,  1418 => 783,  1415 => 782,  1401 => 774,  1397 => 773,  1393 => 772,  1389 => 771,  1385 => 770,  1381 => 769,  1378 => 768,  1373 => 767,  1371 => 766,  1351 => 753,  1340 => 745,  1325 => 732,  1322 => 731,  1312 => 727,  1308 => 726,  1304 => 725,  1299 => 723,  1295 => 722,  1291 => 721,  1288 => 720,  1283 => 719,  1281 => 718,  1266 => 705,  1260 => 704,  1150 => 599,  1137 => 596,  1127 => 593,  1117 => 590,  1095 => 575,  1086 => 568,  1079 => 564,  1074 => 561,  1072 => 560,  1060 => 550,  1050 => 543,  1046 => 542,  1042 => 541,  1037 => 538,  1035 => 537,  1018 => 522,  1009 => 516,  1003 => 513,  997 => 510,  992 => 507,  990 => 506,  927 => 449,  912 => 441,  902 => 438,  892 => 435,  877 => 433,  867 => 429,  857 => 426,  847 => 423,  832 => 421,  821 => 417,  808 => 411,  799 => 409,  775 => 392,  765 => 389,  755 => 386,  751 => 384,  745 => 383,  707 => 352,  694 => 346,  684 => 343,  680 => 341,  674 => 340,  654 => 324,  647 => 322,  641 => 321,  632 => 318,  628 => 316,  618 => 315,  602 => 310,  592 => 307,  577 => 305,  558 => 301,  548 => 298,  533 => 296,  459 => 229,  451 => 228,  443 => 227,  435 => 226,  413 => 211,  395 => 200,  387 => 199,  368 => 187,  359 => 185,  342 => 175,  333 => 173,  326 => 171,  318 => 170,  292 => 150,  283 => 148,  266 => 138,  258 => 137,  248 => 134,  231 => 124,  223 => 123,  215 => 122,  199 => 113,  191 => 112,  183 => 111,  175 => 110,  157 => 98,  150 => 97,  142 => 96,  134 => 95,  120 => 86,  112 => 85,  97 => 77,  19 => 1,);
    }
}
