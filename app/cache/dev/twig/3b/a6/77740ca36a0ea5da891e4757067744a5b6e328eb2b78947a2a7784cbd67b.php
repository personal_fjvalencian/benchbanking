<?php

/* BenchAdminBundle:Default:uploadDico.html.twig */
class __TwigTemplate_3ba677740ca36a0ea5da891e4757067744a5b6e328eb2b78947a2a7784cbd67b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 4
        echo "   <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
 <link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/css/upload.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
<meta name=\"robots\" content=\"noindex\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, FOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"INDEX, NOFOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, NOFOLLOW\">
 
 <script type=\"text/javascript\" src=\"//api.filepicker.io/v1/filepicker.js\"></script>
 
 
  <script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/js/dicom.js"), "html", null, true);
        echo "\"></script>

<form id=\"uploadFilepicker\" action=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("SaveFileDicom");
        echo "\" method=\"post\"></form>
<form id=\"delFilepicker\" action=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("DelFile");
        echo "\" method=\"post\"></form>



<div class=\"container-upload\">

<div class=\"box-upload\">
<span class=\"orange\">Agregar archivos Dicom a  ";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nombre", array()), "html", null, true);
        echo " Rut / ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "rut", array()), "html", null, true);
        echo ".<span class=\"color-porcentaje\" id=\"porsuelodfijo3\"></span><span class=\"carga-ie\" id=\"sueldofijo3ie\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/img/load.gif"), "html", null, true);
        echo "\"> </span></span>



        <ul id=\"listaSueldoFijo3\" >
                          <input  type=\"hidden\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\" id=\"usuarioId\">
        
      
        ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["archivos1"]);
        foreach ($context['_seq'] as $context["_key"] => $context["archivos1"]) {
            // line 33
            echo "       

            
            <li class=\"li-upload\" id=\"itemFijo3-";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["archivos1"], "llave", array()), "html", null, true);
            echo "\">
                
            <a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["archivos1"], "url", array()), "html", null, true);
            echo "\"  target=\"_blank\"> <img src=\"img/fileicon.jpg\"> <span id=\"nombrefile\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["archivos1"], "nombreArchivo", array()), "html", null, true);
            echo "</a></span><button class=\"del_button\" id=\"del-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["archivos1"], "llave", array()), "html", null, true);
            echo "\">X</button>
            
            </li>
            
            <br>
            
            
            
           
    
        
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivos1'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
        
                                                                                                                                                                                                      
        </ul>

        <!--<input type=\"file\"  class=\"buttonupload\">
        <div class=\"classname\">nffknak</div><input />-->
        
        <div style=\"clear: both;\"></div>
        
       <div class=\"file-input-wrapper\">
        <button class=\"btn-file-input\">Agregar Archivos</button>
        <input type=\"file\" name=\"file\" id=\"AgregarSueldoFijo3\"/>
       </div>
        
        
        
      
        
     
        
        
        
        </div>
    
    </div>


";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:uploadDico.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 50,  90 => 38,  85 => 36,  80 => 33,  76 => 32,  70 => 29,  58 => 24,  48 => 17,  44 => 16,  39 => 14,  27 => 5,  22 => 4,  19 => 2,);
    }
}
