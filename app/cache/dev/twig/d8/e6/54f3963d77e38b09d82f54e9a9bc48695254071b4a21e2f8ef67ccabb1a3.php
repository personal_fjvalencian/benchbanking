<?php

/* BenchFilepickerBundle:Default:permisoCirculacion.html.twig */
class __TwigTemplate_d8e654f3963d77e38b09d82f54e9a9bc48695254071b4a21e2f8ef67ccabb1a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/js/circulacion.js"), "html", null, true);
        echo "\"></script>
<div class=\"container-upload\">

<div class=\"box-upload\">
<span class=\"orange\">Padrón o Permiso de Circulación de Automoviles. <span class=\"color-porcentaje\" id=\"porCirculacion\"></span> <span class=\"carga-ie\" id=\"circulacionpie\"><img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/img/load.gif"), "html", null, true);
        echo "\"></span></span>








    

<ul id=\"listaCirculacion\">
            
        <li>
            
            
      
        <span id=\"nombrefile\"></span>
       
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> </span>
        
        
        <!--   
        <img src=\"img/fileicon.jpg\">
        <span id=\"nombrefile\">File Name</span>
        <a href=\"#\"><span style=\"float:right; color:#F60; font-weight:bold; margin:0 10px;\" id=\"porcentajefile\">X</span></a>
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> <p>100%</p></span>-->
        
        </li>
        
       
        
        ";
        // line 38
        if ((isset($context["archivo4"]) ? $context["archivo4"] : $this->getContext($context, "archivo4"))) {
            // line 39
            echo "        
        ";
            // line 40
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["archivo4"]);
            foreach ($context['_seq'] as $context["_key"] => $context["archivo4"]) {
                // line 41
                echo "       
    
            
        <li class=\"li-upload\" id=\"itemCirculacion-";
                // line 44
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo4"], "llave", array()), "html", null, true);
                echo "\">
                
            <a href=\"";
                // line 46
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo4"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> <img src=\"img/fileicon.jpg\"> <span id=\"nombrefile\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo4"], "nombreArchivo", array()), "html", null, true);
                echo "</a></span><button class=\"del_button\" id=\"del-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo4"], "llave", array()), "html", null, true);
                echo "\">X</button>
            
            </li>
            
         
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivo4'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "        ";
        }
        echo "    
            
        
        
                                                                                                                                                                                                      
        </ul>
        <div style=\"clear: both;\"></div>
        <!--<input type=\"file\" id=\"AgregarCirculacion\" class=\"buttonupload\">-->
        <div class=\"file-input-wrapper\">
        <button class=\"btn-file-input\">Agregar Archivos</button>
        <input type=\"file\" name=\"file\" id=\"AgregarCirculacion\"/>
       </div>
        
        
        
      
        
     
        
        
        
        </div>
    
    </div>


";
    }

    public function getTemplateName()
    {
        return "BenchFilepickerBundle:Default:permisoCirculacion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 52,  81 => 46,  76 => 44,  71 => 41,  67 => 40,  64 => 39,  62 => 38,  27 => 6,  19 => 2,);
    }
}
