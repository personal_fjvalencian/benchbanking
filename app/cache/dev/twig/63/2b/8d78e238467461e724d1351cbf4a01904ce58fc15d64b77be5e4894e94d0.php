<?php

/* BenchPaginasBundle:Default:header.html.twig */
class __TwigTemplate_632b8d78e238467461e724d1351cbf4a01904ce58fc15d64b77be5e4894e94d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        $this->displayBlock('header', $context, $blocks);
    }

    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "
<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\">
</script>
<meta name=\"google-site-verification\" content=\"4sibG4Sz2wGxTyteKnT1E8uJM6vt67srq5ANMPkQ5Vg\" />

<META Http-Equiv=\"Cache-Control\" Content=\"no-cache\">
<META Http-Equiv=\"Pragma\" Content=\"no-cache\">
<META Http-Equiv=\"Expires\" Content=\"0\">


<script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchlogin/js/login.js"), "html", null, true);
        echo "\"></script>





<!-- Google Tag Manager -->
<noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-T8RRB4\"
height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T8RRB4');</script>
<!-- End Google Tag Manager -->



<header>









<!--

-->


<noscript>

</noscript>

<div class=\"wrapper-header\">
   <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" id=\"logo\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/logo.png"), "html", null, true);
        echo "\"></a>
  <nav>
<div id=\"menu-nav\">
    
    
    
    <ul>
        
        <li-nav>
        <a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("quehacemos");
        echo "\" class=\"top\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_qa2.png"), "html", null, true);
        echo "\"></a><a href=\"";
        echo $this->env->getExtension('routing')->getPath("quehacemos");
        echo "\" target=\"_parent\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_qa.png"), "html", null, true);
        echo "\"></a>
        </li-nav>
        <li-nav>
        <a href=\"";
        // line 64
        echo $this->env->getExtension('routing')->getPath("conocecreditos");
        echo "\" class=\"top\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_cdc2.png"), "html", null, true);
        echo "\"></a><a href=\"";
        echo $this->env->getExtension('routing')->getPath("conocecreditos");
        echo "\" target=\"_parent\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_cdc.png"), "html", null, true);
        echo "\"></a>
        </li-nav>
    </ul>
</div>
          
          
\t
  </nav>
 
           <div class=\"box-login\">
           <h1 style=\"font-size:20px;\">Iniciar Sesión</h1>
           <div id=\"div_carga\"><img id=\"cargador\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/loader2.gif"), "html", null, true);
        echo "\"/></div>
           <form class=\"form-login\" id=\"formlogin\" action=\"";
        // line 76
        echo $this->env->getExtension('routing')->getPath("login");
        echo "\"   method=\"post\" autocomplete=\"off\" >
           <label>
           <p>RUT</p></label>
           <input name=\"rut\" class=\"input-box-h\" id=\"rut\" type=\"text\">
        
           <label>
           <p>Clave</p>
           </label>
           
           <input name=\"contrasena\" id=\"contrasenaform\" class=\"input-box-h\" type=\"password\">
           <br>
           <input name=\"\" class=\"classname\" type=\"submit\" value=\"Acceder\" id=\"butingreso\" onClick=\"\">
           
               
          </form>
           <div  id=\"alert\" class=\"alert-form\"> </div>
          
      
          <div class=\"btn-register\"><a href=\"";
        // line 94
        echo $this->env->getExtension('routing')->getPath("registro");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_register.png"), "html", null, true);
        echo "\" width=\"45\" height=\"40\">   <span class=\"white\">¡Regístrate Ahora!</span></a></div>
           
           
           <div class=\"alert-form3\">
             <p><a href=\"";
        // line 98
        echo $this->env->getExtension('routing')->getPath("recupera");
        echo "\">¿Has olvidado tu contraseña?</a></p></div>
           
           
          
           
       </div>
       
       <div class=\"box-btnes-right\">
       
  <a href=\"";
        // line 107
        echo $this->env->getExtension('routing')->getPath("home");
        echo "\" class=\"btn-hm\" title=\"Home\"><span class=\"displace\">Home</span></a><br>

  <a href=\"https://twitter.com/benchbanking\" class=\"btn-tw\" title=\"Twitter\"><span class=\"displace\">Twitter</span></a><br>
  </div>
  <div class=\"clearfix\"></div>
  
  </div>

</header> 

<script>

\$(document).ready(function() {
    \$(\"#butingreso\").click(function(e) {

      \$(\".spinner\").css(\"display\", \"block\");





        e.preventDefault();
        var myData = 'rut=' + \$(\"#rut\").val()
                + '&contrasena=' + \$(\"#contrasenaform\").val()
                + '&token=' + \$('#token').val(); 
                ;


        var token =  \$('#token').val(); 
      

        jQuery.ajax({
            type: \"POST\", // metodo post o get 
            url: \$(\"#formlogin\").attr(\"action\"),
            dataType: \"text\", // tipo datos text o json etc 
            data: myData,
            success: function(response) {



      \$(\".spinner\").css(\"display\", \"none\");



                \$('#loading').append(' ');


                var respuesta = response;
                

                if (respuesta == 100) {
      var username=\$(\"#rut\").val();
  
{

}



                    window.location = \"";
        // line 166
        echo $this->env->getExtension('routing')->getPath("formulario");
        echo "\";


                } else {


                    \$('#alert').empty();
                    \$(\"#alert\").append(response);


                }



            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);



            },
        });
    });
});


</script>

";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:header.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  237 => 166,  175 => 107,  163 => 98,  154 => 94,  133 => 76,  129 => 75,  109 => 64,  97 => 61,  83 => 52,  42 => 14,  30 => 4,  24 => 3,  20 => 1,);
    }
}
