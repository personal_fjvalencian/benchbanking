<?php

/* BenchCreditosBundle:Default:CreditoAutomotriz.html.twig */
class __TwigTemplate_93e08e030b1c6376050575535b773a1de128ef5c760e686ac28ab43ae9399e81 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchcreditos/js/CreditoAutomotriz.js"), "html", null, true);
        echo "\"></script>
<meta name=\"robots\" content=\"noindex\">
<form class=\"form-box-qn\" style=\"padding:0 0 5% 10%;\" id=\"formCreditoAutomotriz\" action=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("guardaautomotriz");
        echo "\" method=\"POST\" autocomplete=\"off\">
  

<input type=\"hidden\" id=\"token\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">            
 <input type=\"hidden\" >
              <div class=\"col1\">

              
<label class=\"label-text\" for=\"rut\">Monto del Auto:</label>

\t\t<input type=\"text\" name=\"valor\" id=\"valorAutomotriz\" data-toggle=\"tooltip\" title=\"Ingresa monto en pesos sin puntos ni comas\" placeholder=\"\$\" class=\"input-box2\" id=\"valor\" placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" />  
\t\t
<label class=\"label-text\" for=\"rut\">Monto del Píe:</label>
\t\t<input type=\"text\" name=\"monto\" placeholder=\"\$\"  class=\"input-box2\" id=\"pieAutomotriz\" title=\"Ingresa monto en pesos sin puntos ni comas\" placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" />
        
       
                

                
        
 <label class=\"label-text2\" for=\"institucion\">Plazo del Crédito</label>
               <select class=\"data\" select=\"selected\" name=\"plazo\" id=\"plazoAutomotriz\">
<option value=\"\">Opción</option>
<option value=\"6\">  6 meses</option>
<option value=\"12\">12 meses</option>
<option value=\"18\">18 meses</option>
<option value=\"24\">24 meses</option>
<option value=\"36\">36 meses</option>
<option value=\"48\">48 meses</option>
<option value=\"60\">60 meses</option>
</select>

</div>

<div class=\"col1\">
 <label class=\"label-text2\" for=\"institucion\">Cuándo piensas comprar tu auto</label>
               <select class=\"data\" select=\"selected\" name=\"tiempo\" id=\"CuandoAutomotriz\">
<option value=\"\">Opción</option>
<option value=\"En 30 días más\">En 30 días más</option>
<option value=\"En 60 días más\">En 60 días más</option>
<option value=\"En 90 días más\">En 90 días más</option>
<option value=\"En más de 90 días\">En más de 90 días</option>
</select>                 
              
<label class=\"label-text2\" for=\"institucion\">Marca de Auto</label>
               <select class=\"data\" select=\"selected\" name=\"marca\" id=\"marcaAutomotriz\">
               <option value=\"\">Opción</option>
               <option value=\"ACADIAN\">ACADIAN</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"ALFA ROMEO\">ALFA ROMEO</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"ASIA MOTORS\">ASIA MOTORS</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"AUDI\">AUDI</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"BMW\">BMW</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"BRILLIANCE\">BRILLIANCE</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"BYD\">BYD</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"CHANGAN\">CHANGAN</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"CHERY\">CHERY</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"CHEVROLET\">CHEVROLET</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"CHRYSLER\">CHRYSLER</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"CITROEN\">CITROEN</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"DAEWOO\">DAEWOO</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"DAIHATSU\">DAIHATSU</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"DODGE\">DODGE</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"FIAT\">FIAT</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"FORD\">FORD</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"GEELY\">GEELY</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"GREAT WALL\">GREAT WALL</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"HAFEI\">HAFEI</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"HAIMA\">HAIMA</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"HONDA\">HONDA</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"HYUNDAI\">HYUNDAI</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JAC\">JAC</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JAGUAR\">JAGUAR</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JEEP\">JEEP</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"KIA\">KIA</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"LANCIA\">LANCIA</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"LAND ROVER\">LAND ROVER</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"LEXUS\">LEXUS</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"LIFAN\">LIFAN</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"MAHINDRA\">MAHINDRA</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"MAZDA\">MAZDA</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"MERCEDES BENZ\">MERCEDES BENZ</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"MG\">MG</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"MINI\">MINI</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"MITSUBISHI\">MITSUBISHI</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"NISSAN\">NISSAN</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"OPEL\">OPEL</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"PEUGEOT\">PEUGEOT</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"PORSCHE\">PORSCHE</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"PROTON\">PROTON</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"RENAULT\">RENAULT</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"ROVER\">ROVER</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"SAMSUNG\">SAMSUNG</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"SEAT\">SEAT</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"SKODA\">SKODA</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"SSANGYONG\">SSANGYONG</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"SUBARU\">SUBARU</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"SUZUKI\">SUZUKI</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"TOYOTA\">TOYOTA</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"VOLKSWAGEN\">VOLKSWAGEN</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"VOLVO\">VOLVO</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"ZOTYE\">ZOTYE</option>



\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"ZX AUTO\">ZX AUTO</option>



\t\t\t\t\t\t\t\t\t\t</select>
                                        
 <label class=\"label-text2\" for=\"institucion\">Año del Auto</label>
               <select class=\"data\" select=\"selected\" name=\"ano\" id=\"anoAutomotriz\">
<option value=\"\">Opción</option><option value=\"1997\">1997</option>
<option value=\"1998\">1998</option>
<option value=\"1999\">1999</option>
<option value=\"2000\">2000</option>
<option value=\"2001\">2001</option>
<option value=\"2002\">2002</option>
<option value=\"2003\">2003</option>
<option value=\"2004\">2004</option>
<option value=\"2005\">2005</option>
<option value=\"2006\">2006</option>
<option value=\"2007\">2007</option>
<option value=\"2008\">2008</option>
<option value=\"2009\">2009</option>
<option value=\"2010\">2010</option>
<option value=\"2011\">2011</option>
<option value=\"2012\">2012</option>
<option value=\"2013\">2013</option>
</select>
</div>
    <div class=\"clearfix\"></div>                              
<div class=\"col1\">
      <button style=\"float:left;\" id=\"ButonCreditoAuto\" class=\"classname\" value=\"Agregar\" type=\"submit\" onClick=\"\" >GUARDAR Y SIGUIENTE</button></div>     
            </form>      

            <section id=\"RespuestaCreditoAutomotriz\">
            \t

            </section> 
";
    }

    public function getTemplateName()
    {
        return "BenchCreditosBundle:Default:CreditoAutomotriz.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 8,  27 => 5,  22 => 3,  19 => 2,);
    }
}
