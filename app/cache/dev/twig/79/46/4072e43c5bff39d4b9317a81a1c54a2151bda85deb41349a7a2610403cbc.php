<?php

/* BenchFilepickerBundle:Default:carnet.html.twig */
class __TwigTemplate_79464072e43c5bff39d4b9317a81a1c54a2151bda85deb41349a7a2610403cbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<script type=\"text/javascript\" src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/js/carnet.js"), "html", null, true);
        echo "\"></script>
<div class=\"container-upload\">

<div class=\"box-upload\">
<span class=\"orange\">Fotocopia Carnet. <span class=\"color-porcentaje\" id=\"porcentajeCarnet\">  </span> <span class=\"carga-ie\" id=\"carnetie\"><img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/img/load.gif"), "html", null, true);
        echo "\"></span></span>







<span class=\"subir\" id=\"porcentajefile\"></span> </span></span> 
        <ul id=\"listaCarnet\" style=\"width:600px;height:auto;\">
            
        <li>
            
            
      
        <span id=\"nombrefile\"></span>
       
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> </span>
        
        
        <!--   
        <img src=\"img/fileicon.jpg\">
        <span id=\"nombrefile\">File Name</span>
        <a href=\"#\"><span style=\"float:right; color:#F60; font-weight:bold; margin:0 10px;\" id=\"porcentajefile\">X</span></a>
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> <p>100%</p></span>-->
        
        </li>
        
       
        
        ";
        // line 37
        if ((isset($context["archivo7"]) ? $context["archivo7"] : $this->getContext($context, "archivo7"))) {
            // line 38
            echo "        
        ";
            // line 39
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["archivo7"]);
            foreach ($context['_seq'] as $context["_key"] => $context["archivo7"]) {
                // line 40
                echo "       
    
            
            <li class=\"li-upload\" id=\"itemCarnet-";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo7"], "llave", array()), "html", null, true);
                echo "\">
                
            <a href=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo7"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> <img src=\"img/fileicon.jpg\"> <span id=\"nombrefile\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo7"], "nombreArchivo", array()), "html", null, true);
                echo "</a></span><button class=\"del_button\" id=\"del-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo7"], "llave", array()), "html", null, true);
                echo "\">X</button>
            
            </li>
            
         
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivo7'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "        ";
        }
        echo "    
            
        
        
                                                                                                                                                                                                      
        </ul>
        <div style=\"clear: both;\"></div>
        <!--<input type=\"file\" id=\"AgregarCarnet\" class=\"buttonupload\">-->
        <div class=\"file-input-wrapper\">
        <button class=\"btn-file-input\">Agregar Archivos</button>
        <input type=\"file\" name=\"file\" id=\"AgregarCarnet\"/>
       </div>
        
        
        
      
        
     
        
        
        
        </div>
    
    </div>


";
    }

    public function getTemplateName()
    {
        return "BenchFilepickerBundle:Default:carnet.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 51,  82 => 45,  77 => 43,  72 => 40,  68 => 39,  65 => 38,  63 => 37,  30 => 7,  23 => 3,  19 => 1,);
    }
}
