<?php

/* BenchPaginasBundle:Default:cotizaciones.html.twig */
class __TwigTemplate_9c14c41fddd3da0844c048e3c72328dae54066fb079ed782d2c6216994c37f43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>

<head>
<meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />
<title>Mis Cotizaciones</title>
<form id=\"cotizarForm\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("enviarCotizacion");
        echo "\"></form>
<link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/form.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<script>
    
    \$(document).ready(function() {
        
        \$('#mensajeCot').hide('fast');
        
    \$(\"#enviarCotizacion\").on(\"click\", \".checkCotizar\", function(e) {
        
        var clickedID = this.id.split('-');
        var DbNumberID = clickedID[1]; // toma el numero del array
        
  
                var myData =   'idCredito=' + DbNumberID;
                 
                var input = document.getElementById(\"adjuntar-\" + id);
                jQuery.ajax({
                    type: \"POST\",
                    url: \$(\"#cotizarForm\").attr(\"action\"),
                    dataType: \"text\",
                    data: myData,
                    
                    success: function(response) {
                        
                     \$(\"#MensajeCot-\" + DbNumberID).css(\"display\", \"block\");
               
                        
                        
                        
                        


                    },
                    error: function(xhr, ajaxOptions, thrownError) {

                        alert(thrownError);
                    }

                });
                
                
        
        console.log(DbNumberID);
        
        console.log('cotizar');
        
        
        
        
    });
      });
    
</script>    
</head>

<body>
    
      <h3 id=\"mensajeCot\">Cotización Enviada</h3>
    
    ";
        // line 67
        $context["cotizaciones"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "cotizaciones", array());
        // line 68
        echo "<div class=\"container-miscoti\">

<div class=\"datagrid\">
  <table id=\"Tablecotizacion1\">
<thead><tr>
    <th width=\"25%\">Fecha de Solicitud</th>
    <th width=\"25%\">Producto</th>
    <th width=\"25%\">Monto</th>
    <th width=\"25%\">Plazo</th></tr></thead>
<tbody>
    
 ";
        // line 79
        $context["CreditoConsumo"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditoconsumo", array());
        // line 80
        echo " ";
        $context["CreditoAutomotriz"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditoautomotriz", array());
        echo "      
 ";
        // line 81
        $context["CreditoHipotecario"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditohipotecario", array());
        echo "       
 ";
        // line 82
        $context["CreditoConsolidacion"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditoconsolidacion", array());
        echo "  
 ";
        // line 83
        $context["creditocuenta"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "creditocuenta", array());
        echo "    
    ";
        // line 84
        $context["cotizaciones"] = $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "cotizaciones", array());
        echo "        

    
    
      ";
        // line 88
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["CreditoConsumo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["CreditoConsumo"]) {
            // line 89
            echo "                   

            

                        <td ></td>
                        <td>Crédito Consumo</td>
           
                        <td>";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "montoCredito", array()), "html", null, true);
            echo "</td>
                       <td>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsumo"], "plazoCredito", array()), "html", null, true);
            echo "</td>
                     
                    </tr>
                    
     

                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['CreditoConsumo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "
                    <!-- INICIO  Credito Automotriz -->

                   ";
        // line 107
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["CreditoAutomotriz"]);
        foreach ($context['_seq'] as $context["_key"] => $context["CreditoAutomotriz"]) {
            // line 108
            echo "                    
                    
                    <tr>
                        <td></td>
                        <td>Crédito Automotriz </td>

                        <td>";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "monto", array()), "html", null, true);
            echo " </td>
                        <td>";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoAutomotriz"], "plazo", array()), "html", null, true);
            echo " </td>
                        
                    </tr>
       
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['CreditoAutomotriz'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "


                    ";
        // line 123
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["CreditoHipotecario"]);
        foreach ($context['_seq'] as $context["_key"] => $context["CreditoHipotecario"]) {
            // line 124
            echo "     
                    <tr>

                        <td></td>
                        <td>Crédito Hipotecario </td>
                        <td>";
            // line 129
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "montoCredito", array()), 0, ".", "."), "html", null, true);
            echo " </td>
                        <td>";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoHipotecario"], "plazoCredito", array()), "html", null, true);
            echo " </td>
                    </tr>    

                    <!-- FIN Credito Automotriz -->
    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['CreditoHipotecario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 136
        echo "

                    <!-- Credito consolidacion -->
                  ";
        // line 139
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["CreditoConsolidacion"]);
        foreach ($context['_seq'] as $context["_key"] => $context["CreditoConsolidacion"]) {
            // line 140
            echo "                 
                    <tr>
                        
                        
            <td></td>            
            <td>Crédito Consolidacion Deuda</td>
            <td>";
            // line 146
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "montoCredito", array()), 0, ".", "."), "html", null, true);
            echo " </td>
            <td>";
            // line 147
            echo twig_escape_filter($this->env, $this->getAttribute($context["CreditoConsolidacion"], "plazo", array()), "html", null, true);
            echo " </td>
 
  
                    </tr>
                 
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['CreditoConsolidacion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 153
        echo "                    
                    
                    ";
        // line 155
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["creditocuenta"]);
        foreach ($context['_seq'] as $context["_key"] => $context["creditocuenta"]) {
            // line 156
            echo "             
                    
                    
                    <tr>
                        
                        <td></td>
                        <td>Cuenta Corriente ";
            // line 162
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditocuenta"], "producto", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditocuenta"], "producto2", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditocuenta"], "producto3", array()), "html", null, true);
            echo " </td>
                        <td></td>
                        <td></td>
                        
                    </tr>
           
                    
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditocuenta'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 170
        echo "                    
                    

</tbody>
</table>
</div>
<br>
<br>
<div class=\"datagrid\">
  <table id=\"enviarCotizacion\">
    <thead>
      <tr>
        <th width=\"23%\">Bancos</th>
        <th width=\"70%\">Oferta</th>
        <th width=\"7%\">Contratar</th>
        </tr>
    </thead>
    <tbody>
      
        ";
        // line 189
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["cotizaciones"]);
        foreach ($context['_seq'] as $context["_key"] => $context["cotizaciones"]) {
            // line 190
            echo "         ";
            if (($this->getAttribute($context["cotizaciones"], "estado", array()) != "Cotizado")) {
                // line 191
                echo "        
        <tr  id=\"cotizacionTr-";
                // line 192
                echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "id", array()), "html", null, true);
                echo "\" >
            
        <td>";
                // line 194
                echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "banco", array()), "html", null, true);
                echo "  </td>
        
        <td>
            ";
                // line 197
                if ($this->getAttribute($context["cotizaciones"], "motivoRechazo", array())) {
                    // line 198
                    echo "            ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "motivoRechazo", array()), "html", null, true);
                    echo "
             ";
                } else {
                    // line 199
                    echo "   
            
            <a href=\"";
                    // line 201
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "Archivourl", array()), "html", null, true);
                    echo "\" target=\"_blank\"><img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/view_icon.png"), "html", null, true);
                    echo "\"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><span id=\"MensajeCot-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "id", array()), "html", null, true);
                    echo "\" style=\"display:none;width: 140px;height: auto;position:relative;float:right;margin-left: -500px;\">Cotización Enviada</span></td>
        
            ";
                }
                // line 204
                echo "            <td>
          <div class=\"squaredTwo\">
            <input type=\"hidden\" id=\"tipoCotizacion-";
                // line 206
                echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "id", array()), "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "tipo", array()), "html", null, true);
                echo "\">
            <input type=\"checkbox\" id=\"chk4-";
                // line 207
                echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "id", array()), "html", null, true);
                echo "\"  class=\"checkCotizar\"name=\"name\" value=\"SI\">
            <label for=\"chk4-";
                // line 208
                echo twig_escape_filter($this->env, $this->getAttribute($context["cotizaciones"], "id", array()), "html", null, true);
                echo "\"></label>
          </div></td>
        </tr>
        
        
        ";
            }
            // line 214
            echo " 
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cotizaciones'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 216
        echo "        
  
  
    </tbody>
  </table>
</div>


<div class=\"line-separator\"></div>



</body>

</html>

";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:cotizaciones.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  389 => 216,  382 => 214,  373 => 208,  369 => 207,  363 => 206,  359 => 204,  349 => 201,  345 => 199,  339 => 198,  337 => 197,  331 => 194,  326 => 192,  323 => 191,  320 => 190,  316 => 189,  295 => 170,  277 => 162,  269 => 156,  265 => 155,  261 => 153,  249 => 147,  245 => 146,  237 => 140,  233 => 139,  228 => 136,  216 => 130,  212 => 129,  205 => 124,  201 => 123,  196 => 120,  185 => 115,  181 => 114,  173 => 108,  169 => 107,  164 => 104,  151 => 97,  147 => 96,  138 => 89,  134 => 88,  127 => 84,  123 => 83,  119 => 82,  115 => 81,  110 => 80,  108 => 79,  95 => 68,  93 => 67,  31 => 8,  27 => 7,  19 => 1,);
    }
}
