<?php

/* BenchTienesBundle:Default:tienesBienesRaices.html.twig */
class __TwigTemplate_9df50e6540b52caef59176e2672fb2da4bad05bbe3122d470b93455ef17f4fae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchtienes/js/tieneBienesRaices.js"), "html", null, true);
        echo "\"></script>
 


<div class=\"fila\">
    
 
    
 

        <div class=\"alert-form2\"><p></p></div>  
  
        \t<div class=\"col1\">
                    
  
                    <section >
                    
     <form  class=\"form-personal\" id=\"forBienesR1\" action=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("tienesBienesRaicesSiNo");
        echo "\" method=\"POST\" autocomplete=\"off\" >


<input type=\"hidden\" id=\"token\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">
      <label class=\"label-text2\" for=\"rut\">¿Tienes alguna Propiedad?</label>

    


      ";
        // line 29
        if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "tienesBienesRaicesSiNo", array()) == "SI")) {
            // line 30
            echo "       
       <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" name=\"BienesRaicesRadio\" class=\"BienesRaicesRadio\" id=\"BienesRaicesRadio\" checked >
      No<input type=\"radio\" value=\"NO\" name=\"BienesRaicesRadio\"    class=\"BienesRaicesRadio\"id=\"BienesRaicesRadio\"   ></p>


       
      
      ";
        } else {
            // line 38
            echo "       
       
       
       
       

      <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" name=\"BienesRaicesRadio\" class=\"BienesRaicesRadio\"id=\"BienesRaicesRadio\"  >
      No<input type=\"radio\" value=\"NO\" name=\"BienesRaicesRadio\"    class=\"BienesRaicesRadio\"id=\"BienesRaicesRadio\" checked   ></p>
          
       
      

      ";
        }
        // line 51
        echo "      


    <div style=\"float:left; height:10px; width:100%;\"></div> 
    <button style=\"float:left;\" class=\"classname\" value=\"Agregar\" id=\"guardaTienesHipotecarioSiNo\">GUARDAR Y SIGUIENTE</button> 
    
    
   
              
</form>

<section id=\"RespuestaBienesRaicesSino\">



</section>



      
                        
                        
          <form class=\"form-personal\" id=\"FormBienesRaices\" action=\"";
        // line 73
        echo $this->env->getExtension('routing')->getPath("bienesraicesguarda");
        echo "\" method=\"post\"  data-validate=\"parsley\" autocomplete=\"off\" >

              <label class=\"label-text2\" for=\"institucion\">Tipo</label>
              <select name=\"THipoTipo\" id=\"THipoTipo\"class=\"data\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
              <option value=\"\">Opción</option>
              <option value=\"Casa\">Casa</option>
              <option value=\"Departamento\">Departamento</option>
              <option value=\"Terreno\">Terreno</option>
              <option value=\"Otro\">Otro</option>
                

              </select>
              <input type=\"hidden\" value=\"<?php echo \$idUser3; ?>\" name=\"ThipoId\" id=\"ThipoId\">
              <input type=\"hidden\" value=\"SI\" name=\"THipoTienes\" id=\"THipoTienes\"> 

               
               
             <label class=\"label-text2\" for=\"institucion\">Dirección</label>
              <input type=\"text\" class=\"input-box2\" name=\"THipoDireccion\" id=\"THipoDireccion\"  data-validation-engine=\"validate[required]\" data-required=\"true\" >
              
              <label class=\"label-text2\" for=\"Region\">Región</label>
              
<!--- select region -->
<select name=\"ComboHipote\" id=\"ComboHipote\" class=\"data\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
  <option value=\"\">Opción</option>
 


 <option value=\"Región de Antofagasta\">Región de Antofagasta </option>


<option value=\"Región de Arica y Parinacota\">Región de Arica y Parinacota </option>


<option value=\"Región de Atacama\">Región de Atacama </option>


<option value=\"Región de Aysén del General Carlos Ibáñez del Campo\">Región de Aysén del General Carlos Ibáñez del Campo </option>


<option value=\"Región de Coquimbo\">Región de Coquimbo </option>


<option value=\"Región de la Araucanía\">Región de la Araucanía </option>


<option value=\"Región de Los Lagos\" >Región de Los Lagos </option>


<option value=\"Región de Los Lagos\">Región de Los Ríos </option>


<option value=\"Región de Magallanes\">Región de Magallanes y la Antártica Chilena </option>


<option value=\"Región de Tarapacá\">Región de Tarapacá </option>


<option value=\"Región de Valparaiso\">Región de Valparaiso </option>


<option value=\"Región del Bío-Bío\">Región del Bío-Bío </option>



<option value=\"Región del Libertador General Bernardo O Higgins\">Región del Libertador General Bernardo O Higgins </option>


<option value=\"Región del Maule\">Región del Maule </option>


<option value=\"Región Metropolitana\">Región Metropolitana </option>




        </select>




<!--- fin select region -->

              <label class=\"label-text2\" for=\"rut\">Comuna</label>
              
              <select name=\"ComboHipoComuna4\" id=\"ComboHipoComuna4\" class=\"data\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
              <option value=\"1\">Opción</option>
            
              </select>
              
                </div>
        </form>
                <div class=\"col1\">
                    <form id=\"FormBienesRaices2\" data-validate=\"parsley\" >

         
              
              <label class=\"label-text2\" for=\"rut\">Avalúo Fiscal</label>
              <input type=\"text\" class=\"input-box2\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" placeholder=\"\$\" name=\"THipoAvaluo_fiscal\" id=\"THipoAvaluo_fiscal\"    data-validation-engine=\"validate[required]\" data-required=\"true\" >
              
              
              

              <label class=\"label-text2\" for=\"rut\">Rol</label>
              <input type=\"text\" class=\"input-box2\"  name=\"THipoRol\" id=\"THipoRol\"   data-validation-engine=\"validate[required]\" data-required=\"true\">
              

              <label class=\"label-text2\" for=\"rut\">Hipotecado</label>
            
            <select name=\"Hipotecado\" id=\"THipoHipotecado\" name=\"THipoHipotecado\" class=\"data\"   data-validation-engine=\"validate[required]\" data-required=\"true\">
              <option value=\"\">Opción</option>
              <option value=\"Si\">Si</option>
              <option value=\"No\">No</option>
            </select>
            
            



            </div>
            
        <div class=\"clearfix\"></div>
       \t  <div class=\"col2\">
          <button style=\"float:left;\" id=\"guardaHipotecario\" class=\"classname\" value=\"Agregar\"  onClick=\"\" >AGREGAR </button>

          
          <div id=\"SiguienteBienesRaices\" class=\"classname\" style=\"margin-top:2px; margin-left: 1%; position: relative;width: 80px; float: left;\" >SIGUIENTE</div>
 
 </form>
          
          
          
          
          
          
          
          
 
 <div class=\"flix-clear\"></div>

                </div> 
              
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"col2\">
              <table class=\"table table-striped\" id=\"respuestahipotecario\">
                <thead style=\"background:#F60\">
                  <tr>
                    <th><p style=\"text-align:center; color:#FFF;\">Tipo</p></th>
                    <th><p style=\"text-align:center;color:#FFF;\">Dirección</p></th>
                    <th><p style=\"text-align:center;color:#FFF;\">Region</p></th>
                    <th><p style=\"text-align:center;color:#FFF;\">Comuna</p></th>
                    <th><p style=\"text-align:center;color:#FFF;\">Avaluó fiscal</p></th>
                    <th><p style=\"text-align:center;color:#FFF;\">Rol</p></th>
                    <th><p style=\"text-align:center;color:#FFF;\">Hipotecado</p></th>
                    <th><p style=\"text-align:center;color:#FFF;\">Eliminar</p></th>


                  </tr>
                </thead>

                

  
                <tbody style=\"background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;\">
                
    ";
        // line 240
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesHipotecario"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesHipotecario"]) {
            echo "          

  <tr id=\"item_";
            // line 242
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "id", array()), "html", null, true);
            echo "\">
    
                    <td><p style=\"text-align:center; color: #666;\">";
            // line 244
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "tipo", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 245
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "tipo", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 246
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "region", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 247
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "comuna", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 248
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "avaluo", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 249
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "rol", array()), "html", null, true);
            echo "</p></td>
                     <td><p style=\"text-align:center;color:#666;\">";
            // line 250
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "hipotecado", array()), "html", null, true);
            echo "</p></td>
                     <td><p style=\"text-align:center;color:#666;\"> <a href=\"#\" class=\"del_button\" id=\"del-";
            // line 251
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "id", array()), "html", null, true);
            echo "\">eliminar</a> </p> </td>



                  </tr>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesHipotecario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 257
        echo "
                


                
               </tbody >
              </table>
            </div>
            
            

\t
\t\t</div>
\t
        

           
          
            


</form>
<form id=\"formeleminarBienesRaices\" action=\"";
        // line 279
        echo $this->env->getExtension('routing')->getPath("delbienesraices");
        echo "\" method=\"post\">

</form>

   
";
    }

    public function getTemplateName()
    {
        return "BenchTienesBundle:Default:tienesBienesRaices.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  355 => 279,  331 => 257,  319 => 251,  315 => 250,  311 => 249,  307 => 248,  303 => 247,  299 => 246,  295 => 245,  291 => 244,  286 => 242,  279 => 240,  109 => 73,  85 => 51,  70 => 38,  60 => 30,  58 => 29,  49 => 23,  43 => 20,  23 => 3,  19 => 1,);
    }
}
