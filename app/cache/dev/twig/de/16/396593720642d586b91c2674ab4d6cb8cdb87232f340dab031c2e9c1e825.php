<?php

/* BenchCreditosBundle:Default:CreditoConsumo.html.twig */
class __TwigTemplate_de16396593720642d586b91c2674ab4d6cb8cdb87232f340dab031c2e9c1e825 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "

<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchcreditos/js/CreditoConsumo.js"), "html", null, true);
        echo "\"></script>
 <form class=\"form-box-qn\" style=\"padding:0 0 5% 10%;\" action=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("guardaconsumo");
        echo "\" method=\"POST\" id=\"formCreditoConsumo\" autocomplete=\"off\">


<input type=\"hidden\" id=\"token\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">
<div class=\"col1\">
<label class=\"label-text\" for=\"rut\">Monto de Cr&eacute;dito:</label>

    ";
        // line 12
        if (($this->getAttribute((isset($context["credito"]) ? $context["credito"] : $this->getContext($context, "credito")), "tipo", array()) == "Crédito Consumo")) {
            // line 13
            echo "

        <input type=\"text\" name=\"valor\" class=\"input-box2\" value=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : $this->getContext($context, "credito")), "montoCredito", array()), "html", null, true);
            echo "\"id=\"montoCredito\" title=\"Ingresar monto en pesos sin puntos ni comas\" placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" />

    ";
        } else {
            // line 18
            echo "
     <input type=\"text\" name=\"valor\" class=\"input-box2\" value=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : $this->getContext($context, "credito")), "montoCredito", array()), "html", null, true);
            echo "\"id=\"montoCredito\" title=\"Ingresar monto en pesos sin puntos ni comas\" placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" />


    ";
        }
        // line 23
        echo "



  <label class=\"label-text\" for=\"nombres\">Plazo de Cr&eacute;dito</label>
<select class=\"data\" select=\"selected\" name=\"plazo\" id=\"plazoCreditoConsumo\"  data-validation-engine=\"validate[required]\" >
   ";
        // line 29
        if (($this->getAttribute((isset($context["credito"]) ? $context["credito"] : $this->getContext($context, "credito")), "tipo", array()) == "Crédito Consumo")) {
            // line 30
            echo "
      <option value=\"credito.plazo\"> ";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : $this->getContext($context, "credito")), "plazo", array()), "html", null, true);
            echo "</option>


    ";
        }
        // line 35
        echo "    <option value=\"\">Opción</option>
    <option value=\"6\">  6 meses</option>
    <option value=\"12\">12 meses</option>
    <option value=\"18\">18 meses</option>
    <option value=\"24\">24 meses</option>
    <option value=\"36\">36 meses</option>
    <option value=\"48\">48 meses</option>
    <option value=\"60\">60 meses</option>



</select>
        
              <label class=\"label-text2\" for=\"rut\">Seguros Asociados</label>
              
               <p class=\"rad\">
                   <input type=\"checkbox\" id=\"chk1\" name=\"seguro1\" value=\"Degravamen\">
                    <label for=\"chk1\">Desgravamen</label>
                   <input type=\"checkbox\" id=\"chk2\" name=\"seguro1\"value=\"Cesantía\">
                    <label for=\"chk2\">Cesantía</label>
                   
                   <!--Degravamen<input type=\"checkbox\" value=\"Degravamen\" name=\"seguro1\">
         Cesantía<input type=\"checkbox\" value=\"Cesantía\" name=\"seguro1\">-->
                   
                   </p> 
         
         </div>
         
         <div class=\"col1\">
         <label class=\"label-text\" for=\"nombres\">¿Para qué utilizarás el Dinero?</label>
         <textarea class=\"textarea2\" id=\"paraqueConsumo\" title=\"Ingresa un breve comentario, por ejemplo: Vacaciones, Renovar tu hogar, etc.\"  cols=\"\" rows=\"\" name=\"paraque\"></textarea>
         </div>        
        <div class=\"clearfix\"></div>
    <div class=\"col1\">

      
      <button style=\"margin-top:20px; float:left;\" id=\"guardaCreditoConsumo\" class=\"classname\" value=\"Agregar\" type=\"submit\" onClick=\"\">GUARDAR Y SIGUIENTE</button></div>     
            
    <section id=\"respuestaconsumo2\"></section>
     </form>";
    }

    public function getTemplateName()
    {
        return "BenchCreditosBundle:Default:CreditoConsumo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 35,  75 => 31,  72 => 30,  70 => 29,  62 => 23,  55 => 19,  52 => 18,  46 => 15,  42 => 13,  40 => 12,  33 => 8,  27 => 5,  23 => 4,  19 => 2,);
    }
}
