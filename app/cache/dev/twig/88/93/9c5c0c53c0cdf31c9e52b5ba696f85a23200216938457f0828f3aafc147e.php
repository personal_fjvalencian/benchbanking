<?php

/* BenchFilepickerBundle:Default:declaracionAnual.html.twig */
class __TwigTemplate_88939c5c0c53c0cdf31c9e52b5ba696f85a23200216938457f0828f3aafc147e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<script type=\"text/javascript\" src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/js/anual.js"), "html", null, true);
        echo "\"></script>
<div class=\"container-upload\">

<div class=\"box-upload\">
<span class=\"orange\">Declaración Anual de los Últimos dos Años. <span  class=\"color-porcentaje\" id=\"porcentajeAnual\"  type=\"filepicker\" data-fp-maxsize=\"2000\" ></span><span class=\"carga-ie\" id=\"declaracionAnualie\"><img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/img/load.gif"), "html", null, true);
        echo "\"></span> </span>











        <ul id=\"listaAnual\" style=\"width:600px;height:auto;\">
            
        <li>
            
            
      
        <span id=\"nombrefile\"></span>
       
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> </span>
        
        
        <!--   
        <img src=\"img/fileicon.jpg\">
        <span id=\"nombrefile\">File Name</span>
        <a href=\"#\"><span style=\"float:right; color:#F60; font-weight:bold; margin:0 10px;\" id=\"porcentajefile\">X</span></a>
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> <p>100%</p></span>-->
        
        </li>
        
       
        
        ";
        // line 40
        if ((isset($context["archivo6"]) ? $context["archivo6"] : $this->getContext($context, "archivo6"))) {
            // line 41
            echo "        
        ";
            // line 42
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["archivo6"]);
            foreach ($context['_seq'] as $context["_key"] => $context["archivo6"]) {
                // line 43
                echo "       
    
            
            <li class=\"li-upload\" id=\"itemAnual-";
                // line 46
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo6"], "llave", array()), "html", null, true);
                echo "\">
                
            <a href=\"";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo6"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> <img src=\"img/fileicon.jpg\"> <span id=\"nombrefile\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo6"], "nombreArchivo", array()), "html", null, true);
                echo "</a></span><button class=\"del_button\" id=\"del-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo6"], "llave", array()), "html", null, true);
                echo "\">X</button>
            
            </li>
            
         
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivo6'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "        ";
        }
        echo "    
            
        
        
                                                                                                                                                                                                      
        </ul>
        <div style=\"clear: both;\"></div>
   
        <div class=\"file-input-wrapper\">
        <button class=\"btn-file-input\">Agregar Archivos</button>
        <input type=\"file\"  id=\"AgregarAnual\"/>
       </div>
        
        
        
      
        
     
        
        
        
        </div>
    
    </div>


";
    }

    public function getTemplateName()
    {
        return "BenchFilepickerBundle:Default:declaracionAnual.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 54,  84 => 48,  79 => 46,  74 => 43,  70 => 42,  67 => 41,  65 => 40,  29 => 7,  22 => 3,  19 => 2,);
    }
}
