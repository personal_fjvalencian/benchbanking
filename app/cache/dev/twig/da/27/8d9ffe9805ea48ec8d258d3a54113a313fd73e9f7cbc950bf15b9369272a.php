<?php

/* BenchDebesBundle:Default:debesHipotecario.html.twig */
class __TwigTemplate_da278d9ffe9805ea48ec8d258d3a54113a313fd73e9f7cbc950bf15b9369272a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchdebes/js/debeshipotecario22.js"), "html", null, true);
        echo "\"></script>

    <div class=\"fila\">
           <!--<h1 class=\"doc\"><span class=\"orange\">Hipotecario</span></h1>-->
          <div class=\"col1\">
     

       <form  id=\"DebesHipotecario\" action=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("debesBienesRaicesSiNo");
        echo "\" method=\"POST\" autocomplete=\"off\"  >

       

<input type=\"hidden\" id=\"token\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">
       <label class=\"label-text2\" for=\"rut\">¿Debes algún Credito Hipotecario?</label>
       <input type=\"hidden\" value=\"\" name=\"debeshipoid\" id=\"debeshipoid\">
      
    ";
        // line 18
        if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "debesHipotecarioSiNo", array()) == "SI")) {
            // line 19
            echo "       
         <p class=\"rad\">Si<input type=\"radio\" value=\"SI\"  class=\"DebesHipotecarioRadio\" name=\"DebesHipotecarioRadio\" id=\"DebesHipotecarioRadio\"   checked>
         No<input type=\"radio\" value=\"NO\" name=\"DebesHipotecarioRadio\"   id=\"DebesHipotecarioRadio\" class=\"DebesHipotecarioRadio\"></p> 
      
       
";
        } else {
            // line 25
            echo "          
          
          
          <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"DebesHipotecarioRadio\" name=\"DebesHipotecarioRadio\" id=\"DebesHipotecarioRadio\" >
         No<input type=\"radio\" value=\"NO\" name=\"DebesHipotecarioRadio\" id=\"DebesHipotecarioRadio\" class=\"DebesHipotecarioRadio\" checked ></p> 
   
          
          ";
        }
        // line 33
        echo "


    

    <div style=\"float:left; height:10px; width:100%;\"></div>
    <button style=\"float:left;\" class=\"classname\" value=\"Agregar\" id=\"debesHipotecario1\" type=\"submit\">GUARDAR Y SIGUIENTE</button> 

               </form>

               <div id=\"RespuestaDebesHipotecario\">

               </div>
              




               <form class=\"form-personal\" id=\"debesHipotecario2\" action=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("hipotecarioguarda");
        echo "\" method=\"post\" >
                     
              
             
               
              <label class=\"label-text2\" for=\"institucion\">Instituci&oacute;n</label>
              <input type=\"text\" class=\"input-box2\" autofocus name=\"institucion\" id=\"institucion\"   data-validation-engine=\"validate[required]\" data-required=\"true\">
               <input type=\"hidden\" class=\"input-box2\" value=\"\" autofocus name=\"id\" id=\"id\">
               
               
             <label class=\"label-text2\" for=\"institucion\">Pago Mensual</label>
              <input type=\"text\"class=\"input-box2\" name=\"pagoM\" placeholder=\"\$\" id=\"pagoM\"  onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  data-validation-engine=\"validate[required]\" data-required=\"true\" >
              
              <label class=\"label-text2\" for=\"rut\">Deuda Total</label>
              <input type=\"text\" class=\"input-box2\" name=\"deudaT\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  placeholder=\"\$\" id=\"deudaT\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
              </div>
              </form>
              <div class=\"col1\">
              <form id=\"debesHipotecario3\">
              <label class=\"label-text2\" for=\"rut\">Vencimiento Final</label>
              <input type=\"text\" class=\"input-box2\" id=\"vencimientoF\" name=\"vencimientoF\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
              
              <label class=\"label-text2\" for=\"rut\">Deuda Vigente</label>
             

              <input type=\"text\" class=\"input-box2\"  placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  name=\"deudaV\" id=\"deudaV\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
              
              </form>
              
            </div>
            
            <div class=\"clearfix\"></div>
            <div class=\"col2\"><button id=\"debesHipotecario\" class=\"classname\" value=\"Agregar\" style=\" float:left;margin-top:10px;\" >AGREGAR </button>

             <div id=\"SiguienteDebesHipotecario\" class=\"classname\" style=\"margin-top:10px; margin-left: 1%; position: relative;width: 80px; float: left;\" >SIGUIENTE</div>
            </div>
            <div class=\"flix-clear\"></div>
    
            <div class=\"clearfix\"></div>
            <br>

           

     

      </form>
         
            <div class=\"col2\">
              <table class=\"table table-striped\" id=\"respuestaDebesHipo\">
                <thead style=\"background:#F60\">
                  <tr>
                    <td><p style=\"text-align:center; color:#FFF;\">Institución</p></td>
                    <td><p style=\"text-align:center;color:#FFF;\">Pago Mensual</p></td>
                    <td><p style=\"text-align:center;color:#FFF;\">Deuda Total</p></td>
                    <td><p style=\"text-align:center;color:#FFF;\">Vencimiento Final</p></td>
                    <td><p style=\"text-align:center;color:#FFF;\">Deuda Vigente</p></td>
                      <td><p style=\"text-align:center;color:#FFF;\">Eliminar</p></td>
                  </tr>
                </thead>
               <tbody style=\"background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;\"> 
                   ";
        // line 111
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["debesHipotecario"]);
        foreach ($context['_seq'] as $context["_key"] => $context["debesHipotecario"]) {
            echo "  
      
                  <tr id=\"itemDebesHipotecario_";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "id", array()), "html", null, true);
            echo "\">

                    <td> <p style=\"text-align:center; color: #666;\">";
            // line 115
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "institucion", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "pagomensual", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "deudatotal", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "venciminetofinal", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "duedavigente", array()), "html", null, true);
            echo "</p></td>
                    
                    <td><p style=\"text-align:center;color:#666;\"> <a href=\"#\" class=\"del_button\" id=\"del-";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "id", array()), "html", null, true);
            echo "\">eliminar</a> </p> </td>
                  </tr>

            
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesHipotecario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "                  
                      </tbody>
                
                
              </table>
            </div>
            
            

  
    </div>
  
        
    

          

            



</form>

<form id=\"deldebesHipotecario\" action=\"";
        // line 149
        echo $this->env->getExtension('routing')->getPath("deldebesHipotecario");
        echo "\"></form>";
    }

    public function getTemplateName()
    {
        return "BenchDebesBundle:Default:debesHipotecario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 149,  193 => 126,  182 => 121,  177 => 119,  173 => 118,  169 => 117,  165 => 116,  161 => 115,  156 => 113,  149 => 111,  86 => 51,  66 => 33,  56 => 25,  48 => 19,  46 => 18,  39 => 14,  32 => 10,  22 => 3,  19 => 2,);
    }
}
