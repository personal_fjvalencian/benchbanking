<?php

/* BenchFilepickerBundle:Default:declaraciones3.html.twig */
class __TwigTemplate_dfa8f314158495fc82b1bd9f7f109038062f73e426febfddd580548aad338adc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<script type=\"text/javascript\" src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/js/impuesto3.js"), "html", null, true);
        echo "\"></script>
<div class=\"container-upload\">

<div class=\"box-upload\">
<span class=\"orange\">3 últimas declaraciones de impuesto.<span class=\"color-porcentaje\" id=\"porcentajeImpuesto3\"> </span> <span class=\"carga-ie\" id=\"declaracion3ie\"><img src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/img/load.gif"), "html", null, true);
        echo "\"></span></span>








        <ul id=\"listaImpuesto3\" style=\"width:600px;height:auto;\">
            
        <li>
            
            
      
        <span id=\"nombrefile\"></span>
       
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> </span>
        
        
        <!--   
        <img src=\"img/fileicon.jpg\">
        <span id=\"nombrefile\">File Name</span>
        <a href=\"#\"><span style=\"float:right; color:#F60; font-weight:bold; margin:0 10px;\" id=\"porcentajefile\">X</span></a>
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> <p>100%</p></span>-->
        
        </li>
        
       
        
        ";
        // line 37
        if ((isset($context["archivo5"]) ? $context["archivo5"] : $this->getContext($context, "archivo5"))) {
            // line 38
            echo "        
        ";
            // line 39
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["archivo5"]);
            foreach ($context['_seq'] as $context["_key"] => $context["archivo5"]) {
                // line 40
                echo "       
    
            
        <li class=\"li-upload\" id=\"itemImpuesto3-";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo5"], "llave", array()), "html", null, true);
                echo "\">
       
            <a href=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo5"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> <img src=\"img/fileicon.jpg\"> <span id=\"nombrefile\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo5"], "nombreArchivo", array()), "html", null, true);
                echo "</a></span><button class=\"del_button\" id=\"del-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo5"], "llave", array()), "html", null, true);
                echo "\">X</button>
            
            </li>
            
         
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivo5'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "        ";
        }
        echo "    
            
        
        
                                                                                                                                                                                                      
        </ul>
        <div style=\"clear: both;\"></div>
        <!--<input type=\"file\" id=\"AgregarImpuesto3\" class=\"buttonupload\">-->
        <div class=\"file-input-wrapper\">
        <button class=\"btn-file-input\">Agregar Archivos</button>
        <input type=\"file\" name=\"file\" id=\"AgregarImpuesto3\"/>
       </div>
        
        
        
      
        
     
        
        
        
        </div>
    
    </div>


";
    }

    public function getTemplateName()
    {
        return "BenchFilepickerBundle:Default:declaraciones3.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 51,  81 => 45,  76 => 43,  71 => 40,  67 => 39,  64 => 38,  62 => 37,  29 => 7,  22 => 3,  19 => 2,);
    }
}
