<?php

/* BenchFilepickerBundle:Default:SueldoFijo3.html.twig */
class __TwigTemplate_c34fcbe394223058946765419fcf2c94ad0e021432fd3e0e1d58ce53d673427d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
 <link href=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/css/upload.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  

 
 <script type=\"text/javascript\" src=\"//api.filepicker.io/v1/filepicker.js\"></script>
 
 
  <script type=\"text/javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/js/sueldofijo3.js"), "html", null, true);
        echo "\"></script>

<form id=\"uploadFilepicker\" action=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("SaveFile");
        echo "\" method=\"post\"></form>
<form id=\"delFilepicker\" action=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("DelFile");
        echo "\" method=\"post\"></form>



<div class=\"container-upload\">

<div class=\"box-upload\">
<span class=\"orange\">Sueldo Fijo 3 últimas líquidaciones de sueldo.<span class=\"color-porcentaje\" id=\"porsuelodfijo3\"></span><span class=\"carga-ie\" id=\"sueldofijo3ie\"><img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/img/load.gif"), "html", null, true);
        echo "\"> </span></span>



        <ul id=\"listaSueldoFijo3\" >
            
        
        
        ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["archivos1"]);
        foreach ($context['_seq'] as $context["_key"] => $context["archivos1"]) {
            // line 28
            echo "       
    
            
            <li class=\"li-upload\" id=\"itemFijo3-";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["archivos1"], "llave", array()), "html", null, true);
            echo "\">
                
            <a href=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["archivos1"], "url", array()), "html", null, true);
            echo "\"   target='_blank'  > <img src=\"img/fileicon.jpg\"> <span id=\"nombrefile\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["archivos1"], "nombreArchivo", array()), "html", null, true);
            echo "</a></span><button class=\"del_button\" id=\"del-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["archivos1"], "llave", array()), "html", null, true);
            echo "\">X</button>
            
            </li>
            
            <br>
            
            
            
           
    
        
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivos1'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "        
        
                                                                                                                                                                                                      
        </ul>

        <!--<input type=\"file\"  class=\"buttonupload\">
        <div class=\"classname\">nffknak</div><input />-->
        
        <div style=\"clear: both;\"></div>
        
       <div class=\"file-input-wrapper\">
        <button class=\"btn-file-input\">Agregar Archivos</button>
        <input type=\"file\" name=\"file\" id=\"AgregarSueldoFijo3\"/>
       </div>
        
        
        
      
        
     
        
        
        
        </div>
    
    </div>


";
    }

    public function getTemplateName()
    {
        return "BenchFilepickerBundle:Default:SueldoFijo3.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 45,  75 => 33,  70 => 31,  65 => 28,  61 => 27,  50 => 19,  40 => 12,  36 => 11,  31 => 9,  22 => 3,  19 => 2,);
    }
}
