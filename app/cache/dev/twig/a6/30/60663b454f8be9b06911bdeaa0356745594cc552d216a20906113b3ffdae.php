<?php

/* BenchTienesBundle:Default:tienesAutomoviles.html.twig */
class __TwigTemplate_a63060663b454f8be9b06911bdeaa0356745594cc552d216a20906113b3ffdae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "

<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchtienes/js/tienesAutomoviles.js"), "html", null, true);
        echo "\"></script>




 <div class=\"fila\">
    
  
          <div class=\"col1\">
            

  
  
 

        <form  action=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("tienesAutomovilesSiNo");
        echo "\" id=\"automovilesSiNo33\" method=\"post\" autocomplete=\"off\">



<input type=\"hidden\" id=\"token\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "html", null, true);
        echo "\">
           
            <label class=\"label-text2\" for=\"rut\">¿Tienes algún Automóvil?</label>

          
             ";
        // line 28
        if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "tienesAutomovilesSiNo", array()) == "SI")) {
            // line 29
            echo "            
                   
      <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"AutosRadio\" name=\"AutosRadio\" id=\"AutosRadio\" checked >
         No<input type=\"radio\" value=\"NO\"  class=\"AutosRadio\" name=\"AutosRadio\" id=\"AutosRadio\" ></p>
          
     
             ";
        } else {
            // line 36
            echo "               <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"AutosRadio\" name=\"AutosRadio\" id=\"AutosRadio\">
         No<input type=\"radio\" value=\"NO\"  class=\"AutosRadio\" name=\"AutosRadio\" id=\"AutosRadio\" checked ></p>
          
            ";
        }
        // line 40
        echo "
        <div style=\"float:left; height:10px; width:100%;\"></div>
       <button style=\"float:left;\" id=\"guardaAutomoviles1\" class=\"classname\" value=\"Agregar\" type=\"submit\">GUARDAR Y SIGUIENTE</button>

            </form>

            <div id=\"RespuestaAutos\">
            
            </div>
              
              

     <form id=\"formTienesAutos\" class=\"form-personal\" action=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("automovilesguarda");
        echo "\"  method=\"post\">
              <label class=\"tipo\" for=\"\">Tipo</label>


              <select class=\"data\" name=\"TienesATipo\" id=\"TienesATipo\" data-validation-engine=\"validate[required]\" data-required=\"true\">

        <option value=\"\">Opción</option>
        <option value=\"Auto\">Auto</option>
        <option value=\"Camioneta\">Camioneta</option>
          <option value=\"Camion\">Camión</option>
        <option value=\"Moto\">Moto</option>
        <option value=\"Otro\">Otro</option>
        </select>
               
              <label class=\"label-text2\" for=\"institucion\">Marca</label>
               <input type=\"text\" class=\"input-box2\" name=\"TienesAMarca\" id=\"TienesAMarca\" data-validation-engine=\"validate[required]\" data-required=\"true\">
          

             <label class=\"label-text2\" for=\"institucion\">Modelo</label>
               <input type=\"text\"class=\"input-box2\" name=\"TienesAModelo\" id=\"TienesAModelo\"  data-validation-engine=\"validate[required]\" data-required=\"true\"   >
              
              <label class=\"label-text2\" for=\"rut\">Patente</label>
              <input type=\"text\" class=\"input-box2\" name=\"TienesAPatente\" id=\"TienesAPatente\"  data-validation-engine=\"validate[required]\" data-required=\"true\" >
              
              </div>
     </form>
              <div class=\"col1\">
                <form id=\"formTienesAutos2\"> 
              <label class=\"label-text2\" for=\"rut\">Año del Auto</label>
              <input type=\"text\" class=\"input-box2\" id=\"TienesAAno\" name=\"TienesAAno\"   data-validation-engine=\"validate[required]\" data-required=\"true\">
              
              <label class=\"label-text2\" for=\"rut\">Valor Comercial</label>
              <input type=\"text\" class=\"input-box2\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" placeholder=\"\$\" name=\"TienesAAvaluo\" id=\"TienesAAvaluo\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
              
              <label class=\"label-text2\" for=\"rut\">Prendado</label>
              
               <p class=\"rad\">Si<input type=\"radio\" value=\"si\" id=\"TienesAPrendadoAuto\" class=\"TienesAPrendadoAuto\" name=\"TienesAPrendadoAuto\"  data-required=\"true\">
         No<input type=\"radio\" value=\"no\" class=\"TienesAPrendadoAuto\" name=\"TienesAPrendadoAuto\"  id=\"TienesAPrendadoAuto\" data-required=\"true\" ></p>
              
              
            </div>
            <div class=\"clearfix\"></div>
              </form>    
          <div class=\"col2\"><button style=\"float:left;margin-top:10px;\" id=\"guardaAutomoviles\" class=\"classname\" value=\"Agregar\" type=\"submit\">AGREGAR</button>
          <div id=\"SiguienteAutomoviles\" class=\"classname\" style=\"margin-top:10px; margin-left: 1%; position: relative;width: 80px; float: left;\" >SIGUIENTE</div>
          </div> 
          
             

    <div class=\"flix-clear\"></div>
 
            <br>
            <div class=\"col2\">
              <table class=\"table table-striped\" id=\"respuestaAutos\">
          <thead style=\"background:#F60\">
                  <tr>
          <td><p style=\"text-align:center; color:#FFF;\">Tipo</p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Marca</p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Modelo</p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Nº Pantente</p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Valor Comercial</p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Año del Auto</p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Prendado </p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Eliminar</p></td>
          </tr>
        </thead>
      <tbody style=\"background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;\">
          
    ";
        // line 120
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesAutomoviles"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesAutomoviles"]) {
            echo "  
          <tr id=\"itemTienesAutos_";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "id", array()), "html", null, true);
            echo "\">
          <td><p style=\"text-align:center;color:#666;\">";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "tipo", array()), "html", null, true);
            echo "</p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "marcaauto", array()), "html", null, true);
            echo "</p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "modelo", array()), "html", null, true);
            echo "</p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "patente", array()), "html", null, true);
            echo "</p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "valorcomercial", array()), "html", null, true);
            echo "</p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 127
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "anoauto", array()), "html", null, true);
            echo "</p></td>  
          <td><p style=\"text-align:center;color:#666;\">";
            // line 128
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "prendado", array()), "html", null, true);
            echo "</p></td> 
        
          
          
          
  

          <td><p style=\"text-align:center;color:#666;\"> <a href=\"#\" class=\"del_button\" id=\"del-";
            // line 135
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "id", array()), "html", null, true);
            echo "\">eliminar</a> </p> </td>
          </tr>
   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAutomoviles'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 138
        echo "        </tbody>
                         
       
             </table>
              
              
              
              
              
              
              
             
              
              
             
            
            </div>
            
            

  
    </div>
  
        

        
            



</form>
              
<form id=\"delautomoviles\" action=\"";
        // line 170
        echo $this->env->getExtension('routing')->getPath("delautomoviles");
        echo "\" method=\"post\"></form>";
    }

    public function getTemplateName()
    {
        return "BenchTienesBundle:Default:tienesAutomoviles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 170,  211 => 138,  202 => 135,  192 => 128,  188 => 127,  184 => 126,  180 => 125,  176 => 124,  172 => 123,  168 => 122,  164 => 121,  158 => 120,  87 => 52,  73 => 40,  67 => 36,  58 => 29,  56 => 28,  48 => 23,  41 => 19,  23 => 4,  19 => 2,);
    }
}
