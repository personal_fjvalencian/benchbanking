<?php

/* BenchAdminBundle:Default:form2.html.twig */
class __TwigTemplate_45bbd5baefe2d8ee069a2dde162a649a06d54e4872da986b85d3e0094c45ed36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"
        \"http://www.w3.org/TR/html4/loose.dtd\">
<html>
    <head>
    </head>
  <style>
    
        
\t* { margin: 0; padding: 0; }
\t
\tbody {font-family: Verdana, Arial; font-size: 12px; line-height: 18px; background-image: url(img/bg_icons.jpg.jpg);
background-position: center top;
background-repeat: no-repeat;}

header{
\twidth:626px;
\theight:160px;
\tmargin:0 auto;
        
      
}


  p{
            font-family: Verdana, Arial;
            
            }
            
\ta { text-decoration: none; font-family: Verdana, Arial; }
\t.container{font-family: Verdana, Arial; margin: 20px auto; width: 700px; height: auto; background-color:#0FF; -webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px; background-color:#EEEEEE;}
\th3 { font-family: Verdana, Arial; padding:2px 0 2px 10px; font-size: 14px; background-color:#F60; color:#FFF; -moz-border-radius: 2px;
\t-webkit-border-radius: 2px;
\tborder-radius: 2px;
\tmargin:5px 0;}
\th4 { font-family: Verdana, Arial; padding:4px 0 4px 10px; font-size: 16px; background-color: #666; color:#FFF; -moz-border-radius: 2px;
\t-webkit-border-radius: 2px;
\tborder-radius: 2px;
\tmargin:5px 0;}
\t
\t
\t.cols {
            font-family: Verdana, Arial;
column-count: 3;
-moz-column-count:3; /* Firefox */
-webkit-column-count:3; /* Safari and Chrome */

}
\t
\t#contactform {
\t
\twidth: 660px;
\tpadding: 5px 16px;
\toverflow:auto;
\t
\t/*border: 1px solid #cccccc;
\t-moz-border-radius: 7px;
\t-webkit-border-radius: 7px;
\tborder-radius: 7px;\t
\t
\t-moz-box-shadow: 2px 2px 2px #cccccc;
\t-webkit-box-shadow: 2px 2px 2px #cccccc;
\tbox-shadow: 2px 2px 2px #cccccc;*/
\t
\t}
\t
\t.field{margin-bottom:2px;}
\t
\tlabel {
\t\tcolor:#666;
\tfont-family: Verdana, Geneva, sans-serif; 
\ttext-shadow: 1px 1px 1px #ccc;
\tdisplay: block; 
\tfloat: left; 
\tfont-weight: normal; 
\tmargin-right:10px; 
\ttext-align: left; 
\twidth: 200px; 
\tline-height: 15px; 
\tfont-size: 12px; 
\t}
\t
\t.label-adress {
\t\tcolor:#666;
\tfont-family: Verdana, Geneva, sans-serif; 
\ttext-shadow: 1px 1px 1px #ccc;
\tdisplay: block; 
\tfloat: left; 
\tfont-weight: normal; 
\tmargin-right:10px; 
\ttext-align: left; 
\twidth: auto; 
\tline-height: 15px; 
\tfont-size: 12px; 
\t}
\t
\t.input-adress{
\t-webkit-border-radius: 4px;
\t-moz-border-radius: 4px;
\tborder-radius: 4px;
\tfont-family: Arial, Verdana; 
\tfont-size: 11px; 
\tpadding: 1px;
\tmargin:1px 0;
\tborder: 1px solid #b9bdc1; 
\twidth: 99%;
\theight:16px; 
\tcolor: #797979;\t
\t}
\t
\t.input{
\t-webkit-border-radius: 4px;
\t-moz-border-radius: 4px;
\tborder-radius: 4px;
\tfont-family: Arial, Verdana; 
\tfont-size: 11px; 
\tpadding: 1px;
\tmargin:1px 0;
\tborder: 1px solid #b9bdc1; 
\twidth: 97%;
\theight:16px; 
\tcolor: #797979;\t
\t}
\t
\t.input:focus{
\tbackground-color:#E7E8E7;
\tborder:1px solid #F95B00;\t
\t}
\t
\t.textarea {
\theight:150px;\t
\t}
\t
\t.hint{
\tdisplay:none;
\tleft:190px;
\t}
\t
\t.field:hover .hint {  
\tposition: absolute;
\tdisplay: block;  
\tmargin: -30px 0 0 455px;
\tcolor: #FFFFFF;
\tpadding: 7px 10px;
\tbackground: rgba(0, 0, 0, 0.6);
\t
\t-moz-border-radius: 4px;
\t-webkit-border-radius: 4px;
\tborder-radius: 4px;\t
\t}
\t
\t.button{
\tfloat: right;
\tmargin:10px 55px 10px 0;
\tfont-weight: bold;
\tline-height: 1;
\tpadding: 6px 10px;
\tcursor:pointer;   
\tcolor: #fff;
\t
\ttext-align: center;
\ttext-shadow: 0 -1px 1px #64799e;
\t
\t/* Background gradient */
\tbackground: #a5b8da;
\tbackground: -moz-linear-gradient(top, #a5b8da 0%, #7089b3 100%);
\tbackground: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#a5b8da), to(#7089b3));
\t
\t/* Border style */
  \tborder: 1px solid #5c6f91;  
\t-moz-border-radius: 10px;
\t-webkit-border-radius: 10px;
\tborder-radius: 10px;
  
\t/* Box shadow */
\t-moz-box-shadow: inset 0 1px 0 0 #aec3e5;
\t-webkit-box-shadow: inset 0 1px 0 0 #aec3e5;
\tbox-shadow: inset 0 1px 0 0 #aec3e5;
\t
\t}
\t
\t.button:hover {
\tbackground: #848FB2;
    cursor: pointer;
\t}
\t
\t.box-patri{
\t\twidth:100%;
\t\theight:auto;
\t\ttext-align:left;
\t\tcolor: #666;
\t\tfont-size:14px;
\t\tborder-top: 1px solid #999;
\t\tpadding:5px 0;
\t}
\t
\t.box-patri img{
\t\tvertical-align: bottom;
\t}
\t
\t/*----- TABLE CSS -------*/
\t
\t
\t.datagrid table { border-collapse: collapse; text-align: left; width: 100%; }
\t.datagrid {font: normal 12px/150% Verdana, Arial, Helvetica, sans-serif; background: #fff;  border: 1px solid #B9BDC1; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; /*width: 95%;*/}
\t.datagrid table td, .datagrid table th { padding: 3px 10px; }
\t.datagrid table thead th {background-color:#B9BDC1; color:#FFFFFF; font-size: 12px; font-weight: normal; border-left: 1px solid #A3A3A3; }
\t.datagrid table thead th:first-child { border: none; }
\t.datagrid table tbody td { color: #7D7D7D; border-left: 1px solid #DBDBDB;font-size: 12px;font-weight: normal; }
\t.datagrid table tbody .alt td { background: #EBEBEB; color: #7D7D7D; }
\t.datagrid table tbody td:first-child { border-left: none; }
\t.datagrid table tbody tr:last-child td { border-bottom: none; }

     </style>
<body>
    
 




<img src='http://www.mediadiv.cl/bench/img/header.png' width=\"626\" height=\"160\" style=\"position: relative; width: 626px; height:160px;margin-left: 40px;  \">







<div class=\"container\">

</br>
<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
<h3 style=\"\">Datos Personales</h3>

<div class=\"cols\">

<div class=\"field\">
\t<label for=\"name\">Rut:</label>
  \t";
        // line 241
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "rut", array()), "html", null, true);
        echo "
</div>

<div class=\"field\">
\t<label for=\"name\">Fecha Registro:</label>
        \t<p>";
        // line 246
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "fechaingreso", array()), "Y-m-d H:i:s"), "html", null, true);
        echo "</p>
  \t
</div>

<div class=\"field\">
\t<label for=\"name\">Nombre:</label>
  \t\t<p>&nbsp;";
        // line 252
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nombre", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Apellidos:</label>
        
        <p>&nbsp;";
        // line 258
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellido", array()), "html", null, true);
        echo "  ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "apellidoseg", array()), "html", null, true);
        echo "</p>
        
        


</div>

<div class=\"field\">
\t<label for=\"name\">fecha de Nacimiento:</label>
  \t<p>&nbsp;";
        // line 267
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "fechanacimiento", array()), "html", null, true);
        echo "
</div>

<div class=\"field\">
\t<label for=\"name\">Sexo:</label>
  \t<p>&nbsp;";
        // line 272
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "sexo", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Nacionalidad:</label>
  \t<p>&nbsp;";
        // line 277
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "nacionalidad", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Estado Civil:</label>
  \t<p>&nbsp;";
        // line 282
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "estadocivil", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Nivel Educación:</label>
  \t<p>&nbsp;";
        // line 287
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "niveleducacion", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Profesión:</label>
       <p>&nbsp;";
        // line 292
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "profesion", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Universidad:</label>
  \t<p>&nbsp;";
        // line 297
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "universidad", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"email\">Email:</label>
  \t<p>&nbsp;";
        // line 302
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "email", array()), "html", null, true);
        echo "</p>
\t<!--<p class=\"hint\">Enter your email.</p>-->
</div>

<div class=\"field\">
\t<label for=\"name\">Celular:</label>
  \t<p>&nbsp;";
        // line 308
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "telefono", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Tipo de Casa:</label>
  \t<p>&nbsp;";
        // line 313
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "tipocasa", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">N° Dependientes:</label>
  \t<p>&nbsp;";
        // line 318
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "ndependientes", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Arriendo / Dividendo:</label>
  \t<p>&nbsp;";
        // line 323
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "montoarriendo", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Región:</label>
  \t<p>&nbsp;";
        // line 328
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "region", array()), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Comuna:</label>
  \t<p>&nbsp;";
        // line 333
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "comuna", array()), "html", null, true);
        echo "</p>
</div>
    
    
    <div class=\"field\">
\t<label for=\"name\">Dirección:</label>
  \t<p>&nbsp;";
        // line 339
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : $this->getContext($context, "usuario")), "direccion", array()), "html", null, true);
        echo "</p>
</div>






    
    </br>
    </br>
</br>



<h3>Datos Laborales</h3>


<div class=\"cols\">
<div class=\"field\">
\t<label for=\"name\">Situación Laboral:</label>
  \t<p>
&nbsp;
";
        // line 362
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "situacionlaboral", array()), "html", null, true);
            echo " ";
        }
        echo " </p>
</div>

<div class=\"field\">
\t<label for=\"name\">Empleador:</label>
  \t<p>&nbsp;";
        // line 367
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "empleadorempresa", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Industria:</label>
  \t<p>&nbsp;";
        // line 372
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "industria", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">fecha de ingreso:</label>
  \t<p>&nbsp;";
        // line 377
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "fechaingreso", array()), "html", null, true);
        }
        echo "</p>
        
</div>

<div class=\"field\">
\t<label for=\"name\">Cargo Actual:</label>
  \t
        <p>&nbsp;";
        // line 384
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "cargoactual", array()), "html", null, true);
        }
        echo "</p>
        
</div>



<div class=\"field\">
\t<label for=\"name\">Rut Empresa:</label>
  \t<p>&nbsp;";
        // line 392
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "rutempresa", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Región:</label>
  \t<p>&nbsp;";
        // line 397
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "region", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Comuna:</label>
  \t<p>&nbsp;";
        // line 402
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "comuna", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Ciudad:</label>
  \t<p>&nbsp;";
        // line 407
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "ciudad", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Teléfono:</label>
  \t<p>&nbsp;";
        // line 412
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "telefono", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"email\">Tipo de Sueldo:</label>
  \t<p>&nbsp;";
        // line 417
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "tiposueldo", array()), "html", null, true);
        }
        echo "</p>
\t<!--<p class=\"hint\">Enter your email.</p>-->
</div>

<div class=\"field\">
\t<label for=\"name\">Sueldo Fijo:</label>
      <p>&nbsp;";
        // line 423
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "sueldofijo", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Sueldo Variable:</label>
  \t<p>&nbsp;";
        // line 428
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "rentavariable", array()), "html", null, true);
        }
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Sueldo Líquido:</label>
  \t<p>&nbsp;";
        // line 433
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "independientesueldoliquido", array()), "html", null, true);
        }
        echo "</p>
</div>


<div class=\"field\">
\t<label for=\"name\">Dirección:</label>
  \t<p>&nbsp;";
        // line 439
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes"))) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : $this->getContext($context, "antecedentes")), "direccion", array()), "html", null, true);
        }
        echo "</p>
</div>
</div>





</br>
<h3>Datos Conyuge</h3>
<div class=\"cols\">
<div class=\"field\">
\t<label for=\"name\">Nombres:</label>
  \t<p>&nbsp;";
        // line 452
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "nombre", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Apellidos:</label>
        <p>&nbsp;";
        // line 457
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "apellido", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Rut:</label>
          <p>&nbsp;";
        // line 462
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "rut", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Fecha de Nacimiento:</label>
  \t<p>&nbsp;";
        // line 467
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "fechanacimiento", array()), "html", null, true);
        echo " &nbsp</p>
        
</div>

<div class=\"field\">
\t<label for=\"name\">Nacionalidad:</label>
  \t<p>&nbsp;";
        // line 473
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "nacionalidad", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Nivel de Educación:</label>
  \t<p>&nbsp;";
        // line 478
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "niveleducacion", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Profesión:</label>
  \t<p>&nbsp; ";
        // line 483
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "profesion", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Universidad:</label>
     <p>&nbsp;";
        // line 488
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "universidad", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Actividad:</label>
  \t<p>&nbsp;";
        // line 493
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "actividad", array()), "html", null, true);
        echo " &nbsp</p>
        
</div>

<div class=\"field\">
\t<label for=\"name\">Cargo Actual:</label>
  \t<p>&nbsp;";
        // line 499
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "cargoactual", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Antiguedad Laboral:</label>
  \t<p>&nbsp;";
        // line 504
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "antiguedadlaboral", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Renta Líquida:</label>
  \t<p>&nbsp;";
        // line 509
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "rentaliquidad", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Empresa:</label>
  \t<p>&nbsp;";
        // line 514
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "empresa", array()), "html", null, true);
        echo " &nbsp</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Teléfono:</label>
  \t<p>&nbsp;";
        // line 519
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : $this->getContext($context, "conyuge")), "telefono", array()), "html", null, true);
        echo " &nbsp</p>
        
</div>




</div>

</br>



</form>

</div>


</div>


<div class=\"container\">

<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
</br>
<h3>Resumen Activos</h3>

<div class=\"cols\">

<div class=\"field\">
\t<label for=\"name\">Bienes Raices:</label>
        
  \t<p>&nbsp;";
        // line 551
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : $this->getContext($context, "totalHipotecario")), 0, ".", "."), "html", null, true);
        echo "</p>
        
        
</div>

<div class=\"field\">
\t<label for=\"name\">Ahorro e inversión:</label>
  \t<p>&nbsp;";
        // line 558
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : $this->getContext($context, "totalAhorroInversion")), 0, ".", "."), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Automóviles:</label>
  \t<p>&nbsp;";
        // line 563
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : $this->getContext($context, "totalAutomoviles")), 0, ".", "."), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Sociedades:</label>
  \t<p> &nbsp;";
        // line 568
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalSociedades"]) ? $context["totalSociedades"] : $this->getContext($context, "totalSociedades")), 0, ".", "."), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\" style=\"color:#F60;\">TOTAL ACTIVOS: &nbsp; ";
        // line 572
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["activos"]) ? $context["activos"] : $this->getContext($context, "activos")), 0, ".", "."), "html", null, true);
        echo "</label>
  
</div>





</div>



</br>

</br>

</br>

</br>
</br>

</br>

</br>


<h3>Resumen Pasivos</h3>

<div class=\"cols\">

<div class=\"field\">
\t<label for=\"name\">Créditos Hipotecarios</label>
  \t<p>&nbsp;";
        // line 604
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : $this->getContext($context, "totalDebesHipotecario")), 0, ".", "."), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Línea de Crédito:</label>
  \t<p>&nbsp;";
        // line 609
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : $this->getContext($context, "totalDebesLinea")), 0, ".", "."), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\">Tarjeta de Crédito:</label>
        
  \t<p>&nbsp;";
        // line 615
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : $this->getContext($context, "totalDebesTarjeta")), 0, ".", "."), "html", null, true);
        echo "</p>
        
        
</div>

<div class=\"field\">
\t<label for=\"name\">Créditos de Consumo:</label>
  \t  <p>&nbsp;";
        // line 622
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : $this->getContext($context, "totalDebesCredito")), 0, ".", "."), "html", null, true);
        echo "</p>
</div>

<div class=\"field\">
\t<label for=\"name\" style=\"color:#F60;\">TOTAL PASIVOS: &nbsp;";
        // line 626
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebes"]) ? $context["totalDebes"] : $this->getContext($context, "totalDebes")), 0, ".", "."), "html", null, true);
        echo "</label>
  \t
      
</div>



</div>

</br>

<div class=\"box-patri\"><img src=\"img/star.png\" width=\"23\" height=\"24\"> PATRIMONIO:  &nbsp;";
        // line 637
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["patrimonio"]) ? $context["patrimonio"] : $this->getContext($context, "patrimonio")), 0, ".", "."), "html", null, true);
        echo "</div>


</br>

</form>
</div>


<br>
<br>
<br><br>
<br>
<br>
<br><br>
<br>

<br><br>
<br>
<br><br>
<br>





<div class=\"container\">

<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
</br>
<h4>Activos</h4>
<h3>Bienes Raices</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th width=\"10%\">Tipo</th>
    <th>Dirección</th>
    <th>Región</th>
    <th>Comuna</th>
    <th>Avalúo Fiscal</th>
    <th>Rol</th>
    <th>Hipotecario</th></tr></thead>

<tbody>
 ";
        // line 680
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesHipotecario"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesHipotecario"]) {
            echo "  
  <tr>
      <td>&nbsp;";
            // line 682
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "tipo", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 683
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "direccion", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 684
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "region", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 685
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "comuna", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 686
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "avaluo", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;{tienesHipotecario.rol}}</td>
      <td>&nbsp;";
            // line 688
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHipotecario"], "hipotecado", array()), "html", null, true);
            echo "</td>

 </tr>
 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesHipotecario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 692
        echo "</tbody>
</table></div>
</br>

<h3>Ahorro de Inversión</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Tipo</th>
    <th>Institución</th>
    <th>Valor Comercial</th>
    <th>Prendado</th></tr>

</thead>
<tbody>
    
    ";
        // line 707
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesAhorroInversion"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesAhorroInversion"]) {
            // line 708
            echo "    <tr>
        
        <td>&nbsp;";
            // line 710
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "tipo", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 711
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "institucion", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 712
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "valorcomercial", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 713
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "prendado", array()), "html", null, true);
            echo "</td>
    
    
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAhorroInversion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 718
        echo "

</tbody>

</table></div>

</br>

<h3>Automóviles</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Tipo</th>
    <th>Marca</th>
    <th>Modelo</th>
    <th>Patente</th>
    <th>Año de Auto</th>
    <th>Valor Comercial</th>
    <th>Prendado</th>
</tr>
</thead>
<tbody>
    ";
        // line 739
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesAutomoviles"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesAutomoviles"]) {
            // line 740
            echo "        
<tr>
    
    <td>&nbsp;";
            // line 743
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "tipo", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 744
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "marcaauto", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 745
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "modelo", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 746
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "patente", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 747
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "anoauto", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 748
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "valorcomercial", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 749
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "prendado", array()), "html", null, true);
            echo "</td>
    

</tr>

   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAutomoviles'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 755
        echo "

</tbody>
</table></div>
</br>

<h3>Participación en Sociedades</h3>
<div class=\"datagrid\"><table>
<thead>
    
    <tr>
    <th>Nombre Sociedad</th>
    <th>Rut</th>
    <th>Porcentaje (%)</th>
    <th>Valor Participativo</th></tr></thead>
<tbody>
";
        // line 771
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesSociedades"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesSociedades"]) {
            // line 772
            echo "    <tr>

<td>&nbsp;";
            // line 774
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "nombre", array()), "html", null, true);
            echo "</td>
<td>&nbsp;";
            // line 775
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "rut", array()), "html", null, true);
            echo "</td>
<td>&nbsp;";
            // line 776
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "porcentaje", array()), "html", null, true);
            echo "</td>
<td>&nbsp;";
            // line 777
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "valor", array()), "html", null, true);
            echo "</td>   
    </tr>
    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesSociedades'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 781
        echo "
</tbody>
</table></div>


</br>

</form>
</div>



<div class=\"container\">

<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
</br>
<h4>Pasivos</h4>
<h3>Hipotecario</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Institución</th>
    <th>Pago Mensual</th>
    <th>Deuda Total</th>
    <th>Vencimiento Final</th>
    <th>Deuda Vigente</th></tr></thead>
<tbody>
";
        // line 807
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["debesHipotecario"]);
        foreach ($context['_seq'] as $context["_key"] => $context["debesHipotecario"]) {
            // line 808
            echo "<tr>
    <td>&nbsp;";
            // line 809
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "institucion", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 810
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "pagomensual", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 811
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "deudatotal", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 812
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "venciminetofinal", array()), "html", null, true);
            echo "</td>
    <td>&nbsp;";
            // line 813
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "duedavigente", array()), "html", null, true);
            echo "</td>

</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesHipotecario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 818
        echo "</tbody>
</table></div>
</br>

<h3>Tarjeta de Crédito</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Tipo de Tarjeta</th>
    <th>Institución</th>
    <th>Monto Aprobado</th>
    <th>Monto Utilizado</th>
   </tr></thead>
<tbody>
    
    ";
        // line 832
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["debesTarjetaCredito"]);
        foreach ($context['_seq'] as $context["_key"] => $context["debesTarjetaCredito"]) {
            // line 833
            echo "    <tr>
        <td>&nbsp; ";
            // line 834
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "tipotarjeta", array()), "html", null, true);
            echo " </td>
        <td> &nbsp;";
            // line 835
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "institucion", array()), "html", null, true);
            echo " </td>
        <td> &nbsp;";
            // line 836
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoaprobado", array()), "html", null, true);
            echo " </td>
        <td> &nbsp;";
            // line 837
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoutilizado", array()), "html", null, true);
            echo " </td>
    
    </tr>
    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesTarjetaCredito'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 842
        echo "</tbody>
</table></div>
</br>

<h3> Línea de Crédito</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Institución</th>
    <th>Monto Aprobado</th>
    <th>Monto Utilizado</th>
    <th>Vencimiento</th>
    </tr></thead>

<tbody>

 ";
        // line 857
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["debesLineaCredito"]);
        foreach ($context['_seq'] as $context["_key"] => $context["debesLineaCredito"]) {
            echo "   
 <tr>
       <td>&nbsp;";
            // line 859
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "institucion", array()), "html", null, true);
            echo "</td> 
       <td>&nbsp;";
            // line 860
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoAprobado", array()), "html", null, true);
            echo "</td> 
       <td>&nbsp;";
            // line 861
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoUtilizado", array()), "html", null, true);
            echo "</td> 
       <td>&nbsp;";
            // line 862
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "vencimientofinal", array()), "html", null, true);
            echo "</td> 
        
</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesLineaCredito'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 867
        echo "
</tbody>
</table></div>
</br>

<h3>Créditos de Consumo</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Institución</th>
    <th>Pago Mensual</th>
    <th>Deuda Total</th>
    <th>Vencimiento Final</th>
    <th>Deuda</th></tr>
</thead>

<tbody>
    
    ";
        // line 884
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["debesCreditoConsumo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["debesCreditoConsumo"]) {
            // line 885
            echo " <tr>
     <td>&nbsp;";
            // line 886
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "institucion", array()), "html", null, true);
            echo "</td>
     <td>&nbsp;";
            // line 887
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "pagomensual", array()), "html", null, true);
            echo "</td>
     <td>&nbsp;";
            // line 888
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudatotal", array()), "html", null, true);
            echo "</td>
     <td>&nbsp;";
            // line 889
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "fechaultimopago", array()), "html", null, true);
            echo "</td>
     <td>&nbsp;";
            // line 890
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudavigente", array()), "html", null, true);
            echo "</td>
 
 
 </tr>
 
 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesCreditoConsumo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 896
        echo "</tbody>
</table></div>

</br>

</form>
</div>


<div class=\"container\">

<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
</br>
<h4>Pedidos</h4>
<h3>Créditos de Consumo</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Monto Crédito</th>
    <th>Plazo de Crédito</th>
    <th>Seguro asociado</th>
    <th>¿Para qué utilizarás el dinero?</th>
    <th>Fecha del
      pedido</th><br>
      </th></tr></thead>
<tbody>
    ";
        // line 921
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["creditoConsumo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["creditoConsumo"]) {
            // line 922
            echo "  <tr>
      <td>&nbsp;";
            // line 923
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsumo"], "montoCredito", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 924
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsumo"], "plazoCredito", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 925
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsumo"], "segurosAsociados", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 926
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsumo"], "paraque", array()), "html", null, true);
            echo "</td>
      <td></td>
      
      
  </tr>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsumo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 932
        echo "</tbody>
</table></div>

</br>

<h3>Automotriz</h3>
<div class=\"datagrid\"><table>
<thead>
    
    <tr>
    <th>Monto </th>
    <th>Píe:</th>
    <th>Plazo </th>
    <th>¿Cuándo piensas comprar tu auto?</th>
    <th>Marca</th>
    <th>Año</th>
    <th>Fecha</th>
    </tr></thead>

<tbody>
    
  ";
        // line 953
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["creditoAutomotriz"]);
        foreach ($context['_seq'] as $context["_key"] => $context["creditoAutomotriz"]) {
            // line 954
            echo "    
    <tr>
        <td>&nbsp;";
            // line 956
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoAutomotriz"], "monto", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 957
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoAutomotriz"], "pie", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 958
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoAutomotriz"], "plazo", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 959
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoAutomotriz"], "cuandocompras", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 960
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoAutomotriz"], "marcaauto", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 961
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoAutomotriz"], "ano", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 962
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["creditoAutomotriz"], "fecha", array()), "Y-m-d"), "html", null, true);
            echo "</td>
    </tr>
    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoAutomotriz'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 965
        echo " 

</tbody>
</table></div>
</br>

<h3>Hipotecario</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Valor Propiedad</th>
    <th>Monto Píe</th>
    <th>Porcentaje</th>
    <th>Monto de Crédito</th>
    <th>Plazo de Crédito</th>
    <th>Tipo de propiedad</th>
    <th>dfl2</th>
    <th>Fecha de Pedido</th>
    <th>Comuna</th>
   <th>Proyecto</th>
    </tr></thead>
<tbody>
    
    ";
        // line 987
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["creditoHipotecario"]);
        foreach ($context['_seq'] as $context["_key"] => $context["creditoHipotecario"]) {
            // line 988
            echo " 
  <tr>
      <td>&nbsp;";
            // line 990
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "valorPropiedad", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 991
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "montoPie", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 992
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "porcentaje", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 993
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "montoCredito", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 994
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "plazoCredito", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 995
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "tipoPropiedad", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 996
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "dfl2", array()), "html", null, true);
            echo "</td>
      
      <td>&nbsp;";
            // line 998
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "fecha", array()), "Y-m-d"), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 999
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "comunaComprar", array()), "html", null, true);
            echo "</td>
      <td>&nbsp;";
            // line 1000
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoHipotecario"], "proyecto", array()), "html", null, true);
            echo "</td>

      

  </tr>
  
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoHipotecario'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1007
        echo "</tbody>
</table></div>

</br>

<h3>Consolidación de deuda / Refinanciamiento</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Crédito 1</th>
    <th>Monto 1</th>
    <th>Plazo 1</th>
    <th>Crédito 2</th>
    <th>Monto 2</th>
    <th>Plazo 2</th>
    <th>Crédito 3</th>
    <th>Monto 3</th>
    <th>Plazo 3</th>
    <th>Fecha</th>
    </tr>
    </thead>
<tbody>
    
    ";
        // line 1029
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["creditoConsolidacion"]);
        foreach ($context['_seq'] as $context["_key"] => $context["creditoConsolidacion"]) {
            // line 1030
            echo "    <tr>
        
        <td>&nbsp;";
            // line 1032
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1033
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1034
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo", array()), "html", null, true);
            echo "</td>
        
        
        <td>&nbsp;";
            // line 1037
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito2", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1038
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito2", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1039
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo2", array()), "html", null, true);
            echo "</td>
        
        <td>&nbsp;";
            // line 1041
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito3", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1042
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito3", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1043
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo3", array()), "html", null, true);
            echo "</td>
    
             <td>&nbsp;";
            // line 1045
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "fecha", array()), "Y-m-d"), "html", null, true);
            echo "</td>
    
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsolidacion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1049
        echo "    
    
</tbody>
</table></div>

</br>

<h3>Pedidos / Cuenta Corriente</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Producto 1</th>
    <th>Producto 2</th>
    <th>Producto 3</th>
    <th>Fecha del pedido</th>
    </tr></thead>
<tbody>
    ";
        // line 1065
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["creditoCuenta"]);
        foreach ($context['_seq'] as $context["_key"] => $context["creditoCuenta"]) {
            // line 1066
            echo "    <tr>
        <td>&nbsp;";
            // line 1067
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoCuenta"], "producto", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1068
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoCuenta"], "producto2", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1069
            echo twig_escape_filter($this->env, $this->getAttribute($context["creditoCuenta"], "producto3", array()), "html", null, true);
            echo "</td>
        <td>&nbsp;";
            // line 1070
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["creditoCuenta"], "fecha", array()), "Y-m-d"), "html", null, true);
            echo "</td>
        
    
    </tr>
    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoCuenta'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1076
        echo "</tbody>

</table></div>

</br>


    
 
    <h3>Documentos</h3>
    
    <div class=\"datagrid\"><table>
        <thead>
        
        <th> Fijo 3 ultimas </th>
\t
        </thead>
        
        <tbody>

        \t
        ";
        // line 1097
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["filepickerArchivo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["filepickerArchivo"]) {
            // line 1098
            echo "        ";
            if (($this->getAttribute($context["filepickerArchivo"], "tipoArchivo", array()) == "fijo3ultimasliquidaciones")) {
                // line 1099
                echo "  \t\t<tr>
  \t\t<td><a href=\"";
                // line 1100
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "nombreArchivo", array()), "html", null, true);
                echo "</a></td>
         </tr>\t

       ";
            }
            // line 1103
            echo "\t
\t   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filepickerArchivo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1104
        echo " 
          
                
        </tbody>
    
    
    </table></div>
    
    
  
    
    
    
    <h3>Variable 6 ultimas.</h3>
    <div class=\"datagrid\"><table>
        <thead>
            
            
        
        <th> Variable 6 ultimas. </th>
        </thead>
        
        <tbody>
            
            \t
        ";
        // line 1129
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["filepickerArchivo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["filepickerArchivo"]) {
            // line 1130
            echo "        ";
            if (($this->getAttribute($context["filepickerArchivo"], "tipoArchivo", array()) == "variable6ultimasliquidaciones")) {
                // line 1131
                echo "  \t\t<tr>
  \t\t<td><a href=\"";
                // line 1132
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "nombreArchivo", array()), "html", null, true);
                echo "</a></td>
         </tr>\t

       ";
            }
            // line 1135
            echo "\t
\t   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filepickerArchivo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1136
        echo " 
          
                
                
        </tbody>
    
    
    </table></div>
    
    
    
    
    <h3>Cartola Afp.</h3>
    <div class=\"datagrid\"><table>
        <thead>
    
  
        </thead>
        
        <tbody>
                  \t
        ";
        // line 1157
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["filepickerArchivo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["filepickerArchivo"]) {
            // line 1158
            echo "        ";
            if (($this->getAttribute($context["filepickerArchivo"], "tipoArchivo", array()) == "cartolaAfp")) {
                // line 1159
                echo "  \t\t<tr>
  \t\t<td><a href=\"";
                // line 1160
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "nombreArchivo", array()), "html", null, true);
                echo "</a></td>
         </tr>\t

       ";
            }
            // line 1163
            echo "\t
\t   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filepickerArchivo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1164
        echo " 
          
                
                
        </tbody>
    
    
    </table></div>
    
    
    
     <h3>Permiso Circulacion.</h3>
    <div class=\"datagrid\">
        <table>
        <thead>
    
    
      <th> Permiso Circulacion. </th>
        </thead>
        
        <tbody>
         
        </tbody>
    
    
    </table>
        </div>
     
     
     
     <h3>Ultimas 3 declaraciones.</h3>
    <div class=\"datagrid\">
        <table>
        <thead>
    
    
     
        </thead>
        
        <tbody>
        
        ";
        // line 1205
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["filepickerArchivo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["filepickerArchivo"]) {
            // line 1206
            echo "        ";
            if (($this->getAttribute($context["filepickerArchivo"], "tipoArchivo", array()) == "impuestos3")) {
                // line 1207
                echo "  \t\t<tr>
  \t\t<td><a href=\"";
                // line 1208
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "nombreArchivo", array()), "html", null, true);
                echo "</a></td>
         </tr>\t

       ";
            }
            // line 1211
            echo "\t
\t   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filepickerArchivo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1212
        echo "   
                
        </tbody>
    
    
    </table>
        </div>
     
     
     
     
     
      <h3>Declaración Anual.</h3>
    <div class=\"datagrid\">
        <table>
        <thead>
    

        </thead>
        
        <tbody>
           ";
        // line 1233
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["filepickerArchivo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["filepickerArchivo"]) {
            // line 1234
            echo "        ";
            if (($this->getAttribute($context["filepickerArchivo"], "tipoArchivo", array()) == "Anual")) {
                // line 1235
                echo "  \t\t<tr>
  \t\t<td><a href=\"";
                // line 1236
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "nombreArchivo", array()), "html", null, true);
                echo "</a></td>
         </tr>\t

       ";
            }
            // line 1239
            echo "\t
\t   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filepickerArchivo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1240
        echo "     
            
                
        </tbody>
    
    
    </table>
        </div>
      
      
      
      
      
      <h3>Carnet.</h3>
    <div class=\"datagrid\">
        <table>
        <thead>
    
    
      <th> Carnet. </th>
        </thead>
        
        <tbody>
           
                
                
                ";
        // line 1266
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["filepickerArchivo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["filepickerArchivo"]) {
            // line 1267
            echo "        ";
            if (($this->getAttribute($context["filepickerArchivo"], "tipoArchivo", array()) == "Carnet")) {
                // line 1268
                echo "  \t\t<tr>
  \t\t<td><a href=\"";
                // line 1269
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["filepickerArchivo"], "nombreArchivo", array()), "html", null, true);
                echo "</a></td>
         </tr>\t

       ";
            }
            // line 1272
            echo "\t
\t   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filepickerArchivo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1273
        echo "        
        </tbody>
    
    
    </table>
        </div>
     
     
     
     
     
     
     
     
      
     
     
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
  

</form>


</body>

</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:form2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1981 => 1273,  1974 => 1272,  1965 => 1269,  1962 => 1268,  1959 => 1267,  1955 => 1266,  1927 => 1240,  1920 => 1239,  1911 => 1236,  1908 => 1235,  1905 => 1234,  1901 => 1233,  1878 => 1212,  1871 => 1211,  1862 => 1208,  1859 => 1207,  1856 => 1206,  1852 => 1205,  1809 => 1164,  1802 => 1163,  1793 => 1160,  1790 => 1159,  1787 => 1158,  1783 => 1157,  1760 => 1136,  1753 => 1135,  1744 => 1132,  1741 => 1131,  1738 => 1130,  1734 => 1129,  1707 => 1104,  1700 => 1103,  1691 => 1100,  1688 => 1099,  1685 => 1098,  1681 => 1097,  1658 => 1076,  1646 => 1070,  1642 => 1069,  1638 => 1068,  1634 => 1067,  1631 => 1066,  1627 => 1065,  1609 => 1049,  1599 => 1045,  1594 => 1043,  1590 => 1042,  1586 => 1041,  1581 => 1039,  1577 => 1038,  1573 => 1037,  1567 => 1034,  1563 => 1033,  1559 => 1032,  1555 => 1030,  1551 => 1029,  1527 => 1007,  1514 => 1000,  1510 => 999,  1506 => 998,  1501 => 996,  1497 => 995,  1493 => 994,  1489 => 993,  1485 => 992,  1481 => 991,  1477 => 990,  1473 => 988,  1469 => 987,  1445 => 965,  1435 => 962,  1431 => 961,  1427 => 960,  1423 => 959,  1419 => 958,  1415 => 957,  1411 => 956,  1407 => 954,  1403 => 953,  1380 => 932,  1368 => 926,  1364 => 925,  1360 => 924,  1356 => 923,  1353 => 922,  1349 => 921,  1322 => 896,  1310 => 890,  1306 => 889,  1302 => 888,  1298 => 887,  1294 => 886,  1291 => 885,  1287 => 884,  1268 => 867,  1257 => 862,  1253 => 861,  1249 => 860,  1245 => 859,  1238 => 857,  1221 => 842,  1210 => 837,  1206 => 836,  1202 => 835,  1198 => 834,  1195 => 833,  1191 => 832,  1175 => 818,  1164 => 813,  1160 => 812,  1156 => 811,  1152 => 810,  1148 => 809,  1145 => 808,  1141 => 807,  1113 => 781,  1103 => 777,  1099 => 776,  1095 => 775,  1091 => 774,  1087 => 772,  1083 => 771,  1065 => 755,  1053 => 749,  1049 => 748,  1045 => 747,  1041 => 746,  1037 => 745,  1033 => 744,  1029 => 743,  1024 => 740,  1020 => 739,  997 => 718,  986 => 713,  982 => 712,  978 => 711,  974 => 710,  970 => 708,  966 => 707,  949 => 692,  939 => 688,  934 => 686,  930 => 685,  926 => 684,  922 => 683,  918 => 682,  911 => 680,  865 => 637,  851 => 626,  844 => 622,  834 => 615,  825 => 609,  817 => 604,  782 => 572,  775 => 568,  767 => 563,  759 => 558,  749 => 551,  714 => 519,  706 => 514,  698 => 509,  690 => 504,  682 => 499,  673 => 493,  665 => 488,  657 => 483,  649 => 478,  641 => 473,  632 => 467,  624 => 462,  616 => 457,  608 => 452,  590 => 439,  579 => 433,  569 => 428,  559 => 423,  548 => 417,  538 => 412,  528 => 407,  518 => 402,  508 => 397,  498 => 392,  485 => 384,  473 => 377,  463 => 372,  453 => 367,  441 => 362,  415 => 339,  406 => 333,  398 => 328,  390 => 323,  382 => 318,  374 => 313,  366 => 308,  357 => 302,  349 => 297,  341 => 292,  333 => 287,  325 => 282,  317 => 277,  309 => 272,  301 => 267,  287 => 258,  278 => 252,  269 => 246,  261 => 241,  19 => 1,);
    }
}
