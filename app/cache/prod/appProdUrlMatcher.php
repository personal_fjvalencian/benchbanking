<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // bench_test_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_test_homepage')), array (  '_controller' => 'Bench\\TestBundle\\Controller\\DefaultController::indexAction',));
        }

        // upload
        if ($pathinfo === '/test/upload') {
            return array (  '_controller' => 'Bench\\TestBundle\\Controller\\DefaultController::uploadAction',  '_route' => 'upload',);
        }

        if (0 === strpos($pathinfo, '/api/v1')) {
            // api_1_new_page
            if (0 === strpos($pathinfo, '/api/v1/pages/new') && preg_match('#^/api/v1/pages/new(?:\\.(?P<_format>xml|json|html))?$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_api_1_new_page;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_1_new_page')), array (  '_controller' => 'Bench\\ApiBundle\\Controller\\ApiController::newPageAction',  '_format' => NULL,));
            }
            not_api_1_new_page:

            if (0 === strpos($pathinfo, '/api/v1/us')) {
                // api_1_get_user
                if (0 === strpos($pathinfo, '/api/v1/user') && preg_match('#^/api/v1/user(?:\\.(?P<_format>xml|json|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_1_get_user;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_1_get_user')), array (  '_controller' => 'Bench\\ApiBundle\\Controller\\ApiController::getUserAction',  '_format' => NULL,));
                }
                not_api_1_get_user:

                // api_1_get_usuario
                if (0 === strpos($pathinfo, '/api/v1/usuarios') && preg_match('#^/api/v1/usuarios/(?P<id>[^/\\.]++)(?:\\.(?P<_format>xml|json|html))?$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_api_1_get_usuario;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_1_get_usuario')), array (  '_controller' => 'Bench\\ApiBundle\\Controller\\ApiController::getUsuarioAction',  '_format' => NULL,));
                }
                not_api_1_get_usuario:

                if (0 === strpos($pathinfo, '/api/v1/users')) {
                    // api_1_post_user_inmobiliarias
                    if (0 === strpos($pathinfo, '/api/v1/users/inmobiliarias') && preg_match('#^/api/v1/users/inmobiliarias(?:\\.(?P<_format>xml|json|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_1_post_user_inmobiliarias;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_1_post_user_inmobiliarias')), array (  '_controller' => 'Bench\\ApiBundle\\Controller\\ApiController::postUserInmobiliariasAction',  '_format' => NULL,));
                    }
                    not_api_1_post_user_inmobiliarias:

                    // api_1_post_user_aliansas_nuevo_remax
                    if (0 === strpos($pathinfo, '/api/v1/users/aliansas/nuevos/remaxes') && preg_match('#^/api/v1/users/aliansas/nuevos/remaxes(?:\\.(?P<_format>xml|json|html))?$#s', $pathinfo, $matches)) {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_api_1_post_user_aliansas_nuevo_remax;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'api_1_post_user_aliansas_nuevo_remax')), array (  '_controller' => 'Bench\\ApiBundle\\Controller\\ApiController::postUserAliansasNuevoRemaxAction',  '_format' => NULL,));
                    }
                    not_api_1_post_user_aliansas_nuevo_remax:

                }

            }

        }

        // bench_mobil_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_mobil_homepage')), array (  '_controller' => 'Bench\\MobilBundle\\Controller\\DefaultController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/mobil')) {
            // homeMobile
            if (rtrim($pathinfo, '/') === '/mobil') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'homeMobile');
                }

                return array (  '_controller' => 'Bench\\MobilBundle\\Controller\\DefaultController::homeMobileAction',  '_route' => 'homeMobile',);
            }

            // homeMobile2
            if ($pathinfo === '/mobil/simulacion') {
                return array (  '_controller' => 'Bench\\MobilBundle\\Controller\\DefaultController::homeMobile2Action',  '_route' => 'homeMobile2',);
            }

        }

        // guardarRegistro
        if ($pathinfo === '/guardarRegistro') {
            return array (  '_controller' => 'Bench\\MobilBundle\\Controller\\DefaultController::guardarRegistroAction',  '_route' => 'guardarRegistro',);
        }

        // homeMobileRegistro
        if (0 === strpos($pathinfo, '/mobil/registro') && preg_match('#^/mobil/registro/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'homeMobileRegistro')), array (  '_controller' => 'Bench\\MobilBundle\\Controller\\DefaultController::homeMobileRegistroAction',));
        }

        // getFiles
        if ($pathinfo === '/getFiles') {
            return array (  '_controller' => 'Bench\\FilepickerBundle\\Controller\\DefaultController::getFilesAction',  '_route' => 'getFiles',);
        }

        // bench_filepicker_homepage
        if ($pathinfo === '/filepicker') {
            return array (  '_controller' => 'Bench\\FilepickerBundle\\Controller\\DefaultController::indexAction',  '_route' => 'bench_filepicker_homepage',);
        }

        // SaveFile
        if ($pathinfo === '/SaveFile') {
            return array (  '_controller' => 'Bench\\FilepickerBundle\\Controller\\DefaultController::SaveFileAction',  '_route' => 'SaveFile',);
        }

        // TestFile
        if ($pathinfo === '/TestFile') {
            return array (  '_controller' => 'Bench\\FilepickerBundle\\Controller\\DefaultController::TestFileAction',  '_route' => 'TestFile',);
        }

        // SaveFileDicom
        if ($pathinfo === '/SaveFileDicom') {
            return array (  '_controller' => 'Bench\\FilepickerBundle\\Controller\\DefaultController::SaveFileDicomAction',  '_route' => 'SaveFileDicom',);
        }

        // UploadOtrosArchivos
        if ($pathinfo === '/UploadOtrosArchivos') {
            return array (  '_controller' => 'Bench\\FilepickerBundle\\Controller\\DefaultController::UploadOtrosArchivosAction',  '_route' => 'UploadOtrosArchivos',);
        }

        // DelFile
        if ($pathinfo === '/DelFile') {
            return array (  '_controller' => 'Bench\\FilepickerBundle\\Controller\\DefaultController::DelFileAction',  '_route' => 'DelFile',);
        }

        // bench_pdf_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_pdf_homepage')), array (  '_controller' => 'Bench\\PdfBundle\\Controller\\DefaultController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/upload')) {
            // bench_upload_homepage
            if ($pathinfo === '/upload5') {
                return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::indexAction',  '_route' => 'bench_upload_homepage',);
            }

            // uploadfijo3ultimas
            if ($pathinfo === '/uploadfijo3ultimas') {
                return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::uploadfijo3ultimasAction',  '_route' => 'uploadfijo3ultimas',);
            }

            // bench_up2
            if ($pathinfo === '/upload8') {
                return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::index2Action',  '_route' => 'bench_up2',);
            }

        }

        // cartolaafp
        if ($pathinfo === '/cartolaafp') {
            return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::cartolaafpAction',  '_route' => 'cartolaafp',);
        }

        // ulitmas3declaraciones
        if ($pathinfo === '/ulitmas3declaraciones') {
            return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::ulitmas3declaracionesAction',  '_route' => 'ulitmas3declaraciones',);
        }

        // permisoCirculacion
        if ($pathinfo === '/permisoCirculacion') {
            return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::permisoCirculacionAction',  '_route' => 'permisoCirculacion',);
        }

        // uploadvariable6ultimas
        if ($pathinfo === '/uploadvariable6ultimas') {
            return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::uploadvariable6ultimasAction',  '_route' => 'uploadvariable6ultimas',);
        }

        if (0 === strpos($pathinfo, '/declaracion')) {
            // declaracionAnual
            if ($pathinfo === '/declaracionAnual') {
                return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::declaracionAnualAction',  '_route' => 'declaracionAnual',);
            }

            // declaracion3
            if ($pathinfo === '/declaracion3') {
                return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::declaracion3Action',  '_route' => 'declaracion3',);
            }

        }

        // carnet
        if ($pathinfo === '/carnet') {
            return array (  '_controller' => 'Bench\\UploadBundle\\Controller\\DefaultController::carnetAction',  '_route' => 'carnet',);
        }

        // upload3
        if ($pathinfo === '/upload3') {
            return array (  '_controller' => 'Bench\\UploadArchivosBundle\\Controller\\DefaultController::indexAction',  '_route' => 'upload3',);
        }

        if (0 === strpos($pathinfo, '/subir')) {
            // subir2
            if ($pathinfo === '/subir2') {
                return array (  '_controller' => 'Bench\\UploadArchivosBundle\\Controller\\DefaultController::subir2Action',  '_route' => 'subir2',);
            }

            // subir3
            if ($pathinfo === '/subir3') {
                return array (  '_controller' => 'Bench\\UploadArchivosBundle\\Controller\\DefaultController::subir3Action',  '_route' => 'subir3',);
            }

        }

        // bench_admin_homepage
        if ($pathinfo === '/adminBench/admin') {
            return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::indexAction',  '_route' => 'bench_admin_homepage',);
        }

        // ingresoAdmin
        if ($pathinfo === '/ingresoAdmin') {
            return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::ingresoAdminAction',  '_route' => 'ingresoAdmin',);
        }

        // updateCotizaciones
        if ($pathinfo === '/adminBench/updateCotizaciones') {
            return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::updateCotizacionesAction',  '_route' => 'updateCotizaciones',);
        }

        // verDatos
        if (0 === strpos($pathinfo, '/banco/reporte') && preg_match('#^/banco/reporte/(?P<banco>[^/]++)/(?P<id>[^/]++)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'verDatos');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'verDatos')), array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::verDatosAction',));
        }

        // prueba22
        if ($pathinfo === '/prueba22') {
            return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\PruebController::prueba22Action',  '_route' => 'prueba22',);
        }

        if (0 === strpos($pathinfo, '/test/prueba')) {
            // pruebaRemax
            if ($pathinfo === '/test/pruebaRemax') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::pruebaRemaxAction',  '_route' => 'pruebaRemax',);
            }

            // pruebaCapi
            if ($pathinfo === '/test/pruebaCapi') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::pruebaCapiAction',  '_route' => 'pruebaCapi',);
            }

        }

        // UpdateCotizacionesArchivo
        if ($pathinfo === '/UpdateCotizacionesArchivo') {
            return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::UpdateCotizacionesArchivoAction',  '_route' => 'UpdateCotizacionesArchivo',);
        }

        if (0 === strpos($pathinfo, '/adminBench')) {
            // EliminarCotizacionesCosumo
            if ($pathinfo === '/adminBench/EliminarCotizacionesCosumo') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::EliminarCotizacionesCosumoAction',  '_route' => 'EliminarCotizacionesCosumo',);
            }

            // GuardarCotizaciones
            if ($pathinfo === '/adminBench/GuardarCotizaciones') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::GuardarCotizacionesAction',  '_route' => 'GuardarCotizaciones',);
            }

            // cotizar
            if (0 === strpos($pathinfo, '/adminBench/cotizar') && preg_match('#^/adminBench/cotizar/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'cotizar')), array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::cotizarAction',));
            }

            if (0 === strpos($pathinfo, '/adminBench/updateArchivos')) {
                // updateArchivos2
                if ($pathinfo === '/adminBench/updateArchivos2') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::updateArchivos2Action',  '_route' => 'updateArchivos2',);
                }

                // updateArchivos
                if ($pathinfo === '/adminBench/updateArchivos') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::updateArchivosAction',  '_route' => 'updateArchivos',);
                }

            }

            // index2
            if ($pathinfo === '/adminBench/index2') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::index2Action',  '_route' => 'index2',);
            }

            if (0 === strpos($pathinfo, '/adminBench/grid')) {
                // grid3
                if ($pathinfo === '/adminBench/grid3') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::grid3Action',  '_route' => 'grid3',);
                }

                // grid4
                if ($pathinfo === '/adminBench/grid4') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::grid4Action',  '_route' => 'grid4',);
                }

            }

            // UpDicom
            if (0 === strpos($pathinfo, '/adminBench/UpDicom') && preg_match('#^/adminBench/UpDicom/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'UpDicom')), array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::UpDicomAction',));
            }

            // otrosUpload
            if (0 === strpos($pathinfo, '/adminBench/otrosUpload') && preg_match('#^/adminBench/otrosUpload/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'otrosUpload')), array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::otrosUploadAction',));
            }

            // vistaSoloRegistrados
            if ($pathinfo === '/adminBench/vistaSoloRegistrados') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::vistaSoloRegistradosAction',  '_route' => 'vistaSoloRegistrados',);
            }

            // usuarioPedido
            if ($pathinfo === '/adminBench/usuarioPedido') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::usuarioPedidoAction',  '_route' => 'usuarioPedido',);
            }

            // vistaArchivados
            if ($pathinfo === '/adminBench/vistaArchivados') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::vistaArchivadosAction',  '_route' => 'vistaArchivados',);
            }

            // CrearPdf
            if (0 === strpos($pathinfo, '/adminBench/pCrearPdf') && preg_match('#^/adminBench/pCrearPdf/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'CrearPdf')), array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::CrearPdfAction',));
            }

            // crearpdf2
            if (0 === strpos($pathinfo, '/adminBench/crearpdf2') && preg_match('#^/adminBench/crearpdf2/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'crearpdf2')), array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::crearpdf2Action',));
            }

            // archivarUsuario2
            if ($pathinfo === '/adminBench/archivarUsuario2') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::archivarUsuario2Action',  '_route' => 'archivarUsuario2',);
            }

            // ver
            if (0 === strpos($pathinfo, '/adminBench/ver') && preg_match('#^/adminBench/ver/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'ver')), array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::verAction',));
            }

            // add
            if ($pathinfo === '/adminBench/add') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::addAction',  '_route' => 'add',);
            }

            // del
            if ($pathinfo === '/adminBench/del') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::delAction',  '_route' => 'del',);
            }

            if (0 === strpos($pathinfo, '/adminBench/grid')) {
                // grid
                if ($pathinfo === '/adminBench/grid') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::gridAction',  '_route' => 'grid',);
                }

                // grid2
                if ($pathinfo === '/adminBench/grid2') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::grid2Action',  '_route' => 'grid2',);
                }

            }

            // benchAdmin
            if ($pathinfo === '/adminBench/benchAdmin') {
                return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::cargaDatosAction',  '_route' => 'benchAdmin',);
            }

            if (0 === strpos($pathinfo, '/adminBench/todos')) {
                // todos
                if ($pathinfo === '/adminBench/todos') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::todosAction',  '_route' => 'todos',);
                }

                // todos2
                if ($pathinfo === '/adminBench/todos2') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::todos2Action',  '_route' => 'todos2',);
                }

            }

            if (0 === strpos($pathinfo, '/adminBench/pdf')) {
                // pdf
                if ($pathinfo === '/adminBench/pdf') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::pdfAction',  '_route' => 'pdf',);
                }

                // pdf2
                if ($pathinfo === '/adminBench/pdf2') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::pdf2Action',  '_route' => 'pdf2',);
                }

                // pdf3
                if ($pathinfo === '/adminBench/pdf3') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::pdf3Action',  '_route' => 'pdf3',);
                }

            }

            if (0 === strpos($pathinfo, '/adminBench/a')) {
                if (0 === strpos($pathinfo, '/adminBench/archiva')) {
                    // archivarUsuario
                    if ($pathinfo === '/adminBench/archivarUsuario') {
                        return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::archivarUsuarioAction',  '_route' => 'archivarUsuario',);
                    }

                    // archivados
                    if ($pathinfo === '/adminBench/archivados') {
                        return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::archivadosAction',  '_route' => 'archivados',);
                    }

                }

                // asignar
                if ($pathinfo === '/adminBench/asignar') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::asignarAction',  '_route' => 'asignar',);
                }

            }

            if (0 === strpos($pathinfo, '/adminBench/d')) {
                // dicomUsuario
                if ($pathinfo === '/adminBench/dicomUsuario') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::dicomUsuarioAction',  '_route' => 'dicomUsuario',);
                }

                // detalleUsuario
                if ($pathinfo === '/adminBench/detalleUsuario') {
                    return array (  '_controller' => 'Bench\\AdminBundle\\Controller\\DefaultController::detalleUsuarioAction',  '_route' => 'detalleUsuario',);
                }

            }

        }

        // login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'Bench\\LoginBundle\\Controller\\DefaultController::loginAction',  '_route' => 'login',);
        }

        // bench_registros_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_registros_homepage')), array (  '_controller' => 'Bench\\RegistrosBundle\\Controller\\DefaultController::indexAction',));
        }

        // registro
        if ($pathinfo === '/registro') {
            return array (  '_controller' => 'Bench\\RegistrosBundle\\Controller\\RegistrosController::registroAction',  '_route' => 'registro',);
        }

        // dec
        if ($pathinfo === '/dec') {
            return array (  '_controller' => 'Bench\\RegistrosBundle\\Controller\\RegistrosController::decAction',  '_route' => 'dec',);
        }

        // guardaRegistro
        if ($pathinfo === '/guardaRegistro') {
            return array (  '_controller' => 'Bench\\RegistrosBundle\\Controller\\RegistrosController::guardaRegistroAction',  '_route' => 'guardaRegistro',);
        }

        // activarUser
        if (0 === strpos($pathinfo, '/activarUser') && preg_match('#^/activarUser/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'activarUser')), array (  '_controller' => 'Bench\\RegistrosBundle\\Controller\\RegistrosController::activarUserAction',));
        }

        // inmobiliarias
        if ($pathinfo === '/inmobiliarias') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::inmobiliariasAction',  '_route' => 'inmobiliarias',);
        }

        // usuarioNoRegistrados
        if ($pathinfo === '/usuarioNoRegistrados') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::usuarioNoRegistradosAction',  '_route' => 'usuarioNoRegistrados',);
        }

        // indexMobil
        if ($pathinfo === '/home') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::indexMobilAction',  '_route' => 'indexMobil',);
        }

        // home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home');
            }

            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::indexAction',  '_route' => 'home',);
        }

        // VistaRecuperar2
        if (0 === strpos($pathinfo, '/FormularioRecuperaContrasena') && preg_match('#^/FormularioRecuperaContrasena/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'VistaRecuperar2')), array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::VistaRecuperar2Action',));
        }

        // testRut
        if ($pathinfo === '/testRut') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::testRutAction',  '_route' => 'testRut',);
        }

        // RecuperContrasenForm
        if ($pathinfo === '/RecuperContrasenForm') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::RecuperContrasenFormAction',  '_route' => 'RecuperContrasenForm',);
        }

        // capitalizarme
        if ($pathinfo === '/capitalizarme') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::capitalizarmeAction',  '_route' => 'capitalizarme',);
        }

        // factor
        if ($pathinfo === '/factor') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::capitalizarmeAction',  '_route' => 'factor',);
        }

        // RF
        if ($pathinfo === '/RF') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::RFAction',  '_route' => 'RF',);
        }

        // inmob2
        if ($pathinfo === '/ES') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::inmob2Action',  '_route' => 'inmob2',);
        }

        // toke
        if ($pathinfo === '/toke') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::tokeAction',  '_route' => 'toke',);
        }

        // remax
        if ($pathinfo === '/remax') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::remaxAction',  '_route' => 'remax',);
        }

        if (0 === strpos($pathinfo, '/simulacion/credito')) {
            // credito1
            if ($pathinfo === '/simulacion/creditoHipotecario') {
                return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::credito1Action',  '_route' => 'credito1',);
            }

            if (0 === strpos($pathinfo, '/simulacion/creditoC')) {
                if (0 === strpos($pathinfo, '/simulacion/creditoCons')) {
                    // credito2
                    if ($pathinfo === '/simulacion/creditoConsumo') {
                        return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::credito2Action',  '_route' => 'credito2',);
                    }

                    // credito3
                    if ($pathinfo === '/simulacion/creditoConsolidacion') {
                        return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::credito3Action',  '_route' => 'credito3',);
                    }

                }

                // credito4
                if ($pathinfo === '/simulacion/creditoCuentaCorriente') {
                    return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::credito4Action',  '_route' => 'credito4',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/re')) {
            // referencia
            if ($pathinfo === '/referencia') {
                return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::referenciaAction',  '_route' => 'referencia',);
            }

            // remax2
            if ($pathinfo === '/remax2') {
                return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::remax2Action',  '_route' => 'remax2',);
            }

        }

        // testToken
        if ($pathinfo === '/testToken') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::testTokenAction',  '_route' => 'testToken',);
        }

        // mantencion
        if ($pathinfo === '/man') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::mantencionAction',  '_route' => 'mantencion',);
        }

        // test2
        if ($pathinfo === '/test2') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::test2Action',  '_route' => 'test2',);
        }

        // maps
        if ($pathinfo === '/maps') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::executeXmlSitemapAction',  '_route' => 'maps',);
        }

        // simuladorLog
        if ($pathinfo === '/simuladorLog') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::simuladorLogAction',  '_route' => 'simuladorLog',);
        }

        // misCotizaciones
        if ($pathinfo === '/misCotizaciones') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::misCotizacionesAction',  '_route' => 'misCotizaciones',);
        }

        // simulador
        if ($pathinfo === '/simulador') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::simuladorAction',  '_route' => 'simulador',);
        }

        // invitacion
        if ($pathinfo === '/invitacion') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::invitacionAction',  '_route' => 'invitacion',);
        }

        // test
        if ($pathinfo === '/test') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::testAction',  '_route' => 'test',);
        }

        // quehacemos
        if ($pathinfo === '/quehacemos') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::quehacemosAction',  '_route' => 'quehacemos',);
        }

        // conocecreditos
        if ($pathinfo === '/conocecreditos') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::conocecreditosAction',  '_route' => 'conocecreditos',);
        }

        // quickdemo
        if ($pathinfo === '/quickdemo') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::quickdemoAction',  '_route' => 'quickdemo',);
        }

        // logout
        if ($pathinfo === '/logout') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::logoutAction',  '_route' => 'logout',);
        }

        // formulario
        if ($pathinfo === '/formulario') {
            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::formularioAction',  '_route' => 'formulario',);
        }

        if (0 === strpos($pathinfo, '/recupera')) {
            // recuperaContrasena
            if ($pathinfo === '/recuperaContrasena') {
                return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::recuperaContrasenaAction',  '_route' => 'recuperaContrasena',);
            }

            // recupera
            if ($pathinfo === '/recupera') {
                return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::recuperaAction',  '_route' => 'recupera',);
            }

        }

        // comboRegion
        if ($pathinfo === '/comboRegion') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_comboRegion;
            }

            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\comboRegionController::comboRegionAction',  '_route' => 'comboRegion',);
        }
        not_comboRegion:

        // enviarMail
        if ($pathinfo === '/enviarMail') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_enviarMail;
            }

            return array (  '_controller' => 'Bench\\PaginasBundle\\Controller\\DefaultController::enviarMailAction',  '_route' => 'enviarMail',);
        }
        not_enviarMail:

        if (0 === strpos($pathinfo, '/h')) {
            // bench_debes_homepage
            if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_debes_homepage')), array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::indexAction',));
            }

            // hipotecarioguarda
            if ($pathinfo === '/hipotecarioguarda') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_hipotecarioguarda;
                }

                return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::hipotecarioAction',  '_route' => 'hipotecarioguarda',);
            }
            not_hipotecarioguarda:

        }

        // tarjetacreditoguarda
        if ($pathinfo === '/tarjetacreditoguarda') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_tarjetacreditoguarda;
            }

            return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::tarjetacreditoAction',  '_route' => 'tarjetacreditoguarda',);
        }
        not_tarjetacreditoguarda:

        // lineacreditoguarda
        if ($pathinfo === '/lineacreditoguarda') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_lineacreditoguarda;
            }

            return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::lineacreditoAction',  '_route' => 'lineacreditoguarda',);
        }
        not_lineacreditoguarda:

        // creditoConsumo
        if ($pathinfo === '/creditoConsumo') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_creditoConsumo;
            }

            return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::creditoConsumoAction',  '_route' => 'creditoConsumo',);
        }
        not_creditoConsumo:

        if (0 === strpos($pathinfo, '/de')) {
            if (0 === strpos($pathinfo, '/deldebes')) {
                // deldebesHipotecario
                if ($pathinfo === '/deldebesHipotecario') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_deldebesHipotecario;
                    }

                    return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::deldebesHipotecarioAction',  '_route' => 'deldebesHipotecario',);
                }
                not_deldebesHipotecario:

                // deldebesTarjeta
                if ($pathinfo === '/deldebesTarjeta') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_deldebesTarjeta;
                    }

                    return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::deldebesTarjetaAction',  '_route' => 'deldebesTarjeta',);
                }
                not_deldebesTarjeta:

                // deldebeslineaCredito
                if ($pathinfo === '/deldebeslineaCredito') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_deldebeslineaCredito;
                    }

                    return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::deldebeslineaCreditoAction',  '_route' => 'deldebeslineaCredito',);
                }
                not_deldebeslineaCredito:

                // deldebesCreditoconsumo
                if ($pathinfo === '/deldebesCreditoconsumo') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_deldebesCreditoconsumo;
                    }

                    return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\DefaultController::deldebesCreditoconsumoAction',  '_route' => 'deldebesCreditoconsumo',);
                }
                not_deldebesCreditoconsumo:

            }

            if (0 === strpos($pathinfo, '/debes')) {
                // debesBienesRaicesSiNo
                if ($pathinfo === '/debesBienesRaicesSiNo') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_debesBienesRaicesSiNo;
                    }

                    return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\debeSiNoController::debesBienesRaicesSiNoAction',  '_route' => 'debesBienesRaicesSiNo',);
                }
                not_debesBienesRaicesSiNo:

                // debesTarjetaCreditoSiNo
                if ($pathinfo === '/debesTarjetaCreditoSiNo') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_debesTarjetaCreditoSiNo;
                    }

                    return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\debeSiNoController::debesTarjetaCreditoSiNoAction',  '_route' => 'debesTarjetaCreditoSiNo',);
                }
                not_debesTarjetaCreditoSiNo:

                // debesLineaCreditoSiNo
                if ($pathinfo === '/debesLineaCreditoSiNo') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_debesLineaCreditoSiNo;
                    }

                    return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\debeSiNoController::debesLineaCreditoSiNoAction',  '_route' => 'debesLineaCreditoSiNo',);
                }
                not_debesLineaCreditoSiNo:

                // debesCreditoConsumoSiNo
                if ($pathinfo === '/debesCreditoConsumoSiNo') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_debesCreditoConsumoSiNo;
                    }

                    return array (  '_controller' => 'Bench\\DebesBundle\\Controller\\debeSiNoController::debesCreditoConsumoSiNoAction',  '_route' => 'debesCreditoConsumoSiNo',);
                }
                not_debesCreditoConsumoSiNo:

            }

        }

        // bench_tienes_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_tienes_homepage')), array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::indexAction',));
        }

        // bienesraicesguarda
        if ($pathinfo === '/bienesraicesguarda') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_bienesraicesguarda;
            }

            return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::bienesRaicesAction',  '_route' => 'bienesraicesguarda',);
        }
        not_bienesraicesguarda:

        if (0 === strpos($pathinfo, '/a')) {
            // ahorroinversionguarda
            if ($pathinfo === '/ahorroinversionguarda') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_ahorroinversionguarda;
                }

                return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::ahorroinversionAction',  '_route' => 'ahorroinversionguarda',);
            }
            not_ahorroinversionguarda:

            // automovilesguarda
            if ($pathinfo === '/automovilesguarda') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_automovilesguarda;
                }

                return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::automovilesAction',  '_route' => 'automovilesguarda',);
            }
            not_automovilesguarda:

        }

        // sociedadesguarda
        if ($pathinfo === '/sociedadesguarda') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_sociedadesguarda;
            }

            return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::sociedadesAction',  '_route' => 'sociedadesguarda',);
        }
        not_sociedadesguarda:

        if (0 === strpos($pathinfo, '/del')) {
            // delbienesraices
            if ($pathinfo === '/delbienesraices') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_delbienesraices;
                }

                return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::delbienesraicesAction',  '_route' => 'delbienesraices',);
            }
            not_delbienesraices:

            if (0 === strpos($pathinfo, '/dela')) {
                // delahorroinversion
                if ($pathinfo === '/delahorroinversion') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_delahorroinversion;
                    }

                    return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::delahorroinversionAction',  '_route' => 'delahorroinversion',);
                }
                not_delahorroinversion:

                // delautomoviles
                if ($pathinfo === '/delautomoviles') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_delautomoviles;
                    }

                    return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::delautomovilesAction',  '_route' => 'delautomoviles',);
                }
                not_delautomoviles:

            }

            // delsociedades2
            if ($pathinfo === '/delsociedades2') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_delsociedades2;
                }

                return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\DefaultController::delsociedades2Action',  '_route' => 'delsociedades2',);
            }
            not_delsociedades2:

        }

        if (0 === strpos($pathinfo, '/tiene')) {
            if (0 === strpos($pathinfo, '/tienes')) {
                // tienesBienesRaicesSiNo
                if ($pathinfo === '/tienesBienesRaicesSiNo') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_tienesBienesRaicesSiNo;
                    }

                    return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\tienesSiNoController::tienesBienesRaicesSiNoAction',  '_route' => 'tienesBienesRaicesSiNo',);
                }
                not_tienesBienesRaicesSiNo:

                if (0 === strpos($pathinfo, '/tienesA')) {
                    // tienesAhorroInversionSiNo
                    if ($pathinfo === '/tienesAhorroInversionSiNo') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_tienesAhorroInversionSiNo;
                        }

                        return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\tienesSiNoController::tienesAhorroInversionSiNoAction',  '_route' => 'tienesAhorroInversionSiNo',);
                    }
                    not_tienesAhorroInversionSiNo:

                    // tienesAutomovilesSiNo
                    if ($pathinfo === '/tienesAutomovilesSiNo') {
                        if ($this->context->getMethod() != 'POST') {
                            $allow[] = 'POST';
                            goto not_tienesAutomovilesSiNo;
                        }

                        return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\tienesSiNoController::tienesAutomovilesSiNoAction',  '_route' => 'tienesAutomovilesSiNo',);
                    }
                    not_tienesAutomovilesSiNo:

                }

            }

            // tieneSociedadesSiNo
            if ($pathinfo === '/tieneSociedadesSiNo') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_tieneSociedadesSiNo;
                }

                return array (  '_controller' => 'Bench\\TienesBundle\\Controller\\tienesSiNoController::tieneSociedadesSiNoAction',  '_route' => 'tieneSociedadesSiNo',);
            }
            not_tieneSociedadesSiNo:

        }

        // bench_solicitudes_creditos_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_solicitudes_creditos_homepage')), array (  '_controller' => 'Bench\\SolicitudesCreditosBundle\\Controller\\DefaultController::indexAction',));
        }

        // guardaSolicitud
        if ($pathinfo === '/guardaSolicitud') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_guardaSolicitud;
            }

            return array (  '_controller' => 'Bench\\SolicitudesCreditosBundle\\Controller\\DefaultController::guardaSolicitudAction',  '_route' => 'guardaSolicitud',);
        }
        not_guardaSolicitud:

        // mandaSolicitud
        if ($pathinfo === '/mandaSolicitud') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_mandaSolicitud;
            }

            return array (  '_controller' => 'Bench\\SolicitudesCreditosBundle\\Controller\\DefaultController::mandaSolicitudAction',  '_route' => 'mandaSolicitud',);
        }
        not_mandaSolicitud:

        // bench_creditos_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_creditos_homepage')), array (  '_controller' => 'Bench\\CreditosBundle\\Controller\\DefaultController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/guarda')) {
            // guardaconsumo
            if ($pathinfo === '/guardaconsumo') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_guardaconsumo;
                }

                return array (  '_controller' => 'Bench\\CreditosBundle\\Controller\\DefaultController::guardaconsumoAction',  '_route' => 'guardaconsumo',);
            }
            not_guardaconsumo:

            // guardaautomotriz
            if ($pathinfo === '/guardaautomotriz') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_guardaautomotriz;
                }

                return array (  '_controller' => 'Bench\\CreditosBundle\\Controller\\DefaultController::guardaautomotrizAction',  '_route' => 'guardaautomotriz',);
            }
            not_guardaautomotriz:

            // guardahipotecario
            if ($pathinfo === '/guardahipotecario') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_guardahipotecario;
                }

                return array (  '_controller' => 'Bench\\CreditosBundle\\Controller\\DefaultController::guardahipotecarioAction',  '_route' => 'guardahipotecario',);
            }
            not_guardahipotecario:

        }

        // comboconsolidacion
        if ($pathinfo === '/comboconsolidacion') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_comboconsolidacion;
            }

            return array (  '_controller' => 'Bench\\CreditosBundle\\Controller\\comboController::comboconsolidacionAction',  '_route' => 'comboconsolidacion',);
        }
        not_comboconsolidacion:

        if (0 === strpos($pathinfo, '/guarda')) {
            // guardaconsolidacion
            if ($pathinfo === '/guardaconsolidacion') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_guardaconsolidacion;
                }

                return array (  '_controller' => 'Bench\\CreditosBundle\\Controller\\DefaultController::guardaconsolidacionAction',  '_route' => 'guardaconsolidacion',);
            }
            not_guardaconsolidacion:

            // guardaCuentaCorriente
            if ($pathinfo === '/guardaCuentaCorriente') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_guardaCuentaCorriente;
                }

                return array (  '_controller' => 'Bench\\CreditosBundle\\Controller\\DefaultController::guardaCuentaCorrienteAction',  '_route' => 'guardaCuentaCorriente',);
            }
            not_guardaCuentaCorriente:

        }

        // bench_usuarios_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bench_usuarios_homepage')), array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\DefaultController::indexAction',));
        }

        // simulacionesSave
        if ($pathinfo === '/simulacionesSave') {
            return array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\SimulacionesController::simulacionesSaveAction',  '_route' => 'simulacionesSave',);
        }

        // datospersonales
        if ($pathinfo === '/datospersonales') {
            return array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\DefaultController::datospersonalesAction',  '_route' => 'datospersonales',);
        }

        // enviarCotizacion
        if ($pathinfo === '/enviarCotizacion') {
            return array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\DefaultController::enviarCotizacionAction',  '_route' => 'enviarCotizacion',);
        }

        // testApi
        if ($pathinfo === '/testApi') {
            return array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\DefaultController::testApiAction',  '_route' => 'testApi',);
        }

        // prueba
        if ($pathinfo === '/prueba') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_prueba;
            }

            return array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\DefaultController::pruebaAction',  '_route' => 'prueba',);
        }
        not_prueba:

        if (0 === strpos($pathinfo, '/datos')) {
            // datospersonalesguarda
            if ($pathinfo === '/datospersonalesguarda') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_datospersonalesguarda;
                }

                return array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\DefaultController::datospesonalesAction',  '_route' => 'datospersonalesguarda',);
            }
            not_datospersonalesguarda:

            // datoslaboralesguarda
            if ($pathinfo === '/datoslaboralesguarda') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_datoslaboralesguarda;
                }

                return array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\DefaultController::datoslaboralesAction',  '_route' => 'datoslaboralesguarda',);
            }
            not_datoslaboralesguarda:

            // datosconyugeguarda
            if ($pathinfo === '/datosconyugeguarda') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_datosconyugeguarda;
                }

                return array (  '_controller' => 'Bench\\UsuariosBundle\\Controller\\DefaultController::conyugeAction',  '_route' => 'datosconyugeguarda',);
            }
            not_datosconyugeguarda:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
