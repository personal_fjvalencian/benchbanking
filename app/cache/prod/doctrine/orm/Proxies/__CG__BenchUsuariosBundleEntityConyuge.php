<?php

namespace Proxies\__CG__\Bench\UsuariosBundle\Entity;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class Conyuge extends \Bench\UsuariosBundle\Entity\Conyuge implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }

    /** @private */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int) $this->_identifier["id"];
        }
        $this->__load();
        return parent::getId();
    }

    public function setRut($rut)
    {
        $this->__load();
        return parent::setRut($rut);
    }

    public function getRut()
    {
        $this->__load();
        return parent::getRut();
    }

    public function setNombre($nombre)
    {
        $this->__load();
        return parent::setNombre($nombre);
    }

    public function getNombre()
    {
        $this->__load();
        return parent::getNombre();
    }

    public function setApellido($apellido)
    {
        $this->__load();
        return parent::setApellido($apellido);
    }

    public function getApellido()
    {
        $this->__load();
        return parent::getApellido();
    }

    public function setApellidoseg($apellidoseg)
    {
        $this->__load();
        return parent::setApellidoseg($apellidoseg);
    }

    public function getApellidoseg()
    {
        $this->__load();
        return parent::getApellidoseg();
    }

    public function setFechanacimiento($fechanacimiento)
    {
        $this->__load();
        return parent::setFechanacimiento($fechanacimiento);
    }

    public function getFechanacimiento()
    {
        $this->__load();
        return parent::getFechanacimiento();
    }

    public function setNacionalidad($nacionalidad)
    {
        $this->__load();
        return parent::setNacionalidad($nacionalidad);
    }

    public function getNacionalidad()
    {
        $this->__load();
        return parent::getNacionalidad();
    }

    public function setNiveleducacion($niveleducacion)
    {
        $this->__load();
        return parent::setNiveleducacion($niveleducacion);
    }

    public function getNiveleducacion()
    {
        $this->__load();
        return parent::getNiveleducacion();
    }

    public function setProfesion($profesion)
    {
        $this->__load();
        return parent::setProfesion($profesion);
    }

    public function getProfesion()
    {
        $this->__load();
        return parent::getProfesion();
    }

    public function setUniversidad($universidad)
    {
        $this->__load();
        return parent::setUniversidad($universidad);
    }

    public function getUniversidad()
    {
        $this->__load();
        return parent::getUniversidad();
    }

    public function setActividad($actividad)
    {
        $this->__load();
        return parent::setActividad($actividad);
    }

    public function getActividad()
    {
        $this->__load();
        return parent::getActividad();
    }

    public function setCargoactual($cargoactual)
    {
        $this->__load();
        return parent::setCargoactual($cargoactual);
    }

    public function getCargoactual()
    {
        $this->__load();
        return parent::getCargoactual();
    }

    public function setAntiguedadlaboral($antiguedadlaboral)
    {
        $this->__load();
        return parent::setAntiguedadlaboral($antiguedadlaboral);
    }

    public function getAntiguedadlaboral()
    {
        $this->__load();
        return parent::getAntiguedadlaboral();
    }

    public function setRentaliquidad($rentaliquidad)
    {
        $this->__load();
        return parent::setRentaliquidad($rentaliquidad);
    }

    public function getRentaliquidad()
    {
        $this->__load();
        return parent::getRentaliquidad();
    }

    public function setEmpresa($empresa)
    {
        $this->__load();
        return parent::setEmpresa($empresa);
    }

    public function getEmpresa()
    {
        $this->__load();
        return parent::getEmpresa();
    }

    public function setTelefono($telefono)
    {
        $this->__load();
        return parent::setTelefono($telefono);
    }

    public function getTelefono()
    {
        $this->__load();
        return parent::getTelefono();
    }

    public function setSituacionLaboral($situacionLaboral)
    {
        $this->__load();
        return parent::setSituacionLaboral($situacionLaboral);
    }

    public function getSituacionLaboral()
    {
        $this->__load();
        return parent::getSituacionLaboral();
    }

    public function setFecha($fecha)
    {
        $this->__load();
        return parent::setFecha($fecha);
    }

    public function getFecha()
    {
        $this->__load();
        return parent::getFecha();
    }

    public function setUsuario(\Bench\UsuariosBundle\Entity\Usuario $usuario = NULL)
    {
        $this->__load();
        return parent::setUsuario($usuario);
    }

    public function getUsuario()
    {
        $this->__load();
        return parent::getUsuario();
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'rut', 'nombre', 'apellido', 'apellidoseg', 'fechanacimiento', 'nacionalidad', 'niveleducacion', 'profesion', 'universidad', 'actividad', 'cargoactual', 'antiguedadlaboral', 'rentaliquidad', 'empresa', 'telefono', 'situacionLaboral', 'fecha', 'usuario');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields as $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}