<?php

/* BenchAdminBundle:plantillas:cruzdelsur.html.twig */
class __TwigTemplate_c626ca4033c91541e893ff98f701b9c20470404b2a05ae32cec019cc8beb0702 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
    <head>
        <meta charset=\"UTF-8\">
        <title>Plantilla Cruz del Sur</title>
        <style>

        body{


            width: 1900px;
        }
            .tabla{
                border-spacing: 0px;
                color:#004b85;
                border: 1px solid #004b85;
                font-size: 12px;
                font-family:Verdana, Geneva, sans-serif;
                width: 700px;
                margin-left: -1.5%;

            }

            p{

                font-size:11px;
                margin-top:2%;
                margin-left: 2%;
                position:relative;
            }
            
            .tabla td{
                
                color:#004b85;
                border: 1px solid #004b85;
            }
            
            .divinput{
                width: 100%;
                height: 100%;
                border-bottom: 1px;
                border-left: 0px;
                border-right: 0px;
                border-top: 0px;
                border-style: solid;
                position: relative;
            }
            
          
            .simuInput{
                width: 100%;
                height: 20px;
                border-bottom: 1px;
                border-left: 0px;
                border-right: 0px;
                border-top: 0px;
                border-style: solid;
                position: relative;
                float: left;
            }
        </style>
    </head>
<body>
<div  style=\"width:700px;\"> 
        <table width=\"710\" height=\"35\" class=\"tabla\">
            <tr>
                <td width=\"456\"><table width=\"456\">
                        <tr>
                            <td width=\"130\">HIPOTECARIA </td>
                            <td width=\"310\">Avda. El Golfo 150, Piso 2, Las Condes Santiago </td>
                        </tr>
                        <tr>
                            <td>CRUZ DEL SUR </td>
                            <td>Tel: 600 461 8000 </td>
                        </tr>
                        <tr>
                            <td>PRINCIPAL S.A </td>
                            <td>www.cruzdelsur.cl </td>
                        </tr>
                    </table></td>
                <td width=\"238\"><table width=\"237\">
                        <tr>
                            <td width=\"227\">HIPOTECARIA</td>
                        </tr>
                        <tr>
                            <td height=\"49\">
                                CRUZ DEL SUR - Principal </td>
                        </tr>
                    </table></td>
            </tr>
        </table>
        <br>
        <br>
        <table width=\"710\">
            <tr>
                <td align=\"center\">
                    <h2 style=\"color: #D3D3D3;\">    Confidencial</h2>
                    </td>
            </tr>
            <tr>
                <td  align=\"center\">
                    <h1 style=\"color: #0A246A;\" >Solicitud de Crédito Hipotecario</h1> </td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" style=\"color:#004b85;\">
            <tr>
                <td width=\"487\">&nbsp;</td>
                <td width=\"211\">N&ordm; DEL SOLICITUD </td>
            </tr>
            <tr>
                <td>I ANTECEDENTES PERSONALES DEL SOLICITANTE </td>
                <td>&nbsp;</td>
            </tr>
        </table>
        
  
       
        
<table width=\"710\" style=\"color:#004b85;\" class=\"tabla\">
          <tr>
            <td height=\"32\" colspan=\"2\"><table width=\"360\">
              <tr>
                <td width=\"91\">DEUDOR</td>
                <td width=\"24\">&nbsp;</td>
                <td width=\"91\">CODEUDOR</td>
                <td width=\"28\">&nbsp;</td>
                <td width=\"79\">CONYUGE</td>
                <td width=\"19\">&nbsp;</td>
              </tr>
            </table></td>
            <td colspan=\"2\"><table width=\"307\" >
              <tr>
                <td width=\"165\">COPROPIETARIO</td>
                <td width=\"24\">&nbsp;</td>
                <td width=\"20\">&nbsp;</td>
                <td width=\"21\">%</td>
                <td width=\"53\">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td width=\"173\">APELLIDO MATERNO</td>
            <td width=\"189\">APELLIDO MATERNO</td>
            <td width=\"198\">NOMBRE</td>
            <td width=\"140\">RUT</td>
          </tr>
          <tr>
            <td>";
        // line 149
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo "<p>&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellido", array()), "html", null, true);
            echo "</p> ";
        }
        echo " </td>
            <td>";
        // line 150
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo "<p> &nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellidoseg", array()), "html", null, true);
            echo "</p> ";
        }
        echo "</td>
            <td>";
        // line 151
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " <p>&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array()), "html", null, true);
            echo "</p> ";
        }
        echo " </td>
            <td>";
        // line 152
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " <p>&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "rut", array()), "html", null, true);
            echo " </p>";
        }
        echo "</td>
          </tr>
          <tr>
            <td>ACTIVIDAD/PROFESION</td>
            <td>NACIONALIDAD</td>
            <td>FECHA NAC,</td>
            <td>EDAD AÑOS</td>
          </tr>
          <tr>
            <td>";
        // line 161
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo "<p>&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "profesion", array()), "html", null, true);
            echo "</p>  ";
        }
        echo "</td>
            <td>";
        // line 162
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo "<p>&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array()), "html", null, true);
            echo "</p> ";
        }
        echo " </td>
            <td>";
        // line 163
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo "<p>&nbsp;";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "fechaNacimiento", array()), "html", null, true);
            echo " </p>  ";
        }
        echo "</td>
            <td>";
        // line 164
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo "<p>&nbsp;";
            echo twig_escape_filter($this->env, (isset($context["edadUsuario"]) ? $context["edadUsuario"] : null), "html", null, true);
            echo " ";
        }
        echo "</p></td>
          </tr>
          <tr>
            <td colspan=\"2\">DIRECCION PARTICULAR (CALLE, N°, DEPTO)</td>
            
            <td>TELEFONO PARTICULAR</td>
            <td>CELULAR</td>
          </tr>
          <tr>
            <td colspan=\"2\"><p>&nbsp;";
        // line 173
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo "</p> </td>
            <td><p>&nbsp;";
        // line 174
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array()), "html", null, true);
            echo "  ";
        }
        echo "</p> </td>
            <td><p>&nbsp; ";
        // line 175
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array()), "html", null, true);
            echo "  ";
        }
        echo "</p> </td>
          </tr>
          <tr>
            <td>COMUNA</td>
            <td>CIUDAD</td>
            <td>REGION</td>
            <td>EMAIL</td>
          </tr>
          <tr>
            <td><p>&nbsp;";
        // line 184
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
            <td>&nbsp;</td>
            <td><p>&nbsp;";
        // line 186
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
            <td><p> &nbsp;";
        // line 187
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "email", array()), "html", null, true);
        echo "</p> </td>
          </tr>
          <tr>
            <td>GRADO/TITULO</td>
            <td>INSTITUTO/UNIVERSIDAD</td>
            <td colspan=\"2\">NIVEL DE ESTUDIOS (E. MEDIA, TECNICO, TEC. PROF., UNIVERSITARIO, OTRO)</td>
           
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;";
        // line 197
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "universidad", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
            <td colspan=\"2\">&nbsp;";
        // line 198
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "niveleducacion", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
          </tr>
         



            <tr>
            <td>Nacionalidad</td>
            <td>VIVIENDA ACTUAL</td>
            <td>ESTADO CIVIL</td>
       
            <td>N° DE CARGAS</td>
          </tr>
          <tr>

           <td><p>&nbsp;";
        // line 213
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
            <td><p>&nbsp;";
        // line 214
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
            <td><p>&nbsp;";
        // line 215
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
   
            <td><p>&nbsp;";
        // line 217
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "ndependientes", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "ndependientes", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
           
          </tr>
          







</table>
    <br><br>
        
        
        
        
        <table style=\"color:#004b85;\" >
            <tr>
                <td>II. ANTECEDENTES LABORALES DEL SOLICITANTE </td>
            </tr>
        </table>
        
        
        <table width=\"710\" class=\"tabla\">
            
            <tr>
                <td colspan=\"4\">RAZON SOCIAL EMPLEADOR ACTUAL </td>
                <td colspan=\"2\">RUT EMPLEADOR </td>
            </tr>
            <tr>
              <td colspan=\"4\"> &nbsp; </td>
              <td colspan=\"2\">&nbsp;<p>&nbsp;";
        // line 249
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "rutempresa", array()), "html", null, true);
            echo " ";
        }
        echo "</p></td>
            </tr>
            
            <tr>
                <td colspan=\"4\">DIRECCION EMPLEADOR (CALLE, N&ordm;, DEPTO/ OF) </td>
                <td colspan=\"2\">COMNUNA</td>
            </tr>
            <tr>
              <td colspan=\"4\"><p>&nbsp;";
        // line 257
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo "</p></td>
              <td colspan=\"2\"><p>&nbsp;";
        // line 258
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo "</p></td>
            </tr>
            <tr>
                <td width=\"91\" >TELEFONOS</td>
                <td width=\"217\">CARGO ACTUAL </td>
                <td width=\"113\" rowspan=\"2\" >ANTIGUEDAD CARGO(A&Ntilde;OS) </td>
                <td width=\"53\" rowspan=\"2\" >&nbsp;</td>
                <td width=\"127\" rowspan=\"2\" >ANTIGUEDAD EMPRESA (A&Ntilde;OS)</td>
                <td width=\"81\" rowspan=\"2\" >&nbsp;</td>
            </tr>
            <tr>
              <td>  <p> &nbsp;";
        // line 269
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "telefono", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
              <td width=\"217\"><p> &nbsp;  ";
        // line 270
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
        }
        echo "  </p></td>
            </tr>
            <tr>
                <td colspan=\"6\">DESCRIPCION CARGO ACTUAL </td>

            </tr>
            <tr>
              <td colspan=\"6\"><p>&nbsp; ";
        // line 277
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "cargoactual", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
            </tr>
            <tr>
                <td colspan=\"6\">DESCRIPCION EMPRESA </td>
            </tr>
            <tr>
              <td colspan=\"6\"><p>&nbsp; ";
        // line 283
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo "</p></td>
            </tr>
            <tr>
                <td colspan=\"4\">NOMBRE JEFE DIRECTO </td>
                <td colspan=\"2\">CARGO </td>
            </tr>
            <tr>
              <td colspan=\"4\"><p> ";
        // line 290
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "  &nbsp; ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "cargoactual", array()), "html", null, true);
            echo " ";
        }
        echo " </p></td>
              <td colspan=\"2\">&nbsp;</td>
            </tr>
            <tr>
                <td colspan=\"2\">ACITIVIDAD</td>
                <td colspan=\"4\">&nbsp; <p>";
        // line 295
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "tipoactividad", array()), "html", null, true);
            echo " ";
        }
        echo "</p></td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"7\"><div align=\"center\">CARGOS EMPLEOS ANTERIORES ULTIMOS 5 A&Ntilde;OS </div></td>
            </tr>
            <tr>
                <td colspan=\"2\">PERIODO</td>
                <td rowspan=\"2\">EMPLEADOR /ACTIVIDAD </td>
                <td rowspan=\"2\">CARGO</td>
                <td rowspan=\"2\">TELEFONO</td>
                <td colspan=\"2\" rowspan=\"2\">RENTA LIQUIDA MENSUAL </td>
            </tr>
            <tr>
                <td>DE</td>
                <td>A</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td rowspan=\"2\"></td>
                <td rowspan=\"2\"></td>
                <td rowspan=\"2\"></td>
                <td colspan=\"2\" rowspan=\"2\"></td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"2\"><div align=\"center\">INGRESOS LIQUIDOS MENSUALES </div></td>
            </tr>
            <tr>
                <td width=\"257\">RENTA LIQUIDA MENSUAL </td>
                <td width=\"437\">&nbsp;";
        // line 330
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "independientesueldoliquido", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
            </tr>
            <tr>
                <td>RENTA POR ARRIENDOS </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>OTROS INGRESOS / BONOS </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>OTROS INGRESOS / COMISIONES </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>RENTA LIQUIDA CONYUGE </td>
                <td> ";
        // line 346
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array()), "html", null, true);
            echo " ";
        }
        echo " </td>
            </tr>
            <tr>
                <td>OTROS (DESCRIBIR) </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>TOTAL</td>
";
        // line 354
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " 
                     ";
            // line 355
            $context["sueldo1"] = strtr($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "independientesueldoliquido", array()), array("." => ""));
            // line 356
            echo "                    ";
            if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
                // line 357
                echo "                     ";
                $context["sueldo2"] = $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array());
                // line 358
                echo "                    ";
            } else {
                // line 359
                echo "                    ";
                $context["sueldo2"] = 0;
                echo ";
                    ";
            }
            // line 361
            echo "
                    ";
            // line 362
            if (((isset($context["sueldo1"]) ? $context["sueldo1"] : null) && (isset($context["sueldo2"]) ? $context["sueldo2"] : null))) {
                // line 363
                echo "
                    ";
                // line 364
                $context["total"] = ((isset($context["sueldo1"]) ? $context["sueldo1"] : null) + (isset($context["sueldo2"]) ? $context["sueldo2"] : null));
                // line 365
                echo "

";
            }
            // line 368
            echo "
                <td>&nbsp; ";
            // line 369
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["total"]) ? $context["total"] : null), 0, ".", "."), "html", null, true);
            echo "</td>


                ";
        }
        // line 373
        echo "
            </tr>
        </table>
        <BR>
          <BR>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"2\"><div align=\"center\">ACTIVOS</div></td>
                <td width=\"226\"><div align=\"center\">Total</div></td>
            </tr>
            <tr>
                <td width=\"300\">DEPOSITO A PLAZO/ACCIONES </td>
                <td></td>
                <td width=\"168\">&nbsp;";
        // line 386
        if ((isset($context["totalAhorroInversionAcciones"]) ? $context["totalAhorroInversionAcciones"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionAcciones"]) ? $context["totalAhorroInversionAcciones"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        echo "</td>
             
            </tr>
            <tr>
                <td>CUENTA DE AHORRO </td>
                <td></td>
                <td>";
        // line 392
        if ((isset($context["totalAhorroInversionCuentas"]) ? $context["totalAhorroInversionCuentas"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionCuentas"]) ? $context["totalAhorroInversionCuentas"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null)) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null), 0, ".", "."), "html", null, true);
                echo " ";
            } else {
                echo " NO  ";
            }
        }
        echo "</td>
             
            </tr>
            <tr>
                <td>BIENES RAICES </td>
                <td></td>
                <td>&nbsp; ";
        // line 398
        if ((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null)) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null), 0, ".", "."), "html", null, true);
                echo " ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo "</td>
          
            </tr>
            <tr>
                <td>VEHICULOS</td>
                <td>&nbsp;</td>
                <td>&nbsp;";
        // line 404
        if ((isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null)) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null), 0, ".", "."), "html", null, true);
                echo "  ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </td>
            </tr>
            <tr>
                <td>PARTICIPACION EN SOCIEDADES </td>
                <td>&nbsp;</td>
                <td>&nbsp; ";
        // line 409
        if ((isset($context["totalSociedades"]) ? $context["totalSociedades"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["totalSociedades"]) ? $context["totalSociedades"] : null), "html", null, true);
            echo " ";
        }
        echo "</td>
            </tr>
            <tr>
                <td>SUBSIDIO</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>PIE CANCELADO </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>TOTAL</td>
                <td>&nbsp;</td>

                <td>&nbsp;";
        // line 425
        if ((isset($context["totalActivos"]) ? $context["totalActivos"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalActivos"]) ? $context["totalActivos"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo " </td>
            </tr>
        </table>
        <BR>
                <BR>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"6\"><div align=\"center\">PASIVOS</div></td>
          </tr>
            <tr>
                <td width=\"149\">TIPO DEUDA </td>
                <td>Total </td>
            </tr>
            <tr>
                <td>HIPOTECARIO</td>
                <td>&nbsp; ";
        // line 440
        if ((isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo "  NO ";
        }
        echo "</td>
             
            </tr>
            <tr>
                <td>CREDITO DE CONSUMO </td>
                <td>&nbsp; ";
        // line 445
        if ((isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo "  NO ";
        }
        echo " </td>
           
            </tr>
            <tr>
                <td>TARJETA DE CREDITO </td>
                <td>&nbsp; ";
        // line 450
        if ((isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO  ";
        }
        echo "</td>
                
            </tr>
            <tr>
                <td>LINEA DE CREDITO </td>
                <td>&nbsp; ";
        // line 455
        if ((isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
        }
        echo "</td>
      
            </tr>
            <tr>
                <td>CASAS COMERCIALES </td>
                <td>&nbsp;</td>
         
            </tr>
            <tr>
                <td>OTROS</td>
                <td>&nbsp;</td>
              
            </tr>
            <tr>
                <td>TOTAL</td>
                <td>&nbsp; ";
        // line 470
        if ((isset($context["totalDebes"]) ? $context["totalDebes"] : null)) {
            echo " ";
        } else {
            echo " ";
        }
        echo "</td>
           
            </tr>
</table>
        <br><br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td  colspan=\"5\">PATICIPACION EN SOCIEDADES</td>
            </tr>
           <tr>
                    <td>Razón Social</td>
                    <td>RUT Sociedad</td>

                    <td>% Participación</td>
                    <td>Valor comercial(\$)</td>
                </tr>

                 ";
        // line 487
        if ((isset($context["tienesSociedades"]) ? $context["tienesSociedades"] : null)) {
            // line 488
            echo "
                ";
            // line 489
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesSociedades"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesSociedades"]) {
                // line 490
                echo "

            <tr>
                <td>&nbsp;";
                // line 493
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "nombre", array()), "html", null, true);
                echo "</td>
                <td>&nbsp;";
                // line 494
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "rut", array()), "html", null, true);
                echo "</td>
                <td>&nbsp;";
                // line 495
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "porcentaje", array()), "html", null, true);
                echo "</td>
                <td>&nbsp;";
                // line 496
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "valor", array()), "html", null, true);
                echo "</td>
                
            </tr>


                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesSociedades'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 502
            echo "                ";
        }
        // line 503
        echo "

        </table>
        <br> <br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"6\">BIENES RACIES</td>
            </tr>
            <tr>
                     <td>Tipo</td>
                        <td>Direccion</td>
                        <td>Region</td>

                        <td>Avalúo Fiscal </td>
                        <td>Rol </td>
                        <td>Hipotecado </td>
            </tr>
                ";
        // line 520
        if ((isset($context["tienesHiporecario"]) ? $context["tienesHiporecario"] : null)) {
            // line 521
            echo "                 ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesHiporecario"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesHiporecario"]) {
                // line 522
                echo "                
            <tr>
                <td><p>";
                // line 524
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "tipo", array()), "html", null, true);
                echo "<p></td>
                <td><p>&nbsp;";
                // line 525
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "direccion", array()), "html", null, true);
                echo "</p></td>
                <td><p>&nbsp;";
                // line 526
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "region", array()), "html", null, true);
                echo "</p></td>
                <td><p>&nbsp; ";
                // line 527
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "avaluo", array()), "html", null, true);
                echo "</p></td>
                <td><p>&nbsp;";
                // line 528
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "rol", array()), "html", null, true);
                echo "</p></td>
                <td><p>&nbsp;";
                // line 529
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "hipotecado", array()), "html", null, true);
                echo "</p></td>
            </tr>

            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesHiporecario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 533
            echo "            ";
        }
        // line 534
        echo "        </table>
        <br><br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"5\" >VEHICULOS</td>
            </tr>
           <tr>
                        <td>Tipo</td>
                        <td>Marca</td>
                        <td>Modelo</td>
                        <td>Año</td>
                        <td>Patente</td>

                        <td>Valor Comercial(\$)</td>
                    </tr>

                      ";
        // line 550
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : null)) {
            // line 551
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesAutomoviles"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesAutomoviles"]) {
                // line 552
                echo "

            <tr>
                <td>&nbsp;";
                // line 555
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "tipo", array()), "html", null, true);
                echo "</td>
                <td>&nbsp;";
                // line 556
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "marcaauto", array()), "html", null, true);
                echo "</td>
                <td>&nbsp;";
                // line 557
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "modelo", array()), "html", null, true);
                echo "</td>
                <td>&nbsp;";
                // line 558
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "anoauto", array()), "html", null, true);
                echo "</td>
                <td>&nbsp;";
                // line 559
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "patente", array()), "html", null, true);
                echo "</td>
                <td>&nbsp;";
                // line 560
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "valorcomercial", array()), "html", null, true);
                echo "</td>
            </tr>

               ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAutomoviles'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 564
            echo "                ";
        }
        // line 565
        echo "

        </table>
        <br><br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td  colspan=\"6\"><div align=\"center\">COMPOSICION GRUPO FAMILIAR DIRECTO </div></td>
            </tr>
            <tr>
                <td width=\"24\">N&deg;</td>
                <td width=\"145\"><div align=\"center\">NOMBRE</div></td>
                <td width=\"52\"><div align=\"center\">EDAD</div></td>
                <td width=\"143\"><div align=\"center\">PARENTESCO</div></td>
                <td width=\"104\"><div align=\"center\">ACTIVIDAD</div></td>
                <td width=\"214\"><div align=\"center\">RENTA LIQUIDA MENSUAL </div></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"4\">OTROS ANTECEDENTES </td>
            </tr>
            <tr>
                <td width=\"498\">&iquest;ESTA OBLIGADO A PAGAR PENSION ALIMENTICIA?</td>
                <td width=\"119\">&nbsp;</td>
                <td width=\"33\">&nbsp;</td>
                <td width=\"40\">&nbsp;</td>
            </tr>
            <tr>
                <td>&iquest;HA TENIDO ALGUN JUICO COMERCIAL?</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&iquest;ES AVAL, CODEUDOR O FIADOR DE PERSONAS NATURALES?</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>EN CASO DE RESPUESTA AFIRMATIVA ESPECIFIQUE </td>
                <td colspan=\"3\">&nbsp;</td>
            </tr>
        </table>
        <br><br>
        <table style=\"color:#004b85;\">
            <tr>
                <td colspan=\"4\">III. ANTECEDENTES CONYUGE DEL SOLICITANTE </td>
            </tr>
        </table>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td width=\"208\">APELLIDO PATERNO </td>
                <td width=\"177\">APELLIDO MATERNO </td>
                <td width=\"124\">NOMBRES</td>
                <td width=\"181\">RUT</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td width=\"177\">&nbsp;";
        // line 633
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellido", array()), "html", null, true);
            echo "  ";
        }
        echo "</td>
                <td width=\"124\">&nbsp;";
        // line 634
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
                <td width=\"181\">&nbsp;";
        // line 635
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "nombre", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
            </tr>
            <tr>
                <td>FECHA NACIMIENTO </td>
                <td>ACTIVIDAD/PROFESION</td>
                <td>EMPRESA</td>
                <td>CARGO</td>
            </tr>
            <tr>
                <td>&nbsp;";
        // line 644
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "fechanacimiento", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
                <td>&nbsp;";
        // line 645
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "actividad", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
                <td>&nbsp;";
        // line 646
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "empresa", array()), "html", null, true);
            echo "  ";
        }
        echo "</td>
                <td>&nbsp; ";
        // line 647
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "cargoactual", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
            </tr>
            <tr>
                <td>RENTA LIQUIDA MENSUAL</td>
                <td>TELEFONO</td>
                <td>CELULAR</td>
                <td>EMAIL</td>
            </tr>
            <tr>
                <td>&nbsp; ";
        // line 656
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array()), "html", null, true);
            echo " ";
        }
        echo "</td>
                <td>&nbsp; ";
        // line 657
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "telefono", array()), "html", null, true);
            echo "  ";
        }
        echo "</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"2\"><div align=\"center\">ACTIVOS</div></td>
                <td width=\"226\"><div align=\"center\">INSTITUCION</div></td>
            </tr>
            <tr>
                <td width=\"300\">DEPOSITO A PLAZO/ACCIONES </td>
                <td width=\"168\">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>CUENTA DE AHORRO </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>BIENES RAICES </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>VEHICULOS</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>PARTICIPACION EN SOCIEDADES </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>SUBSIDIO</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>PIE CANCELADO </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>TOTAL</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"6\"><div align=\"center\">PASIVOS</div></td>
            </tr>
            <tr>
                <td>TIPO DE DEUDA </td>
                <td>BANCO O INSTITUCION </td>
                <td>SALDO DE DEUDA </td>
                <td>N&deg; CUOTAS PAGADAS </td>
                <td>N&deg; TOTAL CUOTAS </td>
                <td>MONTO CUOTA(S) </td>
            </tr>
            <tr>
                <td>HIPOTECARIO</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>CREDITO DE CONSUMO </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>TARJETA DE CREDITO </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>LINEA DE CREDITO </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>CASAS COMERCIALES </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>OTROS</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>TOTAL</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" style=\"color:#004b85;\">
            <tr>
                <td>V. ANTECEDENTES DE LA PROPIEDAD A HIPOTECAR </td>
            </tr>
        </table>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td colspan=\"4\">DIRECCION (CALLE, N&deg;, DEPTO)</td>
                <td width=\"139\">M TERRENO </td>
                <td width=\"215\">M CONSTRUIDOS </td>
            </tr>
            <tr>
                <td colspan=\"4\">&nbsp;</td>
                <td width=\"139\">&nbsp;</td>
                <td width=\"215\">&nbsp;</td>
            </tr>
            <tr>
                <td colspan=\"2\">COMUNA</td>
                <td colspan=\"2\">CIUDAD</td>
                <td>BODEGA N&deg; </td>
                <td>ESTACIONAMIENTO N&deg; </td>
            </tr>
            <tr>
                <td colspan=\"2\">&nbsp;</td>
                <td colspan=\"2\">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan=\"3\">VENDEDOR INMOVILIARIA </td>
                <td colspan=\"2\">RUT VENDEDOR </td>
                <td >TELEFONOS VENDEDOR </td>
            </tr>
            <tr>
                <td colspan=\"3\">&nbsp;</td>
                <td colspan=\"2\">&nbsp;</td>
                <td >&nbsp;</td>
            </tr>
            <tr>
                <td width=\"201\" height=\"12\">TIPO DE OPERACION </td>
                <td colspan=\"4\">TIPO DE VIVIENDA </td>
                <td>ANTIGUEDAD VIVIENDA </td>
            </tr>
            <tr>
                <td height=\"12\">&nbsp;</td>
                <td colspan=\"4\">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan=\"6\">INDICAR BANCO ALZANTE </td>
            </tr>
            <tr>
                <td colspan=\"6\">&nbsp;</td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" style=\"color:#004b85;\">
            <tr>
                <td colspan=\"4\">VI. DIRECCION DE COBRO DE DIVIDENDO </td>
            </tr>
        </table>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td width=\"149\">VIVIENDA ACTUAL </td>
                <td width=\"272\">VIVIENDA QUE ESTA COMPRANDO </td>
                <td width=\"189\">DIRECCION DE TRABAJO </td>
                <td width=\"80\">OTRA</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br><br>
        <table width=\"710\" style=\"color:#004b85;\"> 
            <tr>
                <td colspan=\"4\">VII. FINANCIAMIENTO </td>
            </tr>
        </table>
        <table width=\"710\" class=\"tabla\">
            <tr>
                <td width=\"213\">PRECIO VENTA </td>
                <td colspan=\"3\">&nbsp;</td>
            </tr>
            <tr>
                <td>MONTO CREDITO </td>
                <td colspan=\"3\">&nbsp;</td>
            </tr>
            <tr>
                <td>PAGO CONTADO / PIE </td>
                <td width=\"167\">&nbsp;</td>
                <td width=\"140\">FORMA DE PAGO </td>
                <td width=\"170\">&nbsp;</td>
            </tr>
            <tr>
                <td>SUBSIDIO</td>
                <td colspan=\"3\" >&nbsp;</td>
            </tr>
            <tr>
                <td>OTROS</td>
                <td colspan=\"3\" >&nbsp;</td>
            </tr>
            <tr>
                <td>PLAZO SOLICITADO </td>
                <td colspan=\"3\" >&nbsp;</td>
            </tr>
        </table>



        
            <br/><br/><br/><br/><br/><br/><br/><br/>
            <div style=\"width: 710px;\">
                <hr style=\"color: #808080;\" size=\"8\" noshade>
                <h1 align=\"right\">SOLICITUD DE PRODUCTOS</h1>
                <hr style=\"color: #808080;\" size=\"8\" noshade>
            </div>


            <h3>Crédito de consumo:</h3>
            <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Monto de Crédito:</td>
                        <td>Plazo de Crédito </td>
                        <td>Seguros Asociados </td>
                        <td>¿Para qué utilizarás el Dinero? </td>

                    </tr>



                            <tr>
                                <td><div class=\"input\">&nbsp;   ";
        // line 920
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "montoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 921
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 922
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "segurosAsociados", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 923
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "paraque", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                            </tr>






                    </tbody>
                </table>




                <h3>Crédito Hipotecario </h3>
                <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Valor Propiedad :</td>
                        <td>Monto de Píe </td>
                        <td>Porcentaje </td>
                        <td>Monto de Crédito </td>

                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp;   ";
        // line 958
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "valorPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 959
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoPie", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;   ";
        // line 960
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "porcentaje", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;  ";
        // line 961
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>









                    <tr>
                        <td>Plazo de Crédito </td>
                        <td>Tipo de Propiedad </td>
                        <td>Estado </td>
                        <td>¿Tu propiedad es DFL2 y nueva ? </td>

                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 985
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;";
        // line 986
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "tipoPropiedad", array()), "html", null, true);
            echo " ";
        }
        // line 987
        echo "                                </div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 988
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "estadoPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 989
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "dfl2", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>









                    <tr>
                        <td>Comuna donde desea comprar</td>
                        <td>Proyecto / Inmobiliaria </td>


                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 1012
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "comunaComprar", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1013
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "proyecto", array()), "html", null, true);
            echo "     ";
        }
        echo "</div></td>


                        </tr>







                    </tbody>
                </table>




                    Crédito Automotriz

                    <table class=\"tabla\">
                        <thead>

                        </thead>
                        <tbody>
                        <tr>
                            <td>Monto del Auto </td>
                            <td>Monto del Píe  </td>
                            <td>Plazo del Crédito </td>


                        </tr>


                                <tr>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1047
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "monto", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1048
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "pie", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1049
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "plazo", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                                </tr>



                        <tr>
                            <td>Cuándo piensas comprar tu auto </td>
                            <td>Marca de Auto  </td>
                            <td>Año del Auto </td>


                        </tr>


                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 1066
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "cuandocompras", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1067
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "marcaauto", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1068
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "ano", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>




                        </tbody>
                    </table>

<table class=\"tabla\" >
<h3>
DATOS CRÉDITO CONSOLIDACION 
</h3>
<tr>
    <th>Crédito </th>
    <th>Monto </th>
    <th>Plazo </th>

  
    </tr>



    </thead>

    <tr>
    ";
        // line 1096
        $context["con"] = 0;
        // line 1097
        echo "    ";
        if ((isset($context["creditoConsolidacion"]) ? $context["creditoConsolidacion"] : null)) {
            // line 1098
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["creditoConsolidacion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["creditoConsolidacion"]) {
                // line 1099
                echo "    ";
                $context["con"] = ((isset($context["con"]) ? $context["con"] : null) + 1);
                // line 1100
                echo "
    ";
                // line 1101
                if (((isset($context["con"]) ? $context["con"] : null) <= 1)) {
                    // line 1102
                    echo "
    <tr>   

        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1105
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1106
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1107
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo", array()), "html", null, true);
                    echo " </div> </td>
     </tr>
        
       <tr>    

        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1112
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1113
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1114
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo2", array()), "html", null, true);
                    echo " </div> </td>
</tr>
<tr>   


        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1119
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito3", array()), "html", null, true);
                    echo "  </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1120
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito3", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1121
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo3", array()), "html", null, true);
                    echo " </div></td>
       
   </tr> 
    

    ";
                }
                // line 1127
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsolidacion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1128
            echo "    ";
        }
        // line 1129
        echo "    
        </tr>

</table>


        </div>

    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:plantillas:cruzdelsur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1843 => 1129,  1840 => 1128,  1834 => 1127,  1825 => 1121,  1821 => 1120,  1817 => 1119,  1809 => 1114,  1805 => 1113,  1801 => 1112,  1793 => 1107,  1789 => 1106,  1785 => 1105,  1780 => 1102,  1778 => 1101,  1775 => 1100,  1772 => 1099,  1767 => 1098,  1764 => 1097,  1762 => 1096,  1727 => 1068,  1719 => 1067,  1711 => 1066,  1687 => 1049,  1679 => 1048,  1671 => 1047,  1630 => 1013,  1622 => 1012,  1593 => 989,  1586 => 988,  1583 => 987,  1577 => 986,  1569 => 985,  1538 => 961,  1530 => 960,  1522 => 959,  1514 => 958,  1472 => 923,  1464 => 922,  1456 => 921,  1448 => 920,  1178 => 657,  1170 => 656,  1154 => 647,  1146 => 646,  1138 => 645,  1130 => 644,  1114 => 635,  1106 => 634,  1098 => 633,  1028 => 565,  1025 => 564,  1015 => 560,  1011 => 559,  1007 => 558,  1003 => 557,  999 => 556,  995 => 555,  990 => 552,  985 => 551,  983 => 550,  965 => 534,  962 => 533,  952 => 529,  948 => 528,  944 => 527,  940 => 526,  936 => 525,  932 => 524,  928 => 522,  923 => 521,  921 => 520,  902 => 503,  899 => 502,  887 => 496,  883 => 495,  879 => 494,  875 => 493,  870 => 490,  866 => 489,  863 => 488,  861 => 487,  837 => 470,  813 => 455,  799 => 450,  785 => 445,  771 => 440,  749 => 425,  726 => 409,  704 => 404,  681 => 398,  659 => 392,  644 => 386,  629 => 373,  622 => 369,  619 => 368,  614 => 365,  612 => 364,  609 => 363,  607 => 362,  604 => 361,  598 => 359,  595 => 358,  592 => 357,  589 => 356,  587 => 355,  583 => 354,  568 => 346,  545 => 330,  503 => 295,  491 => 290,  477 => 283,  464 => 277,  452 => 270,  444 => 269,  426 => 258,  418 => 257,  403 => 249,  364 => 217,  355 => 215,  347 => 214,  339 => 213,  317 => 198,  309 => 197,  296 => 187,  288 => 186,  279 => 184,  264 => 175,  257 => 174,  249 => 173,  233 => 164,  225 => 163,  217 => 162,  209 => 161,  193 => 152,  185 => 151,  177 => 150,  169 => 149,  19 => 1,);
    }
}
