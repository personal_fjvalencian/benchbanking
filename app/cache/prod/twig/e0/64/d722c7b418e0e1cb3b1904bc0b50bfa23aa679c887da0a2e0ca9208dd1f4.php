<?php

/* BenchAdminBundle:Default:testPdf.html.twig */
class __TwigTemplate_e064d722c7b418e0e1cb3b1904bc0b50bfa23aa679c887da0a2e0ca9208dd1f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html>

<head>
<meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />
<title>BenchBanking - Estado de situación</title>

<style>
        
\t* { margin: 0; padding: 0; }
\t
\tbody {font-family: Verdana, Geneva, sans-serif !important; font-size: 12px; line-height: 18px; background-image: url(img/bg_icons.jpg.jpg);
background-position: center top;
background-repeat: no-repeat;}

header{
\twidth:626px;
\theight:160px;
\tmargin:0 auto;
}
\ta { text-decoration: none; }
\t.container{margin: 20px auto; width: 612px; height: auto; background-color:#0FF; -webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px; background-color:#EEEEEE;}
\th3 { padding:2px 0 2px 10px; font-size: 14px; background-color: #F60; color:#FFF; -moz-border-radius: 2px;
\t-webkit-border-radius: 2px;
\tborder-radius: 2px;
\tmargin:5px 0;}
\th4 { padding:4px 0 4px 10px; font-size: 16px; background-color: #666; color:#FFF; -moz-border-radius: 2px;
\t-webkit-border-radius: 2px;
\tborder-radius: 2px;
\tmargin:5px 0;}
\t
\t
\t.cols {
column-count: 3;
-moz-column-count:3; /* Firefox */
-webkit-column-count:3; /* Safari and Chrome */
}
\t
\t#contactform {
\t
\twidth: 580px;
\tpadding: 5px 16px;
\toverflow:auto;
\t
\t/*border: 1px solid #cccccc;
\t-moz-border-radius: 7px;
\t-webkit-border-radius: 7px;
\tborder-radius: 7px;\t
\t
\t-moz-box-shadow: 2px 2px 2px #cccccc;
\t-webkit-box-shadow: 2px 2px 2px #cccccc;
\tbox-shadow: 2px 2px 2px #cccccc;*/
\t
\t}
\t
\t.field{margin-bottom:2px;}
\t
\tlabel {
\t\tcolor:#666;
\tfont-family: Verdana, Geneva, sans-serif !important; 
\tdisplay: block; 
\tfloat: left; 
\tfont-weight: normal; 
\ttext-align: left; 
\twidth: 100%; 
\tline-height: 15px; 
\tfont-size: 12px; 
\t}
\t
\t.label-adress {
\t\tcolor:#666;
\tfont-family: Verdana, Geneva, sans-serif !important; 
\ttext-shadow: 1px 1px 1px #ccc;
\tdisplay: block; 
\tfloat: left; 
\tfont-weight: normal; 
\tmargin-right:10px; 
\ttext-align: left; 
\twidth: auto; 
\tline-height: 15px; 
\tfont-size: 12px; 
\t}
\t
\t.input-adress{
\t-webkit-border-radius: 4px;
\t-moz-border-radius: 4px;
\tborder-radius: 4px;
\tfont-family: Verdana, Geneva, sans-serif !important;
\tfont-size: 11px; 
\tpadding: 1px;
\tmargin:1px 0;
\tborder: 1px solid #b9bdc1; 
\twidth: 99%;
\theight:16px; 
\tcolor: #797979;\t
\t}
\t
\t.input{
\t-webkit-border-radius: 4px;
\t-moz-border-radius: 4px;
\tborder-radius: 4px;
\tfont-family: Verdana, Geneva, sans-serif !important; 
\tfont-size: 11px; 
\tpadding: 1px;
\tmargin:1px 0;
\tborder: 1px solid #b9bdc1; 
\twidth: 97%;
\theight:16px; 
\tcolor: #797979;\t
\t}
\t
\t.input:focus{
\tbackground-color:#E7E8E7;
\tborder:1px solid #F95B00;\t
\t}
\t
\t.textarea {
\theight:150px;\t
\t}
\t
\t.hint{
\tdisplay:none;
\tleft:190px;
\t}
\t
\t.field:hover .hint {  
\tposition: absolute;
\tdisplay: block;  
\tmargin: -30px 0 0 455px;
\tcolor: #FFFFFF;
\tpadding: 7px 10px;
\tbackground: rgba(0, 0, 0, 0.6);
\t
\t-moz-border-radius: 4px;
\t-webkit-border-radius: 4px;
\tborder-radius: 4px;\t
\t}
\t
\t.button{
\tfloat: right;
\tmargin:10px 55px 10px 0;
\tfont-weight: bold;
\tline-height: 1;
\tpadding: 6px 10px;
\tcursor:pointer;   
\tcolor: #fff;
\t
\ttext-align: center;
\ttext-shadow: 0 -1px 1px #64799e;
\t
\t/* Background gradient */
\tbackground: #a5b8da;
\tbackground: -moz-linear-gradient(top, #a5b8da 0%, #7089b3 100%);
\tbackground: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#a5b8da), to(#7089b3));
\t
\t/* Border style */
  \tborder: 1px solid #5c6f91;  
\t-moz-border-radius: 10px;
\t-webkit-border-radius: 10px;
\tborder-radius: 10px;
  
\t/* Box shadow */
\t-moz-box-shadow: inset 0 1px 0 0 #aec3e5;
\t-webkit-box-shadow: inset 0 1px 0 0 #aec3e5;
\tbox-shadow: inset 0 1px 0 0 #aec3e5;
\t
\t}
\t
\t.button:hover {
\tbackground: #848FB2;
    cursor: pointer;
\t}
\t
\t.box-patri{
\t\twidth:100%;
\t\theight:auto;
\t\ttext-align:left;
\t\tcolor: #666;
\t\tfont-size:14px;
\t\tborder-top: 1px solid #999;
\t\tpadding:5px 0;
\t}
\t
\t.box-patri img{
\t\tvertical-align: bottom;
\t}
\t
\t/*----- TABLE CSS -------*/
\t
\t
\t.datagrid table { border-collapse: collapse; text-align: left; width: 100%; }
\t.datagrid {font: normal 12px/150% Verdana, Geneva, sans-serif !important, sans-serif; background: #fff;  border: 1px solid #B9BDC1; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; /*width: 95%;*/}
\t.datagrid table td, .datagrid table th { padding: 3px 10px; }
\t.datagrid table thead th {background-color:#B9BDC1; color:#FFFFFF; font-size: 12px; font-weight: normal; border-left: 1px solid #A3A3A3; }
\t.datagrid table thead th:first-child { border: none; }
\t.datagrid table tbody td { color: #7D7D7D; border-left: 1px solid #DBDBDB;font-size: 12px;font-weight: normal; }
\t.datagrid table tbody .alt td { background: #EBEBEB; color: #7D7D7D; }
\t.datagrid table tbody td:first-child { border-left: none; }
\t.datagrid table tbody tr:last-child td { border-bottom: none; }
\t
\t
/*NEW*/\t
\t
td{
\t/*background-color:#0FF;*/
\twidth:33%;}\t
        
 </style>       

</head>

<body>
<div><img src=\"http://www.theclick.cl/img/header.png\" width=\"626\" height=\"160\"></div>

<!------- PAGINA 1 --------->

<div class=\"container\">

</br>
<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">

<h3>Datos Personales</h3>
<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
  <tr>
    <td>
    <input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"email\" id=\"email\" />
    </td>
    <td>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td colspan=\"3\">
      <input type=\"text\" class=\"input-adress\" name=\"name\" id=\"name\" />
    </td>
  </tr>
</table>


</br>



<h3>Datos Laborales</h3>
<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
  <tr>
    <td>
    <label for=\"name\">Situación Laboral:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Rut Empresa:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"email\">Tipo de Sueldo:</label>
  \t<input type=\"text\" class=\"input\" name=\"email\" id=\"email\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Empleador:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Región:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Sueldo Fijo:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Industria:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Comuna:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Sueldo Variable:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">fecha de ingreso:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td><label for=\"name\">Ciudad:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Sueldo Líquido:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Cargo Actual:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Teléfono:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan=\"3\">
    <label class=\"label-adress\" for=\"name\">Dirección:</label>
  \t<input type=\"text\" class=\"input-adress\" name=\"name\" id=\"name\" />
    </td>
    </tr>
</table>

<div class=\"cols\">
<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>



<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
\t<!--<p class=\"hint\">Enter your email.</p>-->
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

<div class=\"field\">
\t
</div>

</div>

<div class=\"field\">
\t
</div>


</br>
<h3>Datos Conyuge</h3>
<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
  <tr>
    <td>
    <label for=\"name\">Nombres:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Nivel de Educación:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Antiguedad Laboral:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Apellidos:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Profesión:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Renta Líquida:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Rut:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Universidad:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Empresa:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Fecha de Nacimiento:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Actividad:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Teléfono:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Nacionalidad:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Cargo Actual:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>&nbsp;</td>
  </tr>
</table>
</br>



</form>

</div>


</div>

<!------- PAGINA 2 --------->

<div class=\"container\">

<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
</br>

<h3>Resumen Activos</h3>
<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
  <tr>
    <td>
    <label for=\"name\">Bienes Raices:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Automóviles:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\" style=\"color:#F60;\">TOTAL ACTIVOS:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Ahorro e inversión:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Sociedades:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>&nbsp;</td>
  </tr>
</table>
</br>

<h3>Resumen Pasivos</h3>
<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
  <tr>
    <td>
    <label for=\"name\">Créditos Hipotecarios</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Tarjeta de Crédito:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\" style=\"color:#F60;\">TOTAL PASIVOS:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
  </tr>
  <tr>
    <td>
    <label for=\"name\">Línea de Crédito:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>
    <label for=\"name\">Créditos de Consumo:</label>
  \t<input type=\"text\" class=\"input\" name=\"name\" id=\"name\" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan=\"3\">
    <div class=\"box-patri\"><img src=\"img/star.png\" width=\"23\" height=\"24\"> PATRIMONIO: DATA</div>
    </td>
    </tr>
</table>
</br>



</br>

</form>
</div>


<!------- PAGINA 3 --------->

<div class=\"container\">

<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
</br>
<h4>Activos</h4>
<h3>Bienes Raices</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Tipo</th>
    <th>Dirección</th>
    <th>Región</th>
    <th>Comuna</th>
    <th>Avalúo Fiscal</th>
    <th>Rol</th>
    <th>Hipotecario</th></tr></thead>
<tbody><tr><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>
</br>

<h3>Ahorro de Inversión</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Tipo</th>
    <th>Institución</th>
    <th>Valor Comercial</th>
    <th>Prendado</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
</tbody>
</table></div>

</br>

<h3>Automóviles</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Tipo</th>
    <th>Marca</th>
    <th>Modelo</th>
    <th>Patente</th>
    <th>Año de Auto</th>
    <th>Valor Comercial</th>
    <th>Prendado</th>
</tr>
</thead>
<tbody><tr><td>data</td><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
<tr><td>data</td><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
</tbody>
</table></div>
</br>

<h3>Participación en Sociedades</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Nombre Sociedad</th>
    <th>Rut</th>
    <th>Porcentaje (%)</th>
    <th>Valor Participativo</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
</tbody>
</table></div>


</br>

</form>
</div>

<!------- PAGINA 4 --------->

<div class=\"container\">

<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
</br>
<h4>Pasivos</h4>
<h3>Hipotecario</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Institución</th>
    <th>Pago Mensual</th>
    <th>Deuda Total</th>
    <th>Vencimiento Final</th>
    <th>Deuda Vigente</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>
</br>

<h3>Tarjeta de Crédito</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Tipo de Tarjeta</th>
    <th>Institución</th>
    <th>Monto Aprobado</th>
    <th>Monto Utilizado</th>
    <th>Vencimiento</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>
</br>

<h3> Línea de Crédito</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Institución</th>
    <th>Monto Aprobado</th>
    <th>Monto Utilizado</th>
    <th>Vencimiento</th>
    </tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
</tr>
</tbody>
</table></div>
</br>

<h3>Créditos de Consumo</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Institución</th>
    <th>Pago Mensual</th>
    <th>Deuda Total</th>
    <th>Vencimiento Final</th>
    <th>Deuda</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>

</br>

</form>
</div>

<!------- PAGINA 5 --------->

<div class=\"container\">

<form id=\"contactform\" class=\"rounded\" method=\"post\" action=\"\">
</br>
<h4>Pedidos</h4>
<h3>Créditos de Consumo</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Monto Crédito</th>
    <th>Plazo de Crédito</th>
    <th>Degravamen</th>
    <th>Cesantía</th>
    <th>¿Para qué utilizarás el dinero?</th>
    <th>Fecha<br>
      del<br>
      pedido</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>&nbsp;</td>
  <td>data</td></tr>
</tbody>
</table></div>

</br>

<h3>Automotriz</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Monto del Auto:</th>
    <th>Monto del Píe:</th>
    <th>Plazo del Crédito</th>
    <th>¿Cuándo piensas comprar tu auto?</th>
    <th>Marca</th>
    <th>Año</th>
    </tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
</tr>
</tbody>
</table></div>
</br>

<h3>Hipotecario</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Valor Propiedad</th>
    <th>Monto Píe</th>
    <th>Porcentaje</th>
    <th>Monto de Crédito</th>
    <th>Plazo de Crédito</th>
    <th>Tipo de propiedad</th>
    <th>dfl2</th>
    <th>Fecha de Pedido</th></tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td>
  <td>data</td></tr>
</tbody>
</table></div>

</br>

<h3>Consolidación de deuda / Refinanciamiento</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Crédito a
      Consolidar /
      Refinanciar</th>
    <th>Monto de
      Crédito</th>
    <th>Plazo
      Solicitado</th>
    <th>Fecha
      del
      pedido</th>
    </tr></thead>
<tbody><tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr><td>data</td><td>data</td><td>data</td>
  <td>data</td>
  </tr>
<tr class=\"alt\"><td>data</td><td>data</td><td>data</td>
  <td>data</td>
</tr>
</tbody>
</table></div>

</br>

<h3>Pedidos / Cuenta Corriente</h3>
<div class=\"datagrid\"><table>
<thead><tr>
    <th>Producto</th>
    <th>Fecha del pedido</th>
    </tr></thead>
<tbody><tr><td>data</td><td>data</td>
</tr>
</tbody>
</table></div>

</br>

</form>

</div>


</body>

</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:testPdf.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
