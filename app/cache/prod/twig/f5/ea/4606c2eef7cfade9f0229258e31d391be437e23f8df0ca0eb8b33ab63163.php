<?php

/* BenchAdminBundle:Default:principal.html.twig */
class __TwigTemplate_f5ea4606c2eef7cfade9f0229258e31d391be437e23f8df0ca0eb8b33ab63163 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\" />
  <title>Administrador Bench Banking</title>
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
  <meta name=\"description\" content=\"\" />
  <meta name=\"author\" content=\"\" />
<meta name=\"robots\" content=\"noindex\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, FOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"INDEX, NOFOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, NOFOLLOW\">

  <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"main-theme-script\" />
  <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-specific-script\" />
  <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/mask.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.js"), "html", null, true);
        echo "\"></script>
  <script type=\"text/javascript\" src=\"php/combo.js\"></script>
  <script type=\"text/javascript\" src=\"php/asignar.js\"></script>




     <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

     <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.columnFilter.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

  
    <script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/usuarios/listausuario.js"), "html", null, true);
        echo "\"></script>


    <script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.avgrund.js"), "html", null, true);
        echo "\"></script>

    <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/avgrund.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"main-theme-script\" />

  
  <link href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/css/datepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  


  <link rel=\"stylesheet\" type=\"text/css\" media=\"screen,projection\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />
  

  <link type=\"text/css\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.intenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />   

 
  <link href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/simplenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  

  <link rel=\"shortcut icon\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/favicon.ico"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-144-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-114-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-72-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-57-precomposed.png"), "html", null, true);
        echo "\" />
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>
<body id=\"dashboard\" class=\"hidden\">






 <div id=\"div_carga\">
   <img id=\"cargador\" src=\"./img/loader.gif\"/>
   </div>

<!-- Top navigation bar -->
<div class=\"navbar navbar-fixed-top\">
  <div class=\"navbar-inner\">
    <div class=\"container-fluid\">
      <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </a>
      <a class=\"brand\" href=\"index.html\">Bench banking</a>
      <div class=\"btn-group pull-right\">
        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
          <i class=\"icon-user\"></i> 
          <span class=\"caret\"></span>
        </a>
        <ul class=\"dropdown-menu\">
        
          <li><a href=\"login.html\">Logout</a></li>
        </ul>
      </div>
      <div class=\"nav-collapse\">
          
          
        <ul class=\"nav\">
        
              
          <li class=\"dropdown\">
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  Menu
                      <b class=\"caret\"></b>
                </a>
                <ul class=\"dropdown-menu\">
                \t  <li class=\"nav-header\">Main</li>        
          <li class=\"active\"><a href=\"listados.php\"><i class=\"icon-home\"></i> Home </a></li>
          <li><a href=\"listados.php\"><i class=\"icon-edit\"></i>Listado de Usuarios </a></li>
          
               <h6><a href=\"todos\">Ver todos los usuarios</a></h6>
           <li><a href=\"listados2.php\"><i class=\"icon-edit\"></i>Listad de usuarios Archivados</a>11</li>
         
                </ul>
          </li>
       
        </ul>
      </div>
    </div>
  </div>
</div>
  
<div class=\"container-fluid\">
  <div class=\"row-fluid\">
    
    <!-- Bread Crumb Navigation -->
  <div class=\"span10\">
      

   
  
         
         <!-- Portlet Set 5 -->
             

             <h6><a href=\"todos.php\">Ver todos los usuarios</a></h6>
        
\t  <!-- Table -->
      <div  style=\"width:1900px; height:auto; margin:auto;\">
      
        <div class=\"span12\">
         \t <!-- Portlet: Browser Usage Graph -->
             <div class=\"box\" id=\"box-0\">
              <h4 class=\"box-header round-top\">Usuarios
                  <a class=\"box-btn\" title=\"close\"><i class=\"icon-remove\"></i></a>
                     <a class=\"box-btn\" title=\"toggle\"><i class=\"icon-minus\"></i></a>  
                 
              </h4>         
              <div class=\"box-container-toggle\">
                  <div class=\"box-content\" style=\"\">
                    

      
      

                     <section id=\"contenido\">
                         
                         
          
                         



                     </section>










                  </div>
              </div>
            </div><!--/span-->




   </div>
      </div>  
 
    </div><!--/span-->
  </div><!--/row-->



<div style=\"clear:both;\"></div>










  <footer>
    <p>&copy; Bench Banking</p>
  </footer>
    <div id=\"box-config-modal\" class=\"modal hide fade in\" style=\"display: none;\">
      <div class=\"modal-header\">
        <button class=\"close\" data-dismiss=\"modal\">×</button>
        <h3></h3>
      </div>
      <div class=\"modal-body\">
        <p></p>
      </div>
      <div class=\"modal-footer\">
        <a href=\"#\" class=\"btn btn-primary\" data-dismiss=\"modal\"></a>
        <a href=\"#\" class=\"btn\" data-dismiss=\"modal\"></a>
      </div>
    </div>
</div><!--/.fluid-container-->



    
  
    <script src=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.core.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
    
  
    <script src=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js"), "html", null, true);
        echo "\"></script>

 
    <script src=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/bootbox/bootbox.min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>

\t\t
    <!-- jQuery Cookie -->    
    <script src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.cookie/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Full Calender -->
    <script type='text/javascript' src='";
        // line 237
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
    
    <!-- CK Editor -->
\t<script type=\"text/javascript\" src=\"";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Chosen multiselect -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 243
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script>  
    
    <!-- Uniform -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 246
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
    
    
\t
    <script src=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/simplenso/simplenso.js"), "html", null, true);
        echo "\"></script>
  </body>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:principal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  369 => 250,  362 => 246,  356 => 243,  350 => 240,  344 => 237,  338 => 234,  331 => 230,  326 => 228,  322 => 227,  316 => 224,  312 => 223,  306 => 220,  302 => 219,  298 => 218,  294 => 217,  290 => 216,  126 => 55,  122 => 54,  118 => 53,  114 => 52,  110 => 51,  104 => 48,  98 => 45,  92 => 42,  85 => 38,  79 => 35,  74 => 33,  68 => 30,  62 => 27,  57 => 25,  47 => 18,  43 => 17,  39 => 16,  35 => 15,  19 => 1,);
    }
}
