<?php

/* BenchFilepickerBundle:Default:cotizacionAfp.html.twig */
class __TwigTemplate_6083bf27031ffcbe4e2c11e199d81d521ffd964dc3f7b98b906011b92f7a0b84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<script type=\"text/javascript\" src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/js/cartolaAfp.js"), "html", null, true);
        echo "\"></script>
<div class=\"container-upload\">
<div class=\"box-upload\">
<span class=\"orange\">Cartola de últimas 12 cotizaciones afp. <span class=\"color-porcentaje\" id=\"porCartolaAfp\"></span><span class=\"carga-ie\" id=\"cartolaAfpie\"><img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchfilepicker/img/load.gif"), "html", null, true);
        echo "\"></span> </span>

<span class=\"subir\" id=\"porcentajefile\"> </span> 
        <ul id=\"listaAfp\" style=\"width:600px;height:auto;\">
            
        <li>
            
            
      
        <span id=\"nombrefile\"></span>
       
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> </span>
        
        
        <!--   
        <img src=\"img/fileicon.jpg\">
        <span id=\"nombrefile\">File Name</span>
        <a href=\"#\"><span style=\"float:right; color:#F60; font-weight:bold; margin:0 10px;\" id=\"porcentajefile\">X</span></a>
        <span style=\"float:right; color:#999;\" id=\"porcentajefile\"> <p>100%</p></span>-->
        
        </li>
        
       
        
        ";
        // line 30
        if ((isset($context["archivo3"]) ? $context["archivo3"] : null)) {
            // line 31
            echo "        
        ";
            // line 32
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["archivo3"]);
            foreach ($context['_seq'] as $context["_key"] => $context["archivo3"]) {
                // line 33
                echo "       
     
            
            <li class=\"li-upload\" id=\"itemAfp-";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo3"], "llave", array()), "html", null, true);
                echo "\">
                
            <a href=\"";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo3"], "url", array()), "html", null, true);
                echo "\"  target=\"_blank\"> <img src=\"img/fileicon.jpg\"> <span id=\"nombrefile\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo3"], "nombreArchivo", array()), "html", null, true);
                echo "</a></span><button class=\"del_button\" id=\"del-";
                echo twig_escape_filter($this->env, $this->getAttribute($context["archivo3"], "llave", array()), "html", null, true);
                echo "\">X</button>
            
            </li>
            
         
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivo3'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "        ";
        }
        echo "    
            
        
        
                                                                                                                                                                                                      
        </ul>

        <div style=\"clear: both;\"></div>

        <!--<input type=\"file\" id=\"AgregarCartolaAfp\" class=\"buttonupload\">-->
        <div class=\"file-input-wrapper\">
        <button class=\"btn-file-input\">Agregar Archivos</button>
        <input type=\"file\" name=\"file\" id=\"AgregarCartolaAfp\"/>
       </div>
        
        
        
      
        
        
        
     
        
        
        
        </div>
    
    </div>


";
    }

    public function getTemplateName()
    {
        return "BenchFilepickerBundle:Default:cotizacionAfp.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 44,  74 => 38,  69 => 36,  64 => 33,  60 => 32,  57 => 31,  55 => 30,  28 => 6,  22 => 3,  19 => 2,);
    }
}
