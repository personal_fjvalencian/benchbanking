<?php

/* BenchUploadArchivosBundle:Default:index.html.twig */
class __TwigTemplate_da26c9e991e14c59de99c83e8ce4c22146e923d01c0e3d90c34372c7dda0b966 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
  
<html>

<head>
        
<meta http-equiv=\"Content-Type\" content=\"application/json;charset=UTF-8\">


<style type=\"text/css\">

</style>
<script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/css/ui-lightness/jquery-ui-1.8.14.custom.css"), "html", null, true);
        echo "\"></script>

<link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/css/ui-lightness/jquery-ui-1.8.14.custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/css/fileUploader.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

<script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/js/jquery-ui-1.8.14.custom.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/js/jquery.fileUploader.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script type=\"text/javascript\">
\t\tjQuery(function(\$){
\t\t\t\$('.fileUpload').fileUploader();
\t\t});
\t</script>


<div id=\"main_container\">
\t
\t<form action=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("subir2");
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
\t\t<input type=\"file\" name=\"userfile\" class=\"fileUpload\" multiple>
\t\t
\t\t<button id=\"px-submit\" type=\"submit\">Subir arhivos</button>
\t\t<button id=\"px-clear\" type=\"reset\">Borrar</button>
\t</form>
\t
</div>

";
    }

    public function getTemplateName()
    {
        return "BenchUploadArchivosBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 31,  52 => 20,  48 => 19,  42 => 16,  38 => 15,  33 => 13,  19 => 1,);
    }
}
