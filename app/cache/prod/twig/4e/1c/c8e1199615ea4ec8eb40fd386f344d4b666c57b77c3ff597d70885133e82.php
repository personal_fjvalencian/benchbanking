<?php

/* BenchUploadArchivosBundle:Default:sueldovariable6.html.twig */
class __TwigTemplate_4e1cc8e1199615ea4ec8eb40fd386f344d4b666c57b77c3ff597d70885133e82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<script src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/css/ui-lightness/jquery-ui-1.8.14.custom.css"), "html", null, true);
        echo "\"></script>

<link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/css/ui-lightness/jquery-ui-1.8.14.custom.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/css/fileUploader.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

<script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/js/jquery-ui-1.8.14.custom.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchuploadarchivos/js/jquery.fileUploader.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

<script type=\"text/javascript\">
\t\tjQuery(function(\$){
\t\t\t\$('.fileUpload2').fileUploader();
\t\t});
\t</script>


<div id=\"main_container\">
    
\t
\t<form action=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("subir3");
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
\t\t<input type=\"file\" name=\"userfile\" class=\"fileUpload2\" multiple>
\t\t
\t\t<button id=\"px-submit\" type=\"submit\">Subir arhivos</button>
\t\t<button id=\"px-clear\" type=\"reset\">Borrar</button>
\t</form>
\t
</div>
        
";
    }

    public function getTemplateName()
    {
        return "BenchUploadArchivosBundle:Default:sueldovariable6.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 20,  40 => 8,  36 => 7,  31 => 5,  27 => 4,  22 => 2,  19 => 1,);
    }
}
