<?php

/* BenchPaginasBundle:Default:index.html.twig */
class __TwigTemplate_e7ca8fdfd108ddda051efd3b97d075207b6648aa3bc5bdde8f5449827d002349 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
 <html>
<head>

<meta http-equiv=\"X-Frame-Options\" content=\"deny\">
<meta http-equiv=\"cache-control\" content=\"max-age=0\" />
<meta http-equiv=\"cache-control\" content=\"no-cache\" />
<meta http-equiv=\"expires\" content=\"0\" />
<meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\" />
<meta http-equiv=\"pragma\" content=\"no-cache\" />
        
<script type=\"text/javascript\">
var WRInitTime=(new Date()).getTime(); </script>

    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/ship.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
<link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/basic.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
<link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/flexslider.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
<script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.flexslider-min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.carouFredSel-6.1.0-packed.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciador.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/botoneshead.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/html5.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.simplemodal.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/basic.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/parsley.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/parsley.extend.js"), "html", null, true);
        echo "\"></script>

<META name=\"keywords\" content=\"credito hipotecario, credito consumo, simulador, tasa, banco\">
<meta name=\"google-site-verification\" content=\"4sibG4Sz2wGxTyteKnT1E8uJM6vt67srq5ANMPkQ5Vg\" />
<title> BenchBanking │ Créditos Convenientes </title>
<META name=\"description\" content=\"Créditos más convenientes y mejor informados, compara las tasas antes de contratarlo\">  
<link rel=\"shortcut icon\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
<link rel=\"icon\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
    </head>

<body>

 <div class=\"spinner\">
      <div class=\"bounce1\"></div>
      <div class=\"bounce2\"></div>
      <div class=\"bounce3\"></div>
    </div>

    

    <!-- ClickTale Top part -->
<script type=\"text/javascript\">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->



    <input type=\"hidden\" id=\"token\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
    
    
    
    ";
        // line 62
        $this->env->loadTemplate("BenchPaginasBundle:Default:header.html.twig")->display($context);
        // line 63
        echo "    ";
        $this->displayBlock('header', $context, $blocks);
        // line 65
        echo "





    
    
    <div class=\"wrapper\" style=\"padding-top:20px;\">


  <div class=\"creditosHome\">
  

   <section class=\"credito1\">

      <a href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("credito1");
        echo "\" target=\"_self\">
       <img id=\"\" src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_hi_gray.png"), "html", null, true);
        echo "\">
        <h3 style=\"color:#f95b00;\" class=\"tituloCreditos\" >Cr&eacute;dito Hipotecario</h3>

      </a>
       <button class=\"classname\" style=\"margin-top:10px;\"><a href=\"";
        // line 86
        echo $this->env->getExtension('routing')->getPath("credito1");
        echo "\" target=\"_self\"> <p style=\"color:#f0f0f0;font-size:14px; \">Simular</p></a></button>


   </section>
   


    <section class=\"credito1\">

      <a href=\"";
        // line 95
        echo $this->env->getExtension('routing')->getPath("credito2");
        echo "\" target=\"_self\">
     <img id=\"\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_cc_gray.png"), "html", null, true);
        echo "\">
     <h3  style=\"color:#f95b00;\"  class=\"tituloCreditos\">Cr&eacute;dito de Consumo</h3></a>


    <button class=\"classname\" style=\"margin-top:10px;\"><a href=\"";
        // line 100
        echo $this->env->getExtension('routing')->getPath("credito2");
        echo "\" target=\"_self\"> <p style=\"color:#f0f0f0;font-size:14px; \">Simular</p></a></button>





    </section>


<section class=\"credito1\">
<a href=\"";
        // line 110
        echo $this->env->getExtension('routing')->getPath("credito3");
        echo "\" target=\"_self\">
  <img id=\"\" src=\"";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_cdd_gray.png"), "html", null, true);
        echo "\">
 <h3 style=\"color:#f95b00;\"  class=\"tituloCreditos\" >Consolidaci&oacute;n de deuda </h3>

    <button class=\"classname\" style=\"margin-top:10px;\"><a href=\"";
        // line 114
        echo $this->env->getExtension('routing')->getPath("credito3");
        echo "\" target=\"_self\"> <p style=\"color:#f0f0f0;font-size:14px; \">Simular</p></a></button>


</a>

   </section>

    <section class=\"credito1\">

    

    <a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("credito4");
        echo "\"  target=\"_self\">
    <img id=\"\" src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_ccu_gray.png"), "html", null, true);
        echo "\">

    <h3 style=\"color:#f95b00;\"  class=\"tituloCreditos\">Cuenta Corriente</h3>

        <button class=\"classname\" style=\"margin-top:10px;\"><a href=\"";
        // line 130
        echo $this->env->getExtension('routing')->getPath("credito4");
        echo "\" target=\"_self\"> <p style=\"color:#f0f0f0;font-size:14px; \">Simular</p></a></button>


    </a>


   </section>





  <div>
  </div>


  </div>


  <div style=\"width:200px;height:30px;\"></div>
<div class=\"clearfix\"></div>        


  <div class=\"box-slider\">



  \t
  <div class=\"flexslider\">
\t    <ul class=\"slides\">
\t    \t<li>
\t    \t\t
                    <img src=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/slider/info01.jpg"), "html", null, true);
        echo "\"/>
                <!--<p class=\"flex-caption\">Captions and cupcakes. Winning combination.</p>-->
\t    \t</li>
            <li>
\t    \t\t<img src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/slider/info02.jpg"), "html", null, true);
        echo "\"/>
\t    \t</li>
            <li>
\t    \t\t<img src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/slider/info03.jpg"), "html", null, true);
        echo "\"/>
\t    \t</li>
            <li>
\t    \t\t<img src=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/slider/info04.jpg"), "html", null, true);
        echo "\"/>
\t    \t</li>
\t    \t<li>
\t    \t\t<div id='basic-modal'><a href=\"#\" class=\"basic5\"><img src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/slider/02.jpg"), "html", null, true);
        echo "\"/></a></div>
\t    \t</li>
\t    </ul>
\t  </div>
              </div>


  <div class=\"box-datos\">
      <!--<a target=\"_blank\" href='http://benchbanking.blogspot.com/'><img src=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/caluga_blog.png"), "html", null, true);
        echo "\"/></a>-->
      <div class=\"box-caluga01\" style=\"margin-top:-1%;width:358px;height:273px; background-color:; \">
          <a href=\"http://benchbanking.blogspot.com/\"  target=\"_blank\">
          <img src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/blogGrande.png"), "html", null, true);
        echo "\" style=\"position:relative;width:358px;height:273px;margin:auto;\"/>
          </a>
          
      </div>

  </div>
  <!--
  <a href=\"";
        // line 193
        echo $this->env->getExtension('routing')->getPath("registro");
        echo "\" class=\"btn-cotiza\" title=\"cotiza y compara\"><span class=\"displace\">cotiza7 y compara</span></a>
  -->

  <!-- Testimonios -->
     
<!--
      <div class=\"list_carousel\">
\t\t<ul id=\"foo2\">
        <li>
          <div class=\"box-photo-test\"><div id='basic-modal'><a href=\"#\" class=\"basic5\"><img src=\"";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/testimonies/test_img00.jpg"), "html", null, true);
        echo "\" width=\"145\" height=\"86\"></a> Video</div></div>
 <p style=\"display:block; text-align:left; margin-top:10px;\">Conócenos a través de nuestro video explicativo.</p>
\t\t  </li>
\t\t\t\t\t<li><div class=\"box-photo-test\"><img src=\"";
        // line 205
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/testimonies/test_img01.jpg"), "html", null, true);
        echo "\" width=\"145\" height=\"86\">Guillermo</div>
 <p style=\"display:block; text-align:left; margin-top:10px;\">BenchBanking me ha ayudado a tomar la mejor decisión para elegir mi crédito automotriz.</p>
\t\t  </li>
\t\t\t\t\t<li>
 <div class=\"box-photo-test\"><img src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/testimonies/test_img02.jpg"), "html", null, true);
        echo "\" width=\"145\" height=\"86\">Juan</div>
 <p style=\"display:block; text-align:left; margin-top:10px;\">Gracias a Bench Banking pude continuar mis estudios en el extranjero.</p>
\t\t  </li>
\t<li>
 <div class=\"box-photo-test\"><img src=\"";
        // line 213
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/testimonies/test_img03.jpg"), "html", null, true);
        echo "\" width=\"145\" height=\"86\">Loreto y Matías</div>
 <p style=\"display:block; text-align:left; margin-top:10px;\">Al cotizar mi crédito hipotecario pudimos acceder a la casa de nuestros sueños.</p>
\t</li>
            <li>
          <div class=\"box-photo-test\"><div id='basic-modal'><a href=\"#\" class=\"basic5\"><img src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/testimonies/test_img00.jpg"), "html", null, true);
        echo "\" width=\"145\" height=\"86\"></a> Video</div></div>
 <p style=\"display:block; text-align:left; margin-top:10px;\">Conócenos a través de nuestro video explicativo.</p>
\t\t  </li>
<li>
 <div class=\"box-photo-test\"><img src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/testimonies/test_img01.jpg"), "html", null, true);
        echo "\" width=\"145\" height=\"86\">Guillermo</div>
 <p style=\"display:block; text-align:left; margin-top:10px;\">BenchBanking me a ayudado a tomar la mejor decisión para elegir mi crédito hipotecario, ahora disfruto junto a mi familia la casa de mis sueños.</p>
 ..
</li>
<li>
 <div class=\"box-photo-test\"><img src=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/testimonies/test_img02.jpg"), "html", null, true);
        echo "\" width=\"145\" height=\"86\">Juan</div>
 <p style=\"display:block; text-align:left; margin-top:10px;\">Gracias a Bench Banking pude continuar mis estudios en el extranjero.</p>
</li>
<li>
 <div class=\"box-photo-test\"><img src=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/testimonies/test_img03.jpg"), "html", null, true);
        echo "\" width=\"145\" height=\"86\">Loreto y Matías</div>
 <p style=\"display:block; text-align:left; margin-top:10px;\">Al cotizar mi crédito hipotecario pudimos acceder a la casa de nuestros sueños.</p>
</li>
\t\t\t\t</ul>
\t\t
\t    <a id=\"prev2\" class=\"prev\" href=\"#\"><img src=\"";
        // line 235
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/test_arrow_pre.png"), "html", null, true);
        echo "\"></a>
\t    <a id=\"next2\" class=\"next\" href=\"#\"><img src=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/test_arrow_next.png"), "html", null, true);
        echo "\"></a>



      </div> -->


      <!-- fin de testimonios -->



<div class=\"clearfix\"></div>                
</div>

 <div style=\"width:10px;height:20px;position: relative;\"></div>
<div class=\"clearfix\"></div>

 <script>


 </script>

    
    
      ";
        // line 260
        $this->env->loadTemplate("BenchPaginasBundle:Default:footer.html.twig")->display($context);
        // line 261
        echo "



  <!-- ClickTale Bottom part -->
  
  <script type='text/javascript'>
  // The ClickTale Balkan Tracking Code may be programmatically customized using hooks:
  // 
  //   function ClickTalePreRecordingHook() { /* place your customized code here */  }
  //
  // For details about ClickTale hooks, please consult the wiki page http://wiki.clicktale.com/Article/Customizing_code_version_2
  
  document.write(unescape(\"%3Cscript%20src='\"+
  (document.location.protocol=='https:'?
  \"https://clicktalecdn.sslcs.cdngc.net/www02/ptc/16e0b4e0-35fc-48d1-ba47-23a3f23ad1e0.js\":
  \"http://cdn.clicktale.net/www02/ptc/16e0b4e0-35fc-48d1-ba47-23a3f23ad1e0.js\")+\"'%20type='text/javascript'%3E%3C/script%3E\"));
  </script>
  




</body>
";
    }

    // line 63
    public function block_header($context, array $blocks = array())
    {
        // line 64
        echo "    ";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  462 => 64,  459 => 63,  431 => 261,  429 => 260,  402 => 236,  398 => 235,  390 => 230,  383 => 226,  375 => 221,  368 => 217,  361 => 213,  354 => 209,  347 => 205,  341 => 202,  329 => 193,  319 => 186,  313 => 183,  302 => 175,  296 => 172,  290 => 169,  284 => 166,  277 => 162,  242 => 130,  235 => 126,  231 => 125,  217 => 114,  211 => 111,  207 => 110,  194 => 100,  187 => 96,  183 => 95,  171 => 86,  164 => 82,  160 => 81,  142 => 65,  139 => 63,  137 => 62,  130 => 58,  106 => 37,  102 => 36,  93 => 30,  89 => 29,  85 => 28,  81 => 27,  77 => 26,  73 => 25,  69 => 24,  65 => 23,  61 => 22,  57 => 21,  53 => 20,  49 => 19,  45 => 18,  41 => 17,  37 => 16,  20 => 1,);
    }
}
