<?php

/* AliDatatableBundle:Main:index.html.twig */
class __TwigTemplate_e78999949a0ff53a8837326f35ad6c2522095059cbab1793de67dde92c61e9dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('main', $context, $blocks);
    }

    public function block_main($context, array $blocks = array())
    {
        // line 2
        echo "    <script type=\"text/javascript\">
    \$(document).ready(function(){
        function getId() { return \"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\"; };
        function getWrapper() { return  \"#";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "_wrapper\"; };
        var s = getWrapper();


        // Archivar action





        \$(\"#";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\").on(\"click\",\".pdf_button\" , function(e){


            var clickedID = this.id.split('=');
            var DbNumberID = clickedID[1]; // toma el numero del array
            console.log(DbNumberID);

            \$(\"#bbva\").attr('href', '../banco/reporte/bbva/'+ DbNumberID + '/');
            \$(\"#bbva2\").attr('href', '../banco/reporte/bbva2/'+ DbNumberID + '/');
            \$(\"#estandar\").attr('href', '../adminBench/pCrearPdf/'+ DbNumberID );
            \$(\"#bci\").attr('href', '../banco/reporte/bci/'+ DbNumberID + '/');
            \$(\"#consorcio\").attr('href', '../banco/reporte/consorcio/'+ DbNumberID + '/');
            \$(\"#metlife\").attr('href', '../banco/reporte/metlife/'+ DbNumberID + '/');
            \$(\"#cruzdelsur\").attr('href', '../banco/reporte/cruzdelsur/'+ DbNumberID + '/');

            

            \$('#myModal').modal('show');


        });


        \$(\"#";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\").on(\"click\",\".archivar_button\" , function(e){

            var clickedID = this.id.split('=');
            var DbNumberID = clickedID[1]; // toma el numero del array
            console.log(DbNumberID);


            var myData = 'archivar='+ DbNumberID;
            console.log(myData);
            console.log(DbNumberID);


            jQuery.ajax({
                type: \"POST\",

                url: \"archivarUsuario\",
                dataType : \"text\",
                data:myData,
                success:function(response){

                    \$('#itemusuario_'+DbNumberID).hide('slow');

                    location.reload();






                },
                error:function(xhr, ajaxOptions, thrownError){

                    alert(thrownError);

                }


            });




        });


        var multiple = ";
        // line 83
        if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
            echo " true ";
        } else {
            echo " false ";
        }
        echo ";
        var multiple_rawhtml = '';
        ";
        // line 85
        if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
            // line 86
            echo "        var multiple_rawhtml = '\\
                    <label class=\"dataTables_multiple\">\\
                    <span style=\"margin:0 40px 0 0px;\">\\
                        Execute\\
                        <select name=\"dataTables[select]\">\\
                            ";
            // line 91
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["multiple"]) ? $context["multiple"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                echo "\\
                            <option value=\"";
                // line 92
                echo $this->env->getExtension('routing')->getPath($this->getAttribute($context["item"], "route", array()));
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                echo "</option>\\
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 93
            echo "\\
                        </select>\\
                        <button class=\"btn-datatable-multiple\" >OK</button>\\
                    <span>\\
                    </label>';
        ";
        }
        // line 99
        echo "        var \$js_conf = ";
        echo (isset($context["js_conf"]) ? $context["js_conf"] : null);
        echo ";
        var \$js = {};
        for(key in \$js_conf) {
            if (\$js_conf.hasOwnProperty(key)) {
                eval ( ' \$js[\"'+key+'\"] = ' + \$js_conf[key]+ ' ; ' );
            }
        }
        var \$options = ";
        // line 106
        echo (isset($context["js"]) ? $context["js"] : null);
        echo ";
        var \$aoColumnDefs = new Array();
        ";
        // line 108
        if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
            // line 109
            echo "        \$aoColumnDefs.push({ 'bSortable': false, 'aTargets': [ 0 ] });
        ";
        }
        // line 111
        echo "        ";
        if (((isset($context["action"]) ? $context["action"] : null) && (!(isset($context["action_twig"]) ? $context["action_twig"] : null)))) {
            // line 112
            echo "        \$aoColumnDefs.push({
            \"fnRender\": function ( oObj ) {
                var \$edit_url = strtr(
                        \"";
            // line 115
            echo $this->env->getExtension('routing')->getPath((isset($context["edit_route"]) ? $context["edit_route"] : null), array("id" => "xx"));
            echo "\",
                        { \"xx\": oObj.aData[";
            // line 116
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] }
                );


                var \$ver_url = strtr(
                        \"";
            // line 121
            echo $this->env->getExtension('routing')->getPath((isset($context["ver_route"]) ? $context["ver_route"] : null), array("id" => "xx"));
            echo "\",
                        { \"xx\": oObj.aData[";
            // line 122
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] }
                );


                var \$UpDicom_url = strtr(
                        \"";
            // line 127
            echo $this->env->getExtension('routing')->getPath((isset($context["UpDicom_url"]) ? $context["UpDicom_url"] : null), array("id" => "xx"));
            echo "\",
                        { \"xx\": oObj.aData[";
            // line 128
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] }
                );


                var \$Up_url = strtr(
                        \"";
            // line 133
            echo $this->env->getExtension('routing')->getPath((isset($context["Up_url"]) ? $context["Up_url"] : null), array("id" => "xx"));
            echo "\",
                        { \"xx\": oObj.aData[";
            // line 134
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] }
                );












                var \$link_edit = \"<a  style='float:right; margin-right:10px' class='dialog' title='edit'\";







                \$link_edit += \"href='\"+\$edit_url+\"'>\";
                \$link_edit += \"";
            // line 157
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.edit"), "html", null, true);
            echo "</a>\";
                var \$delete_url = strtr(
                        \"";
            // line 159
            echo $this->env->getExtension('routing')->getPath((isset($context["delete_route"]) ? $context["delete_route"] : null), array("id" => "xx"));
            echo "\",
                        { \"xx\": oObj.aData[";
            // line 160
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] }
                );




                var \$otra_route = strtr(
                        \"";
            // line 167
            echo $this->env->getExtension('routing')->getPath((isset($context["otra_route"]) ? $context["otra_route"] : null), array("id" => "xx"));
            echo "\",
                        { \"xx\": oObj.aData[";
            // line 168
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] } );


                var \$archivar_route = strtr(
                        \"";
            // line 172
            echo $this->env->getExtension('routing')->getPath((isset($context["archivar_route"]) ? $context["archivar_route"] : null), array("id" => "xx"));
            echo "\",
                        { \"xx\": oObj.aData[";
            // line 173
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] } );




                var \$ver_route = strtr(
                        \"";
            // line 179
            echo $this->env->getExtension('routing')->getPath((isset($context["ver_route"]) ? $context["ver_route"] : null), array("id" => "xx"));
            echo "\",
                        { \"xx\": oObj.aData[";
            // line 180
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] } );




                var \$link_ver =  \"<a \";
                \$link_ver += \"href='\"+\$ver_url+\"' target='_blank'>\";
                \$link_ver += \"";
            // line 187
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver Detalle"), "html", null, true);
            echo "</a>\";




                var \$link_DicomUp =  \"<a \";
                \$link_DicomUp+= \"href='\"+\$UpDicom_url+\"' target='_blank'>\";
                \$link_DicomUp += \"";
            // line 194
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ad. Dicom "), "html", null, true);
            echo "</a>\";



                var \$link_Archivos=  \"<a \";
                \$link_Archivos+= \"href='\"+\$Up_url+\"' target='_blank'>\";
                \$link_Archivos += \"";
            // line 200
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ad. Otro"), "html", null, true);
            echo "</a>\";




                var \$link_archivar =  \"<a \";
                \$link_archivar += \"href='#' class='archivar_button' id='bot-\"+\$archivar_route+\"' >\";
                \$link_archivar += \"";
            // line 207
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Archivar"), "html", null, true);
            echo "</a>\";


                var \$link_Cotizar=  \"<a \";
                \$link_Cotizar+= \"href='\"+\$delete_url+\"' target='_blank'>\";
                \$link_Cotizar += \"";
            // line 212
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ad. Cotizaciones"), "html", null, true);
            echo "</a>\";








           /*
                var \$link_pdf =   \"<a \";
                \$link_pdf += \"href='\"+\$otra_route+\"'  target='_blank'>\";
                \$link_pdf += \"";
            // line 224
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear pdf"), "html", null, true);
            echo "</a>\";    */




                var \$link_pdf=  \"<a \";
                \$link_pdf += \"href='#' class='pdf_button' id='bot-\"+\$archivar_route+\"' >\";
                \$link_pdf += \"";
            // line 231
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear pdf"), "html", null, true);
            echo "</a>\";






                var \$link_delete = \"<form style='float:right' class='form' action='\"+\$delete_url+\"' method='post'>\";
                \$link_delete += strtr(
                        '";
            // line 240
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["delete_form"]) ? $context["delete_form"] : null), 'widget');
            echo "',
                        { \"_id_\": oObj.aData[";
            // line 241
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] }
                );



                \$link_delete += \" </form>\";
                var \$out =  \$link_pdf + \" <br>  \" +  \$link_ver  + \"<br> \" + \$link_archivar + \"<br> \" +\$link_DicomUp  + '<br> ' + \$link_Archivos + '<br>' + \$link_Cotizar;
                if (oObj.aData[";
            // line 248
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo "-1] == null )
                {
                    \$out = \"<div style='float:right'>";
            // line 250
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.no_action"), "html", null, true);
            echo "</div>\";
                }
                return \$out
            },
            \"aTargets\": [ multiple ? ";
            // line 254
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo " : (";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["fields"]) ? $context["fields"] : null)), "html", null, true);
            echo " -1)  ]
        });
        ";
        }
        // line 257
        echo "        var \$defaults = {
            \"bJQueryUI\": true,
            \"sPaginationType\": \"full_numbers\",
            \"aLengthMenu\": [[5,10, 25, 50, -1], [5,10, 25, 50, \"All\"]],
            \"iDisplayLength\": 10,
            \"bServerSide\": true,
            \"bProcessing\": true,
            \"sAjaxSource\": null,
            \"bPaginate\": true,
            \"bLengthChange\": true,
            \"aoColumnDefs\": \$aoColumnDefs,
            \"fnDrawCallback\": function(oSettings) {
                var s = getWrapper();
                if( multiple && \$('.dataTables_multiple',\$(s)).length==0){
                    \$(s+' .dataTables_length').prepend(multiple_rawhtml);
                }
            },
            \"bSort\": true,
            \"bFilter\": ";
        // line 275
        if ((isset($context["search"]) ? $context["search"] : null)) {
            echo " true ";
        } else {
            echo " false ";
        }
        echo ",
            \"oLanguage\": {
                \"sProcessing\":     '";
        // line 277
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sProcessing"), "html", null, true);
        echo "',
                \"sLengthMenu\":     '";
        // line 278
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sLengthMenu"), "html", null, true);
        echo "',
                \"sZeroRecords\":    '";
        // line 279
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sZeroRecords"), "html", null, true);
        echo "',
                \"sInfo\":           '";
        // line 280
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sInfo"), "html", null, true);
        echo "',
                \"sInfoEmpty\":      '";
        // line 281
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sInfoEmpty"), "html", null, true);
        echo "',
                \"sInfoFiltered\":   '";
        // line 282
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sInfoFiltered"), "html", null, true);
        echo "',
                \"sInfoPostFix\":    '";
        // line 283
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sInfoPostFix"), "html", null, true);
        echo "',
                \"sSearch\":         '";
        // line 284
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sSearch"), "html", null, true);
        echo "',
                \"sLoadingRecords\": '";
        // line 285
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.sLoadingRecords"), "html", null, true);
        echo "',
                \"sUrl\":            \"\",
                \"oPaginate\": {
                    \"sFirst\":    '";
        // line 288
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Primero"), "html", null, true);
        echo "',
                    \"sPrevious\": '";
        // line 289
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aterior"), "html", null, true);
        echo "',
                    \"sNext\":     '";
        // line 290
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Proximo"), "html", null, true);
        echo "',
                    \"sLast\":     '";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ultimo"), "html", null, true);
        echo "'
                }
            },
            \"bAutoWidth\" : false
        };
        \$.extend(\$defaults,\$js);
        \$.extend(\$defaults,\$options);
        \$('#";
        // line 298
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "').trigger('datatables_init', \$defaults );
        eval(\"var \"+ \"oTable_\"+'";
        // line 299
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "'.split('-').join('_') + \"  = \$('#";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "').dataTable(\$defaults)\");
        \$(s).on('click','.button-delete:parent',function(e){
            if (!confirm('";
        // line 301
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ali.common.confirm_delete"), "html", null, true);
        echo "')) {
                return false;
            }
            \$(this).parents('form:eq(0)').submit();
        });

        if (multiple===true) {
            var chbox =  'input:checkbox[name=\"dataTables[actions][]\"]';
            var chbox_all = 'input:checkbox[name=\"datatable_action_all\"]';
            \$(s).on('click','.btn-datatable-multiple:not(.search_init)',function(e){
                if(\$('input:focus',\$(s)).length > 0){
                    return false;
                }
                e.preventDefault();
                if(\$(chbox+':checked').length > 0){
                    if (!confirm('Are you sure ?')) {
                        return false;
                    }
                    var form = \$(this).parents('form:eq(0)');
                    var action = \$('select[name=\"dataTables[select]\"]',\$(s)).val();
                    \$.ajax({
                        type: \"POST\",
                        url: action,
                        data: form.serialize(),
                        success: function(msg) {
                            \$('#'+getId()).dataTable().fnDraw();
                            \$.unblockpage();
                        },
                        beforeSend: function() {
                            \$.blockpage({
                                msg: 'saving data'
                            });
                        }
                    });
                } else {
                    alert('You need to select at least one element.');
                }
            });
            \$(s).on('click',chbox_all,function(e){
                if(\$(this).is(':checked')) {
                    \$(chbox,\$(s)).attr(\"checked\",false).click();
                } else {
                    \$(chbox,\$(s)).attr(\"checked\",true).click();
                }
            });
        }

        ";
        // line 348
        if ((isset($context["search"]) ? $context["search"] : null)) {
            // line 349
            echo "        \$(\".dataTables_filter\").remove();
        var search_selector = \"#";
            // line 350
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
            echo " input[searchable=true]\";
        \$(search_selector).keypress( function (event) {
            var index = \$(this).attr('index');
            var oTable = eval('oTable_";
            // line 353
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
            echo "'.split('-').join('_'));
            if (event.keyCode == '13') {
                oTable.fnFilter(
                        this.value,
                        oTable.oApi._fnVisibleToColumnIndex(
                                oTable.fnSettings(),
                                index
                        )
                );
            }
        }).each( function (i) {
                    this.initVal = this.value;
                }).focus( function () {
                    if ( this.className == \"search_init\" ){
                        this.className = \"\";
                        this.value = \"\";
                    }
                }).blur( function (i) {
                    if ( this.value == \"\" ){
                        this.className = \"search_init\";
                        this.value = this.initVal;
                    }
                });
        ";
        }
        // line 377
        echo "    });
    </script>
    ";
        // line 379
        if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
            // line 380
            echo "        <form name=\"frm-";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
            echo "\">
    ";
        }
        // line 382
        echo "    <table class=\"display table table-bordered\" id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\">

        ";
        // line 384
        if ((isset($context["search"]) ? $context["search"] : null)) {
            // line 385
            echo "            ";
            $context["i"] = 0;
            // line 386
            echo "            <thead>
            <tr>
                ";
            // line 388
            if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
                // line 389
                echo "                    <th></th>
                ";
            }
            // line 391
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["fields"]) ? $context["fields"] : null));
            foreach ($context['_seq'] as $context["label"] => $context["key"]) {
                // line 392
                echo "                    ";
                if (($context["label"] != "_identifier_")) {
                    // line 393
                    echo "                        ";
                    if ((!twig_test_empty((isset($context["search_fields"]) ? $context["search_fields"] : null)))) {
                        // line 394
                        echo "                            ";
                        if (twig_in_filter((isset($context["i"]) ? $context["i"] : null), (isset($context["search_fields"]) ? $context["search_fields"] : null))) {
                            // line 395
                            echo "                                <td><input index=\"";
                            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
                            echo "\" searchable=\"true\" type=\"text\" placeholder=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Buscar"), "html", null, true);
                            echo "\" class=\"search_init\" /></td>
                            ";
                        } else {
                            // line 397
                            echo "                                <td></td>
                            ";
                        }
                        // line 399
                        echo "                        ";
                    } else {
                        // line 400
                        echo "                            <td><input index=\"";
                        echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
                        echo "\" searchable=\"true\" type=\"text\" placeholder=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Buscar "), "html", null, true);
                        echo "\" class=\"search_init\" /></td>
                        ";
                    }
                    // line 402
                    echo "                    ";
                } elseif (((($context["label"] == "_identifier_") && (isset($context["action"]) ? $context["action"] : null)) && (!(isset($context["action_twig"]) ? $context["action_twig"] : null)))) {
                    // line 403
                    echo "                        <td></td>
                    ";
                }
                // line 405
                echo "                    ";
                $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                // line 406
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['label'], $context['key'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 407
            echo "            </tr>
            </thead>
        ";
        }
        // line 410
        echo "

        <thead>
        <tr>
            ";
        // line 414
        if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
            // line 415
            echo "                <th width=\"1%\" ><input type=\"checkbox\" name=\"datatable_action_all\" /></th>
            ";
        }
        // line 417
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fields"]) ? $context["fields"] : null));
        foreach ($context['_seq'] as $context["label"] => $context["key"]) {
            // line 418
            echo "                ";
            if (($context["label"] != "_identifier_")) {
                // line 419
                echo "                    <th>";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "</th>
                ";
            }
            // line 421
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 422
        echo "            ";
        if ((isset($context["action"]) ? $context["action"] : null)) {
            // line 423
            echo "                <th>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
            echo "</th>
            ";
        }
        // line 425
        echo "        </tr>
        </thead>

    </table>
    ";
        // line 429
        if ((isset($context["multiple"]) ? $context["multiple"] : null)) {
            // line 430
            echo "        </form>
    ";
        }
        // line 432
        echo "



";
    }

    public function getTemplateName()
    {
        return "AliDatatableBundle:Main:index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  756 => 432,  752 => 430,  750 => 429,  744 => 425,  738 => 423,  735 => 422,  729 => 421,  723 => 419,  720 => 418,  715 => 417,  711 => 415,  709 => 414,  703 => 410,  698 => 407,  692 => 406,  689 => 405,  685 => 403,  682 => 402,  674 => 400,  671 => 399,  667 => 397,  659 => 395,  656 => 394,  653 => 393,  650 => 392,  645 => 391,  641 => 389,  639 => 388,  635 => 386,  632 => 385,  630 => 384,  624 => 382,  618 => 380,  616 => 379,  612 => 377,  585 => 353,  579 => 350,  576 => 349,  574 => 348,  524 => 301,  517 => 299,  513 => 298,  503 => 291,  499 => 290,  495 => 289,  491 => 288,  485 => 285,  481 => 284,  477 => 283,  473 => 282,  469 => 281,  465 => 280,  461 => 279,  457 => 278,  453 => 277,  444 => 275,  424 => 257,  416 => 254,  409 => 250,  404 => 248,  394 => 241,  390 => 240,  378 => 231,  368 => 224,  353 => 212,  345 => 207,  335 => 200,  326 => 194,  316 => 187,  306 => 180,  302 => 179,  293 => 173,  289 => 172,  282 => 168,  278 => 167,  268 => 160,  264 => 159,  259 => 157,  233 => 134,  229 => 133,  221 => 128,  217 => 127,  209 => 122,  205 => 121,  197 => 116,  193 => 115,  188 => 112,  185 => 111,  181 => 109,  179 => 108,  174 => 106,  163 => 99,  155 => 93,  145 => 92,  139 => 91,  132 => 86,  130 => 85,  121 => 83,  73 => 38,  47 => 15,  34 => 5,  30 => 4,  26 => 2,  20 => 1,);
    }
}
