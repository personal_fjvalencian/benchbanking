<?php

/* BenchFilepickerBundle:Default:prueba.html.twig */
class __TwigTemplate_9227b2f0d2c009fd280e3b5f912c1a2887e27b44abf5641e54cbc58ac42dab21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["archivo"]);
        foreach ($context['_seq'] as $context["_key"] => $context["archivo"]) {
            // line 2
            echo "
<a href=\"";
            // line 3
            echo twig_escape_filter($this->env, $context["archivo"], "html", null, true);
            echo "\" download=\"";
            echo twig_escape_filter($this->env, $context["archivo"], "html", null, true);
            echo "\"> archivo </a>
<br>
<p> ";
            // line 5
            echo twig_escape_filter($this->env, $context["archivo"], "html", null, true);
            echo "</p>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['archivo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "BenchFilepickerBundle:Default:prueba.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 5,  26 => 3,  23 => 2,  19 => 1,);
    }
}
