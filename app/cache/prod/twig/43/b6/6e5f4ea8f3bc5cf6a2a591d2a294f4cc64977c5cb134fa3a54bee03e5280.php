<?php

/* BenchTienesBundle:Default:tienesAhorroInversion.html.twig */
class __TwigTemplate_43b66e5f4ea8f3bc5cf6a2a591d2a294f4cc64977c5cb134fa3a54bee03e5280 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchtienes/js/tienesAhorroInversion.js"), "html", null, true);
        echo "\"></script>


<div class=\"fila\">
        
           <!--<h1 class=\"doc\"><span class=\"orange\">Ahorro / Inversión</span></h1>-->
          <div class=\"col1\">
            
        <form  id=\"FormAhorroinversion1\" action=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("tienesAhorroInversionSiNo");
        echo "\" method=\"post\" autocomplete=\"off\"  >
       <label class=\"label-text2\" for=\"rut\">¿Tienes algún Ahorro o Inversión?</label>
 

<input type=\"hidden\" id=\"token\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
       <input type=\"hidden\" value=\"\" name=\"TAhorroTId\" id=\"TAhorroTId\">
   
       ";
        // line 18
        if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tienesAhorroInversionSiNo", array()) == "SI")) {
            // line 19
            echo "       
         <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"AhorroInversionRadio\" name=\"AhorroInversionRadio\" id=\"AhorroInversionRadio\" checked >
         No<input type=\"radio\" value=\"NO\" name=\"AhorroInversionRadio\"  id=\"AhorroInversionRadio\" class=\"AhorroInversionRadio\"></p> 
         
         


 ";
        } else {
            // line 27
            echo "        
        
             
     
     <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"AhorroInversionRadio\" name=\"AhorroInversionRadio\" id=\"AhorroInversionRadio\" >
         No<input type=\"radio\" value=\"NO\" name=\"AhorroInversionRadio\"  checked  id=\"AhorroInversionRadio\" class=\"AhorroInversionRadio\"></p> 

            
       
       
       ";
        }
        // line 38
        echo "        

      



 

<div style=\"float:left; height:10px; width:100%;\"></div>
    <button style=\"float:left;\" class=\"classname\" value=\"Agregar\" id=\"guardaAhorroInversion1\" type=\"submit\">GUARDAR Y SIGUIENTE</button> 

               </form>

              

               <div id=\"RespuestaAhorroInversionSino\"> 

              
               </div>


              

               <form class=\"form-personal\" id=\"TienesAhorroinversionform\" action=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("ahorroinversionguarda");
        echo "\" method=\"post\" autocomplete=\"off\">

              <label class=\"label-text2\" for=\"institucion\">Tipo </label>

              <input type=\"hidden\" value=\"<?php echo \$idUser3;?>\" name=\"TAhorroTId\" id=\"TAhorroTId\">
               <select class=\"data\" select=\"selected\" name=\"TAhorroTipoBienes\" id=\"TAhorroTipoBienes\"data-validation-engine=\"validate[required]\" data-required=\"true\">
                  <option value=\"\">Opción</option>
                  <option value=\"Deposito\">Deposito a plazo</option>
                  <option value=\"Acciones\">Acciones</option>
                  <option value=\"Bonos\">Bonos</option>
                  <option value=\"Cuentas\">Cuentas de Ahorro</option>
                  <option value=\"Fondo Mutuos\">Fondos Mutuos</option>
                  <option value=\"Otros\">Otros</option>
               </select>
               
               
             <label class=\"label-text2\" for=\"institucion\">Institución</label>
              <input type=\"text\" class=\"input-box2\" name=\"TAhorroInstitucion\" id=\"TAhorroInstitucion\"    data-validation-engine=\"validate[required]\" data-required=\"true\" >
              
                           <label class=\"label-text2\" for=\"institucion\">Valor Comercial</label>
              <input type=\"text\"class=\"input-box2\" name=\"TAhorroVComercial\"  placeholder=\"\$\" id=\"TAhorroVComercial\"  data-validation-engine=\"validate[required]\" data-required=\"true\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" >
              
              
              
              <label class=\"label-text2\" for=\"rut\">Prendado</label>
              
               <p class=\"rad\">Si<input type=\"radio\" value=\"si\" name=\"TAhorroVPrendadoInversion\" id=\"TAhorroVPrendadoInversion\" required=\"required\" >
         No<input type=\"radio\" value=\"no\" name=\"TAhorroVPrendadoInversion\" id=\"TAhorroVPrendadoInversion\" required=\"required\" ></p>              
              
              
              
            </div>
            
            <div class=\"clearfix\"></div>
            
          <div class=\"col2\">
          <button style=\"float:left; margin-top:10px;\" id=\"guardaAhorroInversion\" class=\"classname\" value=\"Agregar\" type=\"submit\">AGREGAR </button>

         <div id=\"SiguienteAhorroInversion\" class=\"classname\" style=\"margin-top:10px; margin-left: 1%; position: relative;width: 80px; float: left;\" >SIGUIENTE</div></div> 
         
          <div class=\"flix-clear\"></div>
 
            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"col2\">
              <table class=\"table table-striped\" id=\"respuestaAhorroI\">
          <thead style=\"background:#FF6600;\">
                  <tr>
          <td width=\"12%\"><p style=\"text-align:center; color:#FFF;\">Tipo</p></td>
          <td width=\"19%\"><p style=\"text-align:center;color:#FFF;\">Institución</p></td>
          <td width=\"32%\"><p style=\"text-align:center;color:#FFF;\">Valor Comercial</p></td>
          <td width=\"20%\"><p style=\"text-align:center;color:#FFF;\">Prendado</p></td>
                    <td width=\"17%\"><p style=\"text-align:center;color:#FFF;\">Eliminar</p></td>                   
          </tr>
        </thead>
        <tbody style=\"background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;\">
           ";
        // line 118
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesAhorroInversion"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesAhorroInversion"]) {
            echo "  
            
             
          <tr  id=\"itemahorroinversion_";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "id", array()), "html", null, true);
            echo "\">
        
          <td><p style=\"text-align:center;color:#666;\">";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "tipo", array()), "html", null, true);
            echo " </p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "institucion", array()), "html", null, true);
            echo " </p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "valorcomercial", array()), "html", null, true);
            echo "   </p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "prendado", array()), "html", null, true);
            echo " </p></td>
          
          <td><p style=\"text-align:center;color:#666;\"> <a href=\"#\" class=\"del_button\" id=\"del-";
            // line 128
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "id", array()), "html", null, true);
            echo "\">eliminar</a> </p> </td>

          </tr>
          

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAhorroInversion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 134
        echo "        </tbody>
                                  
       
             </table>
              
              
              
              
              
              
              
             
              
              
             
            
            </div>
            
            

  
    </div>
  
        

        
            



</form>
              
              
<form id=\"delahorroinversion\" action=\"";
        // line 167
        echo $this->env->getExtension('routing')->getPath("delahorroinversion");
        echo "\" method=\"post\" autocomplete=\"off\" ></form>";
    }

    public function getTemplateName()
    {
        return "BenchTienesBundle:Default:tienesAhorroInversion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 167,  198 => 134,  186 => 128,  181 => 126,  177 => 125,  173 => 124,  169 => 123,  164 => 121,  156 => 118,  96 => 61,  71 => 38,  58 => 27,  48 => 19,  46 => 18,  40 => 15,  33 => 11,  22 => 3,  19 => 2,);
    }
}
