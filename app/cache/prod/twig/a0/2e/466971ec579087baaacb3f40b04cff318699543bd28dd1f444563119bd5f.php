<?php

/* BenchAdminBundle:Default:modalSeleccionFotos.html.twig */
class __TwigTemplate_a02e466971ec579087baaacb3f40b04cff318699543bd28dd1f444563119bd5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " <!-- Modal -->
 <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
     <div class=\"modal-header\">
         <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
         <h3 id=\"myModalLabel\">Seleccione formato exportacion</h3>
     </div>
     <div class=\"modal-body\">


         <p><a href=\"\" id=\"estandar\" target=\"_blank\">Formato Estandar </a></p>

         <p><a href=\"\" id=\"bbva\" target=\"_blank\">Formato Bbva </a></p>
         <p><a href=\"\" id=\"bbva2\" target=\"_blank\">Formato Bbva2 </a></p>
         <p><a href=\"\" id=\"bci\" target=\"_blank\">Formato Bci </a></p>
         <p><a href=\"\" id=\"consorcio\" target=\"_blank\">Formato Consorcio </a></p>
         <p><a href=\"\" id=\"metlife\" target=\"_blank\">Formato Metlife </a></p>


     </div>
     <div class=\"modal-footer\">
         <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>

     </div>
 </div>
";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:modalSeleccionFotos.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
