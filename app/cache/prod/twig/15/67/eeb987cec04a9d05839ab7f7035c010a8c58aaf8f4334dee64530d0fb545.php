<?php

/* BenchAdminBundle:Default:remaxprueba.html.twig */
class __TwigTemplate_1567eeb987cec04a9d05839ab7f7035c010a8c58aaf8f4334dee64530d0fb545 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Document</title>
</head>
<body>


<form method=\"post\"  action=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("api_1_post_user_aliansas_nuevo_remax");
        echo "\" class=\"fsForm fsMultiColumn\" id=\"fsForm1660803\">


<div class=\"fsSection fs2Col\" id=\"fsSection23456298\">



    <div class=\"fsSectionHeader\">
        <h2 class=\"fsSectionHeading\">Información ejecutivo REMAX</h2>

    </div>















    <div id=\"fsRow1660803-2\" class=\"fsRow fs Row\">







        <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23456146\" lang=\"es\">
            <label id=\"label23456146\" class=\"fsLabel fsRequiredLabel\" for=\"23456146\">Nombre Ejecutivo Remax<span class=\"fsRequiredMarker\">*</span>                                            </label>


            <input type=\"text\" id=\" 23456146\" name=\" 23456146\" size=\"50\" required=\"\" value=\"\" class=\"fs  fsRequired\" aria-required=\"true\">





        </div>



















        <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23455943\" lang=\"es\">
            <label id=\"label23455943\" class=\"fsLabel fsRequiredLabel\" for=\" 23455943\">Email Ejecutivo Remax<span class=\"fsRequiredMarker\">*</span>                                            </label>


            <input type=\"text\" id=\" 23455943\" name=\" 23455943\" size=\"50\" required=\"\" value=\"\" class=\"fs  fsRequired\" aria-required=\"true\">





        </div>
    </div>














    <div id=\"fsRow1660803-3\" class=\"fsRow fs Row\">







        <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23455945\" lang=\"es\">
            <label id=\"label23455945\" class=\"fsLabel fsRequiredLabel\" for=\" 23455945\">Telefono Ejecutivo Remax<span class=\"fsRequiredMarker\">*</span>                                            </label>


            <input type=\"text\" id=\" 23455945\" name=\" 23455945\" size=\"50\" required=\"\" value=\"\" class=\"fs  fsRequired\" aria-required=\"true\">





        </div>



















        <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell26026706\" lang=\"es\">
            <label id=\"label26026706\" class=\"fsLabel fsRequiredLabel\" for=\" 26026706\">Telefono Ejecutivo Remax<span class=\"fsRequiredMarker\">*</span>                                            </label>


            <input type=\"text\" id=\" 26026706\" name=\" 26026706\" size=\"50\" required=\"\" value=\"\" class=\"fs  fsRequired\" aria-required=\"true\">





        </div>
    </div>














    <div id=\"fsRow1660803-4\" class=\"fsRow fs Row fsLastRow fsHidden\">







        <div class=\"fsRowBody fsCell fs Cell fsReadOnly fsFirst  fsLabelVertical fsHidden fsSpan50 fsHiddenBy Setting\" id=\"fsCell23456380\" lang=\"es\">
            <label id=\"label23456380\" class=\"fsLabel\" for=\" 23456380\">Solicitud de crédito REMAX                                            </label>


            <input type=\"text\" id=\" 23456380\" name=\" 23456380\" size=\"50\" value=\"Solicitud de crédito REMAX\" readonly=\"readonly\" class=\"fs \">





        </div>
    </div>







</div>






<div class=\"fsSection fs2Col\" id=\"fsSection23594527\">



    <div class=\"fsSectionHeader\">
        <h2 class=\"fsSectionHeading\">Información de Cliente</h2>

        <div class=\"fsSectionText\"><h1><span style=\"color: #3366ff;\">&nbsp;</span></h1></div>
    </div>















    <div id=\"fsRow1660803-6\" class=\"fsRow fs Row\">







        <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23455872\" lang=\"es\">
            <label id=\"label23455872\" class=\"fsLabel fsRequiredLabel\" for=\" 23455872\">Rut Cliente<span class=\"fsRequiredMarker\">*</span>                                            </label>


            <input type=\"text\" id=\" 23455872\" name=\" 23455872\" size=\"50\" required=\"\" value=\"\" class=\"fs  fsRequired\" aria-required=\"true\">





        </div>



















        <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23455878\" lang=\"es\">
            <label id=\"label23455878\" class=\"fsLabel fsRequiredLabel\" for=\" 23455878\">Nombre y Apellido Cliente<span class=\"fsRequiredMarker\">*</span>                                            </label>


            <input type=\"text\" id=\" 23455878\" name=\" 23455878\" size=\"50\" required=\"\" value=\"\" class=\"fs  fsRequired\" aria-required=\"true\">





        </div>
    </div>














    <div id=\"fsRow1660803-7\" class=\"fsRow fs Row\">







        <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23455882\" lang=\"es\">
            <label id=\"label23455882\" class=\"fsLabel fsRequiredLabel\" for=\" 23455882\">Telefono Cliente<span class=\"fsRequiredMarker\">*</span>                                            </label>


            <input type=\"text\" id=\" 23455882\" name=\" 23455882\" size=\"50\" required=\"\" value=\"\" class=\"fs  fsRequired\" aria-required=\"true\">





        </div>



















        <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23455896\" lang=\"es\">
            <label id=\"label23455896\" class=\"fsLabel fsRequiredLabel\" for=\" 23455896\">Email Cliente<span class=\"fsRequiredMarker\">*</span>                                            </label>


            <input type=\"text\" id=\" 23455896\" name=\" 23455896\" size=\"50\" required=\"required\" value=\"\" class=\"fs  fsFormatEmail fsRequired\" >





        </div>
    </div>














    <div id=\"fsRow1660803-8\" class=\"fsRow fs Row fsLastRow\">







        <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23455917\" aria-describedby=\"fsSupporting23455917\" lang=\"es\">
            <label id=\"label23455917\" class=\"fsLabel\" for=\" 23455917\">Sueldo Liquido Cliente                                            </label>


            <input type=\"text\" id=\" 23455917\" name=\" 23455917\" size=\"50\" value=\"\" class=\"fs \">
            <div id=\"fsSupporting23455917\" class=\"fsSupporting\">Opcional</div>





        </div>
    </div>







</div>






<div class=\"fsSection fs2Col\" id=\"fsSection23594510\">



<div class=\"fsSectionHeader\">
    <h2 class=\"fsSectionHeading\">Información Propiedad</h2>

</div>















<div id=\"fsRow1660803-10\" class=\"fsRow fs Row\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23594511\" lang=\"es\">
        <label id=\"label23594511\" class=\"fsLabel fsRequiredLabel\" for=\"23594511\">Valor Propiedad - UF<span class=\"fsRequiredMarker\">*</span>                                            </label>


        <input type=\"text\" id=\"23594511\" name=\" 23594511\" size=\"10\" required=\"\" value=\"\" class=\"fs  fsRequired\" aria-required=\"true\">





    </div>



















    <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23594513\" aria-describedby=\"fsSupporting23594513\" lang=\"es\">
        <label id=\"label23594513\" class=\"fsLabel\" for=\" 23594513\">Plazo de Crédito                                            </label>


        <input type=\"text\" id=\"23594513\" name=\"23594513\" size=\"2\" value=\"\" class=\"fs \">
        <div id=\"fsSupporting23594513\" class=\"fsSupporting\">Años Solicitados</div>





    </div>
</div>














<div id=\"fsRow1660803-11\" class=\"fsRow fs Row\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23594512\" aria-describedby=\"fsSupporting23594512\" lang=\"es\">
        <label id=\"label23594512\" class=\"fsLabel\" for=\"23594512\">Monto PIE - UF                                            </label>


        <input type=\"text\" id=\"23594512\" name=\"23594512\" size=\"10\" value=\"\" class=\"fs \">
        <div id=\"fsSupporting23594512\" class=\"fsSupporting\">Minimo de 10%</div>





    </div>



















    <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23594514\" lang=\"es\">
        < set id=\"label23594514\">
            <legend class=\"fsLabel fsLabelVertical\"><span>¿Es la propiedad DFL2?</span></legend>
            <div class=\" set-content\">




                <label class=\"fsOptionLabel vertical\" for=\"23594514_1\"><input type=\"checkbox\" id=\"23594514_1\" name=\" 23594514[]\" value=\"SI\" class=\"fs   vertical\">SI</label>




                <label class=\"fsOptionLabel vertical\" for=\" 23594514_2\"><input type=\"checkbox\" id=\"23594514_2\" name=\" 23594514[]\" value=\"NO\" class=\"fs   vertical\">NO</label>






            </div></ set>



    </div>
</div>














<div id=\"fsRow1660803-12\" class=\"fsRow fs Row\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23594515\" lang=\"es\">
        <label id=\"label23594515\" class=\"fsLabel\" for=\"23594515\">Tipo de Propiedad                                            </label>


        <select id=\"23594515\" name=\"23594515\" size=\"1\" class=\"fs  \">
            <option value=\"Elija un opción\">Elija un opción</option>
            <option value=\"Casa\">Casa</option>
            <option value=\"Departamento\">Departamento</option>
            <option value=\"Terreno\">Terreno</option>
            <option value=\"Otros\">Otros</option>
        </select>





    </div>



















    <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23594516\" lang=\"es\">
        <label id=\"label23594516\" class=\"fsLabel\" for=\"23594516\">Estado Propiedad                                            </label>


        <select id=\"23594516\" name=\"23594516\" size=\"1\" class=\"fs  \">
            <option value=\"Elija una opción\">Elija una opción</option>
            <option value=\"Nueva\">Nueva</option>
            <option value=\"Usada\">Usada</option>
        </select>





    </div>
</div>














<div id=\"fsRow1660803-13\" class=\"fsRow fs Row\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23594518\" lang=\"es\">
        <label id=\"label23594518\" class=\"fsLabel\" for=\"23594518\">Comuna Propiedad                                            </label>


        <input type=\"text\" id=\"23594518\" name=\"23594518\" size=\"20\" value=\"\" class=\"fs \">





    </div>



















    <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23594517\" lang=\"es\">
        <label id=\"label23594517\" class=\"fsLabel\" for=\" 23594517\">¿Cuándo comprará la propiedad?                                            </label>


        <select id=\"23594517\" name=\"23594517\" size=\"1\" class=\"fs  \">
            <option value=\"No lo sabé\">No lo sabé</option>
            <option value=\"Dentro de 1 mes\">Dentro de 1 mes</option>
            <option value=\"Entre 1 a 3 meses\">Entre 1 a 3 meses</option>
            <option value=\"Entre 4 y 6 meses\">Entre 4 y 6 meses</option>
            <option value=\"En más de 6 meses\">En más de 6 meses</option>
        </select>





    </div>
</div>














<div id=\"fsRow1660803-14\" class=\"fsRow fs Row fsLastRow\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23594519\" lang=\"es\">
        <label id=\"label23594519\" class=\"fsLabel\" for=\" 23594519\">Nombre Proyecto / Inmobiliaria                                            </label>


        <input type=\"text\" id=\"23594519\" name=\"23594519\" size=\"50\" value=\"\" class=\"fs \">





    </div>
</div>







</div>






<div class=\"fsSection fs2Col\" id=\"fsSection24487468\">



<div class=\"fsSectionHeader\">
    <h2 class=\"fsSectionHeading\">Adjuntos</h2>

</div>















<div id=\"fsRow1660803-16\" class=\"fsRow fs Row\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23455947\" aria-describedby=\"fsSupporting23455947\" lang=\"es\">
        <label id=\"label23455947\" class=\"fsLabel\" for=\" 23455947\">Liquidaciones de sueldo - Último Mes                                            </label>


        <input type=\"text\" id=\"23455947\" name=\"23455947\" size=\"30\" class=\"\">
        <div class=\"showMobile\">La carga de archivos no pueden trabajar en algunos dispositivos móviles.</div>

        <div id=\"fsSupporting23455947\" class=\"fsSupporting\">Opcional</div>





    </div>



















    <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23456102\" aria-describedby=\"fsSupporting23456102\" lang=\"es\">
        <label id=\"label23456102\" class=\"fsLabel\" for=\" 23456102\">Liquidación Sueldo - Mes 2                                            </label>


        <input type=\"text\" id=\"23456102\" name=\" 23456102\" size=\"30\" class=\" \">
        <div class=\"showMobile\">La carga de archivos no pueden trabajar en algunos dispositivos móviles.</div>

        <div id=\"fsSupporting23456102\" class=\"fsSupporting\">Opcional</div>





    </div>
</div>














<div id=\"fsRow1660803-17\" class=\"fsRow fs Row\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23463605\" aria-describedby=\"fsSupporting23463605\" lang=\"es\">
        <label id=\"label23463605\" class=\"fsLabel\" for=\" 23463605\">Liquidación - Mes 3                                            </label>


        <input type=\"text\" id=\"23463605\" name=\"23463605\" size=\"30\" class=\"\">
        <div class=\"showMobile\">La carga de archivos no pueden trabajar en algunos dispositivos móviles.</div>

        <div id=\"fsSupporting23463605\" class=\"fsSupporting\">Opcional </div>





    </div>



















    <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23456043\" aria-describedby=\"fsSupporting23456043\" lang=\"es\">
        <label id=\"label23456043\" class=\"fsLabel\" for=\" 23456043\">Fotocopia Carnet - Cliente                                            </label>


        <input type=\"text\" id=\"23456043\" name=\"23456043\" size=\"30\" class=\" \">
        <div class=\"showMobile\">La carga de archivos no pueden trabajar en algunos dispositivos móviles.</div>

        <div id=\"fsSupporting23456043\" class=\"fsSupporting\">Opcional</div>





    </div>
</div>














<div id=\"fsRow1660803-18\" class=\"fsRow fs Row\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23456042\" aria-describedby=\"fsSupporting23456042\" lang=\"es\">
        <label id=\"label23456042\" class=\"fsLabel\" for=\" 23456042\">Certificado AFP                                             </label>


        <input type=\"text\" id=\"23456042\" name=\"23456042\" size=\"30\" class=\"\">
        <div class=\"showMobile\">La carga de archivos no pueden trabajar en algunos dispositivos móviles.</div>

        <div id=\"fsSupporting23456042\" class=\"fsSupporting\">Opcional</div>





    </div>



















    <div class=\"fsRowBody fsCell fs Cell   fsLast fsLabelVertical  fsSpan50 \" id=\"fsCell23488550\" aria-describedby=\"fsSupporting23488550\" lang=\"es\">
        <label id=\"label23488550\" class=\"fsLabel\" for=\" 23488550\">Otros                                            </label>


        <input type=\"text\" id=\"23488550\" name=\"23488550\" size=\"30\" class=\"\">
        <div class=\"showMobile\">La carga de archivos no pueden trabajar en algunos dispositivos móviles.</div>

        <div id=\"fsSupporting23488550\" class=\"fsSupporting\">Opcional</div>





    </div>
</div>














<div id=\"fsRow1660803-19\" class=\"fsRow fs Row fsLastRow\">







    <div class=\"fsRowBody fsCell fs Cell  fsFirst  fsLabelVertical  fsSpan50 \" id=\"fsCell23488553\" aria-describedby=\"fsSupporting23488553\" lang=\"es\">
        <label id=\"label23488553\" class=\"fsLabel\" for=\" 23488553\">Otros                                            </label>


        <input type=\"text\" id=\"23488553\" name=\"23488553\" size=\"30\" class=\"fs  fsUpload uploadTypes-jpg,jpeg,gif,png,bmp,tif,psd,pdf,doc,docx,xls,xlsx,txt,mp3,mp4,aac,wav,au,wmv,avi,mpg,mpeg,zip,gz,rar,z,tgz,tar,sitx \">
        <div class=\"showMobile\">La carga de archivos no pueden trabajar en algunos dispositivos móviles.</div>

        <div id=\"fsSupporting23488553\" class=\"fsSupporting\">Opcional</div>





    </div>
</div>







</div>






<div class=\"fsSection fs2Col\" id=\"fsSection24730683\">



    <div class=\"fsSectionHeader\">

    </div>















    <div id=\"fsRow1660803-21\" class=\"fsRow fs Row fsLastRow fsHidden\">







        <div class=\"fsRowBody fsCell fs Cell fsReadOnly fsFirst  fsLabelVertical fsHidden fsSpan50 fsHiddenBy Setting\" id=\"fsCell23463509\" lang=\"es\">
            <label id=\"label23463509\" class=\"fsLabel\" for=\" 23463509\">Tipo de Crédito                                            </label>


            <input type=\"text\" id=\"23463509\" name=\"23463509\" size=\"50\" value=\"CHIP\">





        </div>
    </div>




</div>


</div>
<div id=\"fsSubmit1660803\" class=\"fsSubmit fsPagination\">

    <button type=\"button\" id=\"fsPreviousButton1660803\" class=\"fsPreviousButton\" value=\"Previous Page\" style=\"display:none; color:#000000;\"><span class=\"fsFull\">Anterior</span><span class=\"fsSlim\">←</span></button>
    <button type=\"button\" id=\"fsNextButton1660803\" class=\"fsNextButton\" value=\"Next Page\" style=\"display: none; color: rgb(0, 0, 0);\"><span class=\"fsFull\">Siguiente</span><span class=\"fsSlim\">→</span></button>
    <input id=\"fsSubmitButton1660803\" class=\"fsSubmitButton\" style=\"color: rgb(0, 0, 0);\" type=\"submit\" value=\"Enviar\">



    <div class=\"clear\"></div>
</div>




</form>



</body>
</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:remaxprueba.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 10,  19 => 1,);
    }
}
