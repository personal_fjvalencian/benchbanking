<?php

/* BenchUsuariosBundle:Default:dondeBackup.html.twig */
class __TwigTemplate_1c79b77ed792e26724e1643ccb9fbd25149dffdb3354a212fa73cfa936e9d7f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<script>

\$('document').ready(function(e) {

  /*  Banco 1 */
   \$('.banco1').click(function() {
\t   
\t   var valor = \$('#banco01').val();
\t   
\t   
\t   if(valor == 'activo'){
\t\t   
\t\t   \$('#banco01').val('desactivo');
\t\t   \$('.banco1').addClass('apretado');
\t\t   
\t   }else{
\t\t   
\t\t     \$('#banco01').val('activo');
\t\t   \$('.banco1').removeClass('apretado');
\t\t   
\t\t   
\t   }
\t   
\t
\t  }); 


    /*  Banco 2 */  



   \$('.banco2').click(function() {
     
     var valor = \$('#banco02').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco02').val('desactivo');
       \$('.banco2').addClass('apretado');
       
     }else{
       
         \$('#banco02').val('activo');
       \$('.banco2').removeClass('apretado');
       
       
       
       
       
     }
     
     
    }); 


   /*  Banco 3 */  



   \$('.banco3').click(function() {
     
     var valor = \$('#banco03').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco03').val('desactivo');
       \$('.banco3').addClass('apretado');
       
     }else{
       
         \$('#banco03').val('activo');
       \$('.banco3').removeClass('apretado');
       
       
       
       
       
     }
     
   
    }); 


      /*  Banco 4 */  



   \$('.banco4').click(function() {
     
     var valor = \$('#banco04').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco04').val('desactivo');
       \$('.banco4').addClass('apretado');
       
     }else{
       
         \$('#banco04').val('activo');
       \$('.banco4').removeClass('apretado');
       
       
       
       
       
     }
     
  
    });


         /*  Banco 5 */  



   \$('.banco5').click(function() {
     
     var valor = \$('#banco05').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco05').val('desactivo');
       \$('.banco5').addClass('apretado');
       
     }else{
       
         \$('#banco05').val('activo');
       \$('.banco5').removeClass('apretado');
       
       
       
       
       
     }
     
     
    });

            /*  Banco 6 */  



   \$('.banco6').click(function() {
     
     var valor = \$('#banco06').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco06').val('desactivo');
       \$('.banco6').addClass('apretado');
       
     }else{
       
         \$('#banco06').val('activo');
       \$('.banco6').removeClass('apretado');
       
       
       
       
       
     }
     
   
    });



            /*  Banco 7 */  



   \$('.banco7').click(function() {
     
     var valor = \$('#banco07').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco07').val('desactivo');
       \$('.banco7').addClass('apretado');
       
     }else{
       
         \$('#banco07').val('activo');
       \$('.banco7').removeClass('apretado');
       
       
       
       
       
     }
     
     
    });


            /*  Banco 8 */  



   \$('.banco8').click(function() {
     
     var valor = \$('#banco08').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco08').val('desactivo');
       \$('.banco8').addClass('apretado');
       
     }else{
       
         \$('#banco08').val('activo');
       \$('.banco8').removeClass('apretado');
       
       
       
       
       
     }
     

    });



            /*  Banco 9 */  



   \$('.banco9').click(function() {
     
     var valor = \$('#banco09').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco09').val('desactivo');
       \$('.banco9').addClass('apretado');
       
     }else{
       
         \$('#banco09').val('activo');
       \$('.banco9').removeClass('apretado');
       
       
       
       
       
     }
     
  
    });


            /*  Banco 10 */  



   \$('.banco10').click(function() {
     
     var valor = \$('#banco10').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco10').val('desactivo');
       \$('.banco10').addClass('apretado');
       
     }else{
       
         \$('#banco10').val('activo');
       \$('.banco10').removeClass('apretado');
       
       
       
       
       
     }
     
  
    });


            /*  Banco 11 */  



   \$('.banco11').click(function() {
     
     var valor = \$('#banco11').val();
     
     
     if(valor == 'activo'){
       
       \$('#banco11').val('desactivo');
       \$('.banco11').addClass('apretado');
       
     }else{
       
         \$('#banco11').val('activo');
       \$('.banco11').removeClass('apretado');
       
       
       
       
       
     }
     
   
    });  



});

</script>




<form class=\"form-personal\" method=\"post\" action=\"";
        // line 329
        echo $this->env->getExtension('routing')->getPath("guardaSolicitud");
        echo "\" id=\"DondeForm\"></form>

  <div class=\"container-seleccbanks\">


      

  <div id=\"box-banks\" value=\"activo\" class=\"banco1\">
  <input type=\"hidden\" value=\"activo\" id=\"banco01\">
  
  \t<a href=\"#\">
                <img src=\"";
        // line 340
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/principal.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>PRINCIPAL<br>FINANCIAL GROUP<br></span>
        </br>
        <span style=\"font-size:10px\">www.principal.cl</span>
  \t</a> 
  </div>
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco2\">
  <input type=\"hidden\" value=\"activo\" id=\"banco02\">
  \t<a href=\"#\">
                <img src=\"";
        // line 351
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/metlife.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>METLIFE<br><br></span>
        </br>
        <span style=\"font-size:10px\">www.metlife.cl</span>
  \t</a> 
  </div>
      
      
  <div id=\"box-banks\" value=\"activo\" class=\"banco3\">
  <input type=\"hidden\" value=\"activo\" id=\"banco03\">
  \t<a href=\"#\">
                <img src=\"";
        // line 363
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/logo_bancoconsorcio.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>CONSORCIO<br><br></span>
        </br>
        <span style=\"font-size:10px\">www.consorcio.cl</span>
  \t</a> 
  </div>    
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco4\">
  <input type=\"hidden\" value=\"activo\" id=\"banco04\" >
    <a href=\"#\">
      <img src=\"";
        // line 374
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/bci.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>BANCO<br>BCI<br></span>
        </br>
        <span style=\"font-size:10px\">www.bci.cl</span>
    </a> 
  </div>
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco5\">
  <input type=\"hidden\" value=\"activo\" id=\"banco05\">
    <a href=\"#\">
      <img src=\"";
        // line 385
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/logo_bancoscotiabank.jpg"), "html", null, true);
        echo "\">
        <br>
        <span>SCOTIABANK<br><br></span>
        </br>
        <span style=\"font-size:10px\">www.scotiabank.cl</span>
    </a> 
  </div>
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco6\">
  <input type=\"hidden\" value=\"activo\" id=\"banco06\">
    <a href=\"#\">
      <img src=\"";
        // line 396
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/logo_bancosantander.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>BANCO<br>SANTANDER<br></span>
        </br>
        <span style=\"font-size:10px\">www.santander.cl</span>
    </a> 
  </div>
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco7\">
  <input type=\"hidden\" value=\"activo\" id=\"banco07\">
    <a href=\"#\">
      <img src=\"";
        // line 407
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/bancoItau.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>BANCO<br>ITAU<br></span>
        </br>
        <span style=\"font-size:10px\">www.itau.cl</span>
    </a> 
  </div>
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco8\">
  <input type=\"hidden\" value=\"activo\" id=\"banco08\">
    <a href=\"#\">
      <img src=\"";
        // line 418
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/bancobice.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>BANCO<br>BICE<br></span>
        </br>
        <span style=\"font-size:10px\">www.bice.cl</span>
    </a> 
  </div>
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco9\">
  <input type=\"hidden\" value=\"activo\" id=\"banco09\">
    <a href=\"#\">
      <img src=\"";
        // line 429
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/logo_bancodechile.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>BANCO<br>DE CHILE<br></span>
        </br>
        <span style=\"font-size:10px\">www.bancochile.cl</span>
    </a> 
  </div>
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco10\">
  <input type=\"hidden\" value=\"activo\" id=\"banco10\">
    <a href=\"#\">
      <img src=\"";
        // line 440
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/logo_bancosecurity.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>BANCO<br>SECURITY<br></span>
        </br>
        <span style=\"font-size:10px\">www.security.cl</span>
    </a> 
  </div>
  
  <div id=\"box-banks\" value=\"activo\" class=\"banco11\">
  <input type=\"hidden\" value=\"activo\" id=\"banco11\">
    <a href=\"#\">
      <img src=\"";
        // line 451
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/bancos/bbva.jpg"), "html", null, true);
        echo "\">
        <br><br>
        <span>BANCO<br>BBVA<br></span>
        </br>
        <span style=\"font-size:10px\">www.bbva.cl</span>
    </a> 
  </div>
  
</div>
    
 <br>  
 <section id=\"secAutorizo\">

     

            
<a href=\"#\" class=\"btn-bancos\" id=\"dondeguarda\"  title=\"Enviar a Bancos\"><span class=\"displace\">Enviar información a los bancos</span></a>
                                                                            
                             
                                                      
<div class=\"box-txtpermiso\" id='boxDicom'>
    
    <div   id=\"rutnovalido\"  style=\"width: 280px;height: auto; \">
<img src=\"";
        // line 474
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/alert_icon.png"), "html", null, true);
        echo "\">
<span style=\"color: #FF8000;\">Para procesar su solicitud debe autorizar el uso de datos personales.</span>

</div>       
    
    <br>
    
                   
    
<h1 style=\"font-size: 16px;\"><input type=\"checkbox\" value=\"SI\" id=\"autorizoDicom\" name=\"autorizoDicom\" /> Autorizo para obtener mis datos personales de carácter económico, financiero, bancario o comercial</h1>

<br>
    
    
<p>Autorizo expresamente a Benchbanking SpA para obtener mis datos personales de carácter económico, financiero, bancario o comercial a que se refiere el Título III de la ley N° 19.628, sobre Protección de la Vida Privada, de todo tipo de distribuidores de información de carácter económico, financiero, bancario o comercial, de conformidad con lo dispuesto en la legislación vigente y con pleno respeto a mis derechos.

Benchbanking SpA sólo podrá utilizar estos datos para comunicarlos a instituciones financieras para la evaluación de riesgo comercial y para el proceso de crédito.

El Cliente podrá en todo momento revocar esta autorización, comunicando a Benchbanking SpA su decisión de revocarla mediante correo electrónico dirigido a info@benchbanking.com</p>






</div>
  


  
  
  
  
  
\t<div class=\"clearfix\"></div>
        
        

      




</section>
 
 
 
 <div class=\"\" id=\"op0\" style=\"display: block;\">
<img src=\"";
        // line 522
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/alert_icon.png"), "html", null, true);
        echo "\">
Felicitaciones! tus datos han sido enviados correctamente...
</div>
<div class=\"classname\" id=\"op1\" style=\"display: inline-block;\">Solicitar otro Crédito</div>
<div class=\"classname\" id=\"op2\" style=\"display: inline-block;\">Cerrar Sesión</div>

";
    }

    public function getTemplateName()
    {
        return "BenchUsuariosBundle:Default:dondeBackup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  580 => 522,  529 => 474,  503 => 451,  489 => 440,  475 => 429,  461 => 418,  447 => 407,  433 => 396,  419 => 385,  405 => 374,  391 => 363,  376 => 351,  362 => 340,  348 => 329,  19 => 2,);
    }
}
