<?php

/* BenchPaginasBundle:Default:footer.html.twig */
class __TwigTemplate_2080f3b2743ea2b4ae7a07567316449618a9c13ab444a5dfc97a8e5abed5c3fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<footer>
     <script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/mail.js"), "html", null, true);
        echo "\"></script>
<div class=\"main-wrapper\">


    
    
   <div class=\"col01-footer\">
   <h1 style=\"color: #CCC; text-align:left;\">Quiénes Somos</h1>
   <p style=\"text-align:justify; color: #CCC; margin-top:5px;\">Somos un equipo de emprendedores que con el apoyo de Startup Chile  desarrolló una plataforma que permite a sus clientes cotizar, comparar y contratar sus créditos en un sólo lugar, logrando así elegir el crédito más conveniente en términos de cuota y calidad de atención.</p>
   <div class=\"block-color-footer\">
   <h1 style=\"color: #CCC; text-align:left;\">Misión</h1>
    <p style=\"text-align:justify; color: #CCC; margin-top:10px;\">
    Nuestra misión es ayudar a nuestros clientes en Latinoamérica a tomar mejores decisiones financieras de forma fácil y simple, como también, el proveer a las instituciones financieras una plataforma que permita comunicar su oferta de valor a un mayor público.
    </p>
   </div>
   <div class=\"block-color-footer\">
   <h1 style=\"color: #CCC;text-align:left;\">Visión</h1>
    <p style=\"text-align:justify; color: #CCC; margin-top:10px;\">
    Convertirnos en el portal de contratación de productos financieros más importantes de Latinoamérica construyendo una experiencia sobresaliente para nuestros clientes.</p>
    <br>
   </div>   
   </div>
   
   <div class=\"col02-footer\">
   <h1 style=\"color: #CCC; text-align:left;\">Contacto</h1>
   

   <div id=\"alert2\">
       
       <div id=\"enviadoMail\" style=\"color:#FFFFFF;\"><h3>Mensaje enviado gracias !!</h3></div>
       
  <form class=\"form-contact\" action=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("enviarMail");
        echo "\"  id=\"contactoMailInfo\" method=\"post\" data-validate=\"parsley\">
   <div class=\"col-form-contact\">
     
      <input name=\"nombre\"  id=\"nombreMailInfo\" type=\"text\" class=\"input-box-h\" placeholder=\"Nombre\" data-required=\"true\"  >
      <input name=\"asunto\"  id=\"asuntoMailInfo\" type=\"text\" class=\"input-box-h\" placeholder=\"Asunto\" data-required=\"true\"  >
      <input name=\"email\"   id=\"emailMailInfo\"  type=\"text\" class=\"input-box-h\" placeholder=\"Email\"  data-required=\"true\"  >
      </div>
      <div class=\"col-form-contact\">
      <textarea name=\"mensaje\" cols=\"4\" rows=\"2\"  placeholder=\"Mensaje\" id=\"mensajeMailInfo\" data-required=\"true\" ></textarea>
    

      
  
      
      <input name=\"\" id=\"enviarCorreo\" style=\"width: 90px;\"  class=\"classname\" value=\"Enviar\">
     
   </form>
       
    
 </div>

 </div>



   
<!--- FIN  FORM CONTACTO -->




   <div class=\"clearfix\"></div>
   <div class=\"box-data-contact\">
   <h3 style=\"color:#CCC;\">Dirección</h3>
   <p style=\"color:#CCC;font-size:0.6em;\">
       Av. Providencia 229 (Obispo Perez de Espinoza), Providencia.

 </p>
   </div>
   <div class=\"box-data-contact\">
     <h3 style=\"color:#CCC;\">Fono</h3>
   <p style=\"color:#CCC; font-size:0.6em;\">
       +56 2 25709004 </p>
 </div>

  


   <div class=\"box-data-contact\">
     <h3 style=\"color:#CCC;\">E-mail</h3>
     <p style=\"color:#CCC;font-size:0.6em;\">info@benchbanking.com<br></p>
   </div>
   </div>
   </div>
   <div class=\"flix-clear\"></div>
</footer>

<div class=\"pie\">
<div class=\"pie-wrapper\">

<div class=\"col02-pie\">
                <ul>
            \t<li><div id='basic-modal'><a href='#' class='basic2'>Información Legal</a></div></li>
                <li><div id='basic-modal'><a href='#' class='basic4'>Privacidad</a></div></li>
                
                <li style=\"border:none;\"><p style=\"color: #F60;\">Sitio web optimizado para navegadores <a href='http://www.mozilla.org/'>Firefox</a>, <a href='http://www.google.com/chrome/'>Chrome</a>, <a href='http://www.apple.com/safari/'>Safari</a>, <a href='http://www.opera.com/download/‎'>Opera</a> e <a href='http://windows.microsoft.com/es-xl/internet-explorer/download-ie'>IE 9+</a></p></li> 
             </ul>
             
\t\t\t
</div>

<div class=\"col01-pie\">
\t<a href=\"http://wayra.org/\" target=\"_blank\"><img src=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/logo_waira.png"), "html", null, true);
        echo "\"></a>
    <a href=\"http://startupchile.org/\" target=\"_blank\"><img src=\"";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/logo_startup.png"), "html", null, true);
        echo "\"></a>
    
    
</div>
</div>
</div>
           
                    
  
</body>
</html>

<!-- modal content -->
\t\t<div id=\"basic-modal-content\">
\t\t\t<div class=\"h7\" style=\"margin:10px auto;\">Qué Hacemos</div>
\t\t\t<p class=\"gray\">Pedir un préstamo es una tarea abrumadora, toma mucho tiempo y es difícil de entender. Nosotros sólo necesitamos saber que tipo de préstamos necesitas y te entregaremos cotizaciones con las instituciones financieras que tu elijas.</p>
\t\t    <br>
            <h6 class=\"gray\">¡UNETÉ!</h6>
            <br>
            <h6 class=\"gray\">Solicita tu crédito en <span class=\"orange\">TRES</span> simples pasos:</h6>
            <br>
            
            <ul class=\"gray\" style=\"font-size:0.8em;\">
            
            <li> <span class=\"orange\">1.</span> Llena nuestros formularios </li>
            <li> <span class=\"orange\">2.</span> Recibe una lista de ofertas </li>
            <li> <span class=\"orange\">3.</span> Elige la oferta que más te acomoda </li>
            </ul>
            
            <br>
            

            
\t\t</div>
        
<!-- modal content -->
\t\t<div id=\"basic-modal-content2\">
        
                <div class=\"h7\" style=\"margin:10px auto;\">Información Legal</div>
        
        
\t\t\t<h1 class=\"orange\" style=\"margin:20px auto;\">Términos y Condiciones</h1>
\t\t\t

<h1 class=\"orange\" style=\"margin:20px auto;\">I. INTRODUCCIÓN</h1>

<p class=\"gray\">El presente sitio web ubicado en la dirección www.benchbanking.com, los servicios prestados por su intermedio, y cualquiera de las páginas que se pueda acceder a través del mismo, en adelante el \"Sitio\", y que es provisto Benchbanking S.P.A o sus sociedades relacionadas, en adelante \"Benchbanking\"; y su acceso, se encuentra condicionado a la aceptación completa del presente acuerdo que contiene las condiciones y términos del servicio, en adelante los \"términos del servicio\". El acceso al Sitio, en forma directa o indirecta, y su uso y/o descarga de información contenida en él supone que Ud. acepta los presentes términos del servicio en todas sus partes. Por el contrario si Ud. no aceptare los presentes términos del servicio, deberá abstenerse de acceder a este sitio, ya sea directa o indirectamente, y de utilizar cualquier información o servicio provista por el mismo.

Para los efectos del presente acuerdo, la abreviación \"Ud.\" y las palabras \"Usted\" o \"Usuario\" representan a cualquier persona natural o jurídica, agrupación sin personalidad legal, o representante en cualquier forma de los mismos, que use los servicios y/o información contenidas en el software o el sitio web.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Privacidad y confidencialidad:</h1>

<p class=\"gray\">El usuario declara conocer y aceptar la política de confidencialidad y se obliga a guardar absoluta reserva sobre los datos aquí proporcionados.

Toda la información personal proporcionada por Ud., incluidos los datos para los despachos es de responsabilidad exclusiva de quién la aporta, y solo será utilizada por BenchBanking para registrarlo en los programas y servicios, crear cuentas personales, procesar y darle seguimiento a los pedidos, contestar correos electrónicos y proporcionarle información con respecto a su cuenta y respecto de los bienes y servicios contratados.

BenchBanking se reserva el derecho de usar esta información para enviar correos electrónicos con información relativa a su cuenta o de los bienes y servicios contratados, como también para enviar información sobre promociones, productos o servicios que le puedan interesar, a menos que Usted indique que no desea recibir dichos correos electrónicos.

BenchBanking no venderá, arrendará o intercambiará con terceros la información personal que Usted nos proporcione.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Propiedad Intelectual e Industrial:</h1>

<p class=\"gray\">El contenido, organización, gráficas, diseño, compilación y otros aspectos del Sitio se encuentran protegidos por leyes de Propiedad Intelectual. La copia, redistribución, uso o publicación por Ud. de cualquiera de tales materias o partes esta prohibida por la ley.

La publicación o transmisión de la información o de documentos en este sitio, no constituye una renuncia de cualquier derecho relacionado con tales documentos o informaciones.

El acceso, impresión, descarga o transmisión de cualquier contenido, gráficas, imágenes, logotipos, formularios o documentos del Sitio, le otorga al usuario únicamente el derecho limitado y no exclusivo para su uso propio, y no para su republicación distribución, cesión, sublicencia, venta o cualquier otro uso.

En caso de que los usuarios deseen utilizar las marcas, nombres, logos o cualquier otro signo distintivo disponible en el Sitio, y que sean de propiedad de BenchBanking, puede solicitar la autorización necesaria poniéndose en contacto con el webmaster dirigiéndose al mismo por los medios de contacto indicados en los presentes términos del servicio.

En caso de que los usuarios deseen utilizar las marcas, nombres, logos o cualquier otro signo distintivo disponible en el Sitio, y que sean de propiedad de BenchBanking, puede solicitar la autorización necesaria poniéndose en contacto con el webmaster dirigiéndose al mismo por los medios de contacto indicados en los presentes términos del servicio.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Acceso al Sitio:</h1>

<p class=\"gray\">El acceso al Sitio está restringido a los usuarios autorizados, previa suscripción de los mismos al servicio. Para el acceso a determinados servicios se requiere el registro de usuario en las bases de datos de BenchBanking.

Es obligación del Usuario el custodiar sus datos y claves de acceso, puesto que son para el uso exclusivo del titular de las mismas, y su custodia y correcta utilización son de su entera responsabilidad, respondiendo por tanto de las violaciones a las obligaciones de confidencialidad y privacidad de la información contenida en el sitio.

Obligaciones sobre la veracidad de la información:

Toda la información que proporcione el Usuario a BenchBanking deberá ser veraz y comprobable. Para estos efectos el Usuario garantiza la autenticidad de todos los datos proporcionados, y la actualización de los mismos, siendo éste responsable por los daños que cualquier inexactitud pueda causarle al mismo, a BenchBanking o a terceros.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Vínculos:</h1>

<p class=\"gray\">El presente sitio puede contener hipervínculos a otros sitios web, que no sean controlados, editados, ni tengan relación legal alguna con los sitios de BenchBanking, no siendo esta responsable por tanto del contenido ni la exactitud de la información contenida en ellos.

La función de los links que se encuentran en este sitio es meramente informativa, y se limita solo a dar a conocer al usuario otras fuentes de información relacionadas a las materias propias del sitio. BenchBanking en ningún caso es responsable de la información que directa o indirectamente se pueda obtener de los sitios a los que se acceda a través de los hipervínculos contenidos en el sitio.

Aquellos usuarios que deseen establecer hipervínculos con el sitio deberán abstenerse de realizar manifestaciones falsas, inexactas o incorrectas sobre el sitio o su contenido.

En ningún caso se dará a entender que BenchBanking autoriza el hipervínculo, o que supervisa, aprueba o asume de cualquier forma los contenidos o servicios ofrecidos o puestos a disposición en la página web en la que se establezca el vínculo al sitio.

El establecimiento de estos vínculos no implica en forma alguna la existencia de relación alguna entre BenchBanking y el titular de la pagina web en la que se establezca el mismo.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Capacidad:</h1>

<p class=\"gray\">Los Servicios sólo están disponibles para personas que tengan capacidad legal para contratar. No podrán utilizar los servicios las personas que no tengan esa capacidad, los menores de edad o usuarios que hayan sido suspendidos temporalmente o inhabilitados definitivamente.

Cambio de los Términos de Servicio:

Nos reservamos el derecho, en cualquier tiempo, de modificar, adicionar, eliminar, alterar y poner al día los presentes términos del servicio. Los cambios referidos se harán efectivos inmediatamente una vez notificados los mismos, la cual podrá efectuarse por cualquier medio fehaciente, entre otras, por su publicación en el sitio. Será de su responsabilidad revisar periódicamente los Términos de Servicio en el Sitio. Su uso del presente sitio con posterioridad a haberse efectuado algún cambio en los Términos de Servicio, constituirá una aceptación expresa de los mismos.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Prohibiciones:</h1>

<p class=\"gray\">Además de las prohibiciones que expresamente se contengan en otras secciones de los presentes términos del servicio, se prohíbe a los Usuarios usar el Sitio de cualquier forma que pudiere dañar, deshabilitar, afectar o sobrecargar el sitio; cargar, hacer avisos o de cualquier otra forma transmitir o distribuir en este sitio cualquier ítem, incluyendo sin limitación alguna virus computacionales, caballos troyanos, gusanos, mecanismos de apagado automático o cualquier otro sistema, programa o archivo disruptivo; interferir con la seguridad de este sitio; infringir patentes comerciales, marcas registradas, secretos comerciales e industriales, derechos de publicidad o cualquier otros derechos propietarios de cualquier persona natural o jurídica, colectividades, etc.; impedir o interrumpir el uso del sitio por parte de terceros; usar programas automáticos, mecanismos o procesos manuales para monitorear, copiar, resumir, o extraer información de cualquier otra forma desde este sitio; usar las cuentas y claves de terceros Usuarios, o cualquier otra información sin el consentimiento previo y por escrito de su titular; crear cuentas o utilizar el sitio proporcionando datos falsos; transmitir desde este sitio SPAM, cadenas, correo basura o cualquier otro tipo de correo masivo no solicitado; cargar, distribuir o diseminar desde el sitio material o información amenazante, de acoso, difamatorio, obsceno, pornográfico, fraudulento, engañador, o que de cualquier otra forma pudiere ser atentatorio contra la Ley, las buenas costumbres o que viole los derechos de cualquier otra parte, o que contenga solicitudes de recaudación de fondos, entre otras.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Fallas de Sistema:</h1>

<p class=\"gray\">BenchBanking no se responsabiliza por cualquier daño, perjuicio o pérdida al Usuario causados por fallas en el software, en el servidor o en Internet. BenchBanking tampoco será responsable por cualquier virus que pudiera infectar el equipo del Usuario como consecuencia del acceso, uso o examen del sitio o a raíz de cualquier transferencia de datos, archivos, imágenes, textos, o audio contenidos en el mismo.

Los usuarios no podrán imputarle responsabilidad alguna ni exigir pago por lucro cesante, en virtud de perjuicios resultantes de dificultades técnicas o fallas en el software, los sistemas o en Internet. BenchBanking no garantiza el acceso y uso continuado o ininterrumpido de su sitio. El sistema puede eventualmente no estar disponible debido a dificultades técnicas o fallas de Internet, o por cualquier otra circunstancia ajena a BenchBanking; en tales casos se procurará restablecerlo con la mayor celeridad posible sin que por ello pueda imputársele algún tipo de responsabilidad. BenchBanking no será responsable por ningún error u omisión contenidos en el Sitio web.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Responsabilidad:</h1>

<p class=\"gray\">BenchBanking no es responsable por la pérdida o el daño eventual que puedan sufrir los archivos que le son enviados para la prestación de sus servicios, ya sea por problemas técnicos, hecho suyos o de sus dependientes, razón por la que es responsabilidad del Usuario el respaldo de la información que se envía para la prestación de los servicios.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Jurisdicción y Ley Aplicable:</h1>

<p class=\"gray\">Este acuerdo estará regido por las leyes de la República de Chile.

Cualquier controversia derivada del presente acuerdo, su existencia, validez, interpretación, alcance o cumplimiento, será sometida a las leyes aplicables y a los Tribunales competentes de la Ciudad de Santiago de Chile, y los procedimientos se llevarán a cabo en idioma castellano.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">Términos y Condiciones adicionales para todos los productos y servicios ofrecidos a través de BenchBanking:</h1>

<p class=\"gray\">Todos los créditos cotizados y contratados de cualquier forma desde el Sitio o de cualquier sitio web asociado a BenchBanking (en lo sucesivo, los \"Creditos\") son créditos que estarán sujetos a los términos y condiciones del banco cotizante. El Banco es el prestador del crédito que usted está contratando.

El titular y el emisor del crédito es el Banco. Como titular y emisor del credito, el banco será plenamente responsable de cualquier y todas las lesiones, enfermedades, daños y perjuicios, demandas, responsabilidades y costos sufridos por o en relación con un cliente, causados en su totalidad o en parte por el Banco, así como de cualquier responsabilidad derivada de BenchBanking.</p>

</div> 

<!-- modal content -->
\t\t<div id=\"basic-modal-content3\">
        <div class=\"h7\" style=\"margin:10px auto;\">Conoce de Créditos</div>
        <h1 class=\"orange\">¿Qué es la CAE?</h1>
        <br>
<p class=\"gray\">Es la Carga Anual Equivalente. Es un porcentaje que permite que usted pueda comparar qué empresa le ofrece el crédito más barato.
Verifique lo siguiente:
Siempre será más barato el crédito que tenga la carga Anual Equivalente más baja.
La Carga Anual Equivalente incluye todos los gastos y costos del crédito y los expresa en un solo porcentaje que permite compararlo.
Fíjese en LA CAE, así usted podrá vitrinear y elegir la opción de crédito que más le conviene.</p>
<br>

<ul class=\"gray\" style=\"font-size:0.8em;\">
<li><span class=\"orange\">•</span> Mismo plazo (ejemplo 12 meses)</li>
<li><span class=\"orange\">•</span> Mismo monto ( ejemplo 1 millón de pesos)</li>
<li><span class=\"orange\">•</span> Mire el porcentaje que señala LA CAE</li>
</ul>

<br>

<h1 class=\"orange\">¿Qué me deben informar al solicitar un crédito?</h1>
<br>

<h6 class=\"gray\">Deben informarle:</h6>
<br>


<ul class=\"gray\" style=\"font-size:0.8em;\">
<li><span class=\"orange\">•</span> El costo total del producto o servicio. La CAE o Carga Anual</li>
<li><span class=\"orange\">•</span> Equivalente.</li>
<li><span class=\"orange\">•</span> El precio al contado del bien o servicio de que se trate.</li>
<li><span class=\"orange\">•</span> La tasa de interés del crédito.</li>\t\t
<li><span class=\"orange\">•</span> Otros costos como: Impuestos correspondientes a la operación del crédito, gastos notariales, seguros que haya aceptado o contratado, otros.</li>
<li><span class=\"orange\">•</span> Las alternativas que le ofrecen para pagar el crédito según monto solicitado, por ejemplo: número de cuotas, número de pagos a efectuar y su periodicidad ( mensual, anual, etc).</li>
</ul>

</div> 

<!-- modal content -->
<div id=\"basic-modal-content4\">
\t\t<div class=\"h7\" style=\"margin:10px auto;\">Privacidad</div>
        
        
<h1 class=\"orange\" style=\"margin:20px auto;\">Protegemos su información personal y sus datos </h1>
\t\t\t

<p class=\"gray\">Debido a la actividad comercial que desarrolla BenchBanking Chile recoge y, en algunos casos, comunica información sobre los Usuarios de su Sitio web.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">1. Protegiéndolo</h1>
<p class=\"gray\">BenchBanking Chile está comprometido a proteger su privacidad. En esta Política de Privacidad se describe la información que BenchBanking Chile recoge sobre sus Usuarios y visitantes y lo que puede ocurrir con dicha información. Si no está de acuerdo con esta Política de Privacidad no utilice el Sitio. Nuestra Política de Privacidad asegura que cualquier información que nos provea será mantenida privada y segura. Para dar fe de esto, en este documento proveemos los detalles de qué información recabamos y de qué manera la utilizamos. Nunca recolectaremos información sin su consentimiento explícito. Este documento es parte integrante de los Términos y Condiciones de BenchBanking Chile. Mediante la aceptación de los Términos y Condiciones el Usuario acepta las Políticas de Privacidad aquí contenidas. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">2. La información que recabamos. </h1> 
        <p class=\"gray\"> Recabamos información de identificación personal (IIP) que es cedida voluntariamente durante el proceso de registración o en respuesta a requerimientos explícitos presentados por BenchBanking Chile También podemos recabar su dirección de IP (Internet Protocol) para ayudar a diagnosticar problemas con nuestro servidor, y para administrar el Sitio. Una dirección de IP es un número que se le asigna a su computadora cuando usa internet. Su dirección de IP también es utilizada para ayudar a identificarle dentro de una sesión particular y para recolectar información demográfica general. Podemos solicitarte tu e-mail durante la utilización del sitio.

Se establece que en cualquier momento los Usuarios de BenchBanking Chile podrán solicitar la baja de su solicitud y la eliminación de su cuenta e información de la base de datos de BenchBanking Chile. BenchBanking Chile recoge y almacena automáticamente cierta información sobre la actividad de los Usuarios dentro de nuestro Sitio web. Tal información puede incluir la URL de la que provienen (estén o no en nuestro Sitio web), a qué URL acceden seguidamente (estén o no en nuestro Sitio web), qué navegador están usando, así como también las páginas visitadas, las búsquedas realizadas, las publicaciones, compras o ventas, mensajes, etc. </p>   
        
        <h1 class=\"orange\" style=\"margin:20px auto\">3. Uso que hacemos de la información. </h1> 
        <p class=\"gray\"> Para suministrar mejores cotizaciones de créditos por parte de los bancos, BenchBanking Chile requiere ciertos datos de carácter personal. La recolección de información nos permite ofrecerles servicios y funcionalidades que se adecuan mejor a sus necesidades. Los Datos Personales que recabamos tienen las siguientes finalidades:

- Entregar a los bancos un resumen de cada usuario para recibir cotización de créditos. - Ponernos en contacto directo con Ud. cada vez que BenchBanking Chile lo considere conveniente con el fin de ofrecerle por distintos medios y vías (incluyendo mail, SMS, etc.) BenchBankinges de otros productos y/o servicios. -Mejorar nuestras iniciativas comerciales y promocionales para analizar las páginas más visitadas por los Usuarios, las búsquedas realizadas, perfeccionar nuestra oferta de contenidos, personalizar dichos contenidos, presentación y servicios. -Enviar información o mensajes sobre cotizaciones de bancos. Si el Usuario lo prefiere, puede solicitar que lo excluyan de las listas para el envío de información promocional o publicitaria. - -Suministrar los Datos Personales de los Usuarios a las entidades que intervengan en la resolución de disputas entre los mismos, tales como: Compañías de Seguros o tribunales competentes para solucionar tales disputas. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">4. Confidencialidad de la información. </h1> 
        <p class=\"gray\">Los datos de los Usuarios serán suministrados únicamente por BenchBanking Chile en las formas establecidas en estas Políticas de Privacidad. BenchBanking Chile hará todo lo que esté a su alcance para proteger la privacidad de la información. Puede suceder que en virtud de órdenes judiciales, o de regulaciones legales, BenchBanking Chile se vea compelido a revelar información a las autoridades o terceras partes bajo ciertas circunstancias, o bien en casos que terceras partes puedan interceptar o acceder a cierta información o transmisiones de datos en cuyo caso BenchBanking Chile no responderá por la información que sea revelada. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">5. Menores de Edad  </h1>
        <p class=\"gray\">Nuestros servicios sólo están disponibles para aquellas personas que tengan capacidad legal para contratar. Por lo tanto, aquellos que no cumplan con esta condición deberán abstenerse de suministrar información personal para ser incluida en nuestras bases de datos. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">6. Cookies  </h1>
        <p class=\"gray\">El Usuario del Sitio web de BenchBanking Chile conoce y acepta que BenchBanking Chile podrá utilizar un sistema de seguimiento mediante la utilización de cookies (las \"Cookies\"). Las Cookies son pequeños archivos que se instalan en el disco rígido, con una duración limitada en el tiempo que ayudan a personalizar los servicios. También ofrecemos ciertas funcionalidades que sólo están disponibles mediante el empleo de Cookies. Las Cookies se utilizan con el fin de conocer los intereses, el comportamiento y la demografía de quienes visitan o son visitantes de nuestro Sitio web y de esa forma, comprender mejor sus necesidades e intereses y darles un mejor servicio o proveerle información relacionada. También usaremos la información obtenida por intermedio de las Cookies para analizar las páginas navegadas por el visitante o Usuario, las búsquedas realizadas, mejorar nuestras iniciativas comerciales y promocionales, mostrar publicidad o promociones, banners de interés, noticias sobre BenchBanking Chile, perfeccionar nuestra oferta de contenidos y artículos, personalizar dichos contenidos, presentación y servicios. Adicionalmente BenchBanking Chile utiliza las Cookies para que el Usuario no tenga que introducir su clave tan frecuentemente durante una sesión de navegación, también para contabilizar y corroborar los registros, la actividad del Usuario y otros conceptos y acuerdos comerciales, siempre teniendo como objetivo de la instalación de las Cookies, el beneficio del Usuario que la recibe, y no será usado con otros fines ajenos a BenchBanking Chile. Se establece que la instalación, permanencia y existencia de las Cookies en el computador del Usuario o del visitante depende de su exclusiva voluntad y puede ser eliminada de su computador cuando así lo desee. Para saber cómo quitar las Cookies del sistema es necesario revisar la sección Ayuda (Help) del navegador. También, se pueden encontrar Cookies u otros sistemas similares instalados por terceros en ciertas páginas de nuestro Sitio. BenchBanking Chile no controla el uso de Cookies por terceros. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">7. Emails </h1>
        <p class=\"gray\"> Podemos enviarle e-mails con los siguientes propósitos:

- Como parte del Servicio. Por ejemplo, le enviaremos (o podríamos enviarle) e-mails en las siguientes circunstancias -- Luego de la registración, notificándole los datos de su cuenta -- E-mails con recordatorios de los servicios que ofrecemos (especialmente aquellos que aun no haya utilizado o no haya utilizado en un tiempo considerable) -- Para enviar información sobre los creditos que haya pedido - Como parte de un Newsletter - Como e-mails promocionales - Para ofrecer servicios relacionados

De todas maneras, en cada uno de los e-mails que enviemos siempre ofreceremos la posibilidad de desuscribirse (opt-out) para dejar de recibir e-mails en el futuro. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">8. Seguridad y almacenamiento  </h1>
        <p class=\"gray\">Empleamos diversas técnicas de seguridad para proteger tales datos de accesos no autorizados por visitantes del Sitio de dentro o fuera de nuestra compañía. Sin embargo, es necesario tener en cuenta que la seguridad perfecta no existe en Internet. Por ello, BenchBanking Chile no se hace responsable por interceptaciones ilegales o violación de sus sistemas o bases de datos por parte de personas no autorizadas. BenchBanking Chile, tampoco se hace responsable por la indebida utilización de la información obtenida por esos medios. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">9. Transferencia en circunstancias especiales   </h1>
        <p class=\"gray\">Si existe una venta, una fusión, consolidación, cambio en el control societario, transferencia de activos sustancial, reorganización o liquidación de BenchBanking Chile entonces, en nuestra discreción, podemos transferir, vender o asignar la información recabada en este Sitio a una o más partes relevantes. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">10. Servicio al cliente   </h1>
        <p class=\"gray\">Si tiene alguna duda o preocupación no dude en contactarse con nuestro servicio al cliente: info@BenchBanking.com</p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">11. Modificaciones de las Políticas de Privacidad  </h1>
        <p class=\"gray\">BenchBanking Chile podrá modificar en cualquier momento los términos y condiciones de estas Políticas de Privacidad y confidencialidad. Cualquier cambio será efectivo apenas sea publicado en el Sitio. Dependiendo de la naturaleza del cambio podremos anunciar el mismo a través de: (a) la página de inicio del Sitio, o (b) un e-mail. De todas maneras, el continuo uso de nuestro Sitio implica la aceptación por parte del Usuario de los Terminos de esta Politica de Privacidad. Si Ud. no está de acuerdo con la Politica de Privacidad vigente absténgase de utilizar el Sitio. </p>
       
\t\t</div>
        
 <!-- modal content -->
\t\t<div id=\"basic-modal-content5\">
      <iframe  style=\"width:700px; height:493px; position:relative; margin:auto; margin-left:40px;\"src=\"https://player.vimeo.com/video/62476396\" width=\"700\" height=\"493\" frameborder=\"0\" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
   <input id=\"token\" value=\"";
        // line 332
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\" type=\"hidden\">
      
            
\t\t</div>         


        


";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 332,  136 => 108,  132 => 107,  57 => 35,  23 => 4,  19 => 2,);
    }
}
