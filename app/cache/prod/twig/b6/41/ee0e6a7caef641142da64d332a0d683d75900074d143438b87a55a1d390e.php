<?php

/* BenchUploadBundle:Default:upload.html.twig */
class __TwigTemplate_b641ee0e6a7caef641142da64d332a0d683d75900074d143438b87a55a1d390e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\">
<head>
<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"/>



<style type=\"text/css\">
\tbody {
\t\tfont-family:Verdana, Geneva, sans-serif;
\t\tfont-size:13px;
\t\tcolor:#333;
\t\tbackground:url(bg.jpg);
\t}
</style>

<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/html5.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"http://bp.yahooapis.com/2.4.21/browserplus-min.js\"></script>

<script type=\"text/javascript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.gears.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.silverlight.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.flash.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.browserplus.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.html4.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.html5.js"), "html", null, true);
        echo "\"></script>

</head>
<body>

<h1>test upload nuevo</h1>

<p>.</p>

<div id=\"container\">
    <div id=\"filelist\">lista archivos.</div>
    <br />
    <a id=\"pickfiles\" href=\"javascript:;\">Selecciona</a> 
    <a id=\"uploadfiles\" href=\"javascript:;\">[Upload files]</a>
</div>


<div id=\"container\">





<form id='upload5' action=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("uploadArchivo5");
        echo "\" method='post'></form>

  <script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-1.7.1.min.js\"></script>


<script type=\"text/javascript\">
    \$('document').ready(function() {
// Custom example logic

var navegador = navigator.appName;

if (navegador == 'Microsoft Internet Explorer'){
    

function \$(id) {
\treturn document.getElementById(id);\t
}

var uploader = new plupload.Uploader({
\t
        
        runtimes : 'flash',
\tbrowse_button : 'pickfiles',
\tcontainer: 'container',
\tmax_file_size : '10mb',
\turl : 'uploadArchivo5',
\t//resize : {width : 320, height : 240, quality : 90},
\tflash_swf_url : '  ";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.flash.swf"), "html", null, true);
        echo "',
\tsilverlight_xap_url : 'bundles/benchupload/js/plupload.silverlight.xap',
        
        
      
\tfilters : [
\t\t{title : \"Image files\", extensions : \"jpg,gif,png\"},
\t\t{title : \"Zip files\", extensions : \"zip\"}
\t]
});


uploader.bind('Init', function(up, params) {
\t\$('filelist').innerHTML = \"<div>Current runtime: \" + params.runtime + \"</div>\";
});

uploader.init();

uploader.bind('FilesAdded', function(up, files) {
\tfor (var i in files) {
\t\t\$('filelist').innerHTML += '<div id=\"' + files[i].id + '\">' + files[i].name + ' (' + plupload.formatSize(files[i].size) + ') <b></b></div>';
\t}
});

uploader.bind('UploadProgress', function(up, file) {
\t\$(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + \"%</span>\";
});

\$('uploadfiles').onclick = function() {
\tuploader.start();
\treturn false;
};


}else{
    
    
    function \$(id) {
\treturn document.getElementById(id);\t
}


var uploader = new plupload.Uploader({
\t
        runtimes : 'html5',
\tbrowse_button : 'pickfiles',
\tcontainer: 'container',
\tmax_file_size : '10mb',
\turl : 'uploadArchivo5',
\tresize : {width : 320, height : 240, quality : 90},
\tflash_swf_url : '  ',
\tsilverlight_xap_url : 'bundles/benchupload/js/plupload.silverlight.xap',
        
        
      
\tfilters : [
\t\t{title : \"Image files\", extensions : \"jpg,gif,png\"},
\t\t{title : \"Zip files\", extensions : \"zip\"}
\t]
});


uploader.bind('Init', function(up, params) {
\t\$('filelist').innerHTML = \"<div>Current runtime: \" + params.runtime + \"</div>\";
});

uploader.init();

uploader.bind('FilesAdded', function(up, files) {
\tfor (var i in files) {
\t\t\$('filelist').innerHTML += '<div id=\"' + files[i].id + '\">' + files[i].name + ' (' + plupload.formatSize(files[i].size) + ') <b></b></div>';
\t}
});

uploader.bind('UploadProgress', function(up, file) {
\t\$(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + \"%</span>\";
});

\$('uploadfiles').onclick = function() {
\tuploader.start();
\treturn false;
};

    
    
}
});
</script>


  
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "BenchUploadBundle:Default:upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 76,  93 => 49,  67 => 26,  63 => 25,  59 => 24,  55 => 23,  51 => 22,  47 => 21,  43 => 20,  37 => 17,  19 => 1,);
    }
}
