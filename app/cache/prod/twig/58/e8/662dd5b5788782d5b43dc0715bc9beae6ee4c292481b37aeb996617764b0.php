<?php

/* BenchUsuariosBundle:Default:conyuge.html.twig */
class __TwigTemplate_58e8662dd5b5788782d5b43dc0715bc9beae6ee4c292481b37aeb996617764b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'containerConyuge' => array($this, 'block_containerConyuge'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('containerConyuge', $context, $blocks);
        // line 366
        echo " 
";
    }

    // line 3
    public function block_containerConyuge($context, array $blocks = array())
    {
        echo " 

<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchusuarios/js/conyuge.js"), "html", null, true);
        echo "\"></script>

<form class=\"form-personal\" id=\"formConyuge\" method=\"post\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("datosconyugeguarda");
        echo "\" autocomplete=\"off\" >


<input type=\"hidden\" id=\"token\" value=\"";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
    <div class=\"fila\">

        <!--<h1 class=\"doc\"><span class=\"orange\">Datos Conyugue</span></h1>-->
        <div class=\"col1\">
            <label class=\"label-text2\" for=\"nombres\">Nombres:</label>


                   ";
        // line 18
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 19
            echo "

            <input type=\"text\" name=\"nombres\" value=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "nombre", array()), "html", null, true);
            echo "\" id=\"Connombres\" class=\"input-box2\" data-validation-engine=\"validate[required]\"   >


                  ";
        } else {
            // line 24
            echo " 

            <input type=\"text\" name=\"nombres\" value=\"\" id=\"Connombres\" class=\"input-box2\" data-validation-engine=\"validate[required]\"   >

                  ";
        }
        // line 28
        echo " 




            <label class=\"label-text2\" for=\"apellido1\">Apellido Paterno:</label>


           ";
        // line 36
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 37
            echo "

            <input type=\"text\" name=\"apellido1\" class=\"input-box2\" id=\"Conapellido1\" value=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "apellido", array()), "html", null, true);
            echo "\" data-validation-engine=\"validate[required]\"  />



         ";
        } else {
            // line 43
            echo " 

            <input type=\"text\" name=\"apellido1\" class=\"input-box2\" id=\"Conapellido1\" value=\"\" data-validation-engine=\"validate[required]\"  />

          ";
        }
        // line 47
        echo " 



            <label class=\"label-text2\" for=\"apellido2\">Apellido Materno:</label>

               ";
        // line 53
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 54
            echo "
            <input type=\"text\" name=\"apellido2\" class=\"input-box2\" id=\"Conapellido2\"  value=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "apellidoseg", array()), "html", null, true);
            echo "\" data-validation-engine=\"validate[required]\"  />


                ";
        } else {
            // line 58
            echo " 


            <input type=\"text\" name=\"apellido2\" class=\"input-box2\" id=\"Conapellido2\"  value=\"\" data-validation-engine=\"validate[required]\"  />



                   ";
        }
        // line 65
        echo " 

            <label class=\"label-text2\" for=\"rut\">RUT:</label>


             ";
        // line 70
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 71
            echo "
            <input type=\"text\" name=\"rut\" class=\"input-box2\" id=\"Conrutconyuge\" value=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "rut", array()), "html", null, true);
            echo "\" data-validation-engine=\"validate[required]\"  />

                      ";
        } else {
            // line 74
            echo " 

            <input type=\"text\" name=\"rut\" class=\"input-box2\" id=\"Conrutconyuge\" value=\"\" data-validation-engine=\"validate[required]\"  />



                       ";
        }
        // line 80
        echo " 




            <label class=\"label-text2\">Fecha de Nacimiento:</label>

                ";
        // line 87
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 88
            echo "



            <input type=\"text\" name=\"nacimiento\" id=\"Connacimiento\" class=\"input-box2\"  value=\"";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "fechanacimiento", array()), "html", null, true);
            echo "\" data-validation-engine=\"validate[required]\"  />

       ";
        } else {
            // line 94
            echo " 


            <input type=\"text\" name=\"nacimiento\" id=\"Connacimiento\" class=\"input-box2\"  value=\"\" data-validation-engine=\"validate[required]\"  />

              ";
        }
        // line 99
        echo " 



        </div>

        <div class=\"col1\">

            <label class=\"tipo\" for=\"\">Nacionalidad</label>



            <select class=\"data\" data-errormessage-value-missing=\"Debe seleccionar un opcion\" data-validation-engine=\"validate[required]\" name=\"nacionalidad\"  id=\"Connacionalidad\" data-validation-engine=\"validate[required]\" >


                 ";
        // line 114
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 115
            echo "
                <option value=\"";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "nacionalidad", array()), "html", null, true);
            echo "\" selected>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "nacionalidad", array()), "html", null, true);
            echo "</option>


                    ";
        } else {
            // line 119
            echo " 



                <option value=\"\">Opción</option>

                    ";
        }
        // line 125
        echo " 



                <option value=\"Chileno\">Chileno</option>
                <option value=\"Extranjero Residente\">Extranjero Residente</option>
                <option value=\"Extranjero No Residente\">Extranjero No Residente</option>
            </select>



            <label class=\"tipo\" for=\"\" >Nivel de Educación</label>



            <select class=\"data\" 
                    data-errormessage-value-missing=\"Debe seleccionar un opcion\" data-validation-engine=\"validate[required]\"  name=\"niveleducacion\" id=\"Conniveleducacion\">





  ";
        // line 147
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 148
            echo "
                <option value=\"";
            // line 149
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "niveleducacion", array()), "html", null, true);
            echo "\"><p class=\"seleccionado\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "niveleducacion", array()), "html", null, true);
            echo " </p></option>
 ";
        } else {
            // line 150
            echo " 

                <option value=\"\" select>Opción</option>

     ";
        }
        // line 154
        echo " 




                <option value=\"basico\">Basico</option>
                <option value=\"medio\">Medio</option>
                <option value=\"tecnico\">Técnico</option>
                <option value=\"universitario\">Universitario</option>
                <option value=\"postgrado\">Postgrado</option>



            </select>





            <label class=\"label-text2\" for=\"Profesión\">Profesión</label>


              ";
        // line 176
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 177
            echo "


            <input type=\"text\" class=\"input-box2\" autofocus  value=\"";
            // line 180
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "profesion", array()), "html", null, true);
            echo "\" name=\"profesion\"  id=\"Conprofesion\" data-validation-engine=\"validate[required]\" >




             ";
        } else {
            // line 186
            echo "
            <input type=\"text\" class=\"input-box2\" autofocus  value=\"\" name=\"profesion\"  id=\"Conprofesion\" data-validation-engine=\"validate[required]\" >


              ";
        }
        // line 190
        echo "  






            <label class=\"label-text2\" >Universidad o Instituto</label>


            ";
        // line 200
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 201
            echo "
            <input type=\"text\" class=\"input-box2\" autofocus  value=\"";
            // line 202
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "universidad", array()), "html", null, true);
            echo "\" name=\"universidad\" id=\"Conuniversidad\" data-validation-engine=\"validate[required]\" >

              ";
        } else {
            // line 205
            echo "
            <input type=\"text\" class=\"input-box2\" autofocus  value=\"\" name=\"universidad\" id=\"Conuniversidad\" data-validation-engine=\"validate[required]\" >

             ";
        }
        // line 208
        echo "  



            <label class=\"label-text2\" for=\"\">Actividad</label>


           ";
        // line 215
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 216
            echo "            
            
            

            
            
            <input type=\"text\" class=\"input-box2\" autofocus value=\"";
            // line 222
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "actividad", array()), "html", null, true);
            echo "\" name=\"actividad\" id=\"Conactividad\" data-validation-engine=\"validate[required]\" >

            
            
           ";
        } else {
            // line 227
            echo "
            
            
            <input type=\"text\" class=\"input-box2\" autofocus value=\"\" name=\"actividad\" id=\"Conactividad\" data-validation-engine=\"validate[required]\" >



           ";
        }
        // line 234
        echo "  


        </div>
        <div class=\"col1\">


            <label class=\"tipo\" for=\"\">Cargo Actual</label>
            
            
                  ";
        // line 244
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 245
            echo "            
            
            <input type=\"text\" class=\"input-box2\"  autofocus=\"\"  id=\"Concargo\" name=\"cargo\" value=\"";
            // line 247
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "cargoactual", array()), "html", null, true);
            echo "\"  data-validation-engine=\"validate[required]\" >

            
                   ";
        } else {
            // line 251
            echo "
            <input type=\"text\" class=\"input-box2\"  autofocus=\"\"  id=\"Concargo\" name=\"cargo\" value=\"\"  data-validation-engine=\"validate[required]\" >

            
            
                   ";
        }
        // line 256
        echo "  

            
            
            <label class=\"tipo\" for=\"\">Antigüedad Laboral</label>
            
            
             ";
        // line 263
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 264
            echo "            
            
            <input type=\"text\" class=\"input-box2\"  autofocus=\"\"  name=\"antiguedad\"  id=\"ConAntiguedad\" value=\"";
            // line 266
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "antiguedadlaboral", array()), "html", null, true);
            echo "\" data-validation-engine=\"validate[required]\" >

               ";
        } else {
            // line 269
            echo "            
            
              
            <input type=\"text\" class=\"input-box2\"  autofocus=\"\"  name=\"antiguedad\"  id=\"ConAntiguedad\" value=\"\" data-validation-engine=\"validate[required]\" >

            
              ";
        }
        // line 275
        echo "  
              
              
            
            
            
            
            <label class=\"tipo\" for=\"\"> Renta Líquida</label>
            
            
              ";
        // line 285
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 286
            echo "            
            
            <input type=\"text\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\" id=\"ConRenta\" class=\"input-box2\" value=\"";
            // line 288
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "rentaliquidad", array()), "html", null, true);
            echo "\" name=\"renta\" placeholder=\"\$\" data-validation-engine=\"validate[required]\">

            
                ";
        } else {
            // line 292
            echo "            
              
            <input type=\"text\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\" id=\"ConRenta\" class=\"input-box2\" value=\"\" name=\"renta\" placeholder=\"\$\" data-validation-engine=\"validate[required]\">

            
                ";
        }
        // line 297
        echo "  
                
                
            
            <label class=\"tipo\" for=\"direccion\">Empresa Trabajo</label>
            
            
             
            
              ";
        // line 306
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 307
            echo "            
            
            <input type=\"text\" class=\"input-box2\" autofocus name=\"empresa\" id=\"Conempresa\"  value=\"";
            // line 309
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "empresa", array()), "html", null, true);
            echo "\" data-validation-engine=\"validate[required]\" >
                
            
            
              ";
        } else {
            // line 314
            echo "            
            
              <input type=\"text\" class=\"input-box2\" autofocus name=\"empresa\" id=\"Conempresa\"  value=\"\" data-validation-engine=\"validate[required]\" >
                
            
              ";
        }
        // line 319
        echo "  
                
            
            
            <label class=\"label-text2\" for=\"telefono\">Teléfono</label>
            
            
             ";
        // line 326
        if ((isset($context["conyuge"]) ? $context["conyuge"] : null)) {
            // line 327
            echo "            
            
            <input type=\"text\" class=\"input-box2\" autofocus  name=\"telefono\"  id=\"Contelefono\" value=\"";
            // line 329
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["conyuge"]) ? $context["conyuge"] : null), "telefono", array()), "html", null, true);
            echo "\" data-validation-engine=\"validate[required]\" >


              ";
        } else {
            // line 333
            echo "            
            
            <input type=\"text\" class=\"input-box2\" autofocus  name=\"telefono\"  id=\"Contelefono\"value=\"\" data-validation-engine=\"validate[required]\" >


            ";
        }
        // line 338
        echo "  
                





        </div>



    </div>


    <div class=\"fila\">
        <div class=\"col1\"><br></div> 
        <div class=\"clearfix\"></div>
        <div class=\"col1\">
            <br>                    
            <button style=\"float:left;\" id=\"botonconyugue\" class=\"classname\" value=\"Guardar\" type=\"submit\">GUARDAR Y SIGUIENTE</button>
        </div>
        </form>
        <div class=\"col1\"><br></div> 
    </div>

    <section id=\"respuestaConyuge\"></section>


         ";
    }

    public function getTemplateName()
    {
        return "BenchUsuariosBundle:Default:conyuge.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  552 => 338,  544 => 333,  537 => 329,  533 => 327,  531 => 326,  522 => 319,  514 => 314,  506 => 309,  502 => 307,  500 => 306,  489 => 297,  481 => 292,  474 => 288,  470 => 286,  468 => 285,  456 => 275,  447 => 269,  441 => 266,  437 => 264,  435 => 263,  426 => 256,  418 => 251,  411 => 247,  407 => 245,  405 => 244,  393 => 234,  383 => 227,  375 => 222,  367 => 216,  365 => 215,  356 => 208,  350 => 205,  344 => 202,  341 => 201,  339 => 200,  327 => 190,  320 => 186,  311 => 180,  306 => 177,  304 => 176,  280 => 154,  273 => 150,  266 => 149,  263 => 148,  261 => 147,  237 => 125,  228 => 119,  219 => 116,  216 => 115,  214 => 114,  197 => 99,  189 => 94,  183 => 92,  177 => 88,  175 => 87,  166 => 80,  157 => 74,  151 => 72,  148 => 71,  146 => 70,  139 => 65,  129 => 58,  122 => 55,  119 => 54,  117 => 53,  109 => 47,  102 => 43,  94 => 39,  90 => 37,  88 => 36,  78 => 28,  71 => 24,  64 => 21,  60 => 19,  58 => 18,  47 => 10,  41 => 7,  36 => 5,  30 => 3,  25 => 366,  23 => 3,  20 => 2,);
    }
}
