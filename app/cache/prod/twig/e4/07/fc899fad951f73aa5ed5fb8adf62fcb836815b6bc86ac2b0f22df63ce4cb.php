<?php

/* BenchAdminBundle:plantillas:bbva.html.twig */
class __TwigTemplate_e407fc899fad951f73aa5ed5fb8adf62fcb836815b6bc86ac2b0f22df63ce4cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Document</title>
</head>
<body>


<style>
    body{

        font-family: arial, sans-serif;
        font-size: 12px;
    }
    #contenedor{
        width: 700px;
        height: 1000px;
        /*  background-color: #ffeeee; */
        position: relative;
        margin: auto;


    }

    .logo{
        width: 200px;
        height: 70px;
        background-color: #ffffff;
        position: relative;
        float: left;



    }

    .titulo{

        width: 700px;
        background-color: #001d6e;
        height: 70px;
        position: relative;
        float: right;



    }


    h3{

        color: #ffffff;



    }

    .titulo2{
        margin-top: 49px;
        float: right;
        margin-right: 5px;
        width: auto;
        height: auto;
        position: relative;

    }
    .row880{
        position: relative;
        height: 24px;
        width: 890px;
        margin: auto;

        background-color: #ffffff;

    }

    .row880Azul{
        position: relative;
        height: 21px;
        width: 900px;
        margin: auto;

        background-color: #001d6e;

    }



    .clear{

        clear: both;
    }
    input[type=\"text\"] {
        padding: 10px;
        margin-top:-20px;
        border: none;
        text-align: center;
        border-bottom: solid 1px #000;
        transition: border 0.3s;
    }


    input[type=\"text\"]:focus,
    input[type=\"text\"].focus {
        border-bottom: solid 1px #000;
    }


    .titleInput1{
        position: relative;
        height: 100px;
        width: 100px;
        margin-top: 100px;

    }

    table{
        margin-top:10px;
    }

    .simuInput{
        width: 300px;
        height: 20px;
        border-bottom: 1px;
        border-left: 0px;
        border-right: 0px;
        border-top: 0px;
        border-style: solid;
        position: relative;
        float: left;


    }

    .titulo33{
        position: relative;
        width: auto;
        height: auto;
        float: left;
        margin-right: 8px;
    }
        

    

</style>



<div id=\"contenedor\" style=\"background-color:#FFF;\">


<table width=\"695\">
    <tr>
        <td width=\"300\" height=\"96\"><img src=\"http://www.mediadiv.cl/imgPdf/bvva.jpg\"></td>
        <td width=\"383\" style=\"background-color:#001d6e\"><span style=\"color:#FFF;width:auto;height:auto;float:right;margin-right:4px;margin-top:60px;\">PREAPROBACION DE CREDITOS </span></td>

    </tr>

</table>
<table>

    <tr></tr>
</table>


<table width=\"700\">
    <tr>


        <td width=\"301\"><span class=\"titulo33\">Inmobiliarias:       </span> <div class=\"simuInput\" style=\"width:140px; \"> &nbsp  ";
        // line 171
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "proyecto", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "proyecto", array()), "html", null, true);
                echo " ";
            }
        }
        echo "   </div>  </td>
        <td width=\"185\"><span class=\"titulo33\"> Proyecto: </span> <div class=\"simuInput\" style=\"width:90px; \"> &nbsp ";
        // line 172
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "proyecto", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "proyecto", array()), "html", null, true);
                echo " ";
            }
        }
        echo "   </div></td>

        <td width=\"198\"> <span class=\"titulo33\">Ejecutivo:</span> <div class=\"simuInput\" style=\"width:110px; \"> &nbsp </div></td>


    </tr>


</table>



<table width=\"700\" height=\"10\">
    <tr>

        <td  style=\"background-color: #001d6e;\"><span style=\"color:#FFF;\">ANTECEDENTES DEL CLIENTE</span></td>

    </tr>


</table>




<table width=\"702\">
    <tr>
        <td width=\"489\"><span class=\"titulo33\">Nombre :</span> <div class=\"simuInput\" style=\"width:390px; \"> &nbsp; ";
        // line 199
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellido", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellido", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellidoseg", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

        <td width=\"201\"> <span class=\"titulo33\"> Rut:</span> <div class=\"simuInput\" style=\"width:141px; \">&nbsp; ";
        // line 201
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "rut", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "rut", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
    </tr>


</table>



<table width=\"700\">
    <tr>
        <td colspan=\"2\"><span class=\"titulo33\" style=\"width:200px;\"> Estado Civil:</span> <div class=\"simuInput\" style=\"width:280px;\">&nbsp; ";
        // line 211
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array()), "html", null, true);
            echo " ";
        }
        echo "</div> </td>
        <td width=\"219\"> <span class=\"titulo33\"> Fecha Nacimiento:</span> <div class=\"simuInput\" style=\"width:130px; \">&nbsp; ";
        // line 212
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "fechaNacimiento", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "fechaNacimiento", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>

        <td width=\"130\"><span class=\"titulo33\">  Edad:</span> <div class=\"simuInput\" style=\"width:130px; \"> &nbsp ";
        // line 214
        echo twig_escape_filter($this->env, (isset($context["edadUsuario"]) ? $context["edadUsuario"] : null), "html", null, true);
        echo "  </div></td>
    </tr>


</table>


<table width=\"700\">

    <td><span class=\"titulo33\"> Domicilio: </span> <div class=\"simuInput\" style=\"width:602px; \"> ";
        // line 223
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo " ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
</table>


<table width=\"700\">
    <tr>
        <td width=\"236\"><span class=\"titulo33\"> Vivienda: </span> <div class=\"simuInput\" style=\"width:152px; \"> &nbsp; ";
        // line 229
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array())) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
        <td width=\"252\"><span class=\"titulo33\"> Años residencia : </span> <div class=\"simuInput\" style=\"width:150px; \"> &nbsp; </div></td>

        <td width=\"196\"><span class=\"titulo33\"> Familiares Nº:</span> <div class=\"simuInput\" style=\"width:78px;\"> &nbsp; ";
        // line 232
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "ndependientes", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "ndependientes", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
    </tr>


</table>


<table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\"> Arriendo / Dividendo: </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp; ";
        // line 241
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "montoArriendo", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "montoArriendo", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
        <td width=\"224\"><span class=\"titulo33\"> Teléfono : </span> <div class=\"simuInput\" style=\"width:120px;\"> &nbsp; ";
        // line 242
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>

     
    </tr>




</table>


<hr style=\"color: #001d6e;
background-color: #001d6e;
height: 5px;\">


<span style=\"color:#001d6e;\">DATOS PROFESIONALES DEL CLIENTE</span>


";
        // line 261
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 262
            echo "<table width=\"700\">
    <tr>
        <td width=\"326\"><span class=\"titulo33\"> Actividad : </span> <div class=\"simuInput\" style=\"width:233px;\"> &nbsp ";
            // line 264
            if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "profesion", array()), "html", null, true);
                echo "  ";
            }
            echo "</div></td>
        <td width=\"362\"><span class=\"titulo33\">  Empresa: </span> <div class=\"simuInput\" style=\"width:271px;\"> &nbsp ";
            // line 265
            if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "industria", array()), "html", null, true);
                echo " ";
            }
            echo "</div></td>


    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"205\"><span class=\"titulo33\"> Giro Emp: </span><div class=\"simuInput\" style=\"width:110px;\"> &nbsp  </div></td>
        <td width=\"179\"><span class=\"titulo33\">Rut: </span><div class=\"simuInput\" style=\"width:133px;\">&nbsp  ";
            // line 278
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "rutempresa", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "rutempresa", array()), "html", null, true);
                echo "  ";
            }
            echo " </div></td>
        <td width=\"200\"><span class=\"titulo33\"> Teléfono Of.:  </span> <div class=\"simuInput\" style=\"width:130px;\">&nbsp ";
            // line 279
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "telefono", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "telefono", array()), "html", null, true);
                echo " ";
            }
            echo " </div></td>

    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"326\"><span class=\"titulo33\"> Profesión:</span> <div class=\"simuInput\" style=\"width:234px;\"> &nbsp ";
            // line 290
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "profesion", array()), "html", null, true);
            echo "  </div></td>
        
        <td width=\"171\"><span class=\"titulo33\"> Fax.: </span> <div class=\"simuInput\" style=\"width:110px;\">&nbsp  </div></td>

    </tr>

    <tr>
        
<td width=\"187\"><span class=\"titulo33\"> Contrato:  </span> <div class=\"simuInput\" style=\"width:202px;\"> ";
            // line 298
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "situacionlaboral", array())) {
                echo " &nbsp ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "situacionlaboral", array()), "html", null, true);
                echo "  ";
            }
            echo "    </div></td>

    </tr>


</table>



<table width=\"700\">
    <tr>

      
        


        <td width=\"233\"><span class=\"titulo33\"> Fecha Inicio / Act:</span> <div class=\"simuInput\" style=\"width:90px;\">&nbsp </div> </td>
        <td width=\"200\"><span class=\"titulo33\"> Email.: </span> <div class=\"simuInput\" style=\"width:130px;\">";
            // line 315
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "email", array()), "html", null, true);
            echo " </div></td>

    </tr>


<tr>
    
      <td width=\"205\"><span class=\"titulo33\"> Cargo Actual: </span><div class=\"simuInput\" style=\"width:210px;\">&nbsp ";
            // line 322
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "cargoactual", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "cargoactual", array()), "html", null, true);
                echo " ";
            }
        }
        echo "</div></td>


</tr>
</table>


<hr style=\"color: #001d6e;
background-color: #001d6e;
height: 5px;\">


<span style=\"color:#001d6e;\">DATOS PROFESIONALES DEL CLIENTE</span>
<div style=\"clear:both;\"></div>

<table width=\"700\" style=\"\">
    <tr>
        <td width=\"161\" style=\"border: solid 1px  #001d6e;\" ><span style=\"font-size:10px; text-align:center;width:auto;position:relative;margin:auto;\"> ACTIVOS </span></td>
        <td width=\"145\"  style=\"border: solid 1px  #001d6e;\" ><span style=\"font-size:10px; text-align:center;\"> PASIVOS </span></td>
        <td width=\"378\"  style=\"border: solid 1px  #001d6e;\"><span style=\"font-size:10px;;\"> INGRESOS LIQUIDOS MENSUAL </span></td>
    </tr>
</table>



<div class=\"clear:both;\"></div>

<table width=\"700\" height=\"20\">

    <tr>
        <td width=\"55\"><p style=\"font-size:9px;\"> Tipo </p></td>
        <td width=\"102\"><p style=\"font-size:9px;\"> Valor Comercial </p></td>

        <td width=\"61\"><p style=\"font-size:9px;\"> Tipo deuda </p></td>
        <td width=\"81\"><p style=\"font-size:9px;\"> Deuda vigente </p></td>
        <td width=\"365\"><p style=\"font-size:9px;\"> Pago Mensual</p></td>
        <td width=\"8\"></td>
    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Bienes Raices</span> <div class=\"simuInput\" style=\"width:100px;\">&nbsp; ";
        // line 367
        if ((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null)) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null), 0, ".", "."), "html", null, true);
                echo " ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo "</div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Hipotecario </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp; ";
        // line 368
        if ((isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo "</div>  <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; ";
        if ((isset($context["totalDebesHipotecarioMensual"]) ? $context["totalDebesHipotecarioMensual"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecarioMensual"]) ? $context["totalDebesHipotecarioMensual"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : null)) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : null), 0, ".", "."), "html", null, true);
                echo "  ";
            } else {
                echo " NO  ";
            }
            echo " ";
        }
        echo "   </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Sueldo  </span> <div class=\"simuInput\" style=\"width:150px;\">&nbsp; ";
        // line 369
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "independientesueldoliquido", array()), "html", null, true);
            echo " ";
        }
        echo " </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Vehículos </span><div class=\"simuInput\" style=\"width:115px;\">&nbsp; ";
        // line 379
        if ((isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null)) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null), 0, ".", "."), "html", null, true);
                echo "  ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Consumo </span> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp; ";
        // line 380
        if ((isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO  ";
            }
            echo " ";
        }
        echo "</div> <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; ";
        if ((isset($context["totalPagoMensual"]) ? $context["totalPagoMensual"] : null)) {
            echo " SI 
        ";
            // line 381
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalPagoMensual"]) ? $context["totalPagoMensual"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : null)) {
                echo " ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualCreditoApi"]) ? $context["pagoMensualCreditoApi"] : null), 0, ".", "."), "html", null, true);
                echo "  ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> </span> <div class=\"simuInput\" style=\"width:130px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Acciones </span> <div class=\"simuInput\" style=\"width:115px;\">&nbsp; ";
        // line 392
        if ((isset($context["totalAhorroInversionAcciones"]) ? $context["totalAhorroInversionAcciones"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionAcciones"]) ? $context["totalAhorroInversionAcciones"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        echo "</div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Tarjeta Crédito</span> <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp; ";
        // line 393
        if ((isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        echo " </div><div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp;  </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Boletas Hon. </span> <div class=\"simuInput\" style=\"width:140px;\">&nbsp;  </div></p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Ahorros </span><div class=\"simuInput\" style=\"width:118px;\">&nbsp; ";
        // line 405
        if ((isset($context["totalAhorroInversionCuentas"]) ? $context["totalAhorroInversionCuentas"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionCuentas"]) ? $context["totalAhorroInversionCuentas"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null)) {
                echo " SI ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null), 0, ".", "."), "html", null, true);
                echo " ";
            } else {
                echo " NO  ";
            }
        }
        echo " </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Linea sobregiro </span><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp; ";
        // line 406
        if ((isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : null), 0, ".", "."), "html", null, true);
            echo "  ";
        } else {
            echo "  ";
            if ((isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO   ";
            }
            echo " ";
        }
        echo "</div> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> 


";
        // line 409
        if ((isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : null)) {
            echo " SI  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        // line 410
        echo "         &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">  Pension/Jubil.</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otros </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; ";
        // line 421
        if ((isset($context["otrosTienes"]) ? $context["otrosTienes"] : null)) {
            echo " SI ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["otrosTienes"]) ? $context["otrosTienes"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " NO ";
        }
        echo "</div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otras Deudas <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div>  <div class=\"simuInput\" style=\"width:110px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Arriendos </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  </div> </p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total Act. </span><div class=\"simuInput\" style=\"width:105px;\">&nbsp;

         ";
        // line 436
        if ((isset($context["totalActivos"]) ? $context["totalActivos"] : null)) {
            echo "   ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalActivos"]) ? $context["totalActivos"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo " 


         </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Total Pas. </span> <div class=\"simuInput\" style=\"width:220px;\">&nbsp; ";
        // line 440
        if ((isset($context["totalDebes"]) ? $context["totalDebes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebes"]) ? $context["totalDebes"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "  </div>  </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total Ingresos</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp; ";
        // line 441
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "independientesueldoliquido", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "independientesueldoliquido", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo " </div> </p></td>

    </tr>


</table>






<table width=\"700\" height=\"10\">
    <tr>

        <td  style=\"background-color: #001d6e;\"><span style=\"color:#FFF;\">ANTECEDENTES DEL CONYUGE</span><input type=\"checkbox\"></td>







    </tr>


</table>




<table width=\"702\">
    <tr>
        <td width=\"489\"><span class=\"titulo33\">Nombre : </span> <div class=\"simuInput\" style=\"width:390px; \"> &nbsp    ";
        // line 474
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "nombre", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellido", array()), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellidoseg", array()), "html", null, true);
            echo "    ";
        }
        echo "   </div></td>

        <td width=\"201\"> <span class=\"titulo33\"> Rut:</span> <div class=\"simuInput\" style=\"width:141px; \"> &nbsp ";
        // line 476
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rut", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"214\"><span class=\"titulo33\"> Estado Civil:</span> <div class=\"simuInput\" style=\"width:110px; \">&nbsp 

";
        // line 488
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " Casado ";
        }
        // line 489
        echo "        </div> </td>
        <td width=\"272\"> <span class=\"titulo33\"> Fecha Nacimiento:</span> <div class=\"simuInput\" style=\"width:110px; \">&nbsp ";
        // line 490
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "fechanacimiento", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

        <td width=\"198\"><span class=\"titulo33\">  Edad:</span> <div class=\"simuInput\" style=\"width:130px; \"> &nbsp  ";
        // line 492
        echo twig_escape_filter($this->env, (isset($context["edadCon"]) ? $context["edadCon"] : null), "html", null, true);
        echo "  </div></td>
    </tr>


</table>


<table width=\"700\">

    <td><span class=\"titulo33\"> Domicilio: </span> <div class=\"simuInput\" style=\"width:602px; \"> 
";
        // line 502
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo "  
";
            // line 503
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array()), "html", null, true);
                echo " ";
            }
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array()), "html", null, true);
                echo " ";
            }
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array()), "html", null, true);
                echo " ";
            }
        }
        // line 504
        echo "
    &nbsp </div></td>
</table>


<table width=\"700\">
    <tr>
        <td width=\"400\" colspan=\"2\"><span class=\"titulo33\"> Vivienda: </span> <div class=\"simuInput\" style=\"width:302px; \"> &nbsp";
        // line 511
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo "   ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array()), "html", null, true);
                echo "  ";
            }
        }
        echo " </div></td>
        <td width=\"300\"><span class=\"titulo33\"> Años residencia : </span> <div class=\"simuInput\" style=\"width:150px; \"> &nbsp </div></td>

     
    </tr>


</table>


<table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\"> Arriendo / Dividendo: </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp ";
        // line 523
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "montoArriendo", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "montoArriendo", array()), "html", null, true);
                echo "  ";
            }
            echo "  ";
        }
        echo "  </div></td>
        <td width=\"224\"><span class=\"titulo33\"> Teléfono : </span> <div class=\"simuInput\" style=\"width:120px;\"> &nbsp  ";
        // line 524
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "telefono", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\">Profesión  </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp ";
        // line 535
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "profesion", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "profesion", array()), "html", null, true);
                echo "  ";
            }
            echo "  ";
        }
        echo "  </div></td>
        <td width=\"224\"><span class=\"titulo33\"> Universidad : </span> <div class=\"simuInput\" style=\"width:220px;\"> &nbsp  ";
        // line 536
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "universidad", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

    </tr>


</table>



<table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\">Sueldo  </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp ";
        // line 547
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array()), "html", null, true);
                echo "  ";
            }
            echo "  ";
        }
        echo "  </div></td>
        <td width=\"224\"><span class=\"titulo33\"> Cargo : </span> <div class=\"simuInput\" style=\"width:200px;\"> &nbsp  ";
        // line 548
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "cargoactual", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

    </tr>



    <table width=\"700\">
    <tr>
        <td width=\"264\"><span class=\"titulo33\">Empresa : </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp ";
        // line 556
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "empresa", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "empresa", array()), "html", null, true);
                echo "  ";
            }
            echo "  ";
        }
        echo "  </div></td>
        <td width=\"224\"><span class=\"titulo33\"> Antiguedad Laboral: </span> <div class=\"simuInput\" style=\"width:200px;\"> &nbsp  ";
        // line 557
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "antiguedadlaboral", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>

    </tr>


</table>




<hr style=\"color: #001d6e;
background-color: #001d6e;
height: 5px;\">







<span style=\"color:#001d6e;\">DATOS PROFESIONALES DEL CLIENTE</span>
<div style=\"clear:both;\"></div>


<table width=\"700\" style=\"\">
    <tr>
        <td width=\"161\" style=\"border: solid 1px  #001d6e;\" ><span style=\"font-size:10px; text-align:center;width:auto;position:relative;margin:auto;\"> ACTIVOS </span></td>
        <td width=\"145\"  style=\"border: solid 1px  #001d6e;\" ><span style=\"font-size:10px; text-align:center;\"> PASIVOS </span></td>
        <td width=\"378\"  style=\"border: solid 1px  #001d6e;\"><span style=\"font-size:10px;;\"> INGRESOS LIQUIDOS MENSUAL </span></td>
    </tr>
</table>

<div class=\"clear:both;\"></div>

<table width=\"700\" height=\"20\">

    <tr>
        <td width=\"55\"><p style=\"font-size:9px;\"> Tipo </p></td>
        <td width=\"102\"><p style=\"font-size:9px;\"> Valor Comercial </p></td>

        <td width=\"61\"><p style=\"font-size:9px;\"> Tipo deuda </p></td>
        <td width=\"81\"><p style=\"font-size:9px;\"> Deuda vigente </p></td>
        <td width=\"365\"><p style=\"font-size:9px;\"> Deuda vigente </p></td>
        <td width=\"8\"></td>
    </tr>


</table>




<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Bienes Raices</span> <div class=\"simuInput\" style=\"width:100px;\">&nbsp; </div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Hipotecario </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp;</div>  <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Sueldo Fijo</span> <div class=\"simuInput\" style=\"width:150px;\">&nbsp; </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Vehículos </span><div class=\"simuInput\" style=\"width:115px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Consumo </span> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp; </div> <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; </div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Sueldo Variable</span> <div class=\"simuInput\" style=\"width:130px;\">&nbsp; </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Acciones </span> <div class=\"simuInput\" style=\"width:115px;\">&nbsp; </div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Tarjeta Crédito</span> <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div><div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp;  </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Boletas Hon. </span> <div class=\"simuInput\" style=\"width:140px;\">&nbsp;  </div></p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Ahorros </span><div class=\"simuInput\" style=\"width:118px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Linea sobregiro </span><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">  Pension/Jubil.</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otros </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otras Deudas <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div>  <div class=\"simuInput\" style=\"width:110px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Arriendos </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  </div> </p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total Act. </span><div class=\"simuInput\" style=\"width:105px;\">&nbsp;  </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Total Pas. </span> <div class=\"simuInput\" style=\"width:220px;\">&nbsp;  </div>  </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total Ingresos</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp;  </div> </p></td>

    </tr>


</table>




<br>
<br>
<br>
<br>
<br>
<br>

<br>
<br>

<br>
<br>
<br>
<br>

<br>
<br>
<br>
<br>


<hr style=\"color: #001d6e;
background-color: #001d6e;
height: 5px;\">



<span style=\"color:#001d6e;margin-top: 800px; position: relative;width: auto;height: auto;\">
DATOS CRÉDITO HIPOTECARIO
</span>




<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Valor Propiedad</span> <div class=\"simuInput\" style=\"width:100px;\">&nbsp; ";
        // line 721
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "valorPropiedad", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "valorPropiedad", array()), "html", null, true);
                echo " UF ";
            }
        }
        echo "</div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Reserva </span> <div class=\"simuInput\" style=\"width:100px;\"> &nbsp;</div>  <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Estado </span> <div class=\"simuInput\" style=\"width:150px;\">&nbsp; ";
        // line 723
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "estado", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "estado", array()), "html", null, true);
                echo " &nbsp; ";
                if (($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "dfl2", array()) == "si")) {
                    echo "  DFL2  ";
                }
                echo " ";
            }
        }
        echo "</div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Estacionamiento </span><div class=\"simuInput\" style=\"width:70px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Pie  </span> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp; ";
        // line 734
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoPie", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoPie", array()), "html", null, true);
                echo " UF ";
            }
            echo " ";
        }
        echo "</div> <div class=\"simuInput\" style=\"width:100px;margin-left:10px;\">&nbsp; </div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> MONTO CRÉDITO </span> <div class=\"simuInput\" style=\"width:130px;\">&nbsp;";
        // line 735
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoCredito", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoCredito", array()), "html", null, true);
                echo " ";
            }
        }
        echo " </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Bodega</span> <div class=\"simuInput\" style=\"width:115px;\">&nbsp; </div></p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Ahorro </span> <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div><div class=\"simuInput\" style=\"width:100px;margin-left:5px;\"> &nbsp;  </div></p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">PLAZO (años) \t </span> <div class=\"simuInput\" style=\"width:140px;\">&nbsp;";
        // line 747
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "plazoCredito", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "plazoCredito", array()), "html", null, true);
                echo "  ";
            }
            echo " ";
        }
        echo " </div></p></td>

    </tr>


</table>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Descuento </span><div class=\"simuInput\" style=\"width:118px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Subsidio </span><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div> <div class=\"simuInput\" style=\"width:100px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">  % Financiamiento\t</span> <div class=\"simuInput\" style=\"width:138px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Total </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; </div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Otras Deudas <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;</div>  <div class=\"simuInput\" style=\"width:110px;margin-left:5px;\">  &nbsp;</div> </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Fecha de Entrega\t  </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  </div> </p></td>

    </tr>


</table>


<br/><br/><br/>
<span style=\"color:#001d6e;margin-top: 800px; position: relative;width: auto;height: auto;\">
DATOS CRÉDITO CONSUMO
</span>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Monto de Crédito: </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp;  ";
        // line 789
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "montoCredito", array()), 0, ".", "."), "html", null, true);
            echo "  ";
        }
        echo "</div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Plazo de Crédito <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp; ";
        // line 790
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "plazoCredito", array()), "html", null, true);
            echo "  ";
        }
        echo " </div>   </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Seguros Asociados  </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  ";
        // line 791
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "segurosAsociados", array()), "html", null, true);
            echo "  ";
        }
        echo " </div> </p></td>

    </tr>


    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Monto de Crédito: </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; ";
        // line 797
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "paraque", array()), "html", null, true);
            echo "  ";
        }
        echo " </div> </p></td>


    </tr>


</table>


<br/><br/><br/>


<span style=\"color:#001d6e;margin-top: 800px; position: relative;width: auto;height: auto;\">
DATOS CRÉDITO AUTOMOTRIZ
</span>



<table width=\"700\" style=\"margin-top:3%;\">
    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Monto del Auto </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp;  ";
        // line 817
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "montoCredito", array()), "html", null, true);
            echo "  ";
        }
        echo "</div> </p></td>
        <td width=\"283\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Plazo de Crédito <div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp; ";
        // line 818
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "plazoCredito", array()), "html", null, true);
            echo "  ";
        }
        echo " </div>   </p></td>
        <td width=\"226\"><p style=\"font-size:9px;\"><span class=\"titulo33\">Seguros Asociados  </span> <div class=\"simuInput\" style=\"width:158px;\">&nbsp;  ";
        // line 819
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "segurosAsociados", array()), "html", null, true);
            echo "  ";
        }
        echo " </div> </p></td>

    </tr>


    <tr>
        <td width=\"175\"><p style=\"font-size:9px;\"><span class=\"titulo33\"> Monto de Crédito: </span> <div class=\"simuInput\" style=\"width:125px;\">&nbsp; ";
        // line 825
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "paraque", array()), "html", null, true);
            echo "  ";
        }
        echo " </div> </p></td>


    </tr>


</table>


<br>
<br>
<br>
<br>
<br>
<br>
<br>


<table>
<thead>


<span style=\"color:#001d6e;margin-top: 800px; position: relative;width: auto;height: auto;\">
DATOS CRÉDITO CONSOLIDACION 
</span>
<tr>
    <th>Crédito </th>
    <th>Monto </th>
    <th>Plazo </th>

  
    </tr>



    </thead>

    <tr>
    ";
        // line 863
        $context["con"] = 0;
        // line 864
        echo "    ";
        if ((isset($context["creditoConsolidacion"]) ? $context["creditoConsolidacion"] : null)) {
            // line 865
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["creditoConsolidacion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["creditoConsolidacion"]) {
                // line 866
                echo "    ";
                $context["con"] = ((isset($context["con"]) ? $context["con"] : null) + 1);
                // line 867
                echo "
    ";
                // line 868
                if (((isset($context["con"]) ? $context["con"] : null) <= 1)) {
                    // line 869
                    echo "    
    <tr>   

        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 872
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 873
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 874
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo", array()), "html", null, true);
                    echo " años </div> </td>
     </tr>
        
       <tr>    

        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 879
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 880
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 881
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo2", array()), "html", null, true);
                    echo " años </div> </td>
</tr>
<tr>   


        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 886
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito3", array()), "html", null, true);
                    echo "  </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 887
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito3", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 888
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo3", array()), "html", null, true);
                    echo "  </div></td>
       
   </tr> 


   ";
                }
                // line 894
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsolidacion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 896
            echo "    ";
        }
        // line 897
        echo "    
        </tr>

</table></div>









</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:plantillas:bbva.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1563 => 897,  1560 => 896,  1553 => 894,  1544 => 888,  1540 => 887,  1536 => 886,  1528 => 881,  1524 => 880,  1520 => 879,  1512 => 874,  1508 => 873,  1504 => 872,  1499 => 869,  1497 => 868,  1494 => 867,  1491 => 866,  1486 => 865,  1483 => 864,  1481 => 863,  1436 => 825,  1423 => 819,  1415 => 818,  1407 => 817,  1380 => 797,  1367 => 791,  1359 => 790,  1351 => 789,  1299 => 747,  1277 => 735,  1267 => 734,  1243 => 723,  1231 => 721,  1060 => 557,  1048 => 556,  1033 => 548,  1021 => 547,  1003 => 536,  991 => 535,  973 => 524,  961 => 523,  939 => 511,  930 => 504,  911 => 503,  907 => 502,  894 => 492,  885 => 490,  882 => 489,  878 => 488,  859 => 476,  846 => 474,  803 => 441,  795 => 440,  784 => 436,  760 => 421,  747 => 410,  739 => 409,  721 => 406,  704 => 405,  683 => 393,  673 => 392,  647 => 381,  629 => 380,  611 => 379,  594 => 369,  562 => 368,  544 => 367,  491 => 322,  481 => 315,  457 => 298,  446 => 290,  428 => 279,  420 => 278,  400 => 265,  392 => 264,  388 => 262,  386 => 261,  361 => 242,  353 => 241,  337 => 232,  327 => 229,  302 => 223,  290 => 214,  282 => 212,  275 => 211,  258 => 201,  238 => 199,  201 => 172,  191 => 171,  19 => 1,);
    }
}
