<?php

/* BenchUploadBundle:Default:upload8.html.twig */
class __TwigTemplate_64b62ded76b8b8ee9979cfdebe3f31781a3e6cb229470114f101f6ec572f88f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "

<form  method=\"post\" action=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("declaracionAnual");
        echo "\">

    <div id=\"uploader11\" style=\"width:900px;height:200px;margin-top: 200px;position: relative;\">
        <p></p>
    </div>
</form>
<script type=\"text/javascript\">


    \$(document).ready(function() {



        \$(function() {
            \$(\"#uploader11\").plupload({
              
                runtimes: 'html5,flash',
                url: 'declaracionAnual',
                max_file_size: '1000mb',
                max_file_count: 20, // user can add no more then 20 files at a time
                chunk_size: '1mb',
                autostart : true,
                rename: true,
                multiple_queues: true,
                // Resize images on clientside if we can
              //  resize: {width: 320, height: 240, quality: 90},
                // Rename files by clicking on their titles
                rename: true,
                        // Sort files
                        sortable: true,
                // Specify what files to browse for
                filters: [
                    {title: \"Image files\", extensions: \"jpg,gif,png\"},
                    {title: \"Zip files\", extensions: \"zip,avi,pdf,doc,pdf\"}
                ],
                // Flash settings
                flash_swf_url: \"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.flash.swf"), "html", null, true);
        echo "\",
                // Silverlight settings
                silverlight_xap_url: \"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.silverlight.xap"), "html", null, true);
        echo "\",
            });

            // Client side form validation
            \$('form').submit(function(e) {
                var uploader = \$('#uploader').plupload('getUploader');

                // Files in queue upload them first
                if (uploader.files.length > 0) {
                    // When all files are uploaded submit form
                    uploader.bind('StateChanged', function() {
                        if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                            \$('form')[0].submit();
                        }
                    });

                    uploader.start();
                } else
                    alert('You must at least upload one file.');

                return false;
            });


        });
    });
    </script>

";
    }

    public function getTemplateName()
    {
        return "BenchUploadBundle:Default:upload8.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 43,  62 => 41,  23 => 5,  19 => 3,);
    }
}
