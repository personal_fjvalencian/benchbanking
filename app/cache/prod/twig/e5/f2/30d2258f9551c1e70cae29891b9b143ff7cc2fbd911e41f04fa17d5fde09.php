<?php

/* AliDatatableBundle:Renderers:_default.html.twig */
class __TwigTemplate_e5f230d2258f9551c1e70cae29891b9b143ff7cc2fbd911e41f04fa17d5fde09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, (isset($context["dt_item"]) ? $context["dt_item"] : null), "html", null, true);
    }

    public function getTemplateName()
    {
        return "AliDatatableBundle:Renderers:_default.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
