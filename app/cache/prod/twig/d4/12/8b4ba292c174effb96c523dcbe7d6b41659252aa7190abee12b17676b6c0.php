<?php

/* BenchCreditosBundle:Default:CreditoHipotecario.html.twig.twig */
class __TwigTemplate_d4128b4ba292c174effb96c523dcbe7d6b41659252aa7190abee12b17676b6c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchcreditos/js/CreditoHipotecario.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchcreditos/js/calcularHipo.js"), "html", null, true);
        echo "\"></script>


<form class=\"form-box-qn\" style=\"padding:0 0 5% 10%;\" id=\"formCreditoHipotecario\" action=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("guardahipotecario");
        echo "\" method=\"POST\" data-validate=\"parsley\" autocomplete=\"off\">
     

<input type=\"hidden\" id=\"token\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">       

<div class=\"col1\" id=\"calculo\">



    
    
    
    
<label class=\"label-text\" for=\"rut\">Valor Propiedad:</label>
    ";
        // line 20
        if (($this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "tipo", array()) == "Crédito Hipotecario")) {
            // line 21
            echo "\t\t<input type=\"text\" name=\"valor\" placeholder=\"UF\"  value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "montoCredito", array()), "html", null, true);
            echo "\"class=\"input-box\" id=\"valor99\" title=\"Debes ingresar el valor de la propiedad en Unidad de Fomento\"   onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  />

    ";
        } else {
            // line 24
            echo "
       <input type=\"text\" name=\"valor\" placeholder=\"UF\" class=\"input-box\" id=\"valor99\" title=\"Debes ingresar el valor de la propiedad en Unidad de Fomento\"   onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  />



   ";
        }
        // line 30
        echo "

    <input type=\"hidden\" value=\"\" name=\"idUCreditoHipotecario\" >

  
        
<label class=\"label-text\" for=\"rut\">Monto de Píe:</label>

    ";
        // line 38
        if (($this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "tipo", array()) == "Crédito Hipotecario")) {
            // line 39
            echo "
\t\t<input type=\"text\" name=\"pie\" placeholder=\"UF\" value=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "pie", array()), "html", null, true);
            echo "\"class=\"input-box\" id=\"pie99\" title=\"Debes ingresar el valor de la propiedad en Unidad de Formento, generalmente es un 10% o 20%\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" />


    ";
        } else {
            // line 44
            echo "



        <input type=\"text\" name=\"pie\" placeholder=\"UF\" class=\"input-box\" id=\"pie99\" title=\"Debes ingresar el valor de la propiedad en Unidad de Formento, generalmente es un 10% o 20%\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" />



    ";
        }
        // line 53
        echo "

<label class=\"label-text\" for=\"rut\">Porcentaje:</label>

    ";
        // line 57
        if (($this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "tipo", array()) == "Crédito Hipotecario")) {
            // line 58
            echo "
\t\t<input type=\"text\" name=\"porcentaje\" value=\"";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "pieP", array()), "html", null, true);
            echo "\" placeholder=\"%\" class=\"input-box\" id=\"porcentaje99\" />

    ";
        } else {
            // line 62
            echo "
        <input type=\"text\" name=\"porcentaje\" placeholder=\"%\" class=\"input-box\" id=\"porcentaje99\" />

    ";
        }
        // line 66
        echo "  

  <label class=\"label-text\" for=\"rut\">Monto de Crédito:</label>



    ";
        // line 72
        if (($this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "tipo", array()) == "Crédito Hipotecario")) {
            // line 73
            echo "
  <input type=\"text\" name=\"monto\" value=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "montoPropiedadSin", array()), "html", null, true);
            echo "\" placeholder=\"\$\" title=\"Ingresa monto en pesos sin puntos ni comas\" class=\"input-box\" id=\"total99\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  />
    
   ";
        } else {
            // line 77
            echo "
    <input type=\"text\" name=\"monto\" placeholder=\"\$\" title=\"Ingresa monto en pesos sin puntos ni comas\" class=\"input-box\" id=\"total99\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  />


    ";
        }
        // line 82
        echo "        
        </div>


        

        
        
        
        <div class=\"col1\">
        
       

    <label class=\"label-text\" for=\"nombres\">Plazo de Cr&eacute;dito</label>
<select class=\"data\" select=\"selected\" name=\"plazo\" id=\"plazoCreditoH\">
    ";
        // line 97
        if (($this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "tipo", array()) == "Crédito Hipotecario")) {
            // line 98
            echo "
        <option value=\"";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "plazo", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "plazo", array()), "html", null, true);
            echo " años</option>

    ";
        } else {
            // line 102
            echo "


    ";
        }
        // line 106
        echo "
<option value=\"\">Opción</option>
<option value=\"6\">6 años</option>
<option value=\"7\">7 años</option>
<option value=\"8\">8 años</option>
<option value=\"9\">9 años</option>
<option value=\"10\">10 años</option>
<option value=\"11\">11 años</option>
<option value=\"12\">12 años</option>
<option value=\"13\">13 años</option>
<option value=\"14\">14 años</option>
<option value=\"15\">15 años</option>
<option value=\"16\">16 años</option>
<option value=\"17\">17 años</option>
<option value=\"18\">18 años</option>
<option value=\"19\">19 años</option>
<option value=\"20\">20 años</option>
<option value=\"21\">21 años</option>
<option value=\"22\">22 años</option>
<option value=\"23\">23 años</option>
<option value=\"24\">24 años</option>
<option value=\"25\">25 años</option>
<option value=\"26\">26 años</option>
<option value=\"27\">27 años</option>
<option value=\"28\">28 años</option>
<option value=\"29\">29 años</option>
<option value=\"30\">30 años</option>

</select>

    <label class=\"label-text\" for=\"nombres\">Tipo de Propiedad</label>
               <select class=\"data\" select=\"selected\"  id=\"tipoPropiedad\"name=\"tipopropiedad\">
<option value=\"\">Opción</option>
<option value=\"Casa\">Casa</option>
<option value=\"Departamento\">Departamento</option>
<option value=\"Terreno\">Terreno</option>
<option value=\"Otro\">Otro</option>
</select>

  <label class=\"label-text\" for=\"nombres\">Estado</label>
<select class=\"data\" select=\"selected\" name=\"estado\" id=\"estadoPropiedad\">
<option value=\"\">Opción</option>
<option value\"Usada\">Usada</option>
<option value=\"Nueva\">Nueva</option>

</select>


        
              <label class=\"label-text2\" for=\"rut\">¿Tu propiedad es DFL2 y nueva?</label>
              
              <p class=\"rad\">
              <input type=\"radio\" id=\"radio1\" name=\"dfl2\" value=\"si\">
              <label for=\"radio1\" >Si</label>
              <input type=\"radio\" id=\"radio2\" name=\"dfl2\"value=\"no\" checked>
              <label for=\"radio2\"  >No</label>
              </p>
              
               <!--<p class=\"rad\">Si<input type=\"radio\"  id=\"radio1\" value=\"si\" name=\"dfl2\">
\t\t\t   No<input type=\"radio\" value=\"no\" name=\"dfl2\"></p>--> 
               
               </div>   
    
    
    <div class=\"col1\" style=\"margin-top: -2px;\">




     <label class=\"label-text2\" for=\"institucion\">Cuándo piensas comprar </label>
               <select class=\"data\" select=\"selected\" name=\"tiempo\" id=\"CuandoHipotecario\">
<option value=\"\">Opción</option>
<option value=\"En 30 días más\">En 30 días más</option>
<option value=\"En 60 días más\">En 60 días más</option>
<option value=\"En 90 días más\">En 90 días más</option>
<option value=\"En más de 90 días\">En más de 90 días</option>
</select>   




        
        <label class=\"label-text\" for=\"nombres\">Comuna donde desea comprar </label>
       
        <input type=\"text\" name=\"comunaDondeComprar\" class=\"input-box\" id=\"comunaDondeComprar\" data-validation-engine=\"validate[required]\" data-required=\"true\"   />
        
        
        <label class=\"label-text\" for=\"nombres\">Proyecto / Inmobiliaria </label>
        
        
        <input type=\"text\" name=\"proyectoInmobiliaria\"  class=\"input-box\" id=\"proyectoInmobiliaria\"    />
        
        
    



        
    
    
    </div>
        
    <div class=\"col-vacia\"></div> 
    <div class=\"clearfix\"></div>                              
<div class=\"col1\">
      <button style=\"float:left;\" id=\"BtnCreditoHipotecario\" class=\"classname\" value=\"Agregar\" type=\"submit\"  >GUARDAR Y SIGUIENTE</button></div>     
            </form>     

            <section id=\"RespuestaCreditoHipotecario\">
              


            </section>  ";
    }

    public function getTemplateName()
    {
        return "BenchCreditosBundle:Default:CreditoHipotecario.html.twig.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 106,  179 => 102,  171 => 99,  168 => 98,  166 => 97,  149 => 82,  142 => 77,  136 => 74,  133 => 73,  131 => 72,  123 => 66,  117 => 62,  111 => 59,  108 => 58,  106 => 57,  100 => 53,  89 => 44,  82 => 40,  79 => 39,  77 => 38,  67 => 30,  59 => 24,  52 => 21,  50 => 20,  36 => 9,  30 => 6,  24 => 3,  19 => 2,);
    }
}
