<?php

/* BenchDebesBundle:Default:debesTarjetaCredito.html.twig */
class __TwigTemplate_1afcf64f52e573998b80500da4b8446662dc547fe19e3ea63bd294cc83294084 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "

<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchdebes/js/debestarjetacredito.js"), "html", null, true);
        echo "\"></script>


<div class=\"fila\">

    <!--<h1 class=\"doc\"><span class=\"orange\">Línea de Crédito</span></h1>-->
    <div class=\"col1\">


        <form  id=\"DebesLineCreditosino\" action=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("debesTarjetaCreditoSiNo");
        echo "\"  method=\"POST\"  autocomplete=\"off\" >
          

<input type=\"hidden\" id=\"token\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">

            <label class=\"label-text2\" for=\"rut\">¿Tienes algúna Tarjeta de Crédito ?</label>
            <input type=\"hidden\" value=\"<?php echo \$idUser3;?>\" name=\"debesLineaCredito\" id=\"debesLineaCredito\">

      ";
        // line 21
        if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "debesTarjetaCreditoSiNo", array()) == "SI")) {
            // line 22
            echo "
  <p class=\"rad\">Si<input type=\"radio\" value=\"SI\"  class=\"debesLcreditoRadio\" name=\"debesLcreditoRadio\" id=\"debesLcreditoRadio\"  checked>
                No<input type=\"radio\" value=\"NO\" name=\"debesLcreditoRadio\"  id=\"debesLcreditoRadio\" class=\"debesLcreditoRadio\"></p> 

          
 ";
        } else {
            // line 28
            echo "
   
  <p class=\"rad\">Si<input type=\"radio\" value=\"SI\"  class=\"debesLcreditoRadio\" name=\"debesLcreditoRadio\" id=\"debesLcreditoRadio\" >
                No<input type=\"radio\" value=\"NO\" name=\"debesLcreditoRadio\" id=\"debesLcreditoRadio\" class=\"debesLcreditoRadio\" checked></p> 


           ";
        }
        // line 35
        echo "




          
            <button style=\"float:left;\" class=\"classname\" value=\"Agregar\" id=\"guardaLineaC1\" type=\"submit\">GUARDAR Y SIGUIENTE</button> 

        </form>


      




        <form class=\"form-personal\"  id=\"debesTarjeta\" action=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("tarjetacreditoguarda");
        echo "\">

            <label class=\"label-text2\" for=\"institucion\">Tipo de Tarjeta </label>
            <input type=\"text\" class=\"input-box2\" autofocus name=\"tipo\"  id=\"debeslctipo\" data-validation-engine=\"validate[required]\" data-required=\"true\">
            <input type=\"hidden\" value=\"<?php echo \$idUser3;?>\" name=\"id\" id=\"debeslcID\" >



            <label class=\"label-text2\" for=\"institucion\">Institucion</label>
            <input type=\"text\"class=\"input-box2\" name=\"institucion\" id=\"debeslcinstitucion\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
          
            <label class=\"label-text2\" for=\"institucion\">Monto Aprobado</label>
            <input type=\"text\" class=\"input-box2\" name=\"montoAprobado\" id=\"debeslcmontoAprobado\" placeholder=\"\$\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
    
          
      </form>
            </div>
            <div class=\"col1\">
       

        <br><br><br><br><br>

        

        <form id=\"debesTarjetaCredito2\">
    
            <label class=\"label-text2\" for=\"rut\">Monto Utilizado</label>
            <input type=\"text\" class=\"input-box2\" id=\"debeslcmontoUtilizado\" name=\"debeslcmontoUtilizado\" placeholder=\"\$\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\"  data-validation-engine=\"validate[required]\" data-required=\"true\">
 
        </form>

    </div>

    <div class=\"clearfix\"></div>
    <div class=\"col2\">
        <button style=\"float:left;margin-top:10px;\" id=\"guardaLineaC\" class=\"classname\" value=\"Agregar\" type=\"submit\">AGREGAR</button>
        <div id=\"SiguienteDebesLineaCredito\" class=\"classname\" style=\"margin-top:10px; margin-left: 1%; position: relative;width: 80px; float: left;\" >SIGUIENTE</div>
    </div> 

    <div class=\"flix-clear\"></div>

    <div class=\"clearfix\"></div>
    <br>

    <div class=\"col2\">

        <table class=\"table table-striped\" id=\"respuesta2\">
            <thead style=\"background:#F60\">
                <tr>
                    <td><p style=\"text-align:center; color:#FFF;\">Tipo de Tarjeta</p></td>
                    <td><p style=\"text-align:center;color:#FFF;\">Institución</p></td>
                    <td><p style=\"text-align:center;color:#FFF;\">Monto Aprobado</p></td>
                    <td><p style=\"text-align:center;color:#FFF;\">Monto Utilizado</p></td>

                    <td><p style=\"text-align:center;color:#FFF;\">Eliminar</p></td>
                </tr>
            </thead>


";
        // line 110
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["debesTarjetaCredito"]);
        foreach ($context['_seq'] as $context["_key"] => $context["debesTarjetaCredito"]) {
            echo "  


            <tbody style=\"background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;\">



                <tr  id=\"itemTarjeta_";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "id", array()), "html", null, true);
            echo "\">

                    <td><p style=\"text-align:center;color:#666;\">";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "tipotarjeta", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 120
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "institucion", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoaprobado", array()), "html", null, true);
            echo "</p></td>
                    <td><p style=\"text-align:center;color:#666;\">";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoutilizado", array()), "html", null, true);
            echo "</p></td>

                    <td><p style=\"text-align:center;color:#666;\"> <a href=\"#\" class=\"del_button\" id=\"del-";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "id", array()), "html", null, true);
            echo "\">eliminar</a> </p> </td>

                </tr>



            </tbody>

         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesTarjetaCredito'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 133
        echo "



        </table>












    </div>


<form id=\"debestarjetacreditoform\" action=\"";
        // line 153
        echo $this->env->getExtension('routing')->getPath("deldebesTarjeta");
        echo "\" ></form>

</div>


";
    }

    public function getTemplateName()
    {
        return "BenchDebesBundle:Default:debesTarjetaCredito.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 153,  197 => 133,  182 => 124,  177 => 122,  173 => 121,  169 => 120,  165 => 119,  160 => 117,  148 => 110,  86 => 51,  68 => 35,  59 => 28,  51 => 22,  49 => 21,  41 => 16,  35 => 13,  23 => 4,  19 => 2,);
    }
}
