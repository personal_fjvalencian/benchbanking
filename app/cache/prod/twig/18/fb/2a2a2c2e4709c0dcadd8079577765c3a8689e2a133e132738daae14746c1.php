<?php

/* BenchPaginasBundle:Default:inmobiliarias.html.twig */
class __TwigTemplate_18fb2a2a2c2e4709c0dcadd8079577765c3a8689e2a133e132738daae14746c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<!doctype html>
<html>
<head>
<meta charset=\"utf-8\">
<title>Remax</title>

<style>

header{
    width: 626px;
    height: 160px;
    margin: 0 auto;
}

.container-js{
    width: 1000px;
    height: 1200px;
    margin: 0 auto;
    position: relative;
    
}

</style>

</head>

<body>
    
    <header>
        <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/header.png"), "html", null, true);
        echo "\"/>
    </header>
    
    <div class=\"container-js\">

        
        
        <script type=\"text/javascript\" src=\"https://www.formstack.com/forms/js.php?1666426-WhGckOFoZp-v3\"></script><noscript><a href=\"https://www.formstack.com/forms/?1666426-WhGckOFoZp\" title=\"Online Form\">Online Form - Formulario INMOBILIARIA - FULL</a></noscript>
        
    </div>

</body>
</html>


";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:inmobiliarias.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 34,  19 => 4,);
    }
}
