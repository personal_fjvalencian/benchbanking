<?php

/* BenchPaginasBundle:Default:formulario.html.twig */
class __TwigTemplate_6cdf7add0f5629963605d8da73d60980a39eefb7a10f948335556b58b331cc12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'containerheadertab' => array($this, 'block_containerheadertab'),
            'containerCreditoConsumo' => array($this, 'block_containerCreditoConsumo'),
            'containerCreditoAutomotriz' => array($this, 'block_containerCreditoAutomotriz'),
            'containerCreditoHipotecario' => array($this, 'block_containerCreditoHipotecario'),
            'containerCreditoConsolidacion' => array($this, 'block_containerCreditoConsolidacion'),
            'containerCreditoC' => array($this, 'block_containerCreditoC'),
            'containerDatosPersonales' => array($this, 'block_containerDatosPersonales'),
            'containerDatosLaborales' => array($this, 'block_containerDatosLaborales'),
            'containerConyuge' => array($this, 'block_containerConyuge'),
            'tienesBienesRaices' => array($this, 'block_tienesBienesRaices'),
            'containerAhorroinversion' => array($this, 'block_containerAhorroinversion'),
            'containerAutomoviles' => array($this, 'block_containerAutomoviles'),
            'container' => array($this, 'block_container'),
            'containerdebes' => array($this, 'block_containerdebes'),
            'tarjetaCreditodebes' => array($this, 'block_tarjetaCreditodebes'),
            'lineaCreditodebes' => array($this, 'block_lineaCreditodebes'),
            'lineaCreditoConsumo' => array($this, 'block_lineaCreditoConsumo'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">

<html xmlns=\"http://www.w3.org/1999/xhtml\">

    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">

        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=9\" />

        <META Http-Equiv=\"Cache-Control\" Content=\"no-cache\">
        <META Http-Equiv=\"Pragma\" Content=\"no-cache\">


        <head>
            <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
                <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
                    <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/form.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
                        <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/ship.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
                            <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
                                <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/basic.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
                                    <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/puntos.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/mask.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/mascara.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/mask.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/tipoSueldoUpload.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/tienesAp.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/panelControl.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/parsley.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/parsley.extend.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciador2.js"), "html", null, true);
        echo "\"></script>  
                                    <script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/validaciontablokeo.js"), "html", null, true);
        echo "\"></script>  
                                    <script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/validacionDesblokeo.js"), "html", null, true);
        echo "\"></script> 
                                    <script src=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/validacionDesblokeo.js"), "html", null, true);
        echo "\"></script> 
                                    <script src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/comboRegion.js"), "html", null, true);
        echo "\"></script> 
                                    <script src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchtienes/js/tienesSiNo.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchtienes/js/tienesSiNoGuarda.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchdebes/js/debesSiNoGuarda.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchsolicitudescreditos/js/guarda.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchsolicitudescreditos/js/enviar.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/botoneshead.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/autoscroll.js"), "html", null, true);
        echo "\"></script>
                                    <script src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/html5.js"), "html", null, true);
        echo "\"></script>

                                    <link rel=\"stylesheet\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/jqueryui.css"), "html", null, true);
        echo "\" type=\"text/css\" />


                                    <link rel=\"stylesheet\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css"), "html", null, true);
        echo "\" type=\"text/css\" />




                                    <script type=\"text/javascript\" src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jqueryui.js.js"), "html", null, true);
        echo "\"></script>
                                    <script type=\"text/javascript\" src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jqueryui.js.js"), "html", null, true);
        echo "\"></script>
                                   

                                   <script type=\"text/javascript\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/CreditosBotones.js"), "html", null, true);
        echo "\"></script>


                       
                                   
                                  
                                    
                                    <script type=\"text/javascript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciadorTooltip.js"), "html", null, true);
        echo "\"></script>
                                     
                                  
                                    
     
                                        
<link rel=\"shortcut icon\" href=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\"/>

<META name=\"keywords\" content=\"credito hipotecario, credito consumo, simulador, tasa, banco\">
<title> BenchBanking │ Créditos Convenientes </title>
<META name=\"description\" content=\"Créditos más convenientes y mejor informados, compara las tasas antes de contratarlo\">  
<link rel=\"shortcut icon\" href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
<link rel=\"icon\" href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">






        </head>





    <body>


</ul>




    <!-- Google Tag Manager -->
    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-T8RRB4\"
                      height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T8RRB4');</script>
    <!-- End Google Tag Manager -->


<script>




</script>

 <div class=\"spinner\">
      <div class=\"bounce1\"></div>
      <div class=\"bounce2\"></div>
      <div class=\"bounce3\"></div>
    </div>

    

 ";
        // line 124
        $this->env->loadTemplate("BenchPaginasBundle:Default:headertab.html.twig")->display($context);
        // line 125
        echo "




              ";
        // line 130
        $this->displayBlock('containerheadertab', $context, $blocks);
        // line 136
        echo " 
                                            
                                            <input type=\"hidden\" value=\"";
        // line 138
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\" id=\"token\" >



                                            <div id=\"div_carga\">
                                                <img id=\"cargador\" src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/loader2.gif"), "html", null, true);
        echo "\"/>
                                            </div>
                                            
                                            <div class=\"wrapper\">
                                              
                                                
<!-- DASHBOARD -->                                                               
    <div class=\"container-dashboard\" style=\"margin-top:4%\">
        <div class=\"lengueta_title\"><img src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/home_icon.png"), "html", null, true);
        echo "\"/>Panel de Control</div>
        <div id=\"btn-tabs-dashboard01\">
            <a href=\"#\" id=\"solicitarCredito\">Solicitar un<br>nuevo Cr&eacute;dito</a> 
        </div>
        <div id=\"btn-tabs-dashboard02\">
            <a href=\"#\" id=\"informacionCliente\">Mi<br>Informaci&oacute;n</a> 
\t</div>
        <div id=\"btn-tabs-dashboard03\">
            <a href=\"#\" id=\"miscotizacionesButton\" >Mis<br>Cotizaciones</a> 
\t</div>
        <div id=\"btn-tabs-dashboard04\">
            <a href=\"#\"  id=\"simuladorButton\">Simulador<br>Autom&aacute;tico</a> 
\t</div>
    </div>





<!-- FIN DASHBOARD --> 

                                                <input type=\"hidden\" value=\"<?php echo \$totaLBienes ; ?>\" id=\"totaLBienes\">

                                                    <input type=\"hidden\" value=\"<?php echo \$idUser3;?>\" id=\"Iduser3\">
                                                        <input type=\"hidden\" value=\"SI\" id=\"SIform\">
                                                            <div class=\"alert\" style=\"width:400px; margin-left:50em\" id=\"alertpedidos\">



                                                            </div>

                                                            <div id=\"content\">




                                                                <ul id=\"tab1\" class=\"nav nav-tabs\" data-tabs=\"tabs\" style=\"margin-top: 5%;\">
                                                                    <!--<div class=\"lengueta_title\"><img src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/home_icon.png"), "html", null, true);
        echo "\"/>Panel de Control</div>-->
                                                                    
                                                                    <li class=\"active\"><a href=\"#quenecesitas\" data-toggle=\"tab\" ><strong>1.</strong> Qué Necesitas</a></li>
                                                                    <li><a href=\"#01\"  id=\"tusDatos\"    data-toggle=\"tab\" ><strong>2.</strong> Tus Datos</a></li>
                                                                    <li><a href=\"#yellow\" id=\"linkquetienes\"  data-toggle=\"tab\"><strong>3.</strong> Qué Tienes?</a></li>
                                                                    <li><a href=\"#green\" id=\"linkquedebes\" data-toggle=\"tab\"><strong>4.</strong> Qué debes?</a></li>
                                                                    <li id=\"cot3\"><a href=\"#blue\" id=\"linkdondecotizar\" data-toggle=\"tab\" id=\"con6\"><strong>5.</strong>Donde Cotizar</a></li>
                                                                    <li id=\"cot6\"><a href=\"#misCotizaciones\" data-toggle=\"tab\"><strong></strong>Mis Cotizaciones</a></li>
                                                                     
                                                                    <li id=\"cot7\" style=\"display:none;\"><a href=\"#simulador\" data-toggle=\"tab\"><strong></strong>Mis Cotizaciones</a></li>
                                                                </ul>
                                                                
                                                                
 





                                                                <div class=\"tab-content\">
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    <div class=\"tab-pane active\" id=\"quenecesitas\">
                                                                        
                                                                        <div id=\"flip3\" class=\"flip\">
                                                                            <img id=\"imgCreditoHipotecario\" src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_hi.png"), "html", null, true);
        echo "\">
                                                                                
                                                                           <input type=\"hidden\" id=\"estadoCreditoHipotecario\" value=\"desactivo\">
                                                                               
                                                                             
                                                                               
                                                                            <h3 style=\"display:block; margin-top:10px;\">Cr&eacute;dito Hipotecario</h3></div>
                                                                        <div class=\"line-flip\"></div>

                                                                        <div id=\"flip1\" class=\"flip\">
                                                                            <input type=\"hidden\" id=\"estadoCreditoConsumo\"  value=\"desactivo\"></input>
                                                                            
                                                                            <img id=\"imgCreditoConsumo\"src=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_cc.png"), "html", null, true);
        echo "\">
                                                                                <h3 style=\"display:block; margin-top:10px;\">Cr&eacute;dito de Consumo</h3></div>
                                                                        <div class=\"line-flip\"></div>

                                                                        <div id=\"flip2\" class=\"flip\">
                                                                        
                                                                            <img  id=\"imgCreditoAutomotriz\"src=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_au.png"), "html", null, true);
        echo "\">
                                                                           
                                                                               <input type=\"hidden\" id=\"estadoAutomotriz\" value=\"desactivo\"></input>
                                                                                   
                                                                            
                                                                           <h3 style=\"display:block; margin-top:10px;\">Cr&eacute;dito Automotriz</h3></div>
                                                                        <div class=\"line-flip\"></div>

                                                                        <div id=\"flip4\" class=\"flip\">
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            <img id=\"imgConsolidacion\" src=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_cdd.png"), "html", null, true);
        echo "\">
                                                                                
                                                                            <input type=\"hidden\" id=\"estadoCon\" value=\"desactivo\"></input>
                                                                           
                                                                             
                                                                           <h3 style=\"display:block; margin-top:10px;\">Consolidaci&oacute;n de deuda / Refinanciamiento</h3></div>
                                                                        <div class=\"line-flip\"></div>

                                                                        <div id=\"flip5\" class=\"flip\">
                                                                            <img  id=\"imgCuentaCorriente\" src=\"";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/icon_ccu.png"), "html", null, true);
        echo "\">
                                                                              
                                                                                
                                                                            <h3 style=\"display:block; margin-top:10px;\">Cuenta Corriente</h3></div>
                                                                            
                                                                                  <input type=\"hidden\"  id=\"estadoCuentaCorriente\" value=\"desactivo\"></input>


                                                                        <!-- Credito consumo --> 

                                                                        <section id=\"panel1\" class=\"panel\">

                                                                            <!--<div class=\"alert-form2\" id=\"respuestaconsumo2\"><p>Acá va el mensaje oculto de respuesta</p></div>-->  

          ";
        // line 272
        $this->env->loadTemplate("BenchCreditosBundle:Default:CreditoConsumo.html.twig")->display($context);
        // line 273
        echo "





              ";
        // line 279
        $this->displayBlock('containerCreditoConsumo', $context, $blocks);
        // line 285
        echo " 






                                                                        </section> 

                                                                        <!-- Credito consumo --> 




                                                                        <!-- Credito Automotriz -->

                                                                        <section id=\"panel2\" class=\"panel\">



                ";
        // line 305
        $this->env->loadTemplate("BenchCreditosBundle:Default:CreditoAutomotriz.html.twig")->display($context);
        // line 306
        echo "







              ";
        // line 314
        $this->displayBlock('containerCreditoAutomotriz', $context, $blocks);
        // line 320
        echo " 




                                                                        </section>


                                                                        <!-- fin  Credito Automotriz -->


                                                                        <section id=\"panel3\" class=\"panel\">




              ";
        // line 336
        $this->env->loadTemplate("BenchCreditosBundle:Default:CreditoHipotecario.html.twig.twig")->display($context);
        // line 337
        echo "





              ";
        // line 343
        $this->displayBlock('containerCreditoHipotecario', $context, $blocks);
        // line 349
        echo " 



                                                                        </section>


                                                                        <section id=\"panel4\" class=\"panel\">





                ";
        // line 362
        $this->env->loadTemplate("BenchCreditosBundle:Default:CreditoConsolidacion.html.twig")->display($context);
        // line 363
        echo "





              ";
        // line 369
        $this->displayBlock('containerCreditoConsolidacion', $context, $blocks);
        // line 375
        echo " 






                                                                        </section>




                                                                        <section id=\"panel5\" class=\"panel\">




                ";
        // line 392
        $this->env->loadTemplate("BenchCreditosBundle:Default:CreditoCuentaC.html.twig")->display($context);
        // line 393
        echo "





              ";
        // line 399
        $this->displayBlock('containerCreditoC', $context, $blocks);
        // line 405
        echo " 






                                                                        </section>





                                                                    </div>
                                                                        

                                                                        
                                                                        
                                                                        
                                                                        
                                                                        

                                                                    <div class=\"tab-pane\" id=\"01\"  >
                                                                        <ul id=\"tabpersonal\" class=\"nav nav-tabs2\" data-tabs=\"tabs\">
                                                                            <li class=\"active\"><a href=\"#dp01\" id=\"tabDatosPersonales\" data-toggle=\"tab\">Datos Personales</a></li>
                                                                            <li><a href=\"#dp02\" data-toggle=\"tab\" id=\"laborallink\">Datos Laborales</a></li>
                                                                            <li id=\"con99\"><a href=\"#dp03\" data-toggle=\"tab\" id=\"linkConyuge\">Datos Conyuge</a></li>
                                                                            <li><a href=\"#dp04\" data-toggle=\"tab\" id=\"linkAdjunta\">Adjunta Documentos</a></li>
                                                                        </ul>

                                                                        <div class=\"tab-content\">


                                                                            <section class=\"tab-pane active\" id=\"dp01\">  <!----------------------- inicio Datos personales ---------------------------------->





 ";
        // line 444
        $this->env->loadTemplate("BenchUsuariosBundle:Default:datospersonales.html.twig")->display($context);
        // line 445
        echo "




              ";
        // line 450
        $this->displayBlock('containerDatosPersonales', $context, $blocks);
        // line 456
        echo " 

                                                                            </section> <!-------------------------------- fin Datos personales ------------------------------------------------------>



                                                                            <section class=\"tab-pane\" id=\"dp02\">  <!--------------------------- inicio datos laborales ---------------------------------->






 ";
        // line 469
        $this->env->loadTemplate("BenchUsuariosBundle:Default:antecedentelaborales.html.twig")->display($context);
        // line 470
        echo "

";
        // line 472
        $this->displayBlock('containerDatosLaborales', $context, $blocks);
        // line 478
        echo " 




                                                                            </section><!------------------ fin  datos laborales ------------------------------------------------------------------------>




                                                                            <section class=\"tab-pane\" id=\"dp03\">

";
        // line 490
        $this->env->loadTemplate("BenchUsuariosBundle:Default:conyuge.html.twig")->display($context);
        // line 491
        echo "

";
        // line 493
        $this->displayBlock('containerConyuge', $context, $blocks);
        // line 499
        echo " 







                                                                            </section>



                                                                            <section class=\"tab-pane\" id=\"dp04\" style=\"\">








                                                                        
                                                                                <section id=\"SueldoFijo3\">


                      


";
        // line 527
        $this->env->loadTemplate("BenchFilepickerBundle:Default:SueldoFijo3.html.twig")->display($context);
        // line 528
        echo "                                                                                
                                                                                </section>

                                                                                
                                                                           
                                                                                  <section id=\"SueldoVariable6\">


                      


";
        // line 539
        $this->env->loadTemplate("BenchFilepickerBundle:Default:SueldoVariable6.html.twig")->display($context);
        // line 540
        echo "                                                                                      
                                                                                
                                                                                </section>
                                                                                
                                                                                
                                                                                <section id=\"cotizacionAfp\">
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    ";
        // line 550
        $this->env->loadTemplate("BenchFilepickerBundle:Default:cotizacionAfp.html.twig")->display($context);
        // line 551
        echo "                                                                                
                                                                                
                                                                                </section>
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                <section id=\"permisoCirculacion\">
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    ";
        // line 564
        $this->env->loadTemplate("BenchFilepickerBundle:Default:permisoCirculacion.html.twig")->display($context);
        // line 565
        echo "                                                                                
                                                                                    
                                                                                    
                                                                                </section>
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                 
                                                                                <section id=\"declaraciones3\">
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    ";
        // line 579
        $this->env->loadTemplate("BenchFilepickerBundle:Default:declaraciones3.html.twig")->display($context);
        // line 580
        echo "                                                                                
                                                                                    
                                                                                    
                                                                                </section>
                                                                                
                                                                                
                                                                                
                                                                                  <section id=\"declaracionAnual\">
                                                                                    
                                                                                    
                                                                                    
                                                                                      
                                                                                    
                                                                                    ";
        // line 593
        $this->env->loadTemplate("BenchFilepickerBundle:Default:declaracionAnual.html.twig")->display($context);
        // line 594
        echo "                                                                                
                                                                                    
                                                                                    
                                                                                </section>
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                 <section id=\"carnet\">
                                                                                    
                                                                                    
                                                                                    
                                                                                      
                                                                                   
                                                                                    
                                                                                    ";
        // line 609
        $this->env->loadTemplate("BenchFilepickerBundle:Default:carnet.html.twig")->display($context);
        // line 610
        echo "                                                                                
                                                                                    
                                                                                    
                                                                                </section>
                                                                                
                                                                                
                                                                      
                                                                                
                                                                                
                                                                              
                                                                                   <div style=\"clear: both;\"></div>
                                                                                   
                                                                                  
                                                                                   
                                                                                 <buttton style=\"width: 500px;height: 20px;margin-left: -30%; position: relative; margin-top:20px;\" id=\"Sigui\" class=\"classname\"> Debes selecciónar un tipo Sueldo / Vuelve a datos Laborales</buttton>
                                                                          
                                                                                
                                                                                
                                                                                
                                                                                <div style=\"clear: both;\"></div>
                                                                                 <buttton style=\"width: 200px;height: 20px;margin-left: -63%; position: relative; margin-top:20px;\" id=\"SiguienteUpload\" class=\"classname\">Siguiente</buttton>
                                                                                

                                                                                <!-- fin de upload explorer -->


                                                                                   <div style=\"clear: both;\"></div>
                                                                                   
                                                                               

                                                                            </section>
                                                                            
                                                                            
                                                                            
                                                                            <!-- FIN UPLOD -->



                                                                        </div>
                                                                    </div>












                                                                    <section class=\"tab-pane\" id=\"yellow\"><!-------------------------------- tab que tienes inicio ------------------------------------------>


                                                                        <div class=\"tabbable\"> 
                                                                            <ul class=\"nav nav-tabs2\" id=\"tabtienes\">
                                                                                <li class=\"active\"><a href=\"#tabBienesRaices\"  id=\"linkBienesRaices\"data-toggle=\"tab\">Bienes Raices</a></li>
                                                                                <li><a href=\"#tabAhorroInversion\" id=\"linkAhorroInversion\" data-toggle=\"tab\">Ahorro  e Inversión</a></li>
                                                                                <li><a href=\"#tabAutomoviles\" id=\"linkAutomoviles\" data-toggle=\"tab\">Automoviles</a></li>
                                                                                <li><a href=\"#tabSociedades\" id=\"linkSociedades\" data-toggle=\"tab\">Sociedades</a></li>
                                                                            </ul>
                                                                            <div class=\"tab-content\">


                                                                                <div class=\"tab-pane active\" id=\"tabBienesRaices\"> <!---------------- iniccio detalle bienes raices -------------------------------------------------->



    ";
        // line 679
        $this->env->loadTemplate("BenchTienesBundle:Default:tienesBienesRaices.html.twig")->display($context);
        // line 680
        echo "    ";
        $this->displayBlock('tienesBienesRaices', $context, $blocks);
        // line 685
        echo "




                                                                                </div> <!---------------- fin detalle bienes raices ---------------------------------------------->


                                                                                <div class=\"tab-pane\" id=\"tabAhorroInversion\"><!--- Ahorro inversion -->


 ";
        // line 696
        $this->env->loadTemplate("BenchTienesBundle:Default:tienesAhorroInversion.html.twig")->display($context);
        // line 697
        echo "



 ";
        // line 701
        $this->displayBlock('containerAhorroinversion', $context, $blocks);
        // line 707
        echo " 







                                                                                </div><!--- fin  Ahorro inversion -->


                                                                                <div class=\"tab-pane\" id=\"tabAutomoviles\"><!--- AUTOMOVILES -->




 ";
        // line 723
        $this->env->loadTemplate("BenchTienesBundle:Default:tienesAutomoviles.html.twig")->display($context);
        // line 724
        echo "



 ";
        // line 728
        $this->displayBlock('containerAutomoviles', $context, $blocks);
        // line 734
        echo " 



                                                                                </div> <!--- FIN  AUTOMOVILES -->





                                                                                <div class=\"tab-pane\" id=\"tabSociedades\"> <!------------ Sociedades ------------------------------------------------------------------------------------>



  ";
        // line 748
        $this->env->loadTemplate("BenchTienesBundle:Default:tienesSociedades.html.twig")->display($context);
        // line 749
        echo "



 ";
        // line 753
        $this->displayBlock('container', $context, $blocks);
        // line 759
        echo " 




                                                                                </div><!------------  fin  Sociedades ------------------------------------------------------------------------------------>





                                                                            </div>




                                                                        </div>







                                                                    </section><!-----------------------------------------------------------------------------------------------  fin de que tienes ---------------------------------->



                                                                    <!------------------ que debes ------------------------------------------------------------>

                                                                    <section class=\"tab-pane\" id=\"green\">


                                                                        <div class=\"tabbable\"> <!-- Only required for left/right tabs -->
                                                                            <ul class=\"nav nav-tabs2\" id=\"ulDebes\">


                                                                                <li class=\"active\"><a href=\"#debesHipo\" id=\"linkDebesHipotecario\" data-toggle=\"tab\">Hipotecario</a></li>
                                                                                <li><a href=\"#debeLcredito\" id=\"linkLcredito\" data-toggle=\"tab\">Tarjeta de Crédito</a></li>
                                                                                <li><a href=\"#debesTarjetaCredito\" id=\"linkTarjetaCredito2\"  data-toggle=\"tab\">Linea de Crédito</a></li>
                                                                                <li><a href=\"#debesCreditoConsumo\" id=\"linkCreditoConsumo\" data-toggle=\"tab\">Crédito de Consumo</a></li>
                                                                            </ul>







                                                                            <div class=\"tab-content\">


                                                                                <div class=\"tab-pane active\" id=\"debesHipo\"> <!--------------------- debes hipotecario ------------------------------------>



   ";
        // line 815
        $this->env->loadTemplate("BenchDebesBundle:Default:debesHipotecario.html.twig")->display($context);
        // line 816
        echo "

";
        // line 818
        $this->displayBlock('containerdebes', $context, $blocks);
        // line 824
        echo " 




                                                                                </div> 


                                                                                <div class=\"tab-pane\" id=\"debeLcredito\">  <!-------------------------- debes linea credito ------------------------------------------------------>



   ";
        // line 836
        $this->env->loadTemplate("BenchDebesBundle:Default:debesTarjetaCredito.html.twig")->display($context);
        // line 837
        echo "

";
        // line 839
        $this->displayBlock('tarjetaCreditodebes', $context, $blocks);
        // line 845
        echo " 





                                                                                </div> <!-------------------------- debes linea credito ------------------------------------------------------>


                                                                                <div class=\"tab-pane\" id=\"debesTarjetaCredito\"><!----- debes tajeta credito -->



        ";
        // line 858
        $this->env->loadTemplate("BenchDebesBundle:Default:debesLineCredito.html.twig")->display($context);
        // line 859
        echo "

";
        // line 861
        $this->displayBlock('lineaCreditodebes', $context, $blocks);
        // line 869
        echo " 





                                                                                </div><!-----  fin debes tajeta credito -->



                                                                                <div id=\"debesCreditoConsumo\" class=\"tab-pane\" ><!----- debes Credito consumo-->



          ";
        // line 883
        $this->env->loadTemplate("BenchDebesBundle:Default:debesCreditoConsumo.html.twig")->display($context);
        // line 884
        echo "

";
        // line 886
        $this->displayBlock('lineaCreditoConsumo', $context, $blocks);
        // line 894
        echo " 




                                                                                </div>


                                                                                <!----- debes Credito consumo-->



                                                                            </div>
                                                                        </div>

                                                                    </section>

                                                                    <!--------------------- que Debes final -------------------------------------------------------->       


                                                                    <!-------------------------------- tab donde -->
                                                                    <section class=\"tab-pane\" id=\"blue\">

                                                                        <section id=\"secdonde\">

                    ";
        // line 919
        $this->env->loadTemplate("BenchUsuariosBundle:Default:donde.html.twig")->display($context);
        // line 920
        echo "
                                                                        </section>
                                                                        <section id=\"mensajeDonde\">

                                                                            <h1 style=\"margin:50px auto; color:#F95B00;\">Muchas Gracias por cotizar con nosotros, tu solicitud de crédito se encuentra en trámite...</h1>
                                                                            <button id=\"siguientePaso\">Ir Al siguiente</button> 



                                                                        </section>





                                                                    </section>





                                                             





                                                              
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    <!--- Inicio mis Cotizaciones -->
                                                                        <section class=\"tab-pane\" id=\"misCotizaciones\">
           


";
        // line 960
        $this->env->loadTemplate("BenchPaginasBundle:Default:cotizaciones.html.twig")->display($context);
        // line 961
        echo "
                                                                  

                                                                            
                                                                            


                                                                    </section>
                                                                
                                                                   
                                                                   <!--- Fin mis Cotizaciones -->

                                                                   
                                                                   
                                                                   
                                                      <!--- Inicio Simulador  -->                       
                                                                        <section class=\"tab-pane\" id=\"simulador\">
           

                                                                  
                                                                            
                                                                    ";
        // line 982
        $this->env->loadTemplate("BenchPaginasBundle:Default:simuladorLog.html.twig")->display($context);
        // line 983
        echo "                                                                            <div></div>

                                                                    </section>
                                                      
                                                      
                                                      <!-- fin simulador -->

                                                                </div>    

                                                            </div>    
                                                            </div>


                                                            <div class=\"clearfix\"></div>










     ";
        // line 1007
        $this->env->loadTemplate("BenchPaginasBundle:Default:footer2.html.twig")->display($context);
        // line 1008
        echo "








</ul>
 </ul>
 </ul>
 </ul>












                                        </body>




    <script>
        \$(document).ready(function() {


                var user = '";
        // line 1042
        echo twig_escape_filter($this->env, (isset($context["user"]) ? $context["user"] : null), "html", null, true);
        echo "';





            var tipo = '";
        // line 1048
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["credito"]) ? $context["credito"] : null), "tipo", array()), "html", null, true);
        echo "' ;



            if(tipo == 'Crédito Hipotecario'){



                if(user == 'nuevo'){


                    var trackPageview = '/registro/registrado/CreditoHipotecario';
                    dataLayer.push({
                        'event':'PageView',
                        'trackPageview': trackPageview });



                }



                \$(\"#panel3\").slideToggle(\"slow\");
                \$(\"#panel1\").hide(\"slow\");
                \$(\"#panel2\").hide(\"slow\");
                \$(\"#panel4\").hide(\"slow\");
                \$(\"#panel5\").hide(\"slow\");

            }

            if(tipo == 'Crédito Consumo'){



                if(user == 'nuevo'){


                    var trackPageview = '/registro/registrado/CreditoConsumo';
                    dataLayer.push({
                        'event':'PageView',
                        'trackPageview': trackPageview });



                }

                \$(\"#panel1\").slideToggle(\"slow\");
                \$(\"#panel2\").hide(\"slow\");
                \$(\"#panel3\").hide(\"slow\");
                \$(\"#panel4\").hide(\"slow\");
                \$(\"#panel5\").hide(\"slow\");

            }

            if(tipo == 'Crédito Consolidacion'){


                if(user == 'nuevo'){


                    var trackPageview = '/registro/registrado/CreditoConsolidacion';
                    dataLayer.push({
                        'event':'PageView',
                        'trackPageview': trackPageview });



                }


                \$(\"#panel4\").slideToggle(\"slow\");
                \$(\"#panel1\").hide(\"slow\");
                \$(\"#panel2\").hide(\"slow\");
                \$(\"#panel3\").hide(\"slow\");
                \$(\"#panel5\").hide(\"slow\");

            }

            if(tipo == 'Cuenta Corriente'){



                if(user == 'nuevo'){


                    var trackPageview = '/registro/registrado/CuentaCorriente';
                    dataLayer.push({
                        'event':'PageView',
                        'trackPageview': trackPageview });



                }


                \$(\"#panel5\").slideToggle(\"slow\");
                \$(\"#panel1\").hide(\"slow\");
                \$(\"#panel2\").hide(\"slow\");
                \$(\"#panel3\").hide(\"slow\");
                \$(\"#panel4\").hide(\"slow\");

            }

            \$('#tusDatos').click(function(){



                var trackPageview = '/formulario/tus-datos';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });



            });

            \$(\"#laborallink\").click(function(){


                var trackPageview = '/formulario/tus-datos/datos-laborales';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });


            });

            \$('#linkAdjunta').click(function(){


                var trackPageview = '/formulario/tus-datos/adjuntar';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });



            });



            \$('#linkAhorroInversion').click(function(){
                var trackPageview = '/formulario/que-tienes/ahorro-inversion';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });


            });

            \$('#linkAutomoviles').click(function(){
                var trackPageview = '/formulario/que-tienes/automoviles';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });

            });


            \$('#linkquetienes').click(function(){


                var trackPageview = '/formulario/que-tienes';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });


            });

            \$('#linkSociedades').click(function(){

                var trackPageview = '/formulario/que-tienes/sociedades';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });


            });

            \$('#linkquedebes').click(function(){
                var trackPageview = '/formulario/que-debes';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });


            });

            \$('#linkDebesHipotecario').click(function(){

                var trackPageview = '/formulario/que-debes/hipotecario';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });


            });

            \$('#linkLcredito').click(function(){

                var trackPageview = '/formulario/que-debes/tarjeta-credito';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });

            });


            \$('#linkTarjetaCredito2').click(function(){


                var trackPageview = '/formulario/que-debes/linea-credito';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });

            });


            \$('#linkCreditoConsumo').click(function(){




                var trackPageview = '/formulario/que-debes/consumo-credito';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });


            });

            \$('#linkdondecotizar').click(function(){
                var trackPageview = '/formulario/donde-cotizar';
                dataLayer.push({
                    'event':'PageView',
                    'trackPageview': trackPageview });


            });


        });

    </script>









</html>


";
    }

    // line 130
    public function block_containerheadertab($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 279
    public function block_containerCreditoConsumo($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 314
    public function block_containerCreditoAutomotriz($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 343
    public function block_containerCreditoHipotecario($context, array $blocks = array())
    {
        echo " 





             ";
    }

    // line 369
    public function block_containerCreditoConsolidacion($context, array $blocks = array())
    {
        echo " 





             ";
    }

    // line 399
    public function block_containerCreditoC($context, array $blocks = array())
    {
        echo " 





             ";
    }

    // line 450
    public function block_containerDatosPersonales($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 472
    public function block_containerDatosLaborales($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 493
    public function block_containerConyuge($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 680
    public function block_tienesBienesRaices($context, array $blocks = array())
    {
        // line 681
        echo "


    ";
    }

    // line 701
    public function block_containerAhorroinversion($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 728
    public function block_containerAutomoviles($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 753
    public function block_container($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 818
    public function block_containerdebes($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 839
    public function block_tarjetaCreditodebes($context, array $blocks = array())
    {
        echo " 





 ";
    }

    // line 861
    public function block_lineaCreditodebes($context, array $blocks = array())
    {
        echo " 







 ";
    }

    // line 886
    public function block_lineaCreditoConsumo($context, array $blocks = array())
    {
        echo " 







 ";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:formulario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1736 => 886,  1722 => 861,  1710 => 839,  1698 => 818,  1686 => 753,  1674 => 728,  1662 => 701,  1655 => 681,  1652 => 680,  1640 => 493,  1628 => 472,  1616 => 450,  1604 => 399,  1592 => 369,  1580 => 343,  1568 => 314,  1556 => 279,  1544 => 130,  1281 => 1048,  1272 => 1042,  1236 => 1008,  1234 => 1007,  1208 => 983,  1206 => 982,  1183 => 961,  1181 => 960,  1139 => 920,  1137 => 919,  1110 => 894,  1108 => 886,  1104 => 884,  1102 => 883,  1086 => 869,  1084 => 861,  1080 => 859,  1078 => 858,  1063 => 845,  1061 => 839,  1057 => 837,  1055 => 836,  1041 => 824,  1039 => 818,  1035 => 816,  1033 => 815,  975 => 759,  973 => 753,  967 => 749,  965 => 748,  949 => 734,  947 => 728,  941 => 724,  939 => 723,  921 => 707,  919 => 701,  913 => 697,  911 => 696,  898 => 685,  895 => 680,  893 => 679,  822 => 610,  820 => 609,  803 => 594,  801 => 593,  786 => 580,  784 => 579,  768 => 565,  766 => 564,  751 => 551,  749 => 550,  737 => 540,  735 => 539,  722 => 528,  720 => 527,  690 => 499,  688 => 493,  684 => 491,  682 => 490,  668 => 478,  666 => 472,  662 => 470,  660 => 469,  645 => 456,  643 => 450,  636 => 445,  634 => 444,  593 => 405,  591 => 399,  583 => 393,  581 => 392,  562 => 375,  560 => 369,  552 => 363,  550 => 362,  535 => 349,  533 => 343,  525 => 337,  523 => 336,  505 => 320,  503 => 314,  493 => 306,  491 => 305,  469 => 285,  467 => 279,  459 => 273,  457 => 272,  440 => 258,  428 => 249,  412 => 236,  403 => 230,  388 => 218,  355 => 188,  315 => 151,  304 => 143,  296 => 138,  292 => 136,  290 => 130,  283 => 125,  281 => 124,  231 => 77,  227 => 76,  219 => 71,  210 => 65,  200 => 58,  194 => 55,  190 => 54,  182 => 49,  176 => 46,  171 => 44,  167 => 43,  163 => 42,  159 => 41,  155 => 40,  151 => 39,  147 => 38,  143 => 37,  139 => 36,  135 => 35,  131 => 34,  127 => 33,  123 => 32,  119 => 31,  115 => 30,  111 => 29,  107 => 28,  103 => 27,  99 => 26,  95 => 25,  91 => 24,  87 => 23,  83 => 22,  79 => 21,  75 => 20,  71 => 19,  67 => 18,  63 => 17,  59 => 16,  55 => 15,  51 => 14,  36 => 1,);
    }
}
