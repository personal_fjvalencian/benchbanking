<?php

/* BenchAdminBundle:plantillas:bci.html.twig */
class __TwigTemplate_6c5f51de99d2606a57ed5d12852045f8c5fadf30bcc06ed5cc6d5fc18f4b7a60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Plantilla bci</title>
    <style>

        body{
            font-family:Verdana, Geneva, sans-serif;
            font-size:12px;
        }

        .titText{
            width:auto;
            height:auto;
            position:relative;
            margin-left:3px;
            margin-top:2px;
            font-size:10px;
            font-weight:bold;
        }

        .simuInputOld{
            width: 300px;
            height: 20px;
            border-bottom: 1px;
            border-left: 0px;
            border-right: 0px;
            border-top: 0px;
            border-style: solid;
            position: relative;
            float: left;


        }

        .simuInput{
            width: 120px;
            height: 20px;
            border-style: solid;
            border-width: 1px;
            position: relative;
            float: left;
        }

        .simuInputdeudasLargoPlazo{
            width: 100px;
            height: 20px;
            border-style: solid;
            border-width: 1px;
            position: relative;
            float: left;
        }

        .simuInputbonosyAcciones{
            width: 100px;
            height: 20px;
            border-style: solid;
            border-width: 3px;
            position: relative;
            float: left;

        }

        .simuInputCpmyuge{
            width: 98px;
            height: 20px;
            border-style: solid;
            border-width: 1px;
            position: relative;
            float: left;

        }
        .simuInputPequeño{
            width: 20px;
            height: 20px;
            border-style: solid;
            border-width: 1px;
            position: relative;
            float: left;
        }

        .tabla{
            width: 710px;
        }

        .tdPequenio{
            width: 25px;
        }

        .contTitulos{
            width:710px;height:20px;background-color: #999999;

        }
        .rayaAzul{
            width:500px;background-color:#0000ff;height:20px;
            float:right;margin-left:200px;
        }
    </style>
</head>
<body>

<div class=\"rayaAzul\">
    &nbsp;
</div>

<div style=\"clear:both;\"></div>

<div style=\"width:350px;height:20px;position:relative;float:right;margin-right:-10%;\">
    Estado Situacion para personas naturales
</div>

<div style=\"width:auto;height:auto;position:relative;margin-top:-3%;\">
    <img src=\"http://1.bp.blogspot.com/-4bg_L8hhIpk/TwDbq3G0NMI/AAAAAAAAAeg/mBWVl7rqUq4/s1600/BCI.png\" style=\"width:22%\";/>
</div>


<div style=\"width:auto;height:auto;position:relative;float:right;margin-right:-1.1%;\">
    <div style=\"float: left; margin-right: 10px; padding-top: 4px;\">R.U.T</div>
    <div class=\"simuInput\">
        &nbsp ";
        // line 121
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "rut", array()), "html", null, true);
            echo " ";
        }
        // line 122
        echo "
    </div>
</div>

<div style=\"clear:both\"></div>

<br>
<br>


<div class=\"container\" style=\"width:710px; \">




<div class=\"contTitulos\"> <span class=\"titText\">Estado de situacion para personas naturales </span></div>


<table style=\"margin-top:10px;\">

    <tr>
        <td>
            <div class=\"simuInput\" style=\"width:682px;\">
                &nbsp ";
        // line 145
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellido", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellido", array()), "html", null, true);
            echo "  ";
        }
        // line 146
        echo "                &nbsp &nbsp  &nbsp &nbsp &nbsp &nbsp ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellidoseg", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        // line 147
        echo "                ";
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array()), "html", null, true);
            echo " ";
        }
        // line 148
        echo "          </div>
        </td>

    </tr>
    <tr>
        <td>
       <span> Apellidos Paternos   &nbsp  &nbsp  &nbsp  &nbsp        Apellidos Maternos    &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp           Nombres
        </td>

    </tr>

</table>
<table id=\"datosPersonales\" class=\"tabla\" style=\"width:690px\">
<thead>

</thead>
<tbody>

<tr>
    <td colspan=\"2\">
        <div class=\"simuInput\" style=\"width:280px;\">&nbsp ";
        // line 168
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array()), "html", null, true);
            echo "  ";
        }
        echo "</div>
    </td>

    <td colspan=\"1\">
        <div class=\"simuInput\" style=\"width:200px;\">&nbsp ";
        // line 172
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array()), "html", null, true);
            echo "  ";
        }
        echo "</div>
    </td>
    <td colspan=\"1\">
        <div class=\"simuInput\" style=\"width:150px;\">&nbsp ";
        // line 175
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo "</div>
    </td>

</tr>


<tr>
    <td colspan=\"2\">Direccion Particular / Calle</td>

    <td colspan=\"1\">Region</td>
    <td colspan=\"1\">Comuna</td>

</tr>



</tbody>
    </table>


<table>



    <tr>
    <td>
        <div class=\"simuInput\" style=\"width:120px;\">&nbsp ";
        // line 201
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array()), "html", null, true);
            echo "  ";
        }
        echo "   </div>
    </td>
    </tr>

    <tr>
    <td>Teléfono Particular </td>
    </tr>





<tr>
    <td colspan=\"2\">

        <div class=\"simuInput\" style=\"width: 280px;\">&nbsp ";
        // line 216
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo "</div>

    </td>

    <td>
        <div class=\"simuInput\" style=\"width:190px;\">&nbsp ";
        // line 221
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo "</div>
    </td>
    <td colspan=\"2\" >
        <div class=\"simuInput\" style=\"width:190px;\">&nbsp ";
        // line 224
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "region", array()), "html", null, true);
            echo " ";
        }
        echo "</div>
    </td>


</tr>



</table>







<tr>
    <td>
        <div class=\"simuInput\">&nbsp ";
        // line 242
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "fechanacimiento", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "fechanacimiento", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo " </div>
    </td>
    <td>
        <div class=\"simuInput\" style=\"width:200px;\">&nbsp ";
        // line 245
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo " </div>
    </td>
    <td>
        <div class=\"simuInput\"  style=\"width:200px;\">&nbsp ";
        // line 248
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo " </div>
    </td>

    <td>
        <div class=\"simuInput\" style=\"width:50px;\"> ";
        // line 252
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "ndependientes", array()), "html", null, true);
            echo " ";
        }
        echo "&nbsp</div>
    </td>
</tr>
<tr>
    <td>Fecha Nacimiento</td>
    <td>Nacionalidad</td>
    <td>Estado Civil</td>

    <td>N° de cargas familiares</td>
</tr>
<tr>

    <td>
        <div class=\"simuInput\"></div>
    </td>


</tr>
<tr>
    <td>Régimen Conyugal</td>

</tr>
</tbody>
</table>
<div class=\"contTitulos\"> <span class=\"titText\">Antecendentes Conyuge </span></div>

<table id=\"antecedentesConyuge\" class=\"tabla\" style=\"margin-top:10px;\">

    <tbody>
    <tr>
        <td width=\"125\">
            <div class=\"simuInput\">&nbsp ";
        // line 283
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellido", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellido", array()), "html", null, true);
                echo " ";
            }
        }
        echo "</div>
        </td>
        <td width=\"122\">
            <div class=\"simuInput\">&nbsp";
        // line 286
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellidoseg", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellidoseg", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        // line 287
        echo "            </div>
        </td>
        <td width=\"447\">
            <div class=\"simuInput\" style=\"width:430px;\">&nbsp ";
        // line 290
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "nombre", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "nombre", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo " </div>
        </td>
    </tr>
    <tr>
        <td>
            Apellido Paterno
        </td>
        <td>
            Apellido Materno
        </td>
        <td>
            Nombres
        </td>
    </tr>

    </tbody>
</table>

<table>
    <tr>
        <td width=\"122\">
            <div class=\"simuInput\">&nbsp ";
        // line 311
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rut", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rut", array()), "html", null, true);
                echo " ";
            }
            echo "    ";
        }
        echo "</div>
        </td>
        <td width=\"124\">
            <div class=\"simuInput\">&nbsp ";
        // line 314
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "profesion", array())) {
                echo "  ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "profesion", array()), "html", null, true);
                echo " ";
            }
            echo "  ";
        }
        echo "         </div>
        </td>
        <td width=\"122\">
            <div class=\"simuInput\">&nbsp ";
        // line 317
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo "</div>
        </td>
        <td width=\"123\">
            <div class=\"simuInput\">&nbsp ";
        // line 320
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            if ($this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "telefono", array())) {
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "telefono", array()), "html", null, true);
                echo " ";
            }
            echo " ";
        }
        echo "</div>
        </td>
        <td width=\"195\">
            <div class=\"simuInput\" style=\"width:177px;\">&nbsp ";
        // line 323
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "telefono", array()), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>
    </tr>
    <tr>
        <td>
            R.U.T / C.I.
        </td>
        <td>
            Prefesión/Actividad
        </td>
        <td>
            Renta Liq Mensual(\$)
        </td>
        <td>
            Teléfono Comercial
        </td>
        <td>
            Fax - Celular
        </td>
    </tr>
    </tbody>
</table>

<div class=\"contTitulos\"> <span class=\"titText\"> Antecedentes Laborales </span></div>



<table id=\"antecedentesLaborales\">





    <tr>
        <td>
            <div class=\"simuInput\" style=\"width:220px;\">&nbsp ";
        // line 358
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "situacionlaboral", array()), "html", null, true);
            echo " ";
        }
        echo "  </div>
        </td>
        <td>
            <div class=\"simuInput\" style=\"width:170px;\">&nbsp  ";
        // line 361
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "empleadorempresa", array()), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>
        <td>
            <div class=\"simuInput\"  style=\"width:180px;\">&nbsp ";
        // line 364
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "industria", array()), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>

        <td>
            <div class=\"simuInput\" style=\"width:90px;\">&nbsp ";
        // line 368
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "fechaingreso", array()), "html", null, true);
            echo " ";
        }
        echo "</div>
        </td>
    </tr>
    <tr>
        <td>Situación Laboral </td>
        <td>Empleador Empresa </td>
        <td>Industria </td>

        <td>Fecha de Ingreso </td>
    </tr>


</table>

<table>

    <tr>
        <td>
            <div class=\"simuInput\" style=\"width:220px;\">&nbsp ";
        // line 386
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "cargoactual", array()), "html", null, true);
            echo " ";
        }
        echo "  </div>
        </td>
        <td>
            <div class=\"simuInput\" style=\"width:170px;\">&nbsp  ";
        // line 389
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "rutempresa", array()), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>
        <td>
            <div class=\"simuInput\"  style=\"width:280px;\">&nbsp ";
        // line 392
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "direccion", array()), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>


    </tr>
    <tr>
        <td>Cargo Actual</td>
        <td>RUT Empresa  </td>
        <td>Dirección  </td>


    </tr>


</table>




<table>

    <tr>
        <td>
            <div class=\"simuInput\" style=\"width:220px;\">&nbsp ";
        // line 415
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "region", array()), "html", null, true);
            echo " ";
        }
        echo "  </div>
        </td>
        <td>
            <div class=\"simuInput\" style=\"width:170px;\">&nbsp  ";
        // line 418
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>
        <td>
            <div class=\"simuInput\"  style=\"width:280px;\">&nbsp ";
        // line 421
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "ciudad", array()), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>


    </tr>
    <tr>
        <td>Región</td>
        <td>Comuna </td>
        <td>Ciudad  </td>


    </tr>


</table>



<table>

    <tr>
        <td>
            <div class=\"simuInput\" style=\"width:220px;\">&nbsp ";
        // line 443
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "telefono", array()), "html", null, true);
            echo " ";
        }
        echo "  </div>
        </td>
        <td>
            <div class=\"simuInput\" style=\"width:170px;\">&nbsp  ";
        // line 446
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "independientesueldoliquido", array()), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>



    </tr>
    <tr>
        <td>Teléfono </td>
        <td>Sueldo Líquido</td>


    </tr>


</table>






<br>
<br>
<br>
<br>




<div class=\"contTitulos\"> <span class=\"titText\"> Referencias </span></div>


<table width=\"703\" id=\"Referencias\">

    <tbody>
    <tr>
        <td width=\"170\">
            Bancarias
        </td>
    </tr>
    <tr>
        <td>
            <div class=\"simuInput\">&nbsp</div>
        </td>
        <td width=\"192\">
            <div class=\"simuInput\">&nbsp</div>
        </td>
        <td width=\"123\">
            <div class=\"simuInput\">&nbsp</div>
        </td>
        <td width=\"198\">
            <div class=\"simuInput\" style=\"width:174px;\">&nbsp</div>
        </td>
    </tr>
    <tr>
        <td>
            Banco
        </td>
        <td>
            Sucursal
        </td>
        <td>
            N° de cuenta
        </td>
        <td>
            Tipo de Tarjeta
        </td>
    </tr>
    <tr colspan=\"4\">
        <td>Personales:</td>
    </tr>
    <tr>
        <td>
            <div class=\"simuInput\">&nbsp</div>
        </td>
        <td></td>
        <td>
            <div class=\"simuInput\">&nbsp</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>
            <div class=\"simuInput\">&nbsp</div>
        </td>
        <td></td>
        <td>
            <div class=\"simuInput\">&nbsp</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>
            Nombre
        </td>
        <td></td>
        <td>
            Teléfono
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>
            <div class=\"\"></div>
        </td>
        <td>
            <div class=\"simuInput\">&nbsp</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>Firma Cliente</td>
        <td>Fecha</td>
        <td></td>
    </tr>
    </tbody>
</table>




<br>
<br>
<br>
<br>

<div class=\"contTitulos\"> <span class=\"titText\"> Activos </span></div>


<br><br>
<strong>Depositos a plazo, libretas ahorro, fondos mutuos / ";
        // line 578
        if ((isset($context["tienesAhorroInversion"]) ? $context["tienesAhorroInversion"] : null)) {
            echo " SI ";
        } else {
            echo "  ";
            if ((isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </strong>

<table id=\"depositos\">


    <tbody>
    <tr colspan=\"2\">
        <td width=\"171\"></td>
    </tr>



";
        // line 590
        if ((isset($context["tienesAhorroInversion"]) ? $context["tienesAhorroInversion"] : null)) {
            // line 591
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesAhorroInversion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesAhorroInversion"]) {
                // line 592
                echo "
    <tr>


        <td>
            <div class=\"simuInput\">&nbsp 1 ";
                // line 597
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "institucion", array()), "html", null, true);
                echo "</div>
        </td>
        <td width=\"189\">
            <div class=\"simuInput\">&nbsp 2 ";
                // line 600
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "tipo", array()), "html", null, true);
                echo " </div>
        </td>
        <td width=\"106\">
            \$
        </td>
        <td width=\"204\">
            <div class=\"simuInput\" style=\"width:180px;\">&nbsp ";
                // line 606
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "valorcomercial", array()), "html", null, true);
                echo "  </div>
        </td>



    </tr>


";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAhorroInversion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 616
        echo "








    <tr>
        <td>
            Institución bancaria
        </td>
        <td>
            Tipo inversion
        </td>
        <td>
            Total \$
        </td>
        <td>
            <div class=\"simuInput\" style=\"width:180px;\">&nbsp ";
        // line 636
        if ((isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo " </div>
        </td>
    </tr>
    </tbody>
</table>
<table class=\"vehiculos\" >
    <thead>
    <th> 
        Vehiculos  / ";
        // line 644
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : null)) {
            echo "  SI ";
        } else {
            echo " ";
            if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO  ";
            }
            echo " ";
        }
        echo "  
    </th>
    </thead>
    <tbody>


";
        // line 650
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : null)) {
            // line 651
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesAutomoviles"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesAutomoviles"]) {
                // line 652
                echo "    <tr>
        <td><div class=\"simuInput\">&nbsp ";
                // line 653
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "marcaauto", array()), "html", null, true);
                echo " </div></td>
        <td><div class=\"simuInput\">&nbsp ";
                // line 654
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "modelo", array()), "html", null, true);
                echo "</div></td>
        <td><div class=\"simuInput\">&nbsp ";
                // line 655
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "anoauto", array()), "html", null, true);
                echo " </div></td>
        <td></td>
        <td><div class=\"simuInputPequeño\">&nbsp ";
                // line 657
                if (($this->getAttribute($context["tienesAutomoviles"], "prendado", array()) == "si")) {
                    echo "  X ";
                }
                echo " </div></td>
        <td><div class=\"simuInputPequeño\">&nbsp ";
                // line 658
                if (($this->getAttribute($context["tienesAutomoviles"], "prendado", array()) == "no")) {
                    echo "  X ";
                }
                echo " </div></td>
        <td>\$  </td>
        <td><div class=\"simuInput\" style=\"width:138px;\"> &nbsp ";
                // line 660
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "valorcomercial", array()), "html", null, true);
                echo " </div></td>
    </tr>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAutomoviles'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 664
            echo "
";
        }
        // line 666
        echo "


    <tr>
        <td>
            Marca
        </td>
        <td>
            Modelo
        </td>
        <td>
            Año
        </td>
        <td>
            Prenda Bco
        </td>
        <td>
            Si
        </td>
        <td>
            No
        </td>
        <td>
            Total \$
        </td>
        <td>
            <div class=\"simuInput\" style=\"width:138px;\" >&nbsp  ";
        // line 692
        if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div>
        </td>
    </tr>
    </tbody>
</table>
<table id=\"propiedades\">
    <thead>
    <th>


        Propiedades    /     ";
        // line 702
        if ((isset($context["tienesHiporecario"]) ? $context["tienesHiporecario"] : null)) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 703
        echo "

    </th>
    </thead>
    <tbody>

    ";
        // line 709
        if ((isset($context["tienesHiporecario"]) ? $context["tienesHiporecario"] : null)) {
            // line 710
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesHiporecario"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesHiporecario"]) {
                // line 711
                echo "

    <tr>
        <td>
            <div class=\"simuInput\">&nbsp ";
                // line 715
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "tipo", array()), "html", null, true);
                echo "</div>
        </td>
        <td>
            <div class=\"simuInput\" style=\"font-size:9px;\">&nbsp ";
                // line 718
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "direccion", array()), "html", null, true);
                echo "</div>
        </td>
        <td>
            <div class=\"simuInput\" style=\"font-size:9px;\" >&nbsp  ";
                // line 721
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "comuna", array()), "html", null, true);
                echo " </div>
        </td>
        <td>
            <div class=\"simuInput\">&nbsp ";
                // line 724
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "hipotecado", array()), "html", null, true);
                echo " </div>
        </td>
        <td>
            \$
        </td>
        <td>
            <div class=\"simuInput\" style=\"width:132px;\">&nbsp ";
                // line 730
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "avaluo", array()), "html", null, true);
                echo "</div>
        </td>
    </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesHiporecario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 734
            echo "

   ";
        }
        // line 737
        echo "


    <tr>
        <td>Tipo Propiedad</td>
        <td>Dirección</td>
        <td>Comuna</td>
        <td>Hipoteca Bco</td>
        <td>Total \$</td>
        <td><div class=\"simuInput\" style=\"width:132px;\">&nbsp  ";
        // line 746
        if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null)) {
            echo " 
        ";
            // line 747
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null), 0, ".", "."), "html", null, true);
            echo "   
        ";
        } else {
            // line 748
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo " </div></td>
    </tr>


    </tbody>
</table>
<table id=\"participaciones\">
    <thead>
    <th width=\"122\">Participaciones  / ";
        // line 756
        if ((isset($context["tienesSociedades"]) ? $context["tienesSociedades"] : null)) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo "</th>
    <td width=\"122\"></thead>
    <tbody>

";
        // line 760
        if ((isset($context["tienesSociedades"]) ? $context["tienesSociedades"] : null)) {
            echo " 
";
            // line 761
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesSociedades"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesSociedades"]) {
                // line 762
                echo "    <tr>
        <td>
            <div class=\"simuInput\">&nbsp ";
                // line 764
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "nombre", array()), "html", null, true);
                echo "</div>
        </td>
        <td>
            <div class=\"simuInput\">&nbsp ";
                // line 767
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "rut", array()), "html", null, true);
                echo "</div>
        </td>
        <td width=\"122\">
            <div class=\"simuInput\">&nbsp ";
                // line 770
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "porcentaje", array()), "html", null, true);
                echo " </div>
        </td>
        <td width=\"123\">\$</td>
        <td width=\"197\">
            <div class=\"simuInput\" style=\"width:172px;\">&nbsp ";
                // line 774
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "valor", array()), "html", null, true);
                echo "</div>
        </td>
    </tr>

";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesSociedades'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 780
        echo "

    <tr>
        <td>
            Razón social
        </td>
        <td>
            R.U.T
        </td>
        <td>
            %Participacion
        </td>
        <td>
            Total \$
        </td>
        <td>
            <div class=\"simuInput\" style=\"width:172px;\" >&nbsp ";
        // line 796
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalSociedades"]) ? $context["totalSociedades"] : null), 0, ".", "."), "html", null, true);
        echo "</div>
        </td>
    </tr>
    </tbody>
</table>






<div class=\"contTitulos\"> <span class=\"titText\">Pasivos </span></div>



<table id=\"pasivos\">
    <thead>
    <th></th>
    </thead>
</table>
<table width=\"712\" id=\"deudascortoPlazo\">

    <tbody>
    <tr>
        <td colspan=\"9\">
          Linea Credito  / ";
        // line 821
        if ((isset($context["debesLineaCredito"]) ? $context["debesLineaCredito"] : null)) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo "  ";
        }
        // line 822
        echo "        </td>
    </tr>

   ";
        // line 825
        $context["setLineT"] = 0;
        // line 826
        echo "    ";
        if ((isset($context["debesLineaCredito"]) ? $context["debesLineaCredito"] : null)) {
            // line 827
            echo "     ";
            $context["setLineT"] = 0;
            // line 828
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesLineaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesLineaCredito"]) {
                // line 829
                echo "    <tr>
        <td width=\"131\">
            <div class=\"simuInput\">&nbsp ";
                // line 831
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "institucion", array()), "html", null, true);
                echo "</div>
        </td>
        <td width=\"125\">
            <div class=\"simuInput\">&nbsp ";
                // line 834
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoAprobado", array()), "html", null, true);
                echo "</div>
        </td>
        <td width=\"125\">
            <div class=\"simuInput\">&nbsp ";
                // line 837
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoUtilizado", array()), "html", null, true);
                echo "</div>
        </td>

        <td width=\"125\">
           <div class=\"simuInput\">&nbsp ";
                // line 841
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "vencimientofinal", array()), "html", null, true);
                echo "</div>
        </td>

        



        </tr>


        ";
                // line 851
                $context["setLineT"] = ($this->getAttribute($context["debesLineaCredito"], "montoUtilizado", array()) + (isset($context["setLineT"]) ? $context["setLineT"] : null));
                // line 852
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesLineaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 854
            echo "    ";
        }
        // line 855
        echo "


<td width=\"13\"></td>
        <td width=\"95\">Deuda Total \$</td>


 


        <td width=\"199\">

        <div class=\"simuInput\" style=\"width:100px;\">

 ";
        // line 869
        echo twig_escape_filter($this->env, (isset($context["setLineT"]) ? $context["setLineT"] : null), "html", null, true);
        echo "
        
       </div>

        </td>
    </tr>






";
        // line 881
        if ((isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : null)) {
            echo " 
        <td width=\"199\">

        <div class=\"simuInput\" style=\"width:100px;\">


        
         ";
            // line 888
            if ((isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : null)) {
                echo " ";
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualLineaCreditoApi"]) ? $context["pagoMensualLineaCreditoApi"] : null), 0, ".", "."), "html", null, true);
                echo " ";
            }
            echo "</div>

        </td>


        ";
        }
        // line 894
        echo "


    <tr>
        <td>
            Institución Bancaria
        </td>
        <td>
            Monto Aprobado
        </td>
        <td>
           Monto Utilizado
        </td>
        <td>
            Vencimiento

        </td>

        <td>

        </td>
    </tr>
    </tbody>
</table>
<table id=\"tarjetaCredito\">
    <thead>
    <th width=\"129\">
        Tarjetas de crédito  / ";
        // line 921
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : null)) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 922
        echo "    </th>
    <td width=\"122\"></thead>
    <tbody>


    ";
        // line 927
        $context["totalDebesFor"] = 0;
        // line 928
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : null)) {
            // line 929
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesTarjetaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesTarjetaCredito"]) {
                // line 930
                echo "    <tr>
        <td><div class=\"simuInput\">&nbsp ";
                // line 931
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "institucion", array()), "html", null, true);
                echo " </div></td>
        <td><div class=\"simuInput\">&nbsp  ";
                // line 932
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoaprobado", array()), "html", null, true);
                echo " &nbsp;    </div></td>
        <td width=\"122\"><div class=\"simuInput\">&nbsp ";
                // line 933
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoutilizado", array()), "html", null, true);
                echo "  &nbsp; </div></td>
        <td width=\"13\"></td>
      <td width=\"95\">Deuda  \$</td>



      

        <td width=\"199\"><div class=\"simuInput\" style=\"width:180px;\">&nbsp ";
                // line 941
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoutilizado", array()), "html", null, true);
                echo "  </div></td>
    </tr>




    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesTarjetaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 948
            echo "    ";
        }
        // line 949
        echo "
    <tr>
        <td>Institución bancaria</td>
        <td>Cupo</td>
        <td>Pago mensual</td>
        <td></td>
        <td>Total \$</td>
    <td><div class=\"simuInput\" style=\"width:180px;\">&nbsp ";
        // line 956
        if ((isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div></td>
    </tr>
    </tbody>
</table>



<table id=\"tarjetaCredito\">
    <thead>
    <th width=\"129\">
        Hipotecario  / ";
        // line 966
        if ((isset($context["debesHipotecario"]) ? $context["debesHipotecario"] : null)) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        // line 967
        echo "    </th>
    <td width=\"122\"></thead>
    <tbody>


";
        // line 972
        $context["debesHipotecarioFor"] = 0;
        // line 973
        echo "



";
        // line 977
        if ((isset($context["debesHipotecario"]) ? $context["debesHipotecario"] : null)) {
            // line 978
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesHipotecario"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesHipotecario"]) {
                // line 979
                echo "        <tr>
            <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp  ";
                // line 980
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "institucion", array()), "html", null, true);
                echo "  </div></td>
            <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp  ";
                // line 981
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "pagomensual", array()), "html", null, true);
                echo "     </div></td>
            <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp  ";
                // line 982
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "deudatotal", array()), "html", null, true);
                echo "    </div></td>
            <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp   ";
                // line 983
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "venciminetofinal", array()), "html", null, true);
                echo "    </div></td>
        

            <td width=\"13\"></td>
            <td width=\"95\">Deuda Vigente \$</td>





            <td width=\"199\"><div class=\"simuInput\" style=\"width:90px;\" >&nbsp   ";
                // line 993
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "duedavigente", array()), "html", null, true);
                echo " </div></td>
        </tr>


        ";
                // line 997
                $context["debesHipo"] = strtr($this->getAttribute($context["debesHipotecario"], "duedavigente", array()), array("." => ""));
                // line 998
                echo "        ";
                $context["debesHipo"] = strtr($this->getAttribute($context["debesHipotecario"], "duedavigente", array()), array("." => ""));
                // line 999
                echo "
        ";
                // line 1000
                $context["debesHipotecarioFor"] = ((isset($context["debesHipo"]) ? $context["debesHipo"] : null) + (isset($context["debesHipo"]) ? $context["debesHipo"] : null));
                // line 1001
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesHipotecario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 1004
        echo "
<tr>
    <td>Institución bancaria</td>
    <td>Pago Mensual</td>
    <td>Deuda Total</td>
    <td>Vencimiento Final</td>

    <td></td>
    <td>Total \$</td>
    <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp  ";
        // line 1013
        if ((isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["pagoMensualApi"]) ? $context["pagoMensualApi"] : null), 0, ".", "."), "html", null, true);
            echo "   ";
        } else {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div></td>
</tr>
</tbody>
</table>

<table id=\"tarjetaCredito\">
    <thead>
    <th width=\"129\">
        Consumo  / ";
        // line 1021
        if ((isset($context["debesCreditoConsumo"]) ? $context["debesCreditoConsumo"] : null)) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        echo " 
    </th>
    <td width=\"122\"></thead>
    <tbody>



";
        // line 1028
        if ((isset($context["debesCreditoConsumo"]) ? $context["debesCreditoConsumo"] : null)) {
            // line 1029
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesCreditoConsumo"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesCreditoConsumo"]) {
                // line 1030
                echo "        <tr>
            <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp  ";
                // line 1031
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "institucion", array()), "html", null, true);
                echo "  </div></td>
            <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp  ";
                // line 1032
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "pagomensual", array()), "html", null, true);
                echo "     </div></td>
            <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp  ";
                // line 1033
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudatotal", array()), "html", null, true);
                echo "    </div></td>
            <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp   ";
                // line 1034
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "fechaultimopago", array()), "html", null, true);
                echo "    </div></td>
        

            <td width=\"13\"></td>
            <td width=\"95\">Deuda Vigente \$</td>





            <td width=\"199\"><div class=\"simuInput\" style=\"width:90px;\" >&nbsp   ";
                // line 1044
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudavigente", array()), "html", null, true);
                echo " </div></td>
        </tr>


     
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesCreditoConsumo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 1051
        echo "
<tr>
    <td>Institución bancaria</td>
    <td>Pago Mensual</td>
    <td>Deuda Total</td>
    <td>Fecha Último Pago</td>

    <td></td>
    <td>Deuda Vigente Total\$</td>
    <td><div class=\"simuInput\" style=\"width:90px;\">&nbsp ";
        // line 1060
        if ((isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : null)) {
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo "</div></td>
</tr>
</tbody>
</table>

<br>
<br>
<br>
<br>

<br>
<br>
<br>
<br>


<br>
<br>
<br>
<br>

<br>
<br>
<br>
<br>



<br>
<br>
<br>
<br>

<br>
<br>
<br>
<br>





<table>
    <thead>
    <th colspan=\"9\" align=\"left\">
        AVALES OTORGADOS(Deudas Indirectas)
    </th>
    </thead>
    <tbody>
    <tr>
        <td><div class=\"simuInput\">&nbsp</div></td>
        <td><div class=\"simuInput\">&nbsp</div></td>
        <td><div class=\"simuInput\">&nbsp</div></td>
        <td><div class=\"simuInput\">&nbsp</div></td>
    </tr>
    <tr>
        <td><div class=\"simuInput\">&nbsp</div></td>
        <td><div class=\"simuInput\">&nbsp</div></td>
        <td><div class=\"simuInput\">&nbsp</div></td>
        <td><div class=\"simuInput\">&nbsp</div></td>
    </tr>
    <tr>
        <td>Persona avalada</td>
        <td>Institución</td>
        <td>Plazo</td>
        <td>Saldo Deuda</td>
    </tr>
    </tbody>
</table>
<table id=\"firmaCliente\">
    <tr>
        <td><div class=\"simuInputOld\">&nbsp</div></td>
        <td><div class=\"simuInput\">&nbsp</div></td>
    </tr>
    <tr>
        <td>Firma Cliente</td>
        <td>Fecha</td>
    </tr>

</table>






<br>
<br>
<br>
<br>

     <h1 align=\"right\">SOLICITUD DE PRODUCTOS</h1>














  <h3>Crédito de consumo:</h3>
            <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Monto de Crédito:</td>
                        <td>Plazo de Crédito </td>
                        <td>Seguros Asociados </td>
                        <td>¿Para qué utilizarás el Dinero? </td>

                    </tr>



                            <tr>
                                <td><div class=\"simuInput\">&nbsp;   ";
        // line 1185
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "montoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"simuInput\">&nbsp;    ";
        // line 1186
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"simuInput\">&nbsp;    ";
        // line 1187
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "segurosAsociados", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                                <td><div class=\"simuInput\">&nbsp;    ";
        // line 1188
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "paraque", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                            </tr>






                    </tbody>
                </table>




                <h3>Crédito Hipotecario </h3>
                <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Valor Propiedad :</td>
                        <td>Monto de Píe </td>
                        <td>Porcentaje </td>
                       

                    </tr>



                        <tr>
                            <td><div class=\"simuInput\">&nbsp;   ";
        // line 1223
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "valorPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1224
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoPie", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp;   ";
        // line 1225
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "porcentaje", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                        


                        </tr>



                    <tr> <td colspan=\"2\">Monto de Crédito </td></tr>
                    <tr>
                        <td><div class=\"simuInput\">&nbsp;  ";
        // line 1235
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    </tr>





                    <tr>
                        <td>Plazo de Crédito </td>
                        <td>Tipo de Propiedad </td>
                    
                        <td>¿Tu propiedad es DFL2 y nueva ? </td>

                    </tr>



                        <tr>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1253
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                            <td><div class=\"simuInput\">&nbsp;";
        // line 1254
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "tipoPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "   </div></td>
                           
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1256
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "dfl2", array()), "html", null, true);
            echo " ";
        }
        echo "            </div></td>


                        </tr>



                <tr><td colspan=\"2\">Estado</td></tr>

                <tr> <td colspan=\"2\" ><div class=\"simuInput\">&nbsp; ";
        // line 1265
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "estadoPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td></tr>








                    <tr>
                        <td>Comuna donde desea comprar</td>
                        <td>Proyecto / Inmobiliaria </td>


                    </tr>



                        <tr>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1284
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "comunaComprar", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1285
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "proyecto", array()), "html", null, true);
            echo "     ";
        }
        echo "</div></td>


                        </tr>







                    </tbody>
                </table>

    <h3>Crédito Automotriz</h3>

                    <table class=\"tabla\">
                        <thead>

                        </thead>
                        <tbody>
                        <tr>
                            <td>Monto del Auto </td>
                            <td>Monto del Píe  </td>
                            <td>Plazo del Crédito </td>


                        </tr>


                                <tr>
                                    <td><div class=\"simuInput\">&nbsp; ";
        // line 1316
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "monto", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                                    <td><div class=\"simuInput\">&nbsp; ";
        // line 1317
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "pie", array()), "html", null, true);
            echo "    ";
        }
        echo "</div></td>
                                    <td><div class=\"simuInput\">&nbsp; ";
        // line 1318
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "plazo", array()), "html", null, true);
            echo "  ";
        }
        echo "</div></td>


                                </tr>



                        <tr>
                            <td>Cuándo piensas comprar tu auto </td>
                            <td>Marca de Auto  </td>
                            <td>Año del Auto </td>


                        </tr>


                        <tr>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1335
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "cuandocompras", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1336
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "marcaauto", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"simuInput\">&nbsp; ";
        // line 1337
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "ano", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>





                        </tbody>
                    </table>



<br>
<br>
<br>
<br>
<br>
<br>
<br>

<h3>
DATOS CRÉDITO CONSOLIDACION 
</h3>
<tr>
    <th>Crédito </th>
    <th>Monto </th>
    <th>Plazo </th>

  
    </tr>



    </thead>

    <tr>
    ";
        // line 1375
        $context["con"] = 0;
        // line 1376
        echo "    ";
        if ((isset($context["creditoConsolidacion"]) ? $context["creditoConsolidacion"] : null)) {
            // line 1377
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["creditoConsolidacion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["creditoConsolidacion"]) {
                // line 1378
                echo "    ";
                $context["con"] = ((isset($context["con"]) ? $context["con"] : null) + 1);
                // line 1379
                echo "
    ";
                // line 1380
                if (((isset($context["con"]) ? $context["con"] : null) <= 1)) {
                    // line 1381
                    echo "
    <tr>   

        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1384
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1385
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1386
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo", array()), "html", null, true);
                    echo " </div> </td>
     </tr>
        
       <tr>    

        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1391
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1392
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1393
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo2", array()), "html", null, true);
                    echo " </div> </td>
</tr>
<tr>   


        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1398
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito3", array()), "html", null, true);
                    echo "  </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1399
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito3", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"simuInput\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1400
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo3", array()), "html", null, true);
                    echo " </div></td>
       
   </tr> 

    ";
                }
                // line 1405
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsolidacion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1406
            echo "    ";
        }
        // line 1407
        echo "    
        </tr>

</table>




</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:plantillas:bci.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2284 => 1407,  2281 => 1406,  2275 => 1405,  2267 => 1400,  2263 => 1399,  2259 => 1398,  2251 => 1393,  2247 => 1392,  2243 => 1391,  2235 => 1386,  2231 => 1385,  2227 => 1384,  2222 => 1381,  2220 => 1380,  2217 => 1379,  2214 => 1378,  2209 => 1377,  2206 => 1376,  2204 => 1375,  2159 => 1337,  2151 => 1336,  2143 => 1335,  2119 => 1318,  2111 => 1317,  2103 => 1316,  2065 => 1285,  2057 => 1284,  2032 => 1265,  2017 => 1256,  2008 => 1254,  2000 => 1253,  1975 => 1235,  1958 => 1225,  1950 => 1224,  1942 => 1223,  1900 => 1188,  1892 => 1187,  1884 => 1186,  1876 => 1185,  1745 => 1060,  1734 => 1051,  1721 => 1044,  1708 => 1034,  1704 => 1033,  1700 => 1032,  1696 => 1031,  1693 => 1030,  1688 => 1029,  1686 => 1028,  1672 => 1021,  1653 => 1013,  1642 => 1004,  1634 => 1001,  1632 => 1000,  1629 => 999,  1626 => 998,  1624 => 997,  1617 => 993,  1604 => 983,  1600 => 982,  1596 => 981,  1592 => 980,  1589 => 979,  1584 => 978,  1582 => 977,  1576 => 973,  1574 => 972,  1567 => 967,  1555 => 966,  1538 => 956,  1529 => 949,  1526 => 948,  1513 => 941,  1502 => 933,  1498 => 932,  1494 => 931,  1491 => 930,  1486 => 929,  1484 => 928,  1482 => 927,  1475 => 922,  1469 => 921,  1440 => 894,  1427 => 888,  1417 => 881,  1402 => 869,  1386 => 855,  1383 => 854,  1376 => 852,  1374 => 851,  1361 => 841,  1354 => 837,  1348 => 834,  1342 => 831,  1338 => 829,  1333 => 828,  1330 => 827,  1327 => 826,  1325 => 825,  1320 => 822,  1308 => 821,  1280 => 796,  1262 => 780,  1250 => 774,  1243 => 770,  1237 => 767,  1231 => 764,  1227 => 762,  1223 => 761,  1219 => 760,  1208 => 756,  1194 => 748,  1189 => 747,  1185 => 746,  1174 => 737,  1169 => 734,  1159 => 730,  1150 => 724,  1144 => 721,  1138 => 718,  1132 => 715,  1126 => 711,  1121 => 710,  1119 => 709,  1111 => 703,  1105 => 702,  1084 => 692,  1056 => 666,  1052 => 664,  1042 => 660,  1035 => 658,  1029 => 657,  1024 => 655,  1020 => 654,  1016 => 653,  1013 => 652,  1008 => 651,  1006 => 650,  987 => 644,  968 => 636,  946 => 616,  930 => 606,  921 => 600,  915 => 597,  908 => 592,  904 => 591,  902 => 590,  877 => 578,  738 => 446,  728 => 443,  699 => 421,  689 => 418,  679 => 415,  649 => 392,  639 => 389,  629 => 386,  604 => 368,  593 => 364,  583 => 361,  573 => 358,  531 => 323,  517 => 320,  503 => 317,  489 => 314,  475 => 311,  443 => 290,  438 => 287,  428 => 286,  415 => 283,  377 => 252,  362 => 248,  348 => 245,  334 => 242,  309 => 224,  299 => 221,  287 => 216,  266 => 201,  234 => 175,  224 => 172,  213 => 168,  191 => 148,  184 => 147,  178 => 146,  172 => 145,  147 => 122,  141 => 121,  19 => 1,);
    }
}
