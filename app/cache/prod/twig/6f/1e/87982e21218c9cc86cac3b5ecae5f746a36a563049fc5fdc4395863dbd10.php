<?php

/* BenchUsuariosBundle:Default:antecedentelaborales.html.twig */
class __TwigTemplate_6f1e87982e21218c9cc86cac3b5ecae5f746a36a563049fc5fdc4395863dbd10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "





<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchusuarios/js/datoslaborales.js"), "html", null, true);
        echo "\"></script><form class=\"form-personal\"  action=\"";
        echo $this->env->getExtension('routing')->getPath("datoslaboralesguarda");
        echo "\"  method=\"post\"  id=\"formLaboral\" data-validate=\"parsley\" autocomplete=\"off\" >



<input type=\"hidden\" id=\"token\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">

    <div class=\"fila\">


        <div class=\"col1\">


 ";
        // line 20
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 21
            echo "
     
 ";
        } else {
            // line 24
            echo "
         
 ";
        }
        // line 26
        echo "          

            <label class=\"label-text2\" for=\"nombres\">Situación Laboral</label>
            
            <select class=\"data\" autofocus data-validation-engine=\"validate[required]\" id=\"combolaboral2\" name=\"situacion_laboral\" data-required=\"true\">
";
        // line 31
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "  

                    <option value=\"\"></option>

                    <option value=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "situacionlaboral", array()), "html", null, true);
            echo "\" selected>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "situacionlaboral", array()), "html", null, true);
            echo "</option>


                 ";
        } else {
            // line 39
            echo "                    <option value=\"\">Opción</option>


                    ";
        }
        // line 42
        echo " 

                    <option value=\"Dependiente - Renta Fija\">Dependiente - Renta Fija</option>
                    <option value=\"Dependiente - Renta Variable\">Dependiente - Renta Variable</option>
                    <option value=\"Independiente\">Independiente</option>
                    <option value=\"Sin trabajo\">Sin trabajo</option>
                    

                </select>

                <label class=\"label-text2\" for=\"empresa\">Empleador Empresa</label>

          ";
        // line 54
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "     
                <input type=\"text\" name=\"empresa\"  class=\"input-box2\" id=\"laboralempresa\" value=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "empleadorempresa", array()), "html", null, true);
            echo "\" data-required=\"true\" />



               ";
        } else {
            // line 60
            echo "

                <input type=\"text\" name=\"empresa\"  class=\"input-box2\" id=\"laboralempresa\" value=\"\" data-validation-engine=\"validate[required]\" data-required=\"true\" />


               ";
        }
        // line 65
        echo "     



                <input type=\"hidden\" value=\"\" name=\"iduser\">

                <label class=\"label-text2\" for=\"industria\">Industria</label>

         ";
        // line 73
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "  

                <input type=\"text\"  class=\"input-box2\" id=\"laboralIndustria\" name=\"industria\"  value=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "industria", array()), "html", null, true);
            echo "\" data-required=\"true\"  />


         ";
        } else {
            // line 79
            echo "
                <input type=\"text\"  class=\"input-box2\" id=\"laboralIndustria\" name=\"industria\"  value=\"\" data-required=\"true\"  />



            ";
        }
        // line 84
        echo "  


                <label class=\"label-text2\" for=\"ingreso\">Fecha de Ingreso</label>



              ";
        // line 91
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "  


                <input type=\"text\" name=\"fechaingreso\" id=\"fechaingresoLaboral\" title=\"Ingresar Día/Mes/Año\" class=\"input-box2\" id=\"laboralingresoa\" value=\"";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "fechaingreso", array()), "html", null, true);
            echo "\" data-required=\"true\" />


             ";
        } else {
            // line 98
            echo "

                <input type=\"text\" name=\"fechaingreso\" id=\"fechaingresoLaboral\" class=\"input-box2\" id=\"laboralingresoa\" value=\"\" data-required=\"true\" />



            ";
        }
        // line 104
        echo "  


                <label class=\"label-text2\" for=\"cargo\">Cargo Actual</label>


             ";
        // line 110
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "  



                <input type=\"text\" name=\"cargo\" id=\"cargoLaboral\"class=\"input-box2\" id=\"laboralcargo\"  value=\"";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "cargoactual", array()), "html", null, true);
            echo "\" data-required=\"true\" />




              ";
        } else {
            // line 120
            echo "


                <input type=\"text\" name=\"cargo\" id=\"cargoLaboral\"class=\"input-box2\" id=\"laboralcargo\"  value=\"\" data-required=\"true\" />





               ";
        }
        // line 129
        echo "  


                <label class=\"label-text2\" for=\"\">RUT Empresa</label>



                 ";
        // line 136
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "  




                <input type=\"text\" name=\"rutempresa\" class=\"input-box2\" id=\"laboralrutempresa\"  value=\"";
            // line 141
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "rutempresa", array()), "html", null, true);
            echo "\"  />



                  ";
        } else {
            // line 146
            echo "



                <input type=\"text\" name=\"rutempresa\" class=\"input-box2\" id=\"laboralrutempresa\"  value=\"\"  />






                  ";
        }
        // line 157
        echo "  






            </div>

            <div class=\"col1\">



                <label class=\"label-text2\" for=\"rutempresa\">Dirección</label>


                 ";
        // line 173
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo "  

                <input type=\"text\" name=\"direccion\" class=\"input-box2\" id=\"laboraldirecccion\" value=\"";
            // line 175
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "direccion", array()), "html", null, true);
            echo "\" data-required=\"true\" />             


                 ";
        } else {
            // line 179
            echo "


                <input type=\"text\" name=\"direccion\" class=\"input-box2\" id=\"laboraldirecccion\" value=\"\" data-required=\"true\" />             


                 ";
        }
        // line 185
        echo "  

                <label class=\"tipo\" for=\"region\">Región</label>

                <select class=\"data\" select=\"selected\" data-validation-engine=\"validate[required]\" id=\"ComboLaboralRegion\" name=\"region\" data-required=\"true\" >        

";
        // line 191
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 192
            echo "
                    <option value=\"";
            // line 193
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "region", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "region", array()), "html", null, true);
            echo "</option>
                     ";
        } else {
            // line 195
            echo "

                    <option value=\"\">Elige tu region</option>


                    ";
        }
        // line 200
        echo "  





               

 <option value=\"Región de Antofagasta\">Región de Antofagasta </option>


<option value=\"Región de Arica y Parinacota\">Región de Arica y Parinacota </option>


<option value=\"Región de Atacama\">Región de Atacama </option>


<option value=\"Región de Aysén del General Carlos Ibáñez del Campo\">Región de Aysén del General Carlos Ibáñez del Campo </option>


<option value=\"Región de Coquimbo\">Región de Coquimbo </option>


<option value=\"Región de la Araucanía\">Región de la Araucanía </option>


<option value=\"Región de Los Lagos\" >Región de Los Lagos </option>


<option value=\"Región de Los Lagos\">Región de Los Ríos </option>


<option value=\"Región de Magallanes\">Región de Magallanes y la Antártica Chilena </option>


<option value=\"Región de Tarapacá\">Región de Tarapacá </option>


<option value=\"Región de Valparaiso\">Región de Valparaiso </option>


<option value=\"Región del Bío-Bío\">Región del Bío-Bío </option>



<option value=\"Región del Libertador General Bernardo O Higgins\">Región del Libertador General Bernardo O Higgins </option>


<option value=\"Región del Maule\">Región del Maule </option>


<option value=\"Región Metropolitana\">Región Metropolitana </option>




                </select>


                <label class=\"tipo\" for=\"comuna\">Comuna</label>

                <select class=\"data\" id=\"ComboLaboralComuna\" name=\"comuna\" data-required=\"true\">

                 ";
        // line 263
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 264
            echo "                ";
            if ($this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "comuna", array())) {
                // line 265
                echo "



                    <option value=\"";
                // line 269
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "comuna", array()), "html", null, true);
                echo "\"selected> ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "comuna", array()), "html", null, true);
                echo " </option>




                    ";
            } else {
                // line 275
                echo "


                    <option value=\"\">Seleccione Comuna</option>



                    ";
            }
            // line 283
            echo "
  ";
        }
        // line 285
        echo "        



                </select>



                <label class=\"label-text2\" for=\"rutempresa\">Ciudad</label>


                 ";
        // line 296
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 297
            echo "


                <input type=\"text\" name=\"cuidad\" class=\"input-box2\" id=\"laboralcuidad\" value=\"";
            // line 300
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "ciudad", array()), "html", null, true);
            echo "\" data-required=\"true\" />                



                  ";
        } else {
            // line 305
            echo "

                <input type=\"text\" name=\"cuidad\" class=\"input-box2\" id=\"laboralcuidad\" value=\"\" data-required=\"true\" />                


                 ";
        }
        // line 311
        echo "


                <label class=\"label-text2\" for=\"telefono\">Teléfono</label>



                 ";
        // line 318
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 319
            echo "


                <input type=\"text\" class=\"input-box2\" autofocus name=\"telefono4\" id=\"laboraltelefono4\" value=\"";
            // line 322
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "telefono", array()), "html", null, true);
            echo "\" data-required=\"true\" >



                  ";
        } else {
            // line 327
            echo "


                <input type=\"text\" class=\"input-box2\" autofocus name=\"telefono4\" id=\"laboraltelefono4\" value=\"\" data-required=\"true\" >


                 ";
        }
        // line 334
        echo "


            </div>





            <div class=\"col1\">





                <!--<label class=\"tipo\" for=\"\"> Tipo de Sueldo</label>


                <select class=\"data\" name=\"tiporenta\" id=\"laboraltiporenta\" data-required=\"true\">


                    ";
        // line 355
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 356
            echo "

                    <option value=\"";
            // line 358
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "tiposueldo", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "tiposueldo", array()), "html", null, true);
            echo "</option>


                     ";
        } else {
            // line 362
            echo "
                    <option value=\"\">Opción</option>
                    




                     ";
        }
        // line 370
        echo "



                      <option value=\"Renta fija\">Sueldo Fijo</option>
                    <option value=\"Renta Variable\">Sueldo Variable</option>

                </select>

                <label class=\"tipo\" for=\"\"> Sueldo Fijo</label>
                
                  ";
        // line 381
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 382
            echo "                
                 <input type=\"text\"  class=\"input-box2\" value=\"";
            // line 383
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "sueldofijo", array()), "html", null, true);
            echo "\" name=\"sueldofijo\" id=\"sueldofijoLaboral\"  placeholder=\"\$\" title=\"Ingresa monto en pesos sin puntos ni comas\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\"  data-required=\"true\"  >

                
                 ";
        } else {
            // line 387
            echo "                 
                  <input type=\"text\"  class=\"input-box2\" value=\"\" name=\"sueldofijo\" id=\"sueldofijoLaboral\"  placeholder=\"\$\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\"  data-required=\"true\"  >

                
                 ";
        }
        // line 392
        echo "                
                
                
                
              

                <label class=\"tipo\" for=\"\"> Sueldo Variable</label>
                 ";
        // line 399
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 400
            echo "                
                <input type=\"text\"  class=\"input-box2\" value=\"";
            // line 401
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "rentavariable", array()), "html", null, true);
            echo "\" name=\"rentavariable\" id=\"rentavariableLaboral\" placeholder=\"\$\" title=\"Ingresa monto en pesos sin puntos ni comas\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\" >
                  
                
                  ";
        } else {
            // line 405
            echo "                
                
               <input type=\"text\"  class=\"input-box2\" value=\"\" name=\"rentavariable\" id=\"rentavariableLaboral\" placeholder=\"\$\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\" >
                
                 
                
                  ";
        }
        // line 412
        echo "                
                
                
                -->
                
                <section style=\"position: relative;width: 300px;height: 20px;margin-top: -5px;\">
                <label class=\"tipo\" for=\"\"> Sueldo Líquido (Mínimo 500.000 pesos)</label>
                
                
                 ";
        // line 421
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            // line 422
            echo "                
                <input type=\"text\"  class=\"input-box2\" value=\"";
            // line 423
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "independientesueldoliquido", array()), "html", null, true);
            echo "\" name=\"indepsueldoliquido\" id=\"indepsueldoliquidoLaboral\" placeholder=\"\$\" title=\"Ingresa monto en pesos sin puntos ni comas\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\" >

                      ";
        } else {
            // line 426
            echo "                 
                <input type=\"text\"  class=\"input-box2\" value=\"\" name=\"indepsueldoliquido\" id=\"indepsueldoliquidoLaboral\" placeholder=\"\$\" onkeyup=\"puntitos(this, this.value.charAt(this.value.length - 1))\" >

                
                         ";
        }
        // line 431
        echo "                
                </section>

            </div>
        </div>




        <div class=\"fila\">
            <div class=\"col1\"><br></div> 
            <div class=\"clearfix\"></div>
            <div class=\"col1\"> 
                <br>                   
                <button style=\"float:left;\" id=\"GuardaLaborl1\" class=\"classname\" value=\"Guardar\" type=\"submit\" onClick=\"\" >GUARDAR Y SIGUIENTE</button>
            </div>

        </div>


        <section id=\"respuestaLaboral3\">




        </section>




    </form>
";
    }

    public function getTemplateName()
    {
        return "BenchUsuariosBundle:Default:antecedentelaborales.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  634 => 431,  627 => 426,  621 => 423,  618 => 422,  616 => 421,  605 => 412,  596 => 405,  589 => 401,  586 => 400,  584 => 399,  575 => 392,  568 => 387,  561 => 383,  558 => 382,  556 => 381,  543 => 370,  533 => 362,  524 => 358,  520 => 356,  518 => 355,  495 => 334,  486 => 327,  478 => 322,  473 => 319,  471 => 318,  462 => 311,  454 => 305,  446 => 300,  441 => 297,  439 => 296,  426 => 285,  422 => 283,  412 => 275,  401 => 269,  395 => 265,  392 => 264,  390 => 263,  325 => 200,  317 => 195,  310 => 193,  307 => 192,  305 => 191,  297 => 185,  288 => 179,  281 => 175,  276 => 173,  258 => 157,  244 => 146,  236 => 141,  228 => 136,  219 => 129,  207 => 120,  198 => 114,  191 => 110,  183 => 104,  174 => 98,  167 => 94,  161 => 91,  152 => 84,  144 => 79,  137 => 75,  132 => 73,  122 => 65,  114 => 60,  106 => 55,  102 => 54,  88 => 42,  82 => 39,  73 => 35,  66 => 31,  59 => 26,  54 => 24,  49 => 21,  47 => 20,  36 => 12,  27 => 8,  19 => 2,);
    }
}
