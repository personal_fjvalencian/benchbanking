<?php

/* BenchUsuariosBundle:Default:datospersonales.html.twig */
class __TwigTemplate_46fff0767c26cbb1207fca31b5a0438dc5b50f2f40ea2fbc396de428742c97c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'containerDatosPersonales' => array($this, 'block_containerDatosPersonales'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchusuarios/js/datospesonales.js"), "html", null, true);
        echo "\"></script>


  ";
        // line 4
        $this->displayBlock('containerDatosPersonales', $context, $blocks);
        // line 377
        echo " ";
    }

    // line 4
    public function block_containerDatosPersonales($context, array $blocks = array())
    {
        echo " 
   

  <form class=\"form-personal\" id=\"DatosPersonalesForm\" method=\"post\" action=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("datospersonalesguarda");
        echo "\" data-validate=\"parsley\" autocomplete=\"off\">  
    <div class=\"fila\">

    

<input type=\"hidden\" id=\"token\" value=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
      
          <!-- <h1 class=\"doc\" style=\"margin-bottom:20px;\"><span class=\"orange\">Datos Personales</span></h1>-->
          <div class=\"col1\">
            <label class=\"label-text2\" >Nombres</label>
   

        
        <input type=\"text\"   autofocus class=\"input-box2\" id=\"nombreUsario\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array()), "html", null, true);
        echo "\" data-required=\"true\" />
        
        
              <label class=\"label-text2\" >Apellido Paterno</label>
        <input type=\"text\" name=\"apellido1\" class=\"input-box2\" id=\"apellido1\"  value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellido", array()), "html", null, true);
        echo "\"  data-required=\"true\" />
      
              <label class=\"label-text2\" >Apellido Materno</label>
        <input type=\"text\" name=\"apellido2\" class=\"input-box2\" id=\"apellido2\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellidoSeg", array()), "html", null, true);
        echo "\" data-required=\"true\"  />
          
              <label class=\"label-text2\" >RUT</label>
          <input type=\"text\"  class=\"input-box2\" id=\"rutpersonal\" name=\"rut\" value=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "rut", array()), "html", null, true);
        echo "\"   data-required=\"true\" />
          
              <label class=\"label-text2\" >Fecha de Nacimiento</label>
        <input type=\"text\" name=\"nacimiento\" class=\"input-box2\" id=\"nacimiento\" title=\"Ingresar Día/Mes/Año\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "fechaNacimiento", array()), "html", null, true);
        echo "\" data-required=\"true\"  />
                
              <label class=\"tipo\" >Sexo</label>


              <select class=\"data\" data-errormessage-value-missing=\"Debe seleccionar un opcion\" data-validation-engine=\"validate[required]\" name=\"sexo\"  id=\"sexoper\" data-required=\"true\" >
      
                  
              

";
        // line 43
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "sexo", array())) {
            // line 44
            echo "                  
           <option value=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "sexo", array()), "html", null, true);
            echo "\" selected> <h1  style=\"font-family: Arial; font-size: 8px;\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "sexo", array()), "html", null, true);
            echo "</h1> </option>
                  
   ";
        } else {
            // line 47
            echo "               
   
              
           <option value=\"\">Opción</option>
              
              
              ";
        }
        // line 53
        echo "  
        
        
      
        <option value=\"Hombre\">Hombre</option>
        <option value=\"Mujer\">Mujer</option>

 





   
          </select>
                

              
              

              <label class=\"tipo\" for=\"\">Nacionalidad</label>


          <select class=\"data\"  data-errormessage-value-missing=\"Debe seleccionar un opcion\" data-required=\"true\"  id=\"nacionalidad\">
                
         ";
        // line 78
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array())) {
            // line 79
            echo "              
              <option value=\"";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array()), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array()), "html", null, true);
            echo "</option>
              
              ";
        } else {
            // line 83
            echo "              
              
               <option value=\"\">Opción</option>
              
              
              ";
        }
        // line 89
        echo "         
          <option value=\"Chilena\">Chilena</option>
          <option value=\"Extranjero Residente\">Extranjero Residente</option>
          <option value=\"Extranjero No Residente\">Extranjero No Residente</option>

         
        </select>
            </div>
            
            <div class=\"col1\">
              <label class=\"tipo\" for=\"\">Estado Civil</label>
              <select class=\"data\" 
              data-errormessage-value-missing=\"Debe seleccionar un opcion\" data-required=\"true\"  name=\"estadociv\" id=\"estadociv\" data-validation-engine=\"validate[required]\">
               
             
                  
                  ";
        // line 105
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array())) {
            // line 106
            echo "                  
                  
                  
                 <option value=\"";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array()), "html", null, true);
            echo "</option>
                  
                  ";
        } else {
            // line 112
            echo "                 
                 

                    <option value=\"\">Opcion</option>
                  
                  ";
        }
        // line 118
        echo "
              
                <option value=\"Soltero\">Soltero</option>
                <option value=\"Separado\">Separado</option>
                <option value=\"Viudo\">Viudo</option>
                <option value=\"Casado sociedad conyugal\">Casado Sociedad Conyugal</option>
                <option value=\"Casado con separación de bienes\">Casado con Separación de Bienes</option>


               

             
        


        </select>
                
              <label class=\"tipo\" for=\"\">Nivel de Educación</label>


              
              <select class=\"data\" 
              data-errormessage-value-missing=\"Debe seleccionar un opcion\" id=\"niveleduc\"  name=\"niveleducacion\" data-validation-engine=\"validate[required]\">
   
                  
        ";
        // line 143
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "niveleducacion", array())) {
            // line 144
            echo "                  
                    
                   <option value=\"";
            // line 146
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "niveleducacion", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "niveleducacion", array()), "html", null, true);
            echo " </option>
                  
                  ";
        } else {
            // line 149
            echo "                  
                    
                    <option value=\"\">Opción </option>
                  
                  ";
        }
        // line 154
        echo "      
        <option value=\"Basica\">Básica</option> 
        <option value=\"medio\">Medio</option>
        <option value=\"Técnico\">Técnico</option>
        <option value=\"universitario\">Universitario</option>
        <option value=\"Postgrado\">Postgrado</option>


        

        </select>
              
              
              
              <label class=\"label-text2\" for=\"profesion\">Profesión u Oficio</label>
              
              
              <input type=\"text\" class=\"input-box2\"  name=\"profesion\" id=\"profesion2\"  value=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "profesion", array()), "html", null, true);
        echo "\"  data-required=\"true\" >
              
              <label class=\"label-text2\" for=\"universitad\">Universidad o Instituto</label>
              <input type=\"text\" class=\"input-box2\"  name=\"universidad\" id=\"universidad\" value=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "universidad", array()), "html", null, true);
        echo "\"  data-required=\"true\" data-required=\"true\"  >
              
              <label class=\"label-text2\" for=\"rut\">Celular</label>
              
              <input type=\"text\" class=\"input-box2\" title=\"Debes anteponer el 09 al número telefónico\"  name=\"telefono1\" id=\"celularPersonal\" value=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array()), "html", null, true);
        echo "\" data-required=\"true\"  >
            
            
            </div>
            <div class=\"col1\">
              <label class=\"tipo\" for=\"\">Tipo de Casa</label>
       
      <select class=\"data\" data-errormessage-value-missing=\"Debe seleccionar un opcion\" name=\"tipocasa\" id=\"tipocasa\" data-required=\"true\">
        
          
                      
          ";
        // line 189
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array())) {
            // line 190
            echo "          
          
         <option select=\"selected\" value=\"";
            // line 192
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array()), "html", null, true);
            echo " </option>
          
        

         ";
        } else {
            // line 197
            echo "          
          
           <option value=\"\">Opción </option>
           
           
          
          ";
        }
        // line 204
        echo "          
          
     
        <option value=\"Propia con Hipoteca\">Propia con Hipoteca</option>
        <option value=\"Propia sin Hipoteca \">Propia sin Hipoteca</option>
        <option value=\"Arrendada\">Arrendada</option>
        <option value=\"De la familia\">De la familia</option>

     
        </select>
              
              <label class=\"tipo\" for=\"\">N° de Dependientes </label>
              <input type=\"text\" class=\"input-box2\"  name=\"ndependientes\" id=\"ndependientes\"  value=\"";
        // line 216
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "ndependientes", array()), "html", null, true);
        echo "\" data-required=\"true\" data-type=\"number\" >
              
              <label class=\"tipo\" for=\"\"> Monto de Arriendo o Dividendo</label>
              
              <input type=\"text\" class=\"input-box2\"  id=\"montoardiv\" name=\"montoardiv\" title=\"Ingresa monto en pesos sin puntos ni comas\"  data-required=\"true\"  value=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "montoarriendo", array()), "html", null, true);
        echo "\" placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" >
              
              
              <label class=\"tipo\" >Dirección</label>
              
              
              <input type=\"text\" class=\"input-box2\" name=\"direccion\" id=\"direccionPerson\" value=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array()), "html", null, true);
        echo "\" data-required=\"true\" >
              
              
               <label class=\"tipo\" for=\"region\">Región</label>
              
               
               
               <select class=\"data\" select=\"selected\" data-required=\"true\" data-required=\"true\"  id=\"pesonalRegion\" name=\"regionP\">

 
                   
                   
<option value=\"\">Opción</option>
    

        ";
        // line 241
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array())) {
            // line 242
            echo "    
<option value=\"";
            // line 243
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array()), "html", null, true);
            echo "\" selected>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array()), "html", null, true);
            echo "</option>


     ";
        } else {
            // line 247
            echo "




      ";
        }
        // line 253
        echo "    

 <option value=\"\">Elige tu region</option>




 <option value=\"Región de Antofagasta\">Región de Antofagasta </option>


<option value=\"Región de Arica y Parinacota\">Región de Arica y Parinacota </option>


<option value=\"Región de Atacama\">Región de Atacama </option>


<option value=\"Región de Aysén del General Carlos Ibáñez del Campo\">Región de Aysén del General Carlos Ibáñez del Campo </option>


<option value=\"Región de Coquimbo\">Región de Coquimbo </option>


<option value=\"Región de la Araucanía\">Región de la Araucanía </option>


<option value=\"Región de Los Lagos\" >Región de Los Lagos </option>


<option value=\"Región de Los Lagos\">Región de Los Ríos </option>


<option value=\"Región de Magallanes\">Región de Magallanes y la Antártica Chilena </option>


<option value=\"Región de Tarapacá\">Región de Tarapacá </option>


<option value=\"Región de Valparaiso\">Región de Valparaiso </option>


<option value=\"Región del Bío-Bío\">Región del Bío-Bío </option>



<option value=\"Región del Libertador General Bernardo O Higgins\">Región del Libertador General Bernardo O Higgins </option>


<option value=\"Región del Maule\">Región del Maule </option>


<option value=\"Región Metropolitana\">Región Metropolitana </option>




        </select>
              
              
              <label class=\"tipo\" for=\"comuna\">Comuna</label>
              
              <select class=\"data\" name=\"comunaP\"  id=\"comunaP\" data-required=\"true\">
                  
                  
                  
        ";
        // line 317
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array())) {
            // line 318
            echo "    
<option value=\"";
            // line 319
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array()), "html", null, true);
            echo "\" selected>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array()), "html", null, true);
            echo "</option>


     ";
        } else {
            // line 323
            echo "
      <option value=\"\" selected  >Seleccione Comuna</option>



      ";
        }
        // line 329
        echo "        
              
              
            
  
                 
                 </option>

         
               
        </select>
</div>

       

  
    </div>
  
        
        <div class=\"fila\">
          <div class=\"col1\"><br></div>
          <div class=\"clearfix\"></div>
          <div class=\"col1\"> 
              
          <br>                   
             <button style=\"float:left;\" id=\"guardaDatosPersonales\" class=\"classname\" value=\"Guardar\" >GUARDAR Y SIGUIENTE</button>
          </div>
          
         
        <div id=\"RespuestaDatosPersonales\">
            
          
     
        </div>

         <div class=\"col1\"><br></div> 
        </div>

        
            



</form> 

<form id=\"formRegiones\" action=\"";
        // line 374
        echo $this->env->getExtension('routing')->getPath("comboRegion");
        echo "\"></form>


   ";
    }

    public function getTemplateName()
    {
        return "BenchUsuariosBundle:Default:datospersonales.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  532 => 374,  485 => 329,  477 => 323,  468 => 319,  465 => 318,  463 => 317,  397 => 253,  389 => 247,  380 => 243,  377 => 242,  375 => 241,  357 => 226,  348 => 220,  341 => 216,  327 => 204,  318 => 197,  308 => 192,  304 => 190,  302 => 189,  288 => 178,  281 => 174,  275 => 171,  256 => 154,  249 => 149,  241 => 146,  237 => 144,  235 => 143,  208 => 118,  200 => 112,  192 => 109,  187 => 106,  185 => 105,  167 => 89,  159 => 83,  151 => 80,  148 => 79,  146 => 78,  119 => 53,  110 => 47,  102 => 45,  99 => 44,  97 => 43,  84 => 33,  78 => 30,  72 => 27,  66 => 24,  59 => 20,  48 => 12,  40 => 7,  33 => 4,  29 => 377,  27 => 4,  20 => 1,);
    }
}
