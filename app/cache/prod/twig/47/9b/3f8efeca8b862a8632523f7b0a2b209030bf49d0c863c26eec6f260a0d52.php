<?php

/* BenchPaginasBundle:Default:credito1.html.twig */
class __TwigTemplate_479b3f8efeca8b862a8632523f7b0a2b209030bf49d0c863c26eec6f260a0d52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'formulario' => array($this, 'block_formulario'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<html xmlns=\"http://www.w3.org/1999/html\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
<META name=\"keywords\" content=\"credito hipotecario, credito consumo, simulador, tasa, banco\">
<title> BenchBanking │ Créditos Convenientes </title>
<META name=\"description\" content=\"Créditos más convenientes y mejor informados, compara las tasas antes de contratarlo\">
<meta name=\"google-site-verification\" content=\"4sibG4Sz2wGxTyteKnT1E8uJM6vt67srq5ANMPkQ5Vg\" />
<link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/ship.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/basic.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/flexslider.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/botCreditos.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/avgrund.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.flexslider-min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.carouFredSel-6.1.0-packed.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciador.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/botoneshead.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/html5.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/puntos.js"), "html", null, true);
        echo "\"></script>
<link rel=\"shortcut icon\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
<link rel=\"icon\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">

</head>




</ul>




<!-- Google Tag Manager -->
<noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-T8RRB4\"
                  height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T8RRB4');</script>
<!-- End Google Tag Manager -->




<body>
<section class=\"contenido99\">

";
        // line 53
        $this->env->loadTemplate("BenchPaginasBundle:Default:headerCreditos.html.twig")->display($context);
        // line 54
        echo "


";
        // line 57
        $this->displayBlock('header', $context, $blocks);
        // line 61
        echo "
<input type=\"hidden\" id=\"token\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
<div class=\"wrapper\" style=\"padding-top:20px;\">

<div class=\"container-sections\">

    <style>

    .boxRounder{

        width:982px;
        height:500px;
        position: relative;
        background-color:#e6e6e6;
        border-radius: 8px;



    }




    #formulario1{
        width: auto;
        height: auto;
        position: relative;
        margin-top: 2%;

    }

    .columna1{

        width:300px;
        height:auto;
        float:left;

        margin-top:10%;
        margin-left:10%;


    }

    .separador{
        width:30px;
        height: 20px;
        position: relative;
        float: left;




    }

    .lado{
        position: absolute;
        width:34px;
        height:34px;
        background-color:#cccccc;
        margin-top:0.9%;

        margin-left:0.1%;


    }

    .lado p{

        font-size:20px;
        margin-left:30%;
        position:relative;
        color:#ffffff;

    }

    .titulo{

        margin-left:4%;
        margin-top:3%;
        position:absolute;
        width: 400px;
        height: 20px;
        color:#e75221;



    }


    hr {
        color: #565656;
        height: 2px;
        width: 900px;
        margin-top: 20px;
        position: relative;
    }



    label{

        color:#565656;
        margin-left:1%;


    }

    .linea{
        width:982px;
        height:auto;
        background-color: #e75221;
        color:#e75221;
        position:absolute;

    }

    .linea h2{

        font-size:25px;
        color:#ffffff;
        margin-left:10px;

    }
    .datagrid{
        margin-left: 24px;
        margin-top: 20px;

    }


    #logoC{

        position: relative;
        width: 120px;
        height: 120px;
        margin-left: 85%;



    }


    .alerta{

        width: 650px;
        height: 20px;
        position:absolute;
        margin-top:3%;
    }

    .respuestaTexto2{
        font-size: 16px;
        color:#e75221;
        position: relative;
        width: 800px;
        height: auto;
        margin-left: 20px;
        margin-top: 10%;



    }



    .respuestaTexto2 li{

        margin-left:2%;
        margin-top:1%;
        margin-bottom:1%;
        width: 600px;

        list-style-image:url('../img/alert_icon.png');
    }

    .respuestaTexto2 li{

        margin-left:2%;
        width: 600px;
        list-style-image:url('../img/alert_icon.png');
    }

    #respuesta{

        width: 910px;
        margin-top:10%;
        height: 200px;
        background-color: #cccccc;
        position: relative;
        margin:auto;
        border-radius:6px ;
        border-color: 10px #ff8000;
        display: none;






    }

    .uf{

        width: 200px;
        height: 20px;
        position: relative;
        margin-left: 53%;


    }

    .linea h1{

        font-size: 18px;

    }

    .form-control{

        width: 300px;


    }

    .mensajes2{

        width: 724px;
        position: relative;
        margin-top:100px;
        background-color:#faf9f9;
        border-radius:6px;
        height: auto;
        position: relative;
        margin-top:5%;
        margin-left: 4%;
        border:1px solid #ff8000;
        border-color: solid 3px #009933;
        display: none;




    }





    /* ventana modal */

    .cajaModalCreditos{
        width:580px;
        height:200px;
        margin-top:100px;

        background-color: #ededed ;

    }



    .listaModalCreditos{

        width:500px;
        height:190px;
        position: relative;
        margin:auto;
        margin-top:1%;

    }

    .textoModal{

        color:#4b4b4b;
        font-size:13px;
        text-align:justify;
    }


    .textoModal2{

        color:#4b4b4b;
        font-size:13px;
        text-align:justify;
        font-weight:bold;



    }

    .chanchoS{

        width: 640px;
        height: 76px;
        position: relative;
        margin-top: -14%;
        margin-left: 23%;

    }

    .chanchoS span{
        color: #009933;
        font-size: 30px;
        margin-top: 2%;


        position: absolute;
        width: 220px;
        height: 50px;


    }
    </style>


 <div class=\"containerBench\">
 ";
        // line 377
        $this->displayBlock('formulario', $context, $blocks);
        // line 385
        echo "
 </div>





</div>

</div>

<div class=\"clearfix\"></div>



</body>





<script src=\"";
        // line 406
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 407
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 408
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.flexslider-min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 409
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.carouFredSel-6.1.0-packed.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciador.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 412
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/basic.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 413
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/parsley.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 414
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/parsley.extend.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 416
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.avgrund.js"), "html", null, true);
        echo "\"></script>




";
        // line 421
        $this->env->loadTemplate("BenchPaginasBundle:Default:footerCreditos.html.twig")->display($context);
        // line 422
        echo "

</section>

</html>





</body>

";
    }

    // line 57
    public function block_header($context, array $blocks = array())
    {
        // line 58
        echo "

";
    }

    // line 377
    public function block_formulario($context, array $blocks = array())
    {
        // line 378
        echo "





    ";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:credito1.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  553 => 378,  550 => 377,  544 => 58,  541 => 57,  525 => 422,  523 => 421,  515 => 416,  510 => 414,  506 => 413,  502 => 412,  497 => 410,  493 => 409,  489 => 408,  485 => 407,  481 => 406,  458 => 385,  456 => 377,  138 => 62,  135 => 61,  133 => 57,  128 => 54,  126 => 53,  95 => 25,  91 => 24,  87 => 23,  83 => 22,  79 => 21,  75 => 20,  71 => 19,  67 => 18,  63 => 17,  59 => 16,  55 => 15,  51 => 14,  47 => 13,  43 => 12,  39 => 11,  35 => 10,  31 => 9,  21 => 1,);
    }
}
