<?php

/* BenchDebesBundle:Default:debesLineCredito.html.twig */
class __TwigTemplate_b8e7c5806393aabd70caa8bf311fd093c363b647914be827dec38d374dd8cfc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "


<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchdebes/js/debeslinecredito.js"), "html", null, true);
        echo "\"></script>



\t\t<div class=\"fila\">
       
           <!--<h1 class=\"doc\"><span class=\"orange\">Tarjeta de Crédito</span></h1>-->
        \t<div class=\"col1\">
         

        

       
        
     
             
 

      <form  id=\"debesLineacredito1\" action=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("debesLineaCreditoSiNo");
        echo "\" method=\"POST\"  autocomplete=\"off\" >

      

<input type=\"hidden\" id=\"token\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
       <label class=\"label-text2\" for=\"rut\">¿Debes alguna Línea de Crédito?</label>
    
     

       
 
           ";
        // line 34
        if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "debesLineaCreditoSiNo", array()) == "SI")) {
            // line 35
            echo "         <p class=\"rad\">Si<input type=\"radio\" value=\"SI\"  class=\"debesTarjetaCreditoRadio\" name=\"debesTarjetaCreditoRadio\" id=\"debesTarjetaCreditoRadio\"  checked>
         No<input type=\"radio\" value=\"NO\" name=\"debesTarjetaCreditoRadio\"  id=\"debesTarjetaCreditoRadio\" class=\"debesTarjetaCreditoRadio\"></p> 
   
             
        
  ";
        } else {
            // line 41
            echo "
          
        
             <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"debesTarjetaCreditoRadio\" name=\"debesTarjetaCreditoRadio\" id=\"debesTarjetaCreditoRadio\" >
         No<input type=\"radio\" value=\"NO\" name=\"debesTarjetaCreditoRadio\" id=\"debesTarjetaCreditoRadio\" class=\"debesTarjetaCreditoRadio\" checked ></p> 
  
          
       ";
        }
        // line 48
        echo "    
          
          
          


<div style=\"float:left; height:10px; width:100%;\"></div>
    <button style=\"float:left;\" class=\"classname\" value=\"Agregar\" id=\"guardatarjetaC1\" type=\"submit\">GUARDAR Y SIGUIENTE</button> 

               </form>



                    

 <form class=\"form-personal\" id=\"debesLineacredito2\" action=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("lineacreditoguarda");
        echo "\" method=\"post\">
     
     

             
               
      
               <label class=\"label-text2\" for=\"institucion\">Institución</label>
               <input type=\"text\"class=\"input-box2\" name=\"institucion\" id=\"debesTarjetainstitucion\" data-validation-engine=\"validate[required]\" data-required=\"true\">
              
               <label class=\"label-text2\" for=\"rut\">Monto Aprobado</label>
               <input type=\"text\" class=\"input-box2\" name=\"montoAprobado\" id=\"debesTarjetamontoAprobado\" placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" data-validation-engine=\"validate[required]\" data-required=\"true\">
              
         
               <label class=\"label-text2\" for=\"rut\">Monto Utilizado</label>
               <input type=\"text\" class=\"input-box2\" id=\"debesTarjetamontoUtilizado\" name=\"debesTarjetamontoUtilizado\" placeholder=\"\$\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" data-validation-engine=\"validate[required]\" data-required=\"true\">
                
           
               <label class=\"label-text2\" for=\"rut\">Vencimiento</label>
               <input type=\"text\" class=\"input-box2\" id=\"debesTarjetavencimiento\" name=\"debesTarjetavencimiento\"  data-validation-engine=\"validate[required]\" data-required=\"true\" >
                

                
           
              
              
              
            </div>
            
                    <div class=\"clearfix\"></div>
                   
       \t  <div class=\"col2\">
          <button style=\"float:left;margin-top:10px;\" id=\"guardatarjetaC\" class=\"classname\" value=\"Agregar\" type=\"submit\">AGREGAR</button>
           <div id=\"SiguienteDebesTarjetaCredito\" class=\"classname\" style=\"margin-top:10px; margin-left: 1%; position: relative;width: 80px; float: left;\" >SIGUIENTE</div>
        </div>
                    
        </form>
        <div class=\"flix-clear\"></div>

<div class=\"clearfix\"></div>
      <br>
            
            <div class=\"col2\">
          <table class=\"table table-striped\" id=\"respuesta3\">
\t\t\t    <thead style=\"background:#F60\">
          <tr>
\t\t\t
\t\t\t\t\t<td><p style=\"text-align:center;color:#FFF;\">Institución</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#FFF;\">Monto Aprobado</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#FFF;\">Monto Utilizado</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#FFF;\">Vencimiento</p></td>
          <td><p style=\"text-align:center;color:#FFF;\">Eliminar</p></td>
\t\t\t\t  </tr>
\t\t\t\t</thead>
      <tbody style=\"background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;\">\t    
       ";
        // line 118
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["debesLineaCredito"]);
        foreach ($context['_seq'] as $context["_key"] => $context["debesLineaCredito"]) {
            echo "  
            
              
                


     
   

        
          <tr  id=\"itemLinea_";
            // line 128
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t
\t\t\t\t\t<td><p style=\"text-align:center;color:#666;\">";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "institucion", array()), "html", null, true);
            echo "</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#666;\">";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoAprobado", array()), "html", null, true);
            echo "</p></td>
\t\t\t\t\t<td><p style=\"text-align:center;color:#666;\">";
            // line 132
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoUtilizado", array()), "html", null, true);
            echo "</p></td>
\t\t\t\t  <td><p style=\"text-align:center;color:#666;\">";
            // line 133
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "vencimientofinal", array()), "html", null, true);
            echo "</p></td>
          <td><p style=\"text-align:center;color:#666;\"> <a href=\"#\" class=\"del_button\" id=\"del-";
            // line 134
            echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "id", array()), "html", null, true);
            echo "\">eliminar</a> </p> </td>

          </tr>

          
          

         ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesLineaCredito'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 142
        echo "\t</tbody>


                      
\t\t\t 
             </table>
              
              
              
              
              
              
              
             
              
              
             
            
            </div>
            
            

\t
\t\t</div>
\t
        

        
            



</form>

<form id=\"debeslineacreditoForm\" action=\"";
        // line 176
        echo $this->env->getExtension('routing')->getPath("deldebeslineaCredito");
        echo "\"></form>";
    }

    public function getTemplateName()
    {
        return "BenchDebesBundle:Default:debesLineCredito.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 176,  207 => 142,  193 => 134,  189 => 133,  185 => 132,  181 => 131,  177 => 130,  172 => 128,  157 => 118,  99 => 63,  82 => 48,  72 => 41,  64 => 35,  62 => 34,  52 => 27,  45 => 23,  24 => 5,  19 => 2,);
    }
}
