<?php

/* BenchAdminBundle:Default:vistaConpedidos.html.twig */
class __TwigTemplate_5f95f4265cde437a3d55a8810c8f1f6f422a84b1beb3718e95487e33c720795f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\" />
  <title>Administrador Bench Banking </title>
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
  <meta name=\"description\" content=\"\" />
  <meta name=\"author\" content=\"\" />
<meta name=\"robots\" content=\"noindex\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, FOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"INDEX, NOFOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, NOFOLLOW\">


  <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"main-theme-script\" />
  <link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-specific-script\" />
  <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/mask.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.js"), "html", null, true);
        echo "\"></script>





     <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

     <script src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.columnFilter.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>


    
     

  
  <link href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/css/datepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  


  <link rel=\"stylesheet\" type=\"text/css\" media=\"screen,projection\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />
  

  <link type=\"text/css\" href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.intenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />   

 
  <link href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/simplenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  

  <link rel=\"shortcut icon\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/favicon.ico"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-144-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-114-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-72-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-57-precomposed.png"), "html", null, true);
        echo "\" />
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>
<body id=\"dashboard\" class=\"hidden\">






 <div id=\"div_carga\">
   <img id=\"cargador\" src=\"./img/loader.gif\"/>
   </div>

<!-- Top navigation bar -->
<div class=\"navbar navbar-fixed-top\">
  <div class=\"navbar-inner\">
    <div class=\"container-fluid\">
      <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </a>
      <a class=\"brand\" href=\"index.html\">Bench banking</a>
      <div class=\"btn-group pull-right\">
        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
          <i class=\"icon-user\"></i> 
          <span class=\"caret\"></span>
        </a>
        <ul class=\"dropdown-menu\">
        
          <li><a href=\"login.html\">Logout</a></li>
        </ul>
      </div>
      <div class=\"nav-collapse\">
          
          
        <ul class=\"nav\">
        
              
          <li class=\"dropdown\">
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  Menu
                      <b class=\"caret\"></b>
                </a>
                <ul class=\"dropdown-menu\">
                \t  <li class=\"nav-header\">Main</li>        
          <li class=\"active\"><a href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("usuarioPedido");
        echo "\"><i class=\"icon-home\"></i> Home </a></li>
          <li><a href=\"";
        // line 98
        echo $this->env->getExtension('routing')->getPath("vistaArchivados");
        echo "\"><i class=\"icon-edit\"></i>Ver archivados /  Usuarios</a></li>
        
        
         
                </ul>
          </li>
       
        </ul>
      </div>
    </div>
  </div>
</div>
  
<div class=\"container-fluid\">
  <div class=\"row-fluid\">
    
    <!-- Bread Crumb Navigation -->
  
      

   
  
         
         <!-- Portlet Set 5 -->
             
         <h6><a href=\"";
        // line 123
        echo $this->env->getExtension('routing')->getPath("vistaArchivados");
        echo "\">Ver archivados /  Usuarios</a></h6>
         <h6><a href=\"";
        // line 124
        echo $this->env->getExtension('routing')->getPath("index2");
        echo "\">Ver todos / Usuarios</a></h6>
             

        
\t  <!-- Table -->
      
      
         \t <!-- Portlet: Browser Usage Graph -->
            
                 <h4 class=\"box-header round-top\"></h4>
                 <H2> Usuarios Con Pedidos </H2>
                  <a class=\"box-btn\" title=\"close\"><i class=\"icon-remove\"></i></a>
                     <a class=\"box-btn\" title=\"toggle\"><i class=\"icon-minus\"></i></a>  
                 
                     
              </h4>         
              
      
      

                     <section id=\"contenido\">
                         
                         
          
                           
<link href=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/table.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

<link href=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/css/smoothness/jquery-ui-1.8.4.custom.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
<script type=\"text/javascript\" src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.datatable.inc.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>    
   


";
        // line 158
        echo $this->env->getExtension('DatatableBundle')->datatable(array("edit_route" => "add", "delete_route" => "cotizar", "otra_route" => "CrearPdf", "ver_route" => "ver", "archivar_route" => "archivarUsuario2", "UpDicom_url" => "UpDicom", "Up_url" => "otrosUpload", "js" => array("sAjaxSource" => $this->env->getExtension('routing')->getPath("grid3"))));
        // line 174
        echo "


    <script>

  
    
</script>                     




                     </section>










                  </div>
              </div>
            </div><!--/span-->




   </div>
      </div>  
 
    </div><!--/span-->
  </div><!--/row-->



<div style=\"clear:both;\"></div>









 

     ";
        // line 224
        $this->env->loadTemplate("BenchAdminBundle:Default:modalSeleccionBancos.html.twig")->display($context);
        // line 225
        echo "

     


 <script>
     \$( document ).ready(function() {


     });



 </script>




 <footer>
    <p>&copy; Bench Banking</p>
  </footer>
    <div id=\"box-config-modal\" class=\"modal hide fade in\" style=\"display: none;\">
      <div class=\"modal-header\">
        <button class=\"close\" data-dismiss=\"modal\">×</button>
        <h3></h3>
      </div>
      <div class=\"modal-body\">
        <p></p>
      </div>
      <div class=\"modal-footer\">
        <a href=\"#\" class=\"btn btn-primary\" data-dismiss=\"modal\"></a>
        <a href=\"#\" class=\"btn\" data-dismiss=\"modal\"></a>
      </div>
    </div>
</div><!--/.fluid-container-->

    
  
    <script src=\"";
        // line 263
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.core.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 264
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 265
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 266
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 267
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
    
  
    <script src=\"";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js"), "html", null, true);
        echo "\"></script>

 
    <script src=\"";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 275
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/bootbox/bootbox.min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 277
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>

\t\t
    <!-- jQuery Cookie -->    
    <script src=\"";
        // line 281
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.cookie/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Full Calender -->
    <script type='text/javascript' src='";
        // line 284
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
    
    <!-- CK Editor -->
\t<script type=\"text/javascript\" src=\"";
        // line 287
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Chosen multiselect -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 290
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script>  
    
    <!-- Uniform -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 293
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
    
    
\t
    <script src=\"";
        // line 297
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/simplenso/simplenso.js"), "html", null, true);
        echo "\"></script>
  </body>


                 
                         
                ";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:vistaConpedidos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  425 => 297,  418 => 293,  412 => 290,  406 => 287,  400 => 284,  394 => 281,  387 => 277,  382 => 275,  378 => 274,  372 => 271,  368 => 270,  362 => 267,  358 => 266,  354 => 265,  350 => 264,  346 => 263,  306 => 225,  304 => 224,  252 => 174,  250 => 158,  243 => 154,  239 => 153,  235 => 152,  231 => 151,  226 => 149,  198 => 124,  194 => 123,  166 => 98,  162 => 97,  113 => 51,  109 => 50,  105 => 49,  101 => 48,  97 => 47,  91 => 44,  85 => 41,  79 => 38,  72 => 34,  62 => 27,  57 => 25,  48 => 19,  44 => 18,  40 => 17,  36 => 16,  19 => 1,);
    }
}
