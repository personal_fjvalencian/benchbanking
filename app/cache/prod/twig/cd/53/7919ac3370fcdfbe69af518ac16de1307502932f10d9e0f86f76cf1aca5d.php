<?php

/* BenchUploadBundle:Default:upload3.html.twig */
class __TwigTemplate_cd537919ac3370fcdfbe69af518ac16de1307502932f10d9e0f86f76cf1aca5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "



<br>


<form  method=\"post\" action=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("uploadfijo3ultimas");
        echo "\">
\t<div id=\"uploader\" style=\"width:900px;height:200px;margin-top: 80px;\">
\t\t<p></p>
\t</div>
</form>
<script type=\"text/javascript\">
    

\$(function() {
\t\$(\"#uploader\").plupload({
\t\t// General settings
\t\truntimes : 'html5,flash',
\t\turl : 'uploadfijo3ultimas',
\t\tmax_file_size : '1000mb',
\t\tmax_file_count: 20, // user can add no more then 20 files at a time
\t\tchunk_size : '1mb',
                autostart : true,
\t\trename: true,
\t\tmultiple_queues : true,

\t\t// Resize images on clientside if we can
\t\t// resize : {width : 320, height : 240, quality : 90},
\t\t
\t\t// Rename files by clicking on their titles
\t\trename: true,
\t\t
\t\t// Sort files
\t\tsortable: true,

\t\t// Specify what files to browse for
\t\tfilters : [
\t\t\t{title : \"Image files\", extensions : \"jpg,gif,png\"},
\t\t\t{title : \"Zip files\", extensions : \"zip,avi,pdf,doc,pdf\"}
\t\t],

\t\t// Flash settings
\t\tflash_swf_url : \"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.flash.swf"), "html", null, true);
        echo "\",

\t\t// Silverlight settings
\t\tsilverlight_xap_url : \"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchupload/js/plupload.silverlight.xap"), "html", null, true);
        echo "\",
\t});

\t// Client side form validation
\t\$('form').submit(function(e) {
        var uploader = \$('#uploader').plupload('getUploader');

        // Files in queue upload them first
        if (uploader.files.length > 0) {
            // When all files are uploaded submit form
            uploader.bind('StateChanged', function() {
                if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                    \$('form')[0].submit();
                }
            });
                
            uploader.start();
        } else
            alert('You must at least upload one file.');

        return false;
    });
\t 

});


</script>";
    }

    public function getTemplateName()
    {
        return "BenchUploadBundle:Default:upload3.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 47,  67 => 44,  28 => 8,  19 => 1,);
    }
}
