<?php

/* AliDatatableBundle:Renderers:_actions.html.twig */
class __TwigTemplate_bfd97518fbb69dabb0f83edbe979e010c51ffafb7a927b444cfa9f9f25f82ba5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["delete_route"]) ? $context["delete_route"] : null), array("id" => (isset($context["dt_item"]) ? $context["dt_item"] : null))), "html", null, true);
        echo "\" method=\"post\" style=\"float:right\">
    ";
        // line 2
        echo strtr((isset($context["delete_form_prototype"]) ? $context["delete_form_prototype"] : null), array("@id" => (isset($context["dt_item"]) ? $context["dt_item"] : null)));
        echo "
    <a class=\"button-delete\" href=\"#\" type=\"submit\">Delete</a>
</form>
<a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["edit_route"]) ? $context["edit_route"] : null), array("id" => (isset($context["dt_item"]) ? $context["dt_item"] : null))), "html", null, true);
        echo "\">edit</a>";
    }

    public function getTemplateName()
    {
        return "AliDatatableBundle:Renderers:_actions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 5,  24 => 2,  19 => 1,);
    }
}
