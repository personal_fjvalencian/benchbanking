<?php

/* BenchRegistrosBundle:Default:registro.html.twig */
class __TwigTemplate_cc123321ea6d4b325b382c5e64309fbe39605bd8417c815f8e67b34a6f2dab58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
  


<html>

<head>
        
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">


<script src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/in2.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/registro.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/parsley.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/parsley.extend.js"), "html", null, true);
        echo "\"></script>


<link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
<link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/ship.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/basic.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
<link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/flexslider.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
<link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/form.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">








 ";
        // line 34
        $this->env->loadTemplate("BenchPaginasBundle:Default:header.html.twig")->display($context);
        // line 35
        echo "    ";
        $this->displayBlock('header', $context, $blocks);
        // line 40
        echo "

 <div class=\"spinner\">
      <div class=\"bounce1\"></div>
      <div class=\"bounce2\"></div>
      <div class=\"bounce3\"></div>
    </div>





<div class=\"wrapper\">
    <div class=\"wrapper-border-radius\">
    <div class=\"title-section\">Ingresa tus datos  s personales</div>
    
<div class=\"icon-registro\"></div>
<!-- formulario de registro -->
<form class=\"form-box\" style=\"padding:0 0 5% 10%;\" action=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("guardaRegistro");
        echo "\" id=\"FormRegistro\" method=\"POST\" data-validate=\"parsley\" autocomplete=\"off\" >
<label class=\"label-text\"  >RUT:</label>
\t\t<input type=\"text\" name=\"rutRegistro\"  class=\"input-box\" id=\"rut2\"  data-required=\"true\"data-minlength-message=\"Rut invalido\"   />
\t\t

<input type=\"hidden\" id=\"token\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">

    <label class=\"label-text\" for=\"nombres\">Nombres:</label>
\t\t<input type=\"text\" name=\"nombre\" id=\"nombreRegistro\" class=\"input-box\" data-required=\"true\"   id=\"nombres\" />
               
                
              
                
                
<label class=\"label-text\" >Apellido Paterno:</label>
\t\t<input type=\"text\" name=\"aapellido1\" id=\"apellido1Registro\" class=\"input-box\" data-required=\"true\"  />
        

 <label class=\"label-text\" >Apellido Materno:</label>
    <input type=\"text\" name=\"apellido2\" class=\"input-box\" id=\"apellido2Registro\" data-required=\"true\"  />
        

<label class=\"label-text\" for=\"email\">E-mail:</label>
\t  <input type=\"text\" name=\"email\" class=\"input-box\"  id=\"emailRegistro\"  value=\"";
        // line 81
        echo twig_escape_filter($this->env, (isset($context["EmailUsuario"]) ? $context["EmailUsuario"] : null), "html", null, true);
        echo "\" data-required=\"true\"  data-type=\"email\" />



<label class=\"label-text\" for=\"email\">Teléfono :</label>
 <input type=\"text\" name=\"tele\" class=\"input-box\"  id=\"telefono\"  data-required=\"true\"  data-type=\"telefono\" />





    <label class=\"label-text\" for=\"contrasena\">Contraseña:</label>
\t\t<input type=\"password\" name=\"pas1\" class=\"input-box\" id=\"pas1\"  data-required=\"true\" />

                
<label class=\"label-text\" for=\"conf_contrasena\">Confirmar Contraseña:</label>

<input type=\"password\" name=\"password2\" class=\"input-box\" data-required=\"true\"   id=\"pas2\" data-equalto=\"#pas1\"  />
        
                
    <div class=\"clearfix\"></div>                     

            
    
<input  style=\"float:left;\"  type=\"submit\" id=\"RegistroButon\"  class=\"classname\" value=\"Guardar Registro\">

<div class=\"flix-clear\"></div>

<div class=\"alert-form4\"  id=\"rutnovalido\">
<img src=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/alert_icon.png"), "html", null, true);
        echo "\">
Rut no válido

</div>

<div id=\"alertregistro\" class=\"alert-form4\" ></div>
</form>


 


</div>  



<!-- formulario de registro -->
<div class=\"flix-clear\"></div>
    
                 
</div>




<div class=\"clearfix\"></div>

           


  
</body>
</html>
";
    }

    // line 35
    public function block_header($context, array $blocks = array())
    {
        // line 36
        echo "    
    
    
    ";
    }

    public function getTemplateName()
    {
        return "BenchRegistrosBundle:Default:registro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 36,  212 => 35,  174 => 110,  142 => 81,  121 => 63,  113 => 58,  93 => 40,  90 => 35,  88 => 34,  76 => 25,  72 => 24,  68 => 23,  64 => 22,  60 => 21,  54 => 18,  50 => 17,  45 => 15,  41 => 14,  37 => 13,  33 => 12,  20 => 1,);
    }
}
