<?php

/* BenchAdminBundle:plantillas:metlife.html.twig */
class __TwigTemplate_88883aa6d827f92f22c6a8c22545d41af450930d6c399465dd78dec1f802db23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
    <head>
        <meta charset=\"UTF-8\">
        <title>Plantilla metlife</title>
        <style>
            body{
                
                font-family: verdana;
                font-size:12px;
            }
            
            .tabla{
                width: 690px;
                color: #2c7ac8;
                border: solid;
                border-color: #2c7ac8;
            }
            
            .tablaTitulo{
                width: 710px;
            }
            .input{
                color: #2c7ac8;
                width: 80%;
                height: 20px;
                border-style: solid;
                border-width: 1px;
                position: relative;
                float: left;
            }
        </style>
            
    </head>
    <body>
        <table class=\"tablaTitulo\" id=\"logo\">
            <tbody>
                <tr>
                    <td align=\"right\">
                        <img src=\"http://www.mediadiv.cl/bench/img/metlife.jpg\" width=\"200\" height=\"40\">
                    </td>
                </tr>
            </tbody>
        </table>
        <table>
            <tbody>
                <tr>
                    <td>
         
                    </td>
                    <td>
                       
                    </td>
                    <td>
     
                    </td>
                </tr>
            </tbody>
        </table>
        <table class=\"tabla\" style=\"\">
            <thead>
            <th colspan=\"5\" align=\"left\">
                    Antecedentes Personales
                </th>
            </thead>
            <tbody>
                <tr>
                    <td>Deudor</td>
                    <td colspan=\"2\">Codeudor</td>
                    <td colspan=\"2\">Aval</td>
                </tr>
                <tr>
                    <td><div class=\"input\"></div></td>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:320px;\">&nbsp;</div></td>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Apellido paterno  </td>
                    <td>Apelllido Materno </td>
                    <td>Nombres           </td>
                    <td colspan=\"2\">Rut</td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;";
        // line 84
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellido", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellido", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
        // line 85
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellidoseg", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    <td><div class=\"input\" style=\"width:162px; \">&nbsp; ";
        // line 86
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nombre", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    <td colspan=\"2\"><div class=\"input\"  style=\"width:202px;\">&nbsp;";
        // line 87
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "rut", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "rut", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                </tr>
                <tr>
                    <td colspan=\"5\">Dirección particular(calle, número, departamento, casa, etc)</td>
                </tr>
                <tr>
                    <td colspan=\"5\"><div class=\"input\" style=\"width:660px;\">&nbsp; ";
        // line 93
        if ($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "direccion", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                </tr>
                <tr>
                    <td colspan=\"2\">Comuna</td>
                    <td colspan=\"2\">Region</td>

                    <td colspan=\"1\">Teléfono </td>
                </tr>
                <tr>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 102
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "comuna", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 103
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "region", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    <td colspan=\"2\" ><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 104
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array()), "html", null, true);
        }
        echo "</div></td>
                </tr>



                <tr>
                    <td colspan=\"2\">Fecha de Nacimiento</td>
                    <td colspan=\"2\">Sexo</td>

                    <td colspan=\"1\">Nacionalidad </td>
                </tr>
                <tr>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 116
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "fechanacimiento", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 117
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "sexo", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 118
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "nacionalidad", array()), "html", null, true);
        }
        echo "</div></td>
                </tr>



                <tr>
                    <td colspan=\"2\">Estado Civil </td>
                    <td colspan=\"2\">Profesión u Oficio</td>
                    <td colspan=\"1\">Nivel de Educación </td>


                </tr>
                <tr>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 131
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "estadocivil", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                    <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 133
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "profesion", array()), "html", null, true);
        }
        echo "</div></td>


                    <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 136
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "niveleducacion", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

                </tr>




                <tr>
                    <td colspan=\"2\">Universidad o Instituto </td>
                    <td colspan=\"2\">Celular </td>
                    <td colspan=\"1\">Tipo de Casa</td>


                </tr>
                <tr>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 151
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "universidad", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                    <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 153
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "telefono", array()), "html", null, true);
        }
        echo "</div></td>


                    <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 156
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tipocasa", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

                </tr>


                <tr>
                    <td colspan=\"2\">N° de Dependientes  </td>
                    <td colspan=\"2\">Monto de Arriendo o Dividendo </td>



                </tr>


                <tr>
                    <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 171
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "ndependientes", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                    <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 173
        if ((isset($context["usuario"]) ? $context["usuario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "montoarriendo", array()), "html", null, true);
        }
        echo "</div></td>

                </tr>

            </tbody>
        </table>


        <br/>
        <br/>



        <table class=\"tabla\">
            <tbody>
                <tr>
                    <td width=\"302\">
                        <table width=\"98%\">
                            <thead>
                            <th colspan=\"3\" align=\"left\">
                                Antecedentes Laborales
                            </th>
                            </thead>
                            <tbody>

                            <tr>
                                <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 199
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "situacionlaboral", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                                <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 201
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "empleadorempresa", array()), "html", null, true);
        }
        echo "</div></td>


                                <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 204
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "industria", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

                            </tr>




                            <tr>
                                <td colspan=\"2\">Situación Laboral </td>
                                <td colspan=\"2\">Empleador Empresa </td>
                                <td colspan=\"1\">Industria</td>


                            </tr>



                            <tr>
                                <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 222
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "fechaingreso", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                                <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 224
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "cargoactual", array()), "html", null, true);
        }
        echo "</div></td>


                                <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 227
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "rutempresa", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

                            </tr>




                            <tr>
                                <td colspan=\"2\">Fecha de Ingreso </td>
                                <td colspan=\"2\">Cargo Actual</td>
                                <td colspan=\"1\">RUT Empresa </td>


                            </tr>



                            <tr>
                                <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 245
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "ciudad", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                                <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 247
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "telefono", array()), "html", null, true);
        }
        echo "</div></td>


                                <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 250
        if ((isset($context["antecedentes"]) ? $context["antecedentes"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["antecedentes"]) ? $context["antecedentes"] : null), "independientesueldoliquido", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

                            </tr>




                            <tr>
                                <td colspan=\"2\">Ciudad </td>
                                <td colspan=\"2\">Teléfono </td>
                                <td colspan=\"1\">Sueldo Líquido  </td>


                            </tr>







                            </tbody>
                        </table>
                    </td>

            </tbody>
        </table>


        <br/>


        <br>
        <br>



        <table class=\"tabla\">
            <thead>
            <th colspan=\"4\" align=\"left\">Antecdentes Conyuge</th>
            </thead>
            <tbody>



            <tr>
                <td colspan=\"2\">Nombres </td>
                <td colspan=\"2\">Apellido Paterno </td>
                <td colspan=\"1\">Apellido Materno </td>


            </tr>



            <tr>
                <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 306
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "nombre", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 308
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellido", array()), "html", null, true);
        }
        echo "</div></td>


                <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 311
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

            </tr>



            <tr>
                <td colspan=\"2\">RUT </td>
                <td colspan=\"2\">Fecha de Nacimiento </td>
                <td colspan=\"1\">Nacionalidad </td>


            </tr>



            <tr>
                <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 328
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rut", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 330
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "fechanacimiento", array()), "html", null, true);
        }
        echo "</div></td>


                <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 333
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "nacionalidad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

            </tr>




            <tr>
                <td colspan=\"2\">Nivel de Educación </td>
                <td colspan=\"2\">Profesión  </td>
                <td colspan=\"1\">Universidad o Instituto </td>


            </tr>



            <tr>
                <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 351
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "niveleducacion", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 353
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "profesion", array()), "html", null, true);
        }
        echo "</div></td>


                <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 356
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "universidad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

            </tr>




            <tr>
                <td colspan=\"2\">Actividad </td>
                <td colspan=\"2\">Cargo Actual  </td>
                <td colspan=\"1\">Antigüedad Laboral  </td>


            </tr>



            <tr>
                <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 374
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "actividad", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>

                <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 376
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "cargoactual", array()), "html", null, true);
        }
        echo "</div></td>


                <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 379
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "antiguedadlaboral", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

            </tr>


            <tr>
                <td colspan=\"2\">Renta Líquida </td>
                <td colspan=\"2\">Empresa Trabajo  </td>
                <td colspan=\"1\">Teléfono  </td>


            </tr>



            <tr>
                <td colspan=\"2\"><div class=\"input\" style=\"width:210px;\">&nbsp;  ";
        // line 395
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array()), "html", null, true);
            echo " ";
        }
        echo "     </div></td>
                 <td colspan=\"2\"><div class=\"input\" style=\"width:190px;\">&nbsp; ";
        // line 396
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "empresa", array()), "html", null, true);
        }
        echo "</div></td>
                  <td colspan=\"2\"><div class=\"input\" style=\"width:200px;\">&nbsp; ";
        // line 397
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "telefono", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>

            </tr>


            </tbody>
        </table>

        <br/><br/><br/> <br/><br/><br/><br/><br/><br/><br/>


        <table class=\"tabla\">
            <tbody>
            <tr>
                <td colspan=\"7\">
                    Desempeña usted algun cargo público o estatal
                </td>
            </tr>
            <tr>
                <td colspan=\"7\">
                    <div class=\"input\"></div>
                </td>
            </tr>
            <tr>
                <td colspan=\"2\">Cargo que ocupa el solicitante</td>
                <td colspan=\"3\">Permanencia en el cargo</td>
                <td colspan=\"2\">Permanencia en la empresa</td>
            </tr>
            <tr>
                <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                <td colspan=\"3\"><div class=\"input\">&nbsp;</div></td>
                <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
            </tr>
            <tr>
                <td colspan=\"7\">Cargo empleos anteriores</td>
            </tr>
            <tr>
                <td>Desde</td>
                <td>Hasta</td>
                <td>Empleador/Actividad</td>
                <td>Giro</td>
                <td>Teléfono</td>
                <td>Cargo</td>
                <td>Renta liquida</td>
            </tr>
            <tr>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
            </tr>
            <tr>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
                <td><div class=\"input\">&nbsp;</div></td>
            </tr>
            </tbody>
        </table>





        <table class=\"tabla\">
            <thead>
            <th colspan=\"5\" align=\"left\">Grupo familiar</th>
            </thead>
            <tbody>
                <tr>
                    <td>Nombre</td>
                    <td>Parentesco</td>
                    <td>Edad</td>
                    <td>Actividad/Establ.Educacional</td>
                    <td>Renta liquida</td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp; ";
        // line 480
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "nombre", array()), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellido", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "apellidoseg", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    <td><div class=\"input\">&nbsp; ";
        // line 481
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " Conyuge ";
        }
        echo "</div></td>
                    <td><div class=\"input\">&nbsp;  ";
        // line 482
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["edadCon"]) ? $context["edadCon"] : null), "html", null, true);
            echo "  ";
        }
        echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
        // line 483
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "actividad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
        // line 484
        if ((isset($context["Conyuge"]) ? $context["Conyuge"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Conyuge"]) ? $context["Conyuge"] : null), "rentaliquidad", array()), "html", null, true);
            echo " ";
        }
        echo "   </div></td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
            </tbody>
        </table>
        <table class=\"tabla\">
            <thead>
            <th>
                IDENTIFICACIÓN MANDATARIO
            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Apellido Paterno</td>
                    <td>Apellido Materno</td>
                    <td>Nombre</td>
                    <td>R.U.T</td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td colspan=\"2\">Dirección Particular</td>
                    <td>Comuna</td>
                    <td>Ciudad</td>
                </tr>
                <tr>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Nacionalidad</td>
                    <td>Estado civil</td>
                    <td>Profesión</td>
                    <td>Fecha de nacimiento</td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
            </tbody>
        </table>
        <table class=\"tabla\">
            <thead>
            <th colspan=\"6\" align=\"left\">
                Caracteristicas de la operacion
            </th>
            </thead>
            <tbody>
                <tr>
                    <td >Operación</td>
                    <td colspan=\"2\">Tipo</td>
                    <td>Condición</td>
                </tr>
                <tr>
                    <td><div class=\"input\"></div></td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td><div class=\"input\"></div></td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Refinanciamiento</td>
                    <td></td>
                    <td>Fines generales</td>
                    <td></td>
                    <td>Usada</td>
                </tr>
                <tr>
                    <td colspan=\"2\">Indicar banco</td>
                    <td colspan=\"4\"></td>
                </tr>
                <tr>
                    <td>Valor bien raiz</td>
                    <td>UF</td>
                    <td></td>
                    <td>Operacion pactada en</td>
                    <td></td>
                    <td>UF</td>
                    <td></td>
                    <td>\$</td>
                </tr>
                <tr>
                    <td>Plazo credito</td>
                    <td>Años</td>
                    <td></td>
                    <td>Existe promesa de compra venta</td>
                    <td></td>
                    <td>UF</td>
                    <td></td>
                    <td>\$</td>
                </tr>
                <tr>
                    <td>
                        Pago contado
                    </td>
                </tr>
                
                <tr>
                    <td>Monto pagado a la fecha</td>
                    <td>UF</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>A pagar con recursos disponibles</td>
                    <td>UF</td>
                    <td></td>
                    <td>Origen</td>
                    <td></td>
                </tr>
                <tr>
                    <td>A pagar con venta de vehículos o bienes raices</td>
                    <td>UF</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>A pagar con otros recursos</td>
                    <td>UF</td>
                    <td></td>
                    <td>Origen</td>
                </tr>
                <tr>
                    <td>Crédito solicitado</td>
                    <td>UF</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <table class=\"tabla\">
            <thead>
            <th colspan=\"4\">Caraterísticas de la propiedad a adquirir</th>
            </thead>
            <tbody>
                <tr>
                    <td colspan=\"2\">Dirección</td>
                    <td>Comuna</td>
                    <td>Ciudad</td>
                </tr>
                <tr>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Mt<sub>2</sub> Construidos</td>
                    <td colspan=\"2\">Mt<sub>2</sub> Terreno</td>
                    <td>Año aproximado de la construccion </td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Depto n°</td>
                    <td>Estacionamiento N°</td>
                    <td>Bodega N°</td>
                    <td>Rol de la propiedad</td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
            </tbody>
        </table>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>


        <br>
        <br>


        <table class=\"tabla\">
            <thead>
            <th colspan=\"3\" align=\"left\">
                Dirección de cobro
            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Domicilio actual</td>
                    <td>Dirección de trabajo</td>
                    <td>Vivienda que esta comprando</td>
                </tr>
                <tr>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Otro</td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                </tr>
            </tbody>
        </table>
        <table class=\"tabla\">
            <thead>
            <th colspan=\"5\" align=\"left\">
                Estado situación -activos (cifras en miles)
            </th>
            </thead>
            <tbody>
                <tr>
                    <td colspan=\"2\"></td>
                    <td>Monto total</td>
                    <td></td>
                    <td>Monto total</td>
                </tr>
                <tr>
                    <td>Pagos efectuados al vendedor de la propiedad</td>
                    <td>UF</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td>\$</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Depósitos cuenta de ahorro</td>
                    <td>UF</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td>\$</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Acciones</td>
                    <td>UF</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td>\$</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
            </tbody>
        </table>
        <table class=\"tabla\">
            <thead>
            <th colspan=\"7\" align=\"left\">


                Participación en sociedades  / ";
        // line 771
        if ((isset($context["tienesSociedades"]) ? $context["tienesSociedades"] : null)) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 772
        echo "


                
            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Empresa</td>
                   
                    <td>Rut</td>
                    <td>%participación</td>
                    <td>Monto aporte</td>
                </tr>
            
             ";
        // line 787
        if ((isset($context["tienesSociedades"]) ? $context["tienesSociedades"] : null)) {
            // line 788
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesSociedades"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesSociedades"]) {
                // line 789
                echo "                <tr>
                    <td><div class=\"input\" style=\"width:140px;\">&nbsp; ";
                // line 790
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "nombre", array()), "html", null, true);
                echo " </div></td>
               
                    <td><div class=\"input\" style=\"width:150px;\">&nbsp; ";
                // line 792
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "rut", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 793
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "porcentaje", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 794
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "valor", array()), "html", null, true);
                echo "</div></td>
                </tr>
             ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesSociedades'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 797
            echo "             ";
        }
        // line 798
        echo "
            <tr>

                <td>Total </td>
            </tr>


            <tr>

                    <td> ";
        // line 807
        if ((isset($context["totalSociedades"]) ? $context["totalSociedades"] : null)) {
            echo " <div class=\"input\">&nbsp;   ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalSociedades"]) ? $context["totalSociedades"] : null), 0, ".", "."), "html", null, true);
            echo "  </div>";
        }
        echo "</td>
            </tr>

               
            </tbody>
          
        </table>

        <br/>
        <table class=\"tabla\">
            <thead>
            <th colspan=\"5\" align=\"left\">Vehículos / ";
        // line 818
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : null)) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo "</th>
            </thead>
            <tbody>
                <tr>
                    <td>Tipo</td>
                    <td>Marca</td>
                    <td>Modelo</td>
                    <td>Año</td>
                    <td>Valor comercial</td>
                </tr>
                ";
        // line 828
        if ((isset($context["tienesAutomoviles"]) ? $context["tienesAutomoviles"] : null)) {
            // line 829
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesAutomoviles"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesAutomoviles"]) {
                // line 830
                echo "                <tr>
                    <td><div class=\"input\">&nbsp;";
                // line 831
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "tipo", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 832
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "marcaauto", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 833
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "modelo", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 834
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "anoauto", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 835
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAutomoviles"], "valorcomercial", array()), "html", null, true);
                echo "</div></td>
                </tr>

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAutomoviles'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 839
            echo "                ";
        }
        // line 840
        echo "
            <tr>
                <td>

                  Total

                </td>
            </tr>

            <tr>
                <td>
                    <div class=\"input\"> &nbsp; ";
        // line 851
        if ((isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAutomoviles"]) ? $context["totalAutomoviles"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo "  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalAutomovilesApi"]) ? $context["TotalAutomovilesApi"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo " </div>
                </td>


            </tr>
              
            </tbody>  
        </table>


        <br/>



   <table class=\"tabla\">
            <thead>
            <th colspan=\"5\" align=\"left\">Ahorro Inversion /       ";
        // line 867
        if ((isset($context["tienesAhorroInversion"]) ? $context["tienesAhorroInversion"] : null)) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo " ";
        }
        echo " </th>
            </thead>
            <tbody>
                <tr>
                    <td style=\"width:100px;\">Tipo</td>
                    <td>Institución</td>
                    <td>Valor Comercial</td>
                    <td>Prendado</td>
             
                </tr>
                ";
        // line 877
        if ((isset($context["tienesAhorroInversion"]) ? $context["tienesAhorroInversion"] : null)) {
            // line 878
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesAhorroInversion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesAhorroInversion"]) {
                // line 879
                echo "                <tr>
                    <td><div class=\"input\">&nbsp;";
                // line 880
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "tipo", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 881
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 882
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "valorcomercial", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 883
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesAhorroInversion"], "prendado", array()), "html", null, true);
                echo "</div></td>
                 
                </tr>

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesAhorroInversion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 888
            echo "                ";
        }
        // line 889
        echo "
            <tr>
                <td>

                  Total

                </td>
            </tr>

            <tr>
                <td>
                    <div class=\"input\"> &nbsp; ";
        // line 900
        if ((isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversion"]) ? $context["totalAhorroInversion"] : null), 0, ".", "."), "html", null, true);
            echo "  ";
        } else {
            // line 901
            echo "                    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalAhorroInversionApi"]) ? $context["totalAhorroInversionApi"] : null), 0, ".", "."), "html", null, true);
            echo "  ";
        }
        echo " </div>
                </td>


            </tr>
              
            </tbody>  
        </table>


        
        <table class=\"tabla\">
            <thead>
            <th colspan=\"7\" align=\"left\">
                Bienes raíces  / ";
        // line 915
        if ((isset($context["tienesHiporecario"]) ? $context["tienesHiporecario"] : null)) {
            echo " SI ";
        } else {
            echo " ";
            if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null)) {
                echo " SI ";
            } else {
                echo " NO ";
            }
            echo "  ";
        }
        // line 916
        echo "            </th>
            </thead>
            <tbody>


         ";
        // line 921
        if ((isset($context["tienesHiporecario"]) ? $context["tienesHiporecario"] : null)) {
            // line 922
            echo "         ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["tienesHiporecario"]);
            foreach ($context['_seq'] as $context["_key"] => $context["tienesHiporecario"]) {
                // line 923
                echo "



             <tr>

                 <td>Avaluo</td>
                 <td>Direccion</td>
                 <td>Comuna</td>


             </tr>

                <tr>


                    <td><div class=\"input\">&nbsp; ";
                // line 939
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "avaluo", array()), "html", null, true);
                echo " </div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 940
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "direccion", array()), "html", null, true);
                echo " </div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 941
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "comuna", array()), "html", null, true);
                echo " </div></td>

             
                </tr>


             <tr>

                 <td>Region</td>
                 <td>Rol</td>
                 <td>Hipotecado</td>


             </tr>

             <tr>

                 <td><div class=\"input\">&nbsp; ";
                // line 958
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "region", array()), "html", null, true);
                echo " </div></td>
                 <td><div class=\"input\">&nbsp; ";
                // line 959
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "rol", array()), "html", null, true);
                echo " </div></td>
                 <td><div class=\"input\">&nbsp; ";
                // line 960
                echo twig_escape_filter($this->env, $this->getAttribute($context["tienesHiporecario"], "hipotecado", array()), "html", null, true);
                echo " </div></td>


             </tr>


             <tr>
                 <td>&nbsp;</td>
             </tr>
             <tr>
                 <td>&nbsp;</td>
             </tr>


       ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesHiporecario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 975
            echo "

       ";
        }
        // line 978
        echo "
         <tr>
             <td> Total </td>
         </tr>

         <tr>
             <td>  <div class=\"input\"> &nbsp; ";
        // line 984
        if ((isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalHipotecario"]) ? $context["totalHipotecario"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        } else {
            echo " ";
            if ((isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null)) {
                echo " 

                ";
                // line 986
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["TotalPropiedadesApi"]) ? $context["TotalPropiedadesApi"] : null), 0, ".", "."), "html", null, true);
                echo " ";
            }
            echo "   ";
        }
        echo " </div> </td>
         </tr>

              
            </tbody>
        </table>


        <br/><br/><br/><br/>    <br/><br/><br/><br/>   <br/><br/><br/><br/>   <br/><br/><br/><br/>
        <table class=\"tabla\">
            <thead>
            <th colspan=\"9\" align=\"left\">
                Pasivos (Deudas en miles de pesos) - Crédito Consumo   /   ";
        // line 998
        if ((isset($context["debesCreditoConsumo"]) ? $context["debesCreditoConsumo"] : null)) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 999
        echo "            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Institución</td>
                    <td>Pago Mensual</td>
                    <td>Deuda Total</td>
                    <td>Fecha Último Pago</td>
                    <td>Deuda Vigente</td>
                   
                </tr>
                ";
        // line 1010
        if ((isset($context["debesCreditoConsumo"]) ? $context["debesCreditoConsumo"] : null)) {
            // line 1011
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesCreditoConsumo"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesCreditoConsumo"]) {
                // line 1012
                echo "                <tr>
                    <td><div class=\"input\">&nbsp;";
                // line 1013
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1014
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "pagomensual", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1015
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudatotal", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1016
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "fechaultimopago", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1017
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesCreditoConsumo"], "deudavigente", array()), "html", null, true);
                echo "</div></td>
                   
                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesCreditoConsumo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1021
            echo "                ";
        }
        // line 1022
        echo "


                  <tr>
             <td> Total </td>
         </tr>

         <tr>
             <td>  <div class=\"input\"> &nbsp; ";
        // line 1030
        if ((isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : null)) {
            echo " 

                ";
            // line 1032
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesCredito"]) ? $context["totalDebesCredito"] : null), 0, ".", "."), "html", null, true);
            echo "  ";
        }
        // line 1033
        echo "

                 </div> </td>
         </tr>

               
            </tbody>
        </table>
        <table class=\"tabla\">
            <thead>
            <th colspan=\"8\" align=\"left\">
                Líneas de crédito  / ";
        // line 1044
        if ((isset($context["debesLineaCredito"]) ? $context["debesLineaCredito"] : null)) {
            echo " SI ";
        } else {
            echo "  NO ";
        }
        // line 1045
        echo "            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Institución</td>
                    <td>Monto Aprobado</td>
                    <td>Monto Utilizado</td>
                    <td>Vencimiento</td>
                    
                </tr>
                ";
        // line 1055
        if ((isset($context["debesLineaCredito"]) ? $context["debesLineaCredito"] : null)) {
            // line 1056
            echo "

                ";
            // line 1058
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesLineaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesLineaCredito"]) {
                // line 1059
                echo "
                <tr>

                    <td><div class=\"input\">&nbsp;";
                // line 1062
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1063
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoAprobado", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1064
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "montoUtilizado", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1065
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesLineaCredito"], "vencimientofinal", array()), "html", null, true);
                echo "</div></td>

                </tr>


                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesLineaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1071
            echo "

                ";
        }
        // line 1074
        echo "
                 <tr>
             <td> Total </td>
         </tr>

         <tr>
             <td>  <div class=\"input\"> &nbsp; ";
        // line 1080
        if ((isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesLinea"]) ? $context["totalDebesLinea"] : null), 0, ".", "."), "html", null, true);
            echo "   ";
        }
        echo " </div> </td>
         </tr>

              
            </tbody>
        </table>





           <table class=\"tabla\">
            <thead>
            <th colspan=\"8\" align=\"left\">
                Tarjetas de crédito   / ";
        // line 1094
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : null)) {
            echo " SI ";
        } else {
            echo "  NO ";
        }
        // line 1095
        echo "            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Institución</td>
                    <td>Tipo</td>
                    <td>Monto Utilizado</td>
                    <td>Monto Aprobado</td>
          
                    
                </tr>
                ";
        // line 1106
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : null)) {
            // line 1107
            echo "

                ";
            // line 1109
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesTarjetaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesTarjetaCredito"]) {
                // line 1110
                echo "
                <tr>

                    <td><div class=\"input\" style=\"width:140px;\">&nbsp;";
                // line 1113
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\" style=\"width:120px;\">&nbsp;";
                // line 1114
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "tipotarjeta", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1115
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoutilizado", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\">&nbsp;";
                // line 1116
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoaprobado", array()), "html", null, true);
                echo "</div></td>

                </tr>


                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesTarjetaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1122
            echo "

                ";
        }
        // line 1125
        echo "
                 <tr>
             <td> Total </td>
         </tr>

         <tr>
             <td>  <div class=\"input\"> &nbsp; ";
        // line 1131
        if ((isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null), 0, ".", "."), "html", null, true);
            echo "   ";
        }
        echo " </div> </td>
         </tr>

              
            </tbody>
        </table>




        <table class=\"tabla\">
            <thead>
            <th colspan=\"8\" align=\"left\">
                Crédito Hipotecario   / ";
        // line 1144
        if ((isset($context["debesHipotecario"]) ? $context["debesHipotecario"] : null)) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 1145
        echo "            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Institución</td>
                    <td>Pago Mensual</td>
                    <td>Deuda Total</td>
                    <td>Vencimiento Final</td>
                    <td>Deuda Vigente </td>
                    
                </tr>

               ";
        // line 1157
        if ((isset($context["debesHipotecario"]) ? $context["debesHipotecario"] : null)) {
            // line 1158
            echo "               ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesHipotecario"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesHipotecario"]) {
                // line 1159
                echo "                <tr>
                    <td><div class=\"input\">&nbsp; ";
                // line 1160
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "institucion", array()), "html", null, true);
                echo " </div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 1161
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "pagomensual", array()), "html", null, true);
                echo " </div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 1162
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "deudatotal", array()), "html", null, true);
                echo " </div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 1163
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "venciminetofinal", array()), "html", null, true);
                echo " </div></td>
                    <td><div class=\"input\">&nbsp; ";
                // line 1164
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesHipotecario"], "duedavigente", array()), "html", null, true);
                echo " </div></td>

                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesHipotecario'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1168
            echo "                ";
        }
        // line 1169
        echo "

                <tr>
                    <td>Total \$ </td>
                </tr>
                <tr>
                    <td> <div class=\"input\">";
        // line 1175
        if ((isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesHipotecario"]) ? $context["totalDebesHipotecario"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                </tr>
            </tbody>
        </table>

        <br>
        <br>
        <br>
        <br>
        <table class=\"tabla\">
            <thead>
            <th colspan=\"9\" align=\"left\">
                Tarjetas de crédito de casas comerciales /   ";
        // line 1187
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : null)) {
            echo " SI ";
        } else {
            echo " NO ";
        }
        // line 1188
        echo "            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Tipo de Tarjeta</td>
                    <td>Institucion</td>
                    <td>Monto Aprobado</td>
                    <td>Monto Utilizado</td>
                    
                </tr>

                ";
        // line 1199
        if ((isset($context["debesTarjetaCredito"]) ? $context["debesTarjetaCredito"] : null)) {
            // line 1200
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["debesTarjetaCredito"]);
            foreach ($context['_seq'] as $context["_key"] => $context["debesTarjetaCredito"]) {
                // line 1201
                echo "
                <tr>
                    <td><div class=\"input\" style=\"width:130px;\">&nbsp;";
                // line 1203
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "tipotarjeta", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\" style=\"width:100px;\">&nbsp;";
                // line 1204
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "institucion", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\" style=\"width:100px;\">&nbsp;";
                // line 1205
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoaprobado", array()), "html", null, true);
                echo "</div></td>
                    <td><div class=\"input\" style=\"width:100px;\">&nbsp;";
                // line 1206
                echo twig_escape_filter($this->env, $this->getAttribute($context["debesTarjetaCredito"], "montoutilizado", array()), "html", null, true);
                echo "</div></td>
                    
                </tr>

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['debesTarjetaCredito'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1211
            echo "                ";
        }
        // line 1212
        echo "               

               <tr>
                    <td>Total \$ </td>
                </tr>
                <tr>
                    <td> <div class=\"input\">";
        // line 1218
        if ((isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["totalDebesTarjeta"]) ? $context["totalDebesTarjeta"] : null), 0, ".", "."), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                </tr>

            </tbody>
        </table>


        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

        
        <table class=\"tabla\">
            <thead>
            <th colspan=\"6\" align=\"left\">
                Ingresos mensuales grupo familiar
            </th>
            </thead>
            <tbody>
                <tr>
                    <td>Renta liguida solicitante</td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                    <td>Renta liquida cónyuge</td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Otras rentas</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td>Especifique</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                    <td>Total Ingresos</td>
                    <td><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Periodo de reajuste anual</td>
                    <td colspan=\"5\"><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td colspan=\"6\" align=\"left\">
                        Otros antecedentes
                    </td>
                </tr>
                <tr>
                    <td>Debe pagar pension alimenticia</td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                    <td>Monto de la pension</td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                </tr>
                <tr>
                    <td>Es aval, codeudor o fiador</td>
                    <td colspan=\"2\" ><div class=\"input\">&nbsp;</div></td>
                    <td>Especificar detalle y monto</td>
                    <td colspan=\"2\"><div class=\"input\">&nbsp;</div></td>
                </tr>
                    
            </tbody>
        </table>







            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
   <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>   <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

            <div style=\"width: 710px;\">
                <hr style=\"color: #808080;\" size=\"8\" noshade>
                <h1 align=\"right\">SOLICITUD DE PRODUCTOS</h1>
                <hr style=\"color: #808080;\" size=\"8\" noshade>
            </div>


            <h3>Crédito de consumo:</h3>
            <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Monto de Crédito:</td>
                        <td>Plazo de Crédito </td>
                        <td>Seguros Asociados </td>
                        <td>¿Para qué utilizarás el Dinero? </td>

                    </tr>



                            <tr>
                                <td><div class=\"input\">&nbsp;   ";
        // line 1342
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "montoCredito", array()), "html", null, true);
            echo " UF";
        }
        echo "</div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 1343
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 1344
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "segurosAsociados", array()), "html", null, true);
            echo " ";
        }
        echo " </div></td>
                                <td><div class=\"input\">&nbsp;    ";
        // line 1345
        if ((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoConsumo"]) ? $context["creditoConsumo"] : null), "paraque", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                            </tr>






                    </tbody>
                </table>




                <h3>Crédito Hipotecario </h3>
                <table class=\"tabla\">

                <table class=\"tabla\">
                    <thead>

                    </thead>
                    <tbody>
                    <tr>
                        <td>Valor Propiedad :</td>
                        <td>Monto de Píe </td>
                        <td>Porcentaje </td>
                        <td>Monto de Crédito </td>

                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp;   ";
        // line 1380
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "valorPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1381
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoPie", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;   ";
        // line 1382
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "porcentaje", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;  ";
        // line 1383
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "montoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>









                    <tr>
                        <td>Plazo de Crédito </td>
                        <td>Tipo de Propiedad </td>
                        <td>Estado </td>
                        <td>¿Tu propiedad es DFL2 y nueva ? </td>

                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 1407
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "plazoCredito", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp;";
        // line 1408
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "tipoPropiedad", array()), "html", null, true);
            echo " ";
        }
        // line 1409
        echo "                                </div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1410
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "estadoPropiedad", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1411
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "dfl2", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>









                    <tr>
                        <td>Comuna donde desea comprar</td>
                        <td>Proyecto / Inmobiliaria </td>


                    </tr>



                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 1434
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo "  ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "comunaComprar", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1435
        if ((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoHipotecario"]) ? $context["creditoHipotecario"] : null), "proyecto", array()), "html", null, true);
            echo "     ";
        }
        echo "</div></td>


                        </tr>







                    </tbody>
                </table>




                    Crédito Automotriz

                    <table class=\"tabla\">
                        <thead>

                        </thead>
                        <tbody>
                        <tr>
                            <td>Monto del Auto </td>
                            <td>Monto del Píe  </td>
                            <td>Plazo del Crédito </td>


                        </tr>


                                <tr>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1469
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "monto", array()), "html", null, true);
            echo " ";
        }
        echo "  </div></td>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1470
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "pie", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                                    <td><div class=\"input\">&nbsp; ";
        // line 1471
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "plazo", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                                </tr>



                        <tr>
                            <td>Cuándo piensas comprar tu auto </td>
                            <td>Marca de Auto  </td>
                            <td>Año del Auto </td>


                        </tr>


                        <tr>
                            <td><div class=\"input\">&nbsp; ";
        // line 1488
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "cuandocompras", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1489
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "marcaauto", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>
                            <td><div class=\"input\">&nbsp; ";
        // line 1490
        if ((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["creditoAutomotriz"]) ? $context["creditoAutomotriz"] : null), "ano", array()), "html", null, true);
            echo " ";
        }
        echo "</div></td>


                        </tr>




                        </tbody>
                    </table>



                    <table class=\"tabla\">

<h3>
DATOS CRÉDITO CONSOLIDACION 
</h3>
<tr>
    <th>Crédito </th>
    <th>Monto </th>
    <th>Plazo </th>

  
    </tr>



    </thead>

    <tr>
        ";
        // line 1521
        $context["con"] = 0;
        // line 1522
        echo "    ";
        if ((isset($context["creditoConsolidacion"]) ? $context["creditoConsolidacion"] : null)) {
            // line 1523
            echo "    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($context["creditoConsolidacion"]);
            foreach ($context['_seq'] as $context["_key"] => $context["creditoConsolidacion"]) {
                // line 1524
                echo "    ";
                $context["con"] = ((isset($context["con"]) ? $context["con"] : null) + 1);
                // line 1525
                echo "
    ";
                // line 1526
                if (((isset($context["con"]) ? $context["con"] : null) <= 1)) {
                    // line 1527
                    echo "
    <tr>   

        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1530
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1531
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1532
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo", array()), "html", null, true);
                    echo " </div> </td>
     </tr>
        
       <tr>    

        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1537
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp;";
                    // line 1538
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito2", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1539
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo2", array()), "html", null, true);
                    echo " </div> </td>
</tr>
<tr>   


        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\">  &nbsp;";
                    // line 1544
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "tipoCredito3", array()), "html", null, true);
                    echo "  </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1545
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "montoCredito3", array()), "html", null, true);
                    echo " </div></td>
        <td><div class=\"input\" style=\"width:80px;margin-left:5px;\"> &nbsp; ";
                    // line 1546
                    echo twig_escape_filter($this->env, $this->getAttribute($context["creditoConsolidacion"], "plazo3", array()), "html", null, true);
                    echo " </div></td>
       
   </tr> 

    ";
                }
                // line 1551
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['creditoConsolidacion'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1552
            echo "    ";
        }
        // line 1553
        echo "
    
    
        </tr>

</table>




                    
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:plantillas:metlife.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2501 => 1553,  2498 => 1552,  2492 => 1551,  2484 => 1546,  2480 => 1545,  2476 => 1544,  2468 => 1539,  2464 => 1538,  2460 => 1537,  2452 => 1532,  2448 => 1531,  2444 => 1530,  2439 => 1527,  2437 => 1526,  2434 => 1525,  2431 => 1524,  2426 => 1523,  2423 => 1522,  2421 => 1521,  2383 => 1490,  2375 => 1489,  2367 => 1488,  2343 => 1471,  2335 => 1470,  2327 => 1469,  2286 => 1435,  2278 => 1434,  2249 => 1411,  2242 => 1410,  2239 => 1409,  2233 => 1408,  2225 => 1407,  2194 => 1383,  2186 => 1382,  2178 => 1381,  2170 => 1380,  2128 => 1345,  2120 => 1344,  2112 => 1343,  2104 => 1342,  1973 => 1218,  1965 => 1212,  1962 => 1211,  1951 => 1206,  1947 => 1205,  1943 => 1204,  1939 => 1203,  1935 => 1201,  1930 => 1200,  1928 => 1199,  1915 => 1188,  1909 => 1187,  1890 => 1175,  1882 => 1169,  1879 => 1168,  1869 => 1164,  1865 => 1163,  1861 => 1162,  1857 => 1161,  1853 => 1160,  1850 => 1159,  1845 => 1158,  1843 => 1157,  1829 => 1145,  1823 => 1144,  1803 => 1131,  1795 => 1125,  1790 => 1122,  1778 => 1116,  1774 => 1115,  1770 => 1114,  1766 => 1113,  1761 => 1110,  1757 => 1109,  1753 => 1107,  1751 => 1106,  1738 => 1095,  1732 => 1094,  1711 => 1080,  1703 => 1074,  1698 => 1071,  1686 => 1065,  1682 => 1064,  1678 => 1063,  1674 => 1062,  1669 => 1059,  1665 => 1058,  1661 => 1056,  1659 => 1055,  1647 => 1045,  1641 => 1044,  1628 => 1033,  1624 => 1032,  1619 => 1030,  1609 => 1022,  1606 => 1021,  1596 => 1017,  1592 => 1016,  1588 => 1015,  1584 => 1014,  1580 => 1013,  1577 => 1012,  1572 => 1011,  1570 => 1010,  1557 => 999,  1551 => 998,  1532 => 986,  1521 => 984,  1513 => 978,  1508 => 975,  1487 => 960,  1483 => 959,  1479 => 958,  1459 => 941,  1455 => 940,  1451 => 939,  1433 => 923,  1428 => 922,  1426 => 921,  1419 => 916,  1407 => 915,  1387 => 901,  1381 => 900,  1368 => 889,  1365 => 888,  1354 => 883,  1350 => 882,  1346 => 881,  1342 => 880,  1339 => 879,  1334 => 878,  1332 => 877,  1309 => 867,  1282 => 851,  1269 => 840,  1266 => 839,  1256 => 835,  1252 => 834,  1248 => 833,  1244 => 832,  1240 => 831,  1237 => 830,  1232 => 829,  1230 => 828,  1207 => 818,  1189 => 807,  1178 => 798,  1175 => 797,  1166 => 794,  1162 => 793,  1158 => 792,  1153 => 790,  1150 => 789,  1145 => 788,  1143 => 787,  1126 => 772,  1120 => 771,  826 => 484,  818 => 483,  810 => 482,  804 => 481,  792 => 480,  702 => 397,  695 => 396,  687 => 395,  664 => 379,  655 => 376,  646 => 374,  621 => 356,  612 => 353,  603 => 351,  578 => 333,  569 => 330,  560 => 328,  536 => 311,  527 => 308,  518 => 306,  455 => 250,  446 => 247,  437 => 245,  412 => 227,  403 => 224,  394 => 222,  369 => 204,  360 => 201,  351 => 199,  319 => 173,  310 => 171,  288 => 156,  279 => 153,  270 => 151,  248 => 136,  239 => 133,  230 => 131,  211 => 118,  203 => 117,  195 => 116,  177 => 104,  169 => 103,  161 => 102,  141 => 93,  128 => 87,  120 => 86,  112 => 85,  104 => 84,  19 => 1,);
    }
}
