<?php

/* BenchAdminBundle:Default:SoloRegistrados.html.twig */
class __TwigTemplate_3add9dbac632da69a816aee7ce331a38150cf49e428d353fbe6db1812d7f6415 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\" />
  <title>Administrador Bench Banking</title>
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
  <meta name=\"description\" content=\"\" />
  <meta name=\"author\" content=\"\" />
  <META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, FOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"INDEX, NOFOLLOW\">
<META NAME=\"ROBOTS\" CONTENT=\"NOINDEX, NOFOLLOW\">

<meta name=\"robots\" content=\"noindex\">
  <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"main-theme-script\" />
  <link href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-specific-script\" />
  <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/mask.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.js"), "html", null, true);
        echo "\"></script>





     <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

     <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.columnFilter.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

  
    
     

  
  <link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/css/datepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  


  <link rel=\"stylesheet\" type=\"text/css\" media=\"screen,projection\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />
  

  <link type=\"text/css\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.intenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />   

 
  <link href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/simplenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  

  <link rel=\"shortcut icon\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/favicon.ico"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-144-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-114-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-72-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-57-precomposed.png"), "html", null, true);
        echo "\" />
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>
<body id=\"dashboard\" class=\"hidden\">






 <div id=\"div_carga\">
   <img id=\"cargador\" src=\"./img/loader.gif\"/>
   </div>

<!-- Top navigation bar -->
<div class=\"navbar navbar-fixed-top\">
  <div class=\"navbar-inner\">
    <div class=\"container-fluid\">
      <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </a>
      <a class=\"brand\" href=\"index.html\">Bench banking</a>
      <div class=\"btn-group pull-right\">
        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
          <i class=\"icon-user\"></i> 
          <span class=\"caret\"></span>
        </a>
        <ul class=\"dropdown-menu\">
        
          <li><a href=\"login.html\">Logout</a></li>
        </ul>
      </div>
      <div class=\"nav-collapse\">
          
          
        <ul class=\"nav\">
        
              
          <li class=\"dropdown\">
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  Menu
                      <b class=\"caret\"></b>
                </a>
                <ul class=\"dropdown-menu\">
                \t  <li class=\"nav-header\">Menu</li>        
          <li class=\"active\"><a href=\"";
        // line 96
        echo $this->env->getExtension('routing')->getPath("usuarioPedido");
        echo "\"><i class=\"icon-home\"></i> Home </a></li>
          <li><a href=\"";
        // line 97
        echo $this->env->getExtension('routing')->getPath("usuarioPedido");
        echo "\">Usuarios Con Pedidos /  Usuarios</a></li>
          <li><a href=\"";
        // line 98
        echo $this->env->getExtension('routing')->getPath("index2");
        echo "\">Ver todos / Usuarios</a></li>
          
        
         
                </ul>
          </li>
       
        </ul>
      </div>
    </div>
  </div>
</div>
  
<div class=\"container-fluid\">
  <div class=\"row-fluid\">
    
    <!-- Bread Crumb Navigation -->
  
      

   
  
         
         <!-- Portlet Set 5 -->
             

        <h6><a href=\"";
        // line 124
        echo $this->env->getExtension('routing')->getPath("usuarioPedido");
        echo "\">Ver Con Pedidos /  Usuarios</a></h6>
         <h6><a href=\"";
        // line 125
        echo $this->env->getExtension('routing')->getPath("index2");
        echo "\">Ver todos / Usuarios</a></h6>
        
\t  <!-- Table -->
      
      
         \t <!-- Portlet: Browser Usage Graph -->
            
                 <h4 class=\"box-header round-top\"></h4>
                 <H2> Usuarios Archivados </H2>
                  <a class=\"box-btn\" title=\"close\"><i class=\"icon-remove\"></i></a>
                     <a class=\"box-btn\" title=\"toggle\"><i class=\"icon-minus\"></i></a>  
                 
                     
              </h4>         
              
      
      

                     <section id=\"contenido\">
                         
                         
          
                           
<link href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/table.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

<link href=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/css/smoothness/jquery-ui-1.8.4.custom.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
<script type=\"text/javascript\" src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.datatable.inc.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>    
   


";
        // line 157
        echo $this->env->getExtension('DatatableBundle')->datatable(array("edit_route" => "add", "delete_route" => "del", "otra_route" => "CrearPdf", "ver_route" => "ver", "archivar_route" => "archivarUsuario2", "UpDicom_url" => "UpDicom", "Up_url" => "otrosUpload", "js" => array("sAjaxSource" => $this->env->getExtension('routing')->getPath("grid4"))));
        // line 171
        echo "


    <script>

  
    
</script>                     




                     </section>










                  </div>
              </div>
            </div><!--/span-->




   </div>
      </div>  
 
    </div><!--/span-->
  </div><!--/row-->



<div style=\"clear:both;\"></div>










  <footer>
    <p>&copy; Bench Banking</p>
  </footer>
    <div id=\"box-config-modal\" class=\"modal hide fade in\" style=\"display: none;\">
      <div class=\"modal-header\">
        <button class=\"close\" data-dismiss=\"modal\">×</button>
        <h3></h3>
      </div>
      <div class=\"modal-body\">
        <p></p>
      </div>
      <div class=\"modal-footer\">
        <a href=\"#\" class=\"btn btn-primary\" data-dismiss=\"modal\"></a>
        <a href=\"#\" class=\"btn\" data-dismiss=\"modal\"></a>
      </div>
    </div>
</div><!--/.fluid-container-->

    
  
    <script src=\"";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.core.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 241
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 242
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 243
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 244
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
    
  
    <script src=\"";
        // line 247
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js"), "html", null, true);
        echo "\"></script>

 
    <script src=\"";
        // line 251
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 252
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/bootbox/bootbox.min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 254
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>

\t\t
    <!-- jQuery Cookie -->    
    <script src=\"";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.cookie/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Full Calender -->
    <script type='text/javascript' src='";
        // line 261
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
    
    <!-- CK Editor -->
\t<script type=\"text/javascript\" src=\"";
        // line 264
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Chosen multiselect -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 267
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script>  
    
    <!-- Uniform -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
    
    
\t
    <script src=\"";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/simplenso/simplenso.js"), "html", null, true);
        echo "\"></script>
  </body>


                 
                         
                ";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:SoloRegistrados.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  404 => 274,  397 => 270,  391 => 267,  385 => 264,  379 => 261,  373 => 258,  366 => 254,  361 => 252,  357 => 251,  351 => 248,  347 => 247,  341 => 244,  337 => 243,  333 => 242,  329 => 241,  325 => 240,  254 => 171,  252 => 157,  245 => 153,  241 => 152,  237 => 151,  233 => 150,  228 => 148,  202 => 125,  198 => 124,  169 => 98,  165 => 97,  161 => 96,  112 => 50,  108 => 49,  104 => 48,  100 => 47,  96 => 46,  90 => 43,  84 => 40,  78 => 37,  71 => 33,  61 => 26,  56 => 24,  47 => 18,  43 => 17,  39 => 16,  35 => 15,  19 => 1,);
    }
}
