<?php

/* BenchPaginasBundle:Default:simuladorLog.html.twig */
class __TwigTemplate_8c176d358f9eb449148360944dfc4f38dd5de3358e7bc2b76cc38adaa289c76a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/main.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/puntos.js"), "html", null, true);
        echo "\"></script>
                                    
    
    <section style=\"width: auto;height: auto;margin-left: 40px;\">
        

          <input type=\"hidden\" id=\"ufa\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["uf"]) ? $context["uf"] : null), "html", null, true);
        echo "\">
      <div style=\"width:200px;height:20px;position:relative;\"></div>
    <div class=\"h7\" style=\"margin:10px auto;\">Simulador de Créditos</div>
        <span class=\"orange\">Elije el Tipo de Crédito :</span>
        <div style=\"clear:both;\"></div>
        <select id=\"tipoCreditoSimulador\" class=\"datasimulador\" name=\"tipocredito\">
        \t<option value=\"\">Opción</option>
            <option value=\"Crédito Hipotecario\">Crédito Hipotecario </option>
            <option value=\"Crédito de Consumo\">Crédito de Consumo </option>
        </select>
        

                
                <div style=\"clear: both;\"></style>

    
    <!-- Simulador de crédito de consumo -->
    
        <div class=\"container-simulador\" id=\"simuladorConsumo\">
            
            
        <div class=\"col1\" style=\"width: 300px;\">
        
        
\t\t
        
        
        <br>
        <section>
        <span class=\"orange\">Monto del Crédito \$:</span>
        <input type=\"text\" name=\"montocredito\" placeholder=\"ingresa el monto\" class=\"input-box\" id=\"valueSliderMontoC\"    onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"/>

\t</section>
        
        <div style=\"clear:both;\"></div>
        
        <section>
            
        <span class=\"orange\">Plazo del Crédito (Meses):</span>
        <input type=\"text\" name=\"plazocredito\" placeholder=\"ingresa el plazo\" class=\"input-box\" id=\"valueSliderPlazo\" />

\t</section>
        
        
           <section>
            
        <span class=\"orange\">Ingresa tu Sueldo:</span>
    <input type=\"text\" name=\"sueldosimulador\" placeholder=\"Ingresa tu Sueldo\" class=\"input-box\" id=\"sueldo22\"  onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"/>

\t</section>
        
        
        
        
    

       
        
        <span class=\"orange\" id=\"resultado2\"></span>
    
       


        <div style=\"clear:both\"></div>
        
   
        <button id=\"calculoBut\"  class=\"classname\"  style=\"margin-left: -67%;\">Calcular </button>

        
        
        </div>
            
        </div>
    
    <div style=\"clear: both; width: 100%;\"></div>
    
    
    <!-- Simulador de crédito Hipotecario -->
    
    <div class=\"container-simulador\" id=\"simuladorHipotecario2\">
        
        <div class=\"col2\" style=\"width: 300px;\">
   
            <br>
            <span class=\"orange\">Valor Propiedad:</span>

\t\t<input type=\"text\" name=\"valor\" placeholder=\"Pesos\" class=\"input-box\" id=\"valor992\" title=\"Debes ingresar el valor de la propiedad en Unidad de Fomento\"   onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  />
    <input type=\"hidden\" value=\"<?php echo \$idUser3 ; ?>\" name=\"idUCreditoHipotecario\" >

    
    <br>
    
    

    
  
        
<span class=\"orange\">Monto de Píe:</span>
<input type=\"text\" name=\"pie\" placeholder=\"UF\" class=\"input-box\" id=\"pie992\" title=\"Debes ingresar el valor de la propiedad en Unidad de Formento, generalmente es un 10% o 20%\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" /> 
        
  <br>
    
<span class=\"orange\">Porcentaje:</span>
<input type=\"text\" name=\"porcentaje\" placeholder=\"%\" class=\"input-box\" id=\"porcentaje992\" /> 
  
  



            <br>
      
             
     <span class=\"orange\">Monto Credito:</span>
     <input type=\"text\" name=\"valor\" placeholder=\"UF\"  class=\"input-box\" id=\"MontoCredito99\" title=\"Debes ingresar el valor de la propiedad en Unidad de Fomento\"   onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"  />

      <br>
    
     
  <span class=\"orange\">Plazo del Crédito:</span>
<select class=\"data\" select=\"selected\" name=\"plazo\" id=\"plazoCreditoH99\">
<option value=\"\">Opción</option>
<option value=\"6\">6 años</option>
<option value=\"7\">7 años</option>
<option value=\"8\">8 años</option>
<option value=\"9\">9 años</option>
<option value=\"10\">10 años</option>
<option value=\"11\">11 años</option>
<option value=\"12\">12 años</option>
<option value=\"13\">13 años</option>
<option value=\"14\">14 años</option>
<option value=\"15\">15 años</option>
<option value=\"16\">16 años</option>
<option value=\"17\">17 años</option>
<option value=\"18\">18 años</option>
<option value=\"19\">19 años</option>
<option value=\"20\">20 años</option>
<option value=\"21\">21 años</option>
<option value=\"22\">22 años</option>
<option value=\"23\">23 años</option>
<option value=\"24\">24 años</option>
<option value=\"25\">25 años</option>
<option value=\"26\">26 años</option>
<option value=\"27\">27 años</option>
<option value=\"28\">28 años</option>
<option value=\"29\">29 años</option>
<option value=\"30\">30 años</option>

</select>
  
  <br>
  <span class=\"orange\">Sueldo:</span>
<input type=\"text\" name=\"sueldosimulador\" placeholder=\"Ingresa tu Sueldo\" class=\"input-box\" id=\"sueldosimulador992\"  onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"/>


       
       
       
       
    

  <!--<span class=\"orange\">Tipo de Propiedad:</span>

 <select class=\"data\" select=\"selected\"  id=\"tipoPropiedad\"name=\"tipopropiedad\">
<option value=\"\">Opción</option>
<option value=\"Casa\">Casa</option>
<option value=\"Departamento\">Departamento</option>
<option value=\"Terreno\">Terreno</option>
<option value=\"Otro\">Otro</option>
</select>-->
        
        
        
    </div>
        
        <div style=\"clear:both;\"></div>
     
          
         
        <button id=\"calculoBut2\"  class=\"classname\"  style=\"margin-left: -83%;position:relative;\">Calcular </button>
       
         <br>
         <br>
         
         <span class=\"orange\" id=\"resultado3\"></span>
         
          
          
         
    </div>

    
    
    <!--- TABLA y ALERTA -->
    
    <div style=\"clear:both;\"></div>
    
    <section>
        
        <div class=\"alert-form4\" id=\"menosde\" style=\"display: block;\">
<img src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/alert_icon.png"), "html", null, true);
        echo "\">
De acuerdo a su renta, su carga financiera es <span></span> lo que está sobre/bajo la carga máxima recomendada.
</div>
         
         
         
         
         
         <div style=\"clear:both;\"></div>
         
         <br>
         
         
         <div class=\"datagrid\" style=\"width:90%;\">
  <table id=\"tablaSueldo\" >
<thead><tr>
    <th width=\"50%\">Tramo de Renta Líquida</th>
    <th width=\"50%\">Carga Financiera Máxima</th></tr></thead>
<tbody>
    
   <tr id=\"tramo1\">
  <td>\$400.000 - \$600.000</td>
  <td>25%</td></tr>
  <tr class=\"alt\" id=\"tramo2\">
    <td>\$601.000 - \$1.300.000</td>
    <td>40%</td>
  </tr>
  
  
  <tr id=\"tramo3\">
    <td>\$1.301.000 - \$2.500.000</td>
    <td>50%</td>
  </tr>
  
  
  <tr class=\"alt\" id=\"tramo4\">
    <td>\$2.501.000 y más</td>
    <td>55%</td>
  </tr>
  
  
  <tr id=\"respuesta\">
    <td colspan=\"2\">*Los tramos de carga financieras son recomendaciones realizadas por el SERNAC según información que se pudo recopilar de instituciones finacieras y fiscalizadoras.</td>
  </tr>
</tbody>
</table>
</div>
    </section>
         <!--- FIN TABLA y ALERTA -->

        
        
    </section>




";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:simuladorLog.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 207,  33 => 8,  24 => 2,  19 => 1,);
    }
}
