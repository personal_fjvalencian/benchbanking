<?php

/* BenchTienesBundle:Default:tienesSociedades.html.twig */
class __TwigTemplate_8c9ae78a7fde4b1f6afe0c2c69a8899e442c4fe619d6d8bfc512e7d31a47f0de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "

<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchtienes/js/tienesSociedades.js"), "html", null, true);
        echo "\"></script>
\t\t<div class=\"fila\">
      
        


        \t<div class=\"col1\">

             
              
               



          <form  action=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("tieneSociedadesSiNo");
        echo "\" id=\"SociedadesSiNo\" method=\"post\">
           

<input type=\"hidden\" id=\"token\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
            <label class=\"label-text2\" for=\"rut\">¿Tienes algúna Sociedad?</label>

            <input type=\"hidden\" name=\"iduseSino\" id=\"iduseSino\"  value=\"<?php echo \$idUser3; ?>\">
          
                
            
    ";
        // line 28
        if (($this->getAttribute((isset($context["usuario"]) ? $context["usuario"] : null), "tienesSociedadesSiNo", array()) == "SI")) {
            // line 29
            echo "                   
 <p class=\"rad\">Si<input type=\"radio\" value=\"SI\"class=\"SociedadesRadio\" name=\"SociedadesRadio\" id=\"SociedadesRadio\" checked >
         No<input type=\"radio\" value=\"NO\"   class=\"SociedadesRadio\" name=\"SociedadesRadio\" id=\"SociedadesRadio\"></p>
   
 
       
 ";
        } else {
            // line 36
            echo "      
   
 <p class=\"rad\">Si<input type=\"radio\" value=\"SI\" class=\"SociedadesRadio\" name=\"SociedadesRadio\" id=\"SociedadesRadio\">
         No<input type=\"radio\" value=\"NO\"  class=\"SociedadesRadio\" name=\"SociedadesRadio\" id=\"SociedadesRadio\"  checked></p>
            
 
 ";
        }
        // line 43
        echo "

   
        <div style=\"float:left; height:10px; width:100%;\"></div>
       <button style=\"float:left;\" id=\"TSguardadatos1\" class=\"classname\" value=\"Agregar\" type=\"submit\">GUARDAR  Y SIGUIENTE</button>

            </form>

            <div id=\"RespuestaSociedades\">
            
            </div>



  <form class=\"form-personal\" id=\"tienesSociedades\" action=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("sociedadesguarda");
        echo "\" method=\"POST\"   autocomplete=\"off\" >
         <label class=\"label-text2\" for=\"institucion\">Nombre Sociedad</label>

                     
               <input type=\"text\" class=\"input-box2\" name=\"TSnombre\" id=\"TSnombre\" data-validation-engine=\"validate[required]\" data-required=\"true\">
           
             

               
             <label class=\"label-text2\" for=\"institucion\">RUT</label>
               <input type=\"text\"class=\"input-box2\" name=\"TSRut\" id=\"TSRut\" data-validation-engine=\"validate[required]\" data-required=\"true\">
               
               </div>
              </form>
                    
             <div class=\"col1\">
              <form id=\"tienesSociedades2\">  
              <label class=\"label-text2\" for=\"rut\">Porcentaje</label>
              <input type=\"text\" class=\"input-box2\" name=\"TSPorcentaje\" id=\"TSPorcentaje\"  placeholder=\"%\" data-validation-engine=\"validate[required]\" data-required=\"true\">
              
              <label class=\"label-text2\" for=\"rut\">Valor Participativo</label>
              <input type=\"text\" class=\"input-box2\" placeholder=\"\$\" name=\"TSValor\" id=\"TSValor\" onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" data-validation-engine=\"validate[required]\" data-required=\"true\">              
        \t</div>
            
             </form>     
       \t  <div class=\"clearfix\"></div>
          <div class=\"col2\"><button style=\"float:left;margin-top:10px; \" id=\"TSguardadatos\" class=\"classname\" value=\"Agregar\" type=\"submit\">AGREGAR</button>

            <div id=\"SiguienteSociedades\" class=\"classname\" style=\"margin-top:10px; margin-left: 1%; position: relative;width: 80px; float: left;\" >SIGUIENTE</div>
          </div> 
          <div class=\"flix-clear\"></div>


            <div class=\"clearfix\"></div>
            <br>
            
            <div class=\"col2\">
              <table class=\"table table-striped\" id=\"respuestaSociedades\">

\t\t\t    <thead style=\"background:#F60\">
                  <tr>
\t\t\t\t\t<td width=\"12%\"><p style=\"text-align:center; color:#FFF;\">Nombre</p></td>
\t\t\t\t\t<td width=\"18%\"><p style=\"text-align:center;color:#FFF;\">Rut</p></td>
\t\t\t\t
\t\t\t\t\t<td width=\"13%\"><p style=\"text-align:center;color:#FFF;\">Porcentaje</p></td>
\t\t\t\t\t<td width=\"19%\"><p style=\"text-align:center;color:#FFF;\">Valor de Participación</p></td>
\t\t\t\t\t<td width=\"18%\"><p style=\"text-align:center;color:#FFF;\">Eliminar</p></td>
\t\t\t\t  </tr>
\t\t\t\t</thead>

      
\t  <tbody style=\"background:#F8F8F8;  font-family: Arial, Helvetica, sans-serif;\">
         ";
        // line 109
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($context["tienesSociedades"]);
        foreach ($context['_seq'] as $context["_key"] => $context["tienesSociedades"]) {
            echo "  
                  
          <tr id=\"itemTienesSociedades_";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "id", array()), "html", null, true);
            echo "\">
\t\t\t\t\t
         
\t  <td><p style=\"text-align:center;color:#666;\">";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "nombre", array()), "html", null, true);
            echo "</p></td>
\t\t
          <td><p style=\"text-align:center;color:#666;\">";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "rut", array()), "html", null, true);
            echo "</p></td>
          <td><p style=\"text-align:center;color:#666;\">";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "porcentaje", array()), "html", null, true);
            echo "</p></td>  
          <td><p style=\"text-align:center;color:#666;\">";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "valor", array()), "html", null, true);
            echo "</p></td>  
          
          
          <td><p style=\"text-align:center;color:#666;\"> <a href=\"#\" class=\"del_button\" id=\"del-";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($context["tienesSociedades"], "id", array()), "html", null, true);
            echo "\">eliminar</a> </p> </td>
\t\t\t\t  
          </tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tienesSociedades'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "    
\t\t\t\t</tbody>
   
             </table>
              
              
              
              
              
              
              
             
              
              
             
            
            </div>
            
            

\t
\t\t</div>
\t
        

        
            



</form>
     
                    

<form id=\"delSociedades\"  action=\"";
        // line 160
        echo $this->env->getExtension('routing')->getPath("delsociedades2");
        echo "\"  method=\"post\"></form>";
    }

    public function getTemplateName()
    {
        return "BenchTienesBundle:Default:tienesSociedades.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 160,  190 => 126,  179 => 121,  173 => 118,  169 => 117,  165 => 116,  160 => 114,  154 => 111,  147 => 109,  92 => 57,  76 => 43,  67 => 36,  58 => 29,  56 => 28,  46 => 21,  40 => 18,  23 => 4,  19 => 2,);
    }
}
