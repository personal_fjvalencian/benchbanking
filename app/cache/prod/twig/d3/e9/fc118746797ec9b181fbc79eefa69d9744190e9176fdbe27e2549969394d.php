<?php

/* BenchPaginasBundle:Default:recupera.html.twig */
class __TwigTemplate_d3e9fc118746797ec9b181fbc79eefa69d9744190e9176fdbe27e2549969394d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
  
<html>

    
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">

<link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
<link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/ship.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  
<link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/basic.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 
<link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/css/flexslider.css"), "html", null, true);
        echo "\" rel=\"stylesheet\"> 

<script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.flexslider-min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.carouFredSel-6.1.0-packed.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciador.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/botoneshead.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/html5.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/recupera.js"), "html", null, true);
        echo "\"></script>

<meta name=\"google-site-verification\" content=\"4sibG4Sz2wGxTyteKnT1E8uJM6vt67srq5ANMPkQ5Vg\" />
<META name=\"keywords\" content=\"credito hipotecario, credito consumo, simulador, tasa, banco\">
<title> BenchBanking │ Créditos Convenientes </title>
<META name=\"description\" content=\"Créditos más convenientes y mejor informados, compara las tasas antes de contratarlo\">  
<link rel=\"shortcut icon\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
<link rel=\"icon\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
</head>


<body>
    
    ";
        // line 34
        $this->env->loadTemplate("BenchPaginasBundle:Default:header.html.twig")->display($context);
        // line 35
        echo "    ";
        $this->displayBlock('header', $context, $blocks);
        // line 43
        echo "    
    
    
    

<div class=\"wrapper\" style=\"padding-top:20px;\">

<div class=\"container-sections\">


    
    
 

    <div class=\"wrapper-border-radius\">
        
        
    <div class=\"title-section\" style=\"width: 240px;height:20px;position: relative;margin:auto;margin-bottom: 10px\">He olvidado mi contraseña.</div>
    <p style=\"margin:20px; color:#666;width:340px; height: 20px; position: relative;margin:auto;\">Para restablecer tu contraseña, introduce tu correo electr&oacute;nico</p>
    
    <form class=\"form-box\" style=\"text-align:center; margin:auto; padding:0 0 5% 0;width:200px;\"  id=\"Formverifica\" action=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("recuperaContrasena");
        echo "\"  method=\"post\"  autocomplete=\"off\"
          >

    
    <input type=\"text\" name=\"email\" class=\"input-box\" id=\"email\" style=\"width:200px;\" />
    
    
    
        <div class=\"flix-clear\"></div>
        <div class=\"alert-form\" id=\"alert-form3\">
          <p></p>

        </div>
        <input class=\"classname\"  id=\"submit2\" type=\"submit\" value=\"Recuperar Contraseña\">

    </form>
    </div>      

    
    
</div> 

<!-- modal content -->
<div id=\"basic-modal-content4\">
\t\t<div class=\"h7\" style=\"margin:10px auto;\">Privacidad</div>
        
        
<h1 class=\"orange\" style=\"margin:20px auto;\">Protegemos su información personal y sus datos </h1>
\t\t\t

<p class=\"gray\">Debido a la actividad comercial que desarrolla BenchBanking Chile recoge y, en algunos casos, comunica información sobre los Usuarios de su Sitio web.</p>

<h1 class=\"orange\" style=\"margin:20px auto\">1. Protegiéndolo</h1>
<p class=\"gray\">BenchBanking Chile está comprometido a proteger su privacidad. En esta Política de Privacidad se describe la información que BenchBanking Chile recoge sobre sus Usuarios y visitantes y lo que puede ocurrir con dicha información. Si no está de acuerdo con esta Política de Privacidad no utilice el Sitio. Nuestra Política de Privacidad asegura que cualquier información que nos provea será mantenida privada y segura. Para dar fe de esto, en este documento proveemos los detalles de qué información recabamos y de qué manera la utilizamos. Nunca recolectaremos información sin su consentimiento explícito. Este documento es parte integrante de los Términos y Condiciones de BenchBanking Chile. Mediante la aceptación de los Términos y Condiciones el Usuario acepta las Políticas de Privacidad aquí contenidas. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">2. La información que recabamos. </h1> 
        <p class=\"gray\"> Recabamos información de identificación personal (IIP) que es cedida voluntariamente durante el proceso de registración o en respuesta a requerimientos explícitos presentados por BenchBanking Chile También podemos recabar su dirección de IP (Internet Protocol) para ayudar a diagnosticar problemas con nuestro servidor, y para administrar el Sitio. Una dirección de IP es un número que se le asigna a su computadora cuando usa internet. Su dirección de IP también es utilizada para ayudar a identificarle dentro de una sesión particular y para recolectar información demográfica general. Podemos solicitarte tu e-mail durante la utilización del sitio.

Se establece que en cualquier momento los Usuarios de BenchBanking Chile podrán solicitar la baja de su solicitud y la eliminación de su cuenta e información de la base de datos de BenchBanking Chile. BenchBanking Chile recoge y almacena automáticamente cierta información sobre la actividad de los Usuarios dentro de nuestro Sitio web. Tal información puede incluir la URL de la que provienen (estén o no en nuestro Sitio web), a qué URL acceden seguidamente (estén o no en nuestro Sitio web), qué navegador están usando, así como también las páginas visitadas, las búsquedas realizadas, las publicaciones, compras o ventas, mensajes, etc. </p>   
        
        <h1 class=\"orange\" style=\"margin:20px auto\">3. Uso que hacemos de la información. </h1> 
        <p class=\"gray\"> Para suministrar mejores cotizaciones de créditos por parte de los bancos, BenchBanking Chile requiere ciertos datos de carácter personal. La recolección de información nos permite ofrecerles servicios y funcionalidades que se adecuan mejor a sus necesidades. Los Datos Personales que recabamos tienen las siguientes finalidades:

- Entregar a los bancos un resumen de cada usuario para recibir cotización de créditos. - Ponernos en contacto directo con Ud. cada vez que BenchBanking Chile lo considere conveniente con el fin de ofrecerle por distintos medios y vías (incluyendo mail, SMS, etc.) BenchBankinges de otros productos y/o servicios. -Mejorar nuestras iniciativas comerciales y promocionales para analizar las páginas más visitadas por los Usuarios, las búsquedas realizadas, perfeccionar nuestra oferta de contenidos, personalizar dichos contenidos, presentación y servicios. -Enviar información o mensajes sobre cotizaciones de bancos. Si el Usuario lo prefiere, puede solicitar que lo excluyan de las listas para el envío de información promocional o publicitaria. - -Suministrar los Datos Personales de los Usuarios a las entidades que intervengan en la resolución de disputas entre los mismos, tales como: Compañías de Seguros o tribunales competentes para solucionar tales disputas. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">4. Confidencialidad de la información. </h1> 
        <p class=\"gray\">Los datos de los Usuarios serán suministrados únicamente por BenchBanking Chile en las formas establecidas en estas Políticas de Privacidad. BenchBanking Chile hará todo lo que esté a su alcance para proteger la privacidad de la información. Puede suceder que en virtud de órdenes judiciales, o de regulaciones legales, BenchBanking Chile se vea compelido a revelar información a las autoridades o terceras partes bajo ciertas circunstancias, o bien en casos que terceras partes puedan interceptar o acceder a cierta información o transmisiones de datos en cuyo caso BenchBanking Chile no responderá por la información que sea revelada. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">5. Menores de Edad  </h1>
        <p class=\"gray\">Nuestros servicios sólo están disponibles para aquellas personas que tengan capacidad legal para contratar. Por lo tanto, aquellos que no cumplan con esta condición deberán abstenerse de suministrar información personal para ser incluida en nuestras bases de datos. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">6. Cookies  </h1>
        <p class=\"gray\">El Usuario del Sitio web de BenchBanking Chile conoce y acepta que BenchBanking Chile podrá utilizar un sistema de seguimiento mediante la utilización de cookies (las \"Cookies\"). Las Cookies son pequeños archivos que se instalan en el disco rígido, con una duración limitada en el tiempo que ayudan a personalizar los servicios. También ofrecemos ciertas funcionalidades que sólo están disponibles mediante el empleo de Cookies. Las Cookies se utilizan con el fin de conocer los intereses, el comportamiento y la demografía de quienes visitan o son visitantes de nuestro Sitio web y de esa forma, comprender mejor sus necesidades e intereses y darles un mejor servicio o proveerle información relacionada. También usaremos la información obtenida por intermedio de las Cookies para analizar las páginas navegadas por el visitante o Usuario, las búsquedas realizadas, mejorar nuestras iniciativas comerciales y promocionales, mostrar publicidad o promociones, banners de interés, noticias sobre BenchBanking Chile, perfeccionar nuestra oferta de contenidos y artículos, personalizar dichos contenidos, presentación y servicios. Adicionalmente BenchBanking Chile utiliza las Cookies para que el Usuario no tenga que introducir su clave tan frecuentemente durante una sesión de navegación, también para contabilizar y corroborar los registros, la actividad del Usuario y otros conceptos y acuerdos comerciales, siempre teniendo como objetivo de la instalación de las Cookies, el beneficio del Usuario que la recibe, y no será usado con otros fines ajenos a BenchBanking Chile. Se establece que la instalación, permanencia y existencia de las Cookies en el computador del Usuario o del visitante depende de su exclusiva voluntad y puede ser eliminada de su computador cuando así lo desee. Para saber cómo quitar las Cookies del sistema es necesario revisar la sección Ayuda (Help) del navegador. También, se pueden encontrar Cookies u otros sistemas similares instalados por terceros en ciertas páginas de nuestro Sitio. BenchBanking Chile no controla el uso de Cookies por terceros. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">7. Emails </h1>
        <p class=\"gray\"> Podemos enviarle e-mails con los siguientes propósitos:

- Como parte del Servicio. Por ejemplo, le enviaremos (o podríamos enviarle) e-mails en las siguientes circunstancias -- Luego de la registración, notificándole los datos de su cuenta -- E-mails con recordatorios de los servicios que ofrecemos (especialmente aquellos que aun no haya utilizado o no haya utilizado en un tiempo considerable) -- Para enviar información sobre los creditos que haya pedido - Como parte de un Newsletter - Como e-mails promocionales - Para ofrecer servicios relacionados

De todas maneras, en cada uno de los e-mails que enviemos siempre ofreceremos la posibilidad de desuscribirse (opt-out) para dejar de recibir e-mails en el futuro. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">8. Seguridad y almacenamiento  </h1>
        <p class=\"gray\">Empleamos diversas técnicas de seguridad para proteger tales datos de accesos no autorizados por visitantes del Sitio de dentro o fuera de nuestra compañía. Sin embargo, es necesario tener en cuenta que la seguridad perfecta no existe en Internet. Por ello, BenchBanking Chile no se hace responsable por interceptaciones ilegales o violación de sus sistemas o bases de datos por parte de personas no autorizadas. BenchBanking Chile, tampoco se hace responsable por la indebida utilización de la información obtenida por esos medios. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">9. Transferencia en circunstancias especiales   </h1>
        <p class=\"gray\">Si existe una venta, una fusión, consolidación, cambio en el control societario, transferencia de activos sustancial, reorganización o liquidación de BenchBanking Chile entonces, en nuestra discreción, podemos transferir, vender o asignar la información recabada en este Sitio a una o más partes relevantes. </p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">10. Servicio al cliente   </h1>
        <p class=\"gray\">Si tiene alguna duda o preocupación no dude en contactarse con nuestro servicio al cliente: info@BenchBanking.com</p>
        
        <h1 class=\"orange\" style=\"margin:20px auto\">11. Modificaciones de las Políticas de Privacidad  </h1>
        <p class=\"gray\">BenchBanking Chile podrá modificar en cualquier momento los términos y condiciones de estas Políticas de Privacidad y confidencialidad. Cualquier cambio será efectivo apenas sea publicado en el Sitio. Dependiendo de la naturaleza del cambio podremos anunciar el mismo a través de: (a) la página de inicio del Sitio, o (b) un e-mail. De todas maneras, el continuo uso de nuestro Sitio implica la aceptación por parte del Usuario de los Terminos de esta Politica de Privacidad. Si Ud. no está de acuerdo con la Politica de Privacidad vigente absténgase de utilizar el Sitio. </p>
            
            </div>
               
</div>


<div class=\"clearfix\"></div>



</body>





<script src=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.Rut.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.flexslider-min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/jquery.carouFredSel-6.1.0-packed.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchpaginas/js/iniciador.js"), "html", null, true);
        echo "\"></script>


  ";
        // line 158
        $this->env->loadTemplate("BenchPaginasBundle:Default:footer.html.twig")->display($context);
        // line 159
        echo "





</html> ";
    }

    // line 35
    public function block_header($context, array $blocks = array())
    {
        // line 36
        echo "    
    
    
    
    
    
    ";
    }

    public function getTemplateName()
    {
        return "BenchPaginasBundle:Default:recupera.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  252 => 36,  249 => 35,  239 => 159,  237 => 158,  231 => 155,  227 => 154,  223 => 153,  219 => 152,  215 => 151,  124 => 63,  102 => 43,  99 => 35,  97 => 34,  88 => 28,  84 => 27,  75 => 21,  71 => 20,  67 => 19,  63 => 18,  59 => 17,  55 => 16,  51 => 15,  47 => 14,  42 => 12,  38 => 11,  34 => 10,  30 => 9,  20 => 1,);
    }
}
