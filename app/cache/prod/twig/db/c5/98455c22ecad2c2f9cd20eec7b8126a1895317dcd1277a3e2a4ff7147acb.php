<?php

/* BenchCreditosBundle:Default:CreditoConsolidacion.html.twig */
class __TwigTemplate_dbc598455c22ecad2c2f9cd20eec7b8126a1895317dcd1277a3e2a4ff7147acb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<script src=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchcreditos/js/combos.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchcreditos/js/CreditoConsolidacion.js"), "html", null, true);
        echo "\"></script>

<form id=\"combocon\"  action=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("comboconsolidacion");
        echo "\" autocomplete=\"off\">
    

</form>
<form class=\"form-box-qn\" style=\"padding:0 0 5% 10%;\" id=\"formCons\" action=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("guardaconsolidacion");
        echo "\" method=\"POST\" autocomplete=\"off\">
  

<input type=\"hidden\" id=\"token\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">

    
    
  
  
  <div class=\"col1\">
<label class=\"label-text\" for=\"nombres\">Crédito a Consolidar / Refinanciar</label>
               <select class=\"data\" select=\"selected\" name=\"tipo\" id=\"ComboCon1\" >
<option value=\"\">Opción</option>
<option value=\"Hipotecario\"> Hipotecario</option>
<option value=\"Consumo\">Consumo</option>
<option value=\"Automotriz\">Automotriz</option>
<option value=\"Tarjeta\">Tarjeta de Crédito</option>
</select>

<label class=\"label-text\" for=\"\">Monto de Cr&eacute;dito:</label>
\t\t<input type=\"text\" name=\"montoCon1\" id=\"montoCon1\" class=\"input-box2\" id=\"rut\" title=\"Ingresa monto en pesos sin puntos ni comas\" placeholder=\"\$\"  onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\" />
        
<label class=\"label-text\" for=\"\">Plazo Solicitado</label>
<select class=\"data\" select=\"selected\" name=\"plazo\" id=\"ComboCon2\">
<option value=\"\">Opción</option>               

</select>        




</div>






  <div class=\"col1\">
<label class=\"label-text\" for=\"nombres\">Crédito a Consolidar / Refinanciar</label>
               <select class=\"data\" select=\"selected\" name=\"tipo\" id=\"ComboCon3\">
<option value=\"\">Opción</option>
<option value=\"Hipotecario\"> Hipotecario</option>
<option value=\"Consumo\">Consumo</option>
<option value=\"Automotriz\">Automotriz</option>
<option value=\"Tarjeta\">Tarjeta de Crédito</option>
</select>

<label class=\"label-text\" for=\"rut\">Monto de Cr&eacute;dito:</label>
\t\t<input type=\"text\" name=\"montoCon3\" id=\"montoCon3\" class=\"input-box2\" id=\"rut\" title=\"Ingresa monto en pesos sin puntos ni comas\" placeholder=\"\$\"  onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"/>

        
<label class=\"label-text\" for=\"nombres\">Plazo Solicitado</label>
               <select class=\"data\" select=\"selected\" name=\"plazo\" id=\"ComboCon4\">
<option value=\"\">Opción</option>               

</select>        
        
 </div>       



 
  <div class=\"col1\">
<label class=\"label-text\" for=\"nombres\">Crédito a Consolidar / Refinanciar</label>
               <select class=\"data\" select=\"selected\" name=\"tipo\" id=\"ComboCon5\">
<option value=\"\">Opción</option>
<option value=\"Hipotecario\"> Hipotecario</option>
<option value=\"Consumo\">Consumo</option>
<option value=\"Automotriz\">Automotriz</option>
<option value=\"Tarjeta\">Tarjeta de Crédito</option>
</select>

<label class=\"label-text\" for=\"rut\">Monto de Cr&eacute;dito:</label>
\t\t<input type=\"text\" name=\"montoCon5\" id=\"montoCon5\" class=\"input-box2\" title=\"Ingresa monto en pesos sin puntos ni comas\"  placeholder=\"\$\"  onkeyup=\"puntitos(this,this.value.charAt(this.value.length-1))\"/>
        
<label class=\"label-text\" for=\"nombres\">Plazo Solicitado</label>
               <select class=\"data\" select=\"selected\" name=\"plazo\" id=\"ComboCon6\">
<option value=\"\">Opción</option>               

</select>        
        
 </div>       
        


        
  
      <button style=\"float:left;\" id=\"guardadatos1\" class=\"classname\" value=\"Agregar\" type=\"submit\" >GUARDAR Y SIGUIENTE</button>     
      </form>
      <section id=\"RespuestaCons\"></section>";
    }

    public function getTemplateName()
    {
        return "BenchCreditosBundle:Default:CreditoConsolidacion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 13,  38 => 10,  31 => 6,  26 => 4,  22 => 3,  19 => 2,);
    }
}
