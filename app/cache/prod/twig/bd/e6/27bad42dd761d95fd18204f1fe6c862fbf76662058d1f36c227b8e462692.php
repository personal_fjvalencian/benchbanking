<?php

/* BenchAdminBundle:Default:prueba.html.twig.twig */
class __TwigTemplate_bde627bad42dd761d95fd18204f1fe6c862fbf76662058d1f36c227b8e462692 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"utf-8\" />
  <title>Administrador Bench Banking</title>
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
  <meta name=\"description\" content=\"\" />
  <meta name=\"author\" content=\"\" />

  <meta name=\"robots\" content=\"noindex\">
  <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"main-theme-script\" />
  <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/themes/default.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" id=\"theme-specific-script\" />
  <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/js/mask.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.js"), "html", null, true);
        echo "\"></script>





     <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

     <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/tables/jquery.dataTables.columnFilter.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>

  
    
     

  
  <link href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/css/datepicker.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  


  <link rel=\"stylesheet\" type=\"text/css\" media=\"screen,projection\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/css/uniform.default.css"), "html", null, true);
        echo "\" />
  

  <link type=\"text/css\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.intenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />   

 
  <link href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/simplenso.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
  

  <link rel=\"shortcut icon\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/favicon.ico"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"144x144\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-144-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"114x114\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-114-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" sizes=\"72x72\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-72-precomposed.png"), "html", null, true);
        echo "\" />
  <link rel=\"apple-touch-icon-precomposed\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/images/ico/apple-touch-icon-57-precomposed.png"), "html", null, true);
        echo "\" />
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>
<body id=\"dashboard\" class=\"hidden\">






 <div id=\"div_carga\">
   <img id=\"cargador\" src=\"./img/loader.gif\"/>
   </div>

<!-- Top navigation bar -->
<div class=\"navbar navbar-fixed-top\">
  <div class=\"navbar-inner\">
    <div class=\"container-fluid\">
      <a class=\"btn btn-navbar\" data-toggle=\"collapse\" data-target=\".nav-collapse\">
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </a>
      <a class=\"brand\" href=\"index.html\">Bench banking</a>
      <div class=\"btn-group pull-right\">
        <a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
          <i class=\"icon-user\"></i> 
          <span class=\"caret\"></span>
        </a>
        <ul class=\"dropdown-menu\">
        
          <li><a href=\"login.html\">Logout</a></li>
        </ul>
      </div>
      <div class=\"nav-collapse\">
          
          
        <ul class=\"nav\">
        
              
          <li class=\"dropdown\">
                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                  Menu
                      <b class=\"caret\"></b>
                </a>
                <ul class=\"dropdown-menu\">
                \t  <li class=\"nav-header\">Menu</li>        
            
          <li class=\"active\"><a href=\"";
        // line 93
        echo $this->env->getExtension('routing')->getPath("usuarioPedido");
        echo "\"><i class=\"icon-home\"></i> Home </a></li>
          <li><a href=\"";
        // line 94
        echo $this->env->getExtension('routing')->getPath("vistaArchivados");
        echo "\"><i class=\"icon-edit\"></i>Ver archivados /  Usuarios</a></li>
        
        
          
                </ul>
          </li>
       
        </ul>
      </div>
    </div>
  </div>
</div>
  
<div class=\"container-fluid\">
  <div class=\"row-fluid\">
    
    <!-- Bread Crumb Navigation -->
  
      

   
  
         
         <!-- Portlet Set 5 -->
             
         

             <h6><a href=\"";
        // line 121
        echo $this->env->getExtension('routing')->getPath("vistaArchivados");
        echo "\">Ver archivados /  Usuarios</a></h6>
             <h6><a href=\"";
        // line 122
        echo $this->env->getExtension('routing')->getPath("usuarioPedido");
        echo "\">Ver Con Pedidos / Usuarios</a></h6>
        
\t  <!-- Table -->
      
      
         \t <!-- Portlet: Browser Usage Graph -->
            
                 <h4 class=\"box-header round-top\"></h4>
                 <H2>Todos los Usuarios </H2>
                  <a class=\"box-btn\" title=\"close\"><i class=\"icon-remove\"></i></a>
                     <a class=\"box-btn\" title=\"toggle\"><i class=\"icon-minus\"></i></a>  
                 
              </h4>         
              
      
      

                     <section id=\"contenido\">
                         
                         
          
                           
<link href=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/css/table.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />

<link href=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/css/smoothness/jquery-ui-1.8.4.custom.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
<script type=\"text/javascript\" src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.datatable.inc.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/alidatatable/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>    
   


";
        // line 153
        echo $this->env->getExtension('DatatableBundle')->datatable(array("edit_route" => "add", "delete_route" => "cotizar", "otra_route" => "CrearPdf", "ver_route" => "ver", "archivar_route" => "archivarUsuario2", "UpDicom_url" => "UpDicom", "Up_url" => "otrosUpload", "o_route" => " ", "js" => array("sAjaxSource" => $this->env->getExtension('routing')->getPath("grid"))));
        // line 168
        echo "


    <script>

  
    
</script>                     




                     </section>










                  </div>
              </div>
            </div><!--/span-->




   </div>
      </div>  
 
    </div><!--/span-->
  </div><!--/row-->



<div style=\"clear:both;\"></div>








 <!-- Modal -->
 <div id=\"myModal\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
     <div class=\"modal-header\">
         <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>
         <h3 id=\"myModalLabel\">Seleccione formato exportacion</h3>
     </div>
     <div class=\"modal-body\">


         <p><a href=\"\" id=\"estandar\" target=\"_blank\">Formato Estandar </a></p>
         <p><a href=\"\" id=\"bbva\" target=\"_blank\">Formato Bbva </a></p>
         <p><a href=\"\" id=\"bbva2\" target=\"_blank\">Formato Bbva 2 </a></p>
         <p><a href=\"\" id=\"bci\" target=\"_blank\">Formato Bci </a></p>
         <p><a href=\"\" id=\"consorcio\" target=\"_blank\">Formato Consorcio </a></p>
         <p><a href=\"\" id=\"metlife\" target=\"_blank\">Formato Metlife </a></p>

 <p><a href=\"\" id=\"cruzdelsur\" target=\"_blank\">Cruz del Sur </a></p>
     </div>
     <div class=\"modal-footer\">
         <button class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\">Close</button>

     </div>
 </div>









 <footer>
    <p>&copy; Bench Banking</p>
  </footer>
    <div id=\"box-config-modal\" class=\"modal hide fade in\" style=\"display: none;\">
      <div class=\"modal-header\">
        <button class=\"close\" data-dismiss=\"modal\">×</button>
        <h3></h3>
      </div>
      <div class=\"modal-body\">
        <p></p>
      </div>
      <div class=\"modal-footer\">
        <a href=\"#\" class=\"btn btn-primary\" data-dismiss=\"modal\"></a>
        <a href=\"#\" class=\"btn\" data-dismiss=\"modal\"></a>
      </div>
    </div>
</div><!--/.fluid-container-->

    
  
    <script src=\"";
        // line 267
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.core.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 268
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js"), "html", null, true);
        echo "\"></script>
    
  
    <script src=\"";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 275
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js"), "html", null, true);
        echo "\"></script>

 
    <script src=\"";
        // line 278
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 279
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/bootbox/bootbox.min.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 281
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>

\t\t
    <!-- jQuery Cookie -->    
    <script src=\"";
        // line 285
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/jquery.cookie/jquery.cookie.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Full Calender -->
    <script type='text/javascript' src='";
        // line 288
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/fullcalendar/fullcalendar/fullcalendar.min.js"), "html", null, true);
        echo "'></script>
    
    <!-- CK Editor -->
\t<script type=\"text/javascript\" src=\"";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
    
    <!-- Chosen multiselect -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/chosen/chosen/chosen.jquery.min.js"), "html", null, true);
        echo "\"></script>  
    
    <!-- Uniform -->
    <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 297
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/uniform/jquery.uniform.min.js"), "html", null, true);
        echo "\"></script>
    
    
\t
    <script src=\"";
        // line 301
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchadmin/scripts/simplenso/simplenso.js"), "html", null, true);
        echo "\"></script>
  </body>


                 
                         
                         
                         
                         
";
    }

    public function getTemplateName()
    {
        return "BenchAdminBundle:Default:prueba.html.twig.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  427 => 301,  420 => 297,  414 => 294,  408 => 291,  402 => 288,  396 => 285,  389 => 281,  384 => 279,  380 => 278,  374 => 275,  370 => 274,  364 => 271,  360 => 270,  356 => 269,  352 => 268,  348 => 267,  247 => 168,  245 => 153,  238 => 149,  234 => 148,  230 => 147,  226 => 146,  221 => 144,  196 => 122,  192 => 121,  162 => 94,  158 => 93,  108 => 46,  104 => 45,  100 => 44,  96 => 43,  92 => 42,  86 => 39,  80 => 36,  74 => 33,  67 => 29,  57 => 22,  52 => 20,  43 => 14,  39 => 13,  35 => 12,  31 => 11,  19 => 1,);
    }
}
