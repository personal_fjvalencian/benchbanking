<?php

/* BenchApiBundle:Usuario:getUsuario.html.twig */
class __TwigTemplate_017cc1a2e56c30dbda51e51270a1987430847e91d47b229b389fe71dccb9b6d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
    }

    public function getTemplateName()
    {
        return "BenchApiBundle:Usuario:getUsuario.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
