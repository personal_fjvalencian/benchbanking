<?php

/* BenchMobilBundle:Default:index.html.twig */
class __TwigTemplate_dca6b403504d800d09908a6a9057d035796e82f1af69aa229246bcfd3f641152 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />

<!-- CHANGE THIS TITLE TAG -->
<title>BenchBanking Mobile</title>


    <!-- Google Tag Manager -->
    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-T8RRB4\"
                      height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T8RRB4');</script>

<!-- media-queries.js -->
<!--[if lt IE 9]>
\t<script src=\"http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js\"></script>
<![endif]-->
<!-- html5.js -->
<!--[if lt IE 9]>
\t<script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
<![endif]-->


<!--<link href=\"font/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\" />-->\t

<link href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">  

<link href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/css/bootstrap-responsive.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />



<link href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/css/styles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />

<link href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/css/media-queries.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />


<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/fancybox/jquery.fancybox-1.3.4.css"), "html", null, true);
        echo "\" media=\"screen\" />

<meta name=\"viewport\" content=\"width=device-width\" />
 
<!--<link rel=\"shortcut icon\" href=\"favicon.ico\" type=\"image/x-icon\">-->

<link href='https://fonts.googleapis.com/css?family=Exo:400,800' rel='stylesheet' type='text/css'>

<script>
window.addEventListener('load', function(){setTimeout(scrollTo, 0, 0, 1);}, false);
window.onload = function() {setTimeout(function(){window.scrollTo(0, 1);}, 100);}
</script>

</head>

<body data-spy=\"scroll\">

<!-- TOP MENU NAVIGATION -->
<div class=\"navbar navbar-fixed-top\" id=\"home\">
\t<div class=\"navbar-inner\">
\t\t<div class=\"container\">
\t
\t\t\t<a class=\"brand\" href=\"#\"><img src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/logo.png"), "html", null, true);
        echo "\"></a>
\t
\t
\t\t
\t\t\t
\t\t\t\t<ul id=\"nav-list\" class=\"nav\">





                    <li><a id=\"registrate\" href=\"#contact\" style=\"height:90px\"><img  src=\" ";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/btn_registrate.png"), "html", null, true);
        echo "\"></a></li>

                    <li  style=\" background-color:#E8501F;;\">&nbsp; <a  style=\"margin-top:-2%;\" href=\"";
        // line 77
        echo $this->env->getExtension('routing')->getPath("homeMobile2");
        echo "\" target=\"_blank\" style=\"margin-left:0%;\"><img id=\"simular\" src=\" ";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/botonSimular.jpg"), "html", null, true);
        echo "\"></a></li>

                    <li style=\" background-color:#F95B00;\"><a href=\"#quehacemos\"><img src=\" ";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/btn_qh.png"), "html", null, true);
        echo "\"></a></li>


                    <li style=\"background-color:#E8501F;\"><a href=\"#conoce\"><img src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/btn_cdc.png"), "html", null, true);
        echo "\"></a></li>
                    
\t\t\t\t</ul>
                        
                        
\t\t 
\t\t
\t  </div>
\t</div>


</div>


<!-- MAIN CONTENT -->
<div class=\"container content container-fluid\">






<!-- CONTACT -->
\t<div class=\"row-fluid\" id=\"contact\">
    
    <div class=\"span6 updates\" id=\"updates\">

\t\t\t\t<ul id=\"nav-list\">
                                    
                                     
                    <li><a href=\"";
        // line 112
        echo "indexMobil";
        echo "\">
                            
                            <img src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/laptop.png"), "html", null, true);
        echo "\"  id=\"versionLap\"></a></li>
                            
                            
\t\t\t\t</ul>
\t\t\t\t

\t\t\t
\t\t</div>
    
    
\t<div class=\"span6\">
<h2 class=\"page-title\" id=\"scroll_up\">
<a href=\"#home\" class=\"arrow-top\">
                <img src=\" ";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/arrow-top.png"), "html", null, true);
        echo "\">
\t\t\t\t</a>
\t\t\t\tRegístrate
\t\t\t</h2>
\t\t
\t\t<!-- CONTACT INFO -->
\t\t<div class=\"span6\" id=\"contact-info\">
\t\t\t<p id=\"tituloRegistrate\">Regístrate aquí y obtén la mejor alternativa de crédito con nosotros.\t\t</p>
\t\t</div>
\t\t
\t\t<!-- CONTACT FORM -->
\t  <div class=\"span6\" id=\"contact-form\">
\t\t
\t\t\t\t<fieldset>
\t\t\t\t\t<div class=\"control-group\">
\t\t\t\t\t\t<label class=\"control-label\" for=\"name\">RUT</label>
\t\t\t\t\t\t<div class=\"controls\">

\t\t\t\t\t\t\t<span class=\"text-msje\" id=\"rutNoValido\">Rut No valido </span>


\t\t\t\t\t\t\t<input class=\"input-xlarge\" type=\"text\" id=\"rut\" placeholder=\"Ingresa tu rut sin puntos\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"control-group\">
\t\t\t\t\t\t<label class=\"control-label\" for=\"email\">Email</label>
\t\t\t\t\t\t<div class=\"controls\">
\t\t\t\t\t\t\t<span class=\"text-msje\" id=\"mailNoValido\">Mail no valido</span>
\t\t\t\t\t\t\t<input class=\"input-xlarge\" type=\"text\" id=\"email\" placeholder=\"Ejemplo: juan@gmail.com\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"control-group\">
\t\t\t\t\t\t<label class=\"control-label\" for=\"subject\">Contraseña</label>
\t\t\t\t\t\t<div class=\"controls\">
\t\t\t\t\t\t\t<input class=\"input-xlarge\" type=\"password\" id=\"contrasena1\" placeholder=\"Ingresa tu contraseña\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"control-group\">
\t\t\t\t\t\t<label class=\"control-label\" for=\"message\">Confirmar Contraseña</label>
\t\t\t\t\t\t<div class=\"controls\">

\t\t\t\t\t\t\t<span class=\"text-msje\" id=\"confirmarContrasena\">Las contraseñas no coinciden </span>
\t\t\t\t\t\t\t<input class=\"input-xlarge\" type=\"password\" id=\"contrasena2\" placeholder=\"Repite tu contraseña\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                    <div class=\"text-msje\" id=\"mensajeRespuesta\">
\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-actions\">
\t\t\t\t\t\t<button  id=\"enviar\" class=\"btn btn-primary\">Enviar</button>
\t\t\t\t\t</div>


\t\t\t\t\t<span  class=\"text-msje\" id=\"Respuesta\"></span>

                    
\t\t\t\t</fieldset>
\t\t
\t  </div>
        
      </div>
      
      </div>
\t
\t
\t
\t<!-- QUE HACEMOS -->
\t<div class=\"row-fluid\" id=\"quehacemos\">
\t
\t\t<div class=\"span6\">
\t\t\t<h2 class=\"page-title\" id=\"scroll_up\">
            <a href=\"#home\" class=\"arrow-top\">
                
              
                
                
\t\t\t\t<img src=\"  ";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/arrow-top.png"), "html", null, true);
        echo "\">
\t\t\t\t</a>
\t\t\t\tQué Hacemos
\t\t\t</h2>
\t\t\t
\t\t\t<p>Pedir un préstamo es una tarea abrumadora, toma mucho tiempo y es difícil de entender. En nuestra plataforma puedes subir en un solo lugar tu información, elige los bancos donde deseas cotizar y nosotros se la enviamos.</p> 
\t\t  <p>¡ÚNETE!<br>
\t\t    <br>
\t\t    Somos un equipo de emprendedores que con el apoyo de Startup Chile desarrolló una plataforma que permite a sus clientes cotizar y contratar sus créditos en un sólo lugar, logrando así elegir el crédito más conveniente en términos de cuota y calidad de atención.<br />
\t\t  </p>
\t\t\t
\t\t</div>
        

\t</div>
    
    
    
    <!-- CONOCE DE CREDITOS -->
\t<div class=\"row-fluid\" id=\"conoce\">
\t
\t  <div class=\"span6\">
\t\t<h2 class=\"page-title\" id=\"scroll_up\">
        <a href=\"#home\" class=\"arrow-top\">
            
              
\t\t\t\t<img src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/img/arrow-top.png"), "html", null, true);
        echo "\">
\t\t  </a>
\t\t\t\tConoce de Créditos
\t\t\t</h2>
\t\t\t
\t\t  <p>¿Qué es la CAE?</p>
\t\t\t<p>Es la Carga Anual Equivalente. Es un porcentaje que permite que puedas comparar qué empresa te ofrece el crédito más barato. Verifica lo siguiente: Siempre será más barato el crédito que tenga la Carga Anual Equivalente más baja. La Carga Anual Equivalente incluye todos los gastos y costos del crédito y los expresa en un sólo porcentaje que permite compararlo. Fíjate en LA CAE, así podrás vitrinear y elegir la opción de crédito que más te convenga.</p>
\t\t\t<p> • Mismo plazo (ejemplo 12 meses)<br>
\t\t\t  • Mismo monto (ejemplo 1 millón de pesos)<br>
\t\t\t  • Mira el porcentaje que señala LA CAE</p>
\t\t\t<p>¿Qué me deben informar al solicitar un crédito?</p>
\t\t\t<p>Deben informarte:</p>
\t\t\t<p> • El costo total del producto o servicio. La CAE o Carga Anual Equivalente.<br>
\t\t\t  • El precio al contado del bien o servicio que se trate.<br>
\t\t\t  • La tasa de interés del crédito.<br>
\t\t\t  • Otros costos como: Impuestos correspondientes a la operación del crédito, gastos notariales, seguros que hayas aceptado o contratado, otros.<br>
\t\t\t  • Las alternativas que te ofrecen para pagar el crédito según monto solicitado, por ejemplo: número de cuotas, número de pagos a efectuar y su periodicidad (mensual, anual, etc).</p>
\t\t\t<p><br />
\t    </p>
\t\t\t
\t\t</div>
        
        
\t</div>
\t
        
\t\t
\t</div>
\t
</div>


<!-- FOOTER -->
<div class=\"footer container container-fluid\">

\t<!-- COPYRIGHT - EDIT HOWEVER YOU WANT! -->
\t<div id=\"copyright\">
\t\tCopyright &copy; 2013 BenchBanking.<br>
\t</div>
\t
\t<!-- CREDIT - PLEASE LEAVE THIS LINK! -->
\t<div id=\"credits\">
\t\tby <a href=\"http://www.mediadiv.cl\">Mediadiv</a>
\t</div>

</div>










<script src=\"https://code.jquery.com/jquery-1.7.2.min.js\"></script>
<script src=\"";
        // line 286
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/js/general.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 288
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 290
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/js/bootstrap-collapse.js"), "html", null, true);
        echo "\"></script>

<script src=\"";
        // line 292
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/js/bootstrap-scrollspy.js"), "html", null, true);
        echo "\"></script>



<script src=\"";
        // line 296
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/fancybox/jquery.mousewheel-3.0.4.pack.js"), "html", null, true);
        echo "\"></script>


<script src=\"";
        // line 299
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/fancybox/jquery.fancybox-1.3.4.pack.js"), "html", null, true);
        echo "\"></script>



<script src=\"";
        // line 303
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/js/init.js"), "html", null, true);
        echo "\"></script>




<script src=\"";
        // line 308
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/js/init.js"), "html", null, true);
        echo "\"></script>


<script src=\"";
        // line 311
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/benchmobil/js/jqueryRut.js"), "html", null, true);
        echo "\"></script>
<form id=\"reg\" action=\"";
        // line 312
        echo $this->env->getExtension('routing')->getPath("guardarRegistro");
        echo "\" ></form>




<script>


\$(document).ready(function() {

    //window.location.href = \"simulacion\";





    var ancla = \"";
        // line 328
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\";


    \$('#versionLap').click(function(){

        if(ancla == 'contact-form'){
            window.location.href = \"../../home\";

        }else{

            window.location.href = \"../home\";


        }

    });




    \$('#simular').click(function(){



        if(ancla == 'contact-form'){


            window.location.href = \"../simulacion\";


        }else{

            window.location.href = \"simulacion\";
        }

    });

    if(ancla == 'contact-form'){



        var scrollIndex = \$('#tituloRegistrate').offset();
        \$(\"body\").animate({scrollTop:(scrollIndex.top)},500,\"linear\");



    }else{




    }

});
    

\$('document').ready(function() {


\$('#rutNoValido').hide('fast');


\$('#rut').Rut({


  on_error: function(){ 


  \t\$('#rutNoValido').show('fast');
  



},

on_success: function(){ 

\$('#rutNoValido').hide('fast');


 } 

});





\$('#mailNoValido').hide('fast');



    \$('#mailNoValido').hide('fast');
    \$('#confirmarContrasena').hide('fast');
    \$('#mensajeRespuesta').hide('fast');

// funcion validar mail

//





\t  function validar_email(valor)
    {
        
        var filter = /[\\w-\\.]{3,}@([\\w-]{2,}\\.)*([\\w-]{2,}\\.)[\\w-]{2,4}/;
   
        if(filter.test(valor))
            return true;
        else
            return false;
    }



\t\$('#enviar').click(function(e) {
  e.preventDefault();
  
      \$(\"#Respuesta\").text('');



\t\tif(\$(\"#email\").val() == '')
        {
            \$('#mailNoValido').show('fast');
        }else 

        if(validar_email(\$(\"#email\").val()))
        

        {
          
        }else
        

        {
            \$('#mailNoValido').show('fast');
        }




     var contrasena1 = \$('#contrasena1').val();
     var contrasena2 = \$('#contrasena2').val();


     if(contrasena1 != contrasena2){


    \$('#confirmarContrasena').show('fast');


     }else{



       if(validar_email(\$(\"#email\").val())){

 \$('#mailNoValido').hide('fast');
  \$('#confirmarContrasena').hide('fast');
  \$('#rutNoValido').hide('fast');




      
      
        
        




\t\tvar myData = 'rut='+\$('#rut').val()


         +'&email='+\$('#email').val()
         +'&contrasena1='+\$('#contrasena1').val()
         +'&contrasena2='+\$('#contrasena2').val()
\t\t;










\t\tjQuery.ajax({

\t\t\ttype: \"POST\",
\t\t\turl:\$('#reg').attr(\"action\"),
\t\t\tdataType: \"text\",
\t\t\tdata : myData,

\t\t

\t\t\tsuccess : function (response){
                                                    if(response== '900'){
                                                        
                                                        \$(\"#Respuesta\").append('Ya existe este Rut en nuestra base datos.');
                                                        
                                                       
                                                    }
                                                    
                                                    if(response == '100'){
                                                    
                                                      \$(\"#Respuesta\").append('¡Gracias por registrarte en BenchBanking!');

                                          
                                                
                                                
                                
                                                    }
\t\t\t\t



\t\t\t},


\t\t\terror:function(xhr, ajaxOptions, thrownError){


\t\t\t\t\t alert(thrownError);


\t\t\t}




\t\t});



 }
 }

\t});

});



</script>

</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "BenchMobilBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  425 => 328,  406 => 312,  402 => 311,  396 => 308,  388 => 303,  381 => 299,  375 => 296,  368 => 292,  363 => 290,  358 => 288,  353 => 286,  293 => 229,  264 => 203,  185 => 127,  169 => 114,  164 => 112,  131 => 82,  125 => 79,  118 => 77,  113 => 75,  99 => 64,  74 => 42,  68 => 39,  63 => 37,  56 => 33,  51 => 31,  19 => 1,);
    }
}
