
$(document).ready(function() {
    
    $('#SiguienteDebesHipotecario').click(function(event) {

                 $(".spinner").css("display", "block");
        if ($('#respuestaDebesHipo>tbody>tr').length > 0) {
            $('#ulDebes a[href="#debeLcredito"]').tab('show');
            $('#linkLcredito').prop('disabled', false);
            $("#linkLcredito").popover('destroy');
                     $(".spinner").css("display", "none");

        } else {

         $(".spinner").css("display", "none");
            $('#debesHipotecario2').parsley('validate');
            $('#debesHipotecario3').parsley('validate');
    
        }

    });

    $("#debesHipotecario").click(function(e) {

                 $(".spinner").css("display", "block");

        e.preventDefault();

        var myData = 'id=' + $("#id").val()
                + '&institucion=' + $("#institucion").val()
                + '&pagoM=' + $("#pagoM").val()
                + '&deudaT=' + $("#deudaT").val()
                + '&vencimientoF=' + $("#vencimientoF").val()
                + '&deudaV=' + $("#deudaV").val()
                + '&token=' + $('#token').val();



        jQuery.ajax({
            type: "POST", // metodo post o get 
            url: $("#debesHipotecario2").attr("action"),
            dataType: "text", // tipo datos text o json etc 
            data: myData,
            success: function(response) {
                var respuesta = response;

         $(".spinner").css("display", "none");
         
                if (respuesta == '200') {

                    $('#debesHipotecario2').parsley('validate');
                    $('#debesHipotecario3').parsley('validate');


                } else {


                    var trackPageview = '/formulario/que-debes/hipotecario/Guardado';
                    dataLayer.push({
                        'event':'PageView',
                        'trackPageview': trackPageview });





                    $("#respuestaDebesHipo").append(response);
                    $('#linkLcredito').prop('disabled', false);
                    $("#linkLcredito").popover('destroy');
                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);



            }

        });
    });



// eliminar 

    $("#respuestaDebesHipo").on("click", ".del_button", function(e) {
        e.preventDefault();
        var clickedID = this.id.split('-');
        var DbNumberID = clickedID[1]; // toma el numero del array

        var myData = 'recordToDelete=' + DbNumberID;
        jQuery.ajax({
            type: "POST",
            url: $("#deldebesHipotecario").attr("action"),
            dataType: "text",
            data: myData,
            success: function(response) {

                $('#itemDebesHipotecario_' + DbNumberID).remove();

            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError);

            }

        });
    });

});