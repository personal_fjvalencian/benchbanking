
$(document).ready(function() {
    
    
    	$('#SiguienteAutomoviles').click(function(event){

                $(".spinner").css("display", "block");

         if ($('#respuestaAutos>tbody>tr').length   >  0){

                $(".spinner").css("display", "none");
             
               $('#linkSociedades').prop('disabled' , false);
               $("#linkSociedades").popover('destroy');



               $('#tabtienes a[href="#tabSociedades"]').tab('show');
  
         }else{
             
                      $(".spinner").css("display", "none");
             
                       $('#formTienesAutos').parsley('validate');
                    $('#formTienesAutos2').parsley('validate');



         }

        });
        

    $("#guardaAutomoviles").click(function(e) {

        e.preventDefault();
         $(".spinner").css("display", "block");



        var myData = 'TienesAIDuser=' + $("#TienesAIDuser").val()
                + '&TienesATipo=' + $("#TienesATipo").val()
                + '&TienesAMarca=' + $("#TienesAMarca").val()
                + '&TienesAPatente=' + $("#TienesAPatente").val()
                + '&TienesAAno=' + $("#TienesAAno").val()
                + '&TienesAAvaluo=' + $("#TienesAAvaluo").val()
                + '&TienesAPrendado=' + $("input[name='TienesAPrendadoAuto']:checked").val()
                + '&TienesAModelo=' + $("#TienesAModelo").val()
                + '&token=' + $('#token').val();
        

        jQuery.ajax({
            type: "POST", // metodo post o get 
            url: $("#formTienesAutos").attr("action"),
            dataType: "text", // tipo datos text o json etc 
            data: myData,
            success: function(response) {

                
         $(".spinner").css("display", "none");



                var respuesta = response;

                if (respuesta == '200') {


                    $('#formTienesAutos').parsley('validate');
                    $('#formTienesAutos2').parsley('validate');




                } else {



                    var trackPageview = '/formulario/que-tienes/automoviles/Guardado';
                    dataLayer.push({
                        'event':'PageView',
                        'trackPageview': trackPageview });





                    $('#linkSociedades').prop('disabled', false);
                    $("#linkSociedades").popover('destroy');

                    $("#respuestaAutos").append(response);

                }


            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);



            }

        });
    });



// eliminar 

    $("#respuestaAutos").on("click", ".del_button", function(e) {
        e.preventDefault();
        var clickedID = this.id.split('-');
        var DbNumberID = clickedID[1]; // toma el numero del array

        var myData = 'recordToDelete=' + DbNumberID;


        jQuery.ajax({
            type: "POST",
            url: $("#delautomoviles").attr("action"),
            dataType: "text",
            data: myData,
            success: function(response) {

                $('#itemTienesAutos_' + DbNumberID).remove();

            },
            error: function(xhr, ajaxOptions, thrownError) {

                alert(thrownError);

            }

        });
    });

});